import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InjectService } from 'app/injectable-service/injectable-service';
import { DataService } from 'app/mts.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    orderStatus: OrderStatus;
    orderStatusParams: OrderStatusParams;
    orderPerformanceStatus: any;
    tatAnalysisStatus: any;
    failureAnalysisStatus: any;
    SampleReceivedOUTHOLD: any;
    MonthlyPassFailAnalysis: any;
    currentYear: number;
    adhocSearch: any;
    userDetail: any;
    userCredential: any;
    selectedYear: any;
    failureSelectedYear: number;
    tatSelectedYear: number;
    sampleReceivedYear: number;
    performanceSelectedYear: number;
    columnList: any;
    logoPath: any;

    constructor(private router: Router, private injectService: InjectService, private dataService: DataService) {
        this.orderStatus = new OrderStatus;
        this.orderStatusParams = new OrderStatusParams;
        this.orderPerformanceStatus = {};
        this.tatAnalysisStatus = {};
        this.failureAnalysisStatus = {};
    }

    ngOnInit() {
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
        if (this.userDetail && this.userCredential) {
            if (this.userCredential.UserPermission.ViewDashboard) {
                if (this.userCredential.BusinessCategoryId == 1) {
                    this.currentYear = new Date().getFullYear();
                    this.failureSelectedYear = this.currentYear;
                    this.tatSelectedYear = this.currentYear;
                    this.sampleReceivedYear = this.currentYear;
                    this.performanceSelectedYear = this.currentYear;
                    this.orderStatusParams.clientId = this.userCredential.CompanyId == 0 ? this.userCredential.UserCompanyId : this.userCredential.CompanyId;
                    this.orderStatusParams.currentYear = new Date().getFullYear();//this.currentYear;
                    this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
                    this.orderStatusParams.userCompanyId = this.userCredential.UserCompanyId;
                    this.orderStatusParams.userType = this.userCredential.UserType;
                    this.getOrderStatus(this.orderStatusParams);
                    // this.getFailureStatusReport(this.orderStatusParams);
                    this.getOrderPerformanceReport(this.orderStatusParams);
                    this.getTatAnalysisReport(this.orderStatusParams);
                    this.GetMonthlyPassFailAnalysis(this.orderStatusParams);
                    this.GetSampleReceivedOUTHOLD(this.orderStatusParams);
                    this.adhocSearch = [{ LogicalOperator: "AND", ColumnName: "", Operator: "", ColumnValue: "" }];
                    this.dataService.get('Order/GetMyOrderColumnsList?selectedCompany=' + this.userCredential.CompanyId + '&FormId=1').subscribe(
                        response => {
                            if (response.IsSuccess) {
                                // this.dynamicSearchColumns = response.SelectedSearchColumn;
                                // this.dynamicGridColumns = response.SelectedGridColumn;
                                this.columnList = response.SelectedAdHocSearchColumn;
                                // this.getOrderList();
                            }
                            else {
                                // this.getOrderList();
                                // swal('', response.Message)
                            }
                        }
                    );
                }
            } else {
                this.router.navigate(['/landing-page']);
            }
        } else {
            this.router.navigate(['/login']);
        }
    }




    // getFailureStatusReport(orderStatusParams) {
    //     this.dataService.post("Dashboard/FailureAnalysisReport", orderStatusParams).subscribe(
    //         response => {
    //             this.failureAnalysisStatus = response;
    //             this.createFailureAnalysisChart();
    //         }
    //     )
    // }
    GetMonthlyPassFailAnalysis(orderStatusParams) {
        this.dataService.post("Dashboard/GetMonthlyPassFailAnalysis", orderStatusParams).subscribe(
            response => {
                this.MonthlyPassFailAnalysis = response;
                this.createMonthlyPassFailAnalysisChart();
            }
        )
    }
    GetSampleReceivedOUTHOLD(orderStatusParams) {
        this.dataService.post("Dashboard/GetSampleReceivedOUTHOLD", orderStatusParams).subscribe(
            response => {
                this.SampleReceivedOUTHOLD = response;
                this.createSampleReceivedOUTHOLD();
            }
        )
    }

    getOrderPerformanceReport(orderStatusParams) {
        this.dataService.post("Dashboard/FetchPerformanceReport", this.orderStatusParams).subscribe(
            response => {

                this.orderPerformanceStatus = response;
                this.createPerformanceChart();
            }
        )
    }

    getOrderStatus(orderStatusParams) {
        this.dataService.post("Dashboard/ordersStatus", orderStatusParams).subscribe(
            response => {
                this.orderStatus = response;

                this.createOrderStatuschart();
            }
        );
    }

    getTatAnalysisReport(orderStatusParams) {
        this.dataService.post("Dashboard/TATAnalysisStatus", orderStatusParams).subscribe(
            response => {
                this.tatAnalysisStatus = response;
                this.createTATAnalysisChart();
            }
        )
    }
    createSampleReceivedOUTHOLD() {
        var chart = AmCharts.makeChart("SampleReceivedVsOutandHold", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "useGraphSettings": true,
                "position": "top"
            },

            "categoryField": "LoginMonth",
            "rotate": false,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "Hold:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "Hold",
                    "type": "column",
                    "valueField": "Hold",
                    "fillColors": "#67b7dc"
                },
                {
                    "balloonText": "IN:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0.2,
                    "title": "IN",
                    "type": "column",
                    "valueField": "IN",
                    "fillColors": "#FFA500"
                }
                , {
                    "balloonText": "Out:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-3",
                    "lineAlpha": 0.2,
                    "title": "Out",
                    "type": "column",
                    "valueField": "Out",
                    "fillColors": "#0067aa"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "bottom",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": this.SampleReceivedOUTHOLD.SampleReceivedOUTHOLD.SampleReceivedOUTHOLD

        });
    }
    createMonthlyPassFailAnalysisChart() {
        var chart = AmCharts.makeChart("MonthlyPassFailAnalysis", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "useGraphSettings": true,
                "position": "top"
            },
            "dataProvider": this.MonthlyPassFailAnalysis.MonthlyPassFailAnalysis.MonthlyPassFailAnalysis,
            "valueAxes": [{
                "integersOnly": true,
                "reversed": false,
                "axisAlpha": 0,
                "dashLength": 5,
                "gridCount": 10,
                "position": "left",
                "title": ""
            }],
            "startDuration": 0.5,
            "graphs": [{
                "balloonText": "Fail [[category]]: [[value]]",
                "bullet": "round",
                "title": "Fail",
                "valueField": "Fail",
                "fillAlphas": 0,
                "lineColor": "#ac2925"
            }, {
                "balloonText": "Pass [[category]]: [[value]]",
                "bullet": "round",
                "title": "Pass",
                "valueField": "Pass",
                "fillAlphas": 0,
                "lineColor": "#84b761"
            }],
            "chartCursor": {
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "LoginMonth",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "fillAlpha": 0,
                "fillColor": "",
                "gridAlpha": 0,
                "position": "bottom"
            },
            "export": {
                "enabled": false,
                "position": "bottom-right"
            }
        });

    }

    createFailureAnalysisChart() {
        var chart = AmCharts.makeChart("failureAnalysisChart", {
            "theme": "light",
            "type": "serial",
            "legend": {
                "horizontalGap": 5,
                "maxColumns": 5,
                "position": "top",
                "useGraphSettings": true,
                "markerSize": 10,
                "marginTop": 20
            },
            "columnWidth": 0.9,
            "dataProvider": this.failureAnalysisStatus.FailureAnalysisList.failureAnalysisReport,
            "valueAxes": [{
                "stackType": "3d",
                "position": "left",
                "title": "Failure Analysis",
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "Critical Failure in [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.1,
                "title": "Critical",
                "type": "column",
                "valueField": "CriticalFailure",
                "fillColors": "#ac2925",
                fixedColumnWidth: 75
            }, {
                "balloonText": "Major Failure in [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.1,
                "title": "Major",
                "type": "column",
                "valueField": "MajorFailure",
                "fillColors": "#FF8C00",
                fixedColumnWidth: 75
            },
            {
                "balloonText": "Minor Failure in [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.1,
                "title": "Minor",
                "type": "column",
                "valueField": "MinorFailure",
                "fillColors": "#fdd400",
                fixedColumnWidth: 75
            }
            ],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 60,
            "angle": 30,
            "categoryField": "LoginMonth",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "export": {
                "enabled": false
            }
        });

    }

    createOrderStatuschart() {
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "funnel",
            "theme": "light",
            "dataProvider":
                [{
                    "title": "Open Orders",
                    "value": this.orderStatus.OpenOrder
                }, {
                    "title": "Hold Orders",
                    "value": this.orderStatus.HoldOrder
                }, {
                    "title": "Closed Orders",
                    "value": this.orderStatus.ClosedOrder
                }, {
                    "title": "Failed Orders",
                    "value": this.orderStatus.FailOrder
                }],
            "titleField": "title",
            "marginRight": 180,
            "marginLeft": 15,
            "labelPosition": "right",
            "funnelAlpha": 0.9,
            "valueField": "value",
            "startX": 0,
            "neckWidth": "160",
            "startAlpha": 0,
            "outlineThickness": 1,
            "neckHeight": "0",
            "balloonText": "[[title]]:<b>[[value]]</b>",
        });
    }

    createPerformanceChart() {

        var chart = AmCharts.makeChart("performanceChart", {
            "theme": "light",
            "type": "serial",
            "legend": {
                "horizontalGap": 5,
                "maxColumns": 5,
                "position": "top",
                "useGraphSettings": true,
                "markerSize": 10,
                "marginTop": 20
            },
            "dataProvider": this.orderPerformanceStatus.PerformanceReportList.PerformanceReport,
            "valueAxes": [{
                "stackType": "3d",
                "position": "left",
                "title": "Performance",
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "Total Fail Order in [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "Fail",
                "type": "column",
                "valueField": "FailOrder",
                "fillColors": "#ac2925",
                fixedColumnWidth: 50,

            }, {
                "balloonText": "Total Pass Order in [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "Pass",
                "type": "column",
                "fillColors": "#84b761",
                "valueField": "PassOrder",
                fixedColumnWidth: 50,
            }],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 35,
            "angle": 40,
            "categoryField": "LoginYear",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "export": {
                "enabled": false
            }
        });
    }

    createTATAnalysisChart() {

        var dataProvider = this.tatAnalysisStatus.TATAnalysisList.tatAnalysisReport.map((data) => {
            if (data.Duration == "1-2 Days") {
                data.color = "#0067aa";
            }
            else if (data.Duration == "3-7 Days") {
                data.color = "#FFA500";
            }
            else if (data.Duration == "More Than 7 Days") {
                data.color = "#67b7dc";
            }
            return data;
        })

        var chart = AmCharts.makeChart("analysisChart", {
            "type": "pie",
            "theme": "light",
            "legend": {
                "markerSize": 10,
                "position": "top",
                "autoMargins": false,
                "maxColumns": 5
            },
            /* "titles": [{
                 "text": "TAT Analysis",
                 "size": 18
             }],*/
            "dataProvider": dataProvider,
            "valueField": "Count",
            "titleField": "Duration",
            "colorField": "color",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": false
            }
        });
    }

    // changeFailureAnalysisChart(currentYear) {
    //     this.orderStatusParams.currentYear = currentYear;
    //     this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
    //     this.getFailureStatusReport(this.orderStatusParams);
    // }
    changeMonthlyPassFailAnalysis(currentYear) {
        this.orderStatusParams.currentYear = currentYear;
        this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
        this.GetMonthlyPassFailAnalysis(this.orderStatusParams);
    }
    changeSampleReceivedVsOutandHold(currentYear) {

        this.orderStatusParams.currentYear = currentYear;
        this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
        this.GetSampleReceivedOUTHOLD(this.orderStatusParams);
    }
    changePerformanceStatusChart(currentYear) {
        this.orderStatusParams.currentYear = currentYear;
        this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
        this.getOrderPerformanceReport(this.orderStatusParams);
    }

    changeTATAnalysisChart(currentYear) {
        this.orderStatusParams.currentYear = currentYear;
        this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
        this.getTatAnalysisReport(this.orderStatusParams);
    }

    changeAllCharts(currentYear) {
        this.failureSelectedYear = currentYear;
        this.tatSelectedYear = currentYear;
        this.sampleReceivedYear = currentYear;
        this.performanceSelectedYear = currentYear;
        this.orderStatusParams.currentYear = currentYear;
        this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
        this.getOrderPerformanceReport(this.orderPerformanceStatus);
        this.getTatAnalysisReport(this.orderStatusParams);
        this.GetMonthlyPassFailAnalysis(this.orderStatusParams);
        this.GetSampleReceivedOUTHOLD(this.orderStatusParams);
        //this.getFailureStatusReport(this.orderStatusParams);
    }

    addAdhocSearch() {
        if (this.adhocSearch.length > 0 && this.adhocSearch.length < 3) {
            if (this.adhocSearch[this.adhocSearch.length - 1].ColumnName && this.adhocSearch[this.adhocSearch.length - 1].Operator) {
                this.adhocSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
            }
        }
    }

    removeAdhocSearch(index: number) {
        if (this.adhocSearch.length > 1) {
            this.adhocSearch.splice(index, 1);
        }
    }

    resetSearchForm() {


        (<any>$('.resetSelect')).prop('selectedIndex', 0);
        (<any>$('.inputField')).val('');
        this.adhocSearch.forEach((data, index) => {
            data.LogicalOperator = "";
            data.ColumnName = "";
            data.ColumnValue = "";
            data.Operator = "";

        })


        for (var i = this.adhocSearch.length; i > 0; i--) {

            this.adhocSearch.splice(i, 1);
        }

    }
    search() {
        this.injectService.additionalSearch = this.adhocSearch;
        this.router.navigate(['/my-order/order-tracking']);
    }

    viewReport(reportType) {
        let additionalSearchObj: any;
        if (reportType == 'Open') {

            this.adhocSearch.push({ ColumnName: "ReportStatus", Operator: "5", ColumnValue: "OPEN" });

        }
        else if (reportType == 'Hold') {
            this.adhocSearch.push({ ColumnName: "ReportStatus", Operator: "5", ColumnValue: "HOLD" });

        }
        else if (reportType == 'Closed') {
            this.adhocSearch.push({ ColumnName: "ReportStatus", Operator: "5", ColumnValue: "LOGOUT" });

        }
        else if (reportType == 'Fail') {
            this.adhocSearch.push({ ColumnName: "ReportRating", Operator: "5", ColumnValue: "FAIL" });
            this.adhocSearch.push({ ColumnName: "ReportStatus", Operator: "2", ColumnValue: "CANCEL" });

        }

        this.injectService.additionalSearch = this.adhocSearch;
        this.router.navigate(['/my-order/order-tracking']);
    }
    changeClass() {

        (<any>$(".open_in")).toggleClass('close_out');
        (<any>$(".collapse")).toggle();
        (<any>$(".hocSearch")).toggle();
        (<any>$("#closeMode")).toggle();
        (<any>$("#collepsMode")).toggle();
    }
}
//Helper classes
export class OrderStatus {
    public OpenOrder: number;
    public ClosedOrder: number;
    public HoldOrder: number;
    public FailOrder: number;
    public TotalOrder: number;
}
export class OrderStatusParams {
    public clientId: number;
    public currentYear: number;
    public startYear: number;
    public userType: any;
    public userCompanyId: number;
}
