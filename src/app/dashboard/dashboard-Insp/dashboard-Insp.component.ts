import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';
import { DataService, BaseUrl, COLUMN_LIST } from '../../mts.service';
import { InjectService } from '../../injectable-service/injectable-service';

@Component({
    selector: 'dashboard-Insp-component',
    templateUrl: './dashboard-Insp.component.html'
})
export class DashboardInspComponent {
    orderStatus: OrderStatus;
    orderStatusParams: OrderStatusParams;
    orderPerformanceStatus: any;
    // tatAnalysisStatus: any;
    scheduledAndFinished: any;
    failureAnalysisStatus: any;
    SamplePassVsFailAndHold: any;
    MonthlyPassFailAnalysis: any;
    currentYear: number;
    adhocSearch: any;
    userDetail: any;
    userCredential: any;
    selectedYear: any;
    failureSelectedYear: number;
    // tatSelectedYear: number;
    scheduledAndFinishedSelectedYear: number;
    sampleReceivedYear: number;
    performanceSelectedYear: number;

    columnList: any;
    logoPath: any;
    constructor(private router: Router, private injectService: InjectService, private dataService: DataService) {
        this.orderStatus = new OrderStatus;
        this.orderStatusParams = new OrderStatusParams;
        this.orderPerformanceStatus = {};
        // this.tatAnalysisStatus = {};
        this.scheduledAndFinished = {};
        this.failureAnalysisStatus = {};
    }


    ngOnInit() {
        //this.columnList=COLUMN_LIST;
        // var selectedCompany = this.userDetail.CompanyName;
        //alert(selectedCompany);
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
        if (this.userDetail && this.userCredential) {
            if (this.userCredential.UserPermission.ViewDashboard) {
                if (this.userCredential.BusinessCategoryId == 2 || this.userCredential.BusinessCategoryId == 3) {
                    this.currentYear = new Date().getFullYear();
                    this.failureSelectedYear = this.currentYear;
                    // this.tatSelectedYear = this.currentYear;
                    this.scheduledAndFinishedSelectedYear = this.currentYear;
                    this.sampleReceivedYear = this.currentYear;
                    this.performanceSelectedYear = this.currentYear;
                    this.orderStatusParams.clientId = this.userCredential.BusinessCompanyId == 0 ? this.userCredential.UserCompanyId : this.userCredential.BusinessCompanyId;
                    this.orderStatusParams.currentYear = new Date().getFullYear();//this.currentYear;
                    this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
                    this.orderStatusParams.userCompanyId = this.userCredential.UserCompanyId;
                    this.orderStatusParams.userType = this.userCredential.UserType;
                    this.orderStatusParams.BusinessCategoryId = this.userCredential.BusinessCategoryId;
                    this.getOrderStatus(this.orderStatusParams);
                    this.getOrderPerformanceReport(this.orderStatusParams);
                    this.getScheduledAndFinished(this.orderStatusParams);
                    this.GetMonthlyPassFailAnalysis(this.orderStatusParams);
                    this.GetSamplePassVsFailAndHold(this.orderStatusParams);
                    this.adhocSearch = [{ LogicalOperator: "AND", ColumnName: "", Operator: "", ColumnValue: "" }];
                    this.dataService.get('Order/GetMyOrderColumnsList?selectedCompany=' + this.userCredential.CompanyId + '&FormId=1&BusinessCategoryId='+this.userCredential.BusinessCategoryId).subscribe(
                        response => {
                            if (response.IsSuccess) {
                                this.columnList = response.SelectedAdHocSearchColumn;
                            }
                            else {
                            }
                        }
                    );
                }
            } else {
                this.router.navigate(['/landing-page']);
            }
        } else {
            this.router.navigate(['/login']);
        }
        if (!this.userCredential.UserPermission.ViewDashboard) {
            this.router.navigate(['./landing-page']);
        }
    }




    GetMonthlyPassFailAnalysis(orderStatusParams) {
        this.dataService.post("Dashboard/InspGetMonthlyPassFailAnalysis", orderStatusParams).subscribe(
            response => {
                this.MonthlyPassFailAnalysis = response;
                this.createMonthlyPassFailAnalysisChart();
            }
        )
    }
    GetSamplePassVsFailAndHold(orderStatusParams) {
        this.dataService.post("Dashboard/InspGetSamplePassVsFailAndHold", orderStatusParams).subscribe(
            response => {
                this.SamplePassVsFailAndHold = response;
                this.createSamplePassVsFailAndHold();
            }
        )
    }

    getOrderPerformanceReport(orderStatusParams) {
        this.dataService.post("Dashboard/InspFetchPerformanceReport", this.orderStatusParams).subscribe(
            response => {

                this.orderPerformanceStatus = response;
                this.createPerformanceChart();
            }
        )
    }

    getOrderStatus(orderStatusParams) {
        this.dataService.post("Dashboard/InspOrdersStatus", orderStatusParams).subscribe(
            response => {
                this.orderStatus = response;

                this.createOrderStatuschart();
            }
        );
    }

    // getTatAnalysisReport(orderStatusParams) {
    //     this.dataService.post("Dashboard/InspTATAnalysisStatus", orderStatusParams).subscribe(
    //         response => {
    //             this.tatAnalysisStatus = response;
    //             this.createTATAnalysisChart();
    //         }
    //     )
    // }

    getScheduledAndFinished(orderStatusParams) {
        this.dataService.post("Dashboard/InspGetScheduledAndFinished", orderStatusParams).subscribe(
            response => {
                this.scheduledAndFinished = response;
                this.createScheduledAndFinished();
            }
        )
    }

    createScheduledAndFinished() {
        var chart = AmCharts.makeChart("ScheduledAndFinished", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "useGraphSettings": true,
                "position": "top"
            },

            "categoryField": "LoginMonth",
            "rotate": false,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "Scheduled:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0.2,
                    "title": "Scheduled",
                    "type": "column",
                    "valueField": "IN",
                    "fillColors": "#FFA500"
                }
                , {
                    "balloonText": "Finished:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-3",
                    "lineAlpha": 0.2,
                    "title": "Finished",
                    "type": "column",
                    "valueField": "Out",
                    "fillColors": "#84b761"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "bottom",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": this.scheduledAndFinished.ScheduledAndFinished.ScheduledAndFinished

        });
    }

    createSamplePassVsFailAndHold() {
        var chart = AmCharts.makeChart("SamplePassVsFailAndHold", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "useGraphSettings": true,
                "position": "top"
            },

            "categoryField": "LoginMonth",
            "rotate": false,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "Pass:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "Pass",
                    "type": "column",
                    "valueField": "IN",
                    "fillColors": "#84b761"
                },
                {
                    "balloonText": "Failed:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0.2,
                    "title": "Failed",
                    "type": "column",
                    "valueField": "Out",
                    "fillColors": "#cc4748"
                }
                , {
                    "balloonText": "Hold:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-3",
                    "lineAlpha": 0.2,
                    "title": "Hold",
                    "type": "column",
                    "valueField": "Hold",
                    "fillColors": "#337ab7"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "bottom",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": this.SamplePassVsFailAndHold.SamplePassVsFailAndHold.InspSamplePassVsFailAndHold

        });
    }
    createMonthlyPassFailAnalysisChart() {
        var chart = AmCharts.makeChart("MonthlyPassFailAnalysis", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "useGraphSettings": true,
                "position": "top"
            },
            "dataProvider": this.MonthlyPassFailAnalysis.MonthlyPassFailAnalysis.MonthlyPassFailAnalysis,
            "valueAxes": [{
                "integersOnly": true,
                "reversed": false,
                "axisAlpha": 0,
                "dashLength": 5,
                "gridCount": 10,
                "position": "left",
                "title": ""
            }],
            "startDuration": 0.5,
            "graphs": [{
                "balloonText": "Fail [[category]]: [[value]]",
                "bullet": "round",
                "title": "Fail",
                "valueField": "Fail",
                "fillAlphas": 0,
                "lineColor": "#ac2925"
            }, {
                "balloonText": "Pass [[category]]: [[value]]",
                "bullet": "round",
                "title": "Pass",
                "valueField": "Pass",
                "fillAlphas": 0,
                "lineColor": "#84b761"
            }],
            "chartCursor": {
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "LoginMonth",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "fillAlpha": 0,
                "fillColor": "",
                "gridAlpha": 0,
                "position": "bottom"
            },
            "export": {
                "enabled": false,
                "position": "bottom-right"
            }
        });

    }

    createFailureAnalysisChart() {
        var chart = AmCharts.makeChart("failureAnalysisChart", {
            "theme": "light",
            "type": "serial",
            "legend": {
                "horizontalGap": 5,
                "maxColumns": 5,
                "position": "top",
                "useGraphSettings": true,
                "markerSize": 10,
                "marginTop": 20
            },
            "columnWidth": 0.9,
            "dataProvider": this.failureAnalysisStatus.FailureAnalysisList.failureAnalysisReport,
            "valueAxes": [{
                "stackType": "3d",
                "position": "left",
                "title": "Failure Analysis",
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "Critical Failure in [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.1,
                "title": "Critical",
                "type": "column",
                "valueField": "CriticalFailure",
                "fillColors": "#ac2925",
                fixedColumnWidth: 75
            }, {
                "balloonText": "Major Failure in [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.1,
                "title": "Major",
                "type": "column",
                "valueField": "MajorFailure",
                "fillColors": "#FF8C00",
                fixedColumnWidth: 75
            },
            {
                "balloonText": "Minor Failure in [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.1,
                "title": "Minor",
                "type": "column",
                "valueField": "MinorFailure",
                "fillColors": "#fdd400",
                fixedColumnWidth: 75
            }
            ],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 60,
            "angle": 30,
            "categoryField": "LoginMonth",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "export": {
                "enabled": false
            }
        });

    }

    createOrderStatuschart() {
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "funnel",
            "theme": "light",
            "dataProvider":
                [{
                    "title": "Open Orders",
                    "value": this.orderStatus.OpenOrder
                }, {
                    "title": "Abortive Orders",
                    "value": this.orderStatus.AbortiveOrder
                }, {
                    "title": "Closed Orders",
                    "value": this.orderStatus.ClosedOrder
                }, {
                    "title": "Re-Inspection Orders",
                    "value": this.orderStatus.ReInspectionOrder
                }],
            "titleField": "title",
            "marginRight": 180,
            "marginLeft": 15,
            "labelPosition": "right",
            "funnelAlpha": 0.9,
            "valueField": "value",
            "startX": 0,
            "neckWidth": "160",
            "startAlpha": 0,
            "outlineThickness": 1,
            "neckHeight": "0",
            "balloonText": "[[title]]:<b>[[value]]</b>",
        });
    }

    createPerformanceChart() {

        var chart = AmCharts.makeChart("performanceChart", {
            "theme": "light",
            "type": "serial",
            "legend": {
                "horizontalGap": 5,
                "maxColumns": 5,
                "position": "top",
                "useGraphSettings": true,
                "markerSize": 10,
                "marginTop": 20
            },
            "dataProvider": this.orderPerformanceStatus.PerformanceReportList.PerformanceReport,
            "valueAxes": [{
                "stackType": "3d",
                "position": "left",
                "title": "Performance",
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "Total Fail Order in [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "Fail",
                "type": "column",
                "valueField": "FailOrder",
                "fillColors": "#ac2925",
                fixedColumnWidth: 50,

            }, {
                "balloonText": "Total Pass Order in [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "Pass",
                "type": "column",
                "fillColors": "#84b761",
                "valueField": "PassOrder",
                fixedColumnWidth: 50,
            }],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 35,
            "angle": 40,
            "categoryField": "LoginYear",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "export": {
                "enabled": false
            }
        });
    }

    // createTATAnalysisChart() {

    //     var dataProvider = this.tatAnalysisStatus.TATAnalysisList.tatAnalysisReport.map((data) => {
    //         if (data.Duration == "1-2 Days") {
    //             data.color = "#0067aa";
    //         }
    //         else if (data.Duration == "3-7 Days") {
    //             data.color = "#FFA500";
    //         }
    //         else if (data.Duration == "More Than 7 Days") {
    //             data.color = "#67b7dc";
    //         }
    //         return data;
    //     })

    //     var chart = AmCharts.makeChart("analysisChart", {
    //         "type": "pie",
    //         "theme": "light",
    //         "legend": {
    //             "markerSize": 10,
    //             "position": "top",
    //             "autoMargins": false,
    //             "maxColumns": 5
    //         },
    //         /* "titles": [{
    //              "text": "TAT Analysis",
    //              "size": 18
    //          }],*/
    //         "dataProvider": dataProvider,
    //         "valueField": "Count",
    //         "titleField": "Duration",
    //         "colorField": "color",
    //         "startEffect": "elastic",
    //         "startDuration": 2,
    //         "labelRadius": 15,
    //         "innerRadius": "50%",
    //         "depth3D": 10,
    //         "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    //         "angle": 15,
    //         "export": {
    //             "enabled": false
    //         }
    //     });
    // }

    changeMonthlyPassFailAnalysis(currentYear) {
        this.orderStatusParams.currentYear = currentYear;
        this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
        this.GetMonthlyPassFailAnalysis(this.orderStatusParams);
    }
    changePassFailHoldStatusChart(currentYear) {

        this.orderStatusParams.currentYear = currentYear;
        this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
        this.GetSamplePassVsFailAndHold(this.orderStatusParams);
    }
    changePerformanceStatusChart(currentYear) {
        this.orderStatusParams.currentYear = currentYear;
        this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
        this.getOrderPerformanceReport(this.orderStatusParams);
    }

    changeScheduledAndFinishedChart(currentYear) {
        this.orderStatusParams.currentYear = currentYear;
        this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
        this.getScheduledAndFinished(this.orderStatusParams);
    }

    changeAllCharts(currentYear) {
        this.failureSelectedYear = currentYear;
        // this.tatSelectedYear = currentYear;
        this.scheduledAndFinishedSelectedYear = currentYear;
        this.sampleReceivedYear = currentYear;
        this.performanceSelectedYear = currentYear;
        this.orderStatusParams.currentYear = currentYear;
        this.orderStatusParams.startYear = this.orderStatusParams.currentYear - 4;
        this.getOrderPerformanceReport(this.orderPerformanceStatus);
        this.getScheduledAndFinished(this.orderStatusParams);
        this.GetMonthlyPassFailAnalysis(this.orderStatusParams);
        this.GetSamplePassVsFailAndHold(this.orderStatusParams);
        //this.getFailureStatusReport(this.orderStatusParams);
    }

    addAdhocSearch() {
        if (this.adhocSearch.length > 0 && this.adhocSearch.length < 3) {
            if (this.adhocSearch[this.adhocSearch.length - 1].ColumnName && this.adhocSearch[this.adhocSearch.length - 1].Operator) {
                this.adhocSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
            }
        }
    }

    removeAdhocSearch(index: number) {
        if (this.adhocSearch.length > 1) {
            this.adhocSearch.splice(index, 1);
        }
    }

    resetSearchForm() {


        (<any>$('.resetSelect')).prop('selectedIndex', 0);
        (<any>$('.inputField')).val('');
        this.adhocSearch.forEach((data, index) => {
            data.LogicalOperator = "";
            data.ColumnName = "";
            data.ColumnValue = "";
            data.Operator = "";

        })


        for (var i = this.adhocSearch.length; i > 0; i--) {

            this.adhocSearch.splice(i, 1);
        }

    }
    search() {
        this.injectService.additionalSearch = this.adhocSearch;
        this.router.navigate(['/my-order/inspection-order-tracking']);
    }

    viewReport(reportType) {
        let additionalSearchObj: any;
        if (reportType == 'Open') {

            this.adhocSearch.push({ ColumnName: "status", Operator: "1", ColumnValue: "scheduled" });

        }
        else if (reportType == 'Abortive') {
            this.adhocSearch.push({ ColumnName: "status", Operator: "1", ColumnValue: "abortive" });

        }
        else if (reportType == 'Closed') {
            this.adhocSearch.push({ ColumnName: "status", Operator: "1", ColumnValue: "closed" });
            // this.adhocSearch.push({ ColumnName: "status", Operator: "5", ColumnValue: "Report Issued" });

        }
        else if (reportType == 'Reinspection') {
            this.adhocSearch.push({ ColumnName: "service", Operator: "1", ColumnValue: "re-inspection" });
            // this.adhocSearch.push({ ColumnName: "ReportStatus", Operator: "2", ColumnValue: "CANCEL" });

        }

        this.injectService.additionalSearch = this.adhocSearch;
        this.router.navigate(['/my-order/inspection-order-tracking']);
    }
    changeClass() {

        (<any>$(".open_in")).toggleClass('close_out');
        (<any>$(".collapse")).toggle();
        (<any>$(".hocSearch")).toggle();
        (<any>$("#closeMode")).toggle();
        (<any>$("#collepsMode")).toggle();
    }




}
export class OrderStatus {
    public OpenOrder: number;
    public ClosedOrder: number;
    public AbortiveOrder: number;
    public ReInspectionOrder: number;
    public TotalOrder: number;
}
export class OrderStatusParams {
    public clientId: number;
    public currentYear: number;
    public startYear: number;
    public userType: any;
    public userCompanyId: number;
    public BusinessCategoryId: number;
}
