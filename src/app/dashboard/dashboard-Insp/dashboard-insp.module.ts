import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardInspRoutingModule } from './dashboard-insp-routing.module';
import { DashboardInspComponent } from 'app/dashboard/dashboard-Insp/dashboard-Insp.component';
import { HeaderModule } from 'app/header/header.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HeaderModule,
    MaterialModule,
    DashboardInspRoutingModule,
  ],
  declarations: [DashboardInspComponent],
  exports:[DashboardInspComponent]
})
export class DashboardInspModule { }
