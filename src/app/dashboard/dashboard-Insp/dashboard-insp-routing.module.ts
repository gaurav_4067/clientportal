import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardInspComponent } from 'app/dashboard/dashboard-Insp/dashboard-Insp.component';

const routes: Routes = [{path:'', component:DashboardInspComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardInspRoutingModule { }
