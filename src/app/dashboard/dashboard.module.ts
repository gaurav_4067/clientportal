import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { FooterComponent } from 'app/footer';
import { FooterModule } from 'app/footer/footer.module';
import { HeaderComponent } from 'app/header';
import { FormsModule } from '@angular/forms';
import { MdCoreModule } from '@angular2-material/core';
import { Md2Module } from 'md2';
import { MaterialModule } from '@angular/material';
import { HeaderModule } from 'app/header/header.module';
import { DashboardInspModule } from 'app/dashboard/dashboard-Insp/dashboard-insp.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    Md2Module.forRoot(),
    MdCoreModule,
    MaterialModule,
    DashboardRoutingModule,
    FooterModule,
    HeaderModule,
    DashboardInspModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
