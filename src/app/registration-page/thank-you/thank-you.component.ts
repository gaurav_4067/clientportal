import { Component, OnInit } from '@angular/core';
import { DataService, BaseUrl, ETRFUrl } from 'app/mts.service';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
declare let swal: any;
@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.css']
})
export class ThankYouComponent implements OnInit {
  public userCredential: any;
  public user: any = { userName: '', password: '' }
  statusEtrf: any;
  constructor(private dataService: DataService, public router: Router, private http: Http) {
  }
  ngOnInit() {

    this.userCredential = { UserId: 0, CompanyId: 0, UserCategoryId: 0, RequestedServiceType: 0 };
    
    var value = JSON.parse(localStorage.getItem("user_login_cred"));
    this.userCredential.RequestedServiceType = value.RequestedServiceType;
    var that = this;
    let UserCompanyIdCondition = 0;
    var status = '';

    if (this.userCredential.RequestedServiceType != 2) {
      this.http.post(BaseUrl + 'user/Authentication', value).subscribe(
        response => {
          let res = response.json();
          UserCompanyIdCondition = res.UserCategoryId;
          // this.isLoader = false;
          that.statusEtrf = res.Status;
          if (res.IsSuccess) {
            localStorage.setItem('userDetail', JSON.stringify(res));
            status = res.Status;


            if (res.IsClient) {
              this.userCredential.UserId = res.UserId;
              this.userCredential.CompanyId = res.CompanyId;
              this.userCredential.UserType = 'Client';
              this.userCredential.UserCompanyId = res.CompanyId;
              this.userCredential.CompanyName = res.CompanyName;
              this.userCredential.BusinessCategoryId = 1;
              this.userCredential.CompanyLogo = ETRFUrl + res.CompanyLogo;
            }
            if (res.AssignedCompanyANDCategory.AssignedCompanyList.length == 1) {
              this.userCredential.UserId = res.UserId;
              this.userCredential.CompanyId = res.AssignedCompanyANDCategory.AssignedCompanyList[0].CompanyId;
              this.userCredential.UserType = res.AssignedCompanyANDCategory.AssignedCompanyList[0].WorkingAs;
              this.userCredential.CompanyName = res.AssignedCompanyANDCategory.AssignedCompanyList[0].CompanyName;
              this.userCredential.UserCompanyId = res.CompanyId;
              this.userCredential.BusinessCategoryId = 1;
              this.userCredential.CompanyLogo = ETRFUrl + res.AssignedCompanyANDCategory.AssignedCompanyList[0].CompanyLogo;
            }
            else if (res.Status == 'Pending' && res.CompanyId == 10000) {
              //Ecommerece
              this.userCredential.UserId = res.UserId;
              this.userCredential.CompanyId = res.CompanyId;
              this.userCredential.BusinessCategoryId = 1;
              this.userCredential.CompanyId = res.CompanyId;
              this.userCredential.UserCategoryId = res.UserCategoryId;
              this.userCredential.UserType = res.UserCategoryName;
              this.userCredential.UserCompanyId = res.CompanyId;
              this.userCredential.CompanyName = res.CompanyName;
              this.userCredential.CompanyLogo = ETRFUrl + res.CompanyLogo;
            }
            // if (status=='Pending') {
            //             this.router.navigate(['./orderPackages']);
            //         }
            this.dataService.get('user/GetUserPermissions?UserId=' + res.UserId + '&CompanyId=' + res.CompanyId + '&BusinessCategoryId=1').subscribe(
              response => {
                let res = response;
                this.userCredential.UserPermission = res.UserPermission;
                if (that.statusEtrf == 'Pending' && this.userCredential.CompanyId == 10000) {
                  res.UserPermission.ViewETRF = true;
                  res.UserPermission.ViewECommerce = true;
                }
                localStorage.setItem('mts_user_cred', JSON.stringify(this.userCredential));

                // if (status == 'Pending') {
                //     this.router.navigate(['./orderPackages']);
                // }

                if (status == 'Pending' && this.userCredential.CompanyId == 10000) {
                  setTimeout(() => {
                    this.router.navigate(['./e-commerce']);
                  }, 7000);
                } else {
                  // setTimeout(() => {
                  //     this.router.navigate(['./home']);
                  // },10000);
                }
              }
            );

          }
          else {

            swal('', res.Message);
            this.user.userName = null;
            this.user.password = null;
          }
        }
      )
      localStorage.removeItem('user_login_cred');
    }
  }
}
