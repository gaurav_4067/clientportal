import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistrationPageRoutingModule } from './registration-page-routing.module';
import { RegistrationPageComponent } from './registration-page.component';
import { FormsModule } from '@angular/forms';
import { MdInputModule } from '@angular2-material/input';
import { MdInputContainer, MaterialModule } from '@angular/material';
import { Md2Module } from 'md2';
import { MdCoreModule } from '@angular2-material/core';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MdInputModule,
    Md2Module.forRoot(),
    MaterialModule,
    MdCoreModule,
    Ng2AutoCompleteModule,
    RegistrationPageRoutingModule
  ],
  declarations: [RegistrationPageComponent]
})
export class RegistrationPageModule { }
