import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/mts.service';
import { ActivatedRoute, Router } from '@angular/router';
import { signUpModel } from 'app/registration-page/registration-model';

declare let swal: any;
@Component({
    selector: 'app-registration-page',
    templateUrl: './registration-page.component.html',
    styleUrls: ['./registration-page.component.css']
})
export class RegistrationPageComponent implements OnInit {
    companyList: any[];
    model: any;
    isLoader: boolean;
    clientWorking: any = "";
    en: any;
    confirmPassword: any = "";
    countryList: any = [];
    stateList: any = [];
    cityList: any = [];
    validationObj: any = {};
    emailFieldMsg: any = "";
    telephoneFieldMsg: any = "";
    telephoneFieldMsg2: any = "";
    passwordMsg: any = "";
    confirmPassMsg: any = "";
    captchaText: any = "";
    captchaVal: any = "";
    captchaMsg: any = "";
    zipCode: string = "";
    /* --for Auto-complete feild */
    ctrList: any;
    sList: any;
    cList: any;
    vendorIdMsg: string = "";
    applicantTypeList: any = [];
    /*--end*/
    constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) {

    }

    ngOnInit() {
        this.model = Object.assign({}, signUpModel);
        this.model.RequestedServiceType = 1;
        this.en = {
            firstDayOfWeek: 0,
            dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        };
        this.getCompanyList();
        var that = this;
        this.dataService.signUpGet('Master/GetCountryList').subscribe(
            r => {
                that.countryList = r.CountryList;
            }
        )

        this.dataService.signUpGet('Master/GetApplicantList').subscribe(
            r => {
                that.applicantTypeList = r.Data;
            }
        )

        this.validationObj = {
            "Email": true,
            "Password": true,
            "ConfirmPassword": true,
            "FirstName": true,
            "LastName": true,
            "CompanyName": true,
            "Address1": true,
            "telephone1": true,
            "mobile1": true,
            "telephone2": true,
            "mobile2": true,
            "Country": true,
            "State": true,
            "City": true,
            "UserCategory": true,
            "ClientWorkingFor": true,
            "captcha": true,
            "VendorId": true,
            "ApplicantId": true
        }

        this.GenerateCaptcha();
    }


    public renderCountry(data: any): string {
        const html = `
          <div class="data-row">
            <div class="col-2">${data.CountryName}</div>
          </div>`;

        return html;
    }

    public renderState(data: any): string {
        const html = `
          <div class="data-row">
            <div class="col-2">${data.StateName}</div>
          </div>`;

        return html;
    }

    public renderCity(data: any): string {
        const html = `
          <div class="data-row">
            <div class="col-2">${data.CityName}</div>
          </div>`;

        return html;
    }


    checkValidation(required: any, format: any, value: any, objField: any) {
        if (required) {
            if (!value) {
                this.validationObj[objField] = false;
                if (format == 'email') {
                    this.emailFieldMsg = 'Required';
                }
            }
            else {
                this.validationObj[objField] = true;
            }
        }

        if (format == 'email' && value != '') {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(value) == false) {
                this.validationObj[objField] = false;
                this.emailFieldMsg = 'Invalid Format';
            }
            else {
                this.validationObj[objField] = true;
            }
        }

        if (format == 'password') {
            if (value.length == 0) {
                this.validationObj[objField] = false;
                this.passwordMsg = "Required";
            }

            else if (value.length < 6 || value.length > 20) {
                this.validationObj[objField] = false;
                this.passwordMsg = "Password should be between 6 to 20 characters";
            }
            else {
                this.validationObj[objField] = true;
            }
        }

        if (format == 'confirmPassword') {
            if (value != this.model.Password) {
                this.validationObj[objField] = false;
                this.confirmPassMsg = "Password do not match";
            }
            else {
                this.validationObj[objField] = true;
            }
        }

        if (format == 'telephone1') {
            var regex = /^[0-9-+]*$/;
            if (value.length == 0) {
                this.validationObj[objField] = false;
                this.telephoneFieldMsg = "Required";
            }
            else if (regex.test(value) == false) {
                this.validationObj[objField] = false;
                this.telephoneFieldMsg = 'Invalid Format';
            }
            else {
                this.validationObj[objField] = true;
            }
        }

        if (format == 'telephone2') {
            var regex = /^[0-9-+]*$/;
            if (value.length == 0) {
                this.validationObj[objField] = false;
                this.telephoneFieldMsg2 = "Required";
            }
            else if (regex.test(value) == false) {
                this.validationObj[objField] = false;
                this.telephoneFieldMsg2 = 'Invalid Format';
            }
            else {
                this.validationObj[objField] = true;
            }
        }
        if (format == 'ClientWorkingFor') {
            if (!value) {
                this.validationObj[objField] = false;
            } else {
                this.validationObj[objField] = true;
            }
        }
        if (format == 'zipCode') {
            if (value && value.length > 0) {
                var regex = /^[0-9]*$/;
                if (regex.test(value) == false) {
                    this.validationObj[objField] = false;
                    this.zipCode = 'Invalid Format';
                } else {
                    this.validationObj[objField] = true;
                }
            }
            else {
                this.validationObj[objField] = true;
            }
        }
        if (format === 'VendorId') {
            if (this.model.ClientsWorkingFor == 386 && value == '') {
                this.vendorIdMsg = "Required";
                this.validationObj[objField] = false;
            } else {
                this.validationObj[objField] = true;
            }
        }

        if (format === 'ApplicantId') {
            if (this.model.ClientsWorkingFor == 386 && value == 0) {
                this.vendorIdMsg = "Required";
                this.validationObj[objField] = false;
            } else {
                this.validationObj[objField] = true;
            }
        }

        if (format === 'RequestedServiceType') {
            if (!this.model.RequestedServiceType) {
                this.vendorIdMsg = "Required";
                this.validationObj[objField] = false;
            } else {
                this.validationObj[objField] = true;
            }
        }


    }

    GenerateCaptcha() {
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var text = '';
        for (var i = 0; i < 3; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        var chr1 = Math.ceil(Math.random() * 10) + '';
        var chr2 = Math.ceil(Math.random() * 10) + '';
        var chr3 = Math.ceil(Math.random() * 10) + '';
        this.captchaText = chr1 + text + chr2 + chr3;
        // this.getCaptchaImage();
    }

    checkCaptcha() {
        if (this.captchaVal != this.captchaText && this.captchaVal != '') {
            this.captchaMsg = "Invalid Captcha";
            this.captchaVal = "";
            this.GenerateCaptcha();
            this.validationObj.captcha = false;
        }
        else {
            this.validationObj.captcha = true;
        }
    }

    click() {

        var ready = true;

        if (this.model.Email == "") {
            this.validationObj.Email = false;
            this.emailFieldMsg = 'Required';
        }

        if (this.model.Password == "") {
            this.validationObj.Password = false;
            this.passwordMsg = "Required";
        }

        if (this.confirmPassword == "") {
            this.validationObj.ConfirmPassword = false;
            this.confirmPassMsg = "Required";
        }

        // if (this.model.Password != this.confirmPassword) {
        //     this.validationObj.ConfirmPassword = false;
        //     this.confirmPassMsg = "Password do not match";
        // }

        // if (this.model.Password && this.confirmPassword == "") {
        //     this.validationObj.ConfirmPassword = false;
        //     this.confirmPassMsg = "Password do not match";
        // }

        if (this.validationObj.Email == false || this.validationObj.Password == false || this.validationObj.ConfirmPassword == false) {
            ready = false;
        }

        if (ready == true) {
            var that = this;
            this.dataService.signUpGet('user/ChkExistance?emailId=' + that.model.Email).subscribe(
                r => {

                    if (r.IsSuccess) {
                        (<any>$('#AccountDetails')).addClass('fadeOutLeft');
                        (<any>$('#AccountDetails')).addClass('SlideOut1');
                        (<any>$('#personalNumber')).addClass('active');
                        (<any>$('#PersonalDetails')).removeClass('SlideOut');
                    }
                    else {
                        swal('', r.Message);
                    }
                }
            )
        }
        else {
            // swal('','Please fill all required fields correctly.');
        }
    }

    click2() {
        var ready = true;

        if (this.model.FirstName == "") {
            this.validationObj.FirstName = false;
        }

        if (this.model.TelephoneNo == "") {
            this.validationObj.telephone1 = false;
        }

        // if (this.model.MobileNo == "") {
        //     this.validationObj.mobile1 = false;
        // }


        if (this.model.LastName == "") {
            this.validationObj.LastName = false;
        }

        if (this.model.TelephoneNo == "") {
            this.validationObj.telephone1 = false;
            this.telephoneFieldMsg = "Required";
        }

        if (this.validationObj.FirstName == false || this.validationObj.LastName == false || this.validationObj.telephone1 == false) {
            ready = false;
        }

        if (ready == true) {
            (<any>$('#AccountDetails')).addClass('fadeOutLeft');
            (<any>$('#PersonalDetails')).addClass('fadeOutLeft');
            (<any>$('#companyNumber')).addClass('active');
            (<any>$('#CompanyDetails')).removeClass('SlideOut');
        }
        else {
            // swal('','Please fill all required fields correctly.');
        }
    }

    back() {
        (<any>$('#AccountDetails')).removeClass('fadeOutLeft');
        (<any>$('#PersonalDetails')).removeClass('fadeOutLeft');
        (<any>$('#personalNumber')).removeClass('active');
        (<any>$('#AccountDetails')).addClass('fadeInLeft');
        (<any>$('#CompanyDetails')).addClass('SlideOut');

    }

    back2() {
        (<any>$('#CompanyDetails')).removeClass('fadeOutLeft');
        (<any>$('#PersonalDetails')).removeClass('fadeOutLeft');
        (<any>$('#companyNumber')).removeClass('active');
        (<any>$('#PersonalDetails')).addClass('fadeInLeft');
        (<any>$('#PersonalDetails')).addClass('SlideOut');
    }

    // addWorkingClient() {
    //     if (this.clientWorking) {
    //         this.workingClients.push(this.clientWorking);
    //         this.validationObj.ClientWorkingFor = true;
    //     }
    //     else {

    //     }
    //     this.clientWorking = "";
    //     this.model.ClientsWorkingFor = this.workingClients.join(',');
    // }

    // removeWorkingClient(index: any) {

    //     this.workingClients.splice(index, 1);
    //     this.model.ClientsWorkingFor = this.workingClients.join(',');
    // }

    // confirmPass()
    // {
    //     if(this.confirmPassword != this.model.Password)
    //     {
    //         swal('', 'Password not matched');
    //         this.confirmPassword = "";
    //     }
    // }

    onCountryChange(CList) {
        if (CList.CountryId) {
            this.isLoader = true;
            this.model.CountryId = CList.CountryId
            var id = this.model.CountryId;
            this.model.StateId = null;
            this.model.CityId = null;
            this.sList = "";
            this.cList = "";
            this.validationObj.Country = true;
            var that = this;
            this.dataService.signUpGet('Master/GetStateList?CountryId=' + id).subscribe(
                r => {
                    that.isLoader = false;
                    that.stateList = r.StateList;
                }
            )
        } else {
            this.sList = "";
            this.cList = "";
            this.validationObj.Country = false;
            this.model.StateId = null;
            this.model.CityId = null;
        }
    }

    onStateChange(sList) {
        if (sList.StateId) {
            this.isLoader = true;
            this.model.StateId = sList.StateId
            var id = this.model.StateId;
            this.cList = "";
            this.validationObj.State = true;
            var that = this;
            this.dataService.signUpGet('Master/GetCityList?StateId=' + id).subscribe(
                r => {
                    that.isLoader = false;
                    that.cityList = r.CityList;
                    this.model.CityId = null;
                }
            )
        } else {
            this.cList = "";
            this.validationObj.State = false;
        }
    }

    onCityChange(cityList) {
        if (cityList.CityId) {
            this.model.CityId = cityList.CityId
            this.validationObj.City = true;
        } else {
            this.validationObj.City = false;
            this.model.CityId = null;
        }
    }

    userCatChange() {
        if (this.model.UserCategory) {
            this.validationObj.UserCategory = true;
        }
    }

    signUp() {
        var ready = true;
        if (this.model.CompanyName == "") {
            this.validationObj.CompanyName = false;
        }
        if (this.model.Address1 == "") {
            this.validationObj.Address1 = false;
        }
        if (!this.model.CountryId) {
            this.validationObj.Country = false;
        }
        if (!this.model.StateId) {
            this.validationObj.State = false;
        }
        if (!this.model.CityId) {
            this.validationObj.City = false;
        }
        if (this.model.CompanyTelephoneNo == "") {
            this.validationObj.telephone2 = false;
            this.telephoneFieldMsg2 = "Required";
        }

        if (!this.model.ClientsWorkingFor) {
            this.validationObj.ClientWorkingFor = false;
        }

        if (this.captchaVal == "" && this.validationObj.captcha) {
            this.captchaMsg = "Required";
            this.validationObj.captcha = false;
        }

        if (this.model.ClientsWorkingFor == 386 && this.model.VendorId == '') {
            this.vendorIdMsg = "Required";
            this.validationObj.VendorId = false;
        } else {
            this.validationObj.VendorId = true;
        }

        if (this.model.ClientsWorkingFor == 386 && this.model.ApplicantId == 0) {
            this.validationObj.ApplicantId = false;
        } else {
            this.validationObj.ApplicantId = true;
        }

        if (!this.model.RequestedServiceType) {
            this.validationObj.RequestedServiceType = false;
        } else {
            this.validationObj.RequestedServiceType = true;
        }

        for (let i in this.validationObj) {
            if (this.validationObj[i] == false) {
                ready = false;
                // swal('', 'Please fill required fields correctly');
                return;
            }
        }
        if (ready == true) {
            var that = this;
            this.isLoader = true;
            var password = that.model.Password
            var UserName = that.model.Email;
            that.model.VendorId = (that.model.VendorId == '') ? 0 : that.model.VendorId;

            this.dataService.signUpPost('User/SignUp', that.model).subscribe(
                r => {

                    if (r.IsSuccess) {
                        that.isLoader = false;
                        // swal('', r.Message);
                        let requestedServiceType = that.model.RequestedServiceType;
                        this.model = Object.assign({}, signUpModel);
                        var userCredential = { Email: UserName, password: password, RequestedServiceType: requestedServiceType }
                        debugger
                        localStorage.setItem('user_login_cred', JSON.stringify(userCredential))
                        that.router.navigate(['/thank-you']);
                    }
                    else {
                        that.isLoader = false;
                        swal('', r.Message);
                    }
                }
            )
        }
    }

    getCompanyList() {
        this.isLoader = true;
        this.model.ClientsWorkingFor = "";
        this.dataService.signUpGet('User/GetFilteredSingupCompany?businessCategoryId=' + this.model.RequestedServiceType).subscribe(
            r => {
                this.isLoader = false;
                this.companyList = r.Data;
            }
        )
    }
}