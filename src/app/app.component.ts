/*
 * Angular 2 decorators and services
 */
import { Component, ViewEncapsulation ,OnInit} from '@angular/core';
import { AppState } from './app.service';
import { InjectService } from './injectable-service/injectable-service';
import { TranslateService } from '../translate';
//import { CoolLoadingIndicator } from 'angular2-cool-loading-indicator';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
  ],
  template: `
    <router-outlet></router-outlet>
    <span defaultOverlayTarget></span>
  `,
    providers: [InjectService]
})
export class AppComponent implements OnInit{
  name = 'MTS Client Solution';
    public translatedText: string;
    public supportedLangs: any[];
    constructor(public appState: AppState,private _translate: TranslateService) {

    }
    ngOnInit() {
        // standing data
        this.supportedLangs = [
            { display: 'English', value: 'en' },
            { display: 'Español', value: 'es' },
            { display: '华语', value: 'zh' },
        ];

        this.selectLang('es');

    }
    isCurrentLang(lang: string) {
        return lang === this._translate.currentLang;
    }

    selectLang(lang: string) {
        // set default;
        this._translate.use(lang);
    }

}

