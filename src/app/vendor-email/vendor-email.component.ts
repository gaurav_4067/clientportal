import { Component, Input, OnInit } from '@angular/core';
import { IMultiSelectOption, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { DataService, COLUMN_LIST, Laboratyemailid, BaseUrl, ETRFUrl } from '../mts.service';
import { InjectService } from '../injectable-service/injectable-service';

import { Router } from '@angular/router'

@Component({
  selector: 'app-vendor-email',
  templateUrl: './vendor-email.component.html'
})

export class VendorEmailComponent implements OnInit {
  private vendorList: IMultiSelectOption[];
  private selectVendorText: IMultiSelectTexts = {
    defaultTitle: 'Select vendor Email'
  };

  vendorListArr: any = [];
  vendorEamilIds: any;
  testFlag2: boolean = false;

  @Input() clientId;
  @Input() vendorId;
  @Input() vendorEmailId;
  @Input() testFlag1;

  constructor(private dataService: DataService, private injectService: InjectService, private router: Router) { }

  ngOnInit() {
    var tempArr = [];
    this.dataService.get('Master/GetVendorContactsList?ClientId=' + this.clientId + '&VendorId=' + this.vendorId).subscribe(
      response => {
        if (response) {
          this.testFlag2 = true;
          for (var i = 0; i < response.CountryList.length; i++) {
            if (!this.vendorEmailId.VendorEmailId.includes(response.CountryList[i].ContactEmailId)) {
              var vendorObj = {
                id: (response.CountryList[i].ContactEmailId).trim(),
                name: (response.CountryList[i].ContactEmailId).trim(),
              }
              tempArr.push(vendorObj);
            }

          }

        }
        else {

        }
        this.vendorList = tempArr;
      }
    );
  }

  closeVendor() {


    this.testFlag1.flag = false;
    this.vendorList = [];
    this.vendorListArr.length = 0;
    (<any>$("#vendorEmail")).modal('hide');
  }
  submitVendorEmails() {

    this.vendorEamilIds = this.vendorListArr.join(',');
    if (this.vendorEamilIds != "") {
      this.vendorEmailId.VendorEmailId = this.vendorEmailId.VendorEmailId + ',' + this.vendorEamilIds;
    }

    this.vendorList = [];
    this.vendorListArr.length = 0;
    this.testFlag1.flag = false;
    (<any>$("#vendorEmail")).modal('hide');
  }

}