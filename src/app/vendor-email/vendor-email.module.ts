import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorEmailComponent } from './vendor-email.component';
import { MdInputModule } from '@angular2-material/input';
import { FormsModule } from '@angular/forms';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

@NgModule({
  imports: [
    CommonModule,
    MultiselectDropdownModule,
    MdInputModule,
    FormsModule
  ],
  declarations: [VendorEmailComponent],
  exports: [VendorEmailComponent]
})
export class VendorEmailModule { }
