import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorEmailComponent } from './vendor-email.component';

describe('VendorEmailComponent', () => {
  let component: VendorEmailComponent;
  let fixture: ComponentFixture<VendorEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
