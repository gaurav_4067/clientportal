import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatBusinessCategoryPipe } from 'app/pipe/format-business-category.pipe';
import { FormatRatingPipe } from 'app/pipe/format-rating.pipe';
import { ConvertToArrayPipe } from 'app/pipe/convert-to-array.pipe';
import { GreenProductPipe } from 'app/pipe/green-product.pipe';
import { GpdocumentFilterArrayPipe } from 'app/pipe/gpdocument-filter-array.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FormatBusinessCategoryPipe,FormatRatingPipe,ConvertToArrayPipe,GreenProductPipe,GpdocumentFilterArrayPipe],
  exports: [FormatBusinessCategoryPipe,FormatRatingPipe,ConvertToArrayPipe,GreenProductPipe,GpdocumentFilterArrayPipe]
})
export class FormatBusinessCategoryModule { }
