import { Pipe, PipeTransform } from '@angular/core';
import { ApplicationProductDetail, ApplicationFactoryDetail } from 'app/green-product-assurance/new-application/new-application.model';
import { debug } from 'util';

@Pipe({
  name: 'greenProduct'
})
export class GreenProductPipe implements PipeTransform {
  transform(NameList: string, filterArray: any, type: string): any {
    NameList='';
    if (type == 'Product') {
      filterArray.forEach((data) => {
        if(NameList==''){
          NameList = data.ProductName
        }else{
          NameList = NameList +','+ data.ProductName
        }
      })
    }

    if (type =='Factory') {
      filterArray.forEach((data) => {
        if(NameList==''){
          NameList = data.FactoryName
        }else{
          NameList = NameList +','+ data.FactoryName
        }

      })
    } 

    if (type =='ProductSealNo') {
      filterArray.forEach((data) => {
        if(NameList==''){
          NameList = data.ProductSealNo
        }else{
          NameList = NameList +','+ data.ProductSealNo
        }
      })
    }

    if (type =='ProductModelNo') {
      filterArray.forEach((data) => {
        if(NameList==''){
          NameList = data.ProductModelNo
        }else{
          NameList = NameList +"ÿ"+ data.ProductModelNo
        }

      })
    }
    return NameList;
  };
}
