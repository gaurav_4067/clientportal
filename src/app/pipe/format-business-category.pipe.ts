import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatBusinessCategory'
})
export class FormatBusinessCategoryPipe implements PipeTransform {

  transform(value: any, args?: any): any {
     
    if (value == 'FCAI') {
      return 'Audit';
    }
    else{
      return value;
    }
  }
}
