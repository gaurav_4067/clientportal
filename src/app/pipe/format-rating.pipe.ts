import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatRating'
})
export class FormatRatingPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value == 'Conformed') {
      return 'Pass';
    }
    else if(value == 'Non-Conformed'){
      return 'Fail';
    }
    else if(value == 'Pending'){
      return 'Hold';
    }
    else{
      return value
    }
  }

}
