import { Pipe, PipeTransform } from "@angular/core";
import { SF_EDI_Data_Extended } from "app/online-etrf/online-etrf.model";

@Pipe({
  name: "ediDataManipulationPipe"
})
export class EdiDataManipulationPipePipe implements PipeTransform {
  transform(ediDataList: Array<SF_EDI_Data_Extended>, args?: any): any {
    //debugger
    ediDataList.forEach(element => {
      if (element.Status == true) {
        let currentDate = new Date();
        let Shipment_Date = new Date(element.Shipment_Date);
        let monthDifference = 0;
        if (currentDate > Shipment_Date) {
          monthDifference = this.monthDiff(
            new Date(
              Shipment_Date.getFullYear(),
              Shipment_Date.getMonth(),
              Shipment_Date.getDay()
            ),
            new Date(
              currentDate.getFullYear(),
              currentDate.getMonth(),
              currentDate.getDay()
            )
          );
        } else {
          monthDifference = this.monthDiff(
            new Date(
              currentDate.getFullYear(),
              currentDate.getMonth(),
              currentDate.getDay()
            ),
            new Date(
              Shipment_Date.getFullYear(),
              Shipment_Date.getMonth(),
              Shipment_Date.getDay()
            )
          );
        }

        if (monthDifference <= 6) {
          element.IsAvailable = false;
        }
      }
      else{
        element.IsAvailable=true;
      }
    });

    return ediDataList;
   
  }

  monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
  }
}
