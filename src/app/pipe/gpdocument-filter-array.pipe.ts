import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gpdocumentFilterArray',
  pure: false
})
export class GpdocumentFilterArrayPipe implements PipeTransform {
  transform(value: any, type: string, ReportType: string): any {
    if (ReportType) {
      return value.filter(i => i.ServiceType == type && i.ReportType == ReportType && i.IsDeleted==false);
    }
    else if (type == 'MST' || type == 'FSA' || type == 'FST' || type=='RR'|| type=='FA' ||type=='ST' ) {
      return value.filter(i => i.ServiceType == type && i.ReportType!= 'SupportingReport' && i.IsDeleted==false);
    }
    else {
      return value.filter(i => i.DocumentType == type);
    }
  }
}
