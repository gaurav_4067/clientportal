import { Pipe, PipeTransform } from '@angular/core';
import { SF_EDI_Data } from 'app/online-etrf/online-etrf.model';

@Pipe({
  name: 'convertToArray'
})
export class ConvertToArrayPipe implements PipeTransform {
ediDataList:Array<SF_EDI_Data>;
  transform(value: any, args?: any): any {
    this.ediDataList=new Array<SF_EDI_Data>();
    this.ediDataList=JSON.parse(value);

    return this.ediDataList;
  }

}
