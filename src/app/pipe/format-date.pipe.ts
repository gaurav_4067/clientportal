import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {

  transform(value: string, args?: any): any {
    let input = value;
    let customDate = new Date(value);
    this.adjustForTimezone(customDate);
    return this.getFormattedDate(customDate);
  }
  getFormattedDate(date: Date) {
    return ((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear());
  }

  adjustForTimezone(date: Date): Date {
    let offset = date.getTimezoneOffset();
    var timeOffsetInMS: number = offset * 60000;
    //if (offset > 0) {
      date.setTime(date.getTime() + timeOffsetInMS + (480 * 60000));
    // }
    // else {
    //   date.setTime(date.getTime() - timeOffsetInMS + (480 * 60000));
    // }
    return date;
  }
}
