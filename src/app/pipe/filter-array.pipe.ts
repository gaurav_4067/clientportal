import { Pipe, PipeTransform } from '@angular/core';
import { EcomPromoOfferList, OfferSearchRequest } from 'app/admin/manage-promooffer/manage-promooffer.model';

@Pipe({
  name: 'filterArray'
})
export class FilterArrayPipe implements PipeTransform {

  filterOffers: Array<EcomPromoOfferList>;
  transform(offers: EcomPromoOfferList[], businessCategoryId: number, offerName: string, offerTypeId: number): any {
    this.filterOffers = new Array<EcomPromoOfferList>();
    if (parseInt(businessCategoryId.toString()) > 0) {
      offers = offers.filter(i => i.BusinessCategoryId.includes(businessCategoryId.toString()));
    }
    if (offerName) {
      offers = offers.filter(i => i.OfferName.includes(offerName.toString()));
      
    }
    if (parseInt(offerTypeId.toString()) > 0) {
      offers = offers.filter(i => i.OfferTypeId == parseInt(offerTypeId.toString()));
    }
      return offers;
  };
}
