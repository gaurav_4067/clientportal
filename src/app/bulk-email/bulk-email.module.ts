import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BulkEmailRoutingModule } from './bulk-email-routing.module';
import { BulkEmailComponent } from './bulk-email.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,FormsModule,
    BulkEmailRoutingModule
  ],
  declarations: [BulkEmailComponent],
  exports:[BulkEmailComponent]
})
export class BulkEmailModule { }
