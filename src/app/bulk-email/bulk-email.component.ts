import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bulk-email',
  templateUrl: './bulk-email.component.html',
  styleUrls: ['./bulk-email.component.css']
})
export class BulkEmailComponent implements OnInit {

  errorEmail: any;
	ccEmailTemp: any;
    ccField: any;

	@Input() clientEmail;
	@Input() checkedReportNo;
	@Input() remarks;
	@Input() ccEmails;

	ngOnInit()
	{
		
	}

	getCCEmail(e) 
    {
        var val = e.target.value;
        if (val.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            this.errorEmail = false;
            this.ccEmailTemp = val;
        }
        else if (val == "") {
            this.errorEmail = false;
        }
        else {
            this.errorEmail = true;
        }
    }

    addCC() 
    {
        if (this.ccEmailTemp != "" && this.errorEmail == false) 
        {
            this.ccEmails.push(this.ccEmailTemp);
            (<any>$('#cc-email-id')).val('');
            this.ccField = "";
        }
        this.ccEmailTemp = "";

    }

    removeCCEmail(i) {
        this.ccEmails.splice(i, 1);
    }

    addRemark(e) {
        var temp = e.target.value;
        this.remarks.push(temp);
    }

}
