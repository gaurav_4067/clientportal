import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from 'app/header/header.component';
import { FormatBusinessCategoryModule } from 'app/helper/format-business-category/format-business-category.module';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    FormatBusinessCategoryModule,
    Ng2AutoCompleteModule
  ],
  declarations: [HeaderComponent],
  exports:[HeaderComponent]
})
export class HeaderModule { }
