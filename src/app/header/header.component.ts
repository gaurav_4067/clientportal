import { Component, OnInit } from '@angular/core';
import { DataService, ETRFUrl } from 'app/mts.service';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
declare let swal: any;

@Component({
  selector: 'custom-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userDetail: any;
  userCredential: any;
  isLoader: boolean;
  categoryList: any;
  companyList: any;
  selectedCategory: number;
  defaultCategory: any;
  selectedCompany: number;
  selectedClientCompany:any;
  defaultCompany: any;
  search: string;
  etrfConfigPath: any;
  UserProfileimg: any;
  BusinessCategoryId: any;
  constructor(private dataService: DataService, public router: Router, private http: Http) {
    this.defaultCategory = { "BusinessCategoryName": "--Select Business Category--", "BusinessCategoryId": 0 };
    this.defaultCompany = { "CompanyName": "--Select Company--", "CompanyId": 0 };
    this.selectedCategory = 0;
    this.selectedCompany = 0;
    this.userCredential = {};
  }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    if (this.userDetail && this.userCredential) {
      this.BusinessCategoryId = this.userCredential.BusinessCategoryId;
      this.UserProfileimg = ETRFUrl + this.userDetail.ProfilePic;
      this.etrfConfigPath = this.dataService.etrfConfigPath;
    } else {
      this.router.navigate(['/login']);
    }
  }

  changeClass() {
    (<any>$("#profileDropdown")).toggle();
  }

  openCustom() {
    this.isLoader = true;
    this.selectedClientCompany='';  
    this.selectedCompany=0;
    this.selectedCategory=0;
    this.dataService.get('user/GetAssignedCompanyCategoryListByUser?UserId=' + this.userDetail.UserId).subscribe(
      r => {
        this.isLoader = false;
        this.categoryList = r.AssignedBusinessCategory;
        this.categoryList.splice(0, 0, this.defaultCategory);
        //this.companyList = r.AssignedCompanyList;
        // this.companyList.splice(0, 0, this.defaultCompany);
        (<any>$("#companyChange")).modal("show");
      }
    );
  }

  logOut() {
    this.isLoader = true;
    this.dataService.post('user/Logout', { UserId: this.userDetail.UserId }).subscribe(
      response => {
        if (response.IsSuccess) {
          this.isLoader = false;
          localStorage.removeItem('mts_user_cred');
          localStorage.removeItem('userDetail');
          localStorage.removeItem('ServiceId');
          localStorage.removeItem('ServiceName');
          localStorage.removeItem('ecommerceEtrfId');
          localStorage.removeItem('orderReviewRequest');
          this.router.navigate(['./login']);

        }

      });

  }

  categoryChange() {
    // alert(this.selectedCategory);
    this.selectedClientCompany='';  
    this.selectedCompany=0;
    this.companyList = [];
    // this.companyList.splice(0, 0, this.defaultCompany);
    var that = this;
    this.userDetail.AssignedCompanyANDCategory.AssignedCompanyList.forEach(element => {
      if (element.BusinessCategoryID == that.selectedCategory) {
        that.companyList.push(element);
      }
    });
  }

  setClientAndCategory() {
    if(this.selectedClientCompany){
      this.selectedCompany = this.selectedClientCompany.CompanyId;
    }
    let companyId = this.selectedCompany;
    let companyObj: any = {};
    let UserCompanyIdCondition = 0;
    if (this.selectedCompany > 0) {
      this.companyList.forEach(function (obj) {
        if (obj.CompanyId == companyId) {
          companyObj = obj;
        }
      })
      this.userCredential.UserId = this.userDetail.UserId;
      this.userCredential.CompanyId = companyObj.CompanyId;
      this.userCredential.UserType = companyObj.WorkingAs;
      this.userCredential.UserCompanyId = this.userDetail.CompanyId;
      this.userCredential.CompanyName = companyObj.CompanyName;
      this.userDetail.CompanyName = companyObj.CompanyName;
      this.userCredential.CompanyLogo = ETRFUrl + companyObj.CompanyLogo;
      this.userCredential.BusinessCategoryId = this.selectedCategory;
      this.userCredential.BusinessCompanyId = companyObj.BusinessCompanyId;
      localStorage.setItem('userDetail', JSON.stringify(this.userDetail));
      UserCompanyIdCondition = this.userDetail.UserCategoryId;
      this.isLoader = true;
      this.dataService.get('user/GetUserPermissions?UserId=' + this.userCredential.UserId + '&CompanyId=' + this.userCredential.CompanyId + '&BusinessCategoryId=' + this.selectedCategory).subscribe(
        res => {

          this.isLoader = false;
          this.userCredential.UserPermission = res.UserPermission;
          localStorage.setItem('mts_user_cred', JSON.stringify(this.userCredential));

          (<any>$("#companyChange")).modal("hide");

          if (UserCompanyIdCondition == 3 && res.UserPermission.ViewDashboard == false) {
            this.router.navigate(['./manage-document/other-report-upload']);
          }
          else if (res.UserPermission.ViewDashboard == false) {
            if (this.userCredential.BusinessCategoryId == 5) {
              this.GoToGreenProduct();
            } else {
              this.router.navigate(['./landing-page']);
            }
          }
          else {
            if (this.router.url.indexOf('dashboard') > 0) {
              location.reload();
            }
            else {
              this.router.navigate(['/dashboard']);
            }
          }
        }
      );

    }
    else {
      swal("", "Please select company.");
    }
  }

  globalSearch(value) {
    this.router.navigate(['./search..', value]);
  }

  GoToGreenProduct(){
    if(this.userCredential.UserPermission.GPInternalReview==true){
      this.router.navigate(['./green-product-assurance/internal-view']);
    }else if(this.userCredential.UserPermission.GPVendorView == true){
      this.router.navigate(['./green-product-assurance/vendor-view']);
    }else{
      this.router.navigate(['./landing-page']);
    }
  }

  public renderCompany(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CompanyName}</div>
      </div>`;

    return html;
  }

}
