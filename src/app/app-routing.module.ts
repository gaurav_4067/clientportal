import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoContentComponent } from './no-content/no-content.component';

// import { AddTrfComponent } from './e_TRF/add_trf';


const ROUTES: Routes =
    [
        { path: '', redirectTo: '/login', pathMatch: 'full' },
        {
            path: 'login',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(require('./login/login.module').LoginModule);
                            })
                })
        },
        //   {
        //     path: 'login',
        //     loadChildren: () => new Promise(
        //         resolve => {
        //             (require as any)
        //                 .ensure(
        //                     [],
        //                     require => {
        //                       resolve(require('./login/login.module').LoginModule);
        //                     })})
        //   },
        {
            path: 'registration-page',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './registration-page/registration-page.module')
                                        .RegistrationPageModule);
                            })
                })
        },
        {
            path: 'thank-you',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './registration-page/thank-you/thank-you.module')
                                        .ThankYouModule);
                            })
                })
        },
        {
            path: 'company',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './login/client-selection/client-selection.module')
                                        .ClientSelectionModule);
                            })
                })
        },
        {
            path: 'dashboard',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require('./dashboard/dashboard.module')
                                        .DashboardModule);
                            })
                })
        },
        {
            path: 'landing-page',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(require('./default-page/default-page.module')
                                    .DefaultPageModule);
                            })
                })
        },
        {
            path: 'forget-password',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './forget-password/forget-password.module')
                                        .ForgetPasswordModule);
                            })
                })
        },
        {
            path: 'reset-password',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './forget-password/reset-password/reset-Password.module')
                                        .ResetPasswordModule);
                            })
                })
        },
        {
            path: 'my-order',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(require('./my-order/my-order.module')
                                    .MyOrderModule);
                            })
                })
        },
        {
            path: 'manage-document',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './manage-document/manage-document.module')
                                        .ManageDocumentModule);
                            })
                })
        },
        {
            path: 'admin',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(require('./admin/admin.module').AdminModule);
                            })
                })
        },
        {
            path: 'order-analysis',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require('./order-analysis/order-analysis.module')
                                        .OrderAnalysisModule);
                            })
                })
        },
        {
            path: 'supporting-tool',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './supporting-tool/supporting-tool.module')
                                        .SupportingToolModule);
                            })
                })
        },
        {
            path: 'web-link',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(require('./web-link/web-link.module')
                                    .WebLinkModule);
                            })
                })
        },
        {
            path: 'order-information',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './order-information/order-information.module')
                                        .OrderInformationModule);
                            })
                })
        },
        {
            path: 'edi-dashboard',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require('./edi-dashboard/edi-dashboard.module')
                                        .EdiDashboardModule);
                            })
                })
        },
        {
            path: 'online-etrf',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(require('./online-etrf/online-etrf.module')
                                    .OnlineEtrfModule);
                            })
                })
        },
        {
            path: 'e-commerce',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require('./e-commerce/e-commerce.module')
                                        .ECommerceModule);
                            })
                })
        },
        {
            path: 'e-commerce-home',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require('./e-commerce/e-commerce.module')
                                        .ECommerceModule);
                            })
                })
        },
        {
            path: 'paypal',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(require('./paypal/paypal.module')
                                    .PaypalModule);
                            })
                })
        }, {
            path: 'green-product-assurance',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require('./green-product-assurance/green-product-assurance.module')
                                        .GreenProductAssuranceModule);
                            })
                })
        }
       ,{
            path: 'production-dashboard',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require('./production-dashboard/production-dashboard.module')
                                        .ProductionDashboardModule);
                            })
                })
        },
        {
            path: '**',
            component: NoContentComponent
        }
    ]

@NgModule({
    imports: [RouterModule.forRoot(ROUTES, { useHash: true })],
    exports: [RouterModule]
}) export class AppRoutingModule { }