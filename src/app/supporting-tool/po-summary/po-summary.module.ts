import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PoSummaryRoutingModule } from './po-summary-routing.module';
import { PoSummaryComponent } from './po-summary.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { FormsModule } from '@angular/forms';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { BulkEmailModule } from 'app/bulk-email/bulk-email.module';

@NgModule({
  imports: [
    CommonModule,
    PoSummaryRoutingModule,
    HeaderModule,
    FooterModule,
    FormsModule,
    MyDateRangePickerModule,
    BulkEmailModule
  ],
  declarations: [PoSummaryComponent]
})
export class PoSummaryModule { }
