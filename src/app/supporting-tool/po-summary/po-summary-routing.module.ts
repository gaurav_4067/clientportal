import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PoSummaryComponent } from 'app/supporting-tool/po-summary/po-summary.component';

const routes: Routes = [{
  path: '',
  component: PoSummaryComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PoSummaryRoutingModule { }
