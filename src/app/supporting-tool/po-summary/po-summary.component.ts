import { Component, OnInit } from '@angular/core';
import { SerachParameters } from './po-summary.model'
import { DataService, BaseUrl } from './../../mts.service'
declare let swal: any;

@Component({
  selector: 'app-po-summary',
  templateUrl: './po-summary.component.html',
  styleUrls: ['./po-summary.component.css']
})
export class PoSummaryComponent implements OnInit {
  isLoader: boolean = false;
  userCredential: any;
  userDetail: any;
  DateSelection: string = 'DateRange';
  showdatePicker: boolean = true;
  serachParameters: SerachParameters;
  ccEmails: any;
  remarks: any;
  Generating: boolean = false;
  progressLoader: boolean = false;
  bulkEmailSendObject: any = {
    EmailLogID: 0,
    EmailD: '',
    CompanyName: '',
    Body: '',
    Status: false,
    FileName: '',
    CreatedDate: Date.now(),
    type: '',
    CreatedBy: '',
    ModifiedBy: 0,
    ModifiedDate: Date.now(),
    CCEmailID: '',
    FileStatus: 'U'
  }

  constructor(private dataService: DataService) {
    this.serachParameters = new SerachParameters();
  }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.ccEmails = [];
    this.remarks = [];
  }

  changeRadioselection(type: string) {
    if (type == 'DateRange') {
      this.DateSelection = type;
      this.showdatePicker = true;
      this.serachParameters.StartDate = null;
      this.serachParameters.EndDate = null;
    }
    else if (type == 'Daily') {
      this.DateSelection = type;
      this.showdatePicker = false;
      this.serachParameters.StartDate = new Date();
      this.serachParameters.EndDate = new Date();
    }
    else if (type == 'Weekly') {
      this.DateSelection = type;
      this.showdatePicker = false;
      var DateFrom = new Date();
      this.serachParameters.StartDate = DateFrom = new Date(DateFrom.setDate(DateFrom.getDate() - 6));
      this.serachParameters.EndDate = new Date();
    }
    else if (type == 'Monthly') {
      this.DateSelection = type;
      this.showdatePicker = false;
      var DateFrom = new Date();
      this.serachParameters.StartDate = new Date(DateFrom.setMonth(DateFrom.getMonth() - 1));
      this.serachParameters.EndDate = new Date();
    }
  }

  onDateRangeChanged(event: any) {
    this.serachParameters.StartDate =JSON.parse(JSON.stringify(event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year));
    this.serachParameters.EndDate = JSON.parse(JSON.stringify(event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year));
  }

  generatePoSummary() {
    this.isLoader = true;
    this.serachParameters.ClientId = this.userCredential.CompanyId;
    this.dataService.post('User/GeneratePoSummary', this.serachParameters).subscribe(
      response => {
        if (response.IsSuccess) {
          this.isLoader = false;
          let downloadFileName = encodeURIComponent(response.Message);
          window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId="+this.userCredential.BusinessCategoryId;
        }
      })
  }

  Reset() {
    (<any>$('.btnclearenabled')).click();
    this.DateSelection = 'DateRange';
    this.showdatePicker = true;
    this.serachParameters = new SerachParameters();
  }

  SendMail() {
    this.userDetail.EmailId = this.userDetail.EmailId;
    (<any>$('#DownloadFile')).modal('show');
    this.ccEmails = [];
    this.remarks = [];
  }

  resetFile(index, e) {
    (<any>$('.FileCheck')).prop('checked', false);
  }

  downloadFile() {
    this.progressLoader = true;
    this.Generating = true;
    this.serachParameters.ClientId = this.userCredential.CompanyId;
    this.dataService.post('User/GeneratePoSummary', this.serachParameters).subscribe(
      response => {
        if (response.IsSuccess) {
          let downloadFileName = encodeURIComponent(response.Message);
          this.Generating = false;
          var that = this;
          if (that.userDetail.EmailId == "") {
            swal('', "Please enter emailid");
            return;
          } else {
            that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
          }
          that.bulkEmailSendObject.Subject = "Po-Summary";
          that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
          that.bulkEmailSendObject.FileName = downloadFileName;
          that.bulkEmailSendObject.CreatedBy = that.userCredential.UserId;
          that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
          that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
          that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
            response => {
              if (response.IsSuccess) {
                (<any>$('#DownloadFile')).modal('hide');
                this.progressLoader = false;
                this.Generating = true;
                swal('', response.Message);
                that.ccEmails = [];
                that.remarks = [];
              }
              else {
                this.progressLoader = false;
                this.Generating = true;
                swal('', response.Message);
              }
            }
          )
        }
      })
  }

}
