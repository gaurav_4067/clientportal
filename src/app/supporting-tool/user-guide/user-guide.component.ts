
import { Component, OnInit } from '@angular/core';
import { DataService, BaseUrl, ETRFUrl, COLUMN_LIST } from '../../mts.service';
import { Http, Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
declare let swal: any;


@Component({
  selector: 'app-user-guide',
  templateUrl: './user-guide.component.html',
  styleUrls: ['./user-guide.component.css'],
  providers: [DataService]
})

export class UserGuideComponent implements OnInit {

  documentArray: any;
  menuList: any;
  submenuList: any;
  uploadobj: any;
  Menu_error_text: any;
  SubMenu_error_text: any;
  userCredential: any;
  isLoader: boolean;
  MenuId: any = "";
  SubMenuId: any = "";
  disableMenu: boolean;
  flagMenu: any = "";
  userDetail: any;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) {

  }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      this.disableMenu = false;
      this.uploadobj = {};
      this.isLoader = true;
      this.menuListD();
      if (!this.userCredential.UserPermission.ViewUserGuide) {
        this.router.navigate(['./landing-page']);
      }
      this.dataService.get('Master/GetMenuList').subscribe(
        response => {
          if (response) {
            if (response.MenuList != null) {
              this.menuList = response.MenuList;
            }
            else {
              this.menuList = [];
              swal('', "Please try again", 'info');
            }
          }
        }
      )
    } else {
      this.router.navigate(['/login']);
    }
  }

  menuListD() {

    var that = this;
    this.isLoader = true;
    that.dataService.get('Master/GetUserGuideDocuments').subscribe(
      response => {
        if (response) {

          that.flagMenu = true;
          if (response.UserGuide != null) {
            that.documentArray = response.UserGuide;
            that.isLoader = false;
            for (let i = 0; i < that.documentArray.length; i++) {
              if (that.documentArray[i].SubMenu != null && that.documentArray[i].SubMenu.length > 0) {
                that.flagMenu = false;
                break;
              }

            }
          }
          else {
            this.documentArray = [];
            this.isLoader = false
          }
        }
      }
    )

  }

  onMenuChange(event: any) {

    this.uploadobj.SubMenuId = "";
    let menuId = event.target.value;
    if (menuId == "" || menuId == null || menuId == undefined) {
      this.Menu_error_text = "Required"
    } else {
      this.Menu_error_text = ""
    }
    this.dataService.get('Master/GetSubMenuList?MenuId=' + menuId).subscribe(
      response => {
        if (response) {

          if (response.SubMenuList != null) {
            this.submenuList = response.SubMenuList;
          }
          else {
            this.submenuList = [];
          }
        }
      }
    )
  }

  subMenuChange() {
    if (this.uploadobj.SubMenuId == "" || this.uploadobj.SubMenuId == null || this.uploadobj.SubMenuId == undefined) {
      this.SubMenu_error_text = "Required"
    }
    else {
      this.SubMenu_error_text = ""
    }
  }

  Download(FileName, name) {
    if (name == 'Doc') {
      let fileD = FileName.split("/");
      let downloadFileDoc = fileD[2].trim();
      // window.location.href = BaseUrl + "Master/DownloadUserGuideFile?FileName=" + downloadFileDoc;
    } else {
      let fileD = FileName.split("/");
      let downloadFileVideo = fileD[2].trim();
      // window.location.href = BaseUrl + "Master/DownloadUserGuideFile?FileName=" + downloadFileVideo;
    }

  }

  redirect(FileName, name) {

    if (name == 'Doc') {
      let docFile = ETRFUrl + FileName
      window.open(docFile, '_blank');
    } else {
      let videoFile = ETRFUrl + FileName;
      var new_window = window.open('', '_blank');
      var html_contents = "<div id='html_contents' data-new-window>" +
        "<video controls style='width: 100%; height: auto;'>" +
        "<source src='" + videoFile + "' type='video/mp4' /></video></div>";
      if (html_contents !== null) {
        new_window.document.write('<!doctype html><html><head><title>My Video</title><meta charset="UTF-8" /></head><body>' + html_contents + '</body></html>');
        new_window.document.close();
      }
      window.open(videoFile, '_blank');
    }

  }

  fileuploadDoc(event: any) {

    if (event.target.files[0].name && event.target.files) {
      this.uploadobj.DocumentFileName = event.target.files[0].name;
      this.uploadobj.DocumentFile = event.target.files[0];
    }
    this.validateFileExtension1(this.uploadobj.DocumentFileName);

  }

  fileuploadVideo(event: any) {
    if (event.target.files[0].name && event.target.files) {
      this.uploadobj.VideoFileName = event.target.files[0].name;
      this.uploadobj.VideoFile = event.target.files[0];
    }
    this.validateFileExtension2(this.uploadobj.VideoFileName);
  }

  resetVideo(event) {
    //event.currentTarget.files=null;
    event.currentTarget.value = "";
    //this.uploadobj.nativeElement.value = "";
    this.uploadobj.VideoFileName = null;
    this.uploadobj.VideoFile = {};
  }

  resetDoc(event) {
    event.currentTarget.value = "";
    this.uploadobj.DocumentFileName = null;
    this.uploadobj.DocumentFile = {};
  }

  upload() {
    var validationTest = {
      'f1': true,
      'f2': true,
      'f3': true,
      'f4': true
    };
    var finalValidation = true;

    if (this.uploadobj.MenuId == "" || this.uploadobj.MenuId == null || this.uploadobj.MenuId == undefined) {
      this.Menu_error_text = "Required"
      validationTest.f1 = false;

    } else {
      this.Menu_error_text = "";
      validationTest.f1 = true;
    }
    if (this.uploadobj.SubMenuId == "" || this.uploadobj.SubMenuId == null || this.uploadobj.SubMenuId == undefined) {
      this.SubMenu_error_text = "Required";
      validationTest.f2 = false;
    } else {
      this.SubMenu_error_text = "";
      validationTest.f2 = true;
    }
    if (this.uploadobj.DocumentFileName) {
      var result1 = this.validateFileExtension1(this.uploadobj.DocumentFileName);
      validationTest.f3 = result1;
    }
    if (this.uploadobj.VideoFileName) {
      var result2 = this.validateFileExtension2(this.uploadobj.VideoFileName);
      validationTest.f4 = result2;
    }

    for (let i in validationTest) {
      if (validationTest[i] == false) {
        finalValidation = false;
        return;
      }
    }
    if (finalValidation == true) {
      this.uploadFunc()
    }
  }

  uploadFunc() {
    this.uploadobj.CreatedBy = this.userCredential.UserId;
    let data = new FormData();
    data.append("UploadDocument", this.uploadobj.DocumentFile);
    data.append("UploadVideo", this.uploadobj.VideoFile);
    data.append("UploadUserGuide", JSON.stringify(this.uploadobj));
    let thisObj = this;
    thisObj.isLoader = true;


    let returnObj = thisObj.dataService.postFile('Master/UploadDocuments', data);
    returnObj.done(function (xhr, textStatus) {
      thisObj.isLoader = false;

      if (xhr.IsSuccess) {
        thisObj.menuListD();
        thisObj.uploadobj.DocumentFile = {};
        thisObj.uploadobj.VideoFile = {};
        thisObj.Menu_error_text = "";
        thisObj.SubMenu_error_text = "";
        thisObj.uploadobj = {};
        thisObj.submenuList = [];
        // this.menuList = [];
        (<any>$("#addUserGuide")).modal('hide')
      }
      else {
        swal('', xhr.Message);
        thisObj.uploadobj.DocumentFile = {};
        thisObj.uploadobj.VideoFile = {};
        (<any>$("#addUserGuide")).modal('show')
      }

    })
  }

  edit(obj) {
    this.disableMenu = true;
    this.isLoader = true;
    var that = this;
    that.dataService.get('Master/GetUserGuideById?Id=' + obj.Id).subscribe(
      response => {
        that.uploadobj.Id = response.Id;
        that.uploadobj.MenuId = response.MenuId;
        that.uploadobj.SubMenuId = response.SubMenuId;
        that.uploadobj.DocumentFileName = response.DocumentFileName;
        that.uploadobj.VideoFileName = response.VideoFileName;
        that.uploadobj.Description = response.Description;
        that.isLoader = false;
        this.dataService.get('Master/GetSubMenuList?MenuId=' + response.MenuId).subscribe(
          response => {
            if (response) {
              if (response.SubMenuList != null) {
                this.submenuList = response.SubMenuList;
              }
              else {
                this.submenuList = [];
              }
            }
          }
        )
      }
    );

  }

  delete(submenuId) {
    var that = this;
    swal({
      title: "",
      text: "<span style='color:#ef770e;font-size:20px;'>Are you sure?</span><br><br>",
      html: true,
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Proceed",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true

    }, function (isConfirm) {
      if (isConfirm) {
        that.isLoader = true;
        that.dataService.get('Master/DeleteUserGuide?Id=' + submenuId.Id).subscribe(
          response => {
            if (response.IsSuccess) {
              // swal('', response.Message);
              that.isLoader = false;
              that.menuListD();
            }
            else {
              that.isLoader = false;
              swal('', response.Message)
            }
          }
        );
      }
    }
    );

  }

  close() {
    this.disableMenu = false;
    this.Menu_error_text = "";
    this.SubMenu_error_text = "";
    this.uploadobj.MenuId = "";
    this.uploadobj.SubMenuId = "";
    this.uploadobj.DocumentFileName = "";
    this.uploadobj.VideoFileName = "";
    this.uploadobj.Description = "";
    this.uploadobj.DocumentFile = {};
    this.uploadobj.VideoFile = {};
    this.submenuList = [];
    //this.menuList = [];


  }

  validateFileExtension1(fileName): boolean {

    let extension = fileName.substr(fileName.lastIndexOf('.') + 1);
    if (['pdf', 'doc', 'docx', 'xls', 'xlsx', 'jpeg', 'jpg', 'png', 'txt', 'ppt', 'pptx'].indexOf(extension.toLowerCase()) > -1) {
      return true;
    }
    else {
      swal('', 'Please Upload only .pdf, .doc, .docx, .xls, .xlsx ,.jpeg, .jpg, .png, .txt, .ppt, .pptx files');
      this.uploadobj.DocumentFileName = null;
      this.uploadobj.DocumentFile = {};
      return false;
    }
  }

  validateFileExtension2(fileName): boolean {
    let extension = fileName.substr(fileName.lastIndexOf('.') + 1);
    if (['mp4'].indexOf(extension.toLowerCase()) > -1) {
      return true;
    }
    else {
      swal('', 'Please Upload only.mp4 files');
      this.uploadobj.VideoFileName = null;
      this.uploadobj.VideoFile = {};
      return false;
    }
  }
}
