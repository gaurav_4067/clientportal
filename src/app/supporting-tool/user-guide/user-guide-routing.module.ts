import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserGuideComponent } from 'app/supporting-tool/user-guide/user-guide.component';

const routes: Routes = [{
  path:'',
  component:UserGuideComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserGuideRoutingModule { }
