import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UserGuideRoutingModule } from './user-guide-routing.module';
import { UserGuideComponent } from './user-guide.component';
import { FooterModule } from 'app/footer/footer.module';
import { HeaderModule } from 'app/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UserGuideRoutingModule,
    FooterModule,
    HeaderModule
  ],
  declarations: [UserGuideComponent]
})
export class UserGuideModule { }
