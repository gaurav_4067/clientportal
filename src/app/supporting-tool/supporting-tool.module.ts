import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SupportingToolRoutingModule } from './supporting-tool-routing.module';
import { NoContentModule } from '../no-content/no-content.module';

@NgModule({
  imports: [
    CommonModule,
    SupportingToolRoutingModule,
    NoContentModule
  ],
  declarations: []
})
export class SupportingToolModule { }
