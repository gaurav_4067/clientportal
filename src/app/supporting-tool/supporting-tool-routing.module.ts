import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoContentComponent } from '../no-content/no-content.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: NoContentComponent
    },
    {
      path: 'user-guide',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./user-guide/user-guide.module').UserGuideModule);
        })
      })
    },
    {
      path: 'po-summary',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./po-summary/po-summary.module').PoSummaryModule);
        })
      })
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportingToolRoutingModule { }
