import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from 'app/app-routing.module';

// Angular 2 Localization.
import { TranslateService, TRANSLATION_PROVIDERS } from '../translate';

// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { DataService } from './mts.service';
import { NoContentModule } from './no-content/no-content.module';
import { FormatBusinessCategoryModule } from 'app/helper/format-business-category/format-business-category.module';

 
// Application wide providers
const APP_PROVIDERS = [...APP_RESOLVER_PROVIDERS, AppState];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NoContentModule,
    FormatBusinessCategoryModule
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    APP_PROVIDERS, TRANSLATION_PROVIDERS, TranslateService, DataService
  ],
  
  // IMPORTANT:
  // Since 'AdditionCalculateWindow' is never explicitly used (in a template)
  // we must tell angular about it.
  entryComponents: []
})
export class AppModule {
  /*constructor(public appRef: ApplicationRef, public appState: AppState) {}

   hmrOnInit(store: StoreType) {
   if (!store || !store.state) return;
   console.log('HMR store', JSON.stringify(store, null, 2));
   // set state
   this.appState._state = store.state;
   // set input values
   if ('restoreInputValues' in store) {
   let restoreInputValues = store.restoreInputValues;
   setTimeout(restoreInputValues);
   }

   this.appRef.tick();
   delete store.state;
   delete store.restoreInputValues;
   }

   hmrOnDestroy(store: StoreType) {
   const cmpLocation = this.appRef.components.map(cmp =>
   cmp.location.nativeElement);
   // save state
   const state = this.appState._state;
   store.state = state;
   // recreate root elements
   store.disposeOldHosts = createNewHosts(cmpLocation);
   // save input values
   store.restoreInputValues  = createInputTransfer();
   // remove styles
   removeNgStyles();
   }

   hmrAfterDestroy(store: StoreType) {
   // display new elements
   store.disposeOldHosts();
   delete store.disposeOldHosts;
   }*/
}
