import { Component, HostListener } from '@angular/core';

@Component({
    selector: 'app-privacy-policy',
    templateUrl: './privacy-policy.component.html',
    styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent {
    @HostListener('window:scroll', ['$event'])
    onScrollEvent(event) {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }

    constructor() { }

    topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    goToSection(id) {
        document.getElementById(id).scrollIntoView();
        document.documentElement.scrollTop = document.documentElement.scrollTop - 61;
    }
}