import { ECommerceHeaderModule } from './e-commerce-header.module';

describe('ECommerceHeaderModule', () => {
  let eCommerceHeaderModule: ECommerceHeaderModule;

  beforeEach(() => {
    eCommerceHeaderModule = new ECommerceHeaderModule();
  });

  it('should create an instance', () => {
    expect(eCommerceHeaderModule).toBeTruthy();
  });
});
