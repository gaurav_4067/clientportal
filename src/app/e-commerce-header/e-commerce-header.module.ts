import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {ECommerceHeaderRoutingModule} from './e-commerce-header-routing.module';
import {ECommerceHeaderComponent} from './e-commerce-header.component';

@NgModule({
  imports: [CommonModule, FormsModule, ECommerceHeaderRoutingModule],
  declarations: [ECommerceHeaderComponent],
  exports: [ECommerceHeaderComponent]
})
export class ECommerceHeaderModule {
}
