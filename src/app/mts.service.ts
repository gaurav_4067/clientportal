/**
 * Created by azmat on 23/11/16.
 */
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
declare let swal: any;
import * as jQuery from 'jquery';

export const COLUMN_LIST = [
    { name: 'Select Column', value: '' },
    { name: 'Brand', value: 'Brand' },
    { name: 'Lab Locations', value: 'LabLocationName' },
    { name: 'PO Number', value: 'PONumber' },
    { name: 'Rating', value: 'ReportRating' },
    { name: 'Vendor Name', value: 'ApplicantName' },
    { name: 'Agent Name', value: 'AgentName' },
    { name: 'Manufacture Name', value: 'ManufacturerName' },
    { name: 'Report No', value: 'ReportNo' },
    { name: 'Style No', value: 'StyleNo' },
    { name: 'Sku No', value: 'SKUNo' },
    { name: 'Sample Description', value: 'SampleDescription' },
    { name: 'Pandora Project #', value: 'AmazonProjecNumber' },
    { name: 'Project Name', value: 'C1' },
    { name: 'Product Name', value: 'Description' },
    { name: 'Vendor Item #', value: 'Licence_no' },
    { name: 'Collection', value: 'Season' },
    { name: 'Supplier Name /#', value: 'SampleDetailContactId' },
    { name: 'Batch #', value: 'Lot_Batch_No' },
    { name: 'Country of Origin', value: 'CountryofOrigin' },
    { name: 'Material Type', value: 'Material_Type' },
    { name: 'Product Type', value: 'ProductType' },
    { name: 'Drop #', value: 'Art_no' },
    { name: 'Item AX #', value: 'C2' },
    { name: 'Manufacturer', value: 'TGCC_Name_of_Manufacturer' },
    { name: 'PO #', value: 'PONumber' },
    { name: 'Quantity of sample', value: 'Po_Qty' },
    { name: 'Objective', value: 'EndUses' }
];

export const Laboratyemailid: string = 'daveho@mts-global.com';

//production value:
export const BaseUrl = 'https://clientportal.mts-global.com/ClientSolutionService/api/';
export const ETRFUrl ="https://clientportal.mts-global.com/ClientSolutionService/";
export const SealLogoBaseUrl: string = 'https://clientportal.mts-global.com/ClientSolutionService/Files/GreenProduct/';
 
// // Localhost
// export const BaseUrl: string = 'http://localhost:57833/api/';
// export const ETRFUrl: string = 'http://localhost:57833/';
// export const SealLogoBaseUrl: string = 'http://localhost:57833/Files/GreenProduct/';

//UAT --3000
// export const BaseUrl: string = 'http://182.73.105.242:200/api/';
//  export const ETRFUrl: string = 'http://182.73.105.242:200/';
// export const SealLogoBaseUrl: string = 'http://182.73.105.242:200/Files/GreenProduct/';

//Testing --3001
// export const BaseUrl: string = 'http://182.73.105.242:152/api/';
// export const ETRFUrl: string = 'http://182.73.105.242:152/';
// export const SealLogoBaseUrl: string = 'http://182.73.105.242:152/Files/GreenProduct/';
 
// //GPA -3007
// export const BaseUrl: string = 'http://182.73.105.242:170/api/';
// export const ETRFUrl: string = 'http://182.73.105.242:170/';
// export const SealLogoBaseUrl: string = 'http://182.73.105.242:170/Files/GreenProduct/';

@Injectable()
export class DataService {
    public showUpdateETRFButton: boolean = false;
    // public updateETRFUrl: string = 'http://182.73.105.242:200/api/';
    public updateETRFUrl: string = 'https://clientportal.mts-global.com/ClientSolutionService/api/';
    public updateETRFButtonText: string = "Update To Production";
    // public updateETRFButtonText: string = "Update To UAT";
    // public baseUrl: string = 'http://localhost:57833/api/';
    public imageSizeForEcommEtrf: boolean = false;
    // public baseUrl: string = 'http://182.73.105.242:170/api/';
    // public baseUrl: string = 'http://182.73.105.242:200/api/';
    // public baseUrl: string = 'http://35.154.110.251:82/api/';
    // public baseUrl: string  = 'http://182.73.105.242:170/api/';
    public baseUrl: string = 'https://clientportal.mts-global.com/ClientSolutionService/api/';
    public selectedServiceText: string = "Select Packages";
    public addNewChartColumnList = [];
    public EditGraphConfiguration: any = 0;
    public etrfConfigPath: string = 'http://localhost:63342/trunk/Code/src/index.html#/manage-form';
    selectedPackages = [];
    selectedService = { name: '', id: '' };
    EcommFlag = { Flag: 1 }
    EcommPackageUniqueId = '';
    EcommApplicatntAndLabInfo = '';
    DynamicDropValue = [];
    SendEmailEtrfFile = '';
    EcommPackageDetails = { IsPackages: true, PackageName: '', PackageTestName: [], IndividualTestItems: [] }
    EcommETRFData = { submittedby: '', report_Document: '', servicelevel: '', countryofdestination: '', agegrade: '', IsEcomm: false }
    modalOpened: string = "";
    ready: boolean = false;
    EcommETRFtitle: "(rohit)";
    registeredCompanyName: string = "";
    ecommInspectionSendEmail: any;

    fieldType:String="";
    fieldValue:String="";
    ediDataFieldId:number=0;
    ediDataList1:any;

    // Mapping Data
    currentFormId:number=0;
    DynamicDropDependentValue = [];
    SF_EDI_DATA_ID:string = "";
    SFSelectedBusinessLine: string="";
    caption: any;
    constructor(private http: Http, private router: Router) {
    }

    get(url: string): any {
        return this.http.get(`${this.baseUrl}` + url, { headers: this.getHeaders() }).map(
            (res: Response) => {
                if (res) {
                    if (res.status === 200) {
                        return res.json();
                    }
                }
            }).catch(
                (error: any) => {
                    if (error.status === 401) {
                        return this.throwError();
                    }
                })
    }

    getStatic(url: string): any {
        return this.http.get('http://182.73.105.242:152/api/' + url, { headers: this.getStaticHeaders() }).map(
            (res: Response) => {
                if (res) {
                    if (res.status === 200) {
                        return res.json();
                    }
                }
            }).catch(
                (error: any) => {
                    if (error.status === 401) {
                        return this.throwError();
                    }
                })
    }

    private getStaticHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        // headers.append('Token', JSON.parse(localStorage.getItem('userDetail')).AuthToken);
        return headers;
    }

    getOrder(url: string): any {
        return this.http.get(`${this.baseUrl}` + url, {}).map(
            (res: Response) => {
                if (res) {
                    if (res.status === 200) {
                        return res.json();
                    }
                }
            }).catch(
                (error: any) => {
                    if (error.status === 401) {
                        return this.throwError();
                    }
                })
    }

    post(url: string, Data: any): Observable<any> {
        return this.http.post(`${this.baseUrl}` + url, Data, { headers: this.getHeaders() }).map(
            (res: Response) => {
                if (res) {
                    if (res.status === 200) {
                        return res.json();
                    }
                }
            }).catch(
                (error: any) => {
                    if (error.status === 401) {
                        return this.throwError();
                    }
                }
            )
    }

    signUpGet(url: string): any {
        return this.http.get(`${this.baseUrl}` + url, { headers: this.getStaticHeaders() }).map(
            (res: Response) => {
                if (res) {
                    if (res.status === 200) {
                        return res.json();
                    }
                }
            }).catch(
                (error: any) => {
                    if (error.status === 401) {
                        return this.throwError();
                    }
                })
    }

    signUpPost(url: string, Data: any): Observable<any> {
        return this.http.post(`${this.baseUrl}` + url, Data, { headers: this.getStaticHeaders() }).map(
            (res: Response) => {
                if (res) {
                    if (res.status === 200) {
                        return res.json();
                    }
                }
            }).catch(
                (error: any) => {
                    if (error.status === 401) {
                        return this.throwError();
                    }
                }
            )
    }

    public clone(object: any) {
        return JSON.parse(JSON.stringify(object));
    }


    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Token', JSON.parse(localStorage.getItem('userDetail')).AuthToken);
        return headers;
    }
    postFile(url: string, data: any) {
        var ajaxRequest = (<any>$).ajax({
            type: "POST",
            url: this.baseUrl + url,
            contentType: false,
            processData: false,
            data: data,
            headers: { 'Token': JSON.parse(localStorage.getItem('userDetail')).AuthToken }
        });
        return ajaxRequest;
    }
    private throwError(): Observable<any> {

        this.router.navigate(['/login']);
        //swal('','Error 401..');
        return Observable.empty();
    }

    fnExcelReport(id: any) {
        //var txtArea1: any;
        var txtArea1 = (<any>$('#txtArea1'));
        var sa;
        var tab_text = "<table border='2px'><tr>";
        var textRange; var j = 0;
        var tab = <any>document.getElementById(id); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
        }
        tab_text = tab_text + "</table>";
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {

            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "vendorScore.xls");
        }
        else {
            //other browser not tested on IE 11
            //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent("\ufeff"+tab_text));
            var str = encodeURIComponent("\ufeff" + tab_text);
            var uri = 'data:application/vnd.ms-excel,' + str;
            var downloadLink = document.createElement("a");
            downloadLink.href = uri;
            // downloadLink.download = "VendorPerformanceAnalysis.xls";
            downloadLink.download = id + ".xls";
            <any>document.body.appendChild(downloadLink);
            downloadLink.click();
            <any>document.body.removeChild(downloadLink);
        }
        //return (sa);
    }
}
