/**
 * Created by Ved Parkash Sharma on 05-01-2017.
 */

export const SEARCH_MODEL={
    "PageSize":"",
    "PageNumber":"",
    "OrderBy":"LogoutTime",
    "Order":"desc",
    "TotalRecords":"",
    "StartDate":"",
    "EndDate" :"",
    "SearchText":[]
};
export class SearchModel{
    ClientId:string;
    UserType:any
    StartDate:string;
    EndDate:string;
    ReportNo:string;
    SKUNo:string;
    StyleNo:string;
    Description:string;
    UserCompanyId:string;
    PageSize:number;
    PageNumber:number;
}







