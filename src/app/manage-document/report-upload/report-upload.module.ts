import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { Ng2PaginationModule } from 'ng2-pagination';
import { MdInputModule } from '@angular2-material/input';
import { TooltipDirective } from 'ng2-tooltip-directive/components';

import { ReportUploadRoutingModule } from './report-upload-routing.module';
import { ReportUploadComponent } from './report-upload.component';
import { FooterModule } from 'app/footer/footer.module';
import { HeaderModule } from 'app/header/header.module';
import { InspectionReportUploadModule } from 'app/manage-document/report-upload/inspection-report-upload/inspection-report-upload.module';
import { VendorEmailModule } from 'app/vendor-email/vendor-email.module';
import {TooltipModule} from "ngx-tooltip";
import { BulkEmailModule } from 'app/bulk-email/bulk-email.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    Ng2PaginationModule,
    MyDateRangePickerModule,
    MdInputModule,
    ReportUploadRoutingModule,
    FooterModule,
    HeaderModule,
    BulkEmailModule,
    InspectionReportUploadModule,
    TooltipModule,
    VendorEmailModule
  ],
  declarations: [ReportUploadComponent]
})
export class ReportUploadModule { }
