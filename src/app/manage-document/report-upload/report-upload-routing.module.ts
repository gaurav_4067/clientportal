import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportUploadComponent } from 'app/manage-document/report-upload/report-upload.component';


const routes: Routes = [{
  path: '',
  component: ReportUploadComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportUploadRoutingModule { }
