import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionReportUploadComponent } from './inspection-report-upload.component';

describe('InspectionReportUploadComponent', () => {
  let component: InspectionReportUploadComponent;
  let fixture: ComponentFixture<InspectionReportUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionReportUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionReportUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
