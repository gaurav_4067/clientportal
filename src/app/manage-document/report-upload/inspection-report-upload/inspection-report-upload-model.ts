
export class InspectionSearchModel {
    ClientId: any;
    UserId:number;
    BusinessCategoryId: number;
    UserType: string;
    UserCompanyId: number;
    PageSize: number;
    PageNumber: number;
    Order: string = 'desc';
    OrderBy: string = 'Bookingid';
    ReportNo: string;
    SearchText: any = [];
}
export class InspectionSearchText {
    InspStartDate: string;
    InspEndDate: string;
    ReportStartDate: string;
    ReportEnddate: string;
    ReportNo: string;
    InspCountry: string;
    InspState: string;
    InspCity: string;
}


export class InspectionCountries {
    public InspectionCountries() {
        this.States = new Array<InspStates>();
    }
    CountryID: number;
    Country_Name: string;
    public States: Array<InspStates>;

}

export class InspStates {
    public InspProvince() {
        this.Locations = new Array<InspCities>();
    }
    CountryID: number;
    ProvinceID: number;
    Province: string;
    public Locations: Array<InspCities>
}

export class InspCities {
    ProvinceID: number;
    LocationID: number;
    Location_Name: string
}

export class SelectedLocation {
    selectedCountryID: any = '';
    selectedStateId: any = '';
    selectedCityId: any = ''
    selectedCountryName: string = "";
    selectedStateName: string = "";
    selectedcityName: string = "";
}