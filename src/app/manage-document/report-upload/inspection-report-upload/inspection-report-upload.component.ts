
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Http, Response } from '@angular/http';
import { DataService, BaseUrl, COLUMN_LIST, ETRFUrl } from '../../../mts.service';
import { Router } from '@angular/router'
import { InjectService } from '../../../injectable-service/injectable-service';
import {
  InspectionSearchModel, InspectionCountries, SelectedLocation,
  InspStates, InspCities, InspectionSearchText
} from './inspection-report-upload-model';
import Order = jasmine.Order;
declare let swal: any;

@Component({
  selector: 'app-inspection-report-upload',
  templateUrl: './inspection-report-upload.component.html',
  styleUrls: ['./inspection-report-upload.component.css']
})

export class InspectionReportUploadComponent implements OnInit {
  InspectionSearchModel: InspectionSearchModel;
  inspectionCountries: Array<InspectionCountries>;
  inspStates: Array<InspStates>;
  inspCities: Array<InspCities>;
  selectedLocation: SelectedLocation;
  InspectionSearchText: InspectionSearchText;
  supportingFilesUpload: any;
  uploadedGCCCount: any;
  @ViewChild('fileUpload') fileUploader: ElementRef;
  @ViewChild('BookingForm"') BookingFormUploader: ElementRef;
  private currentPageNumber: number;
  private totalItem: number;
  private itemPerPage: any;
  private dateRange: any;
  private fileName: string;
  private ReportDateRange: any = {};
  fileType: string;
  vendorId: any;
  fileListNames: any;
  reportFilesUpload: any = [];
  gccFilesUpload: any = [];
  uploadedReportCount: any;
  //GCC: any;
  filesCount: any;
  Model: any;
  reportList: any;
  fileToUpload: any;
  report: any = {};
  emailObj: any = {
    "VendorEmailId": "",
    "ManufacturerEmailId": "",
    "LaboratoryEmailId": "",
    "CC": "",
    "Subject": "",
    "WebLink": "",
    "Comments": "",

  };
  invoiceNumber: any;
  invoiceValue: any;
  isLoader: boolean = false;
  isUploadReport: boolean = false;
  userCredential: any;
  userDetail: any;
  TrfPop: boolean = false;
  ReportPop: boolean = false;
  InvoicePop: boolean = false;
  showDownloadFiles: boolean = false;
  DownloadfileName: any;
  labLocationList: any;
  DivisionName: any;
  checkedReportNo: any = [];
  checkedReportArr: any = [];
  clientEmail: any;
  ccEmails: any = [];
  remarks: any = [];
  DueDate: any;
  /* Ng-Model Paramter For Search */
  ReportNo: number;
  DateRange: any;
  bulkEmailSendObject: any = {
    EmailLogID: 0,
    EmailD: '',
    Subject: '',
    Body: '',
    ReportNo: '',
    Status: false,
    CompanyName: '',
    FileName: '',
    CreatedDate: Date.now(),
    CreatedBy: '',
    ModifiedBy: 0,
    type: '',
    ModifiedDate: Date.now(),
    CCEmailID: '',
    FileStatus: 'U',
    BusinessCategoryId:0,
    FileType:''
  }
  private myDateRangePickerOptions = {
    clearBtnTxt: 'Clear',
    beginDateBtnTxt: 'Begin Date',
    endDateBtnTxt: 'End Date',
    acceptBtnTxt: 'OK',
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'mo',
    sunHighlight: true,
    height: '34px',
    width: '260px',
    inline: false,
    selectionTxtFontSize: '12px',
    alignSelectorRight: false,
    indicateInvalidDateRange: true,
    showDateRangeFormatPlaceholder: false,
    showClearBtn: false,
    customPlaceholderTxt: 'Login Date-Range'
  };
  uploadType: any = "";
  bulkUploadFiles: any = [];
  tempBulkUploadFiles: any = [];
  bulkFileUpload: any = [];
  bulkFileDisplay: any = [];
  ETRFNumber: any;
  failReason: string;
  testFlag1: any = {};
  countryList: any = [];
  stateList: any = [];
  cityList: any = [];

  ctrList: any;
  sList: any;
  cList: any;



  constructor(private dataService: DataService, private injectService: InjectService, private router: Router, private http: Http) {
    this.inspectionCountries = new Array<InspectionCountries>();
    this.inspStates = new Array<InspStates>();
    this.inspCities = new Array<InspCities>();
    this.selectedLocation = new SelectedLocation();
    this.InspectionSearchModel = new InspectionSearchModel();
    this.InspectionSearchText = new InspectionSearchText();
  }

  ngOnInit() {
    this.isLoader = true;
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    if (this.userCredential && this.userDetail) {
      if (this.userCredential.UserPermission.ViewReport) {
        this.InspectionSearchModel.ClientId = this.userCredential.BusinessCompanyId == 0 ? this.userCredential.UserCompanyId : this.userCredential.BusinessCompanyId;
        this.itemPerPage = 1;
        this.totalItem = 0;
        this.dateRange = {};
        this.labLocationList = [];
        this.DivisionName = [];
        this.report = {};
        var that = this;
        that.dataService.get('Master/GetCountryList').subscribe(
          r => {
            that.countryList = r.CountryList;
          }
        )

        this.InspectionSearchModel.ClientId = this.userCredential.BusinessCompanyId == 0 ? this.userCredential.UserCompanyId : this.userCredential.BusinessCompanyId;
        this.InspectionSearchModel.UserId = this.userCredential.UserId
        this.InspectionSearchModel.UserType = this.userCredential.UserType;
        this.InspectionSearchModel.UserCompanyId = this.userCredential.UserCompanyId;
        this.InspectionSearchModel.BusinessCategoryId = this.userCredential.BusinessCategoryId;
        this.InspectionSearchModel.PageSize = 10;
        this.InspectionSearchModel.PageNumber = 1;
        this.InspectionSearchModel.SearchText = this.InspectionSearchText;
        this.getReportList();
        this.testFlag1.flag = false;
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  showDownload() {
    this.showDownloadFiles = this.showDownloadFiles == true ? this.showDownloadFiles = false : this.showDownloadFiles = true;
  }

  public renderCountry(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CountryName}</div>
      </div>`;

    return html;
  }

  public renderState(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.StateName}</div>
      </div>`;

    return html;
  }

  public renderCity(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CityName}</div>
      </div>`;

    return html;
  }

  showTrfPop() {
    this.TrfPop = this.TrfPop == true ? this.TrfPop = false : this.TrfPop = true;
  }

  // onDueDateChanged(e) {

  //     var date = e.date.month + '/' + e.date.day + '/' + e.date.year;
  //     this.searchParam.SearchText[2].ColumnValue = date;
  // }

  showReportPop() {

    this.ReportPop = this.ReportPop == true ? this.ReportPop = false : this.ReportPop = true;
    setTimeout(function () {
      (<any>$('#collapse2')).collapse('show');
    }, 0)


  }

  showInvoicePop() {
    this.InvoicePop = this.InvoicePop == true ? this.InvoicePop = false : this.InvoicePop = true;
  }

  resetSearchForm() {
    this.selectedLocation = new SelectedLocation();
    (<any>$('.btnclearenabled')).click();
    (<any>$('.resetInput')).val('');
    this.InspectionSearchModel.ReportNo = '';
    this.sList = "";
    this.cList = "";
    this.ctrList = "";
    this.searchReport();



  }

  searchReport() {
    if (this.dateRange.StartDate && this.dateRange.EndDate) {
      this.InspectionSearchText.InspStartDate = JSON.parse(JSON.stringify(this.dateRange.StartDate));
      this.InspectionSearchText.InspEndDate = JSON.parse(JSON.stringify(this.dateRange.EndDate));
    }
    if (this.ReportDateRange.StartDate && this.ReportDateRange.EndDate) {
      this.InspectionSearchText.ReportStartDate = JSON.parse(JSON.stringify(this.ReportDateRange.StartDate));
      this.InspectionSearchText.ReportEnddate = JSON.parse(JSON.stringify(this.ReportDateRange.EndDate));
    }
    this.InspectionSearchText.ReportNo = this.InspectionSearchModel.ReportNo;
    this.InspectionSearchText.InspCountry = this.selectedLocation.selectedCountryName;
    this.InspectionSearchText.InspState = this.selectedLocation.selectedStateName;
    this.InspectionSearchText.InspCity = this.selectedLocation.selectedcityName;
    this.getReportList();
  }

  getPage(page: number) {
    this.isLoader = true;
    this.InspectionSearchModel.PageNumber = page;
    this.getReportList();
  }

  sorting(column: string) {
    this.InspectionSearchModel.OrderBy = column;
    if (this.InspectionSearchModel.Order == 'asc') {
      this.InspectionSearchModel.Order = 'desc';
    } else {
      this.InspectionSearchModel.Order = 'asc';
    }

    this.getReportList();
  }

  download(fileName: any, data: any, fileReport: any) {
    this.fileType = data;
    this.ReportNo = fileName.ReportNo;
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userDetail.EmailId = this.userDetail.EmailId;
    (<any>$('#DownloadFile')).modal('show');
    if (data == 'BookingForm') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileName.BookingForm.trim();
    }
    else if (data == 'Report') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileReport.trim();
    }
    if (data == 'GCC') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileReport.trim();
    }
    if (data == 'SUP') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileReport.trim();
    }
    if (data == 'Invoice') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileReport.trim();
    }
    // else {
    //   this.bulkEmailSendObject.type = "Invoice";
    //   this.DownloadfileName = data.trim();
    // }
    this.checkedReportNo = [];
    this.ccEmails = [];
    this.remarks = [];
    this.TrfPop = false;
    this.ReportPop = false;
    this.InvoicePop = false;
    this.showDownloadFiles = false;
    (<any>$('.CheckPop')).prop('checked', false)
  }

  downloadFile() {

    var that = this;
    if (that.showDownloadFiles && that.userDetail.EmailId == "") {
      swal('', "Please enter emailid");
      return;
    } else {
      that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
    }
    if (that.showDownloadFiles == true) {
      that.bulkEmailSendObject.ReportNo = that.ReportNo;
      that.bulkEmailSendObject.Subject = "MTS - Test Report No.: " + that.ReportNo;
      that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
      that.bulkEmailSendObject.FileName = that.DownloadfileName;
      that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
      that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
      that.bulkEmailSendObject.FileStatus = 'D';
      that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
      that.bulkEmailSendObject.FileType = that.fileType;
      that.bulkEmailSendObject.BusinessCategoryId = that.userCredential.BusinessCategoryId;
      that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
        response => {
          if (response.IsSuccess) {
            swal('', response.Message);
            that.checkedReportNo = [];
            that.ccEmails = [];
            that.remarks = [];
            that.userDetail.EmailId = "";
            (<any>$('#DownloadFile')).modal('hide');
          }
          else {
            swal('', response.Message);
          }
        }
      )
    }
    else {
      let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
      window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "&fileType=" + this.fileType + "&businessCategoryId=" + this.userCredential.BusinessCategoryId;
    }
  }

  uploadInvoice(report) {

    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.fileName = "";
    this.fileToUpload = "";
    this.invoiceNumber = "";
    this.invoiceValue = "";
    this.ReportNo = report.ReportNo;
    this.checkedReportNo = [];
    this.ccEmails = [];
    this.remarks = [];
    this.TrfPop = false;
    this.ReportPop = false;
    this.InvoicePop = false;
    this.showDownloadFiles = false;
    (<any>$('.CheckPop')).prop('checked', false)
  }

  UploadBookingForm(report) {
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.fileName = "";
    this.fileToUpload = "";
    this.ReportNo = report.ReportNo;
    this.checkedReportNo = [];
    this.ccEmails = [];
    this.remarks = [];
    this.TrfPop = false;
    this.ReportPop = false;
    this.InvoicePop = false;
    this.showDownloadFiles = false;
    (<any>$('.CheckPop')).prop('checked', false)
  }

  //GK: this function is calling from Upload GCC button click from grid.
  uploadGCC(report) {
    let that = this;
    that.ReportNo = report.ReportNo;
    this.gccFilesUpload = [];
    this.fileToUpload = [];
    this.fileName = "";
    let index = this.reportList.findIndex(i => i.ReportNo == that.ReportNo);
    this.uploadedGCCCount = this.reportList[index].GCC.length;
  }

  //GK: this function is calling from Upload Supporting Document button click from grid.
  uploadSupportingDocument(report) {
    let that = this;
    that.ReportNo = report.ReportNo;
    this.supportingFilesUpload = [];
    this.fileToUpload = [];
    this.fileName = "";
  }

  //GK: this method is calling from plus icon click on upload GCC file popup.
  addSupportingFile() {
    if (this.fileToUpload) {
      for (let i = 0; i < this.fileToUpload.length; i++) {
        if (this.supportingFilesUpload.length < 5) {
          this.supportingFilesUpload.push(this.fileToUpload[i]);
        }
        else {
          swal('', 'Maximum 5 files can be uploaded at a time.');
        }
      }
    }
    this.fileToUpload = [];
    this.fileName = "";
  }

  deleteSupportingFile(index) {
    this.supportingFilesUpload.splice(index, 1);
  }

  uploadSUPFile() {
    var that = this;
    var fileNames = '';
    that.supportingFilesUpload.forEach((file) => {
      fileNames = fileNames + file.name;
    });
    if (this.fileToUpload && (that.supportingFilesUpload.length == 0)) {
      for (let i = 0; i < this.fileToUpload.length; i++) {
        if (that.supportingFilesUpload.length <= 5) {
          fileNames = fileNames + this.fileToUpload[i].name;
          that.supportingFilesUpload.push(this.fileToUpload[i]);
        }
        else {
          swal('', 'Maximum 5 files can be uploaded at a time.');
        }
      }
    }

    this.fileUploadRequest(BaseUrl + 'ManageDocuments/UploadSupportingDocuments', that.supportingFilesUpload, 'SUP').then(
      (result: any) => {
        ;
        if (!result.IsSuccess) {
          if (!result.noAPICall == true) {
            this.supportingFilesUpload = [];
            this.fileToUpload = [];
          }
          swal('', result.Message);
        }
        else {
          this.supportingFilesUpload = [];
          this.fileToUpload = [];
          this.getReportList();
          (<any>$('#uploadSupportingFile')).modal('hide');
          swal('', result.Message);
        }
        this.isLoader = false;
      },
      (error) => {
        this.isLoader = false;
      }
    )
  }


  //Divyanshu: this method get all details based on report number

  uploadReport(report) {
    //
    this.isLoader = true;
    this.failReason = "";
    var that = this;
    that.testFlag1.flag = true;
    that.vendorId = report.Vendor;
    var LabEmail = "";
    this.dataService.get('ManageDocuments/GetInspectionReportData?MasterJobNo=' + report.MasterJobNo + "&ReportNo=" + report.ReportNo).subscribe(
      response => {
        that.isLoader = false;
        that.isUploadReport = true;
        that.uploadedReportCount = response.ReportCount;
        that.report = response;
        that.report.MasterJobNo = report.MasterJobNo;
        that.ETRFNumber = that.report.ETRFNumber;
        that.failReason = that.report.FailReason;
        (<any>$('#uploadReport')).modal('show');
        response.InspectionDateFrom = new Date(response.InspectionDateFrom);
        response.InspectionDateTo = new Date(response.InspectionDateTo);
        if (response.InspectionDateTo == "0001-01-01") {
          response.InspectionDateTo = '';
        }
        that.emailObj.VendorEmailId = response.VendorEmailId == null ? "" : response.VendorEmailId;
        if (that.emailObj.VendorEmailId) {
          var y = that.emailObj.VendorEmailId.toLowerCase().split('/');
          if (y.length > 0) {
            y.forEach((val, i) => {
              if (val.includes('unknown')) {
                y.splice(i, 1);
              }
            })
            if (y.length > 0) {
              that.emailObj.VendorEmailId = y.join(',');
            }
            else {
              that.emailObj.VendorEmailId = y[0];
            }
          }
          else {
            if (that.emailObj.VendorEmailId.includes('unknown')) {
              that.emailObj.VendorEmailId = "";
            }
          }

        }

        // if (that.userCredential.CompanyId == '386')
        // {
        //     if (response.ClientEmail) {
        //         var z = response.ClientEmail.toLowerCase().split(',');
        //         if (z.length > 0) {
        //             z.forEach((val, i) => {
        //                 if (val.includes('unknown')) {
        //                     z.splice(i, 1);
        //                 }
        //             })
        //             if (z.length > 0) {
        //                 that.emailObj.CC = z.join(',');
        //             }
        //             else {
        //                 that.emailObj.CC = z[0];
        //             }
        //         }
        //         else  {
        //             if (response.ClientEmail.includes('unknown')) {
        //                 that.emailObj.CC = "";
        //             }
        //         }

        //     }

        // }
        // else {
        //     that.emailObj.CC = this.userDetail.EmailId == null ? "" : this.userDetail.EmailId;
        // }
        // if (response.LaboratoryEmailId) {
        //     var y = response.LaboratoryEmailId.toLowerCase().split(',');
        //     if (y.length > 0) {
        //         y.forEach((val, i) => {
        //             if (val.includes('unknown')) {
        //                 y.splice(i, 1);
        //             }
        //         })
        //         if (y.length > 0) {
        //             response.LaboratoryEmailId = y.join(',');
        //         }
        //         else {
        //             response.LaboratoryEmailId = y[0];
        //         }
        //     }
        //     else {
        //         if (response.LaboratoryEmailId.includes('unknown')) {
        //             response.LaboratoryEmailId = '';
        //         }
        //     }
        //     that.emailObj.CC = that.emailObj.CC + ',' + response.LaboratoryEmailId;
        // }
        that.emailObj.CC = this.userDetail.EmailId == null ? "" : this.userDetail.EmailId;
        var link = window.location.href.split('/#/')[0];
        this.emailObj.WebLink = link + '/#/web-link/order-inspection/' + btoa(report.ReportNo);
        this.emailObj.Subject = "MTS - Inspection Report No.: " + report.ReportNo;
      }
    );


    this.fileName = '';
    this.ReportNo = report.ReportNo;
    this.checkedReportNo = [];
    this.ccEmails = [];
    this.remarks = [];
    this.TrfPop = false;
    this.ReportPop = false;
    this.InvoicePop = false;
    this.showDownloadFiles = false;
    (<any>$('.CheckPop')).prop('checked', false);
  }

  //GK: this method is calling from plus icon click on upload GCC file popup.
  addGCCFile() {
    let index = this.reportList.findIndex(i => i.ReportNo == this.ReportNo);
    let reportData = this.reportList[index];
    let alreadyExistingGCCFileCount = reportData.GCC.length;
    let newGCCFileCount = this.fileToUpload.length;
    let alreadtAddedGCCFileCOunt = this.gccFilesUpload.length;

    if (5 - (newGCCFileCount + alreadyExistingGCCFileCount + alreadtAddedGCCFileCOunt) >= 0) {
      if (this.fileToUpload) {
        for (let i = 0; i < this.fileToUpload.length; i++) {
          if (this.gccFilesUpload.length < 5) {
            this.gccFilesUpload.push(this.fileToUpload[i]);
          }
          else {
            swal('', 'Maximum 5 files can be uploaded at a time.');
          }
        }
      }
      this.fileToUpload = [];
      this.fileName = "";
    }
    else {
      swal('', 'Maximum ' + (5 - alreadyExistingGCCFileCount).toString() + ' files can be uploaded.');
      this.fileToUpload = [];
      this.fileName = "";
    }
  }

  //GK: Upalod all GCC file on submit.
  uploadGCCReportFile() {
    var that = this;
    var fileNames = '';
    this.gccFilesUpload.forEach((file) => {
      fileNames = fileNames + file.name;
    })

    if (this.fileToUpload && this.gccFilesUpload.length == 0) {
      for (let i = 0; i < this.fileToUpload.length; i++) {
        if (this.gccFilesUpload.length < 5) {
          fileNames = fileNames + this.fileToUpload[i].name;
          this.gccFilesUpload.push(this.fileToUpload[i]);
        }
        else {
          swal('', 'Maximum 5 files can be uploaded at a time.');
        }
      }
    }
    else {
      this.addGCCFile();
    }

    if (this.validateFileExtension(fileNames, 'GCC')) {
      this.fileUploadRequest(BaseUrl + 'ManageDocuments/UploadGCC', this.gccFilesUpload, 'GCC').then(
        (result: any) => {
          if (!result.IsSuccess) {
            if (!result.noAPICall == true) {
              this.gccFilesUpload = [];
              this.fileToUpload = [];
            }
            swal('', result.Message);
          }
          else {
            this.gccFilesUpload = [];
            this.fileToUpload = [];
            this.getReportList();
            (<any>$('#uploadGCCFile')).modal('hide');
            swal('', result.Message);
          }
          this.isLoader = false;
        },
        (error) => {
          this.isLoader = false;
        }
      )
    }
    this.gccFilesUpload = [];
    this.fileToUpload = [];
  }

  //rohit email
  //Divyanshu:Upload Report
  submitUploadReport(reportNo: any) {
    var that = this;
    var fileNames = '';
    this.reportFilesUpload.forEach((file) => {
      fileNames = fileNames + file.name;
    })
    if (this.fileToUpload && this.reportFilesUpload.length == 0) {
      for (let i = 0; i < this.fileToUpload.length; i++) {
        if (this.reportFilesUpload.length < 5) {
          fileNames = fileNames + this.fileToUpload[i].name;
          this.reportFilesUpload.push(this.fileToUpload[i]);
        }
        else {
          swal('', 'Maximum 5 files can be uploaded at a time.');
        }
      }
    }
    if (this.validateFileExtension(fileNames, 'REPORT')) {
      that.isLoader = true;
      this.fileUploadRequest(BaseUrl + 'ManageDocuments/UploadInspectionReport', that.reportFilesUpload, 'Report').then(
        (result: any) => {
          this.ETRFNumber = '';
          if (!result.IsSuccess) {

            if (!result.noAPICall == true) {
              that.reportFilesUpload = [];
              that.fileToUpload = [];
            }
            swal('', result.Message);
          }
          else {
            that.reportFilesUpload = [];
            that.fileToUpload = [];
            this.ETRFNumber = '';
            this.getReportList();
            (<any>$('#uploadReport')).modal('hide');
            swal('', result.Message);
          }
          this.isLoader = false;
        },
        (error) => {
          this.isLoader = false;
        }
      )
    }
    this.reset();
  }

  //Divyanshu: Upload Invoice

  //rohit: Update send Email
  submitUploadInvoice() {

    var that = this;
    (<any>$('#uploadInvoice')).val('');
    (<any>$('#uploadreport')).val('');
    (<any>$('#inputfileValue')).val('');

    if (that.InvoicePop && that.userDetail.EmailId == "") {
      swal('', "Please enter emailid");
      return;
    } else {
      if (that.InvoicePop) {
        that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
        that.bulkEmailSendObject.ReportNo = that.ReportNo;
        that.bulkEmailSendObject.Subject = "MTS - Test Report : " + that.ReportNo;
        that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
        that.bulkEmailSendObject.FileName = "";
        that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
        that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
        that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
        that.bulkEmailSendObject.CreatedDate = '01-01-1900';
        that.bulkEmailSendObject.ModifiedDate = '01-01-1900';
      }
      else {
        that.bulkEmailSendObject.ReportNo = that.ReportNo;
        that.bulkEmailSendObject.Subject = "MTS - Test Report : " + that.ReportNo;
        that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
        that.bulkEmailSendObject.FileName = "";
        that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
        that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
        that.bulkEmailSendObject.CreatedDate = '01-01-1900';
        that.bulkEmailSendObject.ModifiedDate = '01-01-1900';
      }
    }
    if (this.invoiceValue && this.invoiceNumber && this.validateFileExtension(this.fileName, 'INVOICE')) {
      this.fileUploadRequest(BaseUrl + 'ManageDocuments/UploadInvoice', this.fileToUpload, 'Invoice').then((result: any) => {

        if (!result.IsSuccess) {
          swal('', result.Message);
          //rohit
        }
        else {
          (<any>$('#uploadInvoice')).modal('hide');
          swal('', result.Message);
          // }
          this.getReportList();
        }
        this.isLoader = false;

      }, (error) => {
        this.isLoader = false;
      });
    }
    else {
      if (!this.fileName && !this.invoiceNumber && !this.invoiceValue) {
        swal('', 'Please Fill Invoice Number, Invoice Value and Choose file');
      }
      if (this.fileName && !this.invoiceNumber || !this.invoiceValue) {
        swal('', 'Please fill Invoice Number Or Invoice Value');
      }
      if (this.invoiceNumber && !this.fileName || !this.invoiceValue) {
        swal('', 'Please Choose a file or fill Invoice Value');
      }
      if (this.invoiceValue && !this.fileName || !this.invoiceNumber) {
        swal('', 'Please Choose a file  or fill Invoice Number');
      }
    }
    this.reset();

  }

  //Divyanshu:It deals with uploading TRF document
  submitUploadBookingForm() {
    var that = this;
    (<any>$('#uploadInvoice')).val('');
    (<any>$('#uploadreport')).val('');
    (<any>$('#inputfileValue')).val('');
    if (that.TrfPop && that.userDetail.EmailId == "") {

      swal('', "Please enter emailid");

      return;
    } else {
      that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
    }


    if (this.validateFileExtension(this.fileName, 'BookingForm')) {
      (<any>$('#UploadBookingForm')).modal('hide');

      this.fileUploadRequest(BaseUrl + 'ManageDocuments/UploadBookingForm', this.fileToUpload, 'BookingForm').then(
        (result: any) => {
          if (!result.IsSuccess) {
            swal('', result.Message)
          }
          else {
            if (this.TrfPop == true) {
              that.bulkEmailSendObject.ReportNo = that.ReportNo;
              that.bulkEmailSendObject.Subject = "MTS - Test Report : " + that.ReportNo;
              that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
              that.bulkEmailSendObject.FileName = result.Data;
              that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
              that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
              that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
              that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
                response => {
                  if (response.IsSuccess) {
                    swal('', response.Message);
                    that.checkedReportNo = [];
                    that.ccEmails = [];
                    that.remarks = [];
                    that.userDetail.EmailId = "";
                  }
                  else {
                    swal('', response.Message);
                  }
                }
              )
            }

            this.getReportList();
          }
          this.isLoader = false;
        }, (error) => {
        });
    }
    this.reset();
  }

  uploadTypeChange() {
    this.bulkFileUpload = [];
    this.bulkFileDisplay = [];
    for (let i = 0; i < this.checkedReportArr.length; i++) {
      this.tempBulkUploadFiles.push({
        "reportNo": this.checkedReportArr[i].ReportNo,
        "files": [],
        "invoiceNo": "",
        "invoiceValue": "",
        "ETRFNumber": this.checkedReportArr[i].ETRFNumber
      })
      this.bulkUploadFiles.push({
        "reportNo": this.checkedReportArr[i].ReportNo,
        "files": [],
        "invoiceNo": "",
        "invoiceValue": "",
        "ETRFNumber": this.checkedReportArr[i].ETRFNumber
      })
    }

  }

  bulkInvoiceChange(i: any, e: any) {
    this.tempBulkUploadFiles[i].invoiceNo = e.target.value;
    this.bulkUploadFiles[i].invoiceNo = e.target.value;

  }
  bulkInvoiceValueChange(i: any, e: any) {
    this.tempBulkUploadFiles[i].invoiceValue = e.target.value;
    this.bulkUploadFiles[i].invoiceValue = e.target.value;
  }
  bulkReportChange(i: any, e: any, reportNo: string) {
    this.tempBulkUploadFiles[i].ETRFNumber = e.target.value;
    this.tempBulkUploadFiles[i].reportNo = reportNo;
    this.bulkUploadFiles[i].ETRFNumber = e.target.value;
    this.bulkUploadFiles[i].reportNo = reportNo;
  }

  submitBatchUploadFile(calledFrom: any) {
    var thisObj = this;
    let flag1 = false;

    if (calledFrom == "" || calledFrom == undefined) {
      swal('', "Please select upload type.");
      return;
    }
    if (calledFrom == 'report') {

      thisObj.uploadBatchFiles(0, 'REPORT');
    }
    if (calledFrom != 'report') {

      for (let i = 0; i < thisObj.bulkUploadFiles.length; i++) {
        if (thisObj.bulkUploadFiles[i] != undefined && thisObj.bulkUploadFiles[i].files.length > 0) {
          flag1 = true;
          break;
        }
      }
      if (!flag1) {
        swal('', "Please choose atleast one file.");
        return;
      }
    }
    if (calledFrom == 'BookingForm') {
      thisObj.uploadBatchFiles(0, 'BookingForm');
    }
    if (calledFrom == 'invoice') {

      for (let i = 0; i < thisObj.bulkUploadFiles.length; i++) {
        if (thisObj.bulkUploadFiles[i] != undefined && thisObj.bulkUploadFiles[i].files.length > 0 &&
          thisObj.bulkUploadFiles[i].invoiceNo == "") {
          swal('', "Please enter invoice no for Report No : " + thisObj.bulkUploadFiles[i].reportNo);
          return;
        }
        if (thisObj.bulkUploadFiles[i] != undefined && thisObj.bulkUploadFiles[i].files.length > 0 &&
          thisObj.bulkUploadFiles[i].invoiceValue == "") {
          swal('', "Please enter invoice Value for Report No : " + thisObj.bulkUploadFiles[i].reportNo);
          return;
        }

        if (thisObj.bulkUploadFiles[i] != undefined && thisObj.bulkUploadFiles[i].invoiceNo != "" &&
          thisObj.bulkUploadFiles[i].files.length < 0) {
          swal('', "Please choose file for Report No : " + thisObj.bulkUploadFiles[i].reportNo);
          return;
        }
      }

      thisObj.uploadBatchFiles(0, 'INVOICE');

    }
    // if (calledFrom == 'report') {


    //     thisObj.uploadBatchFiles(0, 'REPORT');
    // }


  }

  uploadBatchFiles(index: any, calledFrom: any) {
    //  //debugger;
    var that = this;
    var formData: any = new FormData();
    if (index >= this.bulkUploadFiles.length) {
      that.getReportList();
      this.closeBatchUpload();
    }
    if (calledFrom == 'REPORT') {
      var objClient = {
        EmailID: this.userDetail.EmailId,
        CompanyName: this.userDetail.CompanyName
      }
      formData.append("CreatedBy", that.userCredential.UserId);
      formData.append("EmailInfo", JSON.stringify({}));
      formData.append("ClientInfo", JSON.stringify(objClient));
      formData.append('BusinessCategoryId', this.userCredential.BusinessCategoryId);
      var c = index;
      while (c < that.bulkUploadFiles.length) {
        if (this.bulkUploadFiles[c]) {
          formData.append("EtrfNo", this.bulkUploadFiles[c].ETRFNumber);
          formData.append("ReportNo", this.bulkUploadFiles[c].reportNo);
          for (let j = 0; j < this.bulkUploadFiles[c].files.length; j++) {
            formData.append('UploadReport-' + j, this.bulkUploadFiles[c].files[j]);
          }
          c++;
          that.isLoader = true;
          this.dataService.post('ManageDocuments/UploadInspectionReport', formData).subscribe(
            r => {
              that.isLoader = false;
              if (r.IsSuccess) {
                that.uploadBatchFiles(c, 'REPORT');
              }
              else {
                swal('', r.Message);
              }
            }
          )
          break;
        }
        else {
          c++;
        }
      }
    }
    if (calledFrom == 'INVOICE') {
      formData.append("CreatedBy", that.userCredential.UserId);
      formData.append('BusinessCategoryId', this.userCredential.BusinessCategoryId);
      var b = index;
      while (b < that.bulkUploadFiles.length) {
        if (this.bulkUploadFiles[b]) {
          formData.append("ReportNo", this.bulkUploadFiles[b].reportNo);
          formData.append("InvoiceNo", this.bulkUploadFiles[b].invoiceNo);
          formData.append("InvoiceValue", this.bulkUploadFiles[b].invoiceValue);
          for (let j = 0; j < this.bulkUploadFiles[b].files.length; j++) {
            formData.append('UploadInvoice-' + j, this.bulkUploadFiles[b].files[j]);
          }
          b++;
          that.isLoader = true;
          this.dataService.post('ManageDocuments/UploadInvoice', formData).subscribe(
            r => {
              that.isLoader = false;
              if (r.IsSuccess) {
                that.uploadBatchFiles(b, 'INVOICE');
              }
              else {
                swal('', r.Message);
              }
            }
          )
          break;
        }
        else {
          b++;
        }
      }
    }
    if (calledFrom == 'BookingForm') {
      formData.append("CreatedBy", that.userCredential.UserId);
      var a = index;
      while (a < that.bulkUploadFiles.length) {
        if (this.bulkUploadFiles[a]) {
          formData.append("ReportNo", this.bulkUploadFiles[a].reportNo);
          for (let j = 0; j < this.bulkUploadFiles[a].files.length; j++) {
            formData.append('BookingForm', this.bulkUploadFiles[a].files[j]);
            formData.append('BusinessCategoryId', this.userCredential.BusinessCategoryId);
          }
          a++;
          that.isLoader = true;
          this.dataService.post('ManageDocuments/UploadBookingForm', formData).subscribe(
            r => {
              that.isLoader = false;
              if (r.IsSuccess) {
                that.uploadBatchFiles(a, 'BookingForm');
              }
              else {
                swal('', r.Message);
              }
            }
          )
          break;
        }
        else {
          a++;
        }
      }
    }
  }
  closeBatchUpload() {
    this.uploadType = "";
    this.bulkFileUpload = [];
    this.bulkUploadFiles = [];
    this.bulkFileDisplay = [];
    this.tempBulkUploadFiles = [];
    this.checkedReportArr = [];
    (<any>$(".report-table input[type=checkbox]")).prop('checked', false);
    (<any>$('#batchUpload')).modal('hide');
  }

  bulkFileChangeEvent(event: any, calledFrom: any, count: any, index: any, reportNo: any) {
    var fileNames = [];
    if (calledFrom == 'REPORT') {
      var canUploadFile = 15 - count;
      if (event.target.files[0].name && event.target.files) {
        var that = this;
        var bulkFileCount = event.target.files.length;

        if (bulkFileCount <= canUploadFile) {
          if (bulkFileCount <= 5) {
            for (let i = 0; i < event.target.files.length; i++) {
              var result = this.validateFileExtension(event.target.files[i].name, 'REPORT');
              if (result == true) {
                // if (event.target.files[i].name.substring(0, 2) != reportNo.substring(0, 2)) {
                //     swal('', 'Please choose correct report.');
                //     return;
                // }
                // else {
                fileNames.push(event.target.files[i].name);
                //  }
                // fileNames.push(event.target.files[i].name);
                if (this.bulkUploadFiles.length && this.bulkUploadFiles[index] && this.bulkUploadFiles[index].ETRFNumber) {
                  this.tempBulkUploadFiles[index].ETRFNumber = this.bulkUploadFiles[index].ETRFNumber;
                }
                this.tempBulkUploadFiles[index].files.push(event.target.files[i]);
                this.tempBulkUploadFiles[index].reportNo = reportNo;

              }
              else {
                fileNames = [];
                swal('', 'Please Upload only .pdf, .doc, .docx, .xls, .xlsx, .pptx files');
                break;
              }


            }

            if (bulkFileCount) {
              this.bulkFileUpload[index] = fileNames.join(', ');
            }

          }
          else {
            swal('', 'Maximum 5 files can be upload at a time.');
          }
        }
        else {
          swal('', 'You can only upload only ' + canUploadFile + ' files.');
        }
      }
    }
    if (calledFrom == 'INVOICE') {
      var canUploadFile = 4 - count;
      if (event.target.files[0].name && event.target.files) {
        var that = this;
        var bulkFileCount = event.target.files.length;

        if (bulkFileCount <= canUploadFile) {
          if (bulkFileCount <= 4) {
            for (let i = 0; i < event.target.files.length; i++) {
              var result = this.validateFileExtension(event.target.files[i].name, 'INVOICE');
              if (result == true) {
                fileNames.push(event.target.files[i].name);
                if (this.bulkUploadFiles.length && this.bulkUploadFiles[index] && this.bulkUploadFiles[index].invoiceNo) {
                  this.tempBulkUploadFiles[index].invoiceNo = this.bulkUploadFiles[index].invoiceNo;
                }
                if (this.bulkUploadFiles.length && this.bulkUploadFiles[index] && this.bulkUploadFiles[index].invoiceNo) {
                  this.tempBulkUploadFiles[index].invoiceValue = this.bulkUploadFiles[index].invoiceValue;

                }
                this.tempBulkUploadFiles[index].files.push(event.target.files[i]);
                this.tempBulkUploadFiles[index].reportNo = reportNo;
              } else {
                fileNames = [];
                swal('', 'Please Upload only .pdf, .doc, .docx, .xls, .xlsx, .pptx files');
                break;
              }
            }

            if (bulkFileCount) {
              this.bulkFileUpload[index] = fileNames.join(', ');
            }

          }
          else {
            swal('', 'Maximum 4 files can be upload at a time.');
          }
        }
        else {
          swal('', 'You can only upload only ' + canUploadFile + ' files.');
        }
      }
    }
    if (calledFrom == 'BookingForm') {
      var canUploadFile = 1 - count;
      if (event.target.files[0].name && event.target.files) {
        var that = this;
        var bulkFileCount = event.target.files.length;

        if (bulkFileCount <= canUploadFile) {
          if (bulkFileCount <= 1) {
            for (let i = 0; i < event.target.files.length; i++) {
              var result = this.validateFileExtension(event.target.files[i].name, 'BookingForm');
              if (result == true) {
                fileNames.push(event.target.files[i].name);
                this.tempBulkUploadFiles[index].files.push(event.target.files[i]);
                this.tempBulkUploadFiles[index].reportNo = reportNo;
              }
              else {
                fileNames = [];
                swal('', 'Please Upload only .pdf, .doc, .docx, .xls, .xlsx, .pptx files');
                break;
              }
            }

            if (bulkFileCount) {
              this.bulkFileUpload[index] = fileNames.join(', ');
            }

          }
          else {
            swal('', 'Maximum 1 files can be upload at a time.');
          }
        }
        else {
          swal('', 'You can only upload only ' + canUploadFile + ' files.');
        }
      }
    }
  }

  bulkAddFiles(e: any, index: any, count: any, calledFrom: any) {
    var that = this;
    if (!this.bulkUploadFiles[index]) {
      this.bulkUploadFiles[index] = {
        "reportNo": "",
        "files": [],
        "invoiceNo": "",
        "invoiceValue": "",
        "ETRFNumber": "",
      }
    }
    if (this.bulkFileUpload[index] && this.bulkFileUpload[index].length > 0) {
      if (!this.bulkFileDisplay[index]) {
        var x = count;
      }
      else {
        var x = this.bulkFileDisplay[index].length + count;
      }

      if (calledFrom == 'REPORT') {
        if (!this.bulkFileDisplay[index]) {
          this.bulkFileDisplay[index] = [];
        }

        var y = this.bulkFileUpload[index].split(', ');
        y.forEach((data, i) => {
          if (x < 15) {
            if (that.bulkFileDisplay[index].length < 5) {
              that.bulkFileDisplay[index].push(data);
              that.bulkUploadFiles[index].reportNo = that.tempBulkUploadFiles[index].reportNo;
              that.bulkUploadFiles[index].files.push(that.tempBulkUploadFiles[index].files[i]);
              that.bulkUploadFiles[index].ETRFNumber = that.tempBulkUploadFiles[index].ETRFNumber;
            } else {
              swal('', 'Maximum 5 files can be uploaded at a time.');

            }

            x++;
          }
          else {
            swal('', 'You uploaded maximum limit of files');
            return;
          }
        })

      }
      if (calledFrom == 'BookingForm') {
        if (!this.bulkFileDisplay[index]) {
          this.bulkFileDisplay[index] = [];
        }

        var y = this.bulkFileUpload[index].split(', ');
        y.forEach((data, i) => {
          if (x < 1) {
            that.bulkFileDisplay[index].push(data);
            that.bulkUploadFiles[index].reportNo = that.tempBulkUploadFiles[index].reportNo;
            that.bulkUploadFiles[index].files.push(that.tempBulkUploadFiles[index].files[i]);
            x++;
          }
          else {
            swal('', 'You uploaded maximum limit of files');
            return;
          }
        })
      }
      if (calledFrom == 'INVOICE') {
        if (!this.bulkFileDisplay[index]) {
          this.bulkFileDisplay[index] = [];
        }
        else {
          if (this.bulkFileDisplay[index].length >= 1) {
            this.bulkFileUpload[index] = [];
            swal('', 'you can upload one at a time.');
            return;
          }
        }
        var y = this.bulkFileUpload[index].split(', ');
        y.forEach((data, i) => {
          if (x < 4) {
            that.bulkFileDisplay[index].push(data);
            that.bulkUploadFiles[index].reportNo = that.tempBulkUploadFiles[index].reportNo;
            that.bulkUploadFiles[index].files.push(that.tempBulkUploadFiles[index].files[i]);
            that.bulkUploadFiles[index].invoiceNo = that.tempBulkUploadFiles[index].invoiceNo;
            that.bulkUploadFiles[index].invoiceValue = that.tempBulkUploadFiles[index].invoiceValue;

            x++;
          }
          else {
            swal('', 'You uploaded maximum limit of files.');
            return;
          }
        })
      }

      this.bulkFileUpload[index] = [];
      this.tempBulkUploadFiles[index] = { "reportNo": "", "files": [], "invoiceNo": "", "ETRFNumber": "" };
    }
    else {
      swal('', 'Please choose file to upload.');
    }

  }

  //Divyanshu: Assign dynamically fileName and file which is going to upload
  fileChangeEvent(event: any, calledFrom: any) {
    if (calledFrom == 'REPORT') {
      var fileNames = [];
      var canUploadFile = 15 - this.uploadedReportCount;
      if (event.target.files.length > 0 && event.target.files[0].name) {
        var that = this;
        this.filesCount = event.target.files.length;

        if (this.filesCount <= canUploadFile) {
          if (this.filesCount <= 5) {
            for (let i = 0; i < event.target.files.length; i++) {
              // if (event.target.files[i].name.toString().substring(0, 2) != that.ReportNo.toString().substring(0, 2)) {
              //     swal('', 'Please upload correct report.');
              //     return;
              // }
              // else {
              fileNames.push(event.target.files[i].name);
              // }
            }
            if (this.filesCount) {
              this.fileName = fileNames.join(', ');
            }
            this.fileToUpload = event.target.files;

          }
          else {
            swal('', 'Maximum 5 files can be upload at a time.');
          }
        }
        else {
          swal('', 'You can only upload only ' + canUploadFile + ' files.');
        }
      }
    }
    else if (calledFrom == 'GCC' || calledFrom == 'SUP') {
      this.filesCount = event.target.files.length;
      var fileNames = [];

      if (this.filesCount <= 5) {
        for (let i = 0; i < event.target.files.length; i++) {
          fileNames.push(event.target.files[i].name);
        }
        if (this.filesCount) {
          this.fileName = fileNames.join(', ');
        }
        this.fileToUpload = event.target.files;
      }
      else {
        swal('', 'Maximum 5 files can be upload at a time.');
      }

    }
    else {
      if (event.target.files.length > 0 && event.target.files[0].name) {
        this.fileName = event.target.files[0].name;
        this.fileToUpload = event.target.files[0];
      }
    }
  }

  deleteReportFile(index: any) {
    this.reportFilesUpload.splice(index, 1);
  }

  deleteGCCFile(index) {
    this.gccFilesUpload.splice(index, 1);
  }

  deleteBatchReportFile(index: any, innerIndex: any) {

    this.bulkFileDisplay[index].splice(innerIndex, 1);
    this.bulkUploadFiles[index].files.splice(innerIndex, 1);
  }


  addUploadReport() {

    if (this.fileName) {
      this.fileListNames = this.fileName.split(', ');
    }
    if (this.fileToUpload) {
      for (let i = 0; i < this.fileToUpload.length; i++) {
        if (this.reportFilesUpload.length < 5) {
          this.reportFilesUpload.push(this.fileToUpload[i]);
        }
        else {
          swal('', 'Maximum 5 files can be uploaded at a time.');
        }
      }
    }

    this.fileToUpload = [];
    this.fileName = "";
  }

  /*Divyanshu: following method is responsile for making upload request to Api Url
   and also take care of what data should be build based on fileType
   */
  fileUploadRequest(url: string, files: Array<File>, fileType: string) {

    let thisObj = this;
    thisObj.isLoader = true;
    var formData: any = new FormData();
    let xhr = new XMLHttpRequest();
    if (fileType == 'BookingForm') {
      formData.append("BookingForm", thisObj.fileToUpload);
      formData.append("ReportNo", thisObj.ReportNo);
      formData.append("CreatedBy", thisObj.userCredential.UserId);
      formData.append('BusinessCategoryId', thisObj.userCredential.BusinessCategoryId);
    }

    if (fileType == 'Invoice') {
      formData.append("UploadInvoice", thisObj.fileToUpload);
      formData.append("ReportNo", thisObj.ReportNo);
      formData.append("InvoiceNo", thisObj.invoiceNumber);
      formData.append("InvoiceValue", thisObj.invoiceValue);
      formData.append("CreatedBy", thisObj.userCredential.UserId);
      formData.append('BusinessCategoryId', thisObj.userCredential.BusinessCategoryId);
      formData.append("EmailDetails", JSON.stringify(this.bulkEmailSendObject));
    }
    if (fileType == 'Report') {
      var objClient = {
        EmailID: this.userDetail.EmailId,
        CompanyName: this.userDetail.CompanyName,
      }
      for (let i = 0; i < files.length; i++) {
        formData.append('UploadReport-' + i, files[i]);
      }
      formData.append("ReportNo", thisObj.ReportNo);
      formData.append("CreatedBy", thisObj.userCredential.UserId);
      formData.append('BusinessCategoryId', thisObj.userCredential.BusinessCategoryId);
      formData.append('EtrfNo', this.ETRFNumber);
      formData.append('FailReason', thisObj.failReason);


      if (this.ReportPop == true) {
        this.emailObj.VendorEmailId == "abc@gmail.com"
        if (this.emailObj.VendorEmailId == "" && this.emailObj.ManufacturerEmailId == "" && this.emailObj.LaboratoryEmailId == "" && this.emailObj.CC == "") {
          thisObj.isLoader = false;
          return new Promise((resolve, reject) => {
            resolve({ Message: 'Please enter any emailid', noAPICall: true });
          })
        } else {
          formData.append("EmailInfo", JSON.stringify(thisObj.emailObj));
        }

      } else {
        formData.append("EmailInfo", JSON.stringify({}));
      }
      formData.append("ClientInfo", JSON.stringify(objClient));
    }

    if (fileType == 'GCC') {
      for (let i = 0; i < files.length; i++) {
        formData.append('UploadGCC-' + i, files[i]);
      }
      formData.append("ReportNo", thisObj.ReportNo);
      formData.append("CreatedBy", thisObj.userCredential.UserId);
      formData.append('BusinessCategoryId', thisObj.userCredential.BusinessCategoryId);
    }

    if (fileType == 'SUP') {
      for (let i = 0; i < files.length; i++) {
        formData.append('UploadSUP-' + i, files[i]);
      }
      formData.append("ReportNo", thisObj.ReportNo);
      formData.append("CreatedBy", thisObj.userCredential.UserId);
      formData.append('BusinessCategoryId', thisObj.userCredential.BusinessCategoryId);
    }

    return new Promise((resolve, reject) => {

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            (<any>$('#uploadInvoice')).val('');
            (<any>$('#uploadreport')).val('');
            (<any>$('#inputfileValue')).val('');
            (<any>$('.CheckPop')).prop('checked', false);
            thisObj.reportFilesUpload = [];
            resolve(JSON.parse(xhr.response))
          }
          else {
            (<any>$('#uploadInvoice')).val('');
            (<any>$('#uploadreport')).val('');
            (<any>$('#inputfileValue')).val('');
            (<any>$('.CheckPop')).prop('checked', false)
            reject(JSON.parse(xhr.response));
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.setRequestHeader('Token', this.userDetail.AuthToken)
      xhr.send(formData);
    })
  }

  //Get Report List
  getReportList() {
    this.isLoader = true;
    this.dataService.post('ManageDocuments/GetIspectionOrderList', this.InspectionSearchModel).subscribe(
      response => {
        this.isLoader = false;
        this.currentPageNumber = this.InspectionSearchModel.PageNumber;
        if (response.IsSuccess) {
          this.reportList = response.ReportList;
          this.totalItem = Math.ceil(response.TotalRecords / this.InspectionSearchModel.PageSize);
        }
      }
    );
  }

  // Change event of Date Range Picker
  onDateRangeChanged(event: any) {
    this.dateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
    this.dateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
  }

  onDueDateRangeChanged(event: any) {
    this.ReportDateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
    this.ReportDateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
  }

  reset() {
    this.fileUploader.nativeElement.value = null;
  }

  resetBatchFile(e: any) {
    e.srcElement.value = "";
  }

  resetFile() {

    this.fileName = "";
    this.fileToUpload = "";
    this.checkedReportNo = [];
    this.ccEmails = [];
    this.remarks = [];
    this.TrfPop = false;
    this.ReportPop = false;
    this.InvoicePop = false;
    this.showDownloadFiles = false;
    this.emailObj.Comments = "";


    (<any>$('#uploadInvoice')).val('');
    (<any>$('#uploadreport')).val('');
    (<any>$('#inputfileValue')).val('');
    (<any>$('.CheckPop')).prop('checked', false);
    this.reportFilesUpload = [];
  }

  validateFileExtension(fileName, calledFrom): boolean {

    if (calledFrom == 'REPORT') {
      if (fileName) {
        var fileNames = fileName.split(', ');
        var result;
        fileNames.forEach((file) => {
          if (file) {
            let extension = fileName.substr(fileName.lastIndexOf('.') + 1);
            if (['pdf', 'doc', 'docx', 'xls', 'xlsx'].indexOf(extension.toLowerCase()) > -1) {
              result = true;
            }
            else {
              result = false;
              return;
            }
          }
          else {
            swal('', 'Please Choose a file');
          }
        })
        if (result == true) {
          return true;
        }
        else {
          swal('', 'Please Upload only .pdf, .doc, .docx, .xls, .xlsx files');
          return false;
        }

      } else {
        //  swal('', 'Please Choose a file');
        return true;
      }

    }
    else {
      if (fileName) {
        let extension = fileName.substr(fileName.lastIndexOf('.') + 1);
        if (['pdf', 'doc', 'docx', 'xls', 'xlsx'].indexOf(extension.toLowerCase()) > -1) {
          return true;
        }
        else {
          swal('', 'Please Upload only .pdf, .doc, .docx, .xls, .xlsx files');
          return false;
        }
      }
      else {
        swal('', 'Please Choose a file');
      }
    }
  }

  handleCheckboxCheck(e: any, report: any) {
    if (e.target.checked) {
      if (this.checkedReportArr.length < 5) {
        this.checkedReportArr.push(report);
      }
      else {
        swal('', 'Maximum 5 files can be upload at a time.');
        (<any>$('#' + e.target.id)).prop('checked', false);
      }
    }
    else {
      let index = this.checkedReportArr.indexOf(report);
      this.checkedReportArr.splice(index, 1);

    }
  }

  batchUploadBtnClick() {
    if (this.checkedReportArr.length == 0) {
      swal('', 'Please choose atleast one report');
    }
    else {
      var obj = [];
      this.checkedReportArr.forEach(element => {
        obj.push({ ReportNo: element.ReportNo, ETRFNumber: "", disableFeild: false });
      });
      this.dataService.post('ManageDocuments/BatchEtrfNumber', obj).subscribe(
        response => {
          obj = [];
          obj = response;

          for (let i = 0; i < obj.length; i++) {
            let index = this.checkedReportArr.findIndex(y => y.ReportNo == obj[i].ReportNo)
            if (index > -1) {
              this.checkedReportArr[index].ETRFNumber = obj[i].ETRFNo;
              if (this.checkedReportArr[index].ETRFNumber != '') {
                this.checkedReportArr[index].disableFeild = true;
              }
            }
          }
          (<any>$('#batchUpload')).modal('show');
        });

    }
  }
  openVendorDilaog() {
    (<any>$("#vendorEmail")).modal('show');
    this.testFlag1.flag = true;
  }

  onCountryChange(CList) {
    if (CList.CountryId) {
      this.isLoader = true;
      this.selectedLocation.selectedCountryName = CList.CountryName
      var id = CList.CountryId
      this.stateList = [];
      this.cityList = [];
      this.sList = "";
      this.cList = "";
      var that = this;
      this.dataService.get('Master/GetStateList?CountryId=' + id).subscribe(
        r => {
          that.isLoader = false;
          that.stateList = r.StateList;
        }
      )
    } else {
      this.sList = "";
      this.cList = "";
      this.stateList = [];
      this.cityList = [];
    }
  }

  onStateChange(sList) {
    if (sList.StateId) {
      this.isLoader = true;
      this.selectedLocation.selectedStateName = sList.StateName;
      this.cList = "";
      this.cityList = [];
      var that = this;
      this.dataService.get('Master/GetCityList?StateId=' + sList.StateId).subscribe(
        r => {
          that.isLoader = false;
          that.cityList = r.CityList;
        }
      )
    } else {
      this.cList = "";
      this.cityList = [];
    }
  }

  onCityChange(cityList) {
    if (cityList.CityId) {
      this.selectedLocation.selectedcityName = cityList.CityName
    } else {
      this.cList = "";
    }
  }

}


