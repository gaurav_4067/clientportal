import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MdInputModule } from '@angular2-material/input';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { Ng2PaginationModule } from 'ng2-pagination';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { TooltipDirective } from 'ng2-tooltip-directive/components';

import { InspectionReportUploadComponent } from './inspection-report-upload.component';
import { BulkEmailModule } from 'app/bulk-email/bulk-email.module';
import { VendorEmailModule } from 'app/vendor-email/vendor-email.module';
import {TooltipModule} from "ngx-tooltip";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MdInputModule,
    MyDateRangePickerModule,
    Ng2PaginationModule,
    Ng2AutoCompleteModule,
    BulkEmailModule,
    VendorEmailModule,
    TooltipModule
  ],
  declarations: [InspectionReportUploadComponent],
  exports: [InspectionReportUploadComponent]
})
export class InspectionReportUploadModule { }
