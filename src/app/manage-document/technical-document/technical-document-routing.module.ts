import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TechnicalDocumentComponent } from 'app/manage-document/technical-document/technical-document.component';

const routes: Routes = [{
  path: '',
  component: TechnicalDocumentComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TechnicalDocumentRoutingModule { }
