import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataService, BaseUrl } from '../../mts.service';
import { SEARCH_MODEL } from './technical-document-model';
import { InjectService } from '../../injectable-service/injectable-service';
import { isUndefined, debug } from "util";
import { Http, Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router'
declare let swal: any;

@Component({
  selector: 'app-technical-document',
  templateUrl: './technical-document.component.html',
  styleUrls: ['./technical-document.component.css']
})
export class TechnicalDocumentComponent implements OnInit {
  @ViewChild('fileUpload') fileUploader: ElementRef;
  documentList: any;
  documentCategoryList: any;
  subdocumentCategorylist: any;
  model: any;
  private p: any;
  private totalItem: number;
  private itemPerPage: any;
  private dateRange: any;
  additionalSearch: any;
  ddocumentcategory: number;
  ddocumentsubcategory: number;
  defaultdocumentcategory: any;
  defaultdocumentsubcategory: any;

  /*Upload Document Parameters */
  ddUpload_DC: number;
  ddUpload_DSC: number;
  technicalSpecification: any;
  uploaddocumentname: any;
  description: any
  clientId: number;
  createdBy: number;
  filename: any;
  uploadfile: any;
  technicalDocumentObj: any;
  isLoader: boolean;
  DocumentName: string;
  userCredential: any;
  userDetail: any;

  constructor(private dataService: DataService, private injectService: InjectService, private http: Http, private route: ActivatedRoute, private router: Router) {
    this.model = SEARCH_MODEL;
    this.model.ClientId = this.clientId;
    this.model.PageSize = 10;
    this.model.PageNumber = 1;
    this.model.OrderBy = "UploadDocumentID";
    this.model.Order = "desc";
    this.model.StartDate = "";
    this.model.EndDate = "";
  }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userCredential) {
      if (this.userCredential.UserPermission.ViewTechnicalDocument) {
        this.createdBy = this.userCredential.UserId;
        this.clientId = this.userCredential.CompanyId;
        this.model.ClientId = this.clientId;
        this.model.BusinessCategoryId = this.userCredential.BusinessCategoryId;
        this.route.params.subscribe(params => {
        });
        this.technicalDocumentObj = {};
        this.defaultdocumentcategory = { 'DocumentCategoryId': 0, 'DocumentCategoryName': '--Select Category--' };
        this.defaultdocumentsubcategory = {
          'DocumentSubCategoryId': 0,
          'DocumentSubCategoryName': '--Select Sub Category--'
        };
        this.ddUpload_DC = 0;
        this.ddUpload_DSC = 0;
        this.ddocumentcategory = 0;
        this.ddocumentsubcategory = 0;
        this.p = 1;
        this.itemPerPage = 1;
        this.technicalSpecification = "";
        this.DocumentName = "";
        this.description = "";
        this.model.StartDate = '';
        this.model.EndDate = '';
        this.model.SearchText = {};
        var thisObj = this;
        thisObj.isLoader = true;
        thisObj.getTechnicalDocumentList();
        this.dataService.get('ManageDocuments/GetDocumentCategory').subscribe(
          response => {
            this.documentCategoryList = response.DocumentCategory;
            this.documentCategoryList.splice(0, 0, this.defaultdocumentcategory);
            this.subdocumentCategorylist = [this.defaultdocumentsubcategory];
          });
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
    //global search
  }

  getDocumentSubCategory(DocumentCategoryId) {
    this.technicalDocumentObj.SubCategoryId = 0;
    this.dataService.get('ManageDocuments/GetDocumentSubCategory?DocumentCategoryID=' + DocumentCategoryId + '').subscribe(
      response => {
        this.isLoader = false;
        this.subdocumentCategorylist = response.SubDocumentCategory;
        this.subdocumentCategorylist.splice(0, 0, this.defaultdocumentsubcategory);
      }
    );
  }

  resetSearchForm() {
    (<any>$('.btnclearenabled')).click();
    (<any>$('.resetSelect')).prop('selectedIndex', 0);
    (<any>$('.resetInput')).val('');
    this.ddocumentcategory = 0;
    this.ddocumentsubcategory = 0;
    this.model.DocumentCategoryId = 0;
    this.model.DocumentSubCategoryId = 0;
    this.DocumentName = '';
    this.search('search');
  }

  //Divyanshu: Search document
  search(type: any) {
    this.isLoader = true;
    this.model.SearchText = {};
    if (this.ddocumentcategory) {
      // this.model.SearchText['DocumentCategoryId'] = this.ddocumentcategory;
      this.model.DocumentCategoryId = this.ddocumentcategory;
    } else {
      this.model.DocumentCategoryId = this.ddocumentcategory;
      this.model.DocumentSubCategoryId = 0;
    }

    if (this.ddocumentsubcategory) {
      // this.model.SearchText['DocumentSubCategoryId'] = this.ddocumentsubcategory;
      this.model.DocumentSubCategoryId = this.ddocumentsubcategory;
    }
    else {
      this.model.DocumentSubCategoryId = this.ddocumentsubcategory;
    }

    if (this.DocumentName) {
      this.model.SearchText['DocumentName'] = this.DocumentName;

    }

    this.model.SearchText = JSON.stringify(this.model.SearchText);
    // if(this.ddocumentcategory ||this.ddocumentsubcategory ||this.DocumentName ||this.model.Startdate && this.model.EndDate )   
    if (type == "search") {
      this.getTechnicalDocumentList();
    }
    else {
      this.dataService.post('ManageDocuments/GetTechnicalDocumentExcel', this.model).subscribe(
        response => {

          if (response.IsSuccess) {
            this.isLoader = false;
            let downloadFileName = encodeURIComponent(response.Data);
            window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Data + "&fileType=excel&businessCategoryId="+this.userCredential.BusinessCategoryId;
          }
          else {
            this.isLoader = false;
            swal('', response.Message);
          }
        });
    }
  }

  getPage(page: number) {
    this.isLoader = true;
    this.model.PageNumber = page;
    this.dataService.post('ManageDocuments/GetTechnicalDocumentList', this.model).subscribe(
      response => {
        if (response.IsSuccess) {
          this.p = page;
          this.documentList = response.DocumentList;
          this.totalItem = Math.ceil(response.TotalRecords / this.model.PageSize);
          this.isLoader = false;
        }
        else {
          swal('', response.Message);
        }
      }
    )
  }

  //Divyanhsu:Sorting based on Column
  sorting(column: string) {
    this.isLoader = true;
    this.model.OrderBy = column;
    if (this.model.Order == 'asc') {
      this.model.Order = 'desc';
    }
    else {
      this.model.Order = 'asc';
    }
    this.getTechnicalDocumentList();
  }

  fileupload(event: any) {
    this.technicalDocumentObj.FileName = event.target.files[0].name;
    this.technicalDocumentObj.UploadedImage = event.target.files[0];
  }

  deleteRole(ID) {
    var that = this;
    swal({
      title: "",
      text: "<span style='color:#ef770e;font-size:20px;'>Are you sure?</span><br><br>",
      html: true,
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Proceed",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true

    },
      function (isConfirm) {
        if (isConfirm) {
          that.isLoader = true;
          that.dataService.get('ManageDocuments/DeleteTechnicalDocument?ID=' + ID).subscribe(
            r => {

              if (r.IsSuccess == true) {
                that.dataService.post('ManageDocuments/GetTechnicalDocumentList', that.model).subscribe(
                  response => {
                    if (response.IsSuccess) {
                      that.isLoader = false;
                      that.documentList = response.DocumentList;
                      that.totalItem = Math.ceil(response.TotalRecords / that.model.PageSize);
                    }
                    else {
                      swal('', response.Message);
                    }
                  });
                // swal("", r.Message);
              }
            }
          );
        }
        else {
        }
      });
  }

  technicalDocumentUpload(obj) {
    this.technicalDocumentObj = {};
    (<any>$('#uploadTechnicalDocument')).modal('show');
    this.subdocumentCategorylist = [this.defaultdocumentsubcategory];
    this.technicalDocumentObj.ClientId = this.clientId;
    this.technicalDocumentObj.CategoryId = 0; ///this.model.CategoryId;
    this.technicalDocumentObj.SubCategoryId = 0; //this.model.SubCategoryId;
    this.technicalDocumentObj.DocumentName = '';
    this.technicalDocumentObj.TechnicalSpecification = '';
    this.technicalDocumentObj.Description = '';
    this.technicalDocumentObj.CreatedBy = this.createdBy;
    this.technicalDocumentObj.UploadedImage = null;
    this.technicalDocumentObj.BusinessCategoryId = this.userCredential.BusinessCategoryId;
  }

  //Divyanhsu: Upload File
  upload() {

    if (this.validateFileExtension(this.technicalDocumentObj.FileName)) {
      let data = new FormData();
      data.append("UploadedImage", this.technicalDocumentObj.UploadedImage);
      data.append("UploadDocument", JSON.stringify(this.technicalDocumentObj));
      let thisObj = this;
      thisObj.isLoader = true;
      let returnObj = thisObj.dataService.postFile('ManageDocuments/UploadDocument', data);
      returnObj.done(function (xhr, textStatus) {
        thisObj.isLoader = false;
        if (xhr.IsSuccess) {
          (<any>$('#uploadTechnicalDocument')).modal('hide');
          //swal('', xhr.Message);
          thisObj.getTechnicalDocumentList();
        }
        else {
          swal('', xhr.Message);
        }

      })

    }


  }

  //Divyanhsu: Download File
  downLoad(fileName, fileType) {
    let that=this;    
    let downloadFileName = encodeURIComponent(fileName.trim());
    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName + "&fileType=" + fileType + "&businessCategoryId=" + that.userCredential.BusinessCategoryId;
  }

  //Divyanshu: Change Event of Date Range Picker
  onDateRangeChanged(event: any) {
    this.model.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
    this.model.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
  }

  //Divyanshu: Get all Technical Document List
  getTechnicalDocumentList() {
    this.dataService.post('ManageDocuments/GetTechnicalDocumentList', this.model).subscribe(
      response => {
        if (response.IsSuccess) {
          this.isLoader = false;
          this.documentList = response.DocumentList;
          this.totalItem = Math.ceil(response.TotalRecords / this.model.PageSize);
        }
        else {
          swal('', response.Message);
        }
      });
  }


  //Divyanhsu: Reset file attributes
  reset() {
    this.fileUploader.nativeElement.value = "";
    this.technicalDocumentObj.FileName = null;
    this.technicalDocumentObj.UploadedImage = null;
  }

  //Divyanshu: Configuration for Date Range Picker
  myDateRangePickerOptions = {
    clearBtnTxt: 'Clear',
    beginDateBtnTxt: 'Begin Date',
    endDateBtnTxt: 'End Date',
    acceptBtnTxt: 'OK',
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'mo',
    sunHighlight: true,
    height: '34px',
    width: '260px',
    inline: false,
    selectionTxtFontSize: '12px',
    alignSelectorRight: false,
    indicateInvalidDateRange: true,
    showDateRangeFormatPlaceholder: false,
    showClearBtn: false,
    customPlaceholderTxt: 'Login Date-Range'
  };

  validateFileExtension(fileName): boolean {
    if (fileName) {
      let extension = fileName.substr(fileName.lastIndexOf('.') + 1);
      if (['pdf', 'doc', 'docx', 'xls', 'xlsx'].indexOf(extension.toLowerCase()) > -1) {
        return true;
      }
      else {
        swal('', 'Please Upload only .pdf, .doc, .docx, .xls, .xlsx files');
        return false;
      }
    }
    else {
      swal('', 'Please Choose File')
    }
  }

}
