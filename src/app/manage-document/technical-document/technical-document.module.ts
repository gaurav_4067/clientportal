import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { Ng2PaginationModule } from 'ng2-pagination';

import { TechnicalDocumentRoutingModule } from './technical-document-routing.module';
import { TechnicalDocumentComponent } from './technical-document.component';
import { FooterModule } from 'app/footer/footer.module';
import { HeaderModule } from 'app/header/header.module';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import {TooltipModule} from "ngx-tooltip";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    Ng2PaginationModule,
    MyDateRangePickerModule,
    TooltipModule,
    TechnicalDocumentRoutingModule,
    FooterModule,
    HeaderModule,
  ],
  declarations: [TechnicalDocumentComponent]
})
export class TechnicalDocumentModule { }
