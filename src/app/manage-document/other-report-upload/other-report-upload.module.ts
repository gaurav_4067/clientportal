import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { MdInputModule } from '@angular2-material/input';
import { FormsModule } from '@angular/forms';
import { Ng2PaginationModule } from 'ng2-pagination';
import { MyDatePickerModule } from 'mydatepicker';

import { OtherReportUploadRoutingModule } from './other-report-upload-routing.module';
import { OtherReportUploadComponent } from './other-report-upload.component';
import { FooterModule } from 'app/footer/footer.module';
import { HeaderModule } from 'app/header/header.module';
import { ImportExcelModule } from 'app/manage-document/other-report-upload/import-excel/import-excel.module'
import {TooltipModule} from "ngx-tooltip";
import { BulkEmailModule } from 'app/bulk-email/bulk-email.module';


@NgModule({
  imports: [
    CommonModule,
    MdInputModule,
    FormsModule,
    TooltipModule,
    Ng2PaginationModule,
    MyDateRangePickerModule,
    MyDatePickerModule,
    OtherReportUploadRoutingModule,
    FooterModule,
    HeaderModule,
    ImportExcelModule,
    BulkEmailModule,
  ],
  declarations: [OtherReportUploadComponent]
})
export class OtherReportUploadModule { }
