import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OtherReportUploadComponent } from 'app/manage-document/other-report-upload/other-report-upload.component';

const routes: Routes = [{
  path: '',
  component: OtherReportUploadComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OtherReportUploadRoutingModule { }
