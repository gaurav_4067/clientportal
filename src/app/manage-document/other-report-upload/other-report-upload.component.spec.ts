import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherReportUploadComponent } from './other-report-upload.component';

describe('OtherReportUploadComponent', () => {
  let component: OtherReportUploadComponent;
  let fixture: ComponentFixture<OtherReportUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherReportUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherReportUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
