/**
* Created by RohitChoudhary on 30-10-2017.
*/

import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import { DataService, BaseUrl } from '../../../mts.service';

declare let swal: any;

@Component({
    selector: 'importExcel',
    templateUrl: './import-excel.component.html'
})
export class ImportExcelComponent {
    @Output() RestAfterUpload = new EventEmitter();
    //@ViewChild('fileUpload') fileUploader: ElementRef;

    isLoader: boolean = false;
    DownloadeFileName: String = 'SampleOtherReport.xlsx';
    UploadThirdPartyReport: any;
    UploadFileName: any;
    userDetail: any;
    userCredential: any;

    ngOnInit() {
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    }
    //DownloadSampleExcel() {
    //    //let downloadFileName = encodeURIComponent(this.DownloadeFileName);
    //    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + this.DownloadeFileName + "";
    //}
    fileupload(event: any) {
      

        if (event.target.files[0].name && event.target.files) {
            this.UploadFileName = event.target.files[0].name;
            this.UploadThirdPartyReport = event.target.files[0];
        }
    }
    reset(e:any) {
        //  this.fileUploader.nativeElement.value = "";

            e.srcElement.value = "";

        this.UploadFileName = null;
        this.UploadThirdPartyReport = null;
    }
    SubmitFile() {
       
        var formData: any = new FormData();
        let xhr = new XMLHttpRequest();
        var that = this;
        if (this.validateFileExtension(this.UploadFileName)) {
        this.fileUploadRequest(BaseUrl + 'ManageDocuments/UploadOtherReport', this.UploadThirdPartyReport).then(
            (result: any) => {
                if (!result.IsSuccess) {
                    swal('', result.Message);
                    this.UploadFileName = null;
                }
                else {
                    //swal('', result.Message);
                    this.RestAfterUpload.emit();
                    this.UploadFileName = null;
                    (<any>$('#OtherReport')).modal('hide');
                    //window.location.reload();
                }

                this.isLoader = false;
            }, (error) => {
            });
    }
}

    fileUploadRequest(url: string, files: Array<File>) {
        
        let thisObj = this;
        var formData: any = new FormData();
        let xhr = new XMLHttpRequest();
        formData.append("OtherReport", this.UploadThirdPartyReport);
        formData.append("ClientID", this.userCredential.CompanyId);
        formData.append("BusinessCategoryId", this.userCredential.BusinessCategoryId);
        return new Promise((resolve, reject) => {
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        (<any>$('#OtherReport')).val('');
                        resolve(JSON.parse(xhr.response))
                    }
                    else {
                        (<any>$('#OtherReport')).val('');
                        reject(JSON.parse(xhr.response));
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.setRequestHeader('Token', this.userDetail.AuthToken)
            xhr.send(formData);
        })

    }
    close(){
        this.UploadFileName = null;
    }
    validateFileExtension(fileName):boolean {
        if(!!fileName) {
            let extension = fileName.substr(fileName.lastIndexOf('.') + 1);
            if (['xls', 'xlsx'].indexOf(extension.toLowerCase()) > -1) {
                return true;
            }
            else {
                swal('', 'Please upload only .xls, .xlsx  file.');
                this.UploadFileName = null;
                this.UploadThirdPartyReport = null;
                return false;
            }
        } else {
            swal('', 'Please upload file.');
            return false;
        }
    }

}
