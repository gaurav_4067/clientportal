import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportExcelComponent } from 'app/manage-document/other-report-upload/Import-Excel/import-excel.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ImportExcelComponent],
  exports: [ImportExcelComponent]
})
export class ImportExcelModule { }
