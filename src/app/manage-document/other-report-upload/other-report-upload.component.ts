import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataService, BaseUrl } from '../../mts.service';
import { SEARCH_MODEL } from './thirdParty-model';
import { InjectService } from '../../injectable-service/injectable-service';
import { isUndefined } from "util";
import { Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';
declare let swal: any;


@Component({
  selector: 'app-other-report-upload',
  templateUrl: './other-report-upload.component.html',
  styleUrls: ['./other-report-upload.component.css']
})
export class OtherReportUploadComponent implements OnInit {
  @ViewChild('fileUpload') fileUploader: ElementRef;

  private myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd-mm-yyyy',
  };
  private fileName: string;
  showDownloadFiles: boolean = false;
  documentList: any;
  documentCategoryList: any;
  subdocumentCategorylist: any;
  model: any;
  private p: any;
  private totalItem: number;
  private itemPerPage: any;

  ddocumentcategory: number;
  ddocumentsubcategory: number;
  defaultdocumentcategory: any;
  defaultdocumentsubcategory: any;

  technicalSpecification: any;
  description: any
  clientId: number;
  createdBy: number;
  techncalDocumentObj: any;
  isLoader: boolean;
  DocumentName: string;
  labLocationList: any;
  userCredential: any;
  bulkEmailFilesObj: any = {}
  remarks: any = [];
  ccEmails: any = [];
  batchFiles: any = [];
  showFiles: any
  checkedReportArr: any = [];
  filesCount: any;
  ReportNo: any;
  userDetail: any;
  DownloadfileName: any;
  bulkEmailSendObject: any = {
    EmailLogID: 0,
    EmailD: '',
    CompanyName: '',
    Subject: '',
    Body: '',
    Status: false,
    FileName: '',
    CreatedDate: Date.now(),
    type: '',
    CreatedBy: '',
    ModifiedBy: 0,
    ModifiedDate: Date.now(),
    CCEmailID: '',
    FileType:'',
    BusinessCategoryId:0
  }
  Gender: any = [];
  Program: any = [];
  DownloadeFileNameExcel: String = 'SampleOtherReport.xlsx';

  constructor(private dataService: DataService, private http: Http, private router: Router) {
    this.ccEmails = [];
    this.remarks = [];
    this.model = {};
    this.model.PageSize = 10;
    this.model.PageNumber = 1;
    this.model.OrderBy = "ID";
    this.model.Order = "desc";
  }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    if (this.userCredential && this.userDetail) {
      if (this.userCredential.UserPermission.ViewThirdPartyReportList) {
        this.createdBy = this.userCredential.UserId;
        this.clientId = this.userCredential.BusinessCompanyId == 0 ? this.userCredential.UserCompanyId : this.userCredential.BusinessCompanyId;
        this.model.ClientId = this.clientId;
        this.model.BusinessCategoryId = this.userCredential.BusinessCategoryId;
        this.techncalDocumentObj = {};
        this.labLocationList = [];
        this.defaultdocumentcategory = { 'DocumentCategoryId': 0, 'DocumentCategoryName': '--Select Category--' };
        this.defaultdocumentsubcategory = {
          'DocumentSubCategoryId': 0,
          'DocumentSubCategoryName': '--Select Sub Category--'
        };
        this.ddocumentcategory = 0;
        this.ddocumentsubcategory = 0;
        this.p = 1;
        this.itemPerPage = 1;
        this.technicalSpecification = "";
        this.DocumentName = "";
        this.description = "";
        this.model.StartDate = '';
        this.model.EndDate = '';
        this.model.SearchText = [{ ColumnName: "ReportNo", Operator: 5, ColumnValue: "" }, {
          ColumnName: "StyleNo",
          Operator: 5,
          ColumnValue: ""
        }, { ColumnName: "SkuNo", Operator: 5, ColumnValue: "" }];

        var thisObj = this;
        thisObj.isLoader = true;
        thisObj.getTechnicalDocumentList();

        this.dataService.get('ManageDocuments/GetDocumentCategory').subscribe(
          response => {
            this.documentCategoryList = response.DocumentCategory;
            this.documentCategoryList.splice(0, 0, this.defaultdocumentcategory);
            this.subdocumentCategorylist = [this.defaultdocumentsubcategory];
          });
        this.dataService.get('OrderAnalysis/LabLocationListOtherReport').subscribe(
          response => {
            this.labLocationList = response;
            this.labLocationList.splice(0, 0, { "LabLocationId": 0, "LabLocationName": "Select Lab Location" });
          }
        );
        this.dataService.get('ManageDocuments/GenderProgram?ClientID=' + this.clientId).subscribe(
          response => {
            this.Gender = response.Data.Gender;
            this.Program = response.Data.Programs;
          }
        );
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  DownloadSampleExcel() {
    //let downloadFileName = encodeURIComponent(this.DownloadeFileName);
    window.location.href = BaseUrl + "ManageDocuments/DownloadSampleFile?FileName=" + this.DownloadeFileNameExcel + "&Folder=SampleExcel";
  }

  showDownload() {
    this.showDownloadFiles = this.showDownloadFiles == true ? this.showDownloadFiles = false : this.showDownloadFiles = true;
  }

  getDocumentSubCategory(DocumentCategoryId) {

    this.dataService.get(BaseUrl + 'ManageDocuments/GetDocumentSubCategory?DocumentCategoryID=' + DocumentCategoryId + '').subscribe(
      response => {
        this.subdocumentCategorylist = response.SubDocumentCategory;
        this.subdocumentCategorylist.splice(0, 0, this.defaultdocumentsubcategory);
      }
    );
  }

  onDateInChanged(e) {


    var date = e.date.month + '/' + e.date.day + '/' + e.date.year;
    this.techncalDocumentObj.LoginDate = date;
  }

  onDateOutChanged(e) {

    var date = e.date.month + '/' + e.date.day + '/' + e.date.year;
    this.techncalDocumentObj.LogoutDate = date;
  }

  onDueDateChanged(e) {

    var date = e.date.month + '/' + e.date.day + '/' + e.date.year;
    this.techncalDocumentObj.DueDate = date;
  }

  resetSearchForm() {
    (<any>$('.btnclearenabled')).click();
    (<any>$('.resetInput')).val('');
    this.model.SearchText[0].ColumnValue = "";
    this.model.SearchText[1].ColumnValue = "";
    this.model.SearchText[2].ColumnValue = "";
    this.search();
  }

  deleteRole(ID) {

    var that = this;
    swal({
      title: "",
      text: "<span style='color:#ef770e;font-size:20px;'>Are you sure?</span><br><br>",
      html: true,
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Proceed",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true

    },
      function (isConfirm) {
        if (isConfirm) {
          that.isLoader = true;
          that.dataService.get('ManageDocuments/DeleteThirdPartReport?ID=' + ID).subscribe(
            r => {

              if (r.IsSuccess == true) {
                that.dataService.post('ManageDocuments/GetThirdPartyReportList', that.model).subscribe(
                  response => {
                    that.isLoader = false;
                    if (response.IsSuccess) {
                      that.isLoader = false;
                      that.documentList = response.DocumentList;
                      that.totalItem = Math.ceil(response.TotalRecords / that.model.PageSize);
                    }
                    else {
                      swal('', response.Message);
                    }
                  });
                // swal("", r.Message);
              }
            }
          );
        }
        else {
        }
      });
  }

  UploadExcel() {

    (<any>$("#OtherReport")).modal('show');


  }

  ExportToexcel() {

    this.isLoader = true;
    this.dataService.post('ManageDocuments/ExportTothirdPartyExcel', this.model).subscribe(
      response => {
        this.isLoader = false;
        if (response.IsSuccess) {
          let downloadFileName = encodeURIComponent(response.Data);
          window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Data + "&fileType=excel&businessCategoryId="+this.userCredential.BusinessCategoryId;

        }
        else {
          swal('', response.Message);
        }
      });
  }

  search() {
    /*this.model.SearchText = {};
     if (this.ddocumentcategory ) {
     this.model.SearchText['DocumentCategoryId'] = this.ddocumentcategory;
     }
     if (this.ddocumentsubcategory) {
     this.model.SearchText['DocumentSubCategoryId'] = this.ddocumentsubcategory;
     }
     if (this.DocumentName) {
     this.model.SearchText['DocumentName'] = this.DocumentName;

     }*/
    //this.model.SearchText = JSON.stringify(this.model.SearchText);
    // if(this.ddocumentcategory ||this.ddocumentsubcategory ||this.DocumentName ||this.model.Startdate && this.model.EndDate )
    this.getTechnicalDocumentList();
  }

  getPage(page: number) {

    this.model.PageNumber = page;

    //this.dataService.post('ManageDocuments/GetTechnicalDocumentList', this.model).subscribe(
    this.dataService.post('ManageDocuments/GetThirdPartyReportList', this.model).subscribe(
      response => {
        if (response.IsSuccess) {
          this.p = page;
          this.documentList = response.DocumentList;
          this.totalItem = Math.ceil(response.TotalRecords / this.model.PageSize);
          this.checkedReportArr = [];

        }
        else {
          swal('', response.Message);
          this.checkedReportArr = [];
        }
      }
    )
  }

  sorting(column: string) {
    this.model.OrderBy = column;
    if (this.model.Order == 'asc') {
      this.model.Order = 'desc';
    }
    else {
      this.model.Order = 'asc';
    }
    this.getTechnicalDocumentList();
  }

  /*removeFile(){
   // ;
   let input = document.getElementById('inputValue').value=null;
   }*/
  fileupload(event: any) {
    if (event.target.files[0].name && event.target.files) {
      this.techncalDocumentObj.FileName = event.target.files[0].name;
      this.techncalDocumentObj.UploadThirdPartyReport = event.target.files[0];
    }
  }

  technicalDocumentUpload(obj) {
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.techncalDocumentObj = {};
    this.techncalDocumentObj.Gender = "";
    this.techncalDocumentObj.Program = "";
    this.techncalDocumentObj.LabLocationId = 0;
    this.showDownloadFiles = false;
    (<any>$('#uploadTechnicalDocument')).modal('show');
    this.subdocumentCategorylist = [this.defaultdocumentsubcategory];
    this.techncalDocumentObj.CreatedBy = this.createdBy;
    this.techncalDocumentObj.ClientId = this.clientId;
    this.techncalDocumentObj.BusinessCategoryId = this.userCredential.BusinessCategoryId;
  }

  cancelUpload() {
    this.techncalDocumentObj.LoginDate = null;
    this.techncalDocumentObj.LogoutDate = null;
    this.techncalDocumentObj.DueDate = null;
    this.remarks = [];
    this.ccEmails = [];
    this.ReportNo = "";
    this.showDownloadFiles = false;
    (<any>$('.date-pick')).val('');
    (<any>$('#checkPop')).prop('checked', false);
    (<any>$('#checkPop123')).prop('checked', false);
  }


  upload() {

    var that = this;
    if (!this.techncalDocumentObj.UploadThirdPartyReport) {
      swal('', 'Please choose file to upload.');
      return;
    }
    else if (!this.techncalDocumentObj.ReportNo) {
      swal('', 'Please enter report no.');
      return;
    }
    else if (!this.techncalDocumentObj.LoginDate) {
      swal('', 'Please enter login date.');
      return;
    }
    else if (!this.techncalDocumentObj.LabLocationId) {
      swal('', 'Please enter lab location.');
      return;
    }


    if (that.showDownloadFiles && that.userDetail.EmailId == "") {
      swal('', "Please enter emailid.");
      return;
    } else {
      that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
    }


    if (this.techncalDocumentObj.LoginDate != undefined || this.techncalDocumentObj.LoginDate != null) {
      this.techncalDocumentObj.LoginDate = this.techncalDocumentObj.LoginDate.date.month + '/' + this.techncalDocumentObj.LoginDate.date.day + '/' + this.techncalDocumentObj.LoginDate.date.year;
    }
    if (this.techncalDocumentObj.LogoutDate != undefined || this.techncalDocumentObj.LogoutDate != null) {
      this.techncalDocumentObj.LogoutDate = this.techncalDocumentObj.LogoutDate.date.month + '/' + this.techncalDocumentObj.LogoutDate.date.day + '/' + this.techncalDocumentObj.LogoutDate.date.year;
    }
    if (this.techncalDocumentObj.DueDate != undefined || this.techncalDocumentObj.DueDate != null) {
      this.techncalDocumentObj.DueDate = this.techncalDocumentObj.DueDate.date.month + '/' + this.techncalDocumentObj.DueDate.date.day + '/' + this.techncalDocumentObj.DueDate.date.year;
    }
    if (this.validateFileExtension(this.techncalDocumentObj.FileName)) {
      let data = new FormData();
      data.append("UploadThirdPartyReport", this.techncalDocumentObj.UploadThirdPartyReport);
      data.append("ReportData", JSON.stringify(this.techncalDocumentObj));
      let thisObj = this;
      thisObj.isLoader = true;

      let returnObj = thisObj.dataService.postFile('ManageDocuments/ThirdPartReportUpload', data);
      returnObj.done(function (xhr, textStatus) {

        thisObj.isLoader = false;

        if (xhr.IsSuccess) {

          that.techncalDocumentObj.LoginDate = null;
          that.techncalDocumentObj.LogoutDate = null;
          that.techncalDocumentObj.DueDate = null;
          that.techncalDocumentObj.Gender = "";
          that.techncalDocumentObj.Program = "";
          (<any>$('#checkPop')).prop('checked', false);
          (<any>$('#checkPop123')).prop('checked', false);
          // that.techncalDocumentObj={};
          //(<any>$('#uploadTechnicalDocument')).modal('hide');
          // alert(that.userDetail.EmailId);

          thisObj.getTechnicalDocumentList();
          if (that.showDownloadFiles == true) {
            // that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
            that.bulkEmailSendObject.ReportNo = that.techncalDocumentObj.ReportNo;
            that.bulkEmailSendObject.Subject = "MTS - Test Report : " + that.techncalDocumentObj.ReportNo;
            that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
            that.bulkEmailSendObject.FileName = xhr.Data;
            that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
            that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
            that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
            that.bulkEmailSendObject.BusinessCategoryId = that.userCredential.BusinessCategoryId;
            that.bulkEmailSendObject.FileType = "ODOC";
            that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
              response => {
                if (response.IsSuccess) {

                  //swal('', response.Message);
                  that.techncalDocumentObj.ReportNo = "";
                  that.ccEmails = [];
                  that.remarks = [];
                  that.userDetail.EmailId = "";
                  (<any>$('#uploadTechnicalDocument')).modal('hide');
                }
                else {
                  swal('', response.Message);
                }
              }
            )
          }
          else {
            (<any>$('#uploadTechnicalDocument')).modal('hide');
          }
        }
        else {
          //that.techncalDocumentObj.LoginDate=null;
          swal('', xhr.Message);
        }

      })

    }

  }

  download(fileName: any, data: any, reportNo: any) {

    this.ReportNo = reportNo;
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userDetail.EmailId = this.userDetail.EmailId;
    (<any>$('#DownloadFile')).modal('show');

    this.bulkEmailSendObject.type = data;
    //this.DownloadfileName = fileName.FileName;
    this.DownloadfileName = fileName.trim();


    this.ccEmails = [];
    this.remarks = [];
    //this.TrfPop = false;
    //this.ReportPop = false;
    //this.InvoicePop = false;
    this.showDownloadFiles = false;
    (<any>$('.CheckPop')).prop('checked', false)
    // this.DownloadfileName = fileName;
    // let downloadFileName = encodeURIComponent(fileName.trim());
    // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "";
  }

  downloadFile() {

    var that = this;
    if (that.showDownloadFiles && that.userDetail.EmailId == "") {
      swal('', "Please enter emailid");
      return;
    } else {
      that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
    }
    //(<any>$('#DownloadFile')).modal('hide');
    if (this.showDownloadFiles == true) {
      // that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
      that.bulkEmailSendObject.ReportNo = that.ReportNo;
      that.bulkEmailSendObject.Subject = "MTS - Test Report No.: " + that.ReportNo;
      that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
      that.bulkEmailSendObject.FileName = that.DownloadfileName;
      that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
      that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
      that.bulkEmailSendObject.FileStatus = 'D';
      that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
      that.bulkEmailSendObject.FileType="ODOC";
      that.bulkEmailSendObject.BusinessCategoryId=this.userCredential.BusinessCategoryId;
      that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
        response => {
          if (response.IsSuccess) {
            swal('', response.Message);
            //  that.checkedReportNo = [];
            that.ccEmails = [];
            that.remarks = [];
            that.userDetail.EmailId = "";
            (<any>$('#DownloadFile')).modal('hide');

            // let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
            // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "";
          }
          else {
            swal('', response.Message);
          }
        }
      )
    }
    else {
      let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
      window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "&fileType=ODOC&businessCategoryId="+this.userCredential.BusinessCategoryId;
    }
  }

  //downLoad(fileName) {
  //    let downloadFileName = encodeURIComponent(fileName);
  //    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName;
  //}
  onDateRangeChanged(event: any) {
    this.model.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
    this.model.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
  }

  getTechnicalDocumentList() {
    this.isLoader = true;
    this.dataService.post('ManageDocuments/GetThirdPartyReportList', this.model).subscribe(
      response => {
        //this.isLoader = false;
        this.checkedReportArr = [];
        if (response.IsSuccess) {
          this.isLoader = false;
          this.documentList = response.DocumentList;
          this.totalItem = Math.ceil(response.TotalRecords / this.model.PageSize);
        }
        else {
          this.isLoader = false;
          swal('', response.Message);
        }
      });
  }

  myDateRangePickerOptions = {
    clearBtnTxt: 'Clear',
    beginDateBtnTxt: 'Begin Date',
    endDateBtnTxt: 'End Date',
    acceptBtnTxt: 'OK',
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'mo',
    sunHighlight: true,
    height: '34px',
    width: '260px',
    inline: false,
    selectionTxtFontSize: '12px',
    alignSelectorRight: false,
    indicateInvalidDateRange: true,
    showDateRangeFormatPlaceholder: false,
    showClearBtn: false,
    customPlaceholderTxt: 'Login Date-Range'
  };

  reset() {
    this.fileUploader.nativeElement.value = "";
    this.techncalDocumentObj.FileName = null;
    this.techncalDocumentObj.UploadThirdPartyReport = null;
  }

  validateFileExtension(fileName): boolean {
    let extension = fileName.substr(fileName.lastIndexOf('.') + 1);
    if (['pdf', 'doc', 'docx', 'xls', 'xlsx'].indexOf(extension.toLowerCase()) > -1) {
      return true;
    }
    else {
      swal('', 'Please Upload only .pdf, .doc, .docx, .xls, .xlsx files');
      return false;
    }
  }

  handleCheckboxCheck(e: any, report: any) {
    if (e.target.checked) {
      report["filereportArray"] = [];
      report["tempFilereportArray"] = [];
      report["uploadedReportCount"] = report.FileName.split(',').length;
      report["filenames"] = '';
      if (report["uploadedReportCount"] == 15) {
        report["isUploadDisabled"] = true;
      } else {
        report["isUploadDisabled"] = false;
      }


      if (this.checkedReportArr.length < 5) {
        this.checkedReportArr.push(report);
      } else {
        swal('', 'Maximum 5 files can be upload at a time.');
        (<any>$('#' + e.target.id)).prop('checked', false);
      }

    } else {
      let index = this.checkedReportArr.indexOf(report);
      this.checkedReportArr.splice(index, 1);
    }
  }

  batchUploadBtnClick() {
    if (this.checkedReportArr.length == 0) {
      swal('', 'Please choose atleast one report');
    } else {
      (<any>$('#batchUpload')).modal('show');
    }
  }

  fileChangeEvent(event: any, index) {
    if (event.target.files) {
      var canUploadFile = 15 - this.checkedReportArr[index].uploadedReportCount - this.checkedReportArr[index].filereportArray.length;
      if (5 - this.checkedReportArr[index].filereportArray.length < canUploadFile) {
        canUploadFile = 5 - this.checkedReportArr[index].filereportArray.length;
      }
      this.filesCount = event.target.files.length;
      if (this.filesCount <= canUploadFile) {
        // if (this.filesCount <= 5) {
        this.checkedReportArr[index].tempFilereportArray = [];
        for (let i = 0; i < event.target.files.length; i++) {

          var result = this.validateFileExtensionReport(event.target.files[i].name);

          if (result == true) {
            this.checkedReportArr[index].tempFilereportArray.push({ fileObj: event.target.files[i] });

            if (i == 0) {
              this.checkedReportArr[index].filenames = event.target.files[0].name;
            }
            else {
              this.checkedReportArr[index].filenames = i + 1 + " file selected";
            }
          } else {
            this.checkedReportArr[index].filenames = [];
            swal('', 'Please Upload only .pdf, .doc, .docx, .xls, .xlsx, .pptx files');
            break;
          }

        }
        //} else {
        //
        //}
      }
      else {
        swal('', 'You can only upload only ' + canUploadFile + ' files.');
      }


    }


  }

  addUploadReport(index) {

    if (this.checkedReportArr[index].tempFilereportArray.length > 0) {
      for (let i = 0; i < this.checkedReportArr[index].tempFilereportArray.length; i++) {
        this.checkedReportArr[index].filereportArray.push(this.checkedReportArr[index].tempFilereportArray[i]);
      }
      this.checkedReportArr[index].tempFilereportArray = [];
      this.checkedReportArr[index].filenames = '';
      var canUploadFile = 15 - this.checkedReportArr[index].uploadedReportCount - this.checkedReportArr[index].filereportArray.length;
      if (5 - this.checkedReportArr[index].filereportArray.length < canUploadFile) {
        canUploadFile = 5 - this.checkedReportArr[index].filereportArray.length;
      }
      if (canUploadFile == 0) {
        this.checkedReportArr[index].isUploadDisabled = true;
      }
    } else {
      swal('', 'Please choose file to upload.');
    }


  }

  deleteReportFile(index: any, i: any) {
    this.checkedReportArr[index].filereportArray.splice(i, 1);
    this.checkedReportArr[index].isUploadDisabled = false;
  }

  resetFile(index, e) {
    this.checkedReportArr = [];
    (<any>$('.FileCheck')).prop('checked', false)

  }

  resetBatchFile(e: any) {
    e.srcElement.value = "";
  }

  submitReport(Index: any) {
    if (!this.isLoader) {
      this.isLoader = true;
    }
    let that = this;
    let indexNew = Index;
    var formData: any = new FormData();

    if (indexNew < this.checkedReportArr.length) {
      if (this.checkedReportArr[indexNew].filereportArray.length > 0) {
        for (let i = 0; i < this.checkedReportArr[indexNew].filereportArray.length; i++) {
          formData.append('UploadReport-' + i, this.checkedReportArr[indexNew].filereportArray[i].fileObj);
        }
        formData.append('ID', this.checkedReportArr[indexNew].ID);
         
        formData.append('BusinessCategoryId', this.userCredential.BusinessCategoryId);
        this.dataService.post('ManageDocuments/ThirdPartBulkUpload', formData).subscribe(
          response => {

            if (response.IsSuccess) {
              if (that.checkedReportArr.length - 1 > indexNew) {
                that.submitReport(indexNew + 1);//recursive Function
              }
              else {
                //that.submitReport(indexNew + 1);
                that.checkedReportArr = [];
                (<any>$("#batchUpload")).modal('hide');
                (<any>$('.FileCheck')).prop('checked', false);
                that.isLoader = false;
                that.getTechnicalDocumentList();
                //swal('', response.Message);
              }
            }
            else {
              swal('', response.Message);
              that.isLoader = false;
            }
          });

      }
      else { //if any inner array is empty then it will call to recursive function again
        if (that.checkedReportArr.length - 1 > indexNew) {
          that.submitReport(indexNew + 1);
        }
        else {//if last array is empty it will close the popup
          that.checkedReportArr = [];
          (<any>$("#batchUpload")).modal('hide');
          (<any>$('.FileCheck')).prop('checked', false);
          that.isLoader = false;
          that.getTechnicalDocumentList();
        }
      }
    }
    else {
      that.isLoader = false;
      that.checkedReportArr = [];
      (<any>$("#batchUpload")).modal('hide');
      (<any>$('.FileCheck')).prop('checked', false);
      that.getTechnicalDocumentList();
    }
  }

  submitUploadValidate(Index: any) {
    let flag = false;
    for (let i = 0; i < this.checkedReportArr.length; i++) {
      if (this.checkedReportArr[i].filereportArray.length > 0) {
        flag = true;
        break;
      }
    }
    if (flag) {
      this.submitReport(Index);
    }
    else {
      swal('', "Please choose atleast one file.");
    }
  }

  validateFileExtensionReport(fileName: any) {
    if (fileName) {
      var fileNames = fileName.split(', ');
      var result
      fileNames.forEach((file) => {
        if (file) {
          let extension = fileName.substr(fileName.lastIndexOf('.') + 1);
          if (['pdf', 'doc', 'docx', 'xls', 'xlsx', 'pptx'].indexOf(extension.toLowerCase()) > -1) {
            result = true;
          }
          else {
            result = false;
            return;
          }
        }
        else {
          swal('', 'Please Choose a file');
        }
      })
      if (result == true) {
        return true;
      }
      else {
        swal('', 'Please Upload only .pdf, .doc, .docx, .xls, .xlsx ,.pptx files');
        return false;
      }
    } else {
      swal('', 'Please Choose a file');
    }
  }

}
