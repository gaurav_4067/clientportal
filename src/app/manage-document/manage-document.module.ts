import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageDocumentRoutingModule } from './manage-document-routing.module';
import { NoContentModule } from '../no-content/no-content.module';

@NgModule({
  imports: [
    CommonModule,
    ManageDocumentRoutingModule,
    NoContentModule
  ],
  declarations: []
})
export class ManageDocumentModule { }
