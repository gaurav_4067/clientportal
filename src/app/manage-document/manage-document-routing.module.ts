import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoContentComponent } from '../no-content/no-content.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: NoContentComponent
    },
    {
      path: 'other-report-upload',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./other-report-upload/other-report-upload.module').OtherReportUploadModule);
        })
      })
    },
    {
      path: 'technical-document-upload',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./technical-document/technical-document.module').TechnicalDocumentModule);
        })
      })
    },
    {
      path: 'report-upload',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./report-upload/report-upload.module').ReportUploadModule);
        })
      })
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageDocumentRoutingModule { }
