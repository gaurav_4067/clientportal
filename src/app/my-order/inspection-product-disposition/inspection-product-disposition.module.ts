import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { InspectionProductDispositionRoutingModule } from './inspection-product-disposition-routing.module';
import { InspectionProductDispositionComponent } from './inspection-product-disposition.component';
import { BulkEmailModule } from 'app/bulk-email/bulk-email.module';
import { FormsModule } from '@angular/forms';
import { Ng2PaginationModule } from 'ng2-pagination';
import {TooltipModule} from "ngx-tooltip";
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { MdInputModule } from '@angular2-material/input';

@NgModule({
  imports: [
    CommonModule,
    MyDateRangePickerModule,
    InspectionProductDispositionRoutingModule,
    BulkEmailModule ,
    FormsModule,
    Ng2PaginationModule,
    TooltipModule,
    HeaderModule,
    FooterModule,
    MdInputModule,    

  ],
  declarations: [InspectionProductDispositionComponent]
})
export class InspectionProductDispositionModule { }
