import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InspectionProductDispositionComponent } from 'app/my-order/inspection-product-disposition/inspection-product-disposition.component';

const routes: Routes = [{
  path:'',
  component:InspectionProductDispositionComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InspectionProductDispositionRoutingModule { }
