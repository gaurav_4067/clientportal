import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionProductDispositionComponent } from './inspection-product-disposition.component';

describe('InspectionProductDispositionComponent', () => {
  let component: InspectionProductDispositionComponent;
  let fixture: ComponentFixture<InspectionProductDispositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionProductDispositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionProductDispositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
