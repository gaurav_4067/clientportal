export const SEARCH_MODEL = {
    "ClientId": "",
    "UserType": "",
    "UserCompanyId": "",
    "PageSize": "",
    "PageNumber": "",
    "OrderBy": "OrderId",
    "Order": "asc",
    "StartDate": null,
    "EndDate": null,
    "SearchText": [],
    "ProductDisposition": 0
};

export class EmailObject {
    public ClientEmailTo: String;
    public clientremarks: any = [];
    public ClientEmailCC: string;
    public ccField: string;
    public EmailTo: string;
    public errorEmailTo:Boolean=false;
    public errorEmailCC:Boolean=false;
}


export class EDIDataExtractionRequest
{
    public ClientId :number
    public StartDate :Date
    public EndDate : Date;
}