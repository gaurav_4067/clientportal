import { Component, OnInit, OnDestroy } from '@angular/core';
import { SEARCH_MODEL, EmailObject,EDIDataExtractionRequest } from './order-tracking-model';
import { DataService, BaseUrl, COLUMN_LIST } from '../../mts.service';
import { Http, Response } from '@angular/http';
import { InjectService } from '../../injectable-service/injectable-service';
import { Hero } from '../common/model_1';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { ActivatedRoute, Router } from '@angular/router';
import 'table2excel';
declare let swal: any;

@Component({
    selector: 'app-order-tracking',
    templateUrl: './order-tracking.component.html',
    styleUrls: ['./order-tracking.component.css']
})
export class OrderTrackingComponent implements OnInit, OnDestroy {
    orderList: any;
    dateRange: any;
    ediShipDateRange: any;
    ediDataExtractionSearchModal: EDIDataExtractionRequest;
    shipDateRangeModel: any;
    shipDateRangeStart: any;

    LogOutDateRange: any;

    private p: any;
    private totalItem: any;
    private itemPerPage: any;
    searchModal: any;
    searchText: any;
    userDetail: any;
    dynamicSearchColumns: any;
    dynamicGridColumns: any = [];
    dynamicGridColumnsNew: any = [];
    isLoading: boolean;
    viewHistoryList: any;
    reportNo: any;
    loginDate: any;
    dueDate: any;
    rating: any;
    additionalSearch: any;
    userCredential: any;
    columnList: any;
    historyList: any;
    searchParam: any;
    checkedReportNo: any = [];
    clientEmail: any;
    clientId: any;
    bulkEmailSubject: any = 'MTS - Test Report :';
    bulkEmailFiles: any;
    bulkEmailFilesObj: any = {}; // its for temp storage
    ccEmailIds: any = null;
    remarks: any = [];
    repeatedCCFields: any;
    repeatedCCArray: any = [];
    abc: any = 5;
    ccEmails: any = [];
    ccEmailTemp: any;
    errorEmail: boolean;
    showDownloadFiles: boolean = false;
    ReportNo: any;
    DownloadfileName: any;
    isDashboard: any = false;
    PageSizeDynamic: number = 10;
    clientEmailBulkEmail: any = [];
    bulkEmailSendObject: any = {
        EmailLogID: 0,
        EmailD: '',
        Subject: '',
        Body: '',
        CompanyName: '',
        Status: false,
        FileName: '',
        CreatedDate: Date.now(),
        type: '',
        CreatedBy: '',
        ModifiedBy: 0,
        ModifiedDate: Date.now(),
        CCEmailID: '',
        FileType: '',
        BusinessCategoryId: 0
    }
    selectedReportNo: string;
    previousCommentList: any;
    showEmailTemplate: any = false;
    EmailObject: EmailObject;
    fileType: string;
    date: Date;
    labLocationList: any;

    constructor(public dataService: DataService, private injectService: InjectService, private route: ActivatedRoute, private router: Router) {
        this.searchModal = SEARCH_MODEL;
        this.searchModal.PageSize = this.PageSizeDynamic;
        this.searchModal.PageNumber = 1;
        this.searchModal.StartDate = '';
        this.searchModal.EndDate = '';
        this.searchModal.OrderBy = 'LogoutTime';
        this.searchModal.Order = 'desc';
        this.searchModal.FormId = '1';
        this.dateRange = {};
        this.LogOutDateRange = {};
        this.ediShipDateRange = {};
        this.ccEmails = [];
        this.remarks = [];
        this.ccEmailTemp = "";
        this.errorEmail = false;
        this.clientEmail = '';
        this.EmailObject = new EmailObject();
        this.shipDateRangeStart = new Date();
        this.labLocationList = [];
        
        this.shipDateRangeStart.setMonth(this.shipDateRangeStart.getMonth() - 3);
        //this.shipDateRangeStart.setDate(this.shipDateRangeStart.getDate() - 7);
        this.date = new Date();
        this.ediDataExtractionSearchModal = new EDIDataExtractionRequest();


    }
    ngOnInit() {
        this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        // this.clientEmailBulkEmail.push(that.userDetail.EmailId);
        /*Divyanshu: Following if block is for checking AuthToken for each Component
       before doing any further task,please discuss with me before removing it.
       */
        if (this.userDetail && this.userCredential) {
            if (this.userCredential.UserPermission.ViewOrderTracking) {
                /* following  lines of code moved from constructor to ngOnInit ,because cross browser issue,
            please discuss before remove
            */
                this.searchModal.ClientId = this.userCredential.CompanyId;  // It will ClientId
                this.searchModal.UserType = this.userCredential.UserType;
                this.searchModal.UserCompanyId = this.userCredential.UserCompanyId;      //It will VendorId
                // this.columnList = COLUMN_LIST;
                this.p = 1;
                this.itemPerPage = 1;
                this.totalItem = 0;
                this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
                /*below for solving bug ot searching and sorting*/
                this.searchParam = {};
                this.searchParam.SearchText = [{ LogicalOperator: "AND", ColumnName: "ReportNo", Operator: 5, ColumnValue: "" }, { LogicalOperator: "AND", ColumnName: "StyleNo", Operator: 5, ColumnValue: "" },
                { LogicalOperator: "AND", ColumnName: "SampleDescription", Operator: 5, ColumnValue: "" },
                { LogicalOperator: "AND", ColumnName: "LogoutTime", Operator: 6, ColumnValue: "" },
                { LogicalOperator: "AND", ColumnName: "LabLocationId", Operator: 1, ColumnValue: "" }];
                this.searchModal.SearchText = [];
                /*ended*/

                this.additionalSearch = [{ LogicalOperator: "AND", ColumnName: "", Operator: "", ColumnValue: "" }];
                //global search
                var that = this;
                this.route.params.subscribe(params => {
                    that.searchModal.SearchText = JSON.parse(JSON.stringify(that.searchParam.SearchText));
                    that.searchModal.SearchText[0].ColumnValue = params['search'];
                });
                //get grid and search columns for CompanyId
                this.isLoading = true;

                /*below for solving bug ot searching and sorting*/
                /*if (this.injectService.additionalSearch) {
                    var thisObj = this;
                    if (thisObj.searchModal.SearchText.length > 3) {
                        var count = thisObj.searchModal.SearchText.length - 3;
                        thisObj.searchModal.SearchText.splice(3, count);
                    }
                    thisObj.injectService.additionalSearch.forEach(function (addObj) {
                        thisObj.searchModal.SearchText.push(addObj);
                    });
                }*/
                if (this.injectService.additionalSearch) {
                    if (this.searchParam.SearchText.length > 5) {
                        var count = this.searchParam.SearchText.length - 5;
                        this.searchParam.SearchText.splice(5, count);
                    }
                    var that = this;
                    this.injectService.additionalSearch.forEach(function (addObj) {
                        that.searchParam.SearchText.push(addObj);
                    });
                    //addded by Divyanshu
                    this.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
                    this.isDashboard = true;
                }
                else {
                    this.isDashboard = false;
                }
                /*ended*/

                this.dataService.get('OrderAnalysis/LabLocationList').subscribe(
                    response => {
                      this.labLocationList = response;
                      this.labLocationList.splice(0, 0, { "LabLocationId": "", "LabLocationName": "Select Lab Location" });
                    }
                  );


                this.dataService.get('Order/GetMyOrderColumnsList?selectedCompany=' + this.searchModal.ClientId + '&FormId=1').subscribe(
                    response => {
                        if (response.IsSuccess) {
                            this.dynamicSearchColumns = response.SelectedSearchColumn;
                            //this.dynamicGridColumns = response.SelectedGridColumn;
                            this.columnList = response.SelectedAdHocSearchColumn;
                            response.SelectedGridColumn.forEach(function (addObj, i) {
                                if (addObj.ColumnName == 'GCC' || addObj.ColumnName == 'SUP') {
                                    that.dynamicGridColumnsNew.push(addObj);
                                }
                                else {
                                    that.dynamicGridColumns.push(addObj);
                                }
                            });
                            this.getOrderList();
                        }
                        else {
                            this.getOrderList();
                            swal('', response.Message)
                        }
                    }
                );
            } else {
                this.router.navigate(['./landing-page']);
            }
        } else {
            this.router.navigate(['./login']);
        }
    }

    getCCEmail(e) {
        var val = e.target.value;
        if (val.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            this.errorEmail = false;
            this.ccEmailTemp = val;
        }
        else if (val == "") {
            this.errorEmail = false;
        }
        else {
            this.errorEmail = true;
        }
    }

    dateFocusIn() {
        // (<any> $('.custom-material')).
    }

    resetSearchForm() {
        this.PageSizeDynamic = 10;
        (<any>$('.btnclearenabled')).click();
        this.searchParam.SearchText[0].ColumnValue = '';
        this.searchParam.SearchText[1].ColumnValue = '';
        this.searchParam.SearchText[2].ColumnValue = '';
        this.searchParam.SearchText[3].ColumnValue = '';
        this.searchParam.SearchText[4].ColumnValue = '';

        for (let i of this.dynamicSearchColumns) {
            i.ColumnValue = '';
        }



        for (let i in this.additionalSearch[0]) {
            this.additionalSearch[0].ColumnName = "";
            this.additionalSearch[0].ColumnValue = "";
            this.additionalSearch[0].LogicalOperator = "AND";
            this.additionalSearch[0].Operator = "";
        }
        (<any>$('.resetSelect')).prop('selectedIndex', 0);
        (<any>$('.resetDynamicInput')).val('');
        for (var i = this.additionalSearch.length; i > 0; i--) {

            this.additionalSearch.splice(i, 1);
        }
        this.PageSizeDynamic = 10;
        this.search(1);
    }
    selectSize() {
        this.search(1);
    }
    // addCC() {
    //     if (this.ccEmailTemp != "") {
    //         this.ccEmails.push(this.ccEmailTemp);
    //         (<any>$('#cc-email-id')).val('');
    //     }
    //     this.ccEmailTemp = "";
    // }

    // removeCCEmail(i) {
    //     this.ccEmails.splice(i, 1);
    // }

    addSearch() {
        if (this.additionalSearch.length > 0) {
            if (this.additionalSearch[this.additionalSearch.length - 1].ColumnName && this.additionalSearch[this.additionalSearch.length - 1].LogicalOperator && this.additionalSearch[this.additionalSearch.length - 1].Operator) {
                this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
            }
        }
        else {
            this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
        }
    }

    removeSearch(index: number) {
        if (this.additionalSearch.length > 1) {
            //this.additionalSearch.splice(-1,1);
            this.additionalSearch.splice(index, 1);
        }
    }

    // addCC(e)
    // {
    //     this.ccEmailIds = e.target.value;
    // }

    // addRemark(e) {
    //     this.remarks = e.target.value;
    // }

    bulkEmailBtnClick() {
        if (this.checkedReportNo.length == 0) {
            swal('', "Please select at least 1 report no");
        }
        else {
            this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
            // this.userDetail.EmailId = this.userDetail.EmailId;
            (<any>$('#CCRemark')).val('');
            (<any>$('#cc-email-id')).val('');
            this.ccEmails = [];
            (<any>$('#bulkEmailSend')).modal('show');
        }
    }

    cancelBulkEmail() {
        this.ccEmails = [];
        this.remarks[0] = "";
        this.checkedReportNo = [];
        // this.clientEmailBulkEmail[0] = this.userDetail.EmailId;
        (<any>$('#to')).val(this.userDetail.EmailId);
    }

    submitBatchEmail(e) {
        let files: any = [];
        if (this.userDetail.EmailId == "") {
            swal('', "Please enter emailid");
            return;
        } else {
            this.bulkEmailSendObject.EmailD = this.userDetail.EmailId;
        }


        var char = String.fromCharCode(255);

        for (let i in this.bulkEmailFilesObj) {

            if (this.bulkEmailFilesObj[i] != "") {
                this.bulkEmailFilesObj[i];
                files.push(this.bulkEmailFilesObj[i].replace(/,/g, char));
            }
        }

        // this.bulkEmailSendObject.EmailD = this.userDetail.EmailId;
        this.bulkEmailSendObject.Subject = "MTS - Test Report No. : " + this.checkedReportNo.join('/');
        this.bulkEmailSendObject.Body = this.remarks[0] ? this.remarks[0] : "";
        this.bulkEmailSendObject.FileName = files.join(char);
        this.bulkEmailSendObject.CreatedBy = this.clientId;
        this.bulkEmailSendObject.CCEmailID = this.ccEmails.join(',');
        this.bulkEmailSendObject.FileType = "Report";
        this.bulkEmailSendObject.BusinessCategoryId = this.userCredential.BusinessCategoryId;
        this.bulkEmailSendObject.CompanyName = this.userDetail.CompanyName;


        //  this.cancelBulkEmail();
        var that = this;
        this.dataService.post('Order/BulkEmailTOClient', this.bulkEmailSendObject).subscribe(
            response => {
                //this.isLoading = false;
                if (response.IsSuccess) {
                    swal('', response.Message);
                    //this.p = page;
                    //this.orderList = response.Dt;
                    that.checkedReportNo = [];
                    that.ccEmails = [];
                    that.userDetail.EmailId = "";
                    (<any>$('#bulkEmailSend')).modal('hide');
                }
                else {
                    //that.userDetail.EmailId="";
                    //that.checkedReportNo = [];
                    swal('', response.Message);
                }
                that.bulkEmailFilesObj = [];
            }
        )
    }

    handleCheckboxCheck(e: any, order: any) {
        if (e.target.checked) {
            let files: any = [];
            this.checkedReportNo.push(order.ReportNo);
            if (order.Report) {
                files.push(order.Report);
            }
            this.bulkEmailFilesObj[order.ReportNo] = files.join(',');

        }
        else {
            if (this.checkedReportNo.includes(order.ReportNo)) {
                let index = this.checkedReportNo.indexOf(order.ReportNo);
                this.checkedReportNo.splice(index, 1);
                delete this.bulkEmailFilesObj[order.ReportNo];
            }
        }
    }

    search(Type: any) {


        let dynamicSearchLength = 0;
        let thisObj = this;

        if (Type == 1) {
            this.isDashboard = false;
        }

        if (this.isDashboard) {
            if (this.injectService.additionalSearch) {


                if (this.searchParam.SearchText.length > 5) {
                    var count = this.searchParam.SearchText.length - 5;
                    this.searchParam.SearchText.splice(5, count);
                }
                var that = this;
                this.injectService.additionalSearch.forEach(function (addObj) {
                    that.searchParam.SearchText.push(addObj);
                });
                //addded by Divyanshu
                this.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
                //this.isDashboard = true;

            }
        }
        else {
            /*below for solving bug ot searching and sorting*/
            if (thisObj.dateRange.StartDate && thisObj.dateRange.EndDate) {
                thisObj.searchModal.StartDate = JSON.parse(JSON.stringify(thisObj.dateRange.StartDate));
                thisObj.searchModal.EndDate = JSON.parse(JSON.stringify(thisObj.dateRange.EndDate));
            }
            thisObj.searchModal.SearchText = JSON.parse(JSON.stringify(thisObj.searchParam.SearchText));
            /*ended*/
            if (thisObj.dynamicSearchColumns) {
                dynamicSearchLength = thisObj.dynamicSearchColumns.length;
                thisObj.searchModal.SearchText.splice(5, thisObj.searchModal.SearchText.length - 5);
                thisObj.dynamicSearchColumns.forEach(function (column) {
                    thisObj.searchModal.SearchText.push({ "LogicalOperator": "AND", "ColumnName": column.ColumnName, "Operator": 5, "ColumnValue": column.ColumnValue })
                })
            }

            if (thisObj.searchModal.SearchText.length > 5 + dynamicSearchLength) {
                let count = thisObj.searchModal.SearchText.length - 5 + dynamicSearchLength;
                thisObj.searchModal.SearchText.splice(5 + dynamicSearchLength, count);
            }
            thisObj.additionalSearch.forEach(function (addObj) {
                thisObj.searchModal.SearchText.push(addObj);
            })


        }
        this.searchModal.PageSize = this.PageSizeDynamic;
        thisObj.searchModal.PageNumber = 1;
        if (Type == 1) {
            thisObj.getOrderList();
        }
        else {
            thisObj.ExportToExcelTrack();
        }
    }

    getPage(page: number) {
        this.searchModal.PageNumber = page;
        this.isLoading = true;
        this.dataService.post('Order/GetAllOrderDynamicListWithView', this.searchModal).subscribe(
            response => {
                this.isLoading = false;
                if (response.IsSuccess) {
                    this.p = page;
                    this.orderList = response.Dt;
                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }

    sorting(column: string) {

        this.searchModal.OrderBy = column;
        if (this.searchModal.Order == 'asc') {
            this.searchModal.Order = 'desc';
        }
        else {
            this.searchModal.Order = 'asc';
        }
        this.isLoading = true;
        this.dataService.post('Order/GetAllOrderDynamicListWithView', this.searchModal).subscribe(
            response => {
                this.isLoading = false;
                if (response.IsSuccess) {
                    this.orderList = response.Dt;
                    this.totalItem = Math.ceil(response.TotalRecords / this.searchModal.PageSize);
                }
                else {
                    swal('', 'Sorting is not enabled for this information');
                }
            }
        )
    }

    showDownload() {
        this.showDownloadFiles = this.showDownloadFiles == true ? this.showDownloadFiles = false : this.showDownloadFiles = true;
    }

    download(fileName: any, data: any, reportNo) {
        this.fileType = data;
        this.ReportNo = reportNo;
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        this.userDetail.EmailId = this.userDetail.EmailId;
        (<any>$('#DownloadFile')).modal('show');
        if (data == 'TRF') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        else if (data == 'Report') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        else if (data == 'SUP' || data == 'GCC') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        else if (data = 'Invoice') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        // else {
        //     this.bulkEmailSendObject.type = "Invoice";
        //     this.DownloadfileName = fileName.trim();
        // }
        this.checkedReportNo = [];
        this.ccEmails = [];
        this.remarks = [];
        this.showDownloadFiles = false;
        (<any>$('.CheckPop')).prop('checked', false)
        // let downloadFileName = encodeURIComponent(fileName.trim());
        // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim();;
    }

    downloadFile() {
        var that = this;
        if (that.showDownloadFiles && that.userDetail.EmailId == "") {
            swal('', "Please enter emailid");
            return;
        } else {
            that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
        }
        if (this.showDownloadFiles == true) {
            // that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
            that.bulkEmailSendObject.ReportNo = that.ReportNo;
            that.bulkEmailSendObject.Subject = "MTS - Test Report No.: " + that.ReportNo;
            that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
            that.bulkEmailSendObject.FileName = that.DownloadfileName;
            that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
            that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
            that.bulkEmailSendObject.FileStatus = 'D';
            that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
            that.bulkEmailSendObject.FileType = that.fileType;
            that.bulkEmailSendObject.BusinessCategoryId = this.userCredential.BusinessCategoryId;
            that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
                response => {
                    if (response.IsSuccess) {
                        swal('', response.Message);
                        that.checkedReportNo = [];
                        that.ccEmails = [];
                        that.remarks = [];
                        that.userDetail.EmailId = "";
                        (<any>$('#DownloadFile')).modal('hide');

                        // let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
                        // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "";
                        // window.location.href = BaseUrl + "ManageDocuments/DownloadOrderFile?FileName=" + downloadFileName.trim() + "&ReportNo="+that.ReportNo;
                    }
                    else {
                        swal('', response.Message);
                    }

                }
            )
        }
        else {
            let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
            // window.location.href = BaseUrl + "ManageDocuments/DownloadOrderFile?FileName=" + downloadFileName.trim() + "&ReportNo="+that.ReportNo;
            //window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "";
            window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "&fileType=" + that.fileType + "&businessCategoryId=" + this.userCredential.BusinessCategoryId;
        }
    }


    ExportToExcelTrack() {
        this.isLoading = true;
        var that = this;
        this.searchModal.PageNumber = 1;
        this.dataService.post('OrderAnalysis/ExportToExcelTracking', this.searchModal).subscribe(
            response => {
                that.isLoading = false;
                if (response.IsSuccess) {
                    let downloadFileName = encodeURIComponent(response.Message);
                    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                    //that.isLoading=false;
                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }


    getOrderList() {
        this.isLoading = true;
        this.searchModal.PageSize = this.PageSizeDynamic;
        this.dataService.post('Order/GetAllOrderDynamicListWithView', this.searchModal).subscribe(
            response => {
                this.isLoading = false;
                if (response.IsSuccess) {
                    this.orderList = response.Dt;
                    this.totalItem = Math.ceil(response.TotalRecords / this.searchModal.PageSize);
                    if (response.Dt.length > 0) {
                        this.clientEmail = response.Dt[0].ClientEmail;
                        this.clientId = response.Dt[0].ClientID;
                    }

                    this.checkedReportNo = [];

                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }
    //Divyanhsu : Change following as viewRatingHistory because  viewHistory name is more suitable for viewHistory Modal pop Up
    viewRatingHistory(obj) {
        this.reportNo = obj.ReportNo;
        this.viewHistoryList = [];
        let url = '';
        if (obj.ReportRating.toLowerCase() == 'fail') {
            url = 'Order/GetPassOrderTestHistory?ReportNo=' + obj.ReportNo;
        }
        else if (obj.ReportRating.toLowerCase() != 'fail') {
            url = 'Order/GetPassOrderTestHistory?ReportNo=' + obj.ReportNo;
        }
        this.isLoading = true;
        this.dataService.get(url).subscribe(
            response => {
                (<any>$('#ViewFail')).modal('show');
                this.isLoading = false;
                this.viewHistoryList = response.FailCodeList;
            })
    }
    viewHistory(obj) {
        this.reportNo = obj.ReportNo;
        this.loginDate = obj.LogInTime;
        this.dueDate = obj.DueDate;
        this.rating = obj.ReportRating;
        this.dataService.get('Order/GetOrderHistory?ReportNo=' + this.reportNo).subscribe(
            response => {
                this.historyList = response.OrderHistory;
            }
        )
    }

    onDateRangeChanged(event: any) {
        this.dateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
        this.dateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
    }
    logoutonDateRangeChanged(event: any) {
        if(event.beginDate.day > '0' && event.endDate.day> '0'){
            this.LogOutDateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
            this.LogOutDateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
    
            this.searchParam.SearchText[3].ColumnValue = "'" + this.LogOutDateRange.StartDate + "' And '" + this.LogOutDateRange.EndDate + "'";
        }
        else{
            this.searchParam.SearchText[3].ColumnValue="";
        }
       
        
        // this.LogOutDateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
        // this.LogOutDateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;

        // this.searchParam.SearchText[3].ColumnValue = "'" + this.LogOutDateRange.StartDate + "' And '" + this.LogOutDateRange.EndDate + "'";
    }

    myDateRangePickerOptions = {
        clearBtnTxt: 'Clear',
        beginDateBtnTxt: 'Begin Date',
        endDateBtnTxt: 'End Date',
        acceptBtnTxt: 'OK',
        dateFormat: 'dd/mm/yyyy',
        firstDayOfWeek: 'mo',
        sunHighlight: true,
        height: '34px',
        width: '260px',
        inline: false,
        selectionTxtFontSize: '11px',
        alignSelectorRight: false,
        indicateInvalidDateRange: true,
        showDateRangeFormatPlaceholder: false,
        showClearBtn: false,
        customPlaceholderTxt: 'Login Date-Range'
    };

    openPopup(reportNo: string, type: string) {
        this.selectedReportNo = reportNo;
        this.isLoading = true;
        this.previousCommentList = [];
        (<any>$('#showComments')).modal('show');
        this.dataService.get('order/GetOrderPreviousComments?ReportNo=' + reportNo + "&userId=" + this.userCredential.UserId + "&UserType=" + type).subscribe(
            r => {
                this.isLoading = false;
                //  this.previousComments = true;
                this.previousCommentList = r.CommentsList;
            }
        )
    }

    showTestDetail() {
        this.showEmailTemplate = this.showEmailTemplate == true ? this.showEmailTemplate = false : this.showEmailTemplate = true;
    }

    GetPoSummary() {
        (<any>$('#DownloadTestDetail')).modal('show');
        (<any>$('.CheckSendEmail')).prop('checked', false)
    }

    closeDownloadPopup() {
        this.showEmailTemplate = false;
        (<any>$('#DownloadTestDetail')).modal('hide');
        this.EmailObject = new EmailObject();
    }

    ExcelWithTestRating() {
        let downloadFileName;
        var that = this;
        if (that.showEmailTemplate && (!this.EmailObject.ClientEmailTo && !this.EmailObject.EmailTo)) {
            swal('', "Please enter Email id");
            return;
        } else if (this.EmailObject.errorEmailTo) {
            return;
        } else if (this.EmailObject.errorEmailCC) {
            return;
        } this.isLoading = true;
        let dynamicSearchLength = 0;
        if (that.dateRange.StartDate && that.dateRange.EndDate) {
            that.searchModal.StartDate = JSON.parse(JSON.stringify(that.dateRange.StartDate));
            that.searchModal.EndDate = JSON.parse(JSON.stringify(that.dateRange.EndDate));
        }
        that.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
        if (that.dynamicSearchColumns) {
            dynamicSearchLength = that.dynamicSearchColumns.length;
            that.searchModal.SearchText.splice(5, that.searchModal.SearchText.length - 5);
            that.dynamicSearchColumns.forEach(function (column) {
                that.searchModal.SearchText.push({ "LogicalOperator": "AND", "ColumnName": column.ColumnName, "Operator": 5, "ColumnValue": column.ColumnValue })
            })
        }
        if (that.searchModal.SearchText.length > 5 + dynamicSearchLength) {
            let count = that.searchModal.SearchText.length - 5 + dynamicSearchLength;
            that.searchModal.SearchText.splice(5 + dynamicSearchLength, count);
        }
        that.additionalSearch.forEach(function (addObj) {
            that.searchModal.SearchText.push(addObj);
        })
        that.searchModal.PageSize = that.PageSizeDynamic;
        that.searchModal.PageNumber = 1;
        var that = this;
        this.dataService.post('OrderAnalysis/ExportToExcelPOSummary', this.searchModal).subscribe(
            response => {
                if (response.IsSuccess) {
                    downloadFileName = encodeURIComponent(response.Message);
                    if (this.showEmailTemplate == true) {
                        this.SendTestRatingEmail(downloadFileName)
                    } else {
                        this.isLoading = false;
                        downloadFileName = encodeURIComponent(downloadFileName.trim());
                        window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                        this.closeDownloadPopup();
                    }
                }
                else {
                    this.isLoading = false;
                    this.closeDownloadPopup();
                    swal('', response.Message);
                }
            })
    }

    SendTestRatingEmail(downloadFileName) {
        if (this.EmailObject.ccField) {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(this.EmailObject.ccField) == true) {
                if (this.EmailObject.ClientEmailCC) {
                    this.EmailObject.ClientEmailCC = this.EmailObject.ClientEmailCC + "," + this.EmailObject.ccField;
                } else {
                    this.EmailObject.ClientEmailCC = this.EmailObject.ccField;
                }
                this.EmailObject.ccField = "";
            }
        }
        if (this.EmailObject.EmailTo) {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(this.EmailObject.EmailTo) == true) {
                if (this.EmailObject.ClientEmailTo) {
                    this.EmailObject.ClientEmailTo = this.EmailObject.ClientEmailTo + "," + this.EmailObject.EmailTo;
                } else {
                    this.EmailObject.ClientEmailTo = this.EmailObject.EmailTo;
                }
                this.EmailObject.EmailTo = "";
            }
        }
        var that = this;
        that.bulkEmailSendObject.EmailD = this.EmailObject.ClientEmailTo;
        that.bulkEmailSendObject.Subject = "Data With Test Rating";
        that.bulkEmailSendObject.Body = this.EmailObject.clientremarks[0] ? this.EmailObject.clientremarks[0] : "";
        that.bulkEmailSendObject.FileName = downloadFileName;
        that.bulkEmailSendObject.CreatedBy = that.userCredential.UserId;
        that.bulkEmailSendObject.CCEmailID = this.EmailObject.ClientEmailCC;
        that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
        that.bulkEmailSendObject.FileStatus = 'U';
        that.bulkEmailSendObject.FileType = "excel";
        that.bulkEmailSendObject.BusinessCategoryId = this.userCredential.BusinessCategoryId;
        that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
            response => {
                if (response.IsSuccess) {
                    this.isLoading = false;
                    this.closeDownloadPopup();
                    swal('', response.Message);
                }
                else {
                    this.isLoading = false;
                    this.closeDownloadPopup();
                    swal('', response.Message);
                }
            }
        )
    }

    ccEmailsArray(event) {
        this.EmailObject = event
    }

    ngOnDestroy() {
        this.injectService.additionalSearch = [];
    }

    ExportToExcelTestReportSummary() {
        this.isLoading = true;
        let dynamicSearchLength = 0;
        let thisObj = this;
        thisObj.searchModal.OrderBy = "LogoutTime"
        if (thisObj.dateRange.StartDate && thisObj.dateRange.EndDate) {
            thisObj.searchModal.StartDate = JSON.parse(JSON.stringify(thisObj.dateRange.StartDate));
            thisObj.searchModal.EndDate = JSON.parse(JSON.stringify(thisObj.dateRange.EndDate));
        }
        thisObj.searchModal.SearchText = JSON.parse(JSON.stringify(thisObj.searchParam.SearchText));
        if (thisObj.dynamicSearchColumns) {
            dynamicSearchLength = thisObj.dynamicSearchColumns.length;
            thisObj.searchModal.SearchText.splice(5, thisObj.searchModal.SearchText.length - 5);
            thisObj.dynamicSearchColumns.forEach(function (column) {
                thisObj.searchModal.SearchText.push({ "LogicalOperator": "AND", "ColumnName": column.ColumnName, "Operator": 5, "ColumnValue": column.ColumnValue })
            })
        }

        if (thisObj.searchModal.SearchText.length > 5 + dynamicSearchLength) {
            let count = thisObj.searchModal.SearchText.length - 5 + dynamicSearchLength;
            thisObj.searchModal.SearchText.splice(5 + dynamicSearchLength, count);
        }
        thisObj.additionalSearch.forEach(function (addObj) {
            thisObj.searchModal.SearchText.push(addObj);
        })

        this.searchModal.PageSize = this.PageSizeDynamic;
        thisObj.searchModal.PageNumber = 1;
        var that = this;
        this.searchModal.PageNumber = 1;
        this.dataService.post('OrderAnalysis/ExportToExcelTestReportSummary', this.searchModal).subscribe(
            response => {
                that.isLoading = false;
                if (response.IsSuccess) {
                    let downloadFileName = encodeURIComponent(response.Message);
                    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }

    ExportToOverallTrackingReport(value) {
        this.isLoading = true;
        let dynamicSearchLength = 0;
        let thisObj = this;
        thisObj.searchModal.OrderBy = "LogoutTime"
        if (thisObj.dateRange.StartDate && thisObj.dateRange.EndDate) {
            thisObj.searchModal.StartDate = JSON.parse(JSON.stringify(thisObj.dateRange.StartDate));
            thisObj.searchModal.EndDate = JSON.parse(JSON.stringify(thisObj.dateRange.EndDate));
        }
        thisObj.searchModal.SearchText = JSON.parse(JSON.stringify(thisObj.searchParam.SearchText));
        if (thisObj.dynamicSearchColumns) {
            dynamicSearchLength = thisObj.dynamicSearchColumns.length;
            thisObj.searchModal.SearchText.splice(5, thisObj.searchModal.SearchText.length - 5);
            thisObj.dynamicSearchColumns.forEach(function (column) {
                thisObj.searchModal.SearchText.push({ "LogicalOperator": "AND", "ColumnName": column.ColumnName, "Operator": 5, "ColumnValue": column.ColumnValue })
            })
        }

        if (thisObj.searchModal.SearchText.length > 5 + dynamicSearchLength) {
            let count = thisObj.searchModal.SearchText.length - 5 + dynamicSearchLength;
            thisObj.searchModal.SearchText.splice(5 + dynamicSearchLength, count);
        }
        thisObj.additionalSearch.forEach(function (addObj) {
            thisObj.searchModal.SearchText.push(addObj);
        })

        this.searchModal.PageSize = this.PageSizeDynamic;
        thisObj.searchModal.PageNumber = 1;
        var that = this;
        this.searchModal.PageNumber = 1;
        if (value == 1) {
            this.dataService.post('SF/ExportToOverallTrackingReport', this.searchModal).subscribe(
                response => {
                    that.isLoading = false;
                    if (response.IsSuccess) {
                        let downloadFileName = encodeURIComponent(response.Message);
                        window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                    }
                    else {
                        swal('', response.Message);
                    }
                }
            )
        }
        else if (value == 2) {
            this.dataService.post('SF/ExportTestItemTrackingReport', this.searchModal).subscribe(
                response => {
                    that.isLoading = false;
                    if (response.IsSuccess) {
                        let downloadFileName = encodeURIComponent(response.Message);
                        window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                    }
                    else {
                        swal('', response.Message);
                    }
                }
            )
        }
    }

    ExportEDIDataExtraction(){
        let that=this;
        that.isLoading = true;
      that.ediDataExtractionSearchModal = new EDIDataExtractionRequest();
        that.ediDataExtractionSearchModal.ClientId = that.userCredential.CompanyId;
        if (that.ediShipDateRange.StartDate && that.ediShipDateRange.EndDate) {
            that.ediDataExtractionSearchModal.StartDate = JSON.parse(JSON.stringify(that.ediShipDateRange.StartDate));
            that.ediDataExtractionSearchModal.EndDate = JSON.parse(JSON.stringify(that.ediShipDateRange.EndDate));
        }
        
        this.dataService.post('SF/ExportEDIData/',that.ediDataExtractionSearchModal).subscribe(
            response => {
                that.isLoading = false;
                if (response.IsSuccess) {
                    let downloadFileName = encodeURIComponent(response.Message);
                    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                    this.resetShipDateFilter();
                }
                else {
                    this.resetShipDateFilter();
                    swal('', response.Message);
                }
            }
            
        )
        // this.dataService.get('SF/ExportEDIData/'+that.userCredential.CompanyId).subscribe(
        //     response => {
        //         that.isLoading = false;
        //         if (response.IsSuccess) {
        //             let downloadFileName = encodeURIComponent(response.Message);
        //             window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
        //         }
        //         else {
        //             swal('', response.Message);
        //         }
        //     }
        // )
    }

    OpenEDIDataExtractionPopup(){
        (<any>$('#EDIDataExtraction')).modal('show');
    
        this.shipDateRangeModel = {
            beginDate: { year: this.shipDateRangeStart.getFullYear(), month: this.shipDateRangeStart.getMonth()+1 , day: this.shipDateRangeStart.getDate() },
            endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() }
        };

        // this.ediShipDateRange.StartDate=this.shipDateRangeModel.beginDate;
        // this.ediShipDateRange.EndDate=this.shipDateRangeModel.endDate;


        this.ediShipDateRange.StartDate = this.shipDateRangeModel.beginDate.month + '/' + this.shipDateRangeModel.beginDate.day + '/' + this.shipDateRangeModel.beginDate.year;
        this.ediShipDateRange.EndDate = this.shipDateRangeModel.endDate.month + '/' + this.shipDateRangeModel.endDate.day + '/' + this.shipDateRangeModel.endDate.year;
        // this.ediShipDateRange.StartDate=(this.shipDateRangeStart.getMonth()) + "/" + this.shipDateRangeStart.getDate() + "/" + (this.shipDateRangeStart.getFullYear());
        // this.ediShipDateRange.EndDate=(this.date.getMonth()) + "/" + this.date.getDate() + "/" + (this.date.getFullYear());
        // this.ediShipDateRange = {
        //     beginDate: { year: this.dateRangeStart.getFullYear(), month: this.dateRangeStart.getMonth() - 1, day: this.dateRangeStart.getDate() },
        //     endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() }
        // };
    }

    ediShipmentDateRangePickerOptions = {
        clearBtnTxt: 'Clear',
        beginDateBtnTxt: 'Begin Date',
        endDateBtnTxt: 'End Date',
        acceptBtnTxt: 'OK',
        dateFormat: 'yyyy-mm-dd',
        firstDayOfWeek: 'mo',
        sunHighlight: true,
        height: '34px',
        width: '260px',
        inline: false,
        selectionTxtFontSize: '11px',
        alignSelectorRight: false,
        indicateInvalidDateRange: true,
        showDateRangeFormatPlaceholder: false,
        showClearBtn: false,
        customPlaceholderTxt: 'Shipment Date-Range'
    };

    onediShipmentDateRangeChanged(event: any) {
        this.ediShipDateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
        this.ediShipDateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
    }

    resetShipDateFilter(){
        (<any>$('.btnclearenabled')).click();
        (<any>$('#EDIDataExtraction')).modal('hide');
        this.shipDateRangeModel = {
            beginDate: { year: this.shipDateRangeStart.getFullYear(), month: this.shipDateRangeStart.getMonth() , day: this.shipDateRangeStart.getDate() },
            endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() }
        };

    }

}