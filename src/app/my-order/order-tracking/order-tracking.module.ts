import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { MdInputModule } from '@angular2-material/input';
import { MdCoreModule } from '@angular2-material/core';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { Ng2PaginationModule } from 'ng2-pagination';
import {TooltipModule} from "ngx-tooltip";
import { OrderTrackingRoutingModule } from './order-tracking-routing.module';
import { OrderTrackingComponent } from './order-tracking.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { BulkEmailModule } from '../../bulk-email/bulk-email.module';
import { CpEmailTemaplateModule } from 'app/cp-email-temaplate/cp-email-temaplate.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MyDateRangePickerModule,
    MdInputModule,
    MdCoreModule,
    MaterialModule,
    Ng2PaginationModule,
    OrderTrackingRoutingModule,
    HeaderModule,
    FooterModule,
    BulkEmailModule,
    TooltipModule   ,
    CpEmailTemaplateModule
  ],
  declarations: [OrderTrackingComponent]
})
export class OrderTrackingModule { }
