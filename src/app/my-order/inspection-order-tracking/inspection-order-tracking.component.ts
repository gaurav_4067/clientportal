import { Component, OnInit, OnDestroy } from '@angular/core';
import { SEARCH_MODEL } from './inspection-order-tracking.model';
import { DataService, BaseUrl, COLUMN_LIST } from '../../mts.service';
import { Http, Response } from '@angular/http';
import { InjectService } from '../../injectable-service/injectable-service';
import { Hero } from '../../common/model_1';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { ActivatedRoute, Router } from '@angular/router';
import 'table2excel';

declare let swal: any;

@Component({
  selector: 'app-inspection-order-tracking',
  templateUrl: './inspection-order-tracking.component.html',
  styleUrls: ['./inspection-order-tracking.component.css']
})
export class InspectionOrderTrackingComponent implements OnInit, OnDestroy {

  orderList: any;
  dateRange: any;
  LogOutDateRange: any;
  private p: any;
  private totalItem: any;
  private itemPerPage: any;
  searchModal: any;
  searchText: any;
  userDetail: any;
  dynamicSearchColumns: any;
  dynamicGridColumns: any = [];
  dynamicGridColumnsNew: any = [];
  isLoading: boolean;
  viewHistoryList: any;
  reportNo: any;
  loginDate: any;
  dueDate: any;
  rating: any;
  additionalSearch: any;
  userCredential: any;
  columnList: any;
  historyList: any;
  searchParam: any;
  checkedReportNo: any = [];
  clientEmail: any;
  clientId: any;
  bulkEmailSubject: any = 'MTS - Test Report :';
  bulkEmailFiles: any;
  bulkEmailFilesObj: any = {}; // its for temp storage
  ccEmailIds: any = null;
  remarks: any = [];
  repeatedCCFields: any;
  repeatedCCArray: any = [];
  abc: any = 5;
  ccEmails: any = [];
  ccEmailTemp: any;
  errorEmail: boolean;
  showDownloadFiles: boolean = false;
  ReportNo: number;
  DownloadfileName: any;
  isDashboard: any = false;
  PageSizeDynamic: number = 10;
  clientEmailBulkEmail: any = [];
  bulkEmailSendObject: any = {
    EmailLogID: 0,
    EmailD: '',
    Subject: '',
    Body: '',
    CompanyName: '',
    Status: false,
    FileName: '',
    CreatedDate: Date.now(),
    type: '',
    CreatedBy: '',
    ModifiedBy: 0,
    ModifiedDate: Date.now(),
    CCEmailID: '',
    FileType: '',
    BusinessCategoryId: 0
  }
  fileType: string;

  constructor(private dataService: DataService, private injectService: InjectService, private route: ActivatedRoute, private router: Router) {
    this.searchModal = SEARCH_MODEL;
    this.searchModal.PageSize = this.PageSizeDynamic;
    this.searchModal.PageNumber = 1;
    this.searchModal.StartDate = '';
    this.searchModal.EndDate = '';
    this.searchModal.OrderBy = 'Bookingid';
    this.searchModal.Order = 'desc';
    this.searchModal.FormId = '1';
    this.dateRange = {};
    this.LogOutDateRange = {};
    this.ccEmails = [];
    this.remarks = [];
    this.ccEmailTemp = "";
    this.errorEmail = false;
    this.clientEmail = '';
  }
  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    // this.clientEmailBulkEmail.push(that.userDetail.EmailId);
    /*Divyanshu: Following if block is for checking AuthToken for each Component
   before doing any further task,please discuss with me before removing it.
   */
    if (this.userDetail && this.userCredential) {
      if (this.userCredential.UserPermission.ViewOrderTracking) {
        this.searchModal.BusinessCategoryId = this.userCredential.BusinessCategoryId;
        this.searchModal.ClientId = this.userCredential.CompanyId;  // It will ClientId
        this.searchModal.UserId = this.userCredential.UserId;
        this.searchModal.UserType = this.userCredential.UserType;
        this.searchModal.UserCompanyId = this.userCredential.UserCompanyId;      //It will VendorId
        // this.columnList = COLUMN_LIST;
        this.p = 1;
        this.itemPerPage = 1;
        this.totalItem = 0;
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        /*below for solving bug ot searching and sorting*/
        this.searchParam = {};
        this.searchParam.SearchText = [{ LogicalOperator: "AND", ColumnName: "ReportNo", Operator: 5, ColumnValue: "" },
        { LogicalOperator: "AND", ColumnName: "ReportIssueDate", Operator: 6, ColumnValue: "" }
          // { LogicalOperator: "AND", ColumnName: "StyleNo", Operator: 5, ColumnValue: "" },
          // { LogicalOperator: "AND", ColumnName: "SampleDescription", Operator: 5, ColumnValue: "" },
        ];
        this.searchModal.SearchText = [];
        /*ended*/

        this.additionalSearch = [{ LogicalOperator: "AND", ColumnName: "", Operator: "", ColumnValue: "" }];
        //global search
        var that = this;
        this.route.params.subscribe(params => {

          that.searchModal.SearchText = JSON.parse(JSON.stringify(that.searchParam.SearchText));
          that.searchModal.SearchText[0].ColumnValue = params['search'];
        });
        this.isLoading = true;
        if (this.injectService.additionalSearch) {
          if (this.searchParam.SearchText.length > 4) {
            var count = this.searchParam.SearchText.length - 4;
            this.searchParam.SearchText.splice(4, count);
          }
          var that = this;
          this.injectService.additionalSearch.forEach(function (addObj) {
            that.searchParam.SearchText.push(addObj);
          });
          //addded by Divyanshu
          this.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
          this.isDashboard = true;
        }
        else {
          this.isDashboard = false;
        }
        /*ended*/
        this.dataService.get('Order/GetMyOrderColumnsList?selectedCompany=' + this.searchModal.ClientId + '&FormId=1&BusinessCategoryId=' + this.searchModal.BusinessCategoryId).subscribe(
          response => {
            if (response.IsSuccess) {
              this.dynamicSearchColumns = response.SelectedSearchColumn;
              //this.dynamicGridColumns = response.SelectedGridColumn;
              this.columnList = response.SelectedAdHocSearchColumn;
              response.SelectedGridColumn.forEach(function (addObj, i) {
                if (addObj.ColumnName == 'GCC' || addObj.ColumnName == 'SUP') {
                  that.dynamicGridColumnsNew.push(addObj);
                }
                else {
                  that.dynamicGridColumns.push(addObj);
                }
              });

              this.getOrderList();

            }
            else {
              this.getOrderList();
              swal('', response.Message)
            }
          }
        );
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
    /* following  lines of code moved from constructor to ngOnInit ,because cross browser issue,
   please discuss before remove
   */
  }

  getCCEmail(e) {
    var val = e.target.value;
    if (val.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
      this.errorEmail = false;
      this.ccEmailTemp = val;
    }
    else if (val == "") {
      this.errorEmail = false;
    }
    else {
      this.errorEmail = true;
    }
  }

  dateFocusIn() {
    // (<any> $('.custom-material')).
  }

  resetSearchForm() {
    this.PageSizeDynamic = 10;
    (<any>$('.btnclearenabled')).click();
    this.searchParam.SearchText[0].ColumnValue = '';
    // this.searchParam.SearchText[1].ColumnValue = '';
    // this.searchParam.SearchText[2].ColumnValue = '';
    // this.searchParam.SearchText[3].ColumnValue = '';

    for (let i of this.dynamicSearchColumns) {
      i.ColumnValue = '';
    }

    for (let i in this.additionalSearch[0]) {
      this.additionalSearch[0].ColumnName = "";
      // this.additionalSearch[0].ColumnValue = "";
      // this.additionalSearch[0].LogicalOperator = "AND";
      // this.additionalSearch[0].Operator = "";
    }
    (<any>$('.resetSelect')).prop('selectedIndex', 0);
    (<any>$('.resetDynamicInput')).val('');
    for (var i = this.additionalSearch.length; i > 0; i--) {

      this.additionalSearch.splice(i, 1);
    }
    this.PageSizeDynamic = 10;
    this.search(1);
  }
  selectSize() {
    this.search(1);
  }

  addSearch() {
    if (this.additionalSearch.length > 0) {
      if (this.additionalSearch[this.additionalSearch.length - 1].ColumnName && this.additionalSearch[this.additionalSearch.length - 1].LogicalOperator && this.additionalSearch[this.additionalSearch.length - 1].Operator) {
        this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
      }
    }
    else {
      this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
    }
  }

  removeSearch(index: number) {
    if (this.additionalSearch.length > 1) {
      //this.additionalSearch.splice(-1,1);
      this.additionalSearch.splice(index, 1);
    }
  }

  bulkEmailBtnClick() {
    if (this.checkedReportNo.length == 0) {
      swal('', "Please select at least 1 report no");
    }
    else {
      this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
      // this.userDetail.EmailId = this.userDetail.EmailId;
      (<any>$('#CCRemark')).val('');
      (<any>$('#cc-email-id')).val('');
      this.ccEmails = [];
      (<any>$('#bulkEmailSend')).modal('show');
    }
  }

  cancelBulkEmail() {
    this.ccEmails = [];
    this.remarks[0] = "";
    this.checkedReportNo = [];
    // this.clientEmailBulkEmail[0] = this.userDetail.EmailId;
    (<any>$('#to')).val(this.userDetail.EmailId);
  }

  submitBatchEmail(e) {
    let files: any = [];
    if (this.userDetail.EmailId == "") {
      swal('', "Please enter emailid");
      return;
    } else {
      this.bulkEmailSendObject.EmailD = this.userDetail.EmailId;
    }


    var char = String.fromCharCode(255);

    for (let i in this.bulkEmailFilesObj) {

      if (this.bulkEmailFilesObj[i] != "") {
        this.bulkEmailFilesObj[i];
        files.push(this.bulkEmailFilesObj[i].replace(/,/g, char));
      }
    }

    // this.bulkEmailSendObject.EmailD = this.userDetail.EmailId;
    this.bulkEmailSendObject.Subject = "MTS - Test Report No. : " + this.checkedReportNo.join('/');
    this.bulkEmailSendObject.Body = this.remarks[0] ? this.remarks[0] : "";
    this.bulkEmailSendObject.FileName = files.join(char);
    this.bulkEmailSendObject.CreatedBy = this.clientId;
    this.bulkEmailSendObject.CCEmailID = this.ccEmails.join(',');
    this.bulkEmailSendObject.FileType = "Report";
    this.bulkEmailSendObject.BusinessCategoryId = this.userCredential.BusinessCategoryId;
    this.bulkEmailSendObject.CompanyName = this.userDetail.CompanyName;


    //  this.cancelBulkEmail();
    var that = this;
    this.dataService.post('Order/BulkEmailTOClient', this.bulkEmailSendObject).subscribe(
      response => {
        //this.isLoading = false;
        if (response.IsSuccess) {
          swal('', response.Message);
          //this.p = page;
          //this.orderList = response.Dt;
          that.checkedReportNo = [];
          that.ccEmails = [];
          that.userDetail.EmailId = "";
          (<any>$('#bulkEmailSend')).modal('hide');
        }
        else {
          //that.userDetail.EmailId="";
          //that.checkedReportNo = [];
          swal('', response.Message);
        }
        that.bulkEmailFilesObj = [];
      }
    )
  }

  handleCheckboxCheck(e: any, order: any) {
    if (e.target.checked) {
      let files: any = [];
      this.checkedReportNo.push(order.ReportNo);
      if (order.Report) {
        files.push(order.Report);
      }
      this.bulkEmailFilesObj[order.ReportNo] = files.join(',');

    }
    else {
      if (this.checkedReportNo.includes(order.ReportNo)) {
        let index = this.checkedReportNo.indexOf(order.ReportNo);
        this.checkedReportNo.splice(index, 1);
        delete this.bulkEmailFilesObj[order.ReportNo];
      }
    }
  }

  search(Type: any) {

    let dynamicSearchLength = 0;
    let thisObj = this;

    if (Type == 1) {
      this.isDashboard = false;
    }

    if (this.isDashboard) {
      if (this.injectService.additionalSearch) {


        if (this.searchParam.SearchText.length > 4) {
          var count = this.searchParam.SearchText.length - 4;
          this.searchParam.SearchText.splice(4, count);
        }
        // var that = this;
        // this.injectService.additionalSearch.forEach(function (addObj) {
        //   that.searchParam.SearchText.push(addObj);
        // });
        //addded by Divyanshu
        this.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
        //this.isDashboard = true;

      }
    }
    else {
      /*below for solving bug ot searching and sorting*/
      if (thisObj.dateRange.StartDate && thisObj.dateRange.EndDate) {
        thisObj.searchModal.StartDate = JSON.parse(JSON.stringify(thisObj.dateRange.StartDate));
        thisObj.searchModal.EndDate = JSON.parse(JSON.stringify(thisObj.dateRange.EndDate));
      }
      thisObj.searchModal.SearchText = JSON.parse(JSON.stringify(thisObj.searchParam.SearchText));
      /*ended*/
      if (thisObj.dynamicSearchColumns) {
        dynamicSearchLength = thisObj.dynamicSearchColumns.length;
        thisObj.searchModal.SearchText.splice(4, thisObj.searchModal.SearchText.length - 4);
        thisObj.dynamicSearchColumns.forEach(function (column) {
          thisObj.searchModal.SearchText.push(
            { "LogicalOperator": "AND", "ColumnName": column.ColumnName, "Operator": 5, "ColumnValue": column.ColumnValue })
        })
      }

      if (thisObj.searchModal.SearchText.length > 4 + dynamicSearchLength) {
        let count = thisObj.searchModal.SearchText.length - 4 + dynamicSearchLength;
        thisObj.searchModal.SearchText.splice(4 + dynamicSearchLength, count);
      }
      thisObj.additionalSearch.forEach(function (addObj) {
        thisObj.searchModal.SearchText.push(addObj);
      })


    }
    this.searchModal.PageSize = this.PageSizeDynamic;
    thisObj.searchModal.PageNumber = 1;
    if (Type == 1) {
      thisObj.getOrderList();
    }
    else {
      thisObj.ExportToExcelTrack();
    }
  }

  getPage(page: number) {
    this.searchModal.PageNumber = page;
    this.isLoading = true;
    this.dataService.post('Order/Insp_GetAllOrderDynamicListWithView', this.searchModal).subscribe(
      response => {
        this.isLoading = false;
        if (response.IsSuccess) {
          this.p = page;
          this.orderList = response.Dt;
        }
        else {
          swal('', response.Message);
        }
      }
    )
  }

  sorting(column: string) {

    this.searchModal.OrderBy = column;
    if (this.searchModal.Order == 'asc') {
      this.searchModal.Order = 'desc';
    }
    else {
      this.searchModal.Order = 'asc';
    }
    this.isLoading = true;
    this.dataService.post('Order/Insp_GetAllOrderDynamicListWithView', this.searchModal).subscribe(
      response => {
        this.isLoading = false;
        if (response.IsSuccess) {
          this.orderList = response.Dt;
          this.totalItem = Math.ceil(response.TotalRecords / this.searchModal.PageSize);
        }
        else {
          swal('', 'Sorting is not enabled for this information');
        }
      }
    )
  }

  showDownload() {
    this.showDownloadFiles = this.showDownloadFiles == true ? this.showDownloadFiles = false : this.showDownloadFiles = true;
  }

  download(fileName: any, data: any, reportNo) {
    this.fileType = data;
    this.ReportNo = reportNo;
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userDetail.EmailId = this.userDetail.EmailId;
    (<any>$('#DownloadFile')).modal('show');
    if (data == 'TRF') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileName.trim();
    }
    else if (data == 'Report') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileName.trim();
    }
    else if (data == 'SUP' || data == 'GCC') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileName.trim();
    }
    else if (data == "Invoice") {
      this.bulkEmailSendObject.type = "Invoice";
      this.DownloadfileName = fileName.trim();
    }
    this.checkedReportNo = [];
    this.ccEmails = [];
    this.remarks = [];
    this.showDownloadFiles = false;
    (<any>$('.CheckPop')).prop('checked', false)
    // let downloadFileName = encodeURIComponent(fileName.trim());
    // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim();;
  }

  downloadFile() {

    var that = this;
    if (that.showDownloadFiles && that.userDetail.EmailId == "") {
      swal('', "Please enter emailid");
      return;
    } else {
      that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
    }
    if (this.showDownloadFiles == true) {
      // that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
      that.bulkEmailSendObject.ReportNo = that.ReportNo;
      that.bulkEmailSendObject.Subject = "MTS - Test Report No.: " + that.ReportNo;
      that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
      that.bulkEmailSendObject.FileName = that.DownloadfileName;
      that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
      that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
      that.bulkEmailSendObject.FileStatus = 'D';
      that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
      that.bulkEmailSendObject.FileType = that.fileType;
      that.bulkEmailSendObject.BusinessCategoryId = that.userCredential.BusinessCategoryId;
      that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
        response => {
          if (response.IsSuccess) {
            swal('', response.Message);
            that.checkedReportNo = [];
            that.ccEmails = [];
            that.remarks = [];
            that.userDetail.EmailId = "";
            (<any>$('#DownloadFile')).modal('hide');

            // let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
            // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "";
            // window.location.href = BaseUrl + "ManageDocuments/DownloadOrderFile?FileName=" + downloadFileName.trim() + "&ReportNo="+that.ReportNo;
          }
          else {
            swal('', response.Message);
          }

        }
      )
    }
    else {
      let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
      // window.location.href = BaseUrl + "ManageDocuments/DownloadOrderFile?FileName=" + downloadFileName.trim() + "&ReportNo="+that.ReportNo;
      window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "&fileType=" + that.fileType + "&businessCategoryId=" + this.userCredential.BusinessCategoryId;
    }
  }


  ExportToExcelTrack() {
    this.isLoading = true;
    var that = this;
    this.searchModal.PageNumber = 1;
    this.dataService.post('OrderAnalysis/Insp_ExportToExcelTracking', this.searchModal).subscribe(
      response => {
        that.isLoading = false;
        if (response.IsSuccess) {
          let downloadFileName = encodeURIComponent(response.Message);
          window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId="+this.userCredential.BusinessCategoryId;
          //that.isLoading=false;
        }
        else {
          swal('', response.Message);
        }
      }
    )
  }


  getOrderList() {
    this.isLoading = true;
    this.searchModal.PageSize = this.PageSizeDynamic;
    this.dataService.post('Order/Insp_GetAllOrderDynamicListWithView', this.searchModal).subscribe(
      response => {
        this.isLoading = false;
        if (response.IsSuccess) {
          this.orderList = response.Dt;
          this.totalItem = Math.ceil(response.TotalRecords / this.searchModal.PageSize);
          if (response.Dt.length > 0) {
            this.clientEmail = response.Dt[0].ClientEmail;
            this.clientId = response.Dt[0].ClientID;
          }

          this.checkedReportNo = [];

        }
        else {
          swal('', response.Message);
        }
      }
    )
  }
  //Divyanhsu : Change following as viewRatingHistory because  viewHistory name is more suitable for viewHistory Modal pop Up
  viewRatingHistory(obj) {
    this.reportNo = obj.ReportNo;
    this.viewHistoryList = [];
    let url = '';
    if (obj.ReportRating.toLowerCase() == 'fail') {
      url = 'Order/GetPassOrderTestHistory?ReportNo=' + obj.ReportNo;
    }
    else if (obj.ReportRating.toLowerCase() != 'fail') {
      url = 'Order/GetPassOrderTestHistory?ReportNo=' + obj.ReportNo;
    }
    this.isLoading = true;
    this.dataService.get(url).subscribe(
      response => {
        (<any>$('#ViewFail')).modal('show');
        this.isLoading = false;
        this.viewHistoryList = response.FailCodeList;
      })
  }
  viewHistory(obj) {
    this.reportNo = obj.ReportNo;
    this.loginDate = obj.LogInTime;
    this.dueDate = obj.DueDate;
    this.rating = obj.ReportRating;
    this.dataService.get('Order/GetOrderHistory?ReportNo=' + this.reportNo).subscribe(
      response => {
        this.historyList = response.OrderHistory;
      }
    )
  }

  onDateRangeChanged(event: any) {
    this.dateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
    this.dateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
  }
  logoutonDateRangeChanged(event: any) {
    if (event.formatted) {
      this.LogOutDateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
      this.LogOutDateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
      this.searchParam.SearchText[1].ColumnValue = "'" + this.LogOutDateRange.StartDate + "' And '" + this.LogOutDateRange.EndDate + "'";
    } else {
      this.searchParam.SearchText[1].ColumnValue = "";
    }
  }
  myDateRangePickerOptions = {
    clearBtnTxt: 'Clear',
    beginDateBtnTxt: 'Begin Date',
    endDateBtnTxt: 'End Date',
    acceptBtnTxt: 'OK',
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'mo',
    sunHighlight: true,
    height: '34px',
    width: '260px',
    inline: false,
    selectionTxtFontSize: '11px',
    alignSelectorRight: false,
    indicateInvalidDateRange: true,
    showDateRangeFormatPlaceholder: false,
    showClearBtn: false,
    customPlaceholderTxt: 'Login Date-Range'
  };
  ngOnDestroy() {
    this.injectService.additionalSearch = [];
  }

}
