import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InspectionOrderTrackingRoutingModule } from './inspection-order-tracking-routing.module';
import { InspectionOrderTrackingComponent } from './inspection-order-tracking.component';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { FormsModule } from '@angular/forms';
import { MdInputModule } from '@angular2-material/input';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { Ng2PaginationModule } from 'ng2-pagination';
import {TooltipModule} from "ngx-tooltip";
import { MdCoreModule } from '@angular2-material/core';
import { MaterialModule } from '@angular/material';
import { BulkEmailModule } from 'app/bulk-email/bulk-email.module';

@NgModule({
  imports: [
    CommonModule,
    InspectionOrderTrackingRoutingModule,
    MyDateRangePickerModule,
    FormsModule,
    MdInputModule,
    HeaderModule,
    FooterModule,
    Ng2PaginationModule,
    TooltipModule,
    MdCoreModule,
    MaterialModule,
    BulkEmailModule
  ],
  declarations: [InspectionOrderTrackingComponent]
})
export class InspectionOrderTrackingModule { }
