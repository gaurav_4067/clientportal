import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionOrderTrackingComponent } from './inspection-order-tracking.component';

describe('InspectionOrderTrackingComponent', () => {
  let component: InspectionOrderTrackingComponent;
  let fixture: ComponentFixture<InspectionOrderTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionOrderTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionOrderTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
