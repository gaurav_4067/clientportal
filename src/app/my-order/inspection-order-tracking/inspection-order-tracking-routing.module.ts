import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InspectionOrderTrackingComponent } from 'app/my-order/inspection-order-tracking/inspection-order-tracking.component';

const routes: Routes = [{
  path:'',
  component:InspectionOrderTrackingComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InspectionOrderTrackingRoutingModule { }
