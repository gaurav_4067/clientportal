import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyOrderRoutingModule } from './my-order-routing.module';
import { OrderCommunicationModule } from './order-communication/order-communication.module';
import { NoContentModule } from '../no-content/no-content.module';

@NgModule({
  imports: [
    CommonModule,
    MyOrderRoutingModule,
    NoContentModule,
    OrderCommunicationModule
  ],
  declarations: []
})
export class MyOrderModule { }
