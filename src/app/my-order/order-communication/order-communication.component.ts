
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { SEARCH_MODEL } from './order-communication.model';
import { DataService, COLUMN_LIST, Laboratyemailid, BaseUrl, ETRFUrl } from '../../mts.service';
import { Response } from '@angular/http';
import { InjectService } from '../../injectable-service/injectable-service';
import { Hero } from '../common/model_1';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { Router } from '@angular/router';
import { CCEmailList, T_ShareComments } from './order-communication.model'

declare let swal: any;
@Component({
    selector: 'app-order-communication',
    templateUrl: './order-communication.component.html',
    styleUrls: ['./order-communication.component.css']
})
export class OrderCommunicationComponent implements OnInit {
    selectedReportNo: string;
    @ViewChild('fileUpload') fileUploader: ElementRef;
    orderList: any;
    private p: any;
    private totalItem: number;
    private itemPerPage: any;
    private dateRange: any;
    private LogOutDateRange: any;
    vendorId: any;

    searchModal: any;
    searchText: any;
    shareCommentObj: any;
    userCredential: any;
    userDetail: any;
    previousComments: boolean;
    previousCommentList: any;
    isLoader: boolean;
    viewHistoryList: any;
    reportNo: string;
    additionalSearch: any;
    testHistoryMessage: string;
    columnList: any;
    searchParam: any;
    dynamicSearchColumns: any;
    dynamicGridColumns: any;
    PageSizeDynamic: any = 10;
    ccEmails: any = [];
    ccEmailTemp: any;
    errorEmail: boolean;
    showDownloadFiles: boolean = false;
    ReportNo: number;
    DownloadfileName: any;
    remarks: any = [];
    bulkEmailSendObject: any = {
        EmailLogID: 0,
        EmailD: '',
        Subject: '',
        Body: '',
        Status: false,
        FileName: '',
        CreatedDate: Date.now(),
        type: '',
        CreatedBy: '',
        CompanyName: '',
        ModifiedBy: 0,
        ModifiedDate: Date.now(),
        CCEmailID: '',
        FileType: '',
        BusinessCategoryid: 0
    }

    testFlag1: any = {};
    //New Model creation, set value here for EmailpopUp
    CCEmailList: Array<CCEmailList>;
    checkedCCEmail: Array<string>;
    CCMailId: string;
    ToMailId: string;
    isLaboratoryEmailCheckboxChecked: boolean = false;
    shareCommentInfo: T_ShareComments;
    fileType: string;
    constructor(private dataService: DataService, private injectService: InjectService, private router: Router) {
        this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
        this.searchModal = SEARCH_MODEL;
        this.searchModal.PageSize = this.PageSizeDynamic;
        this.searchModal.PageNumber = 1;
        this.searchModal.TotalRecords = 200;
        this.searchModal.OrderBy = 'LogoutTime';
        this.searchModal.Order = 'desc';
        this.searchModal.FormId = '2';
        this.CCEmailList = new Array<CCEmailList>();
        this.checkedCCEmail = new Array<string>();
        this.shareCommentInfo = new T_ShareComments;
    }
    ngOnInit() {
        this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        // this.clientEmailBulkEmail.push(that.userDetail.EmailId);
        /*Divyanshu: Following if block is for checking AuthToken for each Component
       before doing any further task,please discuss with me before removing it.
       */
        if (this.userDetail && this.userCredential) {
            if (this.userCredential.UserPermission.ViewOrderCommunication) {
                this.searchModal.ClientId = this.userCredential.CompanyId;
                this.searchModal.UserType = this.userCredential.UserType;
                this.searchModal.UserCompanyId = this.userCredential.UserCompanyId;
                this.previousCommentList = [{ comment: 'whatever' }, { comment: 'wow its working' }];
                // this.columnList = COLUMN_LIST;
                this.shareCommentObj = {};
                this.p = 1;
                this.itemPerPage = 1;
                this.totalItem = 0;
                this.dateRange = {};
                this.LogOutDateRange = {};
                /*below for solving bug ot searching and sorting*/
                this.searchParam = {};
                this.searchParam.SearchText = [{ LogicalOperator: "AND", ColumnName: "ReportNo", Operator: 5, ColumnValue: "" }, { LogicalOperator: "AND", ColumnName: "StyleNo", Operator: 5, ColumnValue: "" },
                { LogicalOperator: "AND", ColumnName: "SampleDescription", Operator: 5, ColumnValue: "" }, { LogicalOperator: "AND", ColumnName: "LogoutTime", Operator: 6, ColumnValue: "" }];
                this.searchModal.SearchText = [];
                /*ended*/
                this.additionalSearch = [{ LogicalOperator: "AND", ColumnName: "", Operator: "", ColumnValue: "" }];
                // this.vendorList = [];

                this.testFlag1.flag = false;
                this.isLoader = true;
                if (this.injectService.additionalSearch) {
                    if (this.searchParam.SearchText.length > 4) {
                        var count = this.searchParam.SearchText.length - 4;
                        this.searchParam.SearchText.splice(4, count);
                    }
                    var that = this;
                    this.injectService.additionalSearch.forEach(function (addObj) {
                        that.searchParam.SearchText.push(addObj);
                    });
                    //addded by Divyanshu
                    this.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));

                }
                this.dataService.get('Order/GetMyOrderColumnsList?selectedCompany=' + this.searchModal.ClientId + '&FormId=2').subscribe(
                    response => {
                        if (response.IsSuccess) {
                            this.dynamicSearchColumns = response.SelectedSearchColumn;
                            this.dynamicGridColumns = response.SelectedGridColumn;
                            this.columnList = response.SelectedAdHocSearchColumn;
                            this.getOrderList();
                        }
                        else {
                            this.getOrderList();
                            swal('', response.Message)
                        }
                    }
                );
            } else {
                this.router.navigate(['/landing-page']);
            }
        } else {
            this.router.navigate(['/login']);
        }
    }
    getOrderList() {
        this.isLoader = true;
        this.dataService.post('Order/GetAllOrderDynamicListWithView', this.searchModal).subscribe(
            response => {

                this.isLoader = false;
                if (response.IsSuccess) {

                    this.orderList = response.Dt;
                    this.totalItem = Math.ceil(response.TotalRecords / this.searchModal.PageSize);
                    // this.clientEmail = response.Dt[0].ClientEmail;
                    // this.clientId = response.Dt[0].ClientID;
                    // this.checkedReportNo = [];

                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }

    getOrderListForShareComments(responseMessage: string) {
        this.isLoader = true;
        this.dataService.post('Order/GetAllOrderDynamicListWithView', this.searchModal).subscribe(
            response => {
                if (response.IsSuccess) {
                    this.orderList = response.Dt;
                    this.totalItem = Math.ceil(response.TotalRecords / this.searchModal.PageSize);
                    this.isLoader = false;
                    swal('', responseMessage);
                    // this.clientEmail = response.Dt[0].ClientEmail;
                    // this.clientId = response.Dt[0].ClientID;
                    // this.checkedReportNo = [];

                }
                else {
                    swal('', response.Message);
                    this.isLoader = false;
                }
            }
        )
    }

    addSearch() {

        if (this.additionalSearch.length > 0) {
            if (this.additionalSearch[this.additionalSearch.length - 1].LogicalOperator && this.additionalSearch[this.additionalSearch.length - 1].ColumnName && this.additionalSearch[this.additionalSearch.length - 1].Operator) {
                this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
            }
        }
        else {
            this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
        }
    }


    // tableToExcel = (function() {
    //   var uri = 'data:application/vnd.ms-excel;base64,'
    //     , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    //     , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    //     , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
    //   return function(table, name) {
    //     if (!table.nodeType) table = document.getElementById(table)
    //     var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    //     window.location.href = uri + base64(format(template, ctx))
    //   }
    // })()

    //     fnExcelReport(id:any)
    //     {
    //         var tab_text="<table border='2px'><tr>";
    //         var textRange; var j=0;
    //         var tab = document.getElementById(id); // id of table

    //         for(j = 0 ; j < tab.rows.length ; j++)
    //         {
    //             tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
    //         }
    //         tab_text=tab_text+"</table>";
    //         var ua = window.navigator.userAgent;
    //         var msie = ua.indexOf("MSIE ");
    //         if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    //         {
    //             txtArea1.document.open("txt/html","replace");
    //             txtArea1.document.write(tab_text);
    //             txtArea1.document.close();
    //             txtArea1.focus();
    //             sa=txtArea1.document.execCommand("SaveAs",true,"vendorScore.xls");
    //         }
    //         else {
    //             //other browser not tested on IE 11
    //             //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent("\ufeff"+tab_text));
    //             //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
    //             var str = encodeURIComponent("\ufeff"+tab_text);
    // var uri = 'data:application/vnd.ms-excel,' + str;
    // var downloadLink = document.createElement("a");
    // downloadLink.href = uri;
    //  downloadLink.download = "VendorPerformanceAnalysis.xls";
    //  document.body.appendChild(downloadLink);
    // downloadLink.click();
    // document.body.removeChild(downloadLink);
    //         }
    //         //return (sa);
    //     }

    removeSearch(index: number) {
        if (this.additionalSearch.length > 1) {
            this.additionalSearch.splice(index, 1);
        }
    }

    resetSearchForm() {

        this.PageSizeDynamic = 10;
        (<any>$('.btnclearenabled')).click();
        this.searchParam.SearchText[0].ColumnValue = '';
        this.searchParam.SearchText[1].ColumnValue = '';
        this.searchParam.SearchText[2].ColumnValue = '';
        this.searchParam.SearchText[3].ColumnValue = '';
        this.PageSizeDynamic = 10;
        for (let i of this.dynamicSearchColumns) {
            i.ColumnValue = '';
        }
        for (let i in this.additionalSearch[0]) {
            this.additionalSearch[0].ColumnName = "";
            this.additionalSearch[0].ColumnValue = "";
            this.additionalSearch[0].LogicalOperator = "AND";
            this.additionalSearch[0].Operator = "";
        }
        (<any>$('.resetSelect')).prop('selectedIndex', 0);
        (<any>$('.resetDynamicInput')).val('');
        for (var i = this.additionalSearch.length; i > 0; i--) {

            this.additionalSearch.splice(i, 1);
        }
        this.PageSizeDynamic = 10;
        this.search(1);
    }
    search(Type: any) {
        let dynamicSearchLength = 0;
        let thisObj = this;
        /*below for solving bug ot searching and sorting*/
        if (thisObj.dateRange.StartDate && thisObj.dateRange.EndDate) {
            thisObj.searchModal.StartDate = JSON.parse(JSON.stringify(thisObj.dateRange.StartDate));
            thisObj.searchModal.EndDate = JSON.parse(JSON.stringify(thisObj.dateRange.EndDate));
        }
        thisObj.searchModal.SearchText = JSON.parse(JSON.stringify(thisObj.searchParam.SearchText));
        /*ended*/
        if (thisObj.dynamicSearchColumns) {
            dynamicSearchLength = thisObj.dynamicSearchColumns.length;
            thisObj.searchModal.SearchText.splice(4, thisObj.searchModal.SearchText.length - 4);
            thisObj.dynamicSearchColumns.forEach(function (column) {
                thisObj.searchModal.SearchText.push({ "LogicalOperator": "AND", "ColumnName": column.ColumnName, "Operator": 5, "ColumnValue": column.ColumnValue })
            })
        }
        if (thisObj.searchModal.SearchText.length > 4 + dynamicSearchLength) {
            let count = thisObj.searchModal.SearchText.length - 4 + dynamicSearchLength;
            thisObj.searchModal.SearchText.splice(4 + dynamicSearchLength, count);
        }
        thisObj.additionalSearch.forEach(function (addObj) {
            thisObj.searchModal.SearchText.push(addObj);
        })
        this.searchModal.PageSize = this.PageSizeDynamic;
        thisObj.searchModal.PageNumber = 1;
        if (Type == 1) {
            thisObj.getOrderList();
        }
        else {
            thisObj.ExportToExcelTrack();
        }
    }
    selectSize() {
        this.search(1);
    }
    // search() {
    //     var thisObj = this;
    //     /*below for solving bug ot searching and sorting*/
    //     if (thisObj.dateRange.StartDate && thisObj.dateRange.EndDate) {
    //         thisObj.searchModal.StartDate = JSON.parse(JSON.stringify(thisObj.dateRange.StartDate));
    //         thisObj.searchModal.EndDate = JSON.parse(JSON.stringify(thisObj.dateRange.EndDate));
    //     }
    //     thisObj.searchModal.SearchText = JSON.parse(JSON.stringify(thisObj.searchParam.SearchText));
    //     /*ended*/
    //     if (thisObj.searchModal.SearchText.length > 3) {
    //         var count = thisObj.searchModal.SearchText.length - 3;
    //         thisObj.searchModal.SearchText.splice(3, count);
    //     }
    //     thisObj.additionalSearch.forEach(function (addObj) {
    //         thisObj.searchModal.SearchText.push(addObj);
    //     })
    //     thisObj.isLoader = true;
    //     thisObj.searchModal.PageNumber = 1;
    //     thisObj.dataService.post('Order/GetAllOrders', thisObj.searchModal).subscribe(
    //         r => {
    //
    //             thisObj.isLoader = false;
    //             if (r.IsSuccess) {
    //                 thisObj.p = 1;
    //                 thisObj.orderList = r.Dt;
    //                 thisObj.totalItem = Math.ceil(r.TotalRecords / this.searchModal.PageSize);
    //             }
    //             else {
    //                 swal('', r.Message);
    //             }

    //         }
    //     )
    // }
    getPage(page: number) {
        this.searchModal.PageNumber = page;
        this.isLoader = true;
        this.dataService.post('Order/GetAllOrderDynamicListWithView', this.searchModal).subscribe(
            r => {
                this.isLoader = false;
                if (r.IsSuccess) {
                    this.p = page;
                    this.orderList = r.Dt;
                }
                else {
                    swal('', r.Message);
                }
            }
        )
    }
    showDownload() {
        this.showDownloadFiles = this.showDownloadFiles == true ? this.showDownloadFiles = false : this.showDownloadFiles = true;
    }
    download(fileName: any, data: any, reportNo: any) {

        this.ReportNo = reportNo;
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        this.userDetail.EmailId = this.userDetail.EmailId;
        this.fileType = data;
        (<any>$('#DownloadFile')).modal('show');
        // if (data == 'TRF') {
        //     this.bulkEmailSendObject.type = data;
        //     this.DownloadfileName = fileName.TRF.trim();
        // }
        // else if (data == 'Report') {
        //     this.bulkEmailSendObject.type = data;
        //     this.DownloadfileName = fileName.trim();
        // }
        // else {
        //     this.bulkEmailSendObject.type = "Invoice";
        //     this.DownloadfileName = data.trim();
        // }

        if (data == 'TRF') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        else if (data == 'Report') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        else if (data == 'SUP' || data == 'GCC') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        else if (data = 'Invoice') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }

        // this.checkedReportNo = [];
        this.ccEmails = [];
        this.remarks = [];
        this.showDownloadFiles = false;
        (<any>$('.CheckPop')).prop('checked', false)
        // let downloadFileName = encodeURIComponent(fileName.trim());
        // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim();;
    }

    downloadFile() {
        var that = this;
        if (that.showDownloadFiles && that.userDetail.EmailId == "") {
            swal('', "Please enter emailid");
            return;
        } else {
            that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
        }
        if (this.showDownloadFiles == true) {
            //that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
            that.bulkEmailSendObject.ReportNo = that.ReportNo;
            that.bulkEmailSendObject.Subject = "MTS - Test Report No.: " + that.ReportNo;
            that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
            that.bulkEmailSendObject.FileName = that.DownloadfileName;
            that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
            that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
            that.bulkEmailSendObject.FileStatus = 'D';
            that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
            that.bulkEmailSendObject.FileType = that.fileType;
            that.bulkEmailSendObject.BusinessCategoryid = that.userCredential.BusinessCategoryId;
            that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
                response => {
                    if (response.IsSuccess) {
                        swal('', response.Message);
                        // that.checkedReportNo = [];
                        that.ccEmails = [];
                        that.remarks = [];
                        that.userDetail.EmailId = "";
                        (<any>$('#DownloadFile')).modal('hide');

                        // let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
                        // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "";
                        // window.location.href = BaseUrl + "ManageDocuments/DownloadOrderFile?FileName=" + downloadFileName.trim() + "&ReportNo="+that.ReportNo;
                    }
                    else {
                        swal('', response.Message);
                    }
                }
            )
        }
        else {
            let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
            // window.location.href = BaseUrl + "ManageDocuments/DownloadOrderFile?FileName=" + downloadFileName.trim() + "&ReportNo="+that.ReportNo;
            window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "&fileType=" + that.fileType + "&businessCategoryId=" + that.userCredential.BusinessCategoryId;
        }
    }

    sorting(column: string) {
        //this.searchModal.SearchText = [{ ColumnName: "ReportNo", Operator: 5, ColumnValue: "" }, { ColumnName: "StyleNo", Operator: 5, ColumnValue: "" }, { ColumnName: "SampleDescription", Operator: 5, ColumnValue: "" }];
        this.searchModal.OrderBy = column;
        if (this.searchModal.Order == 'asc') {
            this.searchModal.Order = 'desc';
        }
        else {
            this.searchModal.Order = 'asc';
        }
        this.isLoader = true;
        this.dataService.post('Order/GetAllOrderDynamicListWithView', this.searchModal).subscribe(
            r => {
                this.isLoader = false;
                if (r.IsSuccess) {
                    this.orderList = r.Dt;
                    this.totalItem = Math.ceil(r.TotalRecords / this.searchModal.PageSize);
                }
                else {
                    swal('', r.Message);
                }
            }
        )
    }


    shareComment(obj) {
        var that = this;
        that.testFlag1.flag = true;
        that.vendorId = obj.ApplicantCompanyId;
        var LabEmail = "";
        this.shareCommentObj = {};
        this.previousComments = false;
        var vendor = "";
        if (obj.ReportSendToEmail != "" && obj.ReportSendToEmail != null) {
            vendor = obj.ReportSendToEmail;
        }
        if (obj.ApplicantEmailId != "" && obj.ApplicantEmailId != null) {
            var z = obj.ApplicantEmailId.toLowerCase().split(',');
            z.forEach(element => {
                if (vendor.toLowerCase().indexOf(element.toLowerCase()) <= -1) {
                    if (vendor == "") {
                        vendor = element;
                    }
                    else {
                        vendor = vendor + ',' + element;
                    }
                }
            });
        }
        if (vendor) {
            var r = vendor.toLowerCase().split(',');
            if (r.length > 0) {
                r.forEach((val, i) => {
                    if (val.includes('unknown')) {
                        r.splice(i, 1);
                    }
                })
                if (r.length > 0) {
                    vendor = r.join(',');
                }
                else {
                    vendor = r[0];
                }
            }
            else {
                if (vendor.includes('unknown')) {
                    vendor = "";
                }
            }
        }
        this.shareCommentObj.VendorEmailId = vendor;
        if (obj.ManufacturerEmailId) {
            var y = obj.ManufacturerEmailId.toLowerCase().split(',');
            if (y.length > 0) {
                y.forEach((val, i) => {
                    if (val.includes('unknown')) {
                        y.splice(i, 1);
                    }
                })
                if (y.length > 0) {
                    obj.ManufacturerEmailId = y.join(',');
                }
                else {
                    obj.ManufacturerEmailId = y[0];
                }
            }
            else {
                if (obj.ManufacturerEmailId.includes('unknown')) {
                    if (this.userCredential.CompanyId == '386') {
                        obj.ManufacturerEmailId = 'Nothing';
                    } else {
                        obj.ManufacturerEmailId = '';
                    }
                }
            }
        } else if (this.userCredential.CompanyId == '386') {
            obj.ManufacturerEmailId = 'Nothing';
        }

        this.shareCommentObj.ManufacturerEmailId = obj.ManufacturerEmailId;

        if (this.userCredential.CompanyId == '386') {

            var z = obj.ClientEmail.toLowerCase().split(',');
            if (z.length > 0) {
                z.forEach((val, i) => {
                    if (val.includes('unknown')) {
                        z.splice(i, 1);
                    }
                })
                if (z.length > 0) {
                    this.shareCommentObj.cc = z.join(',');
                }
                else {
                    this.shareCommentObj.cc = z[0];
                }
            }
            else {
                if (obj.ClientEmail.includes('unknown')) {
                    this.shareCommentObj.cc = "";
                }
            }
        }
        else {
            this.shareCommentObj.cc = this.userDetail.EmailId;
            // if (this.userCredential.CompanyId == '34')

            if (obj.ClientEmail) {
                var z = obj.ClientEmail.toLowerCase().split(',');
                if (z.length > 0) {
                    z.forEach((val, i) => {
                        if (val.includes('unknown')) {
                            z.splice(i, 1);
                        }
                    })
                    if (z.length > 0) {
                        this.shareCommentObj.To = z.join(',');
                    }
                    else {
                        this.shareCommentObj.To = z[0];
                    }
                }
                else {
                    if (obj.ClientEmail.includes('unknown')) {
                        this.shareCommentObj.To = "";
                    }
                }
            }
        }
        if (obj.LaboratoryEmailId) {
            var y = obj.LaboratoryEmailId.toLowerCase().split(',');
            if (y.length > 0) {
                y.forEach((val, i) => {
                    if (val.includes('unknown')) {
                        y.splice(i, 1);
                    }
                })
                if (y.length > 0) {
                    obj.LaboratoryEmailId = y.join(',');
                }
                else {
                    obj.LaboratoryEmailId = y[0];
                }
            }
            else {
                if (obj.LaboratoryEmailId.includes('unknown')) {
                    obj.LaboratoryEmailId = '';

                }
            }
        }
        this.shareCommentObj.LaboratoryEmailId = obj.LaboratoryEmailId;
        this.shareCommentObj.ReportNo = obj.ReportNo;
        this.shareCommentObj.Subject = " MTS report no. " + obj.ReportNo;
        var link = window.location.href.split('/#/')[0];
        this.shareCommentObj.WebLink = link + '/#/web-link/order/' + btoa(obj.ReportNo);
        this.shareCommentObj.UploadedImage = null;

        //New Model creation, set value here.
        this.shareCommentInfo.Subject = this.shareCommentObj.Subject;
        this.shareCommentInfo.WebLink = this.shareCommentObj.WebLink;
        this.shareCommentInfo.ReportNo = this.shareCommentObj.ReportNo;
        if (this.userCredential.UserType == 'Client' || this.userCredential.UserType == 'MTS Internal User') {
            this.shareCommentInfo.To = this.shareCommentObj.VendorEmailId
        } else {
            this.shareCommentInfo.To = this.shareCommentObj.To;
            if (this.shareCommentObj.VendorEmailId != "" && this.shareCommentObj.VendorEmailId != null) {
                if (this.shareCommentObj.VendorEmailId != "" && this.shareCommentObj.VendorEmailId != null) {
                    var y = this.shareCommentObj.VendorEmailId.toLowerCase().split(',');
                    if (y.length > 0) {
                        y.forEach((val, i) => {
                            if (val.includes('nothing')) {

                            }
                            else {
                                let j = this.CCEmailList.findIndex(k => k.EmailId == val)
                                if (j > -1) {
                                    //Already added this email
                                } else {
                                    this.CCEmailList.push({
                                        "EmailId": val,
                                        "displayChecked": true,
                                    });
                                    this.checkedCCEmail.push(val);
                                }
                            }
                        })
                    }
                }
            }
        }

        if (this.shareCommentObj.ManufacturerEmailId != "" && this.shareCommentObj.ManufacturerEmailId != null) {
            var y = this.shareCommentObj.ManufacturerEmailId.toLowerCase().split(',');
            if (y.length > 0) {
                y.forEach((val, i) => {
                    if (val.includes('nothing')) {

                    }
                    else {
                        let k = this.CCEmailList.findIndex(j => j.EmailId == val)
                        if (k > -1) {
                            //Already added this email
                        } else {
                            this.CCEmailList.push({
                                "EmailId": val,
                                "displayChecked": true,
                            });
                            this.checkedCCEmail.push(val);
                        }
                    }
                })
            }
        }

        if (this.shareCommentObj.cc != "" && this.shareCommentObj.cc != null) {
            var y = this.shareCommentObj.cc.toLowerCase().split(',');
            if (y.length > 0) {
                y.forEach((val, i) => {
                    let k = this.CCEmailList.findIndex(j => j.EmailId == val)
                    if (k > -1) {
                        //Already added this email
                    } else {
                        this.CCEmailList.push({
                            "EmailId": val,
                            "displayChecked": true,
                        });
                        this.checkedCCEmail.push(val);
                    }
                })
            }
        }
        if (this.shareCommentObj.LaboratoryEmailId != "" && this.shareCommentObj.LaboratoryEmailId != null) {
            var y = this.shareCommentObj.LaboratoryEmailId.toLowerCase().split(',');
            if (y.length > 0) {
                y.forEach((val, i) => {
                    let k = this.CCEmailList.findIndex(j => j.EmailId == val)
                    if (k > -1) {
                        //Already added this email
                    } else {
                        this.CCEmailList.push({
                            "EmailId": val,
                            "displayChecked": true,
                        });
                        this.checkedCCEmail.push(val);
                    }
                })
            }
        }
    }

    getPreviousComments() {
        this.isLoader = true;
        this.previousComments = true;
        this.previousCommentList = [];
        this.dataService.get('order/GetOrderPreviousComments?ReportNo=' + this.shareCommentObj.ReportNo + "&userId=" + this.userCredential.UserId + "&UserType=").subscribe(
            r => {
                this.isLoader = false;
                this.previousCommentList = r.CommentsList;
            }
        )
    }

    openPopup(reportNo: string, type: string) {
        this.selectedReportNo = reportNo;
        this.isLoader = true;
        this.previousCommentList = [];
        (<any>$('#showComments')).modal('show');
        this.dataService.get('order/GetOrderPreviousComments?ReportNo=' + reportNo + "&userId=" + this.userCredential.UserId + "&UserType=" + type).subscribe(
            r => {
                this.isLoader = false;
                this.previousComments = true;
                this.previousCommentList = r.CommentsList;
            }
        )
    }

    //------method for old share comment popup--------//

    // sendCommentToMail() {
    //     if (this.shareCommentObj.ManufacturerEmailId == 'Nothing') {
    //         this.shareCommentObj.ManufacturerEmailId = '';
    //     }
    //     // if(!this.shareCommentObj.Subject){
    //     //     swal('','Please enter subject.');
    //     // }
    //     // else if(!this.shareCommentObj.UploadedImage){
    //     //     swal('','Please choose file');
    //     // }
    //     this.shareCommentObj.CreatedBy = this.userDetail.UserId;
    //     this.shareCommentObj.UserType = this.userCredential.UserType;
    //     var data = new FormData();
    //     data.append("UploadedImage", this.shareCommentObj.UploadedImage);
    //     if (this.shareCommentObj.VendorEmailId == "" && this.shareCommentObj.ManufacturerEmailId == "" && this.shareCommentObj.LaboratoryEmailId == "" && this.shareCommentObj.cc == "") {
    //         swal('', "Please enter any emailid");
    //         return;
    //     } else {
    //         data.append("ShareComments", JSON.stringify(this.shareCommentObj));
    //     }
    //     if (this.userCredential.CompanyId == '386') {
    //         var CompanyName = this.userDetail.CompanyName;
    //         var value = {
    //             EmailID: "SFTestReports@stitchfix.com, Robin.Abshier@stitchfix.com",
    //             CompanyName: CompanyName
    //         }
    //         data.append("ShareData", JSON.stringify(value));
    //     }
    //     else {
    //         var CompanyName = this.userDetail.CompanyName;
    //         var value1 = {
    //             EmailID: this.userDetail.EmailId,
    //             CompanyName: CompanyName
    //         }
    //         data.append("ShareData", JSON.stringify(value1));
    //     }

    //     if (this.shareCommentObj.Subject && this.shareCommentObj.Comments) {
    //         let that = this;
    //         let returnObj = this.dataService.postFile('Order/ShareComments', data);
    //         returnObj.done(function (xhr, textStatus) {
    //             (<any>$('#ShareComments')).modal('hide');
    //             that.getOrderListForShareComments(xhr.Message);
    //         });
    //     }
    //     else {
    //         let returnObj = this.dataService.postFile('Order/ShareComments', data);
    //         returnObj.done(function (xhr, textStatus) {
    //             (<any>$('#ShareComments')).modal('show');
    //             swal('', xhr.Message);
    //         })
    //     }
    //     this.reset();
    // }

    onChange(event) {
        this.shareCommentObj.FileName = event.target.files[0].name;
        this.shareCommentObj.UploadedImage = event.target.files[0];
    }

    getBase64(file) {
        var thisObj = this;
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            thisObj.shareCommentObj.Base64 = reader.result;
        };
        reader.onerror = function (error) {
        };
    }
    viewFailHistory(obj) {
        this.testHistoryMessage = 'Test Result';
        this.reportNo = obj.ReportNo;
        this.viewHistoryList = [];
        let url = '';
        if (obj.ReportRating.toLowerCase() == 'fail') {
            url = 'Order/GetPassOrderTestHistory?ReportNo=' + obj.ReportNo;
        }
        else if (obj.ReportRating.toLowerCase() != 'fail') {
            url = 'Order/GetPassOrderTestHistory?ReportNo=' + obj.ReportNo;
        }
        this.isLoader = true;
        this.dataService.get(url).subscribe(

            r => {
                (<any>$('#ViewFail')).modal('show');
                this.isLoader = false;
                this.viewHistoryList = r.FailCodeList;
            }
        )
    }
    // reset() {
    //     this.fileUploader.nativeElement.value = "";
    //     this.shareCommentObj.FileName = null;
    //     this.shareCommentObj.UploadedImage = null;
    // }
    onDateRangeChanged(event: any) {
        this.dateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
        this.dateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
    }
    logoutonDateRangeChanged(event: any) {
            
        if(event.beginDate.day > '0' && event.endDate.day> '0'){
            this.LogOutDateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
            this.LogOutDateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
    
            this.searchParam.SearchText[3].ColumnValue = "'" + this.LogOutDateRange.StartDate + "' And '" + this.LogOutDateRange.EndDate + "'";
            
        }
        else{
            this.searchParam.SearchText[3].ColumnValue="";
        }
       
        // this.LogOutDateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
        // this.LogOutDateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;

        // this.searchParam.SearchText[3].ColumnValue = "'" + this.LogOutDateRange.StartDate + "' And '" + this.LogOutDateRange.EndDate + "'";
    }
    ExportToExcelTrack() {

        this.isLoader = true;
        var that = this;
        this.searchModal.PageNumber = 2;
        this.dataService.post('OrderAnalysis/ExportToExcelTracking', this.searchModal).subscribe(
            response => {

                that.isLoader = false;
                if (response.IsSuccess) {
                    let downloadFileName = encodeURIComponent(response.Message);
                    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryid="+this.userCredential.BusinessCategoryId;
                    // that.isLoader = false;
                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }

    myDateRangePickerOptions = {
        clearBtnTxt: 'Clear',
        beginDateBtnTxt: 'Begin Date',
        endDateBtnTxt: 'End Date',
        acceptBtnTxt: 'OK',
        dateFormat: 'dd/mm/yyyy',
        firstDayOfWeek: 'mo',
        sunHighlight: true,
        height: '34px',
        width: '260px',
        inline: false,
        selectionTxtFontSize: '12px',
        alignSelectorRight: false,
        indicateInvalidDateRange: true,
        showDateRangeFormatPlaceholder: false,
        showClearBtn: false,
        customPlaceholderTxt: 'Login Date-Range'
    };
    openVendorDilaog() {
        (<any>$("#vendorEmail")).modal('show');
        this.testFlag1.flag = true;

    }

    // methods for new sharecomment popup

    CCEmailListshow() {
        (<any>$("#CCEmailList")).modal('show');
    }

    handleCheckboxCheck(evt, i) {
        let emailId = this.CCEmailList[i].EmailId;

        if (evt.target.checked) {
            let index = this.checkedCCEmail.findIndex(i => i == emailId);
            if (index > -1) {
                //Already added this email
            }
            else {
                this.checkedCCEmail.push(emailId);
                this.CCEmailList[i].displayChecked = true;
            }
        }
        else {
            let index = this.checkedCCEmail.findIndex(i => i == emailId);
            this.checkedCCEmail.splice(index, 1);
            this.CCEmailList[i].displayChecked = false;
        }
    }

    handleCCinputfeild(evt) {
        var keycode = evt.charCode || evt.keyCode;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.CCMailId = this.CCMailId.trim();
        if (keycode == 188) {
            this.CCMailId = this.CCMailId.substring(0, this.CCMailId.length - 1);
        }

        if (this.CCMailId) {
            if (reg.test(this.CCMailId) == true && (keycode == 32 || keycode == 13 || keycode == 188)) {
                this.checkedCCEmail.push(this.CCMailId)
                this.CCMailId = "";
            }
        }
    }

    handleToinputfeild(evt) {
        var keycode = evt.charCode || evt.keyCode;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.ToMailId = this.ToMailId.trim();
        if (keycode == 188) {
            this.ToMailId = this.ToMailId.substring(0, this.ToMailId.length - 1);
        }

        if (this.ToMailId) {
            if (reg.test(this.ToMailId) == true && (keycode == 32 || keycode == 13 || keycode == 188)) {
                if (this.shareCommentInfo.To) {
                    this.shareCommentInfo.To = this.shareCommentInfo.To + "," + this.ToMailId;
                }
                else {
                    this.shareCommentInfo.To = this.ToMailId;
                }
                this.ToMailId = "";
            }
        }
    }

    addCC() {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (this.CCMailId) {
            if (reg.test(this.CCMailId) == true) {
                this.checkedCCEmail.push(this.CCMailId)
                this.CCMailId = "";
            }
        }
    }

    addTo() {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (this.ToMailId) {
            if (reg.test(this.ToMailId) == true) {
                if (this.shareCommentInfo.To) {
                    this.shareCommentInfo.To = this.shareCommentInfo.To + "," + this.ToMailId;
                }
                else {
                    this.shareCommentInfo.To = this.ToMailId;
                }
                this.ToMailId = "";
            }
        }
    }

    fileChangeEvent(event) {

        if (event.target.files.length > 0) {
            this.shareCommentInfo.FileName = event.target.files[0].name;
            this.shareCommentInfo.UploadedImage = event.target.files[0];
        }
    }

    closeSharecommentsModal() {
        this.fileUploader.nativeElement.value = "";
        this.shareCommentInfo.UploadedImage = null;
        this.shareCommentInfo.FileName = null;
        this.isLaboratoryEmailCheckboxChecked = false;
        this.shareCommentInfo = new T_ShareComments();
        this.checkedCCEmail = new Array<string>();
        this.CCEmailList = new Array<CCEmailList>();
    }

    removedEmailFromCC(emailId) {
        let index = this.checkedCCEmail.findIndex(i => i == emailId);
        this.checkedCCEmail.splice(index, 1);
        let j = this.CCEmailList.findIndex(k => k.EmailId == emailId);
        if (j > -1) {
            this.CCEmailList[j].displayChecked = false;
        }
    }

    removedEmailFromTo(emailId) {
        let splitToEmailArr = this.shareCommentInfo.To.split(",");
        let index = splitToEmailArr.findIndex(i => i == emailId);
        splitToEmailArr.splice(index, 1);
        this.shareCommentInfo.To = splitToEmailArr.join(",");
        if (!this.shareCommentInfo.To) {
            this.shareCommentInfo.To = null;
        }
    }

    removedShareFile() {
        this.fileUploader.nativeElement.value = "";
        this.shareCommentObj.FileName = null;
        this.shareCommentObj.UploadedImage = null;
        this.shareCommentInfo.FileName = null;
        this.shareCommentInfo.UploadedImage = null;
    }

    reset() {
        this.fileUploader.nativeElement.value = "";
        this.shareCommentObj.FileName = null;
        this.shareCommentObj.UploadedImage = null;
        this.shareCommentInfo.FileName = null;
        this.shareCommentInfo.UploadedImage = null;
    }

    sendCommentToMail() {
        this.shareCommentInfo.CC = this.checkedCCEmail.join(",");
        this.shareCommentInfo.CreatedBy = this.userDetail.UserId;
        this.shareCommentInfo.UserType = this.userCredential.UserType;
        var data = new FormData();
        data.append("UploadedImage", this.shareCommentInfo.UploadedImage);
        if (!this.shareCommentInfo.CC && !this.shareCommentInfo.To) {
            // if (this.shareCommentInfo.CC == "" && this.shareCommentInfo.To == "") {
            swal('', "Please enter any emailid");
            return;
        }
        if (!this.shareCommentInfo.Subject) {
            swal('', "Please enter Subject.");
            return;
        } if (!this.shareCommentInfo.Comments) {
            swal('', "Please enter Comment.");
            return;
        }
        if ((this.shareCommentInfo.CC || this.shareCommentInfo.To) && this.shareCommentInfo.Subject && this.shareCommentInfo.Comments) {
            this.shareCommentInfo.VendorEmailId = this.shareCommentInfo.To;
            data.append("ShareComments", JSON.stringify(this.shareCommentInfo));
        }
        if (this.userCredential.CompanyId == '386') {
            var CompanyName = this.userDetail.CompanyName;
            var value = {
                EmailID: "SFTestReports@stitchfix.com, Robin.Abshier@stitchfix.com",
                CompanyName: CompanyName,
                FileType: "ShareCommentFile",
                BusinessCategoryId:this.userCredential.BusinessCategoryId
            }
            data.append("ShareData", JSON.stringify(value));
        }
        else {
            var CompanyName = this.userDetail.CompanyName;
            var value1 = {
                EmailID: this.userDetail.EmailId,
                CompanyName: CompanyName,
                FileType: "ShareCommentFile",
                BusinessCategoryId:this.userCredential.BusinessCategoryId
            }
            data.append("ShareData", JSON.stringify(value1));
        }

        if (this.shareCommentInfo.Subject && this.shareCommentInfo.Comments) {
            let that = this;
            let returnObj = this.dataService.postFile('Order/ShareComments', data);
            returnObj.done(function (xhr, textStatus) {
                that.shareCommentInfo.Comments = "";
                that.shareCommentInfo.To = "";
                (<any>$('#ShareComments')).modal('hide');
                that.closeSharecommentsModal();
                that.getOrderListForShareComments(xhr.Message);

            });
        }
        else {
            let returnObj = this.dataService.postFile('Order/ShareComments', data);
            returnObj.done(function (xhr, textStatus) {
                (<any>$('#ShareComments')).modal('show');
                swal('', xhr.Message);
            })
        }
        this.reset();
    }
}
