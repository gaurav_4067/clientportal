export const SEARCH_MODEL = {
    "ClientId": 0,
    "UserType": "",
    "UserCompanyId": 0,
    "PageSize": "",
    "PageNumber": "",
    "OrderBy": "Applicant",
    "Order": "asc",
    "TotalRecords": "",
    "StartDate": null,
    "EndDate": null,
    "SearchText": [],
    "ProductDisposition": 0
};

export class CCEmailList {
    public EmailId: string;
    public displayChecked: boolean;
}

export class T_ShareComments {
    public To: string;
    public CC: string;
    public Comments: string;
    public WebLink: string;
    public VendorEmailId: string;
    public LaboratoryEmailId: string;
    public Subject: string;
    public CreatedBy: number;
    public UserType: string;
    public ReportNo: string;
    public FileName: string;
    public UploadedImage: any;
}