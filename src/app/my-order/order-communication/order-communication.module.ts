import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { MdInputModule } from '@angular2-material/input';
import { MdCoreModule } from '@angular2-material/core';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { Ng2PaginationModule } from 'ng2-pagination';
import {TooltipModule} from "ngx-tooltip";
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { BulkEmailModule } from '../../bulk-email/bulk-email.module';
import { OrderCommunicationRoutingModule } from './order-communication-routing.module';
import { OrderCommunicationComponent } from './order-communication.component';
import { VendorEmailModule } from '../../vendor-email/vendor-email.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MyDateRangePickerModule,
    MdInputModule,
    MdCoreModule,
    MaterialModule,
    Ng2PaginationModule,
    HeaderModule,
    FooterModule,
    TooltipModule,
    BulkEmailModule, VendorEmailModule,
    OrderCommunicationRoutingModule
  ],
  declarations: [OrderCommunicationComponent]
})
export class OrderCommunicationModule { }
