import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderCommunicationComponent } from './order-communication.component';

const routes: Routes = [{
  path:'',
  component:OrderCommunicationComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderCommunicationRoutingModule { }
