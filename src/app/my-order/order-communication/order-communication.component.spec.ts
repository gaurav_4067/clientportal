import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCommunicationComponent } from './order-communication.component';

describe('OrderCommunicationComponent', () => {
  let component: OrderCommunicationComponent;
  let fixture: ComponentFixture<OrderCommunicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderCommunicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCommunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
