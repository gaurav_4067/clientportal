import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductDispositionComponent } from './product-disposition.component';

const routes: Routes = [{
  path:'',
  component:ProductDispositionComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductDispositionRoutingModule { }
