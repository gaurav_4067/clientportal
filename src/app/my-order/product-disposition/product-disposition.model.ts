export const SEARCH_MODEL={
    "ClientId" : "",
    "UserType":"",
    "UserCompanyId":"",
    "PageSize":"",
    "PageNumber":"",
    "OrderBy":"OrderId",
    "Order":"asc",
    "StartDate":null,
    
    "EndDate" : null,
    "SearchText":[],
    "ProductDisposition":1
};

export class CCEmailList {
    public EmailId: string;
    public displayChecked: boolean;
}

export class T_OverRideRating {
    public To: string;
    public CC: string;
    public Comments: string;
    public WebLink: string;
    public VendorEmailId: string;
    public Subject: string;
    public CreatedBy: number;
    public UserType: string;
    public ReportNo: string;
    public FileName: string;
    public UploadedImage: any;
    public ReviewStatus:string;
    public TDetail:any;
    public ModifiedByName:string;
    public ModifiedById:number;
}