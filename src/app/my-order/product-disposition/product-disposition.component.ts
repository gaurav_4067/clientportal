import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { SEARCH_MODEL, CCEmailList, T_OverRideRating } from './product-disposition.model';
import { DataService, BaseUrl, COLUMN_LIST } from '../../mts.service';
import { Response } from '@angular/http';
import { InjectService } from '../../injectable-service/injectable-service';
import { Hero } from '../common/model_1';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { ActivatedRoute, Router } from '@angular/router'; ''
declare let swal: any;
@Component({
    selector: 'app-product-disposition',
    templateUrl: './product-disposition.component.html',
    styleUrls: ['./product-disposition.component.css']
})
export class ProductDispositionComponent implements OnInit {
    @ViewChild('fileUpload') fileUploader: ElementRef;
    orderList: any;
    private p: any;
    private totalItem: number;
    private itemPerPage: any;
    private dateRange: any;
    private LogOutDateRange: any;
    searchModal: any;
    searchText: any;
    additionalSearch: any;
    overRideRatingObj: any;
    reviewRadio: string;
    isLoader: boolean;
    userDetail: any;
    viewHistoryList: any;
    reportNo: string;
    userCredential: any;
    SubjectText: any;
    columnList: any;
    searchParam: any;
    dynamicSearchColumns: any;
    dynamicGridColumns: any;
    PageSizeDynamic: any = 10;
    ReportNo: number;
    DownloadfileName: any;
    remarks: any = [];
    ccEmails: any = [];
    showDownloadFiles: boolean = false;
    bulkEmailSendObject: any = {
        EmailLogID: 0,
        EmailD: '',
        Subject: '',
        Body: '',
        Status: false,
        FileName: '',
        CreatedDate: Date.now(),
        type: '',
        CompanyName: '',
        CreatedBy: '',
        ModifiedBy: 0,
        ModifiedDate: Date.now(),
        CCEmailID: '',
        FileType:'',
        BusinessCategoryId:0
    }
    checkedReportArr: any = [];
    // for new product dispostion popup

    CCEmailList: Array<CCEmailList>;
    checkedCCEmail: Array<string>;
    CCMailId: string;
    ToMailId: string;
    isLaboratoryEmailCheckboxChecked: boolean = false;
    overRideRatingInfo: T_OverRideRating;
    previousCommentList: any;
    selectedReportNo: string;
    fileType: string;

    constructor(private dataService: DataService, private injectService: InjectService, private route: ActivatedRoute, private router: Router) {
        this.dateRange = {};
        this.LogOutDateRange = {};
        this.overRideRatingObj = {};
        this.overRideRatingInfo = new T_OverRideRating;
        this.CCEmailList = new Array<CCEmailList>();
    }

    ngOnInit() {

        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
        if (this.userDetail && this.userCredential) {
            if (this.userCredential.UserPermission.ViewProductDisposition) {
                this.searchModal = SEARCH_MODEL;
                this.searchModal.ClientId = this.userCredential.CompanyId;
                this.searchModal.UserType = this.userCredential.UserType;
                this.searchModal.UserCompanyId = this.userCredential.UserCompanyId;
                this.searchModal.PageSize = this.PageSizeDynamic;
                this.searchModal.PageNumber = 1;
                //this.searchModal.TotalRecords = 200;
                this.searchModal.StartDate = '';
                this.searchModal.EndDate = '';
                this.searchModal.OrderBy = 'LogoutTime';
                this.searchModal.Order = 'desc';
                this.searchModal.FormId = '3';
                this.p = 1;
                this.itemPerPage = 1;
                this.totalItem = 0;
                /*below for solving bug ot searching and sorting*/
                this.searchParam = {};
                this.searchParam.SearchText = [{
                    LogicalOperator: "AND",
                    ColumnName: "ReportNo",
                    Operator: 5,
                    ColumnValue: ""
                }, { LogicalOperator: "AND", ColumnName: "StyleNo", Operator: 5, ColumnValue: "" }, {
                    LogicalOperator: "AND",
                    ColumnName: "SampleDescription",
                    Operator: 5,
                    ColumnValue: ""
                },
                { LogicalOperator: "AND", ColumnName: "LogoutTime", Operator: 6, ColumnValue: "" }
                ];
                this.searchModal.SearchText = [];
                /*ended*/
                this.additionalSearch = [{ LogicalOperator: "AND", ColumnName: "", Operator: "", ColumnValue: "" }];
                var that = this;
                this.route.params.subscribe(params => {
                    that.searchModal.SearchText = JSON.parse(JSON.stringify(that.searchParam.SearchText));
                    that.searchModal.SearchText[0].ColumnValue = params['search'];
                });
                //get grid and search columns for CompanyId
                this.isLoader = true;
                /*below for solving bug ot searching and sorting*/
                /*if (this.injectService.additionalSearch) {
                 var thisObj = this;
                 if (thisObj.searchModal.SearchText.length > 3) {
                 var count = thisObj.searchModal.SearchText.length - 3;
                 thisObj.searchModal.SearchText.splice(3, count);
                 }
                 thisObj.injectService.additionalSearch.forEach(function (addObj) {
                 thisObj.searchModal.SearchText.push(addObj);
                 });
                 }*/
                if (this.injectService.additionalSearch) {
                    if (this.searchParam.SearchText.length > 4) {
                        var count = this.searchParam.SearchText.length - 4;
                        this.searchParam.SearchText.splice(4, count);
                    }
                    this.injectService.additionalSearch.forEach(function (addObj) {
                        that.searchParam.SearchText.push(addObj);
                    });
                    //addded by Divyanshu
                    this.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
                }
                /*ended*/
                this.dataService.get('Order/GetMyOrderColumnsList?selectedCompany=' + this.searchModal.ClientId + '&FormId=3').subscribe(
                    response => {
                        if (response.IsSuccess) {
                            this.dynamicSearchColumns = response.SelectedSearchColumn;
                            this.dynamicGridColumns = response.SelectedGridColumn;
                            this.columnList = response.SelectedAdHocSearchColumn;
                            this.getOrderList();
                        }
                        else {
                            this.getOrderList();
                            swal('', response.Message)
                        }
                    }
                );
            } else {
                this.router.navigate(['/landing-page']);
            }
        } else {
            this.router.navigate(['/login']);
        }

        // this.columnList = COLUMN_LIST;



        // this.isLoader = true;
        // this.dateRange={};
        // this.dataService.post('Order/GetTestingOrderList', this.searchModal).subscribe(
        //     r => {
        //         this.isLoader = false;
        //         if (r.IsSuccess) {
        //             this.orderList = r.OrderList;
        //             this.totalItem = Math.ceil(r.TotalRecords / this.searchModal.PageSize);
        //         }
        //         else {
        //             swal('', r.Message);
        //         }
        //     }
        // );
    }

    getOrderList() {

        this.isLoader = true;
        var that = this;
        that.dataService.post('Order/GetAllOrderDynamicListWithView', that.searchModal).subscribe(
            response => {

                that.isLoader = false;
                if (response.IsSuccess) {
                    that.checkedReportArr = [];
                    that.orderList = response.Dt;
                    that.totalItem = Math.ceil(response.TotalRecords / that.searchModal.PageSize);
                    // this.clientEmail = response.Dt[0].ClientEmail;
                    // this.clientId = response.Dt[0].ClientID;
                    // this.checkedReportNo = [];

                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }

    addSearch() {
        if (this.additionalSearch.length > 0) {
            if (this.additionalSearch[this.additionalSearch.length - 1].ColumnName && this.additionalSearch[this.additionalSearch.length - 1].Operator) {
                this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
            }
        }
        else {
            this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
        }

    }

    removeSearch(index: number) {
        if (this.additionalSearch.length > 1) {
            this.additionalSearch.splice(index, 1);
        }
    }


    resetSearchForm() {

        this.PageSizeDynamic = 10;
        (<any>$('.btnclearenabled')).click();
        this.searchParam.SearchText[0].ColumnValue = '';
        this.searchParam.SearchText[1].ColumnValue = '';
        this.searchParam.SearchText[2].ColumnValue = '';
        this.searchParam.SearchText[3].ColumnValue = '';
        //this.searchModal.PageSize = 50;

        for (let i of this.dynamicSearchColumns) {
            i.ColumnValue = '';
        }


        for (let i in this.additionalSearch[0]) {
            this.additionalSearch[0].ColumnName = "";
            this.additionalSearch[0].ColumnValue = "";
            this.additionalSearch[0].LogicalOperator = "AND";
            this.additionalSearch[0].Operator = "";
        }

        (<any>$('.resetSelect')).prop('selectedIndex', 0);
        (<any>$('.resetDynamicInput')).val('');
        for (var i = this.additionalSearch.length; i > 0; i--) {

            this.additionalSearch.splice(i, 1);
        }
        this.PageSizeDynamic = 10;
        this.search(1);
    }

    // search() {
    //     var thisObj = this;
    //     /*below for solving bug ot searching and sorting*/
    //     if (thisObj.dateRange.StartDate && thisObj.dateRange.EndDate) {
    //         thisObj.searchModal.StartDate = JSON.parse(JSON.stringify(thisObj.dateRange.StartDate));
    //         thisObj.searchModal.EndDate = JSON.parse(JSON.stringify(thisObj.dateRange.EndDate));
    //     }
    //     thisObj.searchModal.SearchText = JSON.parse(JSON.stringify(thisObj.searchParam.SearchText));
    //     /*ended*/
    //     if (thisObj.searchModal.SearchText.length > 3) {
    //         var count = thisObj.searchModal.SearchText.length - 3;
    //         thisObj.searchModal.SearchText.splice(3, count);
    //     }
    //     thisObj.additionalSearch.forEach(function (addObj) {
    //         thisObj.searchModal.SearchText.push(addObj);
    //     })
    //     thisObj.isLoader = true;
    //     thisObj.searchModal.PageNumber = 1;
    //     thisObj.dataService.post('Order/GetAllOrders', thisObj.searchModal).subscribe(
    //         r => {
    //             thisObj.isLoader = false;
    //             if (r.IsSuccess) {
    //                 thisObj.p = 1;
    //                 thisObj.orderList = r.OrderList;
    //                 thisObj.totalItem = Math.ceil(r.TotalRecords / thisObj.searchModal.PageSize);
    //             }
    //             else {
    //                 swal('', r.Message);
    //             }
    //         }
    //     )
    // }
    showDownload() {
        this.showDownloadFiles = this.showDownloadFiles == true ? this.showDownloadFiles = false : this.showDownloadFiles = true;
    }

    download(fileName: any, data: any, reportNo: any) {
        this.fileType = data;
        this.ReportNo = reportNo;
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        this.userDetail.EmailId = this.userDetail.EmailId;
        (<any>$('#DownloadFile')).modal('show');
        if (data == 'TRF') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        else if (data == 'Report') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        else if (data == 'SUP' || data == 'GCC') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        else if (data = 'Invoice') {
            this.bulkEmailSendObject.type = data;
            this.DownloadfileName = fileName.trim();
        }
        // this.checkedReportNo = [];
        this.ccEmails = [];
        this.remarks = [];
        this.showDownloadFiles = false;
        (<any>$('.CheckPop')).prop('checked', false)
        // let downloadFileName = encodeURIComponent(fileName.trim());
        // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim();;
    }

    downloadFile() {


        var that = this;
        if (that.showDownloadFiles && that.userDetail.EmailId == "") {
            swal('', "Please enter emailid");
            return;
        } else {
            that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
        }
        if (this.showDownloadFiles == true) {
            //  that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
            that.bulkEmailSendObject.ReportNo = that.ReportNo;
            that.bulkEmailSendObject.Subject = "MTS - Test Report No.: " + that.ReportNo;
            that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
            that.bulkEmailSendObject.FileName = that.DownloadfileName;
            that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
            that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
            that.bulkEmailSendObject.FileStatus = 'D';
            that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
            that.bulkEmailSendObject.FileType=that.fileType;
            that.bulkEmailSendObject.BusinessCategoryId=that.userCredential.BusinessCategoryId;
            that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
                response => {
                    if (response.IsSuccess) {
                        swal('', response.Message);
                        //   that.checkedReportNo = [];
                        that.ccEmails = [];
                        that.remarks = [];
                        that.userDetail.EmailId = "";
                        (<any>$('#DownloadFile')).modal('hide');

                        // let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
                        // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "";
                        // window.location.href = BaseUrl + "ManageDocuments/DownloadOrderFile?FileName=" + downloadFileName.trim() + "&ReportNo="+that.ReportNo;
                    }
                    else {
                        swal('', response.Message);
                    }
                }
            )
        }
        else {
            let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
            // window.location.href = BaseUrl + "ManageDocuments/DownloadOrderFile?FileName=" + downloadFileName.trim() + "&ReportNo="+that.ReportNo;
            window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "&fileType="+this.fileType+"&businessCategoryId="+this.userCredential.BusinessCategoryId;
        }
    }

    search(Type: any) {

        let dynamicSearchLength = 0;
        let thisObj = this;
        /*below for solving bug ot searching and sorting*/
        if (thisObj.dateRange.StartDate && thisObj.dateRange.EndDate) {
            thisObj.searchModal.StartDate = JSON.parse(JSON.stringify(thisObj.dateRange.StartDate));
            thisObj.searchModal.EndDate = JSON.parse(JSON.stringify(thisObj.dateRange.EndDate));
        }
        thisObj.searchModal.SearchText = JSON.parse(JSON.stringify(thisObj.searchParam.SearchText));
        /*ended*/
        if (thisObj.dynamicSearchColumns) {
            dynamicSearchLength = thisObj.dynamicSearchColumns.length;
            thisObj.searchModal.SearchText.splice(4, thisObj.searchModal.SearchText.length - 4);
            thisObj.dynamicSearchColumns.forEach(function (column) {
                thisObj.searchModal.SearchText.push({
                    LogicalOperator: "AND",
                    "ColumnName": column.ColumnName,
                    "Operator": 5,
                    "ColumnValue": column.ColumnValue
                })
            })
        }

        if (thisObj.searchModal.SearchText.length > 4 + dynamicSearchLength) {
            let count = thisObj.searchModal.SearchText.length - 4 + dynamicSearchLength;
            thisObj.searchModal.SearchText.splice(4 + dynamicSearchLength, count);
        }
        thisObj.additionalSearch.forEach(function (addObj) {
            thisObj.searchModal.SearchText.push(addObj);
        })
        this.searchModal.PageSize = this.PageSizeDynamic;
        thisObj.searchModal.PageNumber = 1;
        //  thisObj.getOrderList();
        if (Type == 1) {
            thisObj.getOrderList();
        }
        else {
            thisObj.ExportToExcelTrack();
        }
    }

    selectSize() {
        this.search(1);
    }

    getPage(page: number) {
        this.searchModal.PageNumber = page;
        this.isLoader = true
        this.dataService.post('Order/GetAllOrderDynamicListWithView', this.searchModal).subscribe(
            r => {
                this.isLoader = false;
                if (r.IsSuccess) {
                    this.p = page;
                    this.orderList = r.Dt;
                    this.checkedReportArr = [];

                }
                else {
                    this.checkedReportArr = [];
                    swal('', r.Message);
                }
            }
        )
    }

    sorting(column: string) {

        this.searchModal.OrderBy = column;
        if (this.searchModal.Order == 'asc') {
            this.searchModal.Order = 'desc';
        }
        else {
            this.searchModal.Order = 'asc';
        }
        this.isLoader = true;
        this.dataService.post('Order/GetAllOrderDynamicListWithView', this.searchModal).subscribe(
            r => {
                this.isLoader = false;
                if (r.IsSuccess) {
                    this.orderList = r.Dt;
                    this.totalItem = Math.ceil(r.TotalRecords / this.searchModal.PageSize);
                }
                else {
                    swal('', r.Message);
                }
            }
        )

    }

    overRideRating(obj) {

        this.reviewRadio = 'Retest';
        //fro uncheck all checkbox
        (<any>$('input:checkbox')).prop('checked', false);
        var that = this;
        var LabEmail = "";
        var vendor = "";
        if (obj.ReportSendToEmail != "" && obj.ReportSendToEmail != null) {
            vendor = obj.ReportSendToEmail;
        }
        if (obj.ApplicantEmailId != "" && obj.ApplicantEmailId != null) {
            var z = obj.ApplicantEmailId.toLowerCase().split(',');
            z.forEach(element => {
                if (vendor.toLowerCase().indexOf(element.toLowerCase()) <= -1) {
                    if (vendor == "") {
                        vendor = element;
                    }
                    else {
                        vendor = vendor + ',' + element;
                    }
                }
            });
        }
        if (vendor) {
            var r = vendor.toLowerCase().split(',');
            if (r.length > 0) {
                r.forEach((val, i) => {
                    if (val.includes('unknown')) {
                        r.splice(i, 1);
                    }
                })
                if (r.length > 0) {
                    vendor = r.join(',');
                }
                else {
                    vendor = r[0];
                }
            }
            else {
                if (vendor.includes('unknown')) {
                    vendor = "";
                }
            }
        }
        this.overRideRatingObj.VendorEmailId = vendor;
        if (obj.ManufacturerEmailId) {
            var y = obj.ManufacturerEmailId.toLowerCase().split(',');
            if (y.length > 0) {
                y.forEach((val, i) => {
                    if (val.includes('unknown')) {
                        y.splice(i, 1);
                    }
                })
                if (y.length > 0) {
                    obj.ManufacturerEmailId = y.join(',');
                }
                else {
                    obj.ManufacturerEmailId = y[0];
                }
            }
            else {
                if (obj.ManufacturerEmailId.includes('unknown')) {
                    obj.ManufacturerEmailId = "";
                }
            }
        }
        this.overRideRatingObj.ManufacturerEmailId = obj.ManufacturerEmailId;

        if (this.userCredential.CompanyId == '386')
        // if (this.userCredential.CompanyId == '34')
        {

            var z = obj.ClientEmail.toLowerCase().split(',');
            if (z.length > 0) {
                z.forEach((val, i) => {
                    if (val.includes('unknown')) {
                        z.splice(i, 1);
                    }
                })
                if (z.length > 0) {
                    this.overRideRatingObj.cc = z.join(',');
                }
                else {
                    this.overRideRatingObj.cc = z[0];
                }
            }
            else {
                if (obj.ClientEmail.includes('unknown')) {
                    this.overRideRatingObj.cc = "";
                }
            }

        }
        else {
            this.overRideRatingObj.cc = this.userDetail.EmailId;
            if (obj.ClientEmail) {
                var z = obj.ClientEmail.toLowerCase().split(',');
                if (z.length > 0) {
                    z.forEach((val, i) => {
                        if (val.includes('unknown')) {
                            z.splice(i, 1);
                        }
                    })
                    if (z.length > 0) {
                        this.overRideRatingObj.To = z.join(',');
                    }
                    else {
                        this.overRideRatingObj.To = z[0];
                    }
                }
                else {
                    if (obj.ClientEmail.includes('unknown')) {
                        this.overRideRatingObj.To = "";
                    }
                }
            }
        }

        this.userDetail.AssignedCompanyANDCategory.AssignedCompanyList.forEach(function (Addobj) {
            if (Addobj.CompanyId == that.userCredential.CompanyId) {
                LabEmail = Addobj.LaboratoryEmail;
            }
        });

        this.overRideRatingObj.LaboratoryEmailId = LabEmail;
        this.SubjectText = " MTS report no. " + obj.ReportNo;
        this.overRideRatingObj.Subject = this.SubjectText + " (Product disposition – Retest)";
        var link = window.location.href.split('/#/')[0];
        this.overRideRatingObj.WebLink = link + '/#/web-link/order/' + btoa(obj.ReportNo);

        this.overRideRatingObj.ReportNo = obj.ReportNo;
        this.overRideRatingObj.ClientRating = null;
        this.overRideRatingObj.ReviewStatus = 'Reviewed';
        this.overRideRatingObj.ClientRemarks = null;
        this.overRideRatingObj.UploadedImage = null;
        this.overRideRatingObj.FileName = '';
        this.overRideRatingObj.ModifiedById = this.userDetail.UserId;
        this.overRideRatingObj.ModifiedByName = this.userDetail.FirstName + ' ' + this.userDetail.LastName;
        this.overRideRatingInfo.Subject = this.overRideRatingObj.Subject;

        (<any>$('#confirmDialog')).modal('show');
        //New Model creation, set value here.
        this.checkedCCEmail = new Array<string>();
        this.overRideRatingInfo.Subject = this.overRideRatingObj.Subject;
        this.overRideRatingInfo.WebLink = this.overRideRatingObj.WebLink;
        this.overRideRatingInfo.ReportNo = this.overRideRatingObj.ReportNo;
        this.overRideRatingObj.ModifiedByName = this.overRideRatingObj.ModifiedByName;
        this.overRideRatingObj.ModifiedById = this.overRideRatingObj.ModifiedById;
        // build To field Value
        if (this.overRideRatingObj.VendorEmailId) {
            this.overRideRatingInfo.To = this.overRideRatingObj.VendorEmailId;
        }
        if (this.overRideRatingObj.LaboratoryEmailId) {
            if (this.overRideRatingInfo.To) {
                this.overRideRatingInfo.To = this.overRideRatingObj.VendorEmailId + ',' + this.overRideRatingObj.LaboratoryEmailId;
            }
            else {
                this.overRideRatingInfo.To = this.overRideRatingObj.LaboratoryEmailId;
            }
        }
        //build CC feild value
        if (this.overRideRatingObj.ManufacturerEmailId != "" && this.overRideRatingObj.ManufacturerEmailId != null) {
            var y = this.overRideRatingObj.ManufacturerEmailId.toLowerCase().split(',');
            if (y.length > 0) {
                y.forEach((val, i) => {
                    if (val.includes('nothing')) {

                    }
                    else {
                        let k = this.CCEmailList.findIndex(j => j.EmailId == val)
                        if (k > -1) {
                            //Already added this email
                        } else {
                            this.CCEmailList.push({
                                "EmailId": val,
                                "displayChecked": true,
                            });
                            this.checkedCCEmail.push(val);
                        }
                    }
                })
            }
        }

        if (this.overRideRatingObj.cc != "" && this.overRideRatingObj.cc != null) {
            var y = this.overRideRatingObj.cc.toLowerCase().split(',');
            if (y.length > 0) {
                y.forEach((val, i) => {
                    let k = this.CCEmailList.findIndex(j => j.EmailId == val)
                    if (k > -1) {
                        //Already added this email
                    } else {
                        this.CCEmailList.push({
                            "EmailId": val,
                            "displayChecked": true,
                        });
                        this.checkedCCEmail.push(val);
                    }
                })
            }
        }
    }

    //Ankita: this method is no longer used, need to remove after verifying once.
    reviewRating(isOverride) {
        if (isOverride == 'true') {
            (<any>$('#confirmDialog')).modal('hide');
            (<any>$('#Overriderating')).modal('show');
            this.viewHistoryList = [];
            this.dataService.get('Order/GetFailOrderTestHistory?ReportNo=' + this.overRideRatingObj.ReportNo).subscribe(
                r => {
                    this.isLoader = false;
                    this.viewHistoryList = r.FailCodeList;
                }
            )
        }
        else {
            this.checkedReportArr = [];
            this.isLoader = false;
            this.reviewRadio = 'Reviewed';
            this.overRideRatingInfo.ReviewStatus = 'Reviewed';
            this.sendComment();
        }
    }

    selectAll(val) {
        if ((<any>$("#checkOverride")).prop('checked')) {
            this.viewHistoryList.forEach(function (addObj) {
                addObj.Active = true
            });
        } else {
            this.viewHistoryList.forEach(function (addObj) {
                addObj.Active = false
            });
        }
    }

    selectCheck(index: any) {

        var check = true;
        if (this.viewHistoryList[index].Active == true) {
            this.viewHistoryList[index].Active = false
        } else {
            this.viewHistoryList[index].Active = true
        }
        this.viewHistoryList.forEach(function (addObj) {

            if (addObj.Active != true) {
                check = false;
            }
        });
        if (check == true) {
            (<any>$("#checkOverride")).prop("checked", true)

        } else {
            (<any>$("#checkOverride")).prop("checked", false)
        }
    }

    reviewRadioType(type: string) {
        if (type == 'Retest') {
            this.reviewRadio = type;
            this.overRideRatingObj.Subject = this.SubjectText + " (Product disposition – Retest)";
            this.overRideRatingInfo.Subject = this.overRideRatingObj.Subject
        }
        else if (type == 'Override') {
            if (this.viewHistoryList.length > 0) {
                this.overRideRatingObj.Subject = this.SubjectText + " (Product disposition – Override)";
                this.overRideRatingInfo.Subject = this.overRideRatingObj.Subject
                this.reviewRadio = type;
            } else {
                this.reviewRadio = 'Retest';
                (<any>$("#Override")).prop("checked", false);
                (<any>$("#Retest")).prop("checked", true);
                swal("", "There is no test to override.")
            }

        }
        else if (type == 'Reviewed') {
            this.reviewRadio = type;
        }
    }

    sendComment() {

        this.overRideRatingInfo.CC = this.checkedCCEmail.join(",");
        this.overRideRatingInfo.TDetail = [];
        if (this.reviewRadio == 'Retest') {
            this.overRideRatingInfo.ReviewStatus = 'Retest';
        }
        else if (this.reviewRadio == 'Override') {
            this.overRideRatingInfo.ReviewStatus = 'Overriding';
            this.overRideRatingInfo.TDetail = this.viewHistoryList;
        }
        this.overRideRatingInfo.CreatedBy = this.userDetail.UserId;
        var data = new FormData();
        data.append("UploadedImage", this.overRideRatingInfo.UploadedImage);
        if (this.overRideRatingInfo.To == "" && this.overRideRatingInfo.CC == "") {

            swal('', "Please enter any emailid");
            return;
        } else {
            this.overRideRatingInfo.VendorEmailId = this.overRideRatingInfo.To;
            data.append("ProductDisposition", JSON.stringify(this.overRideRatingInfo));
        }


        if (this.userCredential.CompanyId == '386') {
            var CompanyName = this.userDetail.CompanyName;
            var value = {
                EmailID: "SFTestReports@stitchfix.com, Robin.Abshier@stitchfix.com",
                CompanyName: CompanyName,
                FileType: "DispositionFile",
                BusinessCategoryId:this.userCredential.BusinessCategoryId
            }
            data.append("ShareData", JSON.stringify(value));
        }

        else {
            var CompanyName = this.userDetail.CompanyName;
            var value1 = {
                EmailID: this.userDetail.EmailId,
                CompanyName: CompanyName,
                FileType: "DispositionFile",
                BusinessCategoryId:this.userCredential.BusinessCategoryId
            }
            data.append("ShareData", JSON.stringify(value1));
        }


        this.isLoader = true;
        var thisObj = this;
        var returnObj = thisObj.dataService.postFile('Order/ProductDisposition', data);
        returnObj.done(function (xhr, textStatus) {
            thisObj.dataService.post('Order/GetAllOrderDynamicListWithView', thisObj.searchModal).subscribe(
                r => {
                    (<any>$('#Overriderating')).modal('hide');
                    thisObj.isLoader = false;
                    if (r.IsSuccess) {
                        thisObj.orderList = r.Dt;
                        thisObj.totalItem = Math.ceil(r.TotalRecords / thisObj.searchModal.PageSize);
                    }
                    else {
                    }
                }
            )
        })

        this.reset();

    }

    onChange(event) {
        this.overRideRatingObj.FileName = event.target.files[0].name;
        this.overRideRatingObj.UploadedImage = event.target.files[0];
    }

    getBase64(file) {
        var thisObj = this;
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            thisObj.overRideRatingObj.Base64 = reader.result;
        };
        reader.onerror = function (error) {
        };
    }

    viewFailHistory(obj) {
        this.reportNo = obj.ReportNo;
        this.viewHistoryList = [];
        (<any>$('#ViewFail')).modal('show');
        this.dataService.get('Order/GetFailOrderTestHistory?ReportNo=' + obj.ReportNo).subscribe(
            r => {
                this.isLoader = false;
                this.viewHistoryList = r.FailCodeList;
            }
        )

    }

    // reset() {
    //     this.fileUploader.nativeElement.value = '';
    //     this.overRideRatingObj.FileName = null;
    //     this.overRideRatingObj.UploadedImage
    // }

    onDateRangeChanged(event: any) {
        this.dateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
        this.dateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
        //this.dateRange = this.searchModal.StartDate + " - " + this.searchModal.EndDate;
    }
    logoutonDateRangeChanged(event: any) {
        if(event.beginDate.day > '0' && event.endDate.day> '0'){
            this.LogOutDateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
            this.LogOutDateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
    
            this.searchParam.SearchText[3].ColumnValue = "'" + this.LogOutDateRange.StartDate + "' And '" + this.LogOutDateRange.EndDate + "'";
            
        }
        else{
            this.searchParam.SearchText[3].ColumnValue="";
        }
        // this.LogOutDateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
        // this.LogOutDateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;

        // this.searchParam.SearchText[3].ColumnValue = "'" + this.LogOutDateRange.StartDate + "' And '" + this.LogOutDateRange.EndDate + "'";
    }

    ExportToExcelTrack() {

        this.isLoader = true;
        var that = this;
        this.searchModal.PageNumber = 3;
        this.dataService.post('OrderAnalysis/ExportToExcelTracking', this.searchModal).subscribe(
            response => {

                that.isLoader = false;
                if (response.IsSuccess) {
                    let downloadFileName = encodeURIComponent(response.Message);
                    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                    //that.isLoader = false;
                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }

    handleCheckboxCheck(e: any, report: any) {

        if (e.target.checked) {
            this.checkedReportArr.push(report);
        }
        else {
            let index = this.checkedReportArr.indexOf(report);
            this.checkedReportArr.splice(index, 1);
        }

    }

    batchReview() {

        if (this.checkedReportArr.length == 0) {
            swal('', 'Please choose atleast one report');
        } else {
            //api call
            this.dataService.post('ManageDocuments/productDispositionUpdateReview', this.checkedReportArr).subscribe(
                response => {
                    if (response.IsSuccess) {
                        this.getOrderList();
                        swal('', response.Message);
                        this.checkedReportArr = [];
                        (<any>$('.FileCheck')).prop('checked', false)
                    } else {
                        swal('', response.Message);
                        this.checkedReportArr = [];
                        (<any>$('.FileCheck')).prop('checked', false);
                    }

                }
            )


        }
    }

    myDateRangePickerOptions = {
        clearBtnTxt: 'Clear',
        beginDateBtnTxt: 'From',
        endDateBtnTxt: 'To',
        acceptBtnTxt: 'OK',
        dateFormat: 'dd/mm/yyyy',
        firstDayOfWeek: 'mo',
        sunHighlight: true,
        height: '34px',
        width: '260px',
        inline: false,
        selectionTxtFontSize: '12px',
        alignSelectorRight: false,
        indicateInvalidDateRange: true,
        showDateRangeFormatPlaceholder: false,
        customPlaceholderTxt: 'Login Date-Range',
        showClearBtn: false
    };




    // method for new product dispostion popup

    CCEmailListshow() {
        (<any>$("#CCEmailList")).modal('show');
    }

    handleCheckboxCCList(evt, i) {
        let emailId = this.CCEmailList[i].EmailId;

        if (evt.target.checked) {
            let index = this.checkedCCEmail.findIndex(i => i == emailId);
            if (index > -1) {
                //Already added this email
            }
            else {
                this.checkedCCEmail.push(emailId);
                this.CCEmailList[i].displayChecked = true;
            }
        }
        else {
            let index = this.checkedCCEmail.findIndex(i => i == emailId);
            this.checkedCCEmail.splice(index, 1);
            this.CCEmailList[i].displayChecked = false;
        }
    }

    handleCCinputfeild(evt) {
        var keycode = evt.charCode || evt.keyCode;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.CCMailId = this.CCMailId.trim();
        if (keycode == 188) {
            this.CCMailId = this.CCMailId.substring(0, this.CCMailId.length - 1);
        }

        if (this.CCMailId) {
            if (reg.test(this.CCMailId) == true && (keycode == 32 || keycode == 13 || keycode == 188)) {
                this.checkedCCEmail.push(this.CCMailId)
                this.CCMailId = "";
            }
        }
    }

    handleToinputfeild(evt) {
        var keycode = evt.charCode || evt.keyCode;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.ToMailId = this.ToMailId.trim();
        if (keycode == 188) {
            this.ToMailId = this.ToMailId.substring(0, this.ToMailId.length - 1);
        }

        if (this.ToMailId) {
            if (reg.test(this.ToMailId) == true && (keycode == 32 || keycode == 13 || keycode == 188)) {
                if (this.overRideRatingInfo.To) {
                    this.overRideRatingInfo.To = this.overRideRatingInfo.To + "," + this.ToMailId;
                }
                else {
                    this.overRideRatingInfo.To = this.ToMailId;
                }
                this.ToMailId = "";
            }
        }
    }

    addCC() {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (this.CCMailId) {
            if (reg.test(this.CCMailId) == true) {
                this.checkedCCEmail.push(this.CCMailId)
                this.CCMailId = "";
            }
        }
    }

    addTo() {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (this.ToMailId) {
            if (reg.test(this.ToMailId) == true) {
                if (this.overRideRatingInfo.To) {
                    this.overRideRatingInfo.To = this.overRideRatingInfo.To + "," + this.ToMailId;
                }
                else {
                    this.overRideRatingInfo.To = this.ToMailId;
                }
                this.ToMailId = "";
            }
        }
    }

    fileChangeEvent(event) {

        if (event.target.files.length > 0) {
            this.overRideRatingInfo.FileName = event.target.files[0].name;
            this.overRideRatingInfo.UploadedImage = event.target.files[0];
        }
    }

    closeoverRideRatingModal() {
        this.fileUploader.nativeElement.value = "";
        this.overRideRatingInfo.UploadedImage = null;
        this.overRideRatingInfo.FileName = null;
        this.overRideRatingInfo = new T_OverRideRating();
        this.checkedCCEmail = new Array<string>();
        this.CCEmailList = new Array<CCEmailList>();
    }

    removedEmailFromCC(emailId) {
        let index = this.checkedCCEmail.findIndex(i => i == emailId);
        this.checkedCCEmail.splice(index, 1);
        let j = this.CCEmailList.findIndex(k => k.EmailId == emailId);
        if (j > -1) {
            this.CCEmailList[j].displayChecked = false;
        }
    }

    removedEmailFromTo(emailId) {
        let splitToEmailArr = this.overRideRatingInfo.To.split(",");
        let index = splitToEmailArr.findIndex(i => i == emailId);
        splitToEmailArr.splice(index, 1);
        this.overRideRatingInfo.To = splitToEmailArr.join(",");
        if (!this.overRideRatingInfo.To) {
            this.overRideRatingInfo.To = null;
        }
    }

    removedShareFile() {
        this.fileUploader.nativeElement.value = "";
        this.overRideRatingObj.FileName = null;
        this.overRideRatingObj.UploadedImage = null;
        this.overRideRatingInfo.FileName = null;
        this.overRideRatingInfo.UploadedImage = null;
    }

    reset() {
        this.fileUploader.nativeElement.value = "";
        this.overRideRatingInfo.FileName = null;
        this.overRideRatingInfo.UploadedImage = null;
        this.overRideRatingInfo.FileName = null;
        this.overRideRatingInfo.UploadedImage = null;
    }

    openPopup(reportNo: string, type: string) {
        this.selectedReportNo = reportNo;
        this.isLoader = true;
        this.previousCommentList = [];
        (<any>$('#showComments')).modal('show');
        this.dataService.get('order/GetOrderPreviousComments?ReportNo=' + reportNo + "&userId=" + this.userCredential.UserId + "&UserType=" + type).subscribe(
            r => {
                this.isLoader = false;
                this.previousCommentList = r.CommentsList;
            }
        )
    }
}