import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductDispositionRoutingModule } from './product-disposition-routing.module';
import { ProductDispositionComponent } from './product-disposition.component';
import { HeaderModule } from '../../header/header.module';
import { FooterModule } from '../../footer/footer.module';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { Ng2PaginationModule } from 'ng2-pagination';
import { MdCoreModule } from 'md2';
import { BulkEmailModule } from '../../bulk-email/bulk-email.module';
import { MdInputModule } from '@angular2-material/input';
import {TooltipModule} from "ngx-tooltip";

@NgModule({
  imports: [
    CommonModule, FormsModule, MaterialModule, BulkEmailModule, MdInputModule, MdCoreModule, Ng2PaginationModule, HeaderModule, FooterModule, DateTimePickerModule, MyDateRangePickerModule,TooltipModule,
    ProductDispositionRoutingModule
  ],
  declarations: [ProductDispositionComponent]
})
export class ProductDispositionModule { }
