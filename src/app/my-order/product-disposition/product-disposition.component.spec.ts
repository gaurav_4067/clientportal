import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDispositionComponent } from './product-disposition.component';

describe('ProductDispositionComponent', () => {
  let component: ProductDispositionComponent;
  let fixture: ComponentFixture<ProductDispositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDispositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDispositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
