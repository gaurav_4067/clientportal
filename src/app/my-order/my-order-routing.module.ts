import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoContentComponent } from '../no-content/no-content.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: NoContentComponent
    },
    {
      path: 'order-tracking',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./order-tracking/order-tracking.module').OrderTrackingModule);
        })
      })
    },
    {
      path: 'order-communication',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./order-communication/order-communication.module').OrderCommunicationModule);
        })
      })
    },
    {
      path: 'product-disposition',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./product-disposition/product-disposition.module').ProductDispositionModule);
        })
      })
    },
    {
      path: 'inspection-order-tracking',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./inspection-order-tracking/inspection-order-tracking.module').InspectionOrderTrackingModule);
        })
      })
    },
    {
      path: 'inspection-order-communication',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./inspection-order-communication/inspection-order-communication.module').InspectionOrderCommunicationModule);
        })
      })
    },
    {
      path: 'inspection-product-disposition',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./inspection-product-disposition/inspection-product-disposition.module').InspectionProductDispositionModule);
        })
      })
    },

  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyOrderRoutingModule { }
