import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InspectionOrderCommunicationComponent } from 'app/my-order/inspection-order-communication/inspection-order-communication.component';

const routes: Routes = [{
  path:'',
  component:InspectionOrderCommunicationComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InspectionOrderCommunicationRoutingModule { }
