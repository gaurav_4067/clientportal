import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { SEARCH_MODEL } from '../order-communication/order-communication.model';
import { DataService, COLUMN_LIST, Laboratyemailid, BaseUrl, ETRFUrl } from '../../mts.service';
import { Response } from '@angular/http';
import { InjectService } from '../../injectable-service/injectable-service';
import { Hero } from '../../common/model_1';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { Router } from '@angular/router';

declare let swal: any;

@Component({
  selector: 'app-inspection-order-communication',
  templateUrl: './inspection-order-communication.component.html',
  styleUrls: ['./inspection-order-communication.component.css']
})
export class InspectionOrderCommunicationComponent implements OnInit {

  @ViewChild('fileUpload') fileUploader: ElementRef;
  orderList: any;
  private p: any;
  private totalItem: number;
  private itemPerPage: any;
  private dateRange: any;
  private LogOutDateRange: any;
  vendorId: any;

  searchModal: any;
  searchText: any;
  shareCommentObj: any;
  userCredential: any;
  userDetail: any;
  previousComments: boolean;
  previousCommentList: any;
  isLoader: boolean;
  viewHistoryList: any;
  reportNo: string;
  additionalSearch: any;
  testHistoryMessage: string;
  columnList: any;
  searchParam: any;
  dynamicSearchColumns: any;
  dynamicGridColumns: any;
  PageSizeDynamic: any = 10;
  ccEmails: any = [];
  ccEmailTemp: any;
  errorEmail: boolean;
  showDownloadFiles: boolean = false;
  ReportNo: number;
  DownloadfileName: any;
  remarks: any = [];
  bulkEmailSendObject: any = {
    EmailLogID: 0,
    EmailD: '',
    Subject: '',
    Body: '',
    Status: false,
    FileName: '',
    CreatedDate: Date.now(),
    type: '',
    CreatedBy: '',
    CompanyName: '',
    ModifiedBy: 0,
    ModifiedDate: Date.now(),
    CCEmailID: ''
  }

  testFlag1: any = {};
  fileType: any;
  constructor(private dataService: DataService, private injectService: InjectService, private router: Router) {
    this.searchModal = SEARCH_MODEL;
    this.searchModal.PageSize = this.PageSizeDynamic;
    this.searchModal.PageNumber = 1;
    this.searchModal.TotalRecords = 200;
    this.searchModal.OrderBy = 'Bookingid';
    this.searchModal.Order = 'desc';
    this.searchModal.FormId = '2';
  }
  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));

    // this.clientEmailBulkEmail.push(that.userDetail.EmailId);
    /*Divyanshu: Following if block is for checking AuthToken for each Component
   before doing any further task,please discuss with me before removing it.
   */
    if (this.userDetail && this.userCredential) {
      if (this.userCredential.UserPermission.ViewOrderCommunication) {
        this.searchModal.ClientId = this.userCredential.CompanyId;
        this.searchModal.UserId = this.userCredential.UserId;
        this.searchModal.UserType = this.userCredential.UserType;
        this.searchModal.UserCompanyId = this.userCredential.UserCompanyId;
        this.searchModal.BusinessCategoryId = this.userCredential.BusinessCategoryId;
        this.previousCommentList = [{ comment: 'whatever' }, { comment: 'wow its working' }];
        // this.columnList = COLUMN_LIST;
        this.shareCommentObj = {};
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        this.p = 1;
        this.itemPerPage = 1;
        this.totalItem = 0;
        this.dateRange = {};
        this.LogOutDateRange = {};
        /*below for solving bug ot searching and sorting*/
        this.searchParam = {};
        this.searchParam.SearchText = [{ LogicalOperator: "AND", ColumnName: "ReportNo", Operator: 5, ColumnValue: "" },
        { LogicalOperator: "AND", ColumnName: "ReportIssueDate", Operator: 6, ColumnValue: "" }
          // , { LogicalOperator: "AND", ColumnName: "StyleNo", Operator: 5, ColumnValue: "" },
          // { LogicalOperator: "AND", ColumnName: "SampleDescription", Operator: 5, ColumnValue: "" }, 
        ];
        this.searchModal.SearchText = [];
        /*ended*/
        this.additionalSearch = [{ LogicalOperator: "AND", ColumnName: "", Operator: "", ColumnValue: "" }];
        // this.vendorList = [];

        this.testFlag1.flag = false;
        this.isLoader = true;
        if (this.injectService.additionalSearch) {
          if (this.searchParam.SearchText.length > 4) {
            var count = this.searchParam.SearchText.length - 4;
            this.searchParam.SearchText.splice(4, count);
          }
          var that = this;
          this.injectService.additionalSearch.forEach(function (addObj) {
            that.searchParam.SearchText.push(addObj);
          });
          //addded by Divyanshu
          this.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
        }
        this.dataService.get('Order/GetMyOrderColumnsList?selectedCompany=' + this.searchModal.ClientId + '&FormId=2&BusinessCategoryId=' + this.userCredential.BusinessCategoryId).subscribe(
          response => {
            if (response.IsSuccess) {
              this.dynamicSearchColumns = response.SelectedSearchColumn;
              this.dynamicGridColumns = response.SelectedGridColumn;
              this.columnList = response.SelectedAdHocSearchColumn;
              this.getOrderList();
            }
            else {
              this.getOrderList();
              swal('', response.Message)
            }
          }
        );
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }
  getOrderList() {
    this.isLoader = true;
    this.dataService.post('Order/Insp_GetAllOrderDynamicListWithView', this.searchModal).subscribe(
      response => {
        this.isLoader = false;
        if (response.IsSuccess) {

          this.orderList = response.Dt;
          this.totalItem = Math.ceil(response.TotalRecords / this.searchModal.PageSize);
          // this.clientEmail = response.Dt[0].ClientEmail;
          // this.clientId = response.Dt[0].ClientID;
          // this.checkedReportNo = [];

        }
        else {
          swal('', response.Message);
        }
      }
    )
  }
  addSearch() {

    if (this.additionalSearch.length > 0) {
      if (this.additionalSearch[this.additionalSearch.length - 1].LogicalOperator && this.additionalSearch[this.additionalSearch.length - 1].ColumnName && this.additionalSearch[this.additionalSearch.length - 1].Operator) {
        this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
      }
    }
    else {
      this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
    }
  }

  removeSearch(index: number) {
    if (this.additionalSearch.length > 1) {
      this.additionalSearch.splice(index, 1);
    }
  }

  resetSearchForm() {

    this.PageSizeDynamic = 10;
    (<any>$('.btnclearenabled')).click();
    this.searchParam.SearchText[0].ColumnValue = '';
    this.searchParam.SearchText[1].ColumnValue = '';
    this.searchParam.SearchText[2].ColumnValue = '';
    this.searchParam.SearchText[3].ColumnValue = '';
    this.PageSizeDynamic = 10;
    for (let i of this.dynamicSearchColumns) {
      i.ColumnValue = '';
    }
    for (let i in this.additionalSearch[0]) {
      this.additionalSearch[0].ColumnName = "";
      this.additionalSearch[0].ColumnValue = "";
      this.additionalSearch[0].LogicalOperator = "AND";
      this.additionalSearch[0].Operator = "";
    }
    (<any>$('.resetSelect')).prop('selectedIndex', 0);
    (<any>$('.resetDynamicInput')).val('');
    for (var i = this.additionalSearch.length; i > 0; i--) {

      this.additionalSearch.splice(i, 1);
    }
    this.PageSizeDynamic = 10;
    this.search(1);
  }
  search(Type: any) {
    let dynamicSearchLength = 0;
    let thisObj = this;
    /*below for solving bug ot searching and sorting*/
    if (thisObj.dateRange.StartDate && thisObj.dateRange.EndDate) {
      thisObj.searchModal.StartDate = JSON.parse(JSON.stringify(thisObj.dateRange.StartDate));
      thisObj.searchModal.EndDate = JSON.parse(JSON.stringify(thisObj.dateRange.EndDate));
    }
    thisObj.searchModal.SearchText = JSON.parse(JSON.stringify(thisObj.searchParam.SearchText));
    /*ended*/
    if (thisObj.dynamicSearchColumns) {
      dynamicSearchLength = thisObj.dynamicSearchColumns.length;
      thisObj.searchModal.SearchText.splice(4, thisObj.searchModal.SearchText.length - 4);
      thisObj.dynamicSearchColumns.forEach(function (column) {
        thisObj.searchModal.SearchText.push({ "LogicalOperator": "AND", "ColumnName": column.ColumnName, "Operator": 5, "ColumnValue": column.ColumnValue })
      })
    }
    if (thisObj.searchModal.SearchText.length > 4 + dynamicSearchLength) {
      let count = thisObj.searchModal.SearchText.length - 4 + dynamicSearchLength;
      thisObj.searchModal.SearchText.splice(4 + dynamicSearchLength, count);
    }
    thisObj.additionalSearch.forEach(function (addObj) {
      thisObj.searchModal.SearchText.push(addObj);
    })
    this.searchModal.PageSize = this.PageSizeDynamic;
    thisObj.searchModal.PageNumber = 1;
    if (Type == 1) {
      thisObj.getOrderList();
    }
    else {
      thisObj.ExportToExcelTrack();
    }
  }
  selectSize() {
    this.search(1);
  }

  getPage(page: number) {
    this.searchModal.PageNumber = page;
    this.isLoader = true;
    this.dataService.post('Order/Insp_GetAllOrderDynamicListWithView', this.searchModal).subscribe(
      r => {
        this.isLoader = false;
        if (r.IsSuccess) {
          this.p = page;
          this.orderList = r.Dt;
        }
        else {
          swal('', r.Message);
        }
      }
    )
  }
  showDownload() {
    this.showDownloadFiles = this.showDownloadFiles == true ? this.showDownloadFiles = false : this.showDownloadFiles = true;
  }
  download(fileName: any, data: any, reportNo: any) {
    this.fileType = data;
    this.ReportNo = reportNo;
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userDetail.EmailId = this.userDetail.EmailId;
    (<any>$('#DownloadFile')).modal('show');
    if (data == 'TRF') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileName.trim();
    }
    else if (data == 'Report') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileName.trim();
    }
    else if (data == 'SUP' || data == 'GCC') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileName.trim();
    }
    else if (data = 'Invoice') {
      this.bulkEmailSendObject.type = data;
      this.DownloadfileName = fileName.trim();
    }
    // this.checkedReportNo = [];
    this.ccEmails = [];
    this.remarks = [];
    this.showDownloadFiles = false;
    (<any>$('.CheckPop')).prop('checked', false)
    // let downloadFileName = encodeURIComponent(fileName.trim());
    // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim();;
  }

  downloadFile() {
    var that = this;
    if (that.showDownloadFiles && that.userDetail.EmailId == "") {
      swal('', "Please enter emailid");
      return;
    } else {
      that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
    }
    if (this.showDownloadFiles == true) {
      //that.bulkEmailSendObject.EmailD = that.userDetail.EmailId;
      that.bulkEmailSendObject.ReportNo = that.ReportNo;
      that.bulkEmailSendObject.Subject = "MTS - Test Report No.: " + that.ReportNo;
      that.bulkEmailSendObject.Body = that.remarks[0] ? that.remarks[0] : "";
      that.bulkEmailSendObject.FileName = that.DownloadfileName;
      that.bulkEmailSendObject.CreatedBy = that.userCredential.CompanyId;
      that.bulkEmailSendObject.CCEmailID = that.ccEmails.join(',');
      that.bulkEmailSendObject.FileStatus = 'D';
      that.bulkEmailSendObject.CompanyName = that.userDetail.CompanyName;
      that.bulkEmailSendObject.FileType=this.fileType;
      that.bulkEmailSendObject.BusinessCategoryId = that.userCredential.BusinessCategoryId;
      that.dataService.post('Order/BulkEmailTOClient', that.bulkEmailSendObject).subscribe(
        response => {
          if (response.IsSuccess) {
            swal('', response.Message);
            // that.checkedReportNo = [];
            that.ccEmails = [];
            that.remarks = [];
            that.userDetail.EmailId = "";
            (<any>$('#DownloadFile')).modal('hide');

            // let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
            // window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "";
            // window.location.href = BaseUrl + "ManageDocuments/DownloadOrderFile?FileName=" + downloadFileName.trim() + "&ReportNo="+that.ReportNo;
          }
          else {
            swal('', response.Message);
          }
        }
      )
    }
    else {
      let downloadFileName = encodeURIComponent(this.DownloadfileName.trim());
      // window.location.href = BaseUrl + "ManageDocuments/DownloadOrderFile?FileName=" + downloadFileName.trim() + "&ReportNo="+that.ReportNo;
      window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "&fileType=" + that.bulkEmailSendObject.type + "&businessCategoryId=" + this.userCredential.BusinessCategoryId;
    }
  }

  sorting(column: string) {
    //this.searchModal.SearchText = [{ ColumnName: "ReportNo", Operator: 5, ColumnValue: "" }, { ColumnName: "StyleNo", Operator: 5, ColumnValue: "" }, { ColumnName: "SampleDescription", Operator: 5, ColumnValue: "" }];
    this.searchModal.OrderBy = column;
    if (this.searchModal.Order == 'asc') {
      this.searchModal.Order = 'desc';
    }
    else {
      this.searchModal.Order = 'asc';
    }
    this.isLoader = true;
    this.dataService.post('Order/Insp_GetAllOrderDynamicListWithView', this.searchModal).subscribe(
      r => {
        this.isLoader = false;
        if (r.IsSuccess) {
          this.orderList = r.Dt;
          this.totalItem = Math.ceil(r.TotalRecords / this.searchModal.PageSize);
        }
        else {
          swal('', r.Message);
        }
      }
    )
  }

  shareComment(obj) {

    var that = this;
    that.testFlag1.flag = true;
    that.vendorId = obj.ApplicantCompanyId;
    var LabEmail = "";
    this.shareCommentObj = {};
    this.previousComments = false;
    var vendor = "";
    if (obj.ReportSendToEmail != "" && obj.ReportSendToEmail != null) {
      vendor = obj.ReportSendToEmail;
    }
    if (obj.ApplicantEmail != "" && obj.ApplicantEmail != null) {
      var z = obj.ApplicantEmail.toLowerCase().split(',');
      z.forEach(element => {
        if (vendor.toLowerCase().indexOf(element.toLowerCase()) <= -1) {
          if (vendor == "") {
            vendor = element;
          }
          else {
            vendor = vendor + ',' + element;
          }
        }
      });
    }


    if (vendor) {
      var r = vendor.toLowerCase().split(',');
      if (r.length > 0) {
        r.forEach((val, i) => {
          if (val.includes('unknown')) {
            r.splice(i, 1);
          }
        })
        if (r.length > 0) {
          vendor = r.join(',');
        }
        else {
          vendor = r[0];
        }
      }
      else {
        if (vendor.includes('unknown')) {
          vendor = "";
        }
      }
    }
    this.shareCommentObj.VendorEmailId = vendor;
    if (obj.ManufacturerEmailId) {
      var y = obj.ManufacturerEmailId.toLowerCase().split(',');
      if (y.length > 0) {
        y.forEach((val, i) => {
          if (val.includes('unknown')) {
            y.splice(i, 1);
          }
        })
        if (y.length > 0) {
          obj.ManufacturerEmailId = y.join(',');
        }
        else {
          obj.ManufacturerEmailId = y[0];
        }
      }
      else {
        if (obj.ManufacturerEmailId.includes('unknown')) {
          if (this.userCredential.CompanyId == '386') {
            obj.ManufacturerEmailId = 'Nothing';
          } else {
            obj.ManufacturerEmailId = '';
          }
        }
      }
    } else if (this.userCredential.CompanyId == '386') {
      obj.ManufacturerEmailId = 'Nothing';
    }

    this.shareCommentObj.ManufacturerEmailId = obj.ManufacturerEmailId;

    if (this.userCredential.CompanyId == '386')
    // if (this.userCredential.CompanyId == '34')
    {

      var z = obj.ClientEmail.toLowerCase().split(',');
      if (z.length > 0) {
        z.forEach((val, i) => {
          if (val.includes('unknown')) {
            z.splice(i, 1);
          }
        })
        if (z.length > 0) {
          this.shareCommentObj.cc = z.join(',');
        }
        else {
          this.shareCommentObj.cc = z[0];
        }
      }
      else {
        if (obj.ClientEmail.includes('unknown')) {
          this.shareCommentObj.cc = "";
        }
      }
    }
    else {
      this.shareCommentObj.cc = this.userDetail.EmailId;
    }

    // this.userDetail.AssignedCompanyANDCategory.AssignedCompanyList.forEach(function (Addobj) {
    //     if (Addobj.CompanyId == that.userCredential.CompanyId) {
    //         LabEmail = Addobj.LaboratoryEmail;
    //     }
    // });



    if (obj.LaboratoryEmailId) {
      var y = obj.LaboratoryEmailId.toLowerCase().split(',');
      if (y.length > 0) {
        y.forEach((val, i) => {
          if (val.includes('unknown')) {
            y.splice(i, 1);
          }
        })
        if (y.length > 0) {
          obj.LaboratoryEmailId = y.join(',');
        }
        else {
          obj.LaboratoryEmailId = y[0];
        }
      }
      else {
        if (obj.LaboratoryEmailId.includes('unknown')) {
          // if (this.userCredential.CompanyId == '386') {
          //     obj.LaboratoryEmailId = 'Nothing';
          // } else {
          obj.LaboratoryEmailId = '';
          // }
        }
      }
    }


    this.shareCommentObj.LaboratoryEmailId = obj.LaboratoryEmailId;
    this.shareCommentObj.ReportNo = obj.ReportNo;
    this.shareCommentObj.Subject = " MTS Inspection report no. " + obj.ReportNo;
    var link = window.location.href.split('/#/')[0];
    // this.shareCommentObj.WebLink = 'http://clientportal.mts-global.com/#/order/' + btoa(obj.ReportNo);
    this.shareCommentObj.WebLink = link + '/#/web-link/order-inspection/' + btoa(obj.ReportNo);
    this.shareCommentObj.UploadedImage = null;
  }

  getPreviousComments() {
    this.previousComments = true;
    this.previousCommentList = [];
    this.dataService.get('order/GetOrderPreviousComments?ReportNo=' + this.shareCommentObj.ReportNo).subscribe(
      r => {
        this.isLoader = false;
        this.previousComments = true;
        this.previousCommentList = r.CommentsList;
      }
    )
  }

  sendCommentToMail() {
    if (this.shareCommentObj.ManufacturerEmailId == 'Nothing') {
      this.shareCommentObj.ManufacturerEmailId = '';
    }
    // if(!this.shareCommentObj.Subject){
    //     swal('','Please enter subject.');
    // }
    // else if(!this.shareCommentObj.UploadedImage){
    //     swal('','Please choose file');
    // }
    this.shareCommentObj.CreatedBy = this.userDetail.UserId;
    var data = new FormData();
    data.append("UploadedImage", this.shareCommentObj.UploadedImage);
    if (this.shareCommentObj.VendorEmailId == "" && this.shareCommentObj.ManufacturerEmailId == "" && this.shareCommentObj.LaboratoryEmailId == "" && this.shareCommentObj.cc == "") {
      swal('', "Please enter any emailid");
      return;
    } else {
      data.append("ShareComments", JSON.stringify(this.shareCommentObj));
    }
    if (this.userCredential.CompanyId == '386') {
      var CompanyName = this.userDetail.CompanyName;
      var value = {
        EmailID: "SFTestReports@stitchfix.com, Robin.Abshier@stitchfix.com",
        CompanyName: CompanyName
      }
      data.append("ShareData", JSON.stringify(value));
    }
    else {
      var CompanyName = this.userDetail.CompanyName;
      var value1 = {
        EmailID: this.userDetail.EmailId,
        CompanyName: CompanyName
      }
      data.append("ShareData", JSON.stringify(value1));
    }
    if (this.shareCommentObj.Subject && this.shareCommentObj.Comments) {
      let returnObj = this.dataService.postFile('Order/ShareComments', data);
      returnObj.done(function (xhr, textStatus) {
        (<any>$('#ShareComments')).modal('hide');
        swal('', xhr.Message);
      })
    }
    else {
      let returnObj = this.dataService.postFile('Order/ShareComments', data);
      returnObj.done(function (xhr, textStatus) {
        (<any>$('#ShareComments')).modal('show');
        swal('', xhr.Message);
      })
    }
    this.reset();
  }

  onChange(event) {
    this.shareCommentObj.FileName = event.target.files[0].name;
    this.shareCommentObj.UploadedImage = event.target.files[0];
  }

  getBase64(file) {
    var thisObj = this;
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      thisObj.shareCommentObj.Base64 = reader.result;
    };
    reader.onerror = function (error) {
    };
  }
  viewFailHistory(obj) {
    this.testHistoryMessage = 'Test Result';
    this.reportNo = obj.ReportNo;
    this.viewHistoryList = [];
    let url = '';
    if (obj.ReportRating.toLowerCase() == 'fail') {
      url = 'Order/GetPassOrderTestHistory?ReportNo=' + obj.ReportNo;
    }
    else if (obj.ReportRating.toLowerCase() != 'fail') {
      url = 'Order/GetPassOrderTestHistory?ReportNo=' + obj.ReportNo;
    }
    this.isLoader = true;
    this.dataService.get(url).subscribe(

      r => {
        (<any>$('#ViewFail')).modal('show');
        this.isLoader = false;
        this.viewHistoryList = r.FailCodeList;
      }
    )
  }
  reset() {
    this.fileUploader.nativeElement.value = "";
    this.shareCommentObj.FileName = null;
    this.shareCommentObj.UploadedImage = null;
  }
  onDateRangeChanged(event: any) {
    this.dateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
    this.dateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
  }
  logoutonDateRangeChanged(event: any) {
    if (event.formatted) {
      this.LogOutDateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
      this.LogOutDateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
      this.searchParam.SearchText[1].ColumnValue = "'" + this.LogOutDateRange.StartDate + "' And '" + this.LogOutDateRange.EndDate + "'";
    } else {
      this.searchParam.SearchText[1].ColumnValue = "";
    }
  }
  ExportToExcelTrack() {

    this.isLoader = true;
    var that = this;
    this.searchModal.PageNumber = 2;
    this.dataService.post('OrderAnalysis/Insp_ExportToExcelTracking', this.searchModal).subscribe(
      response => {

        that.isLoader = false;
        if (response.IsSuccess) {
          let downloadFileName = encodeURIComponent(response.Message);
          window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
          // that.isLoader = false;
        }
        else {
          swal('', response.Message);
        }
      }
    )
  }

  myDateRangePickerOptions = {
    clearBtnTxt: 'Clear',
    beginDateBtnTxt: 'Begin Date',
    endDateBtnTxt: 'End Date',
    acceptBtnTxt: 'OK',
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'mo',
    sunHighlight: true,
    height: '34px',
    width: '260px',
    inline: false,
    selectionTxtFontSize: '12px',
    alignSelectorRight: false,
    indicateInvalidDateRange: true,
    showDateRangeFormatPlaceholder: false,
    showClearBtn: false,
    customPlaceholderTxt: 'Login Date-Range'
  };

  openVendorDilaog() {
    (<any>$("#vendorEmail")).modal('show');
    this.testFlag1.flag = true;

  }


}
