import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InspectionOrderCommunicationRoutingModule } from './inspection-order-communication-routing.module';
import { InspectionOrderCommunicationComponent } from './inspection-order-communication.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { MdInputModule } from '@angular2-material/input';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { FormsModule } from '@angular/forms';
import { Ng2PaginationModule } from 'ng2-pagination';
import {TooltipModule} from "ngx-tooltip";
import { BulkEmailModule } from 'app/bulk-email/bulk-email.module';
import { VendorEmailModule } from 'app/vendor-email/vendor-email.module';

@NgModule({
  imports: [
    CommonModule,
    InspectionOrderCommunicationRoutingModule,
    HeaderModule,
    FooterModule,
    MdInputModule,
    MyDateRangePickerModule,
    FormsModule,
    Ng2PaginationModule,
    TooltipModule,
    BulkEmailModule,
    VendorEmailModule

  ],
  declarations: [InspectionOrderCommunicationComponent]
})
export class InspectionOrderCommunicationModule { }
