import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionOrderCommunicationComponent } from './inspection-order-communication.component';

describe('InspectionOrderCommunicationComponent', () => {
  let component: InspectionOrderCommunicationComponent;
  let fixture: ComponentFixture<InspectionOrderCommunicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionOrderCommunicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionOrderCommunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
