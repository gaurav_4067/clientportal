import { Component, OnInit } from '@angular/core';
import { DataService } from '../mts.service';
import { RequestAddtionalTestItem, OrderInformation, PackageTest, ApplicantInfo } from './order-information.model';
import { Order } from '../Order/order.component';
import { Router } from '@angular/router';

declare let swal: any;
@Component({
  selector: 'app-order-information',
  templateUrl: './order-information.component.html',
  styleUrls: ['./order-information.component.css']
})
export class OrderInformationComponent implements OnInit {
  userDetails: any;
  userCredential: any;
  packages: any;
  activePopupPackage: any;
  packageSection: boolean;
  selectPackageColorwayPrice: any;
  selectedPackagePrice: any;
  individualLocationId: string;
  colorQuantityRange: string;
  colorwayData: any;
  ColorwayPrice: any;
  colorPopUpPackage: any;
  isAnyPackageSelected: boolean = false;
  isLoader: boolean;
  selectedItems: Array<PackageTest>;
  orderInformationData: RequestAddtionalTestItem;
  public orderInfo: RequestAddtionalTestItem;

  constructor(private dataService: DataService, private router: Router) {
    this.orderInformationData = new RequestAddtionalTestItem();
    this.orderInfo = new RequestAddtionalTestItem();
    this.selectedItems = new Array<PackageTest>();
  }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetails = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userDetails && this.userCredential) {
      if (this.userCredential.UserPermission.ViewOrderInformation) {

      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  search() {
    this.isLoader = true;
    this.dataService.get('RequestAddtionalTestItem/GetDataByEtrfId/' + this.orderInfo.Temp_TRF_ID).subscribe(
      r => {
        if (r.Status != null) {
          this.orderInformationData = r;
          this.orderInformationData.TestItems.map(i => i.Quantity = 1);
          this.orderInfo = this.orderInformationData;
          this.isLoader = false;
        } else {
          this.isLoader = false;
          swal("", "Please enter valid eTRF number.");
          this.orderInformationData = new RequestAddtionalTestItem();
          this.orderInfo = new RequestAddtionalTestItem();
          this.selectedItems = new Array<PackageTest>();
        }
      },
      err => {

      });
  }

  saveInfo() {
    this.orderInfo.TestItems = this.selectedItems;
    this.dataService.post('RequestAddtionalTestItem/AddAdditionalRequiredTestItem', this.orderInfo).subscribe(
      response => {
        if (response.IsSuccess) {
          this.selectedItems = Array<PackageTest>();
          this.orderInfo = new RequestAddtionalTestItem();
          this.orderInformationData = new RequestAddtionalTestItem();
          swal("", response.Message);
        } else {
          swal("", response.Message);
        }
      });
  }

  public opencolorway(event, packageData) {
    if (event.target.value == 'yes') {
      if (packageData.PackageType != 'T' && this.isAnyPackageSelected && !packageData.isSelected) {
        return false;
      }
      this.colorPopUpPackage = packageData;
      this.ColorwayPrice = packageData.ColorwayPrice;
      this.colorwayData = (packageData.ColorwayQty == 0) ? 1 : packageData.ColorwayQty;
      (<any>$('#AddColorway')).modal('show');
    } else {
      packageData.ColorwayQty = 0;
      this.colorPopUpPackage = packageData;
      this.ColorwayPrice = packageData.ColorwayPrice;
      this.colorwayData = 0;
    }
  }
  public colorwaySubmit(pkg) {
    if (this.colorwayData == undefined) {
      this.colorwayData = "";
    }
    if ((this.colorwayData < 1 || this.colorwayData > 100000)) {
      this.colorQuantityRange = "Quantity must be between 1 & 100000";
      return false;
    } else {
      this.colorQuantityRange = "";
    }
    pkg.ColorwayQty = this.colorwayData;
    this.colorwayData = "";
    (<any>$('#AddColorway')).modal('hide');
  }
  public closeColorWay() {
    (<any>$('#B' + this.colorPopUpPackage.Sno)).prop('checked', true);
    this.colorwayData = "";
    this.colorQuantityRange = "";
    (<any>$('#AddColorway')).modal('hide');
  }

  public selectionCheck(event, pkg) {
    var that = this;
    if (pkg.PackageType == 'T') {
      if (!!this.selectedItems) {
        let index = this.selectedItems.findIndex(i => i.Id == pkg.Id);
        if (index > -1) {
          pkg.Quantity = 1;
          this.selectedItems.splice(index, 1);
        } else {
          this.selectedItems.push(pkg);
        }
      } else {
        this.selectedItems.push(pkg);
      }
      this.isAnyPackageSelected = this.selectedItems.length > 0;
    } else {
      // if (event.target.checked) {
      //   this.selectedPackagePrice = pkg.Price;
      //   this.selectPackageColorwayPrice = pkg.ColorwayPrice;
      //   this.dataService.EcommPackageDetails.IsPackages = true;
      //   this.dataService.EcommPackageDetails.IndividualTestItems = [];
      //   if (pkg.isSelectedPacakge != false) {
      //     this.dataService.get('Package/PackageLoction?ItemId=' + 1 + "&ServiceId=" + pkg.ServiceID + "&MappingId=" + pkg.ServicePackageMappingId).subscribe(
      //       response => {
      //         if (response.IsSuccess) {
      //           pkg.Locations = response.Data;
      //           this.packageSection = true;
      //           this.activePopupPackage = pkg;
      //           let index = pkg.Locations.findIndex(i => i.IsDefault == true);
      //           if (index > -1) {
      //             this.activePopupPackage.SelectedLocation = pkg.Locations[index].GroupId;
      //           }
      //           (<any>$('#myModal')).modal('show');
      //         }
      //         else {
      //           swal('', response.Message);
      //         }
      //       });
      //   }
      // } else {
      //   pkg.isSelectedPacakge = false;
      //   pkg.ColorwayPrice = this.selectPackageColorwayPrice;
      //   pkg.Price = this.selectedPackagePrice;
      //   this.isAnyPackageSelected = this.packages.some(x => x.isSelectedPacakge);
      //   pkg.SelectedLocation = 0;
      //   pkg.ColorwayQty = 0;
      // }
    }
    // this.dataService.get("Package/GetPackagesTests?ServicePackageMappingId=" + pkg.ServicePackageMappingId).subscribe(response => {
    //   that.dataService.EcommPackageDetails.PackageTestName = response.Tests;
    //   that.dataService.EcommPackageDetails.PackageName = pkg.Name;
    // });

  }

  restrictUser(event) {
    event.preventDefault();
  }

  restrictBackspace(event) {
    if (event.keyCode === 8) {
      return false;
    }
  }

}
