import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderInformationComponent } from 'app/order-information/order-information.component';

const routes: Routes = [{
  path:'',
  component:OrderInformationComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderInformationRoutingModule { }
