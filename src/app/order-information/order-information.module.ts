import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderInformationRoutingModule } from './order-information-routing.module';
import { OrderInformationComponent } from 'app/order-information/order-information.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { FormsModule } from '@angular/forms';
import { MdInputModule } from '@angular2-material/input';

@NgModule({
  imports: [
    CommonModule,
    OrderInformationRoutingModule,
    HeaderModule,
    FooterModule,
    FormsModule,
    MdInputModule
  ],
  declarations: [OrderInformationComponent]
})
export class OrderInformationModule { }
