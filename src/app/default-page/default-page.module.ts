import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DefaultPageRoutingModule } from './default-page-routing.module';
import { DefaultPageComponent } from './default-page.component';
import { HeaderModule } from '../header/header.module';

@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    DefaultPageRoutingModule
  ],
  declarations: [DefaultPageComponent]
})
export class DefaultPageModule { }
