import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import { Observable }  from 'rxjs/Observable';
const BASE_URL='http://localhost:3000';
export type InternalStateType = {
  [key: string]: any
};

@Injectable()
export class AppState {
  _state: InternalStateType = { };

  constructor(private http:Http) {

  }

  // already return a clone of the current state
  get state() {
    return this._state = this._clone(this._state);
  }
  // never allow mutation
  set state(value) {
    throw new Error('do not mutate the `.state` directly');
  }


  get(prop?: any) {
    // use our state getter for the clone
    const state = this.state;
    return state.hasOwnProperty(prop) ? state[prop] : state;
  }

  set(prop: string, value: any) {
    // internally mutate our state
    return this._state[prop] = value;
  }


  private _clone(object: InternalStateType) {
    // simple object clone
    return JSON.parse(JSON.stringify( object ));
  }
  public postRequest(url):Observable<any>{
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let food={"fruit":"Apple"};
      let body = JSON.stringify(food);
      return    this.http.post(BASE_URL+'/api/food/', body, headers).map((res: Response) => res.json());
  }
    public getRequest(){
      return this.http.get('/app/food.json').map((res:Response) => res.json());
  }
}
