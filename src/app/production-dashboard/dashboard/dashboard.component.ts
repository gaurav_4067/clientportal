import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { LabDivision, LabLocationResult, Login_Summary_Qty_Data, Login_Summary_Qty_Data_Response, JsonDataArray, PopupDataParam, PopupDataSummary } from './dashboard-model';
import { DataService,BaseUrl } from 'app/mts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { IMyDpOptions } from 'mydatepicker';
declare let AmCharts: any;
declare let swal:any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  // isTomorrowDateHoliday: boolean = false;
  // isTomorrowDatePlus1Holiday: boolean = false;
  // isTomorrowDatePlus2Holiday: boolean = false;
  // isTodayDateHoliday: boolean = false;

  routeName: any;
  labLocationId: number = 0;
  graphFileds: any[];
  isLoader: boolean;
  loginSummaryData: Login_Summary_Qty_Data;
  loginSummaryDataResponse: Login_Summary_Qty_Data_Response;
  LabLocationResult: LabLocationResult;
  LabDivisions: Array<LabDivision>;
  selectedLab: LabDivision;
  selectedLabLocationId: number;
  selectedDivisionId: number;
  tomorrowDate: Date;
  tomorrowDatePlus1: Date;
  tomorrowDatePlus2: Date;
  userCredential: any;
  userDetail: any;
  previousMOnthAndCurrentYear: Date;
  currrentMonthAndPrevYear: Date;
  JsonDataArray: Array<JsonDataArray>;
  validDateSelection = {
    bigBanner: false,
    timePicker: false,
    format: "dd-MMM-yyyy",
    defaultOpen: false,
    addClass: 'wc-date-container1'
  };

  private myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd-mmm-yyyy',
    showClearDateBtn: false
  };
  popupCaption: string;
  PopupDataParam: PopupDataParam;
  PopupDataSummary: Array<PopupDataSummary>;

  private chartId: string = "SLchartdiv40";
  RequestedDate: Date;
  PickerDate: any = { date: { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() } };
  displayAuthenticateHeader: boolean = false;
  constructor(private dataService: DataService, private router: Router, private activateRoute: ActivatedRoute) {
    this.selectedLab = new LabDivision();
    this.LabDivisions = new Array<LabDivision>();
    this.LabLocationResult = new LabLocationResult();
    this.loginSummaryData = new Login_Summary_Qty_Data();
    this.loginSummaryDataResponse = new Login_Summary_Qty_Data_Response();
    this.JsonDataArray = new Array<JsonDataArray>();
    this.PopupDataSummary = new Array<PopupDataSummary>();
  }

  ngAfterViewInit() {
    // this.loginSummaryDataResponse.TestingLabs.forEach(element => {
    //   this.DrawChart(element.DivisionName);
    // });

    // this.Draw4Chart();
  }
  ngOnInit() {
    this.activateRoute.params.subscribe((params: Params) => {
      if (params["labLocationId"]) {
        let labId = atob(params["labLocationId"])
        this.labLocationId = +labId;
      }
    });

    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      if (this.userCredential.UserPermission.ViewProductionDashboard) {
        // if (this.labLocationId) {
        //   this.GetLabDivisionForLIMS();
        // }
        // else {
        this.GetLabDivision();
        //}
      }
      else {
        if (this.labLocationId) {
          this.GetLabDivisionForLIMS();
        }
        else {
          this.router.navigate(['/dashboard']);
        }
      }
      //this.DrawChart();
    } else {
      if (this.labLocationId) {
        this.GetLabDivisionForLIMS();
      }
      else {
        this.router.navigate(['/login']);
      }
    }
  }

  GetDashboardFilterData() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      this.GetDashboardData();
      //this.DrawChart();
    } else {
      if (this.labLocationId) {
        this.GetDashboardDataForLims();
      }
      else {
        this.router.navigate(['/login']);
      }
    }
  }


  GetLabDivisionForLIMS() {
    this.isLoader = true;
    this.dataService.signUpGet('PD/GetLabAndDivision/' + this.labLocationId).subscribe(
      response => {
        if (response.IsSuccess) {
          this.LabLocationResult = response.Data;
          this.LabDivisions = this.LabLocationResult.LabDivision;
          this.RequestedDate = this.LabLocationResult.CurrentDate;
          let nextDay = new Date(this.LabLocationResult.CurrentDate);
          this.PickerDate = {
            date: {
              year: new Date(this.LabLocationResult.CurrentDate).getFullYear(), month: new Date(this.LabLocationResult.CurrentDate).getMonth() + 1,
              day: new Date(this.LabLocationResult.CurrentDate).getDate()
            }
          };
          this.BuildDate(nextDay);

          // this.previousMonthAndCurrentYear= nextDay.getMonth() +'-'+nextDay.getFullYear();
          //this.currrentMonthAndPrevYear=nextDay.getMonth()+1 +'-'+(nextDay.getFullYear())-1;
          this.selectedLabLocationId = this.LabDivisions[0].LabLocation_Id;

          this.selectedLab = this.LabDivisions.filter(i => i.LabLocation_Id == this.selectedLabLocationId)[0];
          this.selectedDivisionId = this.selectedLab.Divisions[0].Division_Id;
          if (this.selectedDivisionId) {
            this.GetDashboardDataForLims();
            this.isLoader = false;
          }
          else {
            this.isLoader = false;
          }
        }
        else {
          this.isLoader = false;
          //swal('', response.Message)
        }
      }
    );
  }

  GetLabDivision() {
    this.isLoader = true;
    this.dataService.get('PD/GetLabAndDivision/' + this.labLocationId).subscribe(
      response => {
        if (response.IsSuccess) {
          this.LabLocationResult = response.Data;
          this.LabDivisions = this.LabLocationResult.LabDivision;
          this.RequestedDate = this.LabLocationResult.CurrentDate;
          let nextDay = new Date(this.LabLocationResult.CurrentDate);
          this.PickerDate = {
            date: {
              year: new Date(this.LabLocationResult.CurrentDate).getFullYear(), month: new Date(this.LabLocationResult.CurrentDate).getMonth() + 1,
              day: new Date(this.LabLocationResult.CurrentDate).getDate()
            }
          };
          this.BuildDate(nextDay);

          // this.previousMonthAndCurrentYear= nextDay.getMonth() +'-'+nextDay.getFullYear();
          //this.currrentMonthAndPrevYear=nextDay.getMonth()+1 +'-'+(nextDay.getFullYear())-1;
          this.selectedLabLocationId = this.LabDivisions[0].LabLocation_Id;

          this.selectedLab = this.LabDivisions.filter(i => i.LabLocation_Id == this.selectedLabLocationId)[0];
          this.selectedDivisionId = this.selectedLab.Divisions[0].Division_Id;
          if (this.selectedDivisionId) {
            this.GetDashboardData();
            this.isLoader = false;
          }
          else {
            this.isLoader = false;
          }
        }
        else {
          this.isLoader = false;
          //swal('', response.Message)
        }
      }
    );
  }

  onDateSelect(e) {
    var date: any = e.date.month + '/' + e.date.day + '/' + e.date.year;
    date = new Date(e.jsdate);
    let selectedDate = new Date(date.getUTCFullYear(),
      date.getUTCMonth(),
      date.getUTCDate(),
      date.getUTCHours(),
      date.getUTCMinutes(),
      date.getUTCSeconds()
    );

    this.PickerDate = new Date(date);
    this.RequestedDate = new Date(selectedDate.setDate(selectedDate.getDate() + 1));
    this.BuildDate(selectedDate);

    this.GetDashboardFilterData();
  }

  private BuildDate(nextDay: Date) {
    let pmcy = new Date(nextDay);
    let cmpy = new Date(nextDay);

    //this.todayDate=nextDay;
    // let selectedDay = nextDay.getDay();
    // this.loginSummaryDataResponse.isTodayDateHoliday = this.getClassName(selectedDay, nextDay);

    this.tomorrowDate = new Date(nextDay.setDate(nextDay.getDate() + 1));
    // let tomorrowDate = new Date(this.tomorrowDate);
    // let tomorrowDay = tomorrowDate.getDay();
    // this.loginSummaryDataResponse.isTomorrowDateHoliday = this.getClassName(tomorrowDay, tomorrowDate);

    this.tomorrowDatePlus1 = new Date(nextDay.setDate(nextDay.getDate() + 1));
    // let plus1Date = new Date(this.tomorrowDatePlus1);
    // let plus1Day = plus1Date.getDay();
    // this.loginSummaryDataResponse.isTomorrowDatePlus1Holiday = this.getClassName(plus1Day, plus1Date);

    this.tomorrowDatePlus2 = new Date(nextDay.setDate(nextDay.getDate() + 1));
    // let plus2Date = new Date(this.tomorrowDatePlus2);
    // let plus2Day = plus2Date.getDay();
    // this.loginSummaryDataResponse.isTomorrowDatePlus2Holiday = this.getClassName(plus2Day, plus2Date);

    this.previousMOnthAndCurrentYear = new Date(pmcy.setMonth((pmcy.getMonth() - 1)));
    this.currrentMonthAndPrevYear = new Date(cmpy.setFullYear((cmpy.getFullYear() - 1)));
  }


  getClassName(day, date) {

    if (day == 0 || day == 6) {
      return true;
    }
    else {

    }
  }

  FilterDivision() {
    let selectedLab = this.LabDivisions.filter(i => i.LabLocation_Id == this.selectedLabLocationId);
    this.selectedLab = selectedLab[0];
    this.selectedDivisionId = this.selectedLab.Divisions[0].Division_Id;
    if (this.selectedDivisionId) {
      this.GetDashboardData();
    }
  }

  GetDashboardDataForLims() {
    this.isLoader = true;
    let requestedObject = { LabLocation_Id: this.selectedLabLocationId, Division_Id: this.selectedDivisionId, RequestedDate: this.RequestedDate }
    this.dataService.signUpPost('PD/GetDashboardData', requestedObject).subscribe(
      response => {
        if (response.IsSuccess) {
          this.loginSummaryDataResponse = response.Data;
          this.loginSummaryData = this.loginSummaryDataResponse.Login_Summary_Qty_Data;

          this.DrawPerformanceWorkStationChart("4chart");
          this.DrawPerformanceWorkStationChart("3chart");
          // this.DrawTestingLabChart("SLChart");
          // this.DrawTestingLabChart("HLChart");
          // this.DrawTestingLabChart("UTChart");
          // this.DrawTestingLabChart("FTChart");
          // this.DrawTestingLabChart("ANChart");
          // this.DrawTestingLabChart("FOODChart");
          this.isLoader = false;
        }
        else {
          this.isLoader = false;
          //swal('', response.Message)
        }
      }
    );
  }

  GetDashboardData() {
    this.isLoader = true;
    let requestedObject = { LabLocation_Id: this.selectedLabLocationId, Division_Id: this.selectedDivisionId, RequestedDate: this.RequestedDate }
    this.dataService.post('PD/GetDashboardData', requestedObject).subscribe(
      response => {
        if (response.IsSuccess) {
          this.loginSummaryDataResponse = response.Data;
          this.loginSummaryData = this.loginSummaryDataResponse.Login_Summary_Qty_Data;

          this.DrawPerformanceWorkStationChart("4chart");
          this.DrawPerformanceWorkStationChart("3chart");
          // this.DrawTestingLabChart("SLChart");
          // this.DrawTestingLabChart("HLChart");
          // this.DrawTestingLabChart("UTChart");
          // this.DrawTestingLabChart("FTChart");
          // this.DrawTestingLabChart("ANChart");
          // this.DrawTestingLabChart("FOODChart");
          this.isLoader = false;
        }
        else {
          this.isLoader = false;
          //swal('', response.Message)
        }
      }
    );
  }
  DrawPerformanceWorkStationChart(chartName): any {
    let that = this;
    var DataArray = [];
    if (chartName == "4chart") {
      DataArray = [{
        "Name": "Sample Login",
        //"AvailableCapacity": 0,
        "OutToday": that.loginSummaryData.LDM_PPW_SampleLogin_OutToday,
        "InToday": that.loginSummaryData.LDM_PPW_SampleLogin_InToday,
        "InBefore": that.loginSummaryData.LDM_PPW_SampleLogin_InBefore,
        //"Target": 0
      }, {
        "Name": "Test Assignment",
        //"AvailableCapacity": 0,
        "OutToday": that.loginSummaryData.LDM_PPW_Test_Assignment_OutToday,
        "InToday": that.loginSummaryData.LDM_PPW_Test_Assignment_InToday,
        "InBefore": that.loginSummaryData.LDM_PPW_Test_Assignment_InBefore,
        //"Target": 0
      }, {
        "Name": "Breakdown",
        //"AvailableCapacity": 0,
        "OutToday": that.loginSummaryData.LDM_PPW_Breakdown_OutToday,
        "InToday": that.loginSummaryData.LDM_PPW_Breakdown_InToday,
        "InBefore": that.loginSummaryData.LDM_PPW_Breakdown_InBefore,
        //"Target": 0
      }, {
        "Name": "Scrapping",
        //"AvailableCapacity": 0,
        "OutToday": that.loginSummaryData.LDM_PPW_Scrapping_OutToday,
        "InToday": that.loginSummaryData.LDM_PPW_Scrapping_InToday,
        "InBefore": that.loginSummaryData.LDM_PPW_Scrapping_InBefore,
        //"Target": 0
      }];
    }
    else if (chartName == "3chart") {
      DataArray = [{
        "Name": "DCD",
        //"AvailableCapacity": 0,
        "OutToday": that.loginSummaryData.LDM_PPW_DCD_OutToday,
        "InToday": that.loginSummaryData.LDM_PPW_DCD_InToday,
        "InBefore": that.loginSummaryData.LDM_PPW_DCD_InBefore,
        //"Target": 0
      }, {
        "Name": "Reporting",
        //"AvailableCapacity": 0,
        "OutToday": that.loginSummaryData.LDM_PPW_Reporting_OutToday,
        "InToday": that.loginSummaryData.LDM_PPW_Reporting_InToday,
        "InBefore": that.loginSummaryData.LDM_PPW_Reporting_InBefore,
        //"Target": 0
      }, {
        "Name": "Logout",
        //"AvailableCapacity": 0,
        "OutToday": that.loginSummaryData.LDM_PPW_Logout_OutToday,
        // "InToday": that.loginSummaryData.LDM_PPW_Logout_InToday,
        // "InBefore": that.loginSummaryData.LDM_PPW_Logout_InBefore,
        "InToday": 0,
        "InBefore": 0,
        //"Target": 0
      }];
    }

    that.JsonDataArray = DataArray;
    that.graphFileds = [];
    // this.graphFileds.push(this.BuildGraphArray('Target', 'Target', '#aeaaaa'));
    that.graphFileds.push(this.BuildGraphArray('InBefore', 'InBefore', "#ffc000"));
    that.graphFileds.push(this.BuildGraphArray('InToday', 'InToday', "#ed7d31"));
    that.graphFileds.push(this.BuildGraphArray('OutToday', 'OutToday', "#ddebf7"));

    AmCharts.makeChart(chartName, {
      "type": "serial",
      "theme": "light",
      "dataProvider": DataArray,
      "valueAxes": [{
        "stackType": "regular",
        "labelsEnabled": false,
        "axisAlpha": 0,
        "gridAlpha": 0
      }],
      "graphs": that.graphFileds,
      "categoryField": "Name",
      "categoryAxis": {
        "gridPosition": "start",
        "labelsEnabled": false,
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left",
        "isResizing": true
      },
      "export": {
        "enabled": false
      }
    });
  }

  BuildGraphArray(title, showTilet, color) {
    var obj = {
      "balloonText": "<b>[[category]] </b><br><span style='font-size:14px'>[[title]]: <b>[[value]]</b></span>",
      "fillAlphas": 1,
      "labelText": "[[value]]",
      "labelPosition": "middle",
      "lineAlpha": 0.5,
      "title": showTilet,
      "type": "column",
      "color": "#000000",
      "lineColor":"#000000",
      "valueField": title,
      "fillColors": color
    }
    // this.graphFileds.push(obj);
    return obj;

  }

  DrawTestingLabChart(chartName): any {

    let that = this;
    var DataArray = [];
    if (chartName == "HLChart") {
      let tlData = that.loginSummaryDataResponse.TestingLabs.filter(i => i.DivisionType == "HL");
      DataArray = [{
        "Name": "LabTesting",
        //"AvailableCapacity": 0,
        "OutToday": tlData[0].LabTesting_OutToday,
        "InToday": tlData[0].LabTesting_InToday,
        "InBefore": tlData[0].LabTesting_InBefore,
        //"Target": 0
      }, {
        "Name": "Resulting",
        //"AvailableCapacity": 0,
        "OutToday": tlData[0].Resulting_OutToday,
        "InToday": tlData[0].Resulting_OutToday,
        "InBefore": tlData[0].Resulting_OutToday,
        //"Target": 0
      }];
    }


    that.JsonDataArray = DataArray;
    AmCharts.makeChart(chartName, {
      "type": "serial",
      "theme": "light",
      "dataProvider": DataArray,
      "valueAxes": [{
        "stackType": "regular",
        "labelsEnabled": false,
        "axisAlpha": 0,
        "gridAlpha": 0
      }],
      "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "labelPosition": "middle",
        "lineAlpha": 0.3,
        "title": "In Before",
        "type": "column",
        "color": "#000000",
        "valueField": "InBefore",
        "fillColors": "#ffc000"
      }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "labelPosition": "middle",
        "lineAlpha": 0.3,
        "title": "In Today",
        "type": "column",
        "color": "#000000",
        "valueField": "InToday",
        "fillColors": "#ed7d31"
      }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "labelPosition": "middle",
        "lineAlpha": 0.3,
        "title": "Out Today",
        "type": "column",
        "color": "#000000",
        "valueField": "OutToday",
        "fillColors": "#ddebf7"
      }],
      "categoryField": "Name",
      "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
      },
      "export": {
        "enabled": true
      }
    });
  }

  GetCalulatedAC(target, InToday, InBefore, OutToday) {
    return target - (InToday + InBefore - OutToday);
  }

  getCSSClass(tomorrowDatePlus2) {
    let aa = tomorrowDatePlus2;
    let plus2Date = new Date(tomorrowDatePlus2);
    let day = plus2Date.getDay();
  }
  // GetDynamicClass(baseValue, comparedValue) {

  //   if (baseValue > 0) {
  //     let halfVaalue = baseValue / 2;
  //     if (halfVaalue >= comparedValue) {
  //       return true;
  //     }
  //   }
  //   else {
  //     return false;
  //   }
  //   // let halfVaalue = baseValue / 2;
  //   // if (halfVaalue >= comparedValue) {
  //   //   return true;
  //   // }
  // }


  ViewReportDetail(MsgText: string, Module: string, SubModule: string, date: any, value: number, DivisionName: string) {
    this.PopupDataParam = new PopupDataParam();
    if (value && value > 0) {
      this.popupCaption = MsgText
      this.PopupDataParam = new PopupDataParam();
      this.PopupDataParam.SubModuleName = SubModule;
      this.PopupDataParam.SearchDate = this.RequestedDate;
      this.PopupDataParam.LabLocationId = this.selectedLabLocationId;
      this.PopupDataParam.DivisionId = this.selectedDivisionId;
      if (Module == 'LabTesting' || Module == 'Resulting') {
        this.PopupDataParam.ModuleName = DivisionName + Module;
      } else {
        this.PopupDataParam.ModuleName = Module;
      }
      this.isLoader = true;

      (<any>$('#ReportDeatil')).modal('show');
      this.getPopUpData(this.PopupDataParam);
      if (SubModule == 'Tomorrow' || SubModule == 'Tomorrow_Plus_1' || SubModule == 'Tomorrow_Plus_2') {
        if (date) {
          var tempDate = new Date(date).getUTCDate() + '-' + (new Date(date).getUTCMonth() + 1) + '-' + new Date(date).getUTCFullYear();
          this.popupCaption = MsgText + '(' + tempDate + ')'
        }
      }
    }
  }

  getPopUpData(obj) {
    this.PopupDataSummary = new Array<PopupDataSummary>();
    this.dataService.signUpPost('PD/GetPopupDataSummary', obj).subscribe(
      response => {
        if (response.IsSuccess) {
          this.PopupDataSummary = response.Data;          
        }else 
        {
       swal('',response.Message);
        }
        this.isLoader = false;
        
      })
  }

  validateMinDate(MyDate) {
    let TempDate = new Date(MyDate);
    let temp = TempDate.getFullYear();
    if (temp > 1) {
      return MyDate
    } else {
      return ''
    }
  }

  ExportToExcelSummaryData(){
    this.dataService.signUpPost('PD/ExportToExcelSummaryData', this.PopupDataParam).subscribe(
      response => {
        if (response.IsSuccess) {
          let downloadFileName = encodeURIComponent(response.Message);
          window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;    
      }else{
       swal('',response.Message);
        }
        this.isLoader = false;
        
      })
  }
}
