import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { FormsModule } from '@angular/forms';
import { FooterModule } from 'app/footer/footer.module';
import { HeaderModule } from 'app/header/header.module';
import { ChartsModule } from '../charts/charts.module';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { MyDatePickerModule } from 'mydatepicker';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DashboardRoutingModule,
    FooterModule,
    HeaderModule,
    ChartsModule,
    AngularDateTimePickerModule,
    MyDatePickerModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
