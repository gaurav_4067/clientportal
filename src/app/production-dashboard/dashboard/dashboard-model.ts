export class LabDivision {
    public LabDivision() {
        this.Divisions = new Array<Division>();
    }
    public LabLocation_Id: number
    public LabLocationName: string
    public LabLocationShortName: string
    public CurrentDate: Date
    public Divisions: Array<Division>
}

export class Division {
    public Division_Id: number
    public DivisionName: string
    public DivisionCode: string
}

export class LabLocationResult {
    public LabLocationResult() {
        this.LabDivision = new Array<LabDivision>();
    }
    public CurrentDate: Date
    public LabDivision: Array<LabDivision>
}


export class Login_Summary_Qty_Data {
    public LDM_ID: number = 0
    public LDM_Date: Date
    public LDM_lab_Location_id: number = 0
    public LDM_lab_Location: string
    public LDM_Division_ID: number = 0
    public LDM_Division: string
    public LDM_No_of_Sample_IN: number = 0
    public LDM_No_of_Sample_Out: number = 0
    public LDM_No_of_Sample_Hold: number = 0
    public LDM_Overdue_Summary_1_day: number = 0
    public LDM_Overdue_Summary_2_day: number = 0
    public LDM_Overdue_Summary_3_day: number = 0
    public LDM_Overdue_Summary_4_day: number = 0
    public LDM_Overdue_Summary_5_day: number = 0
    public LDM_Overdue_Summary_Morethan5_day: number = 0
    public LDM_Report_Out_Early: number = 0
    public LDM_Report_Out_OnTime: number = 0
    public LDM_Report_Out_Late: number = 0
    public LDM_Total_Report_Processed: number = 0
    public LDM_Report_Overdue_Today: number = 0
    public LDM_Report_Due_Total_Today: number = 0
    public LDM_Report_Due_Outstanding_Today: number = 0
    public LDM_Report_Due_Total_Tomorrow: number = 0
    public LDM_Report_Due_Total_Outstanding_Tomorrow: number = 0
    public LDM_Report_Due_Total_Tomorrow_Plus_1_Day: number = 0
    public LDM_Report_Due_Total_Outstanding_Tomorrow_Plus_1_Day: number = 0
    public LDM_Report_Due_Total_Tomorrow_Plus_2_Day: number = 0
    public LDM_Report_Due_Total_Outstanding_Tomorrow_Plus_2_Day: number = 0
    public LDM_Report_Due_Total_Regular_Today: number = 0
    public LDM_Report_Due_Regular_Outstanding_Today: number = 0
    public LDM_Report_Due_Total_Regular_Tomorrow: number = 0
    public LDM_Report_Due_Total_Regular_Outstanding_Tomorrow: number = 0
    public LDM_Report_Due_Total_Regular_Tomorrow_Plus_1_Day: number = 0
    public LDM_Report_Due_Total_Regular_Outstanding_Tomorrow_Plus_1_Day: number = 0
    public LDM_Report_Due_Total_Regular_Tomorrow_Plus_2_Day: number = 0
    public LDM_Report_Due_Total_Regular_Outstanding_Tomorrow_Plus_2_Day: number = 0
    public LDM_Report_Due_Total_Shuttle_Today: number = 0
    public LDM_Report_Due_Shuttle_Outstanding_Today: number = 0
    public LDM_Report_Due_Total_Shuttle_Tomorrow: number = 0
    public LDM_Report_Due_Total_Shuttle_Outstanding_Tomorrow: number = 0
    public LDM_Report_Due_Total_Shuttle_Tomorrow_Plus_1_Day: number = 0
    public LDM_Report_Due_Total_Shuttle_Outstanding_Tomorrow_Plus_1_Day: number = 0
    public LDM_Report_Due_Total_Shuttle_Tomorrow_Plus_2_Day: number = 0
    public LDM_Report_Due_Total_Shuttle_Outstanding_Tomorrow_Plus_2_Day: number = 0
    public LDM_Report_Due_Total_Express_Today: number = 0
    public LDM_Report_Due_Express_Outstanding_Today: number = 0
    public LDM_Report_Due_Total_Express_Tomorrow: number = 0
    public LDM_Report_Due_Total_Express_Outstanding_Tomorrow: number = 0
    public LDM_Report_Due_Total_Express_Tomorrow_Plus_1_Day: number = 0
    public LDM_Report_Due_Total_Express_Outstanding_Tomorrow_Plus_1_Day: number = 0
    public LDM_Report_Due_Total_Express_Tomorrow_Plus_2_Day: number = 0
    public LDM_Report_Due_Total_Express_Outstanding_Tomorrow_Plus_2_Day: number = 0

    public LDM_PPW_SampleLogin_Target: number = 0
    public LDM_PPW_Test_Assignment_Target: number = 0
    public LDM_PPW_Breakdown_Target: number = 0
    public LDM_PPW_Scrapping_Target: number = 0
    public LDM_PPW_DCD_Target: number = 0
    public LDM_PPW_Reporting_Target: number = 0
    public LDM_PPW_Logout_Target: number = 0
    public LDM_PPW_SampleLogin_InBefore: number = 0
    public LDM_PPW_Test_Assignment_InBefore: number = 0
    public LDM_PPW_Breakdown_InBefore: number = 0
    public LDM_PPW_Scrapping_InBefore: number = 0
    public LDM_PPW_DCD_InBefore: number = 0
    public LDM_PPW_Reporting_InBefore: number = 0
    public LDM_PPW_Logout_InBefore: number = 0
    public LDM_PPW_SampleLogin_InToday: number = 0
    public LDM_PPW_Test_Assignment_InToday: number = 0
    public LDM_PPW_Breakdown_InToday: number = 0
    public LDM_PPW_Scrapping_InToday: number = 0
    public LDM_PPW_DCD_InToday: number = 0
    public LDM_PPW_Reporting_InToday: number = 0
    public LDM_PPW_Logout_InToday: number = 0
    public LDM_PPW_SampleLogin_OutToday: number = 0
    public LDM_PPW_Test_Assignment_OutToday: number = 0
    public LDM_PPW_Breakdown_OutToday: number = 0
    public LDM_PPW_Scrapping_OutToday: number = 0
    public LDM_PPW_DCD_OutToday: number = 0
    public LDM_PPW_Reporting_OutToday: number = 0
    public LDM_PPW_Logout_OutToday: number = 0
    public LDM_PPW_SampleLogin_AC: number = 0
    public LDM_PPW_Test_Assignment_AC: number = 0
    public LDM_PPW_Breakdown_AC: number = 0
    public LDM_PPW_Scrapping_AC: number = 0
    public LDM_PPW_DCD_AC: number = 0
    public LDM_PPW_Reporting_AC: number = 0
    public LDM_PPW_Logout_AC: number = 0

    //Analytical
    public LDM_AN_LabTesting_Target: number = 0
    public LDM_AN_LabTesting_InBefore: number = 0
    public LDM_AN_LabTesting_InToday: number = 0
    public LDM_AN_LabTesting_OutToday: number = 0
    public LDM_AN_LabTesting_AC: number = 0
    public LDM_AN_Resulting_Target: number = 0
    public LDM_AN_Resulting_InBefore: number = 0
    public LDM_AN_Resulting_InToday: number = 0
    public LDM_AN_Resulting_OutToday: number = 0
    public LDM_AN_Resulting_AC: number = 0
    //Softline
    public LDM_SL_LabTesting_Target: number = 0
    public LDM_SL_LabTesting_InBefore: number = 0
    public LDM_SL_LabTesting_InToday: number = 0
    public LDM_SL_LabTesting_OutToday: number = 0
    public LDM_SL_LabTesting_AC: number = 0
    public LDM_SL_Resulting_Target: number = 0
    public LDM_SL_Resulting_InBefore: number = 0
    public LDM_SL_Resulting_InToday: number = 0
    public LDM_SL_Resulting_OutToday: number = 0
    public LDM_SL_Resulting_AC: number = 0
    //Hardline
    public LDM_HL_LabTesting_Target: number = 0
    public LDM_HL_LabTesting_InBefore: number = 0
    public LDM_HL_LabTesting_InToday: number = 0
    public LDM_HL_LabTesting_OutToday: number = 0
    public LDM_HL_LabTesting_AC: number = 0
    public LDM_HL_Resulting_Target: number = 0
    public LDM_HL_Resulting_InBefore: number = 0
    public LDM_HL_Resulting_InToday: number = 0
    public LDM_HL_Resulting_OutToday: number = 0
    public LDM_HL_Resulting_AC: number = 0
    //Toys
    public LDM_FT_LabTesting_Target: number = 0
    public LDM_FT_LabTesting_InBefore: number = 0
    public LDM_FT_LabTesting_InToday: number = 0
    public LDM_FT_LabTesting_OutToday: number = 0
    public LDM_FT_LabTesting_AC: number = 0
    public LDM_FT_Resulting_Target: number = 0
    public LDM_FT_Resulting_InBefore: number = 0
    public LDM_FT_Resulting_InToday: number = 0
    public LDM_FT_Resulting_OutToday: number = 0
    public LDM_FT_Resulting_AC: number = 0

    //Automative
    public LDM_UT_LabTesting_Target: number = 0
    public LDM_UT_LabTesting_InBefore: number = 0
    public LDM_UT_LabTesting_InToday: number = 0
    public LDM_UT_LabTesting_OutToday: number = 0
    public LDM_UT_LabTesting_AC: number = 0
    public LDM_UT_Resulting_Target: number = 0
    public LDM_UT_Resulting_InBefore: number = 0
    public LDM_UT_Resulting_InToday: number = 0
    public LDM_UT_Resulting_OutToday: number = 0
    public LDM_UT_Resulting_AC: number = 0

    //History
    public NoOfReport_Out_CurrentMonthPrevYear: number=0
    public NoOfReport_Out_PrevMonthCurrentYear: number=0
    public NoOfStaff_CurrentMonthPrevYear: number=0
    public NoOfStaff_PrevMonthCurrentYear: number=0

    public LDM_Date_Add_Date: Date
    public LDM_Data_Modified_Date: Date
    public LDM_Data_Last_Modified_Date: Date
}

export class Login_Summary_Qty_Data_Response {

    constructor() {
        this.Login_Summary_Qty_Data = new Login_Summary_Qty_Data();
        this.TestingLabs = new Array<TestingLabDivisionInfo>();
    }
    public Login_Summary_Qty_Data: Login_Summary_Qty_Data
    public TestingLabs: Array<TestingLabDivisionInfo>
    public isTomorrowDateHoliday: boolean = false;
    public isTomorrowDatePlus1Holiday: boolean = false;
    public isTomorrowDatePlus2Holiday: boolean = false;
    public isTodayDateHoliday: boolean = false;
}

export class TestingLabDivisionInfo {
    public DivisionName: string
    public DivisionType: string
    public Sequence: number
    public LabTesting_Target: number
    public LabTesting_InBefore: number
    public LabTesting_InToday: number
    public LabTesting_OutToday: number
    public LabTesting_AC: number
    public Resulting_Target: number
    public Resulting_InBefore: number
    public Resulting_InToday: number
    public Resulting_OutToday: number
    public Resulting_AC: number
}

export class JsonDataArray{
    public Name:string;
   // public AvailableCapacity:number;
    public OutToday:number;  
    public InToday:number;
    public InBefore:number;
   // public Target:number;

    
}

export class PopupDataParam {
    public ModuleName: string;
    public SubModuleName: string;
    public LabLocationId: number;
    public DivisionId: number;
    public SearchDate: Date;
}

export class PopupDataSummary {
    public ReportNo: string;
    public LogInTime: Date;
    public DueDate: Date;
    public LogoutTime: Date;
    public ClientName: string;
    public OverallRating: string;
    public Service_Level: string;
    public ReportStatus: string;
}
