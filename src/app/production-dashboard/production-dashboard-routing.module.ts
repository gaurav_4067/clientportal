import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoContentComponent } from 'app/no-content/no-content.component';


const routes: Routes = [{
  path: '',
  children: [
    { path: '', component: NoContentComponent}, {
      path: 'admin',
      loadChildren: () => new Promise(
        resolve => {
          (require as any)
            .ensure(
            [],
            require => {
              resolve(
                require(
                  './admin/admin.module')
                  .AdminModule);
            })
        })
    }
    , {
      path: 'dashboard',
      loadChildren: () => new Promise(
          resolve => {
              (require as any)
                  .ensure(
                      [],
                      require => {
                          resolve(
                              require('./dashboard/dashboard.module')
                                  .DashboardModule);
                      })
          })
  }
  , {
    path: 'dashboard/:labLocationId',
    loadChildren: () => new Promise(
        resolve => {
            (require as any)
                .ensure(
                    [],
                    require => {
                        resolve(
                            require('./dashboard/dashboard.module')
                                .DashboardModule);
                    })
        })
}
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductionDashboardRoutingModule { }