import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { NoContentModule } from 'app/no-content/no-content.module';

@NgModule({
  imports: [
    CommonModule,
    NoContentModule,
    AdminRoutingModule
  ],
  declarations: []
})
export class AdminModule { }
