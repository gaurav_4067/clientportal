import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageBaseTargetComponent } from 'app/production-dashboard/admin/manage-base-target/manage-base-target.component';


const routes: Routes = [{
  path: '',
  component: ManageBaseTargetComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageBaseTargetRoutingModule { }
