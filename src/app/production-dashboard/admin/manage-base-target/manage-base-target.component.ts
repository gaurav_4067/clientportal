import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService, BaseUrl } from '../../../mts.service';
import { Router } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { NgForm } from '@angular/forms';
import { BaseTargetSearchText, TargetInfo, DivisionInfo } from './manage-base-target.model';
import { IMyDpOptions } from 'mydatepicker';
declare let swal: any;

@Component({
  selector: 'app-manage-base-target',
  templateUrl: './manage-base-target.component.html',
  styleUrls: ['./manage-base-target.component.css']
})
export class ManageBaseTargetComponent implements OnInit {
  @ViewChild('TargetDetailForm') TargetDetailForm: NgForm;
  TargetDetailFormSubmitted = false;
  isLoader: boolean;
  userCredential: any;
  userDetail: any;
  labLocationList: any;
  popupLabLocationList: any;
  baseTargetSearchText: BaseTargetSearchText;
  dateRange: any;
  private pageNumber: number = 1;
  private totalItem: any;
  SaveTargetInfoModel: TargetInfo;
  DivisionList: Array<DivisionInfo>;
  popupDivisionList: Array<DivisionInfo>;
  private popupDivisions: any = [];
  targetInfoList: Array<TargetInfo>;
  editButton: boolean = false;
  searchedDate: any;
  ExistDivisionlist:any=[];
  TodayDate = new Date();

  private divisionText: any = { defaultTitle: '--Select Division--' };

  private myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
  };


  constructor(private router: Router, private dataService: DataService, private http: Http) {
    this.baseTargetSearchText = new BaseTargetSearchText();
    this.SaveTargetInfoModel = new TargetInfo();
    this.labLocationList = [];
    this.popupLabLocationList = [];
    this.DivisionList = new Array<DivisionInfo>();
    this.popupDivisionList = new Array<DivisionInfo>();
    this.targetInfoList = new Array<TargetInfo>();
    this.dateRange = {};
    this.popupDivisions = [];
  }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));

    if (this.userDetail && this.userCredential) {
      if (this.userCredential.UserPermission.ViewBaseTarget) {
        this.isLoader = true;
        this.dataService.get('OrderAnalysis/LabLocationList').subscribe(
          response => {
            this.labLocationList = response;
            this.popupLabLocationList = response;
            this.popupLabLocationList.splice(0, 0, { "LabLocationId": 0, "LabLocationName": "Select Lab Location" });
            this.DivisionList.splice(0, 0, { "DepartmentID": 0, "CompanyLocationID": 0, "DepartmentCode": "", "DepartmentDesc": "Select Division" });
          }
        );
        this.getTargetList();

      } else {
        this.router.navigate(['./landing-page']);
      }
    } else {
      this.router.navigate(['./login']);
    }

  }

  getDivisionList(locationId: number) {
    this.isLoader = true;
    this.dataService.get('ManageBaseTarget/GetDivisionList/' + locationId).subscribe(
      response => {
        this.DivisionList = new Array<DivisionInfo>();
        this.DivisionList = JSON.parse(response).Data;
        this.DivisionList.splice(0, 0, { "DepartmentID": 0, "CompanyLocationID": 0, "DepartmentCode": "", "DepartmentDesc": "Select Division" });
        this.isLoader = false;
      }
    );
  }

  getPopupDivisionList(locationId: number) {
    this.isLoader = true;
    this.TargetDetailFormSubmitted = false;
    this.popupDivisions = [];
    this.SaveTargetInfoModel.DivisionIds = [];
    this.dataService.get('ManageBaseTarget/GetDivisionList/' + locationId).subscribe(
      response => {
        this.popupDivisionList = new Array<DivisionInfo>();
        this.popupDivisionList = JSON.parse(response).Data;
        this.isLoader = false;
        let that = this;
        that.popupDivisionList.forEach(function (element) {
          that.popupDivisions.push({ id: element.DepartmentID, name: element.DepartmentDesc })
        });
      }
    );
  }

  onTargetDateChanged(e) {
    var SelectedDate: any = e.date.month + '/' + e.date.day + '/' + e.date.year;
    this.baseTargetSearchText.TargetDate = new Date(SelectedDate);
    this.baseTargetSearchText.TargetDate = new Date(this.baseTargetSearchText.TargetDate.setDate(this.baseTargetSearchText.TargetDate.getDate() + 1));
  }


  getTargetList() {
    this.isLoader = true;
    this.dataService.post('ManageBaseTarget/GetTargetInfoList', this.baseTargetSearchText).subscribe(
      response => {
        let res = JSON.parse(response)
        if (res.IsSuccess) {
          this.targetInfoList = res.Data;
          this.totalItem = res.TotalRecords;
          this.isLoader = false;
        } else {
          swal('', res.Message);
          this.isLoader = false;
        }
      })
  }

  changePageNumber(page: number) {
    this.baseTargetSearchText.PageNumber = page;
    this.pageNumber = page;
    this.getTargetList();
  }

  resetParameters() {
    this.baseTargetSearchText = new BaseTargetSearchText();
    (<any>$('.btnclearenabled')).click();
    this.getTargetList();
  }

  addViewModal() {
    (<any>$('#BaseTarget')).modal('show');
    this.TargetDetailFormSubmitted = false;
    this.editButton = false;
    this.SaveTargetInfoModel.TargetDate = new Date();
    this.SaveTargetInfoModel.LabLocation_Id = 0;
  }

  editViewModal(i) {
    this.SaveTargetInfoModel = new TargetInfo();
    this.SaveTargetInfoModel = this.targetInfoList[i];
    console.log(this.SaveTargetInfoModel)
    this.getPopupDivisionList(this.SaveTargetInfoModel.LabLocation_Id);
    this.SaveTargetInfoModel.DivisionIds = [];
    this.SaveTargetInfoModel.DivisionIds.push(this.SaveTargetInfoModel.Division_Id)
    this.editButton = true;
    this.TargetDetailFormSubmitted = false;
    (<any>$('#BaseTarget')).modal('show');
  }

  AddandUpdateTargetInfo() {
    this.TargetDetailFormSubmitted = true;
    console.log("savemodel",this.SaveTargetInfoModel)
    if (this.SaveTargetInfoModel.LabLocation_Id && (this.SaveTargetInfoModel.DivisionIds.length > 0 || !this.SaveTargetInfoModel.DivisionIds)) {
      this.isLoader = true;
      let currentDate = new Date(this.SaveTargetInfoModel.TargetDate).getMonth() + 1 + '/' + new Date(this.SaveTargetInfoModel.TargetDate).getDate() + '/' + new Date(this.SaveTargetInfoModel.TargetDate).getFullYear();
      let result: boolean = true;
      if (result) {
        let that = this;
        that.isLoader = true;
        let ApiURL: string;

        if (that.editButton) {
          ApiURL = 'ManageBaseTarget/EditTargetInfo'
        } else {
          ApiURL = 'ManageBaseTarget/AddandUpdateTargetInfo'
          let labIndex = that.popupLabLocationList.findIndex(i => i.LabLocationId == that.SaveTargetInfoModel.LabLocation_Id)
          if (labIndex > -1)
            that.SaveTargetInfoModel.LabLocation_Name = that.popupLabLocationList[labIndex].LabLocationName;
        }
        that.dataService.post(ApiURL, that.SaveTargetInfoModel).subscribe(
          response => {
            that.isLoader = false;
            let res = JSON.parse(response)
            if (res.IsSuccess) {            
              if (that.editButton) {
                swal('', res.Message);
                (<any>$('#BaseTarget')).modal('hide');
                that.resetPoupModel();
              }else{
              this.ExistDivisionlist = res.Data;
               (<any>$('#DivisionAlertModal')).modal('show');
                that.isLoader = false;
              }
            } else {
              swal('', res.Message);
              that.isLoader = false;
            }

          }
        );
      }
    } else {
      swal('', 'Please fill lab location or division');
    }

  }

  resetPoupModel() {
    this.getTargetList();
    this.TargetDetailFormSubmitted = false;
    this.TargetDetailForm.resetForm(this.SaveTargetInfoModel);
    this.SaveTargetInfoModel = new TargetInfo();
    this.isLoader = false;
    this.popupDivisions = [];
  }

  CopyPreviousTarget() {
    this.isLoader = true;
    this.dataService.get('ManageBaseTarget/CopyPreviousTarget').subscribe(
      response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          swal('', res.Message);
          this.getTargetList();
          this.isLoader = false;
        } else {
          swal('', res.Message);
          this.isLoader = false;
        }
      })
  }
  
}

