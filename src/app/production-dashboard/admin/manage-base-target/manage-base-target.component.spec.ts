import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageBaseTargetComponent } from './manage-base-target.component';

describe('ManageBaseTargetComponent', () => {
  let component: ManageBaseTargetComponent;
  let fixture: ComponentFixture<ManageBaseTargetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageBaseTargetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBaseTargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
