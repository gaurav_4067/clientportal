
export class BaseTargetSearchText {
  public labLaoctionId: number = 0;
  public DivisionId: number = 0;
  public TargetDate: Date;
  public PageSize: number = 10;
  public PageNumber: number = 1;
}

export class TargetInfo {
  public Target_Id: number;
  public LabLocation_Id: number;
  public LabLocation_Name: string;
  public Division_Id: number;
  public Division_Name: string;
  public TargetDate: Date;
  public SampleLogin_Target: number;
  public TestAssignment_Target: number;
  public Breakdown_Target: number;
  public Scrapping_Target: number;
  public DCD_Target: number;
  public Reporting_Target: number;
  public Logout_Target: number;
  public No_Of_Staff: number;
  public LabTesting_Target: number;
  public LabResulting_Target: number;
  public CreatedDate: Date;
  public ModifiedDate: Date;
  public Active: number;
  public DivisionIds: number[];
  public CurrentDate :Date;
  public SL_LabTesting_Target :number;       
  public SL_LabResulting_Target:number;  
  public HL_LabTesting_Target:number;  
  public HL_LabResulting_Target:number;  
  public TY_LabTesting_Target:number;  
  public TY_LabResulting_Target:number;  
  public FOOD_LabTesting_Target:number;  
  public FOOD_LabResulting_Target:number;  
  public AN_LabTesting_Target:number;  
  public AN_LabResulting_Target:number;  
  public AT_LabTesting_Target:number;  
  public AT_LabResulting_Target:number;  

}


export class DivisionInfo {
  public DepartmentID: number;
  public CompanyLocationID: number;
  public DepartmentCode: string;
  public DepartmentDesc: string;
}


