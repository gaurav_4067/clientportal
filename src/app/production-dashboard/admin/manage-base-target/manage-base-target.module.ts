import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageBaseTargetRoutingModule } from './manage-base-target-routing.module';
import { ManageBaseTargetComponent } from './manage-base-target.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { FormsModule } from '@angular/forms';
import { Ng2PaginationModule } from 'ng2-pagination';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import { MyDatePickerModule } from 'mydatepicker';

@NgModule({
  imports: [
    CommonModule,
    ManageBaseTargetRoutingModule,
    HeaderModule,
    FooterModule,
    FormsModule,
    Ng2PaginationModule,
    MultiselectDropdownModule,
    MyDatePickerModule,

  ],
  declarations: [ManageBaseTargetComponent]
})
export class ManageBaseTargetModule { }
