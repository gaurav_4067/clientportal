import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminModule } from './admin.module';
import { NoContentComponent } from 'app/no-content/no-content.component';

const routes: Routes = [{
  path: '',
  children: [
    {path: '',
      component: NoContentComponent}, {
      path: 'manage-base-target',
      loadChildren: () => new Promise(
        resolve => {
          (require as any)
            .ensure(
            [],
            require => {
              resolve(
                require(
                  './manage-base-target/manage-base-target.module')
                  .ManageBaseTargetModule);
            })
        })
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
