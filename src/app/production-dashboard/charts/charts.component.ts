import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { JsonDataArray } from './charts.model';
import { TestingLabDivisionInfo } from 'app/production-dashboard/dashboard/dashboard-model';
declare let AmCharts: any;
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css'],
})
export class ChartsComponent implements AfterViewInit {

  @Input() testingLab: any;
  graphFileds: any[];

  ngAfterViewInit() {
    let chartName = "";
    if (this.testingLab.DivisionType == "SL") {
      chartName = "SLChart";
    }
    else if (this.testingLab.DivisionType == "HL") {
      chartName = "HLChart";
    }
    else if (this.testingLab.DivisionType == "FT") {
      chartName = "FTChart";
    }
    else if (this.testingLab.DivisionType == "UT") {
      chartName = "UTChart";
    }
    else if (this.testingLab.DivisionType == "AN") {
      chartName = "ANChart";
    }
    else if (this.testingLab.DivisionType == "FOOD") {
      chartName = "FOODChart";
    }
    this.DrawTestingLabChart(chartName);
  }

  BuildGraphArray(title, showTilet, color) {
    var obj = {
      "balloonText": "<b>[[category]] </b><br><span style='font-size:14px'>[[title]]: <b>[[value]]</b></span>",
      "fillAlphas": 1,
      "labelText": "[[value]]",
      "labelPosition": "middle",
      "lineAlpha": 0.5,
      "title": showTilet,
      "type": "column",
      "color": "#000000",
      "lineColor":"#000000",
      "valueField": title,
      "fillColors": color,
      "bulletBorderAlpha":1
    }
    return obj;
  }

  DrawTestingLabChart(chartName): any {
    let that = this;
    var DataArray = [];
    DataArray = [{
      "Name": "LabTesting",
      "OutToday": that.testingLab.LabTesting_OutToday,
      "InToday": that.testingLab.LabTesting_InToday,
      "InBefore": that.testingLab.LabTesting_InBefore,
    }, {
      "Name": "Resulting",
      "OutToday": that.testingLab.Resulting_OutToday,
      "InToday": that.testingLab.Resulting_InToday,
      "InBefore": that.testingLab.Resulting_InBefore,
    }];
    that.graphFileds = [];
    that.graphFileds.push(this.BuildGraphArray('InBefore', 'InBefore', "#ffc000"));
    that.graphFileds.push(this.BuildGraphArray('InToday', 'InToday', "#ed7d31"));
    that.graphFileds.push(this.BuildGraphArray('OutToday', 'OutToday', "#ddebf7"));

    AmCharts.makeChart(chartName, {
      "type": "serial",
      "theme": "light",
      "dataProvider": DataArray,
      "valueAxes": [{
        "stackType": "regular",
        "labelsEnabled": false,
        "axisAlpha": 0,
        "gridAlpha": 0
      }],
      "graphs": this.graphFileds,
      "categoryField": "Name",
      "categoryAxis": {
        "gridPosition": "start",
        "labelsEnabled": false,
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left",
        "isResizing":true

      },
      "export": {
        "enabled": false
      }
    });
  }
}

