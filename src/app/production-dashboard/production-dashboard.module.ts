import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductionDashboardRoutingModule } from './production-dashboard-routing.module';
import { NoContentModule } from 'app/no-content/no-content.module';

@NgModule({
  imports: [
    CommonModule,
    NoContentModule,
    ProductionDashboardRoutingModule
  ],
  declarations: []
})
export class ProductionDashboardModule { }
