import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/mts.service';

@Component({
  selector: 'custom-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
  }

  openTermsOfUse() {
    if (this.dataService.ready) {
      this.dataService.modalOpened = "termsOfUse";
    } else {
      (<any>$('#termsOfUse')).modal('show');
    }
  }

  openPrivacyPolicy() {
    if (this.dataService.ready) {
      this.dataService.modalOpened = "privacyPolicy";
    } else {
      (<any>$('#privacyPolicy')).modal('show');
    }
  }
}
