import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CpEmailTemaplateRoutingModule } from './cp-email-temaplate-routing.module';
import { CpEmailTemaplateComponent } from './cp-email-temaplate.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    CpEmailTemaplateRoutingModule,
    FormsModule
  ],
  declarations: [CpEmailTemaplateComponent],
  exports:[CpEmailTemaplateComponent]
})
export class CpEmailTemaplateModule { }
