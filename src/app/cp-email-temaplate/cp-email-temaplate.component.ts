import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EmailObject } from './../my-order/order-tracking/order-tracking-model'

@Component({
    selector: 'app-cp-email-temaplate',
    templateUrl: './cp-email-temaplate.component.html',
    styleUrls: ['./cp-email-temaplate.component.css']
})
export class CpEmailTemaplateComponent implements OnInit {

    userDetail: any;
    EmailObject: EmailObject;

    @Output() getCCEmails: EventEmitter<any> = new EventEmitter();

    constructor() {
        this.EmailObject = new EmailObject();
    }
    ngOnInit() {
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        this.EmailObject.EmailTo = this.userDetail.EmailId;
        this.addTo();
    }


    addTo() {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (this.EmailObject.EmailTo) {
            if (reg.test(this.EmailObject.EmailTo) == true) {
                if (this.EmailObject.ClientEmailTo) {
                    this.EmailObject.ClientEmailTo = this.EmailObject.ClientEmailTo + "," + this.EmailObject.EmailTo;
                }
                else {
                    this.EmailObject.ClientEmailTo = this.EmailObject.EmailTo;
                }
                this.EmailObject.EmailTo = "";
            }
        }
        this.getCCEmails.emit(this.EmailObject);
    }

    getToEmail(e) {
        var val = e.target.value;
        if (val.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            this.EmailObject.errorEmailTo = false;
        }
        else if (val == "") {
            this.EmailObject.errorEmailTo = false;
        }
        else {
            this.EmailObject.errorEmailTo = true;
        }
        this.getCCEmails.emit(this.EmailObject);
    }

    getCCEmail(e) {
        var val = e.target.value;
        if (val.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            this.EmailObject.errorEmailCC = false;
        }
        else if (val == "") {
            this.EmailObject.errorEmailCC = false;
        }
        else {
            this.EmailObject.errorEmailCC = true;
        }
        this.getCCEmails.emit(this.EmailObject);
    }

    handleToinputfeild(evt) {
        var keycode = evt.charCode || evt.keyCode;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.EmailObject.EmailTo = this.EmailObject.EmailTo.trim();
        if (keycode == 188) {
            this.EmailObject.EmailTo = this.EmailObject.EmailTo.substring(0, this.EmailObject.EmailTo.length - 1);
        }

        if (this.EmailObject.EmailTo) {
            if (reg.test(this.EmailObject.EmailTo) == true && (keycode == 32 || keycode == 13 || keycode == 188 || keycode == 9)) {
                if (this.EmailObject.ClientEmailTo) {
                    this.EmailObject.ClientEmailTo = this.EmailObject.ClientEmailTo + "," + this.EmailObject.EmailTo;
                }
                else {
                    this.EmailObject.ClientEmailTo = this.EmailObject.EmailTo;
                }
                this.EmailObject.EmailTo = "";
            }
        }
        this.getCCEmails.emit(this.EmailObject);
    }

    removedEmailFromTo(emailId) {
        let splitToEmailArr = this.EmailObject.ClientEmailTo.split(",");
        let index = splitToEmailArr.findIndex(i => i == emailId);
        splitToEmailArr.splice(index, 1);
        this.EmailObject.ClientEmailTo = splitToEmailArr.join(",");
        if (!this.EmailObject.ClientEmailTo) {
            this.EmailObject.ClientEmailTo = null;
        }
        this.getCCEmails.emit(this.EmailObject);
    }

    handleCCinputfeild(evt) {
        var keycode = evt.charCode || evt.keyCode;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.EmailObject.ccField = this.EmailObject.ccField.trim();
        if (keycode == 188) {
            this.EmailObject.ccField = this.EmailObject.ccField.substring(0, this.EmailObject.ccField.length - 1);
        }

        if (this.EmailObject.ccField) {
            if (reg.test(this.EmailObject.ccField) == true && (keycode == 32 || keycode == 13 || keycode == 188 || keycode == 9)) {
                if (this.EmailObject.ClientEmailCC) {
                    this.EmailObject.ClientEmailCC = this.EmailObject.ClientEmailCC + "," + this.EmailObject.ccField;
                }
                else {
                    this.EmailObject.ClientEmailCC = this.EmailObject.ccField;
                }
                this.EmailObject.ccField = "";
            }
        }
        this.getCCEmails.emit(this.EmailObject);
    }

    removedEmailFromCC(emailId) {
        let splitToEmailArr = this.EmailObject.ClientEmailCC.split(",");
        let index = splitToEmailArr.findIndex(i => i == emailId);
        splitToEmailArr.splice(index, 1);
        this.EmailObject.ClientEmailCC = splitToEmailArr.join(",");
        if (!this.EmailObject.ClientEmailCC) {
            this.EmailObject.ClientEmailCC = null;
        }
        this.getCCEmails.emit(this.EmailObject);
    }

    addRemark(e) {
        var temp = e.target.value;
        this.EmailObject.clientremarks.push(temp);
        this.getCCEmails.emit(this.EmailObject);
    }
}
