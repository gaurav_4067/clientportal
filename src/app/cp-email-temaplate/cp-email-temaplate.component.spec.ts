import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpEmailTemaplateComponent } from './cp-email-temaplate.component';

describe('CpEmailTemaplateComponent', () => {
  let component: CpEmailTemaplateComponent;
  let fixture: ComponentFixture<CpEmailTemaplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpEmailTemaplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpEmailTemaplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
