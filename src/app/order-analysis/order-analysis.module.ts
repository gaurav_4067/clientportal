import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { Ng2PaginationModule } from 'ng2-pagination';
import { MyDatePickerModule } from 'mydatepicker';
import { MdInputModule } from '@angular2-material/input';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

import { OrderAnalysisRoutingModule } from './order-analysis-routing.module';
import { OrderAnalysisComponent } from 'app/order-analysis/order-analysis.component';
import { InspectionOrderAnalysisModule } from 'app/order-analysis/inspection-order-analysis/inspection-order-analysis.module';
import { DynamicGraphModule } from 'app/order-analysis/dynamic-graph/dynamic-graph.module';
import { FooterModule } from 'app/footer/footer.module';
import { HeaderModule } from 'app/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MyDateRangePickerModule,
    MyDatePickerModule,
    MultiselectDropdownModule,
    OrderAnalysisRoutingModule,
    InspectionOrderAnalysisModule,
    DynamicGraphModule,
    Ng2PaginationModule,
    FooterModule,
    HeaderModule,
    MdInputModule
  ],
  declarations: [OrderAnalysisComponent]
})
export class OrderAnalysisModule { }
