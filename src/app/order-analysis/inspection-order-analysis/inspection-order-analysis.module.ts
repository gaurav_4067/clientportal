import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { Ng2PaginationModule } from 'ng2-pagination';
import { MyDatePickerModule } from 'mydatepicker';

import { InspectionOrderAnalysisComponent } from 'app/order-analysis/inspection-order-analysis/inspection-order-analysis.component';
import { FooterModule } from 'app/footer/footer.module';
import { HeaderModule } from 'app/header/header.module';
import { FormatBusinessCategoryModule } from 'app/helper/format-business-category/format-business-category.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MyDateRangePickerModule,
    Ng2PaginationModule,
    MyDatePickerModule,
    FooterModule,
    HeaderModule,
    FormatBusinessCategoryModule
  ],
  declarations: [InspectionOrderAnalysisComponent],
  exports:[InspectionOrderAnalysisComponent]
})
export class InspectionOrderAnalysisModule { }
