import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Http, Response } from '@angular/http';
import { DataService, BaseUrl } from '../../mts.service';
import { ETRFUrl } from '../../mts.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router'
import { InjectService } from '../../../injectable-service/injectable-service';
declare let swal: any;
declare let AmCharts: any;
@Component({
    selector: 'app-inspection-order-analysis',
    templateUrl: './inspection-order-analysis.component.html'
})

export class InspectionOrderAnalysisComponent implements OnInit {
    userCredential: any;
    count: number;
    private p: any;
    private totalItem: number;
    private itemPerPage: any = 10;
    result: any;
    analysisMode: number = 1;
    analysisType: number = 1;
    failureOrderAnalysis: any;
    passFailAnalysis: any;
    vendorInfos: any;
    orderAnalysisParams: any;
    additionalSearch: any;
    isLoader: boolean;
    ExcelFileName: any;
    DynamicGraph: boolean = true;
    quickPerformanceGraph: any;
    columnList: any = [];
    orderList: any;
    date: any;
    myForm: any;
    dateRange: any;
    searchParam: any;
    dynamicSearchColumns: any;
    isDateSelected: boolean = true;
    vendorAnalysisCount: number = 15;
    isClient: boolean;
    vendorScore: any;
    vendorScoreTab: boolean;
    tatChart: any;
    quickPerformanceChart: any;
    dateRangeModel: any;
    GraphList: any;
    GraphType: any = 0;
    ChartType: any = 0;
    searchModal: any;
    dynamicGridColumns: any;
    dateRangeStart: any;
    SelectedGraphName: any;
    userDetail: any;

    constructor(private dataService: DataService, private router: Router, private formBuilder: FormBuilder) {
        this.date = new Date();
        this.dateRangeStart = new Date();
        this.dateRangeStart.setMonth(this.dateRangeStart.getMonth() - 1);
        this.quickPerformanceGraph = [];
        this.result = {};
        this.failureOrderAnalysis = {};
        this.passFailAnalysis = {};
        this.vendorInfos = [];
        this.orderAnalysisParams = {};
    }

    ngOnInit() {
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
        if (this.userDetail && this.userCredential) {
            if (this.userCredential.UserPermission.ViewOrderAnalysis) {
                this.isClient = this.userDetail.IsClient;
                this.orderAnalysisParams.ClientId = this.userCredential.BusinessCompanyId == 0 ? this.userCredential.UserCompanyId : this.userCredential.BusinessCompanyId;
                this.orderAnalysisParams.UserCompanyId = this.userCredential.UserCompanyId;
                this.orderAnalysisParams.OrderBy = 'ReportNo';
                this.orderAnalysisParams.Order = 'asc';
                this.orderAnalysisParams.UserType = this.userCredential.UserType;
                this.orderAnalysisParams.BusinessCategoryId = this.userCredential.BusinessCategoryId
                this.orderAnalysisParams.EndDate = (new Date().getMonth() + 1) + "/" + new Date().getDate() + "/" + (new Date().getFullYear());
                this.orderAnalysisParams.PageSize = null;
                this.orderAnalysisParams.UserClientId = this.userCredential.CompanyId;
                this.orderAnalysisParams.FormId = 1;

                this.orderAnalysisParams.StartDate = (this.dateRangeStart.getMonth()) + "/" + this.dateRangeStart.getDate() + "/" + (this.dateRangeStart.getFullYear());

                this.searchParam = {};
                this.searchParam.SearchText = [{ ColumnName: "ReportNo", Operator: 5, ColumnValue: "" },
                { ColumnName: "C1", Operator: 5, ColumnValue: "" }];

                this.orderAnalysisParams.SearchText = [{ ColumnName: "ReportNo", Operator: 5, ColumnValue: "" },
                { ColumnName: "C1", Operator: 5, ColumnValue: "" }];
                this.additionalSearch = [{ ColumnName: "", Operator: "", ColumnValue: "" }];
                this.isLoader = true;

                this.dataService.get('Order/GetMyOrderColumnsListAnalysisPopup?selectedCompany=' + this.userCredential.CompanyId + '&FormId=1&BusinessCategoryId=' + this.userCredential.BusinessCategoryId).subscribe(
                    response => {
                        if (response.IsSuccess) {
                            this.dynamicGridColumns = response.SelectedGridColumn;
                            this.columnList = response.SelectedAdHocSearchColumn;
                        }
                        else {
                            swal('', response.Message)
                        }
                    }
                );



                if (this.userCredential.UserType == 'Client' || this.orderAnalysisParams.UserType == 'MTS Internal User') {
                    this.orderAnalysisParams.VendorId = 0;
                    this.dataService.post('OrderAnalysis/GetVendorList', { "ClientId": this.userCredential.CompanyId, "BusinessCategoryId": this.userCredential.BusinessCategoryId }).subscribe(
                        response => {
                            this.vendorInfos = response;
                            this.vendorInfos.splice(0, 0, { "VendorId": 0, "VendorCompanyName": "All Vendor" });
                        }
                    );
                }

                this.analysisType = this.analysisMode;
                let thisObj = this;
                thisObj.dataService.post('OrderAnalysis/PassFailOrderAnalyisforInspection', thisObj.orderAnalysisParams).subscribe(
                    response => {

                        thisObj.quickPerformanceGraph = [];
                        thisObj.isLoader = false;
                        thisObj.passFailAnalysis = response;

                        thisObj.passFailAnalysis.OrderAnalysis.forEach(function (obj) {
                            if (obj.Type == 'FAIL') {
                                thisObj.failureOrderAnalysis.failOrder = obj.Count;
                                thisObj.quickPerformanceGraph.push(obj);
                            }
                            else if (obj.Type == 'PASS') {
                                thisObj.failureOrderAnalysis.passOrder = obj.Count;
                                thisObj.quickPerformanceGraph.push(obj);
                            }
                            else if (obj.Type == 'PENDING') {
                                thisObj.failureOrderAnalysis.otherOrder = obj.Count;
                                thisObj.quickPerformanceGraph.push(obj);
                            }
                            else if (obj.Type == 'Total') {
                                thisObj.failureOrderAnalysis.totalOrder = obj.Count;
                            }
                        })
                        thisObj.createPerformanceAnalysisChart();
                    }
                )

                this.myForm = this.formBuilder.group({
                    myDateRange: [{ beginDate: { year: this.dateRangeStart.getFullYear(), month: this.dateRangeStart.getMonth(), day: this.dateRangeStart.getDate() }, endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() } }, Validators.required]
                });
            } else {
                this.router.navigate(['/landing-page']);
            }
        } else {
            this.router.navigate(['/login']);
        }
        // this.clientId = this.userCredential.BusinessCompanyId == 0 ? this.userCredential.UserCompanyId : this.userCredential.BusinessCompanyId;
    }


    addSearch() {
        if (this.additionalSearch.length > 0) {
            if (this.additionalSearch[this.additionalSearch.length - 1].ColumnName && this.additionalSearch[this.additionalSearch.length - 1].Operator) {
                this.additionalSearch.push({ ColumnName: "", Operator: "", ColumnValue: "" });
            }
        }
        else {
            this.additionalSearch.push({ ColumnName: "", Operator: "", ColumnValue: "" });
        }
    }


    removeSearch(index: number) {
        if (this.additionalSearch.length > 1) {
            this.additionalSearch.splice(index, 1);
        }
    }

    onDateRangeChanged(event: any) {
        if (event.beginJsDate && event.endJsDate !== null) {
            this.isDateSelected = true;
            this.orderAnalysisParams.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
            this.orderAnalysisParams.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
        }
        else {
            this.isDateSelected = false;
        }
    }
    search() {

        var thisObj = this;
        this.ChartType = 0;
        this.GraphType = 0;
        if (thisObj.isDateSelected) {
            this.orderAnalysisParams.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
            if (this.orderAnalysisParams.InnerSearchText) {
                delete this.orderAnalysisParams.InnerSearchText;
            }

            if (this.orderAnalysisParams.SearchText.length > 6) {
                let count = this.orderAnalysisParams.SearchText.length - 5;
                this.orderAnalysisParams.SearchText.splice(5, count);
            }
            let thisObj = this;
            this.additionalSearch.forEach(function (addObj) {
                thisObj.orderAnalysisParams.SearchText.push(addObj);
            })
            this.isLoader = true;
            this.orderAnalysisParams.PageSize = null;
            this.analysisType = this.analysisMode;

            thisObj.dataService.post('OrderAnalysis/PassFailOrderAnalyisforInspection', thisObj.orderAnalysisParams).subscribe(
                response => {
                    thisObj.quickPerformanceGraph = [];
                    thisObj.isLoader = false;
                    thisObj.passFailAnalysis = response;
                    thisObj.passFailAnalysis.OrderAnalysis.forEach(function (obj) {
                        if (obj.Type == 'FAIL') {
                            thisObj.failureOrderAnalysis.failOrder = obj.Count;
                            thisObj.quickPerformanceGraph.push(obj);
                        }
                        else if (obj.Type == 'PASS') {
                            thisObj.failureOrderAnalysis.passOrder = obj.Count;
                            thisObj.quickPerformanceGraph.push(obj);
                        }
                        else if (obj.Type == 'PENDING') {
                            thisObj.failureOrderAnalysis.otherOrder = obj.Count;
                            thisObj.quickPerformanceGraph.push(obj);
                        }
                        else if (obj.Type == 'Total') {
                            thisObj.failureOrderAnalysis.totalOrder = obj.Count;
                        }
                    })
                    thisObj.createPerformanceAnalysisChart();
                }
            )
        }
        else {
            swal('', 'Please Select Date Range');
        }

    }

    createPerformanceAnalysisChart() {
        var dataProvider = this.quickPerformanceGraph.map((data) => {
            if (data.Type == "FAIL") {
                data.color = "#ac2925";
            }
            else if (data.Type == "PENDING") {
                data.Type = "Hold";
                data.color = "#85c5e3";
            }
            else if (data.Type == "PASS") {
                data.color = "#84b761";
            }
            return data;
        })
        this.quickPerformanceChart = AmCharts.makeChart("quickPerformanceAnalysis", {
            "type": "pie",
            "theme": "light",
            "legend": {
                "markerSize": 10,
                "position": "top",
                "autoMargins": false,
                "maxColumns": 5
            },
            "dataProvider": dataProvider,
            "valueField": "Count",
            "titleField": "Type",
            "colorField": "color",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": false
            }
        });

    }


    resetSearchForm() {
        (<any>$('.resetSelect')).prop('selectedIndex', 0);
        (<any>$('.resetInput')).val('');
        (<any>$('#DynamicControls')).empty();
        this.analysisMode = 1;
        this.DynamicGraph = true;
        this.searchParam.SearchText[0].ColumnValue = "";
        this.dateRangeModel = {
            beginDate: { year: this.dateRangeStart.getFullYear(), month: this.dateRangeStart.getMonth() - 1, day: this.dateRangeStart.getDate() },
            endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() }
        };
        this.myForm.myDateRange = [{ beginDate: { year: this.date.getFullYear(), month: this.date.getMonth(), day: this.date.getDate() }, endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() } }, Validators.required];
        var startDate = this.dateRangeStart.getFullYear().toString() + '-' + (this.dateRangeStart.getMonth()).toString() + '-' + this.dateRangeStart.getDate().toString();
        var endDate = this.date.getFullYear().toString() + '-' + (this.date.getMonth() + 1).toString() + '-' + this.date.getDate().toString();
        this.orderAnalysisParams.StartDate = startDate;
        this.orderAnalysisParams.EndDate = endDate;
        if (this.orderAnalysisParams.StartDate && this.orderAnalysisParams.EndDate) {
            this.isDateSelected = true;
        }
        for (let i in this.additionalSearch[0]) {
            this.additionalSearch[0][i] = "";
        }
        for (var i = this.additionalSearch.length; i > 0; i--) {
            this.additionalSearch.splice(i, 1);
        }
        this.search();
    }

    analysisDetails(type, value, pagesize) {
        var thisObj = this;
        this.p = 1;
        thisObj.orderAnalysisParams.PageNumber = 1;
        thisObj.orderAnalysisParams.PageSize = 10;
        thisObj.orderAnalysisParams.SearchText[1] = { ColumnName: "", Operator: 1, ColumnValue: "" };
        if (type == 'Failure') {
            var index = thisObj.orderAnalysisParams.SearchText.length
            thisObj.orderAnalysisParams.SearchText[1].ColumnName = 'Result';
            if (value == 'Conformed' || value == 'Non-Conformed' || value == 'Pending') {
                thisObj.orderAnalysisParams.SearchText[1].ColumnValue = value;
                thisObj.orderAnalysisParams.SearchText[1].ColumnName = 'Result';
            }
        }
        thisObj.isLoader = true;
        thisObj.dataService.post('OrderAnalysis/GetinspectionOrderAnalysisList', this.orderAnalysisParams).subscribe(
            response => {
                thisObj.isLoader = false;
                (<any>$('#analysisDetails')).modal('show');
                if (response.IsSuccess) {
                    thisObj.orderList = response.Data;
                    thisObj.totalItem = Math.ceil(pagesize);
                }
                else {
                    swal('', response.Message);
                }

            }
        )

    }



    getPage(Page: Number) {
        this.orderAnalysisParams.PageNumber = Page;
        this.isLoader = true;
        this.dataService.post('OrderAnalysis/GetinspectionOrderAnalysisList', this.orderAnalysisParams).subscribe(
            response => {
                this.isLoader = false;
                (<any>$('#analysisDetails')).modal('show');
                if (response.IsSuccess) {
                    this.orderList = response.Data;
                    this.p = Page;
                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }

    reset() {
        this.orderList = {};
    }


    getImageold(TableID: any, ImgID: any, chartName: any) {
        for (let i = 0; i < ImgID.length; i++) {
            this.ExcelFileName = TableID;
            var that = this;
            var tmp = new AmCharts.AmExport(this[chartName[i]]);
            tmp.init();
            tmp.output({
                output: 'datastring',
                format: 'jpg'
            }, function (blob) {

                var me = blob.split('base64,');
                var data =
                {
                    'Base': ''
                };

                data['Base'] = me[1];
                if (me.length > 1) {
                    that.dataService.post('OrderAnalysis/Base64Toimage', data).subscribe(
                        response => {
                            if (response.IsSuccess) {
                                var img = <any>document.getElementById(ImgID[i]);
                                img.src = ETRFUrl + response.ImageUrl;
                                var urlid = document.getElementById(TableID);

                                if (i == (ImgID.length - 1)) {
                                    that.getTableToExcel(urlid, 'aa');
                                }
                                else {
                                }
                            }
                            else {
                                swal('', response.Message);
                            }
                        }
                    )
                }
                else {
                    swal('', 'No data to Export Excel!!');
                }
            });
        }
    }

    getImage(TableID: any, ImgID: any, chartName: any) {
        this.isLoader = true;
        this.orderAnalysisParams.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
        if (this.orderAnalysisParams.InnerSearchText) {
            delete this.orderAnalysisParams.InnerSearchText;
        }
        if (this.orderAnalysisParams.SearchText.length > 6) {
            let count = this.orderAnalysisParams.SearchText.length - 5;
            this.orderAnalysisParams.SearchText.splice(5, count);
        }
        var that = this;
        this.additionalSearch.forEach(function (addObj) {
            that.orderAnalysisParams.SearchText.push({ ColumnName: addObj.ColumnName, Operator: addObj.Operator, ColumnValue: addObj.ColumnValue });
        });
        this.orderAnalysisParams.PageSize = null;

        for (let i = 0; i < ImgID.length; i++) {
            this.ExcelFileName = TableID;
            var that = this;
            var tmp = new AmCharts.AmExport(this[chartName[i]]);
            tmp.init();
            tmp.output({
                output: 'datastring',
                format: 'jpg'
            }, function (blob) {

                var me = blob.split('base64,');
                var data =
                    {
                        'Base': ''
                    };
                data['Base'] = me[1];
                if (me.length > 1) {
                    if (i == 0) {
                        that.orderAnalysisParams.FirstBase64ToImage = data['Base'];
                    }
                }
            });
        }       
        that.orderAnalysisParams.CompanyName = that.userCredential.CompanyName;  
        that.dataService.post('GraphAnalysisExportToExcel/Insp_WriteQuickPerformanceAnalysisExportToExcelFile', that.orderAnalysisParams).subscribe(
            response => {
                if (response.IsSuccess) {
                    that.isLoader = false;
                    let downloadFileName = encodeURIComponent(response.Message);
                    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + that.userCredential.BusinessCategoryId;
                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }

    getTableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table >{table}</table></body></html>'
            , base64 = function (s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) }
            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: (table.id) || 'Worksheet', table: table.innerHTML }
            var dlink = <any>document.getElementById("dlink");
            dlink.href = uri + base64(format(template, ctx));
            dlink.download = (table.id) + ".xls";
            dlink.click();
        }
    })()

}
