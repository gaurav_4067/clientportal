
/// <reference path="../dashboard/amchart.d.ts" />
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Http, Response } from '@angular/http';
import { InjectService } from '../injectable-service/injectable-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IMultiSelectOption, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { Router } from '@angular/router';
//import { IMyOptions } from '../../src/my-date-range-picker/interfaces';
import { DataService, BaseUrl, COLUMN_LIST } from '../mts.service';
import { ETRFUrl } from '../mts.service';

import 'table2excel';
import { AdditionalSearch } from 'app/order-analysis/order-analysis-model';
declare let AmCharts: any;

declare let swal: any;
@Component({
    selector: 'order-analysis',
    providers: [DataService],
    templateUrl: './order-analysis.component.html'
})

export class OrderAnalysisComponent implements OnInit {
    hideExportToExcel: boolean;
    radio: any;
    userCredential: any;
    count: number;
    result: any;
    analysisMode: number;
    analysisType: number;
    failureOrderAnalysis: any;
    failureAnalysis: any;
    onHoldAnalysis: any;
    onHoldDuration: any;
    passFailAnalysis: any;
    vendorInfos: any;
    labLocationInfos: any;
    orderAnalysisParams: any;
    additionalSearch: Array<AdditionalSearch>;
    isLoader: boolean;
    ExcelFileName: any;
    DynamicGraph: boolean = true;
    GetAssignCompany: any;
    ExportType: any;
    tatAnalysisGraph: any;
    onHoldGraphOne: any;
    onHoldGraphTwo: any;
    failureGraphOne: any;
    failureGraphTwo: any;
    quickPerformanceGraph: any;
    columnList: any = [];
    orderList: any;
    date: any;
    myForm: any;
    dateRange: any;
    searchParam: any;
    dynamicSearchColumns: any;
    isDateSelected: boolean = true;
    vendorAnalysisCount: number = 15;
    isClient: boolean;
    vendorScore: any;
    vendorTableShow: any = false;
    vendorTestTableShow: any = false;
    vendorScoreTab: boolean;
    scoreByTest: any;
    scoreByTestResult: any;
    failureCodeAnalysis: any;
    failureCodeAnalysisTotal: any;
    failureCodeAnalysisShow: boolean;
    failureCodeAnalysisChart: boolean;
    scoreByTestByShow: boolean;
    tatChart: any;
    quickPerformanceChart: any;
    failureCodeChart: any;
    onHoldDurationChart: any;
    onHoldsubmissionChart: any;
    passAndFailChart: any;
    failureAnalysisChart: any;
    p: any;
    totalItem: any;
    itemPerPage: any;
    dateRangeModel: any;
    GraphList: any;
    companiesList: IMultiSelectOption[];
    companiesListValue: any = [];
    customQueryResponseGraphJson: any;
    customQueryResponseGraphColumn: any;
    objectLength: any = [];
    GraphColumnLength: any = 0;
    objectLengthArr: any = [];

    GraphType: any = 0;
    ChartType: any = 0;

    searchModal: any;
    dynamicGridColumns: any;
    dateRangeStart: any;

    SelectedGraphName: any;
    DynamicSearchOnChange: any = {
        "ChartType": 0,
        "GraphType": 0,
        "GraphJson": "",
        "GraphName": ""
    };
    customQueryModel: any = {
        "ClientId": 0,
        "UserType": "MTS Internal User",
        "UserCompanyId": 0,
        "GraphId": 0,
        "SearchText": [],
    };
    searchObj: any = {
        "ColumnName": "",
        "Operator": 5,
        "ColumnValue": ""
    };

    isMultiGraph: any = false;

    dynamicGraph: any;
    dynamicTable: any;
    dynamicSummary: any;
    showDynamicGraph: any = false;
    dynamicGraphWrapperClass: any = "";
    dynamicTableColSpan: any = 1;
    graphAdditionalSummaryColumn: any = [];
    additionalGraphJson: any = [];
    dynamicSearchResult: any;
    dynamicGraph1: any;
    dynamicTable1: any;
    dynamicGraphWrapperClass1: any;
    additionalGraphClass: any = "";
    additionalGraphClass1: any = "";
    GetOrderList: any = true;
    userDetail: any;
    scoreByTestConsolidate: boolean;
    scoreByVendorConsolidate: boolean;
    scoreByVendorConsolidateArray: Array<any>;
    scoreByTestConsolidateArray: Array<any>;
    vendorConsolidateType: any;
    vendorAnalysisConsolidateCount: number;
    scoreByTestResultConsolidate: Array<any>;
    hideExportToExcelConsolidate: boolean;

    customSearchParam: any;

    failureAnalysisCategory: string;


    constructor(private dataService: DataService, private router: Router, private formBuilder: FormBuilder) {
        this.date = new Date();
        this.dateRangeStart = new Date();
        this.dateRangeStart.setMonth(this.dateRangeStart.getMonth() - 1);
        this.tatAnalysisGraph = [];
        this.onHoldGraphOne = [];
        this.onHoldGraphTwo = [];
        this.failureGraphOne = [];
        this.failureGraphTwo = [];
        this.quickPerformanceGraph = [];
        this.result = {};
        this.failureOrderAnalysis = {};
        this.failureAnalysis = {};
        this.onHoldAnalysis = {};
        this.onHoldDuration = {};
        this.passFailAnalysis = {};
        this.analysisMode = -1;
        this.vendorInfos = [];
        this.labLocationInfos = [];
        this.orderAnalysisParams = {};
        this.additionalSearch = new Array<AdditionalSearch>();
    }

    ngOnInit() {
        this.p = 1;
        this.itemPerPage = 1;
        this.totalItem = 0;

        this.searchParam = {};
        this.searchParam.SearchText = [{ ColumnName: "ReportNo", Operator: 5, ColumnValue: "" },
        { ColumnName: "LabLocationId", Operator: 1, ColumnValue: "0" },
        { ColumnName: "BrandsInformation", Operator: 5, ColumnValue: "" },
        { ColumnName: "C1", Operator: 5, ColumnValue: "" }];

        this.orderAnalysisParams.SearchText = [];

        this.additionalSearch.push(new AdditionalSearch("", "", ""));

        //this.additionalSearch = [{ ColumnName: "", Operator: "", ColumnValue: "" }];

        this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));

        if (this.userCredential && this.userDetail) {
            if (this.userCredential.UserPermission.ViewOrderAnalysis) {
                this.radio = "1";
                this.scoreByTest = [];
                this.vendorScoreTab = false;
                this.failureCodeAnalysisShow = false;
                this.failureCodeAnalysisChart = false;
                this.scoreByTestByShow = false;
                this.failureCodeAnalysis = [];
                this.isClient = this.userDetail.IsClient;

                // if (!this.isClient) 
                // {
                //     COLUMN_LIST.forEach((obj, index) => {
                //         if (obj.name != "Vendor Name" && obj.name != "Agent Name" && obj.name != "Manufacture Name") {
                //             this.columnList.push(obj);
                //         }
                //     })
                // }
                // else 
                // {
                //     this.columnList = COLUMN_LIST;
                // }
                if (this.userCredential.BusinessCategoryId == 1) {
                    this.analysisType = -1;
                    this.orderAnalysisParams.ClientId = this.userCredential.CompanyId;
                    this.orderAnalysisParams.UserCompanyId = this.userCredential.UserCompanyId;
                    this.orderAnalysisParams.OrderBy = 'ModifiedDate';
                    this.orderAnalysisParams.Order = 'desc';
                    this.orderAnalysisParams.UserType = this.userCredential.UserType;
                    this.orderAnalysisParams.EndDate = (new Date().getMonth() + 1) + "/" + new Date().getDate() + "/" + (new Date().getFullYear());
                    this.orderAnalysisParams.PageSize = null;
                    this.orderAnalysisParams.FormId = 1;

                    //following line added by divyanhsu:5 March
                    this.orderAnalysisParams.StartDate = (this.dateRangeStart.getMonth()) + "/" + this.dateRangeStart.getDate() + "/" + (this.dateRangeStart.getFullYear());
                    //commented by divyanhsu:5 March
                    //this.orderAnalysisParams.StartDate = "01/" + (new Date().getMonth() + 1) + "/" + (new Date().getFullYear());

                    /*below for solving bug ot searching and sorting*/
                    // this.searchParam = {};
                    // this.customSearchParam = {};

                    // this.searchParam.SearchText = [{ ColumnName: "ReportNo", Operator: 5, ColumnValue: "" },
                    // { ColumnName: "LabLocationId", Operator: 1, ColumnValue: "" },
                    // { ColumnName: "Brand", Operator: 5, ColumnValue: "" },
                    // { ColumnName: "C1", Operator: 5, ColumnValue: "" }];
                    /*ended*/

                    // this.orderAnalysisParams.SearchText = [{ ColumnName: "ReportNo", Operator: 5, ColumnValue: "" }, 
                    // { ColumnName: "LabLocationId", Operator: 1, ColumnValue: "" }, 
                    // { ColumnName: "Brand", Operator: 5, ColumnValue: "" },
                    // { ColumnName: "C1", Operator: 5, ColumnValue: "" }];


                    // this.additionalSearch = [{ ColumnName: "", Operator: "", ColumnValue: "" }];

                    // this.searchParam.SearchText.forEach(element => {
                    //     this.orderAnalysisParams.SearchText.push(element);
                    // });

                    this.orderAnalysisParams.SearchText = this.searchParam.SearchText;
                    //this.orderAnalysisParams.SearchText

                    this.isLoader = true;


                    // this.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));

                    this.dataService.get('Order/GetMyOrderColumnsListAnalysisPopup?selectedCompany=' + this.userCredential.CompanyId + '&FormId=1&BusinessCategoryId=' + this.userCredential.BusinessCategoryId).subscribe(
                        response => {
                            if (response.IsSuccess) {
                                // this.dynamicSearchColumns = response.SelectedSearchColumn;
                                this.dynamicGridColumns = response.SelectedGridColumn;
                                this.columnList = response.SelectedAdHocSearchColumn;

                            }
                            else {

                                swal('', response.Message)
                            }
                        }
                    );



                    if (this.userCredential.UserType == 'Client' || this.orderAnalysisParams.UserType == 'MTS Internal User') {
                        this.orderAnalysisParams.VendorId = 0;
                        this.dataService.post('OrderAnalysis/GetVendorList', { "ClientId": this.userCredential.CompanyId, "BusinessCategoryId": this.userCredential.BusinessCategoryId }).subscribe(
                            response => {
                                this.vendorInfos = response;
                                this.vendorInfos.splice(0, 0, { "VendorId": 0, "VendorCompanyName": "All Vendor" });
                            }
                        );
                    }

                    this.dataService.get('OrderAnalysis/LabLocationList').subscribe(
                        response => {
                            this.labLocationInfos = response;
                            this.labLocationInfos.splice(0, 0, { "LabLocationId": 0, "LabLocationName": "Select Lab Location" });
                        }
                    );

                    this.dataService.get('OrderAnalysis/GetGraphList?CompanyId=' + this.userCredential.CompanyId + '&UserCompanyId=' + this.userCredential.UserCompanyId).subscribe(
                        response => {
                            this.GraphList = response.Data;
                        });

                    var thisObj = this;



                    this.dataService.post('OrderAnalysis/TATAnalysisStatus', this.orderAnalysisParams).subscribe(
                        response => {
                            this.isLoader = false;
                            // this.searchParam.SearchText = [{ ColumnName: "ReportNo", Operator: 5, ColumnValue: "" },
                            // { ColumnName: "LabLocationId", Operator: 1, ColumnValue: 0 },
                            // { ColumnName: "Brand", Operator: 5, ColumnValue: "" },
                            //  { ColumnName: "C1", Operator: 5, ColumnValue: "" }];
                            //this.searchParam.SearchText.push({ ColumnName: "", Operator: 5, ColumnValue: "" });
                            this.result = response;
                            if (this.result.TATAnalysisList.IsSuccess) {
                                this.result.TATAnalysisList.OrderAnalysis.forEach(element => {
                                    if (element.Type == '1-2 Days') {
                                        thisObj.result.oneTotwo = element.Count;
                                        element.color = "#0067aa";

                                    }
                                    if (element.Type == '3-7 Days') {
                                        thisObj.result.threeToFive = element.Count;
                                        element.color = "#FFA500";
                                    }
                                    if (element.Type == 'More Than 7 Days') {
                                        thisObj.result.moreThanFive = element.Count;
                                        element.color = "#67b7dc";
                                    }
                                    if (element.Type == 'Total') {
                                        thisObj.result.total = element.Count;
                                    }

                                    if (element.Type != 'Total') {
                                        thisObj.tatAnalysisGraph.push(element);
                                    }
                                });

                                this.createTATChart();
                            }

                        }
                    )
                }

                this.myForm = this.formBuilder.group({
                    myDateRange: [{ beginDate: { year: this.dateRangeStart.getFullYear(), month: this.dateRangeStart.getMonth(), day: this.dateRangeStart.getDate() }, endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() } }, Validators.required]
                });
            } else {
                this.router.navigate(['/landing-page']);
            }
        } else {
            this.router.navigate(['/login']);
        }
    }



    resetSearchForm() {
        (<any>$('.resetSelect')).prop('selectedIndex', 0);
        (<any>$('.resetInput')).val('');
        (<any>$('#DynamicControls')).empty();

        this.analysisMode = -1;
        this.DynamicGraph = true;
        this.scoreByTestConsolidate = false;
        this.scoreByVendorConsolidate = false;
        // this.searchParam.SearchText[0].ColumnValue = "";
        // this.searchParam.SearchText[2].ColumnValue = "";
        // this.searchParam.SearchText[1].ColumnValue = "0";
        this.dateRangeModel = {
            beginDate: { year: this.dateRangeStart.getFullYear(), month: this.dateRangeStart.getMonth() - 1, day: this.dateRangeStart.getDate() },
            endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() }
        };
        this.myForm.myDateRange = [{ beginDate: { year: this.date.getFullYear(), month: this.date.getMonth(), day: this.date.getDate() }, endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() } }, Validators.required];
        var startDate = this.dateRangeStart.getFullYear().toString() + '-' + (this.dateRangeStart.getMonth()).toString() + '-' + this.dateRangeStart.getDate().toString();
        var endDate = this.date.getFullYear().toString() + '-' + (this.date.getMonth() + 1).toString() + '-' + this.date.getDate().toString();

        this.orderAnalysisParams.StartDate = startDate;
        this.orderAnalysisParams.EndDate = endDate;
        if (this.orderAnalysisParams.StartDate && this.orderAnalysisParams.EndDate) {
            this.isDateSelected = true;
        }

        // for (let i in this.additionalSearch[0]) {
        //     this.additionalSearch[0][i] = "";
        // }

        // for (var i = this.additionalSearch.length; i > 0; i--) {
        //     this.additionalSearch.splice(i, 1);
        // }
        //  this.searchParam.SearchText

        this.searchParam.SearchText.forEach(element => {
            if (element.ColumnName == "LabLocationId") {
                element.ColumnValue = "0";
            }
            else {
                element.ColumnValue = "";
            }
        });
        this.additionalSearch = new Array<AdditionalSearch>();
        this.additionalSearch.push(new AdditionalSearch("", "", ""));

        this.orderAnalysisParams.VendorId = 0;
        this.search();
    }

    checkChange() {
        this.isLoader = true;
        this.customQueryModel.ClientId = this.companiesListValue;
        // this.dataService.get('GetVendorAndLabLocation?ClientId=' + this.companiesListValue).subscribe(
        //     res => {
        //         this.isLoader = false;
        //         if (res.IsSuccess) {

        //         }
        //     },
        //     err => {
        //         this.isLoader = false;
        //     });
    }

    searchDynamicGraph() {
        this.isLoader = true;
        this.showDynamicGraph = false;
        this.customQueryModel.UserCompanyId = this.userCredential.UserCompanyId;
        this.customQueryModel.UserType = this.userCredential.UserType;
        this.customQueryModel.SearchText = [];
        if (this.dynamicSearchColumns.length) {
            this.dynamicSearchColumns.forEach((data) => {
                if (data.FieldType == "Date Range picker") {

                    if (data.SelectedValue.beginDate) {
                        var date = data.SelectedValue.beginDate.month + "/" + data.SelectedValue.beginDate.day + "/" + data.SelectedValue.beginDate.year + "-" + data.SelectedValue.endDate.month + "/" + data.SelectedValue.endDate.day + "/" + data.SelectedValue.endDate.year;
                        this.customQueryModel.SearchText.push({ "ColumnName": data.ColumnName, "Operator": 6, "ColumnValue": date })
                    }
                    else if (data.SelectedValue != null) {
                        this.customQueryModel.SearchText.push({ "ColumnName": data.ColumnName, "Operator": 6, "ColumnValue": data.SelectedValue.formatted })
                    }
                }
                else {
                    this.customQueryModel.SearchText.push({ "ColumnName": data.ColumnName, "Operator": 1, "ColumnValue": data.SelectedValue })
                }
            })
        }
        this.GraphType = this.DynamicSearchOnChange.GraphType;
        this.ChartType = this.DynamicSearchOnChange.ChartType;

        this.dataService.post('ManageGraph/CustomizeGraphQuery', this.customQueryModel).subscribe(
            response => {

                if (response.IsSuccess == true) {
                    this.isLoader = false;
                    this.dynamicSearchResult = response;
                    this.showDynamicGraph = true;
                }
                else {
                    this.isLoader = false;
                    swal('', response.Message);
                }
            })
        this.analysisType = 0;
    }

    onDateRangeChangedGraph(event: any) {
        if (event.formatted) {
            event.formatted = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year + '-' + event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
        }
    }

    orderAnalysisGraphChange() {
        this.DynamicGraph = true;
    }

    orderAnalysisChange(e) {
        this.isLoader = true;
        this.hideExportToExcel = true;
        var val = e.target.value;
        this.analysisType = val;
        this.isMultiGraph = false;
        if (val != -6) {
            (<any>$(".export-excel")).hide()
        }

        if (val > 0) {
            this.companiesListValue = [];
            this.vendorTableShow = false;
            this.failureCodeAnalysisShow = false;
            this.scoreByTestByShow = false;
            this.customQueryModel.GraphId = val;
            this.DynamicGraph = false;
            if (this.GetAssignCompany == null && this.userCredential.UserCompanyId == 0) {
                this.dataService.get('user/GetAssignedCompanyCategoryListByUser?UserId=' + this.userCredential.UserId).subscribe(
                    response => {
                        this.isLoader = false;
                        this.GetAssignCompany = response.AssignedCompanyList;
                        this.companiesList = this.GetAssignCompany.map((data) => {
                            return ({
                                "id": data.CompanyId,
                                "name": data.CompanyName
                            })
                        })
                    }
                );
            }
            var that = this;
            this.dataService.get('OrderAnalysis/SearchHeader?GraphID=' + this.analysisMode + '&UserType=' + this.userCredential.UserType + '&CompanyId=' + this.userCredential.CompanyId).subscribe(
                response => {

                    this.isLoader = false;
                    this.showDynamicGraph = false;
                    this.SelectedGraphName = response.Data.GraphName;
                    this.DynamicSearchOnChange = response.Data;
                    if (this.DynamicSearchOnChange.IsMultiGraph == true) {
                        this.isMultiGraph = true;
                    }

                    // this.DynamicSearchOnChange.GraphPosition = 4;
                    // Setting Classes for dynamic chart structure
                    // For Graph One

                    if (this.DynamicSearchOnChange.GraphType == 1) {
                        if (this.DynamicSearchOnChange.IsAdditionalSummary) {
                            if (this.DynamicSearchOnChange.GraphAdditionalSummaryPosition == 1) {
                                this.dynamicGraph = "show-left-half margin-right";
                                this.dynamicTable = "show-right-half margin-left";
                                this.dynamicGraphWrapperClass = "";
                                this.additionalGraphClass = "show-none";
                            }
                            else if (this.DynamicSearchOnChange.GraphAdditionalSummaryPosition == 2) {
                                this.dynamicGraph = "show-left-half margin-left";
                                this.dynamicTable = "show-right-half margin-right";
                                this.dynamicGraphWrapperClass = "row-reverse";
                                this.additionalGraphClass = "show-none";
                            }
                            else if (this.DynamicSearchOnChange.GraphAdditionalSummaryPosition == 3) {
                                this.dynamicGraph = "show-top-full";
                                this.dynamicTable = "show-bottom-full";
                                this.dynamicGraphWrapperClass = "col";
                                this.additionalGraphClass = "show-none";
                            }
                            else if (this.DynamicSearchOnChange.GraphAdditionalSummaryPosition == 4) {
                                this.dynamicGraph = "show-top-full";
                                this.dynamicTable = "show-bottom-full";
                                this.dynamicGraphWrapperClass = "col-reverse";
                                this.additionalGraphClass = "show-none";
                            }
                        }
                        else {
                            this.dynamicGraph = "show-full";
                            this.dynamicTable = "show-none";
                            this.dynamicGraphWrapperClass = "";
                            this.additionalGraphClass = "show-none";
                        }
                    }
                    else if (this.DynamicSearchOnChange.GraphType == 2) {
                        this.dynamicGraph = "show-none";
                        this.dynamicTable = "show-full";
                        this.dynamicGraphWrapperClass = "";

                        if (this.DynamicSearchOnChange.IsAdditionalSummary) {
                            this.additionalGraphClass = "";
                        }
                        else {
                            this.additionalGraphClass = "show-none";
                        }

                    }
                    else if (this.DynamicSearchOnChange.GraphType == 3) {
                        if (this.DynamicSearchOnChange.GraphPosition == 1) {
                            this.dynamicGraph = "show-left-half margin-right";
                            this.dynamicTable = "show-right-half margin-left";
                            this.dynamicGraphWrapperClass = "";
                            this.additionalGraphClass = "";
                        }
                        else if (this.DynamicSearchOnChange.GraphPosition == 2) {
                            this.dynamicGraph = "show-left-half margin-left";
                            this.dynamicTable = "show-right-half margin-right";
                            this.dynamicGraphWrapperClass = "row-reverse";
                            this.additionalGraphClass = "";
                        }
                        else if (this.DynamicSearchOnChange.GraphPosition == 3) {
                            this.dynamicGraph = "show-top-full";
                            this.dynamicTable = "show-bottom-full";
                            this.dynamicGraphWrapperClass = "col";
                            this.additionalGraphClass = "";
                        }
                        else if (this.DynamicSearchOnChange.GraphPosition == 4) {
                            this.dynamicGraph = "show-top-full";
                            this.dynamicTable = "show-bottom-full";
                            this.dynamicGraphWrapperClass = "col-reverse";
                            this.additionalGraphClass = "";
                        }
                    }


                    // For Graph Two
                    if (this.DynamicSearchOnChange.GraphType2 == 1) {
                        this.dynamicGraph1 = "show-full";
                        this.dynamicTable1 = "show-none";
                        this.dynamicGraphWrapperClass1 = "";
                    }
                    else if (this.DynamicSearchOnChange.GraphType2 == 2) {
                        this.dynamicGraph1 = "show-none";
                        this.dynamicTable1 = "show-full";
                        this.dynamicGraphWrapperClass1 = "";
                        this.additionalGraphClass1 = "";
                    }
                    else if (this.DynamicSearchOnChange.GraphType2 == 3) {
                        if (this.DynamicSearchOnChange.GraphPosition == 1) {
                            this.dynamicGraph1 = "show-left-half margin-right";
                            this.dynamicTable1 = "show-right-half margin-left";
                            this.dynamicGraphWrapperClass1 = "";
                            this.additionalGraphClass1 = "";
                        }
                        else if (this.DynamicSearchOnChange.GraphPosition == 2) {
                            this.dynamicGraph1 = "show-left-half margin-left";
                            this.dynamicTable1 = "show-right-half margin-right";
                            this.dynamicGraphWrapperClass1 = "row-reverse";
                            this.additionalGraphClass1 = "";
                        }
                        else if (this.DynamicSearchOnChange.GraphPosition == 3) {
                            this.dynamicGraph1 = "show-top-full";
                            this.dynamicTable1 = "show-bottom-full";
                            this.dynamicGraphWrapperClass1 = "col";
                            this.additionalGraphClass1 = "";
                        }
                        else if (this.DynamicSearchOnChange.GraphPosition == 4) {
                            this.dynamicGraph1 = "show-top-full";
                            this.dynamicTable1 = "show-bottom-full";
                            this.dynamicGraphWrapperClass1 = "col-reverse";
                            this.additionalGraphClass1 = "";
                        }
                    }

                    this.dynamicSearchColumns = response.Data.OrderAnalysisGraphSearchColumn;
                    this.dynamicSearchColumns.forEach((data) => {
                        if (data.FieldType == 'Date Range picker') {
                            data.SelectedValue = {
                                //rohit startdate
                                // beginDate: { year: this.date.getFullYear(), month: this.date.getMonth() - 1, day: this.date.getDate() },
                                beginDate: { year: this.dateRangeStart.getFullYear(), month: this.dateRangeStart.getMonth(), day: this.dateRangeStart.getDate() },
                                endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() }
                            };

                        }
                    })

                    setTimeout(function () {
                        (<any>$('.DynamicSelect option[value="0"]')).attr("selected", true);
                        (<any>$('.multi-select')).find('.dropdown-toggle').trigger('click');
                        that.companiesListValue = that.userCredential.CompanyId;
                        that.customQueryModel.ClientId = that.companiesListValue;
                        (<any>$('.dropdown-item a input')).trigger('click');
                        (<any>$('.dropdown-item a input')).trigger('click');
                        (<any>$('.multi-select')).find('.dropdown-toggle').trigger('click');

                    }, 800);
                });
        }
        else {
            this.isLoader = false;
            this.DynamicGraph = true;
            this.showDynamicGraph = false;
            // this.myForm = this.formBuilder.group({
            //     myDateRange: [{
            //         beginDate: { year: this.dateRangeStart.getFullYear(), month: this.dateRangeStart.getMonth(), day: this.dateRangeStart.getDate() },
            //         endDate: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() }
            //     }, Validators.required]
            // });
            if (val != -7) {
                if (this.userCredential.UserType == 'Client' || this.orderAnalysisParams.UserType == 'MTS Internal User') {
                    this.orderAnalysisParams.VendorId = 0;
                    this.dataService.post('OrderAnalysis/GetVendorList', { "ClientId": this.userCredential.CompanyId, "BusinessCategoryId": this.userCredential.BusinessCategoryId }).subscribe(
                        response => {
                            this.vendorInfos = response;
                            this.vendorInfos.splice(0, 0, { "VendorId": 0, "VendorCompanyName": "All Vendor" });
                        }
                    );
                }
            }
        }
        if (val == -5) {
            this.radio = "1";
            this.vendorAnalysisCount = 20;
        }
        if (val == -7) {
            this.isLoader = true;
            this.vendorConsolidateType = '1';
            this.vendorAnalysisConsolidateCount = 20;
            this.orderAnalysisParams.VendorId = 0;
            this.scoreByVendorConsolidateArray = new Array<any>();
            this.scoreByTestConsolidateArray = new Array<any>();
            this.scoreByTestResultConsolidate = new Array<any>();
            this.dataService.get('OrderAnalysis/GetVendorListForVendorPerformaceAnalysisConsolidate?clientId=' + this.userCredential.CompanyId).subscribe(
                response => {
                    if (response.IsSuccess) {
                        this.vendorInfos = response.Data.map(
                            (data) => {
                                return ({ VendorId: data.Id, VendorCompanyName: data.Name, TestingVendorId: data.TestingVendorId, InspectionVendorId: data.InspectionVendorId })
                            });
                        this.vendorInfos.splice(0, 0, { "VendorId": 0, "VendorCompanyName": "All Vendor" });
                        this.isLoader = false;
                    } else {
                        this.isLoader = false;
                        swal('', response.Message);
                    }
                });
        }

        this.search();
    }

    addSearch() {

        if (this.additionalSearch.length > 0) {
            if (this.additionalSearch[this.additionalSearch.length - 1].ColumnName && this.additionalSearch[this.additionalSearch.length - 1].Operator) {
                this.additionalSearch.push(new AdditionalSearch("", "", ""));
            }
        }
        else {
            this.additionalSearch.push(new AdditionalSearch("", "", ""));
        }
    }

    getCount(e) {
        var val = e.target.value;
        if (val > 100) {
            swal('', "More than 100 records are not allowed");
            e.target.value = 100;
        }
        if (this.analysisMode == -7) {
            this.vendorAnalysisConsolidateCount = e.target.value;
        } else {
            this.vendorAnalysisCount = e.target.value;
        }
    }

    removeSearch(index: number) {
        if (this.additionalSearch.length > 1) {
            this.additionalSearch.splice(index, 1);
        }
    }

    getVendorScoreDate() {
        var thisObj = this;
        thisObj.hideExportToExcel=false;
        if (thisObj.radio == "1") {
            thisObj.vendorTableShow = true;
            thisObj.scoreByTestByShow = false;         
            //this.vendorTestTableShow = false; 
            thisObj.isLoader = true;
            thisObj.orderAnalysisParams.PageSize = this.vendorAnalysisCount;
            thisObj.dataService.post('OrderAnalysis/ScoredbyVendorPerformanceAnalysis', this.orderAnalysisParams).subscribe(
                response => {
                    thisObj.vendorScore = response.ScoredbyVendor;
                    thisObj.isLoader = false;

                })

        }
        else if (thisObj.radio == "2") {
            //var thisObj = this;
            ;
            thisObj.vendorTableShow = false;
            thisObj.scoreByTestByShow = true;
            thisObj.isLoader = true;
            thisObj.orderAnalysisParams.PageSize = this.vendorAnalysisCount;
            thisObj.dataService.post('OrderAnalysis/ScoredbyTestVendorPerformanceAnalysis', this.orderAnalysisParams).subscribe(
                response => {
                    ;
                    thisObj.scoreByTest = [];
                    thisObj.isLoader = false;
                    if (response.ScoredbyTestArr != null) {
                        let x = JSON.parse(response.ScoredbyTestArr);
                        thisObj.scoreByTest = x[0];
                        x.splice(0, 1);
                        x.forEach((data) => {
                            var total = 0;
                            data.forEach((value) => {
                                var y = /^\d+$/.test(value);
                                if (y) {
                                    total = total + (+value);
                                }
                            })
                            data.push(total);
                        })
                        thisObj.scoreByTestResult = x;

                    }
                    else {
                        thisObj.scoreByTest = [];
                        thisObj.scoreByTestResult = null;
                    }
                })
        }
    }

    getImage(TableID: any, ImgID: any, chartName: any) {

        for (let i = 0; i < ImgID.length; i++) {
            this.ExcelFileName = TableID;
            var that = this;
            var tmp = new AmCharts.AmExport(this[chartName[i]]);
            tmp.init();
            tmp.output({
                output: 'datastring',
                format: 'jpg'
            }, function (blob) {

                var me = blob.split('base64,');
                var data =
                    {
                        'Base': ''
                    };

                data['Base'] = me[1];
                if (me.length > 1) {
                    that.dataService.post('OrderAnalysis/Base64Toimage', data).subscribe(
                        response => {
                            if (response.IsSuccess) {
                                var img = <any>document.getElementById(ImgID[i]);
                                img.src = ETRFUrl + response.ImageUrl;
                                //document.getElementById(ImgID).src=response.ImageUrl;
                                var urlid = document.getElementById(TableID);

                                if (i == (ImgID.length - 1)) {
                                    that.getTableToExcel(urlid);
                                }
                                else {
                                    //setTimeout(function(){}, 1000);
                                }
                            }
                            else {
                                swal('', response.Message);
                            }
                        }
                    )
                }
                else {
                    swal('', 'No data to Export Excel!!');
                }
            });
        }
    }

    getExcelSheetWithGraph(TableID: any, ImgID: any, chartName: any) {
        this.isLoader = true;
        this.orderAnalysisParams.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
        if (this.orderAnalysisParams.InnerSearchText) {
            delete this.orderAnalysisParams.InnerSearchText;
        }
        if (this.orderAnalysisParams.SearchText.length > 6) {
            let count = this.orderAnalysisParams.SearchText.length - 5;
            this.orderAnalysisParams.SearchText.splice(5, count);
        }
        var that = this;
        this.additionalSearch.forEach(function (addObj) {
            that.orderAnalysisParams.SearchText.push({ ColumnName: addObj.ColumnName, Operator: addObj.Operator, ColumnValue: addObj.ColumnValue });
        });
    
        this.isLoader = true;
        this.orderAnalysisParams.PageSize = null;

        for (let i = 0; i < ImgID.length; i++) {
            this.ExcelFileName = TableID;
            var that = this;
            var tmp = new AmCharts.AmExport(this[chartName[i]]);
            tmp.init();
            tmp.output({
                output: 'datastring',
                format: 'jpg'
            }, function (blob) {

                var me = blob.split('base64,');
                var data =
                    {
                        'Base': ''
                    };
                data['Base'] = me[1];
                if (me.length > 1) {
                    if (i == 0) {
                        that.orderAnalysisParams.FirstBase64ToImage = data;
                    } else if (i == 1) {
                        that.orderAnalysisParams.SecondBase64ToImage = data;
                    }
                }
            });
        }
        that.orderAnalysisParams.CompanyName = that.userCredential.CompanyName;
        let APIurl
        if (that.analysisMode == -1) {
            APIurl = 'GraphAnalysisExportToExcel/WriteTATAnalysisExportToExcelFile'
        }
        else if (this.analysisMode == -2) {
            APIurl = 'GraphAnalysisExportToExcel/WriteOnHoldSubmissionAnalysisExportToExcelFile'
        }
        else if (this.analysisMode == -3) {
            APIurl = 'GraphAnalysisExportToExcel/WriteFailureAnalysisExportToExcelFile'
        }
        else if (that.analysisMode == -4) {
            APIurl = 'GraphAnalysisExportToExcel/WriteQuickPerformanceAnalysisExportToExcelFile'
        }

        that.dataService.post(APIurl, that.orderAnalysisParams).subscribe(
            response => {
                if (response.IsSuccess) {
                    that.isLoader = false;
                    let downloadFileName = encodeURIComponent(response.Message);
                    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + that.userCredential.BusinessCategoryId;
                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }

    VendorPerformanceExportToExcelFile(Type: string) {
        this.isLoader = true;
        this.orderAnalysisParams.PageSize = this.vendorAnalysisCount;
        let APIUrl;
        if (Type == 'VendorScore') {
            APIUrl = 'GraphAnalysisExportToExcel/WriteScoredbyVendorPerformanceExportToExcelFile'
        } else if (Type = "ScoreByTest") {
            APIUrl = 'GraphAnalysisExportToExcel/WriteScoredbyTestVendorPerformanceAnalysisExcelFile'
        }

        this.dataService.post(APIUrl, this.orderAnalysisParams).subscribe(
            response => {

                if (response.IsSuccess) {
                    this.isLoader = false;
                    let downloadFileName = encodeURIComponent(response.Message);
                    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                }
                else {
                    swal('', response.Message);
                }
            })
    }

    searchParametersChange() {
        this.hideExportToExcel = true;
    }


    getTableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table >{table}</table></body></html>'
            , base64 = function (s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) }
            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: (table.id) || 'Worksheet', table: table.innerHTML }
            //window.location.href = uri + base64(format(template, ctx))
            var dlink = <any>document.getElementById("dlink");
            dlink.href = uri + base64(format(template, ctx));
            dlink.download = (table.id) + ".xls";
            dlink.click();
        }
    })()


    search() {
        // this.vendorTableShow = false;
        // this.vendorTestTableShow = false;
        // this.vendorScoreTab = false;
        // this.failureCodeAnalysisShow = false;
        // this.failureCodeAnalysisChart = false;
        // this.scoreByTestByShow = false;
        var thisObj = this;
        this.ChartType = 0;
        this.GraphType = 0;
        this.showDynamicGraph = false;
        if (this.analysisMode != -6) {
            (<any>$(".export-excel")).show();
        }

        if (thisObj.isDateSelected) {
            /*below for solving bug ot searching and sorting*/
            /*if(this.dateRange.StartDate && this.dateRange.EndDate){
                this.orderAnalysisParams.StartDate=JSON.parse(JSON.stringify(this.dateRange.StartDate));
                this.orderAnalysisParams.EndDate=JSON.parse(JSON.stringify(this.dateRange.EndDate));
            }*/
            this.orderAnalysisParams.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
            /*ended*/
            // this.orderAnalysisParams.SearchText[3].ColumnValue = '';
            //this.orderAnalysisParams.SearchText[4] = { ColumnName: "", Operator: 5, ColumnValue: "" };
            if (this.orderAnalysisParams.InnerSearchText) {
                delete this.orderAnalysisParams.InnerSearchText;
            }

            if (this.orderAnalysisParams.SearchText.length > 6) {
                let count = this.orderAnalysisParams.SearchText.length - 5;
                this.orderAnalysisParams.SearchText.splice(5, count);
            }
            let thisObj = this;
            this.additionalSearch.forEach(function (addObj) {
                thisObj.orderAnalysisParams.SearchText.push({ ColumnName: addObj.ColumnName, Operator: addObj.Operator, ColumnValue: addObj.ColumnValue });
            })


            this.isLoader = true;
            this.orderAnalysisParams.PageSize = null;

            if (this.analysisMode == -1) {
                this.vendorTableShow = false;
                this.scoreByTestByShow = false;
                this.failureCodeAnalysisShow = false;
                this.analysisType = this.analysisMode;

                this.dataService.post('OrderAnalysis/TATAnalysisStatus', this.orderAnalysisParams).subscribe(
                    response => {

                        thisObj.tatAnalysisGraph = [];
                        thisObj.isLoader = false;
                        thisObj.result = response;
                        if (thisObj.result.TATAnalysisList.IsSuccess) {
                            thisObj.hideExportToExcel = false;
                            thisObj.result.TATAnalysisList.OrderAnalysis.forEach(element => {
                                if (element.Type == '1-2 Days') {
                                    thisObj.result.oneTotwo = element.Count;
                                    element.color = "#0067aa";

                                }
                                if (element.Type == '3-7 Days') {
                                    thisObj.result.threeToFive = element.Count;
                                    element.color = "#FFA500";
                                }
                                if (element.Type == 'More Than 7 Days') {
                                    thisObj.result.moreThanFive = element.Count;
                                    element.color = "#67b7dc";
                                }
                                if (element.Type == 'Total') {
                                    thisObj.result.total = element.Count;
                                }

                                if (element.Type != 'Total') {
                                    thisObj.tatAnalysisGraph.push(element);
                                }
                            });

                        }

                        // this.result.oneTotwo = this.result.TATAnalysisList.OrderAnalysis[1].Count;
                        // this.result.threeToFive = this.result.TATAnalysisList.OrderAnalysis[0].Count;                  
                        // this.result.moreThanFive = this.result.TATAnalysisList.OrderAnalysis[2].Count;
                        // this.result.total = this.result.TATAnalysisList.OrderAnalysis[3].Count;                  
                        // this.tatAnalysisGraph.push(this.result.TATAnalysisList.OrderAnalysis[1]);
                        // this.tatAnalysisGraph.push(this.result.TATAnalysisList.OrderAnalysis[0]);
                        // this.tatAnalysisGraph.push(this.result.TATAnalysisList.OrderAnalysis[2]);
                        // this.tatAnalysisGraph.push(this.result.TATAnalysisList.OrderAnalysis[3]);
                        this.createTATChart();

                    }
                )
            }
            else if (this.analysisMode == -2) {

                this.vendorTableShow = false;
                this.scoreByTestByShow = false;
                this.failureCodeAnalysisShow = false;

                this.analysisType = this.analysisMode;
                this.dataService.post('OrderAnalysis/HoldOrderAnalyis', this.orderAnalysisParams).subscribe(
                    response => {
                        thisObj.onHoldGraphOne = [];
                        thisObj.onHoldAnalysis = response;
                        thisObj.onHoldAnalysis.total = thisObj.onHoldAnalysis.OrderAnalysis[0].Count;
                        thisObj.onHoldAnalysis.onHold = thisObj.onHoldAnalysis.OrderAnalysis[1].Count;
                        thisObj.onHoldGraphOne.push(thisObj.onHoldAnalysis.OrderAnalysis[0]);
                        thisObj.onHoldGraphOne.push(thisObj.onHoldAnalysis.OrderAnalysis[1]);
                        thisObj.ratioOfOnHoldSubmission();
                        thisObj.dataService.post('OrderAnalysis/HoldOrderDurationAnalyis', thisObj.orderAnalysisParams).subscribe(
                            response => {

                                thisObj.hideExportToExcel = false;
                                thisObj.onHoldGraphTwo = [];
                                thisObj.isLoader = false;
                                thisObj.onHoldDuration = response;
                                thisObj.onHoldDuration.oneToTwo = thisObj.onHoldDuration.OrderAnalysis[0].Count;
                                thisObj.onHoldDuration.threeToFour = thisObj.onHoldDuration.OrderAnalysis[1].Count;
                                thisObj.onHoldDuration.moreThanFive = thisObj.onHoldDuration.OrderAnalysis[2].Count;

                                thisObj.onHoldGraphTwo.push(thisObj.onHoldAnalysis.OrderAnalysis[0]);
                                thisObj.onHoldGraphTwo.push(thisObj.onHoldAnalysis.OrderAnalysis[1]);
                                thisObj.onHoldGraphTwo.push(thisObj.onHoldAnalysis.OrderAnalysis[2]);
                                thisObj.onHoldDurationRate();


                            }
                        )
                    }
                )


            }
            else if (this.analysisMode == -3) {
                this.vendorTableShow = false;
                this.scoreByTestByShow = false;
                this.failureCodeAnalysisShow = false;
                this.analysisType = this.analysisMode;
                let thisObj = this;
                thisObj.dataService.post('OrderAnalysis/PassFailOrderAnalyis', thisObj.orderAnalysisParams).subscribe(
                    response => {
                        thisObj.hideExportToExcel = false;
                        thisObj.failureGraphOne = [];
                        thisObj.isLoader = false;
                        thisObj.passFailAnalysis = response;
                        thisObj.passFailAnalysis.OrderAnalysis.forEach(function (obj) {
                            if (obj.Type == 'FAIL') {
                                thisObj.failureOrderAnalysis.failOrder = obj.Count;
                                thisObj.failureGraphOne.push(obj);
                            }
                            else if (obj.Type == 'PASS') {
                                thisObj.failureOrderAnalysis.passOrder = obj.Count;
                                thisObj.failureGraphOne.push(obj);
                            }
                            else if (obj.Type == 'OTHERS') {
                                thisObj.failureOrderAnalysis.otherOrder = obj.Count;
                                thisObj.failureGraphOne.push(obj);
                            }
                            else if (obj.Type == 'Total') {
                                thisObj.failureOrderAnalysis.totalOrder = obj.Count;
                            }
                        })
                        thisObj.createPassFailAnalysisChart();

                    }
                )
                thisObj.dataService.post('OrderAnalysis/FailureOrderAnalysis', thisObj.orderAnalysisParams).subscribe(
                    response => {

                        thisObj.hideExportToExcel = false;
                        thisObj.failureAnalysis = response;
                        thisObj.failureAnalysis.OrderAnalysis.forEach(function (obj) {
                            if (obj.Type == 'Critical') {
                                obj.color = "#ac2925";
                                thisObj.failureOrderAnalysis.criticalFailure = obj.Count;
                            }
                            else if (obj.Type == 'Minor') {
                                obj.color = "#fdd400";
                                thisObj.failureOrderAnalysis.minorFailure = obj.Count;
                            }
                            else if (obj.Type == 'Major') {
                                obj.color = "#FF8C00";
                                thisObj.failureOrderAnalysis.majorFailure = obj.Count;
                            }
                        })
                        thisObj.createfailureOrderAnalysisChart();

                    }
                )
            }
            else if (this.analysisMode == -4) {

                this.vendorTableShow = false;
                this.scoreByTestByShow = false;
                this.failureCodeAnalysisShow = false;
                this.analysisType = this.analysisMode;
                let thisObj = this;
                thisObj.dataService.post('OrderAnalysis/PassFailOrderAnalyis', thisObj.orderAnalysisParams).subscribe(
                    response => {
                        thisObj.hideExportToExcel = false;
                        thisObj.quickPerformanceGraph = [];
                        thisObj.isLoader = false;
                        thisObj.passFailAnalysis = response;

                        thisObj.passFailAnalysis.OrderAnalysis.forEach(function (obj) {
                            if (obj.Type == 'FAIL') {
                                thisObj.failureOrderAnalysis.failOrder = obj.Count;
                                thisObj.quickPerformanceGraph.push(obj);
                            }
                            else if (obj.Type == 'PASS') {
                                thisObj.failureOrderAnalysis.passOrder = obj.Count;
                                thisObj.quickPerformanceGraph.push(obj);
                            }
                            else if (obj.Type == 'OTHERS') {
                                thisObj.failureOrderAnalysis.otherOrder = obj.Count;
                                thisObj.quickPerformanceGraph.push(obj);
                            }
                            else if (obj.Type == 'Total') {
                                thisObj.failureOrderAnalysis.totalOrder = obj.Count;
                            }
                        })
                        thisObj.createPerformanceAnalysisChart();

                    }
                )
            }
            else if (this.analysisMode == -5) {
                //this.radio = 1;
                this.orderAnalysisParams.PageSize = this.vendorAnalysisCount;
                this.failureCodeAnalysisShow = false;


                if (this.radio == '' || this.radio == undefined) {
                    this.isLoader = false;
                    swal('', 'plese select "Type of Vendor Score Card"');
                }
                else if (this.radio == 1) {

                    thisObj.dataService.post('OrderAnalysis/ScoredbyVendorPerformanceAnalysis', thisObj.orderAnalysisParams).subscribe(
                        response => {
                            thisObj.hideExportToExcel = false;
                            thisObj.vendorScore = response.ScoredbyVendor;
                            thisObj.isLoader = false;
                            thisObj.vendorTableShow = true;
                            thisObj.scoreByTestByShow = false;

                        })
                }
                else if (this.radio == 2) {
                    this.isLoader = true;

                    this.orderAnalysisParams.PageSize = this.vendorAnalysisCount;
                    this.dataService.post('OrderAnalysis/ScoredbyTestVendorPerformanceAnalysis', thisObj.orderAnalysisParams).subscribe(
                        response => {
                            thisObj.hideExportToExcel = false;
                            thisObj.isLoader = false;
                            thisObj.vendorTableShow = false;                           
                            if (response.ScoredbyTestArr != null) {
                                let x = JSON.parse(response.ScoredbyTestArr);
                                thisObj.scoreByTest = x[0];
                                x.splice(0, 1);                              
                                x.forEach((data) => {
                                    var total = 0;
                                    data.forEach((value) => {
                                        var y = /^\d+$/.test(value);
                                        if (y) {
                                            total = total + (+value);
                                        }
                                    })
                                    data.push(total);
                                })
                                thisObj.scoreByTestResult = x;

                            }
                            else {
                                thisObj.scoreByTest = [];
                                thisObj.scoreByTestResult = null;
                            }


                        })
                }
            }
            //Rohit FailureCodeAnalysis
            else if (this.analysisMode == -6) {

                this.vendorTableShow = false;
                this.scoreByTestByShow = false;
                this.analysisType = this.analysisMode;
                this.failureCodeAnalysisShow = true;

                thisObj.dataService.post('OrderAnalysis/FailureCodeAnalysis', thisObj.orderAnalysisParams).subscribe(
                    response => {
                        thisObj.hideExportToExcel = false;
                        thisObj.failureCodeAnalysisTotal = response.Total;
                        thisObj.failureCodeAnalysis = response.FailureCodeAnalysis;

                        thisObj.failureCodeAnalysis.forEach((data) => {
                            data.color = "#84b761"
                        })
                        thisObj.isLoader = false;
                        thisObj.createFailureCodeAnalysisChart();
                    }
                )
            }
            else if (this.analysisMode == -7) {
                this.orderAnalysisParams.PageSize = this.vendorAnalysisConsolidateCount;
                this.failureCodeAnalysisShow = false;
                if (this.vendorConsolidateType == '' || this.vendorConsolidateType == undefined) {
                    this.isLoader = false;
                    swal('', 'plese select "Type of Vendor Score Card"');
                }
                else if (this.vendorConsolidateType == 1) {
                    this.orderAnalysisParams.VendorInfo = this.vendorInfos.filter(i => i.VendorId == this.orderAnalysisParams.VendorId)[0];
                    this.dataService.post('OrderAnalysis/ScoredbyVendorConsolidate', this.orderAnalysisParams).subscribe(
                        response => {
                            this.hideExportToExcelConsolidate = false;
                            this.scoreByVendorConsolidateArray = response.Data;
                            this.isLoader = false;
                            this.scoreByVendorConsolidate = true;
                            this.scoreByTestConsolidate = false;
                        })
                }
                else if (this.vendorConsolidateType == 2) {
                    this.isLoader = true;
                    this.orderAnalysisParams.PageSize = this.vendorConsolidateType;
                    this.orderAnalysisParams.VendorInfo = this.vendorInfos.filter(i => i.VendorId == this.orderAnalysisParams.VendorId)[0];
                    let vendorId = this.orderAnalysisParams.VendorId;
                    this.orderAnalysisParams.VendorId = (this.orderAnalysisParams.VendorInfo.InspectionVendorId != 0 && this.orderAnalysisParams.TestingVendorId == 0) ? -1 : this.orderAnalysisParams.VendorInfo.TestingVendorId;
                    this.dataService.post('OrderAnalysis/ScoredbyTestConsolidate', thisObj.orderAnalysisParams).subscribe(
                        response => {
                            thisObj.hideExportToExcelConsolidate = false;
                            thisObj.isLoader = false;
                            thisObj.scoreByVendorConsolidate = false;
                            if (response.ScoredbyTestArr != null) {
                                let x = JSON.parse(response.ScoredbyTestArr);

                                thisObj.scoreByTestConsolidateArray = x[0];
                                x.splice(0, 1);
                                x.forEach((data) => {
                                    var total = 0;
                                    data.forEach((value) => {
                                        var y = /^\d+$/.test(value);
                                        if (y) {
                                            total = total + (+value);
                                        }
                                    })
                                    data.push(total);
                                })
                                thisObj.scoreByTestResultConsolidate = x;

                            }
                            else {
                                thisObj.scoreByTestConsolidateArray = [];
                                thisObj.scoreByTestResultConsolidate = null;
                            }
                            this.orderAnalysisParams.VendorId = vendorId;
                        })
                }
            }
        }
        else {
            swal('', 'Please Select Date Range');
        }

    }
    createFailureCodeAnalysisChart() {

        this.failureCodeChart = AmCharts.makeChart("failureCodeAnalysisChartTemp", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,
            "autoMargins": true,
            "dataProvider": this.failureCodeAnalysis,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 1,
                "lineAlpha": 0.1,
                "type": "column",
                "valueField": "TestFaliures"
            }],
            "depth3D": 0,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "TestName",
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 90
            }
        })
    }

    createTATChart() {
        this.tatChart = AmCharts.makeChart("Chart", {
            "type": "pie",
            "theme": "light",
            "legend": {
                "markerSize": 10,
                "position": "top",
                "autoMargins": false,
                "maxColumns": 5
            },
            "dataProvider": this.tatAnalysisGraph,
            "valueField": "Count",
            "titleField": "Type",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "colorField": "color",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": false
            }
        });

    }

    onDateRangeChanged(event: any) {
        if (event.beginJsDate && event.endJsDate !== null) {
            this.hideExportToExcel = true;
            this.isDateSelected = true;
            // this.dateRange.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
            // this.dateRange.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
            this.orderAnalysisParams.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
            this.orderAnalysisParams.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
        }
        else {
            this.isDateSelected = false;
            // this.getDefaultAnalysisPeriod();
        }
        // this.orderAnalysisParams.StartDate = event.beginDate.month + '/' + event.beginDate.day + '/' + event.beginDate.year;
        // this.orderAnalysisParams.EndDate = event.endDate.month + '/' + event.endDate.day + '/' + event.endDate.year;
       
    }

    createfailureOrderAnalysisChart() {
        this.failureAnalysisChart = AmCharts.makeChart("failureAnalysisChart", {
            "type": "pie",
            "theme": "light",
            "legend": {
                "markerSize": 10,
                "position": "top",
                "autoMargins": false,
                "maxColumns": 5
            },
            "dataProvider": this.failureAnalysis.OrderAnalysis,
            "valueField": "Count",
            "titleField": "Type",
            "startEffect": "elastic",
            "colorField": "color",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": false
            }
        });

    }

    createPassFailAnalysisChart() {
        var dataProvider = this.failureGraphOne.map((data) => {
            if (data.Type == "FAIL") {
                data.color = "#ac2925";
            }
            else if (data.Type == "PASS") {
                data.color = "#84b761";
            }
            return data;
        })
        this.passAndFailChart = AmCharts.makeChart("passFailAnalysisChart", {
            "type": "pie",
            "theme": "light",
            "legend": {
                "markerSize": 10,
                "position": "top",
                "autoMargins": false,
                "maxColumns": 5
            },
            "dataProvider": dataProvider,
            "valueField": "Count",
            "colorField": "color",
            "titleField": "Type",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": false
            }
        });
    }

    createPerformanceAnalysisChart() {

        var dataProvider = this.quickPerformanceGraph.map((data) => {
            if (data.Type == "FAIL") {
                data.color = "#ac2925";
            }
            else if (data.Type == "OTHERS") {
                data.color = "#85c5e3";
            }
            else if (data.Type == "PASS") {
                data.color = "#84b761";
            }
            return data;
        })

        this.quickPerformanceChart = AmCharts.makeChart("quickPerformanceAnalysis", {
            "type": "pie",
            "theme": "light",
            "legend": {
                "markerSize": 10,
                "position": "top",
                "autoMargins": false,
                "maxColumns": 5
            },
            "dataProvider": dataProvider,
            "valueField": "Count",
            "titleField": "Type",
            "colorField": "color",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": false
            }
        });

    }

    ratioOfOnHoldSubmission() {
        var dataProvider = [];
        var totalCount;
        var onHoldCount;
        this.onHoldGraphOne.forEach((data) => {
            if (data.Type == 'Total') {
                totalCount = data.Count;
            }
            if (data.Type == 'Hold') {
                onHoldCount = data.Count;
                dataProvider.push(data);

                if ((parseInt(totalCount) - parseInt(onHoldCount)) > 0) {
                    let temp = {
                        "Type": "Other",
                        "Count": (parseInt(totalCount) - parseInt(onHoldCount)).toString()
                    }

                    dataProvider.push(temp);
                }
            }
        })

        this.onHoldsubmissionChart = AmCharts.makeChart("ratioOfOnHoldSubmission", {
            "type": "pie",
            "theme": "light",
            "legend": {
                "markerSize": 10,
                "position": "top",
                "autoMargins": false,
                "maxColumns": 5
            },
            "dataProvider": dataProvider,
            "valueField": "Count",
            "titleField": "Type",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": false
            }
        });

    }

    onHoldDurationRate() {

        var dataProvider = this.onHoldDuration.OrderAnalysis.map((data) => {
            if (data.Type == "1-2 Days") {
                data.color = "#0067aa";
            }
            else if (data.Type == "3-5 Days") {
                data.color = "#FFA500";
            }
            else if (data.Type == "More Than 5 Days") {
                data.color = "#67b7dc";
            }
            return data;
        })
        this.onHoldDurationChart = AmCharts.makeChart("onHoldDurationRate", {
            "type": "pie",
            "theme": "light",
            "legend": {
                "markerSize": 10,
                "position": "top",
                "autoMargins": false,
                "maxColumns": 5
            },
            "dataProvider": dataProvider,
            "valueField": "Count",
            "titleField": "Type",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "colorField": "color",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": false
            }
        });

    }

    myDateRangePickerOptions = {
        clearBtnTxt: 'Clear',
        beginDateBtnTxt: 'Begin Date',
        endDateBtnTxt: 'End Date',
        acceptBtnTxt: 'OK',
        dateFormat: 'dd/mm/yyyy',
        firstDayOfWeek: 'mo',
        sunHighlight: true,
        height: '34px',
        width: '260px',
        inline: false,
        selectionTxtFontSize: '11px',
        alignSelectorRight: false,
        indicateInvalidDateRange: true,
        showDateRangeFormatPlaceholder: false,
        showClearBtn: false,
        customPlaceholderTxt: 'Analysis Period'
    }

    // analysisDetails(type, value, pagesize) {
    //      
    //     this.analysisType
    //     var thisObj = this;
    //     this.ExportType = 1;
    //     this.GetOrderList = true;
    //     if (thisObj.orderAnalysisParams.InnerSearchText) {
    //         delete thisObj.orderAnalysisParams.InnerSearchText;
    //     }
    //     thisObj.orderAnalysisParams.PageNumber = 1;
    //     thisObj.orderAnalysisParams.PageSize = 10;
    //     //thisObj.orderAnalysisParams.SearchText[3] = { ColumnName: "", Operator: 5, ColumnValue: "" };
    //     this.orderAnalysisParams.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
    //     this.additionalSearch.forEach(function (addObj) {
    //         thisObj.orderAnalysisParams.SearchText.push({ ColumnName: addObj.ColumnName, Operator: addObj.Operator, ColumnValue: addObj.ColumnValue });
    //     })
    //     if (type == 'TAT') {
    //         //this.sear


    //         // thisObj.orderAnalysisParams.SearchText[3].ColumnName = type;
    //         if (value == '1 AND 2' || value == '3 AND 7') {
    //             this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 6, ColumnValue: value })
    //             //thisObj.orderAnalysisParams.SearchText[3].Operator = 6;
    //         }
    //         else if (value == '7') {
    //             this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 7, ColumnValue: value })
    //             // thisObj.orderAnalysisParams.SearchText[3].Operator = 7;
    //         }
    //         else if (value == '1') {
    //             this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 10, ColumnValue: value })
    //             // thisObj.orderAnalysisParams.SearchText[3].Operator = 7;
    //         }

    //         else {
    //             this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 5, ColumnValue: value })
    //         }
    //         this.GetTATAnalysisReportOnView(thisObj);
    //         //thisObj.orderAnalysisParams.SearchText[3].ColumnValue = value;
    //     }
    //     else if (type == 'Failure') {
    //         if (value == 'PASS' || value == 'FAIL' || value == 'OTHER') {
    //             this.orderAnalysisParams.SearchText.push({ ColumnName: 'ReportRating', Operator: 5, ColumnValue: value })
    //             // thisObj.orderAnalysisParams.SearchText[3].ColumnValue = value;
    //             // thisObj.orderAnalysisParams.SearchText[3].ColumnName = 'ReportRating';
    //         }
    //         else {
    //             let index = this.orderAnalysisParams.SearchText.findIndex(i => i.ColumnName == "ReportRating");
    //             if (index > -1) {
    //                 this.orderAnalysisParams.SearchText.splice(index, 1);
    //             }
    //             //     this.orderAnalysisParams.SearchText.push({ ColumnName: 'ReportRating', Operator: 5, ColumnValue: value })
    //         }
    //         thisObj.isLoader = true;
    //         thisObj.dataService.post('OrderAnalysis/GetOrderAnalysisList', thisObj.orderAnalysisParams).subscribe(
    //             response => {
    //                 thisObj.isLoader = false;
    //                 (<any>$('#analysisDetails')).modal('show');
    //                 if (response.IsSuccess) {
    //                     thisObj.orderList = response.Data;
    //                     thisObj.totalItem = Math.ceil(pagesize);
    //                 }
    //                 else {
    //                     swal('', response.Message);
    //                 }

    //             }
    //         )
    //     }
    // }

    tatAnalysisDetails(type, value, pagesize) {
        var thisObj = this;
        this.ExportType = 1;
        this.GetOrderList = true;
        if (thisObj.orderAnalysisParams.InnerSearchText) {
            delete thisObj.orderAnalysisParams.InnerSearchText;
        }
        thisObj.orderAnalysisParams.PageNumber = 1;
        thisObj.orderAnalysisParams.PageSize = 10;
        this.orderAnalysisParams.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
        this.additionalSearch.forEach(function (addObj) {
            thisObj.orderAnalysisParams.SearchText.push({ ColumnName: addObj.ColumnName, Operator: addObj.Operator, ColumnValue: addObj.ColumnValue });
        })

        if (value == '1 AND 2' || value == '3 AND 7') {
            this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 6, ColumnValue: value })
            //thisObj.orderAnalysisParams.SearchText[3].Operator = 6;
        }
        else if (value == '7') {
            this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 7, ColumnValue: value })
            // thisObj.orderAnalysisParams.SearchText[3].Operator = 7;
        }
        else if (value == '1') {
            this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 10, ColumnValue: value })
            // thisObj.orderAnalysisParams.SearchText[3].Operator = 7;
        }

        else {
            this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 5, ColumnValue: value })
        }

        this.GetTATAnalysisReportOnView(thisObj);
    }

    GetTATAnalysisReportOnView(thisObj) {

        thisObj.orderList = [];
        this.p = 1;
        thisObj.isLoader = true;
        thisObj.dataService.post('OrderAnalysis/GetTATAnalysisList', thisObj.orderAnalysisParams).subscribe(
            response => {
                this.totalItem = 0;
                thisObj.isLoader = false;
                (<any>$('#analysisDetails')).modal('show');
                if (response.IsSuccess) {

                    thisObj.orderList = response.Data;
                    this.totalItem = Math.ceil(response.TotalRecords / this.orderAnalysisParams.PageSize);
                }
                else {
                    swal('', response.Message);
                }

            }
        )
    }

    onHoldAnalysisDetails(type, value, pagesize) {

        this.p = 1;
        this.orderList = [];
        this.GetOrderList = false;
        let thisObj = this;
        this.ExportType = 2;
        this.orderAnalysisParams.PageNumber = 1;
        this.orderAnalysisParams.PageSize = 10;
        thisObj.orderAnalysisParams.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
        this.additionalSearch.forEach(function (addObj) {
            thisObj.orderAnalysisParams.SearchText.push({ ColumnName: addObj.ColumnName, Operator: addObj.Operator, ColumnValue: addObj.ColumnValue });
        })
        if (type == 'HoldDuration') {
            //this.orderAnalysisParams.SearchText[3].ColumnName = type;
            if (value == '') {

            }
            else if (value == 'Hold') {
                this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 1, ColumnValue: value })
            }
            else if (value == '1 AND 2' || value == '3 AND 5') {

                this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 6, ColumnValue: value })
                //this.orderAnalysisParams.SearchText[3].Operator = 6;
            }
            else if (value == '5') {
                this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 7, ColumnValue: value })
                // this.orderAnalysisParams.SearchText[3].Operator = 7;
            }
            else {
                this.orderAnalysisParams.SearchText.push({ ColumnName: type, Operator: 5, ColumnValue: value })
            }
            //this.orderAnalysisParams.SearchText[3].ColumnValue = value;
        }


        this.dataService.post('OrderAnalysis/GetOrderAnalysisHoldList', this.orderAnalysisParams).subscribe(
            response => {
                this.isLoader = false;
                (<any>$('#analysisDetails')).modal('show');
                if (response.IsSuccess) {
                    thisObj.orderList = response.Data;
                    this.totalItem = Math.ceil(response.TotalRecords / this.orderAnalysisParams.PageSize);
                }
                else {
                    swal('', response.Message);
                }
            }
        )
    }

    passFailAnalysisDetails(type, value, pagesize) {
        this.p = 1;
        this.failureAnalysisCategory = '';

        var thisObj = this;
        thisObj.orderList = [];
        this.GetOrderList = true;
        if (thisObj.orderAnalysisParams.InnerSearchText) {
            delete thisObj.orderAnalysisParams.InnerSearchText;
        }
        this.orderAnalysisParams.PageNumber = 1;
        this.orderAnalysisParams.PageSize = 10;
        //thisObj.orderAnalysisParams.SearchText[3] = { ColumnName: "", Operator: 5, ColumnValue: "" };
        this.orderAnalysisParams.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
        this.additionalSearch.forEach(function (addObj) {
            thisObj.orderAnalysisParams.SearchText.push({ ColumnName: addObj.ColumnName, Operator: addObj.Operator, ColumnValue: addObj.ColumnValue });
        })

        if (value == 'PASS' || value == 'FAIL' || value == 'OTHER') {
            this.orderAnalysisParams.SearchText.push({ ColumnName: 'ReportRating', Operator: 5, ColumnValue: value })
            // thisObj.orderAnalysisParams.SearchText[3].ColumnValue = value;
            // thisObj.orderAnalysisParams.SearchText[3].ColumnName = 'ReportRating';
        }
        else {
            let index = this.orderAnalysisParams.SearchText.findIndex(i => i.ColumnName == "ReportRating");
            if (index > -1) {
                this.orderAnalysisParams.SearchText.splice(index, 1);
            }
            //     this.orderAnalysisParams.SearchText.push({ ColumnName: 'ReportRating', Operator: 5, ColumnValue: value })
        }
        thisObj.isLoader = true;
        thisObj.dataService.post('OrderAnalysis/GetPassFailOrderAnalysisListOnView', thisObj.orderAnalysisParams).subscribe(
            response => {
                thisObj.isLoader = false;
                (<any>$('#analysisDetails')).modal('show');
                if (response.IsSuccess) {
                    thisObj.orderList = response.Data;
                    this.totalItem = Math.ceil(response.TotalRecords / this.orderAnalysisParams.PageSize);
                }
                else {
                    swal('', response.Message);
                }

            }
        )
    }

    failureAnaysisDetails(value) {
        this.p = 1;
        this.failureAnalysisCategory = value;
        let thisObj = this;
        //this.ExportType = 3;
        thisObj.orderAnalysisParams.PageNumber = 1;
        thisObj.orderAnalysisParams.PageSize = 10;
        thisObj.orderAnalysisParams.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
        this.additionalSearch.forEach(function (addObj) {
            thisObj.orderAnalysisParams.SearchText.push({ ColumnName: addObj.ColumnName, Operator: addObj.Operator, ColumnValue: addObj.ColumnValue });
        })
        // thisObj.orderAnalysisParams.SearchText[3].ColumnValue = '';
        // thisObj.orderAnalysisParams.SearchText[4] = { ColumnName: "", Operator: 5, ColumnValue: "" };
        thisObj.orderAnalysisParams.InnerSearchText = [{ "ColumnName": "FailCategory", "Operator": 5, "ColumnValue": value }];

        thisObj.isLoader = true;

        thisObj.dataService.post('OrderAnalysis/GetFailureAnalysisOrderList', thisObj.orderAnalysisParams).subscribe(
            response => {
                thisObj.isLoader = false;
                (<any>$('#analysisDetails')).modal('show');
                if (response.IsSuccess) {
                    thisObj.orderList = response.Data;
                    this.totalItem = Math.ceil(response.TotalRecords / this.orderAnalysisParams.PageSize);
                }
                else {
                    swal('', response.Message);
                }

            }
        )
    }


    getPage(Page: Number) {

        this.orderAnalysisParams.PageNumber = Page;
        this.isLoader = true;
        this.p = Page;

        if (this.analysisMode == -1) {
            this.dataService.post('OrderAnalysis/GetTATAnalysisList', this.orderAnalysisParams).subscribe(
                response => {

                    this.isLoader = false;
                    (<any>$('#analysisDetails')).modal('show');
                    if (response.IsSuccess) {
                        this.orderList = response.Data;
                        //this.p = Page;
                    }
                    else {
                        swal('', response.Message);
                    }

                }
            )
        }
        else if (this.analysisMode == -2) {
            this.dataService.post('OrderAnalysis/GetOrderAnalysisHoldList', this.orderAnalysisParams).subscribe(
                response => {
                    this.isLoader = false;
                    (<any>$('#analysisDetails')).modal('show');
                    if (response.IsSuccess) {
                        this.orderList = response.Data;
                        this.p = Page;
                    }
                    else {
                        swal('', response.Message);
                    }
                }
            )
        }

        else if (this.analysisMode == -3 || this.analysisMode == -4) {
            if (this.failureAnalysisCategory == '') {
                this.isLoader = true;
                this.dataService.post('OrderAnalysis/GetPassFailOrderAnalysisListOnView', this.orderAnalysisParams).subscribe(
                    response => {
                        this.isLoader = false;
                        (<any>$('#analysisDetails')).modal('show');
                        if (response.IsSuccess) {
                            this.orderList = response.Data;
                            this.p = Page;
                            //this.totalItem = Math.ceil(response.TotalRecords);
                        }
                        else {
                            swal('', response.Message);
                        }

                    }
                )
            }
            else {
                this.orderAnalysisParams.InnerSearchText = [{ "ColumnName": "FailCategory", "Operator": 5, "ColumnValue": this.failureAnalysisCategory }];
                this.isLoader = true;
                this.dataService.post('OrderAnalysis/GetFailureAnalysisOrderList', this.orderAnalysisParams).subscribe(
                    response => {
                        this.isLoader = false;
                        (<any>$('#analysisDetails')).modal('show');
                        if (response.IsSuccess) {
                            this.orderList = response.Data;
                            this.p = Page;
                        }
                        else {
                            swal('', response.Message);
                        }

                    }
                )
            }
        }
    }



    reset() {
        this.orderList = {};
    }

    //rohit
    FailureCodeAnalysisExcel(TableID: any, ImgID: any, chartName: any) {
        this.isLoader=true;
        var SaveImageUrl = "";
        for (let i = 0; i < ImgID.length; i++) {
            this.ExcelFileName = TableID;

            var that = this;
            var tmp = new AmCharts.AmExport(this[chartName[i]]);
            tmp.init();
            tmp.output({
                output: 'datastring',
                format: 'jpg'
            }, function (blob) {
                var me = blob.split('base64,');
                var data =
                    {
                        'Base': ''
                    };
                data['Base'] = me[1];
                if (me.length > 1) {
                    that.dataService.post('OrderAnalysis/Base64Toimage', data).subscribe(
                        response => {
                            if (response.IsSuccess) {
                                SaveImageUrl = ETRFUrl + response.ImageUrl;
                                var ExcelData = {
                                    'TableData': JSON.stringify(that.failureCodeAnalysis),
                                    'ImageUrl': SaveImageUrl
                                }
                                that.dataService.post('OrderAnalysis/ExportToExcelImage', ExcelData).subscribe(
                                    response => {   
                                        that.isLoader=false;
                                        if (response.IsSuccess) {
                                            window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Data + "&fileType=excel&businessCategoryId=" + that.userCredential.BusinessCategoryId;
                                        }
                                        else {                                            
                                            swal('', response.Message);
                                        }
                                    }
                                )
                            }
                            else {
                                that.isLoader=false;
                                swal('', response.Message);
                            }
                        }
                    )
                }
                else {
                    that.isLoader=false;
                    swal('', 'No data to Export Excel!!');
                }
            });

        }

    }

    ExportToExcelAnalysis() {

        let analysisMode = this.analysisMode;
        this.isLoader = true;
        var that = this;
        this.orderAnalysisParams.PageNumber = 1;
        if (analysisMode == -1) {
            this.dataService.post('OrderAnalysis/ExportToExcelTATOrderAnalysisList', this.orderAnalysisParams).subscribe(
                response => {

                    that.isLoader = false;
                    if (response.IsSuccess) {
                        window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;

                    }
                    else {
                        swal('', response.Message);
                    }
                }
            )
        }
        else if (analysisMode == -2) {
            this.dataService.post('OrderAnalysis/ExportToExcelOnHoldOrderAnalysis', this.orderAnalysisParams).subscribe(
                response => {

                    that.isLoader = false;
                    if (response.IsSuccess) {
                        window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                        // that.isLoader = false;
                    }
                    else {
                        swal('', response.Message);
                    }
                }
            )
        }
        else if (analysisMode == -3) {
            if (this.failureAnalysisCategory == '') {
                this.dataService.post('OrderAnalysis/ExportToExcelPassFailOrderAnalysis', this.orderAnalysisParams).subscribe(
                    response => {

                        that.isLoader = false;
                        if (response.IsSuccess) {
                            window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                            // that.isLoader = false;
                        }
                        else {
                            swal('', response.Message);
                        }
                    }
                )
            }
            else {
                this.dataService.post('OrderAnalysis/ExportToExcelFailureAnalysis', this.orderAnalysisParams).subscribe(
                    response => {

                        that.isLoader = false;
                        if (response.IsSuccess) {
                            window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                            // that.isLoader = false;
                        }
                        else {
                            swal('', response.Message);
                        }
                    }
                )
            }
        }
        else if (analysisMode == -4) {
            this.dataService.post('OrderAnalysis/ExportToExcelPassFailOrderAnalysis', this.orderAnalysisParams).subscribe(
                response => {

                    that.isLoader = false;
                    if (response.IsSuccess) {
                        window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + response.Message + "&fileType=excel&businessCategoryId=" + this.userCredential.BusinessCategoryId;
                        // that.isLoader = false;
                    }
                    else {
                        swal('', response.Message);
                    }
                }
            )
        }
    }

    getVendorConsolidateType() {
        if (this.vendorConsolidateType == '1') {
            this.scoreByVendorConsolidate = true;
            this.scoreByTestConsolidate = false;
            this.isLoader = true;
            this.orderAnalysisParams.PageSize = this.vendorAnalysisConsolidateCount;
            this.orderAnalysisParams.VendorInfo = this.vendorInfos.filter(i => i.VendorId == this.orderAnalysisParams.VendorId)[0];
            this.dataService.post('OrderAnalysis/ScoredbyVendorConsolidate', this.orderAnalysisParams).subscribe(
                response => {
                    if (response.Message) {
                        this.scoreByVendorConsolidateArray = response.Data;
                        this.isLoader = false;
                    } else {
                        this.isLoader = false;
                    }
                });
        }
        else if (this.vendorConsolidateType == '2') {
            this.scoreByVendorConsolidate = false;
            this.scoreByTestConsolidate = true;
            this.isLoader = true;
            this.orderAnalysisParams.PageSize = this.vendorAnalysisConsolidateCount;
            this.orderAnalysisParams.VendorInfo = this.vendorInfos.filter(i => i.VendorId == this.orderAnalysisParams.VendorId)[0];
            let vendorId = this.orderAnalysisParams.VendorId;
            this.orderAnalysisParams.VendorId = (this.orderAnalysisParams.VendorInfo.InspectionVendorId != 0 && this.orderAnalysisParams.TestingVendorId == 0) ? -1 : this.orderAnalysisParams.VendorInfo.TestingVendorId;
            this.dataService.post('OrderAnalysis/ScoredbyTestConsolidate', this.orderAnalysisParams).subscribe(
                response => {
                    if (response.Message) {
                        this.scoreByTestConsolidateArray = [];
                        this.isLoader = false;
                        if (response.ScoredbyTestArr != null) {
                            let x = JSON.parse(response.ScoredbyTestArr);
                            this.scoreByTestConsolidateArray = x[0];
                            x.splice(0, 1);
                            x.forEach((data) => {
                                var total = 0;
                                data.forEach((value) => {
                                    var y = /^\d+$/.test(value);
                                    if (y) {
                                        total = total + (+value);
                                    }
                                })
                                data.push(total);
                            })
                            this.scoreByTestResultConsolidate = x;

                        }
                        else {
                            this.scoreByTestConsolidateArray = [];
                            this.scoreByTestResultConsolidate = null;
                        }
                    } else {
                        this.isLoader = false;
                    }
                    this.orderAnalysisParams.VendorId = vendorId;
                });
        }
    }
}
