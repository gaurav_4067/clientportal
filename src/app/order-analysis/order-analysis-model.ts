export class AdditionalSearch {
    constructor(ColumnName: string, Operator: string, ColumnValue: string) {
        this.ColumnName = ColumnName;
        this.Operator = Operator;
        this.ColumnValue = ColumnValue;
    }
    public ColumnName: string;
    public Operator: string;
    public ColumnValue: string;
}

export class TestSubstanceExcelParam {
    constructor() {
        this.SearchText = new Array<OrderSearch>();
    }
    public Id: number;
    public ClientIds: number;
    public SearchText: Array<OrderSearch>;
    public Type: number;
    public ImageData: string
}

export class OrderSearch {
    public ColumnName: string;
    public Operator: Operator;
    public ColumnValue: string;
    public LogicalOperator: string;
}

export enum Operator {
    Equal = 1,
    NotEqual = 2,
    StartsWith = 3,
    EndsWith = 4,
    Contains = 5,
    Between = 6,
    GreaterThan = 7,
    LessThan = 8,
    NotContains = 9,
    GreaterThanEqualTo = 10
}
