import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicGraphComponent } from 'app/order-analysis/dynamic-graph/dynamic-graph.component';
import { FormsModule } from '../../../../node_modules/@angular/forms';

@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [DynamicGraphComponent],
  exports: [DynamicGraphComponent]
})
export class DynamicGraphModule { }
