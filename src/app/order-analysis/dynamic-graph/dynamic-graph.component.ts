import { Component, OnInit, Input } from '@angular/core';
import { DynamicGraphModel } from './dynamic-graph-model';
import { DataService, BaseUrl } from '../../mts.service';
import {TestSubstanceExcelParam} from '../order-analysis-model';
declare let AmCharts: any;
declare let swal: any;

@Component({
    selector: 'dynamic-graph',
    templateUrl: './dynamic-graph.component.html'
})

export class DynamicGraphComponent implements OnInit {
    vendorBarChart: AmCharts.AmChart;
    barChart: AmCharts.AmChart;

    @Input() count;
    @Input() dynamicGraph;
    @Input() dynamicTable;
    @Input() dynamicGraphWrapperClass;
    @Input() DynamicSearchOnChange;
    @Input() dynamicSearchResult;
    @Input() additionalGraphClass;
    @Input() CompanyList;
    @Input() DynamicSearch

    showGraph: any = false;
    radio1Value: number = 0;
    radio2Value: number;
    GraphSummaryColumn: any = [];
    GraphSummaryJson: any = [];
    GraphSummaryLength: any = 1;
    AdditionalGraphSummaryColumn: any = [];
    AdditionalGraphSummaryJson: any = [];
    AdditionalGraphSummaryLength: any = 1;
    isAdditionalSummary: any = false;
    othersInclude: any = "";

    graphDivId: any;
    showRatio: false;
    graphTitle: any = "";
    showTotal: any = false;
    totalValue: any;
    userCredential: any;
    userDetail: any;
    substanceDateList: any;
    recordDisplay: number = 0;
    GraphSummaryCopy: any = [];
    isSubstanceSelected: boolean = true;
    VendorGraphSummaryCopy: any = [];
    VendorGraphColumn: any = [];
    VendorGraphSummary: string;
    isLoader: boolean;
    selectedId: number = 0;
    scoredByType: number = 0;
    TestSubstanceExcelParam:TestSubstanceExcelParam;
    constructor(private dataService: DataService) {
        this.isSubstanceSelected = true;
        this.TestSubstanceExcelParam = new TestSubstanceExcelParam();
    }
    ngOnInit() {
        this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        if (this.count == "1") {
            if (this.DynamicSearchOnChange.showTotal == true && this.DynamicSearchOnChange.GraphType != '1') {
                if (this.dynamicSearchResult.GraphJson.Total) {
                    this.totalValue = this.dynamicSearchResult.GraphJson.Total;
                    this.showTotal = true;
                }
            }
            (<any>$('.graph-show')).append('<div id="chartHolder" style="width: 100%; height: 350px; max-height: auto;"></div>');
            DynamicGraphModel.GraphName = this.DynamicSearchOnChange.GraphName;
            DynamicGraphModel.ChartType = this.DynamicSearchOnChange.ChartType;
            DynamicGraphModel.GraphType = this.DynamicSearchOnChange.GraphType;
            DynamicGraphModel.GraphPosition = this.DynamicSearchOnChange.GraphPosition;
            DynamicGraphModel.GraphJson = this.DynamicSearchOnChange.GraphJson;
            DynamicGraphModel.GraphQueryColumn = this.dynamicSearchResult.GraphColumn;
            DynamicGraphModel.IsAdditionalSummary = this.DynamicSearchOnChange.IsAdditionalSummary;
            DynamicGraphModel.VendorJson = this.dynamicSearchResult.VendorJson;
            DynamicGraphModel.VendorQueryColumn = this.dynamicSearchResult.VendorColumn;
            this.VendorGraphColumn = DynamicGraphModel.VendorQueryColumn;
            if (this.dynamicSearchResult.VendorJson) {
                DynamicGraphModel.VendorQueryJson = JSON.parse(this.dynamicSearchResult.VendorJson.ResultJson);
                this.VendorGraphSummary = DynamicGraphModel.VendorQueryJson;
                this.VendorGraphSummaryCopy = DynamicGraphModel.VendorQueryJson;
            }

            if (this.dynamicSearchResult.GraphJson) {
                DynamicGraphModel.GraphQueryJson = JSON.parse(this.dynamicSearchResult.GraphJson.ResultJson);
                this.GraphSummaryCopy = DynamicGraphModel.GraphQueryJson;
                this.othersInclude = JSON.parse(this.DynamicSearchOnChange.GraphJson).othersInclude;
                this.showRatio = JSON.parse(this.DynamicSearchOnChange.GraphJson).showRatio;
            }
            else {
                this.othersInclude = "";
                this.showRatio = false;
            }

            this.graphTitle = this.DynamicSearchOnChange.GraphName;
            this.isAdditionalSummary = this.DynamicSearchOnChange.IsAdditionalSummary;

            if (this.DynamicSearchOnChange.GraphType == "1") {
                if (this.dynamicSearchResult.GrpahAdditionalSummaryJson) {
                    this.GraphSummaryJson = JSON.parse(this.dynamicSearchResult.GrpahAdditionalSummaryJson.ResultJson);
                    this.GraphSummaryColumn = this.dynamicSearchResult.GrpahAdditionalSummaryColumn;
                    this.GraphSummaryLength = this.GraphSummaryColumn.length;
                }
            }
            else if (this.showRatio && this.DynamicSearchOnChange.ChartType != "2") {
                this.dynamicSearchResult.GraphColumn.push('Ratio');
                this.GraphSummaryColumn = this.dynamicSearchResult.GraphColumn;
                this.GraphSummaryLength = this.GraphSummaryColumn.length;


                var summaryJson = JSON.parse(this.dynamicSearchResult.GraphJson.ResultJson);
                var fieldName = this.dynamicSearchResult.GraphColumn[1];
                summaryJson.forEach((data) => {
                    data.Ratio = ((parseInt(data[fieldName]) / parseInt(this.dynamicSearchResult.GraphJson.Total)) * 100).toFixed(2).toString() + "%";
                })

                this.GraphSummaryJson = summaryJson;
            }
            else if (this.DynamicSearchOnChange.ChartType == "2") {
                this.GraphSummaryJson = JSON.parse(this.dynamicSearchResult.GraphJson.StackSummaryJson);
                this.GraphSummaryColumn = this.dynamicSearchResult.GraphJson.StackSummaryTableFields;
                this.GraphSummaryLength = this.GraphSummaryColumn.length;
            }
            else {
                this.GraphSummaryJson = JSON.parse(this.dynamicSearchResult.GraphJson.ResultJson);
                this.GraphSummaryColumn = this.dynamicSearchResult.GraphColumn;
                this.GraphSummaryLength = this.GraphSummaryColumn.length;
            }


            if (this.dynamicSearchResult.GrpahAdditionalSummaryJson) {
                DynamicGraphModel.GrpahAdditionalSummaryJson = JSON.parse(this.dynamicSearchResult.GrpahAdditionalSummaryJson.ResultJson);
                DynamicGraphModel.GrpahAdditionalSummaryColumn = this.dynamicSearchResult.GrpahAdditionalSummaryColumn;

                this.AdditionalGraphSummaryColumn = this.dynamicSearchResult.GrpahAdditionalSummaryColumn;
                this.AdditionalGraphSummaryJson = JSON.parse(this.dynamicSearchResult.GrpahAdditionalSummaryJson.ResultJson);
                this.AdditionalGraphSummaryLength = this.AdditionalGraphSummaryColumn.length;
            }

            this.graphDivId = "chartHolder";
        }

        if (this.count == "2") {
            if (this.DynamicSearchOnChange.showTotal2 == true && this.DynamicSearchOnChange.GraphType2 != '1') {
                if (this.dynamicSearchResult.GraphJson2.Total) {
                    this.totalValue = this.dynamicSearchResult.GraphJson2.Total;
                    this.showTotal = true;
                }
            }
            (<any>$('.graph-show')).eq(1).append('<div id="chartHolder1" style="width: 100%; height: 350px; max-height: auto;"></div>');
            DynamicGraphModel.GraphName = this.DynamicSearchOnChange.GraphName2;
            DynamicGraphModel.ChartType = this.DynamicSearchOnChange.ChartType2;
            DynamicGraphModel.GraphType = this.DynamicSearchOnChange.GraphType2;
            DynamicGraphModel.GraphPosition = this.DynamicSearchOnChange.GraphPosition;
            DynamicGraphModel.GraphJson = this.DynamicSearchOnChange.GraphJson2;
            DynamicGraphModel.GraphQueryColumn = this.dynamicSearchResult.GraphColumn2;
            DynamicGraphModel.IsAdditionalSummary = this.DynamicSearchOnChange.IsAdditionalSummary2;

            if (this.dynamicSearchResult.GraphJson2) {
                DynamicGraphModel.GraphQueryJson = JSON.parse(this.dynamicSearchResult.GraphJson2.ResultJson);
                this.othersInclude = JSON.parse(this.DynamicSearchOnChange.GraphJson2).othersInclude;
                this.showRatio = JSON.parse(this.DynamicSearchOnChange.GraphJson2).showRatio;
            }

            if (this.DynamicSearchOnChange.GraphType2 == "1") {
                if (this.dynamicSearchResult.GrpahAdditionalSummaryJson2) {
                    this.GraphSummaryJson = JSON.parse(this.dynamicSearchResult.GrpahAdditionalSummaryJson2.ResultJson);
                    this.GraphSummaryColumn = this.dynamicSearchResult.GrpahAdditionalSummaryColumn2;
                    this.GraphSummaryLength = this.GraphSummaryColumn.length;
                }
            }
            else if (this.showRatio && this.DynamicSearchOnChange.ChartType2 != "2") {
                this.dynamicSearchResult.GraphColumn2.push('Ratio');
                this.GraphSummaryColumn = this.dynamicSearchResult.GraphColumn2;
                this.GraphSummaryLength = this.GraphSummaryColumn.length;


                var summaryJson = JSON.parse(this.dynamicSearchResult.GraphJson2.ResultJson);

                var fieldName = this.dynamicSearchResult.GraphColumn2[1];
                summaryJson.forEach((data) => {
                    data.Ratio = ((parseInt(data[fieldName]) / parseInt(this.dynamicSearchResult.GraphJson2.Total)) * 100).toFixed(2).toString() + "%";
                })

                this.GraphSummaryJson = summaryJson;
            }
            else if (this.DynamicSearchOnChange.ChartType2 == "2") {
                this.GraphSummaryJson = JSON.parse(this.dynamicSearchResult.GraphJson2.StackSummaryJson);
                this.GraphSummaryColumn = this.dynamicSearchResult.GraphJson2.StackSummaryTableFields;
                this.GraphSummaryLength = this.GraphSummaryColumn.length;
            }
            else {
                this.GraphSummaryJson = JSON.parse(this.dynamicSearchResult.GraphJson2.ResultJson);
                this.GraphSummaryColumn = this.dynamicSearchResult.GraphColumn2;
                this.GraphSummaryLength = this.GraphSummaryColumn.length;
            }

            this.graphTitle = this.DynamicSearchOnChange.GraphName2;
            this.isAdditionalSummary = this.DynamicSearchOnChange.IsAdditionalSummary2;

            if (this.dynamicSearchResult.GrpahAdditionalSummaryJSon2) {
                DynamicGraphModel.GrpahAdditionalSummaryJson = JSON.parse(this.dynamicSearchResult.GrpahAdditionalSummaryJSon2.ResultJson);
                DynamicGraphModel.GrpahAdditionalSummaryColumn = this.dynamicSearchResult.GrpahAdditionalSummaryColumn2;

                this.AdditionalGraphSummaryColumn = this.dynamicSearchResult.GrpahAdditionalSummaryColumn2;
                this.AdditionalGraphSummaryJson = JSON.parse(this.dynamicSearchResult.GrpahAdditionalSummaryJSon2.ResultJson);
                this.AdditionalGraphSummaryLength = this.AdditionalGraphSummaryColumn.length;
            }
            this.graphDivId = "chartHolder1";
            //this.showGraph = true;
        }
        this.makeChart();
    }

    makeChart() {
        var that = this;
        if (DynamicGraphModel.GraphType == 1) {
            if (DynamicGraphModel.ChartType == 1) {
                that.makeBarChart(DynamicGraphModel, that.graphDivId);

            }
            else if (DynamicGraphModel.ChartType == 2) {
                that.makeStackBarChart(DynamicGraphModel, that.dynamicSearchResult, that.graphDivId);

            }
            else if (DynamicGraphModel.ChartType == 3) {
                that.MakePieChart(DynamicGraphModel, that.graphDivId);

            }
            else {
                that.makeDonutChart(DynamicGraphModel, that.graphDivId);

            }
        }

        else if (DynamicGraphModel.GraphType == 3) {
            if (DynamicGraphModel.ChartType == 1) {
                that.makeBarChart(DynamicGraphModel, that.graphDivId);

            }
            else if (DynamicGraphModel.ChartType == 2) {
                that.makeStackBarChart(DynamicGraphModel, that.dynamicSearchResult, that.graphDivId);

            }
            else if (DynamicGraphModel.ChartType == 3) {
                that.MakePieChart(DynamicGraphModel, that.graphDivId);

            }
            else {
                setTimeout(function () {
                    that.makeDonutChart(DynamicGraphModel, that.graphDivId);
                }, 100)
            }
        }
    }

    makeDonutChart(data: any, name: any) {
        var graphName = data.GraphName;
        var chartData = data.GraphQueryJson;
        var JsonData = JSON.parse(data.GraphJson);
        var legendPosition = JsonData.GraphLegendPosition.toLowerCase();
        var GraphXAxis = JsonData.GraphXAxis;
        var GraphYAxis = JsonData.GraphYAxis;
        var GraphAppearance = JsonData.GraphAppearance;
        var x = data.GraphQueryColumn[0];
        var y = data.GraphQueryColumn[1];

        AmCharts.addInitHandler(function (chart) {
            if (chart.legend === undefined || chart.legend.truncateLabels === undefined)
                return;

            // init fields
            var titleField = chart.titleField;
            var legendTitleField = chart.titleField + "Legend";

            // iterate through the data and create truncated label properties
            for (var i = 0; i < chart.dataProvider.length; i++) {
                var label = chart.dataProvider[i][chart.titleField];
                if (label.length > chart.legend.truncateLabels)
                    label = label.substr(0, chart.legend.truncateLabels - 1) + '...'
                chart.dataProvider[i][legendTitleField] = label;
            }

            // replace chart.titleField to show our own truncated field
            chart.titleField = legendTitleField;

            // make the balloonText use full title instead
            chart.balloonText = chart.balloonText.replace(/\[\[title\]\]/, "[[" + titleField + "]]");

        }, ["pie"]);

        var chart = AmCharts.makeChart(name, {
            "type": "pie",
            "theme": "light",
            "titles": [{
                "text": "",
                "size": 16
            }],
            "legend": {
                "generateFromData": true,
                "position": legendPosition,
                "autoMargins": false,
                "truncateLabels": 20
            },
            "dataProvider": chartData,
            "valueField": y,
            "titleField": x,
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": GraphAppearance == "2D" ? 0 : 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": GraphAppearance == "2D" ? 0 : 15,
            "export": {
                "enabled": true
            }
        });
    }

    makeStackBarChart(headerData: any, searchData: any, name: any) {
        var fieldsNames = searchData.GraphJson.StackSummaryFields;
        var catField = searchData.GraphColumn[0];
        var graphName = headerData.GraphName;
        var chartData = headerData.GraphQueryJson;
        var JsonData = JSON.parse(headerData.GraphJson);
        var legendPosition = JsonData.GraphLegendPosition.toLowerCase();
        var GraphXAxis = JsonData.GraphXAxis;
        var GraphYAxis = JsonData.GraphYAxis;
        var GraphAppearance = JsonData.GraphAppearance;



        var graphsFields: any = [];
        fieldsNames.forEach((val, index) => {
            let x = {
                "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
                "fillAlphas": 0.9,
                "fontSize": 11,
                "labelText": "[[percents]]%",
                "lineAlpha": 0.5,
                "title": val,
                "type": "column",
                "valueField": val,
                "fixedColumnWidth": 80
            };
            graphsFields.push(x);
        })

        var chart = AmCharts.makeChart(name, {
            "type": "serial",
            "theme": "light",
            "legend": {
                "generateFromData": true,
                "position": legendPosition
            },
            "dataProvider": chartData,
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0
            }],
            "graphs": graphsFields,
            "categoryField": catField,
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0.3,
                "gridAlpha": 0,
                "position": "left",
                "labelFunction": function (valueText, serialDataItem, categoryAxis) {
                    if (valueText.length > 15)
                        return valueText.substring(0, 15) + '...';
                    else
                        return valueText;
                }
            },
            "depth3D": GraphAppearance == "2D" ? 0 : 10,
            "angle": GraphAppearance == "2D" ? 0 : 15,
            "export": {
                "enabled": true
            }

        });
    }

    MakePieChart(data: any, name: any) {
        var graphName = data.GraphName;
        var chartData = data.GraphQueryJson;
        var JsonData = JSON.parse(data.GraphJson);
        var legendPosition = JsonData.GraphLegendPosition.toLowerCase();
        var GraphXAxis = JsonData.GraphXAxis;
        var GraphYAxis = JsonData.GraphYAxis;
        var GraphAppearance = JsonData.GraphAppearance;
        var x = data.GraphQueryColumn[0];
        var y = data.GraphQueryColumn[1];



        AmCharts.addInitHandler(function (chart) {
            if (chart.legend === undefined || chart.legend.truncateLabels === undefined)
                return;

            // init fields
            var titleField = chart.titleField;
            var legendTitleField = chart.titleField + "Legend";

            // iterate through the data and create truncated label properties
            for (var i = 0; i < chart.dataProvider.length; i++) {
                var label = chart.dataProvider[i][chart.titleField];
                if (label.length > chart.legend.truncateLabels)
                    label = label.substr(0, chart.legend.truncateLabels - 1) + '...'
                chart.dataProvider[i][legendTitleField] = label;
            }

            // replace chart.titleField to show our own truncated field
            chart.titleField = legendTitleField;

            // make the balloonText use full title instead
            chart.balloonText = chart.balloonText.replace(/\[\[title\]\]/, "[[" + titleField + "]]");

        }, ["pie"]);

        var chart = AmCharts.makeChart(name, {
            "type": "pie",
            "theme": "light",
            "titles": [{
                "text": "",
                "size": 16
            }],
            "dataProvider": chartData,
            "legend": {
                "generateFromData": true,
                "position": legendPosition,
                "marginRight": 80,
                "autoMargins": false,
                "truncateLabels": 20
            },
            "valueField": y,
            "titleField": x,
            "outlineAlpha": 0.4,
            "depth3D": GraphAppearance == "2D" ? 0 : 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": GraphAppearance == "2D" ? 0 : 30,
            "export": {
                "enabled": true
            }
        });
    }


    makeBarChart(data: any, name: any) {
        var graphName = data.GraphName;
        var chartData;
        if (data.GraphQueryJson.length > 20) {
            chartData = data.GraphQueryJson.slice(0, 20);
        } else {
            chartData = data.GraphQueryJson;
        }
        var JsonData = JSON.parse(data.GraphJson);
        var legendPosition = JsonData.GraphLegendPosition.toLowerCase();
        var GraphXAxis = JsonData.GraphXAxis;
        var GraphYAxis = JsonData.GraphYAxis;
        var GraphAppearance = JsonData.GraphAppearance;
        var x = data.GraphQueryColumn[1];
        var y = data.GraphQueryColumn[2];

        AmCharts.addInitHandler(function (chart) {
            if (chart.legend === undefined || chart.legend.truncateLabels === undefined)
                return;

            // init fields
            var titleField = chart.titleField;
            var legendTitleField = chart.titleField + "Legend";

            // iterate through the data and create truncated label properties
            for (var i = 0; i < chart.dataProvider.length; i++) {
                var label = chart.dataProvider[i][chart.titleField];
                if (label.length > chart.legend.truncateLabels)
                    label = label.substr(0, chart.legend.truncateLabels - 1) + '...'
                chart.dataProvider[i][legendTitleField] = label;
            }

            // replace chart.titleField to show our own truncated field
            chart.titleField = legendTitleField;

            // make the balloonText use full title instead
            chart.balloonText = chart.balloonText.replace(/\[\[title\]\]/, "[[" + titleField + "]]");

        }, ["pie"]);

        this.barChart = AmCharts.makeChart(name, {
            "type": "serial",
            "theme": "light",
            "marginRight": 70,
            "dataProvider": chartData,
            "legend": {
                "generateFromData": true,
                "position": legendPosition,
                "autoMargins": false,
                "truncateLabels": 20
            },
            "valueAxes": [{
                "axisAlpha": 0.3,
                "position": "left",
                "title": GraphXAxis
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<b>[[category]]: [[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": y,
                "fixedColumnWidth": 50
            }],
            "depth3D": GraphAppearance == "2D" ? 0 : 20,
            "angle": GraphAppearance == "2D" ? 0 : 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": x,
            "categoryAxis": {
                "gridPosition": "start",
                "title": GraphYAxis,
                "labelRotation": 45,
                "labelFunction": function (valueText, serialDataItem, categoryAxis) {
                    if (valueText.length > 15)
                        return valueText.substring(0, 15) + '...';
                    else
                        return valueText;
                }
            },
            "export": {
                "enabled": true
            }

        });
    }

    showSubstanceDetail(index: number) {
        if (this.isSubstanceSelected) {
            this.selectedId = parseInt(this.GraphSummaryJson[index]['Substance ID']);
        } else {
            this.scoredByType = 1;
            this.selectedId = parseInt(this.VendorGraphSummary[index]["ApplicantID"]);
        }
        var substanceDetailObject = {
            "Id": this.selectedId,
            "ClientIds": this.CompanyList,
            "Type": this.scoredByType,
            "SearchText": this.DynamicSearch,
        }
        this.dataService.post('ManageGraph/GetTestSubstancePopupData', substanceDetailObject).subscribe(
            res => {
                if (res.IsSuccess) {
                    this.substanceDateList = res.Data;
                    (<any>$('#substanceDetails')).modal('show');
                }
            },
            err => {

            });
    }

    changeSummary() {
        this.recordDisplay = 0;
        if (this.isSubstanceSelected) {
            this.isSubstanceSelected = false;
            this.scoredByType = 1;
            this.GraphSummaryJson = this.GraphSummaryCopy.slice();
            this.makeBarChartVendor(DynamicGraphModel, this.graphDivId);
        } else {
            this.isSubstanceSelected = true;
            this.scoredByType = 0;
            this.VendorGraphSummary = this.VendorGraphSummaryCopy.slice();
            this.makeBarChart(DynamicGraphModel, this.graphDivId);
        }
    }

    selectSize() {
        if (this.isSubstanceSelected) {
            var overallSummary = this.GraphSummaryCopy.slice();
            if (this.recordDisplay == 0) {
                this.GraphSummaryJson = overallSummary.slice();
            } else {
                this.GraphSummaryJson = overallSummary.slice(0, this.recordDisplay);
            }
        } else {
            var overallVendorSummary = this.VendorGraphSummaryCopy.slice();
            if (this.recordDisplay == 0) {
                this.VendorGraphSummary = overallVendorSummary.slice();
            } else {
                this.VendorGraphSummary = overallVendorSummary.slice(0, this.recordDisplay);
            }
        }
    }


    makeBarChartVendor(data: any, name: any) {
        var graphName = data.GraphName;
        var chartData;
        if (data.VendorQueryJson.length > 20) {
            chartData = data.VendorQueryJson.slice(0, 20);
        } else {
            chartData = data.VendorQueryJson;
        }
        var JsonData = JSON.parse(data.GraphJson);
        var legendPosition = JsonData.GraphLegendPosition.toLowerCase();
        var GraphXAxis = JsonData.GraphXAxis;
        var GraphYAxis = JsonData.GraphYAxis;
        var GraphAppearance = JsonData.GraphAppearance;
        var x = data.VendorQueryColumn[1];
        var y = data.VendorQueryColumn[2];

        AmCharts.addInitHandler(function (chart) {
            if (chart.legend === undefined || chart.legend.truncateLabels === undefined)
                return;

            // init fields
            var titleField = chart.titleField;
            var legendTitleField = chart.titleField + "Legend";

            // iterate through the data and create truncated label properties
            for (var i = 0; i < chart.dataProvider.length; i++) {
                var label = chart.dataProvider[i][chart.titleField];
                if (label.length > chart.legend.truncateLabels)
                    label = label.substr(0, chart.legend.truncateLabels - 1) + '...'
                chart.dataProvider[i][legendTitleField] = label;
            }

            // replace chart.titleField to show our own truncated field
            chart.titleField = legendTitleField;

            // make the balloonText use full title instead
            chart.balloonText = chart.balloonText.replace(/\[\[title\]\]/, "[[" + titleField + "]]");

        }, ["pie"]);

        this.vendorBarChart = AmCharts.makeChart(name, {
            "type": "serial",
            "theme": "light",
            "marginRight": 70,
            "dataProvider": chartData,
            "legend": {
                "generateFromData": true,
                "position": legendPosition,
                "autoMargins": false,
                "truncateLabels": 20
            },
            "valueAxes": [{
                "axisAlpha": 0.3,
                "position": "left",
                "title": GraphXAxis
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<b>[[category]]: [[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": y,
                "fixedColumnWidth": 50
            }],
            "depth3D": GraphAppearance == "2D" ? 0 : 20,
            "angle": GraphAppearance == "2D" ? 0 : 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": x,
            "categoryAxis": {
                "gridPosition": "start",
                "title": GraphYAxis,
                "labelRotation": 45,
                "labelFunction": function (valueText, serialDataItem, categoryAxis) {
                    if (valueText.length > 15)
                        return valueText.substring(0, 15) + '...';
                    else
                        return valueText;
                }
            },
            "export": {
                "enabled": true
            }

        });
    }

    exportToExcelTestSubstanceOld() {
        this.isLoader = false;
        var exportToExcelTestSubstance = {
            "Id": 0,
            "ClientIds": this.CompanyList,
            "Type": this.scoredByType,
            "SearchText": this.DynamicSearch,
        }
        this.dataService.post('ManageGraph/TestSubstanceExportToExcel', exportToExcelTestSubstance).subscribe(r => {
            if (r.IsSuccess) {
                this.isLoader = false;
                let downloadFileName = encodeURIComponent(r.Data);
                window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + r.Data + "&fileType=excel&businessCategoryId="+this.userCredential.BusinessCategoryId;
            }
        }, err => {
            this.isLoader = false;
        });
    }

    exportToExcelTestSubstance(ImgID: any, chartName: any) {
        this.isLoader = true;
        var that = this;
        var data ={'Base': ''};
        for (let i = 0; i < ImgID.length; i++) {
            var tmp = new AmCharts.AmExport(this[chartName[i]]);
            tmp.init();
            tmp.output({
                output: 'datastring',
                format: 'jpg'
            }, function (blob) {
                var me = blob.split('base64,');               
                data['Base'] = me[1];
            });
        }
        that.TestSubstanceExcelParam.Id =0;
        that.TestSubstanceExcelParam.ClientIds=that.CompanyList;
        that.TestSubstanceExcelParam.Type=that.scoredByType;
        that.TestSubstanceExcelParam.SearchText=that.DynamicSearch.SearchText;
        that.TestSubstanceExcelParam.ImageData=data['Base'];
 
        that.dataService.post('GraphAnalysisExportToExcel/TestSubstanceOrderAnalysisExportToExcel', that.TestSubstanceExcelParam).subscribe(r => {
            if (r.IsSuccess) {
                that.isLoader = false;
                let downloadFileName = encodeURIComponent(r.Data);
                window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + r.Data + "&fileType=excel&businessCategoryId="+that.userCredential.BusinessCategoryId;
            }
        }, err => {
            that.isLoader = false;
        });

    }

}