import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicReviewRoutingModule } from './public-review-routing.module';
import { PublicReviewComponent } from './public-review.component';
import { FormsModule } from '@angular/forms';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import {Ng2PaginationModule} from 'ng2-pagination';
import { HeaderModule } from 'app/header/header.module';
import { FormatBusinessCategoryModule } from 'app/helper/format-business-category/format-business-category.module';
@NgModule({
  imports: [
    CommonModule,
    PublicReviewRoutingModule,
    FormsModule,
    Ng2AutoCompleteModule,
    Ng2PaginationModule,
    FormatBusinessCategoryModule,
    HeaderModule
  ],
  declarations: [PublicReviewComponent]
})
export class PublicReviewModule { }
