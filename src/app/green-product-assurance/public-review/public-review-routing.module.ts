import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicReviewComponent } from 'app/green-product-assurance/public-review/public-review.component';

const routes: Routes = [{
  path:'',
  component:PublicReviewComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicReviewRoutingModule { }
