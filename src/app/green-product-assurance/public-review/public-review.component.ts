import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../../mts.service';
import { ApplicationListSearchRequest } from 'app/green-product-assurance/internal-review/internal-review.model';
import { ApplicationInformation } from 'app/green-product-assurance/new-application/new-application.model';
declare let swal: any;
@Component({
  selector: 'app-public-review',
  templateUrl: './public-review.component.html',
  styleUrls: ['./public-review.component.css']
})
export class PublicReviewComponent implements OnInit, OnDestroy {
  private pageNumber: number = 1;
  private totalItem: any;
  isLoader: boolean = false;
  userCredential: any;
  userDetail: any;
  searchModal: any;
  isLoading: boolean;
  serchList: any = [];
  companyList: any;
  productList: any;
  modelList: any;
  certificateList: any;
  ProductSealNoList: any;
  public selectedCompany: any = "";
  public selectedProduct: any = "";
  public selectedModel: any = "";
  public selectedCertificate: any = "";
  public selectedSealNo: any = "";
  applicationListSearchRequest: ApplicationListSearchRequest;
  allApplicationInfo: Array<ApplicationInformation>;

  constructor(private dataService: DataService) {
    this.applicationListSearchRequest = new ApplicationListSearchRequest();
    this.allApplicationInfo=new  Array<ApplicationInformation>();
  }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.isLoader=true;
    this.dataService
    .getOrder(
    'GP_VendorReview/GetInformationList')
    .subscribe(r => {
     this.companyList = r.InformationList.CompanyList;
     this.productList = r.InformationList.ProductList;
     this.modelList = r.InformationList.ModelList;
     this.certificateList = r.InformationList.CertificateList;
     this.ProductSealNoList = r.InformationList.ProductSealNoList;

    });

    this.searchApplicationList();
  }

  public renderCertificate(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CertificateNumber}</div>
      </div>`;

    return html;
  }

  public renderSealNO(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.ProductSealNo}</div>
      </div>`;

    return html;
  }

  public renderCompanyName(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.VendorName}</div>
      </div>`;

    return html;
  }

  public renderProductName(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.ProductName}</div>
      </div>`;

    return html;
  }

  public renderModelNo(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.ProductModelNo}</div>
      </div>`;

    return html;
  }



  searchApplicationList() {
    this.applicationListSearchRequest.ProductName=this.selectedProduct.ProductName;
    this.applicationListSearchRequest.ProductModelNo=this.selectedModel.ProductModelNo;
    this.applicationListSearchRequest.VendorName=this.selectedCompany.VendorName;
    this.applicationListSearchRequest.CertificateNumber=this.selectedCertificate.CertificateNumber;
    this.applicationListSearchRequest.ProductSealNo=this.selectedSealNo.ProductSealNo;
    this.applicationListSearchRequest.ApplicationStatus="Approved"
    this.isLoader=true;
    this.dataService.signUpPost('GP_PublicReview/GetAllApplication', this.applicationListSearchRequest)
      .subscribe(response => {
        let res = JSON.parse(response);
        this.isLoader = false;
        if (res.IsSuccess) {
          this.allApplicationInfo = res.Data;
          this.totalItem = res.TotalRecords;
        } else {
          this.isLoader = false;
          swal('', res.Message);
        }
      });
  }

  changePageNumber(page: number) {
    this.applicationListSearchRequest.PageNumber = page;
    this.pageNumber=page;
    this.searchApplicationList();
}

  resetSearchForm() {
    this.selectedCompany = '';
    this.selectedProduct = '';
    this.selectedModel = '';
    this.selectedCertificate = '';
    this.selectedSealNo = '';
    this.pageNumber=1;
    this.searchApplicationList();
  }
  ngOnDestroy() { }

}
