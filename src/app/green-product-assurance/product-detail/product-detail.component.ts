import { Component, OnInit } from '@angular/core';
import { CompanyDetail } from '../vendor-view/see-detail/see-detail.model';
import { Router, Params, ActivatedRoute } from "@angular/router";
import { DataService, BaseUrl, SealLogoBaseUrl } from 'app/mts.service';
import { ApplicationInformation, ApplicationProductDetail, ProductDocument } from 'app/green-product-assurance/new-application/new-application.model';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  activeApplication: any;
  productDetail: ApplicationProductDetail;
  ProductsImages: Array<ProductDocument>;
  sealLogoUrl: string;
  selectedImage: string;
  isLoader: boolean = false;
  Id: string;
  constructor(private activatedRoute: ActivatedRoute,
    private dataService: DataService) {
    this.productDetail = new ApplicationProductDetail();
    this.ProductsImages = new Array<ProductDocument>();
    this.sealLogoUrl = SealLogoBaseUrl;
  }

  ngOnInit() {
    let that = this;
    this.activatedRoute.params.subscribe((params: Params) => {
      that.Id = params["ProductSealNo"];
    })
    this.getApplicationInfo();
  }

  imageSelecton(i) {
    this.selectedImage = this.sealLogoUrl + '/' + this.activeApplication.ApplicationReferenceNo + '/Products/' + this.productDetail.ProductFolderName + '/ProductImage/' + this.ProductsImages[i].DocumentName;
  }

  getApplicationInfo() {
    this.isLoader = true;
    this.dataService
      .getOrder(
      'GP_VendorReview/GetProductDetail?ProductSealNo=' + this.Id)
      .subscribe(r => {
        let res = JSON.parse(r);
        this.activeApplication = res.Data[0];
        this.validateValidity();
        this.productDetail = this.activeApplication.Products[0];
        this.ProductsImages = this.productDetail.ProductDocuments.filter(i => i.DocumentType == 'ProductImage');
        this.selectedImage = this.sealLogoUrl + '/' + this.activeApplication.ApplicationReferenceNo + '/Products/' + this.productDetail.ProductFolderName + '/ProductImage/' + this.ProductsImages[0].DocumentName;
        this.isLoader = false;
      });
  }

  validateValidity() {
    let expiredDate = new Date(this.activeApplication.CertificateExpiryDate);
    let today = new Date();
    if (expiredDate >= today) {
      this.activeApplication.Validity = true;
    } else {
      this.activeApplication.Validity = false;
    }
  }


  
}
