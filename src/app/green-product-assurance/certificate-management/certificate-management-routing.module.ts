import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CertificateManagementComponent } from 'app/green-product-assurance/certificate-management/certificate-management.component';

const routes: Routes = [{
  path:'',
  component:CertificateManagementComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificateManagementRoutingModule { }
