import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificateManagementRoutingModule } from './certificate-management-routing.module';
import { CertificateManagementComponent } from './certificate-management.component';
import { FormsModule } from '@angular/forms';
import {TooltipModule} from "ngx-tooltip";
import {DateTimePickerModule} from 'ng-pick-datetime';
import { HeaderModule } from 'app/header/header.module';
import { Ng2PaginationModule } from 'ng2-pagination';
import { FormatBusinessCategoryModule } from 'app/helper/format-business-category/format-business-category.module';


@NgModule({
  imports: [
    CommonModule,
    CertificateManagementRoutingModule,
    FormsModule,
    TooltipModule,
    HeaderModule,
    DateTimePickerModule,
    Ng2PaginationModule,
    FormatBusinessCategoryModule
  ],
  declarations: [CertificateManagementComponent]
})
export class CertificateManagementModule { }
