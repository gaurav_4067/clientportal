import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { BaseUrl, DataService } from 'app/mts.service';
import { ApplicationInformation, ReviewReportDetail } from '../new-application/new-application.model';
import { ApplicationListSearchRequest } from 'app/green-product-assurance/internal-review/internal-review.model';

declare let swal: any;

@Component({
  selector: 'app-certificate-management',
  templateUrl: './certificate-management.component.html',
  styleUrls: ['./certificate-management.component.css']
})
export class CertificateManagementComponent implements OnInit {
  LogRecoredIndex: number;
  reportServiceCode: string;
  isLoader: boolean = false;
  View: boolean = false;;
  userCredential: any;
  userDetail: any;
  private pageNumber: number = 1;
  private totalItem: any;
  applicationInformation: ApplicationInformation;
  allApplicationInfo: Array<ApplicationInformation>;
  applicationListSearchRequest: ApplicationListSearchRequest;
  RevisedApplicationList: Array<ApplicationInformation>;
  reviewReportDetails: Array<ReviewReportDetail>;
  DetailId: string;


  constructor(private router: Router,
    private dataService: DataService,
    private http: Http
  ) {
    this.applicationInformation = new ApplicationInformation();
    this.allApplicationInfo = new Array<ApplicationInformation>();
    this.applicationListSearchRequest = new ApplicationListSearchRequest();
  }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));

    if (this.userDetail && this.userCredential) {
      if (this.userCredential.UserPermission.GPCertificateManagementView) {

        if (window.location.href.includes('green-product-assurance/application-detail')) {
          var Url = window.location.href;
          var data = window.location.href.split('/');
          this.DetailId = data[data.length - 1];
          this.View = true;
          this.isLoader = true;
          this.dataService.get('GP_ApplicationList/GetApplicationInfo/' + this.DetailId)
            .subscribe(response => {
              let res = JSON.parse(response);
                this.applicationInformation =res[0];
                this.isLoader = false;
            });
        } else {
          this.applicationListSearchRequest = new ApplicationListSearchRequest();
          this.searchApplicationList();
        }

      } else {
        this.router.navigate(['./landing-page']);
      }
    } else {
      this.router.navigate(['./login']);
    }
  }

  searchApplicationList() {
    this.isLoader = true;
    this.dataService.post('GP_ApplicationList/GetAllApplication', this.applicationListSearchRequest)
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          this.allApplicationInfo = res.Data;
          this.isLoader = false;
          this.totalItem = res.TotalRecords;
        } else {
          this.isLoader = false;
          swal('', res.Message);
        }
      });
  }

  resetSearchParam() {
    this.applicationListSearchRequest = new ApplicationListSearchRequest();
    this.allApplicationInfo = new Array<ApplicationInformation>();
    this.pageNumber = 1;
    this.searchApplicationList();
  }

  getClass(value: string) {
    let result;
    if (value == 'Approved')
      return result = 'Approve';

    else if (value == 'Registered' || value == 'Revised' || value == 'Renewed' || value == 'Expired')
      return result = 'Registered';

    if (value == 'In Progress' || value == 'Observation')
      return result = 'InProgress';

    if (value == 'Document Verified')
      return result = 'DocVerfied';

    if (value == 'ST Verified' || value == 'FA Verified')
      return result = 'STVerified';

    if (value == 'Technical Verified')
      return result = 'TechVerfied';

    if (value == 'Rejected' || value == 'Cancelled' || value == 'Terminated' || value == 'Suspended')
      return result = 'Cancelled';
  }

  validateMinDate(MyDate) {
    let TempDate = new Date(MyDate);
    let temp = TempDate.getFullYear();
    if (temp > 1) {
      return MyDate
    } else {
      return ''
    }
  }

  CreateReviseApplication(ReferenceNo, Type) {
    this.isLoader = true;
    let obj = {
      CreatedBy: this.userCredential.UserId,
      ApplicationReferenceNo: ReferenceNo,
      ApplicationType: Type,
    }
    this.dataService.post('GP_CertificateManagement/CreateReviseApplication', obj)
      .subscribe(response => {
        this.isLoader = false;
        let res = JSON.parse(response);
        console.log("res", res)
        let that = this;
        if (res.IsSuccess) {
          swal({
            title: "",
            text: "<span style='color:#797979;font-size:16px;'>" + "Application has successfully submited for " + res.Data.ApplicationType + " certificate " + "</span><br>"
              + "<span style='color:#787878;font-weight: 600;font-size:15px;'> " + res.Data.ApplicationType + "  Application Reference No. :</span><span style='color:#797979;font-weight: 600;font-size:15px;'> <b> " + " " + res.Data.ApplicationReferenceNo + "</b></span><br>",
            html: true,
            confirmButtonText: "OK",
            closeOnConfirm: true,
          }, function (isConfirm) {
            if (isConfirm) {
              that.searchApplicationList();
            }
          }
          );
        } else {
          that.isLoader = false;
          swal('', res.Message);
        }
      });
  }

  changePageNumber(page: number) {
    this.applicationListSearchRequest.PageNumber = page;
    this.pageNumber = page;
    this.searchApplicationList();
  }

  ViewCertificateHistory(ApplicationReferenceNo) {
    this.RevisedApplicationList =new Array<ApplicationInformation>();
    this.isLoader = true;
    this.dataService.get('GP_CertificateManagement/CertificateRevisionHistory/' + ApplicationReferenceNo)
      .subscribe(response => {
        this.isLoader = false;
        let res = JSON.parse(response);
        this.RevisedApplicationList = res.Data;
      });
    (<any>$('#CertificateHistory')).modal('show');
  }

  enableButton(CalledFrom: string, application: ApplicationInformation) {
    let result = false;

      if (application.ApplicationStatus == 'Approved' && application.RevisedApplicationCount==0) {
        let currentDate = new Date();
        if (CalledFrom == "Revised" && this.userCredential.UserPermission.ReviseGPApplication) {
          if (application.RevisedDate >= application.CurrentDate) {    
            result = true;
          }
        } else if (CalledFrom == "Renewed" && this.userCredential.UserPermission.RenewGPApplication) {
            if (application.CurrentDate >= application.RenewDate) {
              result = true;
            }
        }
      }
    return result;
  }

  //************methods for Application-Detail*****************

  viewDetail(id: string) {
    var link = window.location.href.split('/#/')[0];
    window.open(link + '/#/green-product-assurance/application-detail/' + id);
  }

  ViewReportLog(type: string, Temp, serviceType, index) {
    if (serviceType == 'ST') {
      this.reportServiceCode = Temp.ProductFolderName;
    }
    else if (serviceType == 'FA') {
      this.reportServiceCode = Temp.FactoryFolderName;
    }
    this.reviewReportDetails = Temp.ReviewReports.filter(i => i.ServiceType == serviceType);
    this.LogRecoredIndex = index;
    (<any>$('#LogRecord')).modal('show');
  }

  downloadReviewReports(reviewReport) {
    let service;
    if (reviewReport.ServiceType == 'ST') {
      service = 'Products'
    } else if (reviewReport.ServiceType == 'FA') {
      service = 'Factories'
    } else {
      service = 'Application'
    }
    if (!this.reportServiceCode) {
      this.reportServiceCode = '';
    }
    let downloadFileName = encodeURIComponent(reviewReport.FileName.trim());
    window.location.href = BaseUrl + "GP_ApplicationList/DownloadReviewReports?ApplicationReferenceNo="
      + this.applicationInformation.ApplicationReferenceNo.trim() + "&ServiceType=" + service.trim() + "&ModelNo="
      + this.reportServiceCode.trim() + "&DocumentName=" + downloadFileName.trim() + "&DocumentType=" + reviewReport.ServiceType.trim()
      + "&ReportType=" + reviewReport.ReportType.trim();
  }

  downloadDocument(docName, docType, serviceType, modelNo, RefNo) {
    let downloadFileName = encodeURIComponent(docName.trim());
    window.location.href = BaseUrl + "GP_ApplicationList/DownloadDocument?ApplicationReferenceNo="
      + this.applicationInformation.ApplicationReferenceNo.trim() + "&ServiceType=" + serviceType.trim() + "&ModelNo="
      + modelNo.trim() + "&DocumentName=" + downloadFileName.trim() + "&DocumentType=" + docType.trim();
  }

  downloadFilledApplicationDocument(docName, RefNo) {
    let serviceType = 'FilledApplicationForm'
    let downloadFileName = encodeURIComponent(docName.trim());
    window.location.href = BaseUrl + "GP_ApplicationList/DownloadFilledApplicationDocument?ApplicationReferenceNo="
      + RefNo.trim() + "&ServiceType=" + serviceType.trim() + "&DocumentName=" + downloadFileName.trim();
  }

}
