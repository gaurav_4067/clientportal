import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorViewRoutingModule } from './vendor-view-routing.module';
import { VendorViewComponent } from './vendor-view.component';
import { FormsModule } from '@angular/forms';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import {Ng2PaginationModule} from 'ng2-pagination';
import { HeaderModule } from 'app/header/header.module';
import { FormatBusinessCategoryModule } from 'app/helper/format-business-category/format-business-category.module';

@NgModule({
  imports: [
    CommonModule,
    VendorViewRoutingModule,
    FormsModule,
    Ng2AutoCompleteModule,
    Ng2PaginationModule,
    HeaderModule,
    FormatBusinessCategoryModule
  ],
  declarations: [VendorViewComponent]
})
export class VendorViewModule { }
