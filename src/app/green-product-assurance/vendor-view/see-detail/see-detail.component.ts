import { Component, OnInit } from '@angular/core';
import { CompanyDetail } from './see-detail.model';
import { DataService,BaseUrl, SealLogoBaseUrl } from 'app/mts.service';
import { ActivatedRoute } from '@angular/router';
import { ApplicationInformation, ApplicationProductDetail,ProductDocument } from 'app/green-product-assurance/new-application/new-application.model';
import { Http, Response, Headers, RequestOptions } from '@angular/http';


@Component({
  selector: 'app-see-detail',
  templateUrl: './see-detail.component.html',
  styleUrls: ['./see-detail.component.css']
})
export class SeeDetailComponent implements OnInit {
  isLoader: boolean = false;
  imageName: string;
  applicationId: number;
  product: ApplicationProductDetail;
  selectedImage: string;
  selectedProductsImage: Array<ProductDocument>;
  URl: string
  private sub: any;
  activeApplication: ApplicationInformation;
  sealLogoUrl: any;
  userCredential: any;
  userDetail: any;
  showMap:boolean=false;
  ipCountry:string

  constructor(private dataService: DataService,private http:Http,
    private activatedRoute: ActivatedRoute) {
    this.activeApplication = new ApplicationInformation();
    this.product = new ApplicationProductDetail();
    this.selectedProductsImage= new Array<ProductDocument>();
    this.sealLogoUrl = SealLogoBaseUrl;
  }

  ngOnInit() {
    this.http.get('https://api.userinfo.io/userinfos')
    .subscribe( result => {
      let data = result.json();
      console.log(data.country)
      this.ipCountry=data.country.code  
      if(this.ipCountry == 'CN'){
        this.showMap=false; 
     }else{
       this.showMap=true;
     }
    })
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.applicationId = +params['id']; // (+) converts string 'id' to a number

      // In a real app: dispatch action to load the details here.
    });
    this.getApplicationInfo();
    
  }

  getApplicationInfo() {
    this.isLoader=true;
    this.dataService.get('GP_ApplicationList/GetApplicationInfo/' + this.applicationId)
      .subscribe(result => {
        let res = JSON.parse(result);
        this.activeApplication = res[0];
        this.isLoader=false;
      });
  }

  viewProductDetail(selectedProduct) {
    this.product = selectedProduct;
    (<any>$('#ViewProductDetail')).modal('show');
    var link = window.location.href.split('/#/')[0];
    this.selectedProductsImage = this.product.ProductDocuments.filter(i => i.DocumentType == 'ProductImage');
    this.URl = link + '/#/green-product-assurance/product-info/' + this.product.ProductSealNo;
    this.selectedImage = this.sealLogoUrl + '/' + this.activeApplication.ApplicationReferenceNo + '/Products/' + this.product.ProductFolderName + '/ProductImage/' + this.selectedProductsImage[0].DocumentName;
  }

  imageSelecton(imageId) {
    this.selectedImage = this.sealLogoUrl + '/' + this.activeApplication.ApplicationReferenceNo + '/Products/' + this.product.ProductFolderName + '/ProductImage/' + this.selectedProductsImage[imageId].DocumentName;
  }

  viewSealLogo(logoName){
    if(logoName){
    this.imageName=logoName;
    (<any>$('#SealLogoModal')).modal('show');
    }
  }

  downloadDocument(docName, Type) {
    let downloadFileName = encodeURIComponent(docName.trim());
    window.location.href = BaseUrl + "GP_VendorReview/DownloadDocument?ApplicationReferenceNo="
      + this.activeApplication.ApplicationReferenceNo.trim() + "&DocumentName=" + downloadFileName.trim() + "&ServiceType=" + Type.trim();
  }

  validateValidity(MyDate) {
    let expiredDate = new Date(MyDate);
    let today = new Date();
    if (expiredDate >= today) {
     return 'Valid'
    } else {
      return 'Expired'
    }
  }


  validateMinDate(MyDate) {
    let TempDate = new Date(MyDate);
    let temp = TempDate.getFullYear();
    if (temp > 1) {
     return MyDate
    } else {
      return ''
    }
  }

}
