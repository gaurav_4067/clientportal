import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SeeDetailComponent } from 'app/green-product-assurance/vendor-view/see-detail/see-detail.component';

const routes: Routes = [{
  path:'',
  component:SeeDetailComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeeDetailRoutingModule { }
