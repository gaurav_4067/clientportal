
// export class CertificateDetail {
//     public CompanyName:string;
//     public ProductName:string;
//     public ModelNumber:string;
//     public CertificateNumber:string;
// }

export class CompanyDetail {
    public CompanyName:string;
    public CompanyAddress:string;
    public CompanyState:string;
    public CompanyCountry:string;
    public CompanyCity:string;
    public Email:string;
    public Contact:string;
    public Phone:string;
}

// export class CompanyProduct{
// constructor(){
//     this.ModelNumbers = new Array<ModelNumbers>();
// }
//     public ProductName:string;
//     public ModelNumber:string;
//     public CertificateNumber:string;
//     public ExpireDate:string;
//     public Validity:boolean;
//     public ProductImage:string;
//     public ProductImages:Array<string>;
//     public ModelNumbers:Array<ModelNumbers>;
// }
// export class ModelNumbers {
//     public Color:string;
//     public Id:string;
// }