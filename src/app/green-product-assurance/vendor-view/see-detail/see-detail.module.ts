import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SeeDetailRoutingModule } from './see-detail-routing.module';
import { SeeDetailComponent } from './see-detail.component';
import { HeaderModule } from 'app/header/header.module';
import { FormatBusinessCategoryModule } from 'app/helper/format-business-category/format-business-category.module';


@NgModule({
  imports: [
    CommonModule,
    SeeDetailRoutingModule,
    HeaderModule,
    FormatBusinessCategoryModule
  ],
  declarations: [SeeDetailComponent]
})
export class SeeDetailModule { }
