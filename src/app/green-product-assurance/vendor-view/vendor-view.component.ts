import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../mts.service';
import { ApplicationListSearchRequest } from 'app/green-product-assurance/internal-review/internal-review.model';
import { ApplicationInformation } from 'app/green-product-assurance/new-application/new-application.model';
declare let swal: any;

@Component({
  selector: 'app-vendor-view',
  templateUrl: './vendor-view.component.html',
  styleUrls: ['./vendor-view.component.css']
})
export class VendorViewComponent implements OnInit {
  private pageNumber: number = 1;
  private totalItem: any;
  isLoader: boolean = false;
  userCredential: any;
  userDetail: any;
  View: boolean = false;


  public vendoruser: any = {
    VendorName: '',
    ProductName: '',
    ProductModelNo: '',
    CertificateNumber: '',
    CertificateIssuedDate: '',
    CertificateExpiryDate: '',
    ApplicationStatus: '',
    ProductSealNo: ''
  };
  searchModal: any;
  isLoading: boolean;
  serchList: any = [];
  companyList: any;
  productList: any;
  modelList: any;
  certificateList: any;
  public selectedCompany: any = "";
  public selectedProduct: any = "";
  public selectedModel: any = "";
  public selectedCertificate: any = "";
  applicationListSearchRequest: ApplicationListSearchRequest;
  allApplicationInfo: Array<ApplicationInformation>;
  constructor(private router: Router, private dataService: DataService,
  ) {
    this.applicationListSearchRequest = new ApplicationListSearchRequest()
  }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    if (this.userDetail && this.userCredential) {
      if (this.userCredential.UserPermission.GPVendorView) {
        this.isLoader = true;
        this.dataService
          .get(
          'GP_VendorReview/GetInformationList')
          .subscribe(r => {
            this.companyList = r.InformationList.CompanyList;
            this.productList = r.InformationList.ProductList;
            this.modelList = r.InformationList.ModelList;
            this.certificateList = r.InformationList.CertificateList;
          });

          this.searchApplicationList();
      } else {
        this.router.navigate(['./landing-page']);
      }
    } else {
      this.router.navigate(['./login']);
    }
  }

  searchApplicationList() {
    this.View = false;
    this.applicationListSearchRequest.ProductName=this.selectedProduct.ProductName;
    this.applicationListSearchRequest.ProductModelNo=this.selectedModel.ProductModelNo;
    this.applicationListSearchRequest.VendorName=this.selectedCompany.VendorName;
    this.applicationListSearchRequest.CertificateNumber=this.selectedCertificate.CertificateNumber;
    this.isLoader=true;
    this.dataService.post('GP_ApplicationList/GetAllApplication', this.applicationListSearchRequest)
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          this.isLoader = false;
          this.allApplicationInfo = res.Data;
          this.totalItem = res.TotalRecords
        } else {
          this.isLoader = false;
          swal('', res.Message);
        }
      });
  }

  changePageNumber(page: number) {
    this.applicationListSearchRequest.PageNumber = page;
    this.pageNumber=page;
    this.searchApplicationList();
}


  public renderCertificate(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CertificateNumber}</div>
      </div>`;
    return html;
  }

  public renderCompanyName(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.VendorName}</div>
      </div>`;
    return html;
  }

  public renderProductName(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.ProductName}</div>
      </div>`;
    return html;
  }

  public renderModelNo(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.ProductModelNo}</div>
      </div>`;
    return html;
  }


  resetSearchForm() {
    this.selectedCompany = '';
    this.selectedProduct = '';
    this.selectedModel = '';
    this.selectedCertificate = '';
    this.pageNumber=1;
    this.searchApplicationList();
  }
  SeeDetail(applicationId) {
    this.router.navigate(['/green-product-assurance/vendor-view/see-detail/'+ applicationId]);
  }

  getClass(value: string) {
    let result;
    if (value == 'Approved')
      return result = 'Approve';

    else if (value == 'Registered' || value == 'Revised' || value == 'Renewed' || value == 'Expired')
      return result = 'Registered';

    if (value == 'In Progress' || value == 'Observation')
      return result = 'InProgress';

    if (value == 'Document Verified')
      return result = 'DocVerfied';

    if (value == 'ST Verified' || value == 'FA Verified')
      return result = 'STVerified';

    if (value == 'Technical Verified')
      return result = 'TechVerfied';

    if (value == 'Rejected' || value == 'Cancelled' || value == 'Terminated' || value == 'Suspended')
      return result = 'Rejected';
  }
  
    validateMinDate(MyDate) {
      let TempDate = new Date(MyDate);
      let temp = TempDate.getFullYear();
      if (temp > 1) {
       return MyDate
      } else {
        return ''
      }
    }
}


