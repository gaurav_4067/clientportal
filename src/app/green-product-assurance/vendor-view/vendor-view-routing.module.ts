import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorViewComponent } from 'app/green-product-assurance/vendor-view/vendor-view.component';

const routes: Routes = [{
  path: '',
  children: [
      { path: '', component: VendorViewComponent }, {
          path: 'see-detail/:id',
          loadChildren: () => new Promise(
              resolve => {
                  (require as any)
                      .ensure(
                          [],
                          require => {
                              resolve(
                                  require(
                                      './see-detail/see-detail.module')
                                      .SeeDetailModule);
                          })
              })
      },
]
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorViewRoutingModule { }
