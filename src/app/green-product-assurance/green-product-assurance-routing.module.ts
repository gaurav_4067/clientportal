import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoContentComponent } from '../no-content/no-content.component';


const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: NoContentComponent
    },
    {
      path: 'new-application',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./new-application/new-application.module').NewApplicationModule);
        })
      })
    },
    {
      path: 'internal-view',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./internal-review/internal-review.module').InternalReviewModule);
        })
      })
    },
    {
      path: 'public-view',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./public-review/public-review.module').PublicReviewModule);
        })
      })
    },
    {
      path: 'vendor-view',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./vendor-view/vendor-view.module').VendorViewModule);
        })
      })
    },
    {
      path: 'product-info/:ProductSealNo',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./product-detail/product-detail.module').ProductDetailModule);
        })
      })
    }, 
    {
      path: 'certificate-management',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./certificate-management/certificate-management.module').CertificateManagementModule);
        })
      })
    },   
    {
      path: 'application-detail/:DetailId',
      loadChildren: () => new Promise(resolve => {
        (require as any).ensure([], require => {
          resolve(require('./certificate-management/certificate-management.module').CertificateManagementModule);
        })
      })
    }, 
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GreenProductAssuranceRoutingModule { }
