import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalReviewComponent } from './internal-review.component';

describe('InternalReviewComponent', () => {
  let component: InternalReviewComponent;
  let fixture: ComponentFixture<InternalReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
