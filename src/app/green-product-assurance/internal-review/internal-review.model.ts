import { ApplicationProductDetail } from "app/green-product-assurance/new-application/new-application.model";
import { ReviewReportDetail } from "app/green-product-assurance/new-application/new-application.model";


export class DeclineApplication {
  public ApplicationId: number;
  public Comment: string;
  public CommentType:string;
  //If True means reject application else cancel application request
  // public IsDecline: boolean;
  public ModifiedBy: number;
  public CalledFrom:string;
  public ApplicationRefNumber:string;
}

export class ApplicationListSearchRequest {
  public ApplicationReferenceNo: string;
  public VendorName: string;
  public ApplicationStatus: string = '';
  public FactoryName: string;
  public CertificateNumber: string;
  public ProductName: string;
  public ProductModelNo: string;
  public ProductSealNo: string;
  public PageSize: number = 10;
  public PageNumber: number = 1;
}

export class ApplicationProductDetailExtended extends ApplicationProductDetail {
  public IsDeleted: boolean;
}

export class ApproveRequestParams {
  constructor() {
    this.ReportDetail = new Array<ReviewReportDetail>();
  }
  public CommentType: string;
  public Comment: string;
  public ApplicationId: number;
  public ServiceType: string;
  public ServiceId: number;
  public IsApproved: boolean;
  public CreatedBy: number;
  public CertificateValidity:string;
  public ReportDetail: Array<ReviewReportDetail>;


}

export class CommentRequestParam {
  public ServiceTypeId: number;
  public ServiceType: String;
  public ApplicationId: number;
  public CommentType:string;
}

export class GPCommentDetail {
  public CommentId: number;
  public CommentType: string
  public Comment: string
  public ApplicationId: number;
  public ServiceType: string
  public ServiceTypeId: number;
  public Active: boolean
  public CreatedBy: number;
  public CreatedDate: Date;
}
