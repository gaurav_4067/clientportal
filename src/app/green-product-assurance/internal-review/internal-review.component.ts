import { Component, OnInit, ViewChild,ElementRef} from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { BaseUrl, DataService } from 'app/mts.service';
import { ApplicationInformation, ApplicationProductDetail, ApplicationFactoryDetail, DeleteDocumentRequest, FactoryDocument, ProductDocument, ReviewReportDetail } from '../new-application/new-application.model';
import { DeclineApplication, ApplicationListSearchRequest, ApproveRequestParams, CommentRequestParam, GPCommentDetail } from './internal-review.model'
import { NgForm } from '@angular/forms';
import { ReferenceAst } from '@angular/compiler';
declare let swal: any;

@Component({
  selector: 'app-internal-review',
  templateUrl: './internal-review.component.html',
  styleUrls: ['./internal-review.component.css'],

})
export class InternalReviewComponent implements OnInit {
  applicantDetailFormSubmitted: boolean = false;
  productDetailFormSubmitted: boolean = false;
  factoryDetailFormSubmitted: boolean = false;
  componentId:string;
  private pageNumber: number = 1;
  private totalItem: any;
  isLoader: boolean = false;
  userCredential: any;
  userDetail: any;
  View: boolean = false;
  @ViewChild('fileUpload') fileUploader: ElementRef;
  @ViewChild('productDetailForm') productDetailForm: NgForm;
  @ViewChild('factoryForm') factoryForm: NgForm;
  @ViewChild('applicantDetailForm') applicantDetailForm: NgForm;
  applicationInformation: ApplicationInformation;
  productInformation: ApplicationProductDetail;
  factoryInformation: ApplicationFactoryDetail;
  allApplicationInfo: Array<ApplicationInformation>;
  allProductsInfo: Array<ApplicationProductDetail>;
  allFactoriesInfo: Array<ApplicationFactoryDetail>;
  applicationListSearchRequest: ApplicationListSearchRequest;
  deleteDocumentRequest: DeleteDocumentRequest;
  declineFormSubmitted: boolean = false;
  applicationIndex: string;
  declineApplicationParam: DeclineApplication;
  validateProductModelNo: boolean = false;
  disableProductModelNo: boolean = false;
  validateFactoryCode: boolean = false;
  disablefactoryCode: boolean = false;
  editProduct: boolean = false;
  editProductIndex: number;
  editFactory: boolean = false;
  editFactoryIndex: number;
  viewUploadedDoc: boolean = false;
  fileToUpload: any;
  applicationStatusList: any;
  approveRequestParammeters: ApproveRequestParams;
  reportDetail: ReviewReportDetail;
  reviewReportDetails: Array<ReviewReportDetail>;
  TestReportDetails: Array<ReviewReportDetail>;
  ReTestReportDetails: Array<ReviewReportDetail>;
  CAPReportDetails: Array<ReviewReportDetail>;
  SupportingReportDetails: Array<ReviewReportDetail>;
  reportServiceCode: string;
  ApproveFormSubmitted: boolean = false;
  CommentRequestParam: CommentRequestParam;
  GPCommentList: Array<GPCommentDetail>;
  ApprovalWarningMsg: string;
  LogRecoredIndex: number;
  ShowDeleteAction: boolean = true;
  ArrayIndex: number;

  constructor(private router: Router,
    private dataService: DataService,
    private http: Http
  ) {
    this.applicationInformation = new ApplicationInformation();
    this.allApplicationInfo = new Array<ApplicationInformation>();
    this.allProductsInfo = new Array<ApplicationProductDetail>();
    this.allFactoriesInfo = new Array<ApplicationFactoryDetail>();
    this.productInformation = new ApplicationProductDetail();
    this.factoryInformation = new ApplicationFactoryDetail();
    this.declineApplicationParam = new DeclineApplication();
    this.applicationListSearchRequest = new ApplicationListSearchRequest();
    this.approveRequestParammeters = new ApproveRequestParams();
    this.reportDetail = new ReviewReportDetail();
    this.reviewReportDetails = new Array<ReviewReportDetail>();
    this.CommentRequestParam = new CommentRequestParam();
    this.GPCommentList = new Array<GPCommentDetail>();

  }

  ngOnInit() {
    this.isLoader = true;
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    if (this.userDetail && this.userCredential) {
      if (this.userCredential.UserPermission.GPInternalReview) {
        this.dataService.get('GP_ApplicationList/GetApplicationStatusList')
          .subscribe(response => {
            this.applicationStatusList = response.ApplicationStatusList;
          });
        this.searchApplicationList();

      } else {
        this.router.navigate(['./landing-page']);
      }
    } else {
      this.router.navigate(['./login']);
    }
  }

  pad2(n) {
    return (n < 10 ? '0' : '') + n;
  }
  searchApplicationList() {
    this.isLoader = true;
    this.View = false;
    this.dataService.post('GP_ApplicationList/GetAllApplication', this.applicationListSearchRequest)
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          this.allApplicationInfo = res.Data;
          this.isLoader = false;
          this.totalItem = res.TotalRecords
        } else {
          this.isLoader = false;
          swal('', res.Message);
        }
      });
  }

  changePageNumber(page: number) {
    this.applicationListSearchRequest.PageNumber = page;
    this.pageNumber = page;
    this.searchApplicationList();
  }

  resetSearchParam() {
    this.applicationListSearchRequest = new ApplicationListSearchRequest();
    this.pageNumber=1;
    this.searchApplicationList();
  }

  UploadLogo(event: any, id, type) {
    this.isLoader = true;
    this.fileToUpload = [];
    this.fileToUpload.push(event.target.files[0]);
    let url: string;
    if (type == 'CompanyLogo') {
      url = 'GP_ApplicationList/UploadCompanyLogo/'
    } else {
      url = 'GP_ApplicationList/UploadCertificateFile/'
    }
    if (this.validateFileExtension(type)) {
      this.companyLogoRequest(BaseUrl + url, this.fileToUpload, id).then(
        (result: any) => {
          let res = JSON.parse(result);
          if (res.IsSuccess) {
            let index = this.allApplicationInfo.findIndex(i => i.ApplicationReferenceNo == id);
            if (index > -1) {
              if (type == 'CompanyLogo') {
                this.allApplicationInfo[index].CompanyLogo = event.target.files[0].name
              } else {
                this.allApplicationInfo[index].CertificateFile = event.target.files[0].name
              }
            }
            this.fileToUpload = [];
            this.isLoader = false;
            swal('', res.Message);
          } else {
            this.isLoader = false;
            swal('', res.Message);
          }
        },
        (error) => {
        })
    }
  }

  companyLogoRequest(url: string, files: Array<File>, code: any) {
    let thisObj = this;
    var formData: any = new FormData();
    let xhr = new XMLHttpRequest();
    for (let i = 0; i < files.length; i++) {
      formData.append('UploadReport-' + i, files[i]);
    }
    formData.append("ApplicationRefNO", code);
    return new Promise((resolve, reject) => {
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            (<any>$('#inputfileValue')).val('');
            resolve(JSON.parse(xhr.response))
          }
          else {
            (<any>$('#inputfileValue')).val('');
            reject(JSON.parse(xhr.response));
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    })
  }

  validateFileExtension(calledFrom): boolean {
    let result = true;
    if (calledFrom == 'CompanyLogo' || calledFrom == 'ProductImage') {
      if (this.fileToUpload.length) {
        this.fileToUpload.forEach((file) => {
          if (file) {
            let extension = file.name.substr(file.name.lastIndexOf('.') + 1);
            if (['JPG', 'PNG', 'jpg', 'png'].indexOf(extension.toLowerCase()) > -1) {
            } else {
              this.isLoader = false;
              swal('', 'Please Upload only .JPG, .PNG, .jpg, .png files');
              result = false;
            }
            if (calledFrom == 'ProductImage') {
              let Name = file.name.substr(0, file.name.lastIndexOf('.'));
              var reg = /^[a-zA-Z0-9-_\s()]+[a-zA-Z0-9-_\s()]*$/;
              if (reg.test(Name) == true) {
              } else {
                this.isLoader = false;
                swal('', 'file name with  special character is not allowed');
                result = false;
              }
            }
          }
        })
      }
    }
    return result;
  }

  UplaodFilledApplicationForm(event: any) {
    this.isLoader = true;
    this.fileToUpload = [];
    this.fileToUpload.push(event.target.files[0]);
    let url = 'GP_ApplicationRegistration/UploadGpApplicationDocument/FA/FilledApplicationForm/';
    let docType = 'FA';
    if (this.CheckExistingFile('FilledApplicationForm', event.target.files[0].name, '', [])) {
      this.fileUploadRequest(BaseUrl + url, this.fileToUpload, 0, 0).then(
        (result: any) => {
          let res = JSON.parse(result);
          if (res.IsSuccess) {
            this.fileToUpload = [];
            if (!this.applicationInformation.FilledApplicationForm || this.applicationInformation.FilledApplicationForm == '') {
              this.applicationInformation.FilledApplicationForm = event.target.files[0].name;
            } else {
              this.applicationInformation.FilledApplicationForm = this.applicationInformation.FilledApplicationForm + 'ÿ' + event.target.files[0].name;
            }
            this.isLoader = false;
            swal('', res.Message);
          } else {
            this.isLoader = false;
            swal('', res.Message);
          }
          this.reset();
        })
    } else {
      this.isLoader = false;
      swal('', 'This file is already exist!');
    }
  }

  removedfilledApplicationForm(docName) {
    this.deleteDocumentRequest = {
      Guid: this.applicationIndex,
      Type: 'FA',
      DocumentType: 'FilledApplicationForm',
      DocumentName: docName,
      ModelNo: '',
      DeleteIndividual: true
    }
    var thisObj = this;
    swal({
      title: "",
      text: "<span style='color:#ef770e;font-size:20px;'>Are you sure to delete this File ?</span><br><br>",
      html: true,
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: true,
    },
      function (isConfirm) {
        if (isConfirm) {
          thisObj.isLoader = true;
          thisObj.dataService.post('GP_ApplicationList/DeleteProductFactoryDocument', thisObj.deleteDocumentRequest)
            .subscribe(response => {
              let res = JSON.parse(response);
              if (res.IsSuccess) {
                let splitArr = thisObj.applicationInformation.FilledApplicationForm.split("ÿ");
                let index = splitArr.findIndex(i => i == docName);
                splitArr.splice(index, 1);
                thisObj.applicationInformation.FilledApplicationForm = splitArr.join("ÿ");
                thisObj.isLoader = false;
                swal('', res.Message);
              } else {
                thisObj.isLoader = false;
                swal('', res.Message);
              }
            });
        } else {
        }
      });

  }

  ViewUploadedDocuments(refno) {
    let i = this.allApplicationInfo.findIndex(j => j.ApplicationReferenceNo == refno)
    if (i > -1) {
      this.applicationIndex = refno;
      this.applicationInformation = this.allApplicationInfo[i]
      this.allProductsInfo = this.allApplicationInfo[i].Products;
      this.allFactoriesInfo = this.allApplicationInfo[i].Factories;
      (<any>$('#DocumentDetail')).modal('show');
    }
  }

  ViewdeclineApplicationPopup(type: string, id, RefNo) {
    this.declineApplicationParam = new DeclineApplication();
    this.declineFormSubmitted = false;
    if (type == "Reject") {
      this.declineApplicationParam.CommentType = "Rejected";
    } else if (type == "Cancel") {
      this.declineApplicationParam.CommentType = "Cancelled";
    } else if (type == "Approve") {
      this.ApprovalWarningMsg = "Are you sure? You want to finally approve and generate the certificate for this application Number:"
      this.declineApplicationParam.ApplicationRefNumber = RefNo;
    } else if (type == "Resume") {
      this.ApprovalWarningMsg = "Are you sure? You want to approve this application Number:"
      this.declineApplicationParam.CommentType = "Resumed";
      this.declineApplicationParam.ApplicationRefNumber = RefNo;
    }
    this.declineApplicationParam.ApplicationId = id;
    this.declineApplicationParam.CalledFrom = type;
    this.declineApplicationParam.ModifiedBy = this.userCredential.UserId,
      (<any>$('#DeclineModal')).modal('show');
  }

  declineApplication() {
    this.declineFormSubmitted = true;
    if (this.declineApplicationParam.Comment) {
      this.isLoader = true;
      let headers = new Headers({ "Content-Type": "application/json" });
      this.dataService.post('GP_ApplicationList/DeclineApplication', this.declineApplicationParam)
        .subscribe(response => {
          let res = JSON.parse(response);
          swal('', res.Message);
          this.declineFormSubmitted = false;
          this.searchApplicationList();
          this.isLoader = false;
          (<any>$('#DeclineModal .close')).click();
        })
    }
  }

  SeeDetail(i) {
    this.applicantDetailFormSubmitted=false;
    this.applicationInformation = this.allApplicationInfo[i];
    this.applicationIndex = this.allApplicationInfo[i].ApplicationReferenceNo;
    this.View = true;
  }

  //---------------methods for products ------------//

  AddProductDetail() {
    this.productDetailFormSubmitted=false;
    this.productInformation = new ApplicationProductDetail();
    this.editProduct = false;
    this.disableProductModelNo = false;
    this.validateProductModelNo = false;
    let currentdate = new Date();
    let formatedDate = currentdate.getFullYear() +
      this.pad2(currentdate.getMonth() + 1) +
      this.pad2(currentdate.getDate()) +
      this.pad2(currentdate.getHours()) +
      this.pad2(currentdate.getMinutes()) +
      this.pad2(currentdate.getSeconds());
    this.productInformation.ProductFolderName = formatedDate;
    (<any>$('#productDetailModal')).modal('show');
  }

  EditProducts(i) {
    this.productDetailFormSubmitted=false;
    this.productInformation = new ApplicationProductDetail();
    this.productInformation = this.applicationInformation.Products[i];
    this.productInformation = JSON.parse(JSON.stringify(this.productInformation));
    this.editProductIndex = i;
    this.editProduct = true;
    this.disableProductModelNo = true;
    this.validateProductModelNo = false;
    (<any>$('#productDetailModal')).modal('show');
  }

  addProduct() {
    this.productDetailFormSubmitted=true;
    if (this.productInformation.ProductType && this.productInformation.ProductName && this.productInformation.ProductModelNo
      && this.productInformation.ProductWeight) {
      if (this.ValidateProduct() && this.validateModelNo(this.productInformation.ProductModelNo)) {
        if (this.editProduct) {
          this.applicationInformation.Products[this.editProductIndex] = Object.assign(new ApplicationProductDetail(), this.productInformation);
          this.productDetailForm.resetForm(this.productInformation);
          this.productInformation = new ApplicationProductDetail();
          (<any>$('#productDetailModal .close')).click();
        } else {
          if(this.applicationInformation.ApplicationType=='Revised'){
            this.productInformation.ProductDocumentStatus=true;
          }
          this.applicationInformation.Products.push(Object.assign(new ApplicationProductDetail(), this.productInformation));
          this.productDetailForm.resetForm(this.productInformation);
          this.productInformation = new ApplicationProductDetail();
          (<any>$('#productDetailModal .close')).click();
        }
      }
    }
  }

  ValidateProduct(): boolean {
    if (this.productInformation.ProductDocuments) {
      let ProdImageIndex = this.productInformation.ProductDocuments.findIndex(
        i => i.DocumentType == 'ProductImage'
      );
      if (ProdImageIndex > -1) {
      } else {
        swal('', 'Please upload atleast one product image');
        return false;
      }

      let BrochureIndex = this.productInformation.ProductDocuments.findIndex(
        i => i.DocumentType == 'Brochure'
      );
      if (BrochureIndex > -1) {
      } else {
        swal('', 'Please upload atleast one brochure');
        return false;
      }

      let BillOfMaterialIndex = this.productInformation.ProductDocuments.findIndex(
        i => i.DocumentType == 'BillOfMaterial'
      );
      if (BillOfMaterialIndex > -1) {
        return true;
      } else {
        swal('', 'Please upload atleast one bill of material');
        return false;
      }
    } else {
      swal('', 'Please upload atleast one product documnet for each type');
      return false;
    }
  }


  validateModelNo(value: string) {
    let result = true;
    let index = this.applicationInformation.Products.findIndex(
      i => i.ProductModelNo == value
    );
    if (index > -1 && !this.editProduct) {
      this.validateProductModelNo = true;
      result = false;
      swal('', 'Model No :' + value + ' is already exsit in product list');
    }
    else {
      this.validateProductModelNo = false;
    }
    var reg = /^[a-zA-Z0-9,{}.+=^&*()%$#@_+?!\//-|/-\s]*$/;
    if(reg.test(value) == false){
      result =false;
    }
    return result;
  }

  UploadProductDocuments(event: any, calledFrom: any) {
    this.isLoader = true;
    this.fileToUpload = [];
    this.fileToUpload.push(event.target.files[0]);
    let docType = calledFrom
    let url;
    if (this.editProduct && this.productInformation.ApplicationProdDetailID) {
      url = 'GP_ApplicationRegistration/UploadGpApplicationDocument/Product/' + docType + '/'
    } else {
      url = 'GP_ApplicationRegistration/UploadNewGpDocuments/Product/' + docType;
    }
    if (this.validateFileExtension(calledFrom) && this.CheckExistingFile('product', event.target.files[0].name, docType, this.productInformation.ProductDocuments)) {
      this.fileUploadRequest(BaseUrl + url, this.fileToUpload, this.productInformation.ProductFolderName, this.productInformation.ApplicationProdDetailID).then(
        (result: any) => {
          let res = JSON.parse(result);
          let that = this;
          if (res.IsSuccess) {
            that.fileToUpload = [];
            let tempArray = {
              DocumentType: docType,
              DocumentName: event.target.files[0].name,
              ApplicationProductDocumentID: res.Data,
              ApplicationProdDetailID: that.productInformation.ApplicationProdDetailID,
              IsVerified: false,
              Active: true,
              CreatedBy: that.userCredential.UserId,
              CreatedDate: new Date(),
              ModifiedBy: 0,
              ModifiedDate: new Date(),
              IsDeleted: false
            }
            if (that.editProduct) {
              that.applicationInformation.Products[this.editProductIndex].ProductDocuments.push(tempArray);
              that.applicationInformation.Products[this.editProductIndex].IsVerified = false;
            }
            that.productInformation.IsVerified = false;
            that.productInformation.ProductDocuments.push(tempArray);
            that.isLoader = false;
            swal('', res.Message);
            this.reset();
          } else {
            that.isLoader = false;
            swal('', res.Message);
            this.reset();
          }        
        },
        (error) => {
        })
    }

  }

  ReviewReportsUpload() {
    if ((this.reportDetail.Result && this.reportDetail.Result != "") || this.reportDetail.ReportType == "SupportingReport") {
      if (this.reportDetail.FileName && this.reportDetail.FileName != "") {
        this.isLoader = true;
        let url: string;
        let Code: string;
        let Id: number;
        let RefArry;
        if (this.reportDetail.ServiceType == 'ST') {
          url = 'GP_ApplicationRegistration/UploadNewGpDocuments/Product/ST/' + this.reportDetail.ReportType + '/';
          RefArry = this.applicationInformation.Products[this.ArrayIndex].ReviewReports
        }
        else if (this.reportDetail.ServiceType == 'FA') {
          url = 'GP_ApplicationRegistration/UploadNewGpDocuments/Factory/FA/' + this.reportDetail.ReportType + '/';
          RefArry = this.applicationInformation.Factories[this.ArrayIndex].ReviewReports
        } else {
          url = 'GP_ApplicationRegistration/UploadNewGpDocuments/Application/' + this.reportDetail.ServiceType + '/' + this.reportDetail.ReportType;
          RefArry = this.applicationInformation.ReviewReports
        }
        if (this.CheckExistingFile('review', this.reportDetail.FileName, this.reportDetail.ReportType, RefArry)) {
          this.fileUploadRequest(BaseUrl + url, this.fileToUpload, this.reportDetail.ServiceCode, this.reportDetail.ServiceId).then(
            (result: any) => {
              let res = JSON.parse(result);
            
              let that = this;
              if (res.IsSuccess) {
                if (this.reportDetail.ServiceType == 'ST') {
                  let i = this.applicationInformation.Products.findIndex(j => j.ApplicationProdDetailID == this.reportDetail.ServiceId)
                  if (i > -1) {
                    this.applicationInformation.Products[i].ReviewReports.push(this.reportDetail);
                  }
                }
                else if (this.reportDetail.ServiceType == 'FA') {
                  let i = this.applicationInformation.Factories.findIndex(j => j.ApplicationFactoryDetailID == this.reportDetail.ServiceId)
                  if (i > -1) {
                    this.applicationInformation.Factories[i].ReviewReports.push(this.reportDetail);
                  }
                } else {
                  this.applicationInformation.ReviewReports.push(this.reportDetail);
                }
                that.isLoader = false;
                swal('', res.Message);
                (<any>$("#uploadReviewReports")).modal('hide');
                that.fileToUpload = [];               
                this.resetReviewReportsFileField(this.componentId);
              } else {
                that.isLoader = false;
                swal('', res.Message);
                (<any>$("#uploadReviewReports")).modal('hide');
              }
              this.resetReviewReportsFileField(this.componentId);
            },
            (error) => {
            })
        }
      } else {
        swal('', 'Please choose a file to upload');
      }
    } else {
      swal('', 'Please select  result');
    }
  }

  ResetProductForm() {
    this.productDetailForm.resetForm(this.productInformation);
    this.productInformation = new ApplicationProductDetail();
  }

  deleteProductDocument(index, docType) {
    let DocumentName = this.productInformation.ProductDocuments[index].DocumentName
    if (this.editProduct) {
      if (docType == 'CAP' || docType == 'MaterialTestReport') {
        if (this.productInformation.ApplicationProdDetailID) {
          this.productInformation.ProductDocuments[index].IsDeleted = true;
          this.applicationInformation.Products[this.editProductIndex].ProductDocuments[index].IsDeleted = true;
        } else {
          this.deleteProdFactIndividualDocument(index, 'Products', docType, DocumentName, this.productInformation.ProductFolderName);
          this.applicationInformation.Products[this.editProductIndex].ProductDocuments.splice(index, 1);
        }
      } else {
        let tempArray = this.productInformation.ProductDocuments.filter(i => i.DocumentType == docType && !i.IsDeleted)
        if (tempArray.length > 1) {
          if (this.productInformation.ApplicationProdDetailID) {
            this.productInformation.ProductDocuments[index].IsDeleted = true;
            this.applicationInformation.Products[this.editProductIndex].ProductDocuments[index].IsDeleted = true;
          } else {
            this.deleteProdFactIndividualDocument(index, 'Products', docType, DocumentName, this.productInformation.ProductFolderName);
            this.applicationInformation.Products[this.editProductIndex].ProductDocuments.splice(index, 1);
          }
        } else {
          swal('', docType + ' should be atleast one');
        }
      }
    } else {
      this.deleteProdFactIndividualDocument(index, 'Products', docType, DocumentName, this.productInformation.ProductFolderName)
    }
  }

  deleteProductDetail(i) {
    let tempArray = this.applicationInformation.Products.filter(i => !i.IsDeleted)
    if (tempArray.length > 1) {
      if (this.applicationInformation.Products[i].ApplicationProdDetailID) {
        this.applicationInformation.Products[i].IsDeleted = true;
      } else {
        this.deleteProdFactDetail('Products', this.applicationInformation.Products[i].ProductFolderName, false, i)
      }
    } else {
      swal('', "minimum one product should be added");
    }
  }

  //---------------methods for factory ------------

  addNewFactoryDetail() {
    this.factoryDetailFormSubmitted=false;
    this.factoryInformation = new ApplicationFactoryDetail();
    this.editFactory = false;
    this.disablefactoryCode = false;
    this.validateFactoryCode = false;
    let currentdate = new Date();
    let formatedDate = currentdate.getFullYear() +
      this.pad2(currentdate.getMonth() + 1) +
      this.pad2(currentdate.getDate()) +
      this.pad2(currentdate.getHours()) +
      this.pad2(currentdate.getMinutes()) +
      this.pad2(currentdate.getSeconds());
    this.factoryInformation.FactoryFolderName = formatedDate;
    (<any>$('#FactoryDetailModal')).modal('show');
  }

  EditFactoryDetail(i) {
    this.factoryDetailFormSubmitted=false;
    this.factoryInformation = JSON.parse(JSON.stringify(this.applicationInformation.Factories[i]));
    this.editFactoryIndex = i
    this.editFactory = true;
    this.disablefactoryCode = true;
    this.validateFactoryCode = false;
    (<any>$('#FactoryDetailModal')).modal('show');
  }

  addFactory() {
    this.factoryDetailFormSubmitted=true;
    if (this.factoryInformation.FactoryName && this.factoryInformation.FactoryCode) {
      if (this.ValidateFactory() && this.validateCode(this.factoryInformation.FactoryCode)) {
        if (this.editFactory) {
          this.applicationInformation.Factories[this.editFactoryIndex] = Object.assign(new ApplicationFactoryDetail(), this.factoryInformation);
          this.factoryForm.resetForm(this.factoryInformation);
          (<any>$('#FactoryDetailModal .close')).click();
        } else {
          if(this.applicationInformation.ApplicationType=='Revised'){
            this.factoryInformation.FactoryDocumentStatus=true;
          }
          this.applicationInformation.Factories.push(Object.assign(new ApplicationFactoryDetail(), this.factoryInformation));
          this.factoryForm.resetForm(this.factoryInformation);
          (<any>$('#FactoryDetailModal .close')).click();
        }
      }
    }
  }

  ValidateFactory(): boolean {
    if (this.factoryInformation.FactoryDocuments) {
      let FlowChartIndex = this.factoryInformation.FactoryDocuments.findIndex(
        i => i.DocumentType == 'FlowChart'
      );
      if (FlowChartIndex > -1) {
        return true;
      } else {
        swal('', 'Please upload atleast one  factory flow-chart');
        return false;
      }
    } else {
      swal('', 'Please upload atleast one factory documnet');
      return false;
    }
  }

  validateCode(value: string) {
    let result = true;
    this.validateFactoryCode = false;
    let index = this.applicationInformation.Factories.findIndex(
      i => i.FactoryCode == value
    )
    if (index > -1 && !this.editFactory) {
      this.validateFactoryCode = true;
      result = false;
      swal('', 'Factory Code :' + value + ' is already exsit in factory list');
    } else {
      this.validateFactoryCode = false;
    }
    return result;
  }

  UploadFactoryDocument(event: any, calledFrom: any) {
    this.isLoader = true;
    this.fileToUpload = [];
    this.fileToUpload.push(event.target.files[0]);
    let docType = calledFrom
    let url: string;
    if (this.editFactory && this.factoryInformation.ApplicationFactoryDetailID) {
      url = 'GP_ApplicationRegistration/UploadGpApplicationDocument/Factory/' + docType + '/'
    } else {
      url = 'GP_ApplicationRegistration/UploadNewGpDocuments/Factory/' + docType + '/'
    }

    if (this.CheckExistingFile('factory', event.target.files[0].name, docType, this.factoryInformation.FactoryDocuments)) {
      this.fileUploadRequest(BaseUrl + url, this.fileToUpload, this.factoryInformation.FactoryFolderName, this.factoryInformation.ApplicationFactoryDetailID).then(
        (result: any) => {
          let res = JSON.parse(result);
          let that = this;
          if (res.IsSuccess) {
            that.fileToUpload = [];
            let tempArray = {
              DocumentType: docType,
              DocumentName: event.target.files[0].name,
              ApplicationFactoryDocumentID: res.Data,
              ApplicationFactoryDetailID: that.factoryInformation.ApplicationFactoryDetailID,
              IsVerified: false,
              Active: false,
              CreatedBy: that.userCredential.UserId,
              CreatedDate: new Date(),
              ModifiedBy: 0,
              ModifiedDate: new Date(),
              IsDeleted: false
            }
            if (that.editFactory) {
              that.applicationInformation.Factories[that.editFactoryIndex].FactoryDocuments.push(tempArray);
              that.applicationInformation.Factories[that.editFactoryIndex].IsVerified = false;
            }
            that.factoryInformation.IsVerified = false;
            that.factoryInformation.FactoryDocuments.push(tempArray);
            that.isLoader = false;
            swal('', res.Message);
            this.reset();
          } else {
            that.isLoader = false;
            swal('', res.Message);
            this.reset();
          }
        },
        (error) => {
        })
    }
  }


  deleteFactoryDetail(index) {
    let tempArray = this.applicationInformation.Factories.filter(i => !i.IsDeleted)
    if (tempArray.length > 1) {
      if (this.applicationInformation.Factories[index].ApplicationFactoryDetailID) {
        this.applicationInformation.Factories[index].IsDeleted = true;
      } else {
        this.deleteProdFactDetail('Factories', this.applicationInformation.Factories[index].FactoryFolderName, false, index)
      }
    } else {
      swal('', "minimum one factory should be added");
    }
  }

  deleteFactoryDocument(index, type) {
    let DocumentName = this.factoryInformation.FactoryDocuments[index].DocumentName;
    if (this.editFactory) {
      if (type == 'FlowChart') {
        let tempArray = this.factoryInformation.FactoryDocuments.filter(i => i.DocumentType == type && !i.IsDeleted)
        if (tempArray.length > 1) {
          if (this.factoryInformation.ApplicationFactoryDetailID) {
            this.applicationInformation.Factories[this.editFactoryIndex].FactoryDocuments[index].IsDeleted = true;
            this.factoryInformation.FactoryDocuments[index].IsDeleted = true;
          } else {
            this.deleteProdFactIndividualDocument(index, 'Factories', type, DocumentName, this.factoryInformation.FactoryFolderName)
            this.applicationInformation.Factories[this.editFactoryIndex].FactoryDocuments.splice(index, 1);
          }
        } else {
          swal('', type + ' should be atleast one');
        }
      } else {
        if (this.factoryInformation.ApplicationFactoryDetailID) {
          this.applicationInformation.Factories[this.editFactoryIndex].FactoryDocuments[index].IsDeleted = true;
          this.factoryInformation.FactoryDocuments[index].IsDeleted = true;
        } else {
          this.deleteProdFactIndividualDocument(index, 'Factories', type, DocumentName, this.factoryInformation.FactoryFolderName)
          this.applicationInformation.Factories[this.editFactoryIndex].FactoryDocuments.splice(index, 1);
        }
      }
    } else {
      this.deleteProdFactIndividualDocument(index, 'Factories', type, DocumentName, this.factoryInformation.FactoryFolderName)
    }
  }

  ResetFactoryForm() {
    this.factoryForm.resetForm(this.factoryInformation);
    this.factoryInformation = new ApplicationFactoryDetail();
  }

  //************Common methods********* *//

  UploadMoreDocuments(event: any, Type: any, calledFrom: any, demoArray: any, index) {
    this.isLoader = true;
    var DetailId: string;
    let modelNo: string;
    this.fileToUpload = [];
    this.fileToUpload.push(event.target.files[0]);
    let docType = calledFrom
    let RefArray;
    let arrayType: string
    let url = 'GP_ApplicationRegistration/UploadGpApplicationDocument/' + Type + '/' + docType + '/'
    if (Type == 'Product') {
      modelNo = demoArray.ProductFolderName;
      DetailId = demoArray.ApplicationProdDetailID;
      RefArray = this.allProductsInfo[index].ProductDocuments;
      arrayType = 'product';
    } else {
      modelNo = demoArray.FactoryFolderName;
      DetailId = demoArray.ApplicationFactoryDetailID;
      RefArray = this.allFactoriesInfo[index].FactoryDocuments;
      arrayType = 'factory';
    }
    if (this.validateFileExtension(calledFrom) && this.CheckExistingFile(arrayType, event.target.files[0].name, docType, RefArray)) {
      this.fileUploadRequest(BaseUrl + url, this.fileToUpload, modelNo, DetailId).then(
        (result: any) => {
          let res = JSON.parse(result);
          if (res.IsSuccess) {
            this.isLoader = false;
            let that = this;
            let appIndex = this.allApplicationInfo.findIndex(i => i.ApplicationReferenceNo == this.applicationIndex)
            if (Type == 'Product') {
              let tempArray = {
                DocumentType: docType,
                DocumentName: event.target.files[0].name,
                ApplicationProductDocumentID: res.Data,
                ApplicationProdDetailID: demoArray.ApplicationProdDetailID,
                IsVerified: false,
                Active: true,
                CreatedBy: this.userCredential.UserId,
                CreatedDate: new Date(),
                ModifiedBy: 0,
                ModifiedDate: new Date(),
                IsDeleted: false
              }
              that.allProductsInfo[index].ProductDocuments.push(tempArray);
            } else {
              let tempArray = {
                DocumentType: docType,
                DocumentName: event.target.files[0].name,
                ApplicationFactoryDocumentID: res.Data,
                ApplicationFactoryDetailID: demoArray.ApplicationFactoryDetailID,
                IsVerified: false,
                Active: false,
                CreatedBy: this.userCredential.UserId,
                CreatedDate: new Date(),
                ModifiedBy: 0,
                ModifiedDate: new Date(),
                IsDeleted: false
              }
              that.allFactoriesInfo[index].FactoryDocuments.push(tempArray);
            }
            this.searchApplicationList();
            swal('', res.Message);
            this.reset();
          } else {
            this.isLoader = false;
            swal('', res.Message);
            this.reset();
          }
        });
    }
  }

  fileUploadRequest(url: string, files: Array<File>, code: any, Id: any) {
    let thisObj = this;
    var formData: any = new FormData();
    let xhr = new XMLHttpRequest();
    for (let i = 0; i < files.length; i++) {
      formData.append('UploadReport-' + i, files[i]);
    }

    formData.append("ApplicationRefNO", this.applicationIndex);
    formData.append("ModelNo", code);
    formData.append("DetailId", Id);
    formData.append("CreatedBy", this.userCredential.UserId);
    return new Promise((resolve, reject) => {

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            (<any>$('#inputfileValue')).val('');
            resolve(JSON.parse(xhr.response))
          }
          else {
            (<any>$('#inputfileValue')).val('');
            reject(JSON.parse(xhr.response));
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    })
  }

  downloadDocument(docName, docType, serviceType, modelNo, RefNo) {
     
    if (RefNo) {
      this.applicationIndex = RefNo;
    }
    let downloadFileName = encodeURIComponent(docName.trim());
    window.location.href = BaseUrl + "GP_ApplicationList/DownloadDocument?ApplicationReferenceNo="
      + this.applicationIndex.trim() + "&ServiceType=" + serviceType.trim() + "&ModelNo="
      + modelNo.trim() + "&DocumentName=" + downloadFileName.trim() + "&DocumentType=" + docType.trim();
  }

  downloadFilledApplicationDocument(docName, RefNo) {
    let serviceType = 'FilledApplicationForm'
    let downloadFileName = encodeURIComponent(docName.trim());
    window.location.href = BaseUrl + "GP_ApplicationList/DownloadFilledApplicationDocument?ApplicationReferenceNo="
      + RefNo.trim() + "&ServiceType=" + serviceType.trim() + "&DocumentName=" + downloadFileName.trim();
  }

  restrictKey(event) {
    if (event.keyCode === 69 || event.keyCode === 190) {
      return false;
    }
  }

  validateSpace(value: string) {
    if (value = 'Product')
      if (this.productInformation.ProductModelNo) {
        this.productInformation.ProductModelNo = this.productInformation.ProductModelNo.trim();
        this.productInformation.ProductModelNo = this.productInformation.ProductModelNo.replace(/  +/g, ' ');
        this.validateProductModelNo = false;
      } else {
        if (this.factoryInformation.FactoryCode) {
          this.factoryInformation.FactoryCode = this.factoryInformation.FactoryCode.trim();
          this.factoryInformation.FactoryCode = this.factoryInformation.FactoryCode.replace(/  +/g, ' ');
        }
        this.validateFactoryCode = false;
      }

  }

  documentVerification(i, type, detailId, documentId) {
    let that = this
    let object = {
      Type: type,
      DetailId: detailId,
      DocumentId: documentId
    }
    that.dataService.post('GP_ApplicationList/ProductFactoryDocumentVerification', object
    )
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          swal('', res.Message);
          if (type == 'Product') {
            let indexProduct = that.applicationInformation.Products[i].ProductDocuments.findIndex(i => i.ApplicationProductDocumentID == documentId);
            if (indexProduct > -1) {
              that.applicationInformation.Products[i].ProductDocuments[indexProduct].IsVerified = true;
            }
            let Temp = that.applicationInformation.Products[i].ProductDocuments.filter(i => i.IsVerified == false)
            if (Temp.length == 0) {
              that.applicationInformation.Products[i].IsVerified = true;
            }
          } else {
            let indexFactory = that.applicationInformation.Factories[i].FactoryDocuments.findIndex(i => i.ApplicationFactoryDocumentID == documentId)
            if (indexFactory > -1) {
              that.applicationInformation.Factories[i].FactoryDocuments[indexFactory].IsVerified = true;
            }
            let Temp = that.applicationInformation.Factories[i].FactoryDocuments.filter(i => i.IsVerified == false)
            if (Temp.length == 0) {
              that.applicationInformation.Factories[i].IsVerified = true;
            }
          }

        } else {
          swal('', res.Message);
        }
      });
  }

  documentUnVerification(i, type, detailId, documentId) {

    let that = this
    let object = {
      Type: type,
      DetailId: detailId,
      DocumentId: documentId
    }
    that.dataService.post('GP_ApplicationList/ProductFactoryDocumentunVerified', object
    )
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          swal('', res.Message);
          if (type == 'Product') {
            let indexProduct = that.applicationInformation.Products[i].ProductDocuments.findIndex(i => i.ApplicationProductDocumentID == documentId);
            if (indexProduct > -1) {
              that.applicationInformation.Products[i].ProductDocuments[indexProduct].IsVerified = false;
              that.applicationInformation.Products[i].IsVerified = false;
            }
          } else {
            let indexFactory = that.applicationInformation.Factories[i].FactoryDocuments.findIndex(i => i.ApplicationFactoryDocumentID == documentId)
            if (indexFactory > -1) {
              that.applicationInformation.Factories[i].FactoryDocuments[indexFactory].IsVerified = false;
              that.applicationInformation.Factories[i].IsVerified = false;
            }
          }
        } else {
          swal('', res.Message);
        }
      });
  }

  deleteProdFactDetail(Type, ModelNo, DeleteIndividual, index) {
    this.deleteDocumentRequest = {
      Guid: this.applicationIndex,
      Type: Type,
      DocumentType: "",
      DocumentName: "",
      ModelNo: ModelNo,
      DeleteIndividual: DeleteIndividual
    }
    this.dataService.post('GP_ApplicationList/DeleteProductFactoryDocument', this.deleteDocumentRequest)
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          if (Type == 'Products') {
            this.applicationInformation.Products.splice(index, 1);
          } else {
            this.applicationInformation.Factories.splice(index, 1);
          }
        } else {
          swal('', res.Message);
        }
      });
  }

  deleteProdFactIndividualDocument(index, ServiceType, docType, docName, ModelNo) {
    this.deleteDocumentRequest = {
      Guid: this.applicationIndex,
      Type: ServiceType,
      DocumentType: docType,
      DocumentName: docName,
      ModelNo: ModelNo,
      DeleteIndividual: true
    }
    this.dataService.post('GP_ApplicationList/DeleteProductFactoryDocument', this.deleteDocumentRequest)
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          swal('', res.Message);

          if (ServiceType == 'Products') {
            this.productInformation.ProductDocuments.splice(index, 1);
          } else {
            this.factoryInformation.FactoryDocuments.splice(index, 1);
          }
        } else {
          swal('', res.Message);
        }
      });
  }

  submitApplication() {
    this.applicantDetailFormSubmitted=true;
    if (this.applicationInformation.VendorName && this.applicationInformation.VendorCode && this.applicationInformation.Email) {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (!reg.test(this.applicationInformation.Email) == false) {
        this.isLoader = true
        this.dataService.post('GP_ApplicationList/SubmitApplicationInformation', this.applicationInformation)
          .subscribe(result => {
            let res = JSON.parse(result);
            if (res.IsSuccess) {
              this.searchApplicationList();
              swal('', res.Message);
              this.isLoader = false
            } else {
              swal('', res.Message);
              this.isLoader = false
            }

          }
          )
      } else {
        swal('', 'Please enter the valid vendor email');
      }
    } else {
      swal('', 'Please fill the required feild');
    }
  }

  approvedApplication(Id) {
    this.declineFormSubmitted = true;
    if (this.declineApplicationParam.Comment) {
      this.isLoader = true;
      let Obj = {
        ApplicationId: Id,
        ApprovalComment: this.declineApplicationParam.Comment,
        ApprovedBy: this.userCredential.UserId
      }
      this.dataService.post('GP_ApplicationList/ApproveApplication', Obj)
        .subscribe(response => {
          (<any>$('#DeclineModal .close')).click();
          let res = JSON.parse(response);
          if (res.IsSuccess) {
            this.isLoader = false;
            this.applicationInformation = res.Data[0];
            this.searchApplicationList();
            (<any>$('#ApprovedApplicationModal')).modal('show');
          } else {
            swal('', res.Message)
            this.isLoader = false;
          }
        });
    }
  }

  FAandSTDocumentUpload(event: any, m, calledFrom: any) {
    if (this.DisbaleFAandSTUploadButton(m, calledFrom)) {
      this.isLoader = true;
      this.fileToUpload = [];
      this.fileToUpload.push(event.target.files[0]);
      let fileName: string = event.target.files[0].name;
      let url: string;
      let Code: string;
      let Id: number;
      if (calledFrom == 'Product') {
        Code = this.applicationInformation.Products[m].ProductModelNo;
        Id = this.applicationInformation.Products[m].ApplicationProdDetailID;
        url = 'GP_ApplicationRegistration/UploadGpApplicationDocument/Product/ST/';
      } else {
        Code = this.applicationInformation.Factories[m].FactoryCode;
        Id = this.applicationInformation.Factories[m].ApplicationFactoryDetailID;
        url = 'GP_ApplicationRegistration/UploadGpApplicationDocument/Factory/FA';
      }
      this.fileUploadRequest(BaseUrl + url, this.fileToUpload, Code, Id).then(
        (result: any) => {
          let res = JSON.parse(result);
          let that = this;
          if (res.IsSuccess) {
            if (calledFrom == 'Product') {
              if (!that.applicationInformation.Products[m].STDocumentName || that.applicationInformation.Products[m].STDocumentName == '')
                that.applicationInformation.Products[m].STDocumentName = fileName;
              else
                that.applicationInformation.Products[m].STDocumentName = that.applicationInformation.Products[m].STDocumentName + 'ÿ' + fileName;

            } else {
              if (!that.applicationInformation.Factories[m].FADocumentName || that.applicationInformation.Factories[m].FADocumentName == '')
                that.applicationInformation.Factories[m].FADocumentName = fileName;
              else
                that.applicationInformation.Factories[m].FADocumentName = that.applicationInformation.Factories[m].FADocumentName + 'ÿ' + fileName;

            }
            that.isLoader = false;
            swal('', res.Message);
            that.fileToUpload = [];
          } else {
            that.isLoader = false;
            swal('', res.Message);
          }
        },
        (error) => {
        })
    }
  }

  DisbaleFAandSTUploadButton(m, type): Boolean {
    let result: boolean = true;
    if (type == 'Product') {
      if (this.applicationInformation.Products[m].IsVerified) {

      } else {
        swal('', type + ' is not verified yet');
        result = false;
      }
    } else {
      if (this.applicationInformation.Factories[m].IsVerified) {

      } else {
        swal('', type + ' is not verified yet');
        result = false;
      }
    }
    return result;
  }

  getClass(value: string) {
    let result;
    if (value == 'Approved')
      return result = 'Approve';

    else if (value == 'Registered' || value == 'Revised' || value == 'Renewed' || value == 'Expired')
      return result = 'Registered';

    if (value == 'In Progress' || value == 'Observation')
      return result = 'InProgress';

    if (value == 'Document Verified')
      return result = 'DocVerfied';

    if (value == 'ST Verified' || value == 'FA Verified')
      return result = 'STVerified';

    if (value == 'Technical Verified')
      return result = 'TechVerfied';

    if (value == 'Rejected' || value == 'Cancelled' || value == 'Terminated' || value == 'Suspended')
      return result = 'Cancelled';
  }

  validateMinDate(MyDate) {
    let TempDate = new Date(MyDate);
    let temp = TempDate.getFullYear();
    if (temp > 1) {
      return MyDate
    } else {
      return ''
    }
  }

  downloadCertificateDocument(docName, RefNo, Type) {
    let downloadFileName = encodeURIComponent(docName.trim());
    window.location.href = BaseUrl + "GP_VendorReview/DownloadDocument?ApplicationReferenceNo="
      + RefNo.trim() + "&DocumentName=" + downloadFileName.trim() + "&ServiceType=" + Type.trim();
  }

  ViewRejectApprovePopup(type: string, isApproved, id) {
    this.approveRequestParammeters = new ApproveRequestParams();
    if (type == 'Factory Surveillance Audit' ||
      type == 'Factory Surveillance Testing' ||
      type == 'Market Surveillance Testing') {
      this.approveRequestParammeters.CertificateValidity = "Observation";
    }
    if (isApproved) {
      this.approveRequestParammeters.IsApproved = true;
      this.approveRequestParammeters.CommentType = 'Approved';
    } else {
      this.approveRequestParammeters.IsApproved = false;
      this.approveRequestParammeters.CommentType = 'Rejected';
    }
    this.approveRequestParammeters.ServiceType = type;
    this.approveRequestParammeters.ServiceId = id,
      this.approveRequestParammeters.CreatedBy = this.userCredential.UserId,
      this.approveRequestParammeters.ApplicationId = this.applicationInformation.ApplicationInfoMasterID;
    this.ApproveFormSubmitted = false;
    (<any>$('#ApproveRejectModal')).modal('show');
  }

  ApproveFactoryProduct() {
    this.ApproveFormSubmitted = true;
    let tempArray = [];
    if (this.approveRequestParammeters.Comment && this.approveRequestParammeters.Comment != '') {
      if (this.approveRequestParammeters.ServiceType == 'Initial Testing') {
        this.approveRequestParammeters.ServiceType = 'ST';
        tempArray = this.applicationInformation.Products.filter(i => i.ApplicationProdDetailID == this.approveRequestParammeters.ServiceId);
        this.approveRequestParammeters.ReportDetail = tempArray[0].ReviewReports;

      } else if (this.approveRequestParammeters.ServiceType == 'Initial Audit') {
        this.approveRequestParammeters.ServiceType = 'FA';
        tempArray = this.applicationInformation.Factories.filter(i => i.ApplicationFactoryDetailID == this.approveRequestParammeters.ServiceId);
        this.approveRequestParammeters.ReportDetail = tempArray[0].ReviewReports;
      }
      else if (this.approveRequestParammeters.ServiceType == 'Factory Surveillance Audit') {
        this.approveRequestParammeters.ServiceType = 'FSA';
        this.approveRequestParammeters.ReportDetail = this.applicationInformation.ReviewReports.filter(i => i.ServiceType == 'FSA');
      }
      else if (this.approveRequestParammeters.ServiceType == 'Factory Surveillance Testing') {
        this.approveRequestParammeters.ServiceType = 'FST';
        this.approveRequestParammeters.ReportDetail = this.applicationInformation.ReviewReports.filter(i => i.ServiceType == 'FST');
      }
      else if (this.approveRequestParammeters.ServiceType == 'Market Surveillance Testing') {
        this.approveRequestParammeters.ServiceType = 'MST';
        this.approveRequestParammeters.ReportDetail = this.applicationInformation.ReviewReports.filter(i => i.ServiceType == 'MST');
      }
      this.isLoader = true;
      this.dataService.post('GP_ApplicationList/SubmitCommentForApproval', this.approveRequestParammeters)
        .subscribe(response => {
          this.ApproveFormSubmitted = false;
          let res = JSON.parse(response);
          let thisobj = this;
          if (res.IsSuccess) {
            let index;
            if (thisobj.approveRequestParammeters.ServiceType == "Product" || thisobj.approveRequestParammeters.ServiceType == "ST") {
              index = thisobj.applicationInformation.Products.findIndex(i => i.ApplicationProdDetailID == thisobj.approveRequestParammeters.ServiceId)
              {
                if (index > -1) {
                  if (thisobj.approveRequestParammeters.ServiceType == "Product") {
                    if (thisobj.approveRequestParammeters.IsApproved) {
                      thisobj.applicationInformation.Products[index].IsApproved = true;
                      thisobj.applicationInformation.Products[index].IsRejected = false;
                    } else {
                      thisobj.applicationInformation.Products[index].IsRejected = true;
                      thisobj.applicationInformation.Products[index].IsApproved = false;
                    }
                  } else {
                    thisobj.applicationInformation.Products[index].ReviewReports = res.Data.filter(i => i.ServiceType == "ST");
                    if (thisobj.approveRequestParammeters.IsApproved) {
                      thisobj.applicationInformation.Products[index].IsSTApproved = true;
                      thisobj.applicationInformation.Products[index].IsSTRejected = false;
                    } else {
                      thisobj.applicationInformation.Products[index].IsSTRejected = true;
                      thisobj.applicationInformation.Products[index].IsSTApproved = false;
                    }
                  }
                }
              }
            }
            else if (thisobj.approveRequestParammeters.ServiceType == "Factory" || thisobj.approveRequestParammeters.ServiceType == "FA") {
              index = thisobj.applicationInformation.Factories.findIndex(i => i.ApplicationFactoryDetailID == thisobj.approveRequestParammeters.ServiceId)
              {
                if (index > -1) {
                  if (thisobj.approveRequestParammeters.ServiceType == "Factory") {
                    if (thisobj.approveRequestParammeters.IsApproved) {
                      thisobj.applicationInformation.Factories[index].IsApproved = true;
                      thisobj.applicationInformation.Factories[index].IsRejected = false;
                    } else {
                      thisobj.applicationInformation.Factories[index].IsRejected = true;
                      thisobj.applicationInformation.Factories[index].IsApproved = false;
                    }
                  } else {
                    thisobj.applicationInformation.Factories[index].ReviewReports = res.Data.filter(i => i.ServiceType == "FA");
                    if (thisobj.approveRequestParammeters.IsApproved) {
                      thisobj.applicationInformation.Factories[index].IsFAApproved = true;
                      thisobj.applicationInformation.Factories[index].IsFARejected = false;
                    } else {
                      thisobj.applicationInformation.Factories[index].IsFARejected = true;
                      thisobj.applicationInformation.Factories[index].IsFAApproved = false;
                    }
                  }
                }
              }
            }
            else if (thisobj.approveRequestParammeters.ServiceType == "FST" || thisobj.approveRequestParammeters.ServiceType == "MST" || thisobj.approveRequestParammeters.ServiceType == "FSA") {
              if (thisobj.approveRequestParammeters.ServiceType == "FST") {
                thisobj.applicationInformation.ReviewReports = thisobj.applicationInformation.ReviewReports.filter(i => i.ServiceType == "MST" || i.ServiceType == "FSA" || i.ServiceType == "RR")
                res.Data.forEach(element => {
                  thisobj.applicationInformation.ReviewReports.push(element);
                });
                if (thisobj.approveRequestParammeters.IsApproved) {
                  thisobj.applicationInformation.IsFST_Approved = true;
                  thisobj.applicationInformation.IsFST_Rejected = false;
                  thisobj.applicationInformation.FST_Status = "Approved"
                }
                else {
                  thisobj.applicationInformation.IsFST_Rejected = true;
                  thisobj.applicationInformation.IsFST_Approved = false;
                  thisobj.applicationInformation.FST_Status = this.approveRequestParammeters.CertificateValidity;
                }

              } else if (thisobj.approveRequestParammeters.ServiceType == "FSA") {
                thisobj.applicationInformation.ReviewReports = thisobj.applicationInformation.ReviewReports.filter(i => i.ServiceType == "MST" || i.ServiceType == "FST" || i.ServiceType == "RR")
                res.Data.forEach(element => {
                  thisobj.applicationInformation.ReviewReports.push(element);
                });
                if (thisobj.approveRequestParammeters.IsApproved) {
                  thisobj.applicationInformation.IsFSA_Approved = true;
                  thisobj.applicationInformation.IsFSA_Rejected = false;
                  thisobj.applicationInformation.FSA_Status = "Approved"
                }
                else {
                  thisobj.applicationInformation.IsFSA_Rejected = true;
                  thisobj.applicationInformation.IsFSA_Approved = false;
                  thisobj.applicationInformation.FSA_Status = this.approveRequestParammeters.CertificateValidity;
                }
              } else {
                thisobj.applicationInformation.ReviewReports = thisobj.applicationInformation.ReviewReports.filter(i => i.ServiceType == "FST" || i.ServiceType == "FSA" || i.ServiceType == "RR")
                res.Data.forEach(element => {
                  thisobj.applicationInformation.ReviewReports.push(element);
                });
                if (thisobj.approveRequestParammeters.IsApproved) {
                  thisobj.applicationInformation.IsMST_Approved = true;
                  thisobj.applicationInformation.IsMST_Rejected = false;
                  thisobj.applicationInformation.MST_Status = "Approved"
                }
                else {
                  thisobj.applicationInformation.IsMST_Rejected = true;
                  thisobj.applicationInformation.IsMST_Approved = false;
                  thisobj.applicationInformation.MST_Status = this.approveRequestParammeters.CertificateValidity;
                }
              }
            }
          }
          thisobj.isLoader = false;
          this.ApproveFormSubmitted = false;
          if ((thisobj.approveRequestParammeters.ServiceType == "FST" || thisobj.approveRequestParammeters.ServiceType == "MST" || thisobj.approveRequestParammeters.ServiceType == "FSA") && !this.approveRequestParammeters.IsApproved) {
            swal('', thisobj.approveRequestParammeters.ServiceType + " " + this.approveRequestParammeters.CertificateValidity + " successfully");
          } else {
            swal('', res.Message);
          }
          (<any>$('#ApproveRejectModal .close')).click();
        })
    }
  }

  ViewReportLog(type: string, Temp, serviceType, index) {
    this.ShowDeleteAction = true;
    if (serviceType == 'ST') {
      this.reportServiceCode = Temp.ProductFolderName;
      if (Temp.IsSTApproved || Temp.IsSTRejected) {
        this.ShowDeleteAction = false;
      }
    }
    else if (serviceType == 'FA') {
      this.reportServiceCode = Temp.FactoryFolderName;
      if (Temp.IsFAApproved || Temp.IsFARejected) {
        this.ShowDeleteAction = false;
      }
    }
    else if (serviceType == 'FST' && (this.applicationInformation.IsFST_Approved || this.applicationInformation.IsFST_Rejected)) {
      this.ShowDeleteAction = false;
    }
    else if (serviceType == 'FSA' && (this.applicationInformation.IsFSA_Approved || this.applicationInformation.IsFSA_Rejected)) {
      this.ShowDeleteAction = false;
    }
    else if (serviceType == 'MST' && (this.applicationInformation.IsMST_Approved || this.applicationInformation.IsMST_Rejected)) {
      this.ShowDeleteAction = false;
    }
    this.reviewReportDetails = Temp.ReviewReports.filter(i => i.ServiceType == serviceType);
    this.LogRecoredIndex = index;
    (<any>$('#LogRecord')).modal('show');
  }

  UploadReviewReports(calledFrom, serviceType, docType, detail, index) {
    this.componentId=serviceType+docType;
    if (this.applicationInformation.CertificateNumber && this.applicationInformation.CertificateNumber != ""
      || (calledFrom != 'Application')) {      
      this.reportDetail = new ReviewReportDetail();
      this.reportDetail.ReportType = docType;
      this.reportDetail.ServiceType = serviceType;
      this.reportDetail.ApplicationId = this.applicationInformation.ApplicationInfoMasterID;
      this.reportDetail.CreatedBy = this.userCredential.UserId;
      this.reportDetail.CreatedDate = new Date();
      this.reportDetail.Result = "";
      if (calledFrom == 'Product') {
        this.reportDetail.ServiceCode = detail.ProductFolderName;
        this.reportDetail.ServiceId = detail.ApplicationProdDetailID;
        this.ArrayIndex = index;
        (<any>$('#uploadReviewReports')).modal('show');
      } else if (calledFrom == 'Factory') {
        this.reportDetail.ServiceCode = detail.FactoryFolderName;
        this.reportDetail.ServiceId = detail.ApplicationFactoryDetailID;
        this.ArrayIndex = index;
        (<any>$('#uploadReviewReports')).modal('show');
      }else if (calledFrom == 'Application') {
        if(this.applicationInformation.CertificateExpiryDate >=this.applicationInformation.CurrentDate ){
          (<any>$('#uploadReviewReports')).modal('show');
        }      
      }
    } else {
      swal('', 'Application:' + this.applicationInformation.ApplicationReferenceNo + ' is not elgible to upload ' + serviceType + '  reports untill certificate will not issued.');
    }
  }

  ReportUpload(event: any, ) {
    this.fileToUpload = [];
    this.fileToUpload.push(event.target.files[0]);
    this.reportDetail.FileName = event.target.files[0].name;
  }

  downloadReviewReports(reviewReport) {
    let service;
    if (reviewReport.ServiceType == 'ST') {
      service = 'Products'
    } else if (reviewReport.ServiceType == 'FA') {
      service = 'Factories'
    } else {
      service = 'Application'
    }
    if (!this.reportServiceCode) {
      this.reportServiceCode = '';
    }
    let downloadFileName = encodeURIComponent(reviewReport.FileName.trim());
    window.location.href = BaseUrl + "GP_ApplicationList/DownloadReviewReports?ApplicationReferenceNo="
      + this.applicationIndex.trim() + "&ServiceType=" + service.trim() + "&ModelNo="
      + this.reportServiceCode.trim() + "&DocumentName=" + downloadFileName.trim() + "&DocumentType=" + reviewReport.ServiceType.trim()
      + "&ReportType=" + reviewReport.ReportType.trim();
  }

  viewCommentDetails(ServiceType: string, ServiceId: number, CommentType: string) {
    this.CommentRequestParam = new CommentRequestParam();
    this.GPCommentList = new Array<GPCommentDetail>();
    if (ServiceType == "Application" || ServiceType == "Suspended") {
      this.CommentRequestParam.ApplicationId = ServiceId;
      this.CommentRequestParam.ServiceTypeId = 0;
      this.CommentRequestParam.CommentType = CommentType;
    } else {
      this.CommentRequestParam.ApplicationId = this.applicationInformation.ApplicationInfoMasterID;
      this.CommentRequestParam.ServiceTypeId = ServiceId;
    }
    this.CommentRequestParam.ServiceType = ServiceType;
    this.dataService.post('GP_ApplicationList/GetGPCommentList', this.CommentRequestParam)
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          this.GPCommentList = res.Data;
          (<any>$('#viewComments')).modal('show');
        } else {
          swal('', res.Message);
        }
      });
  }

  UplaodApplicationReviewReports(event: any, ) {
    this.isLoader = true;
    this.reportDetail = new ReviewReportDetail();
    this.reportDetail.ReportType = "ReviewReport";
    this.reportDetail.ServiceType = "RR";
    this.fileToUpload = [];
    this.fileToUpload.push(event.target.files[0]);
    this.reportDetail.FileName = event.target.files[0].name;
    this.reportDetail.ApplicationId = this.applicationInformation.ApplicationInfoMasterID;
    this.reportDetail.CreatedBy = this.userCredential.UserId;
    this.reportDetail.CreatedDate = new Date();
    let url = 'GP_ApplicationRegistration/UploadNewGpDocuments/Application/' + this.reportDetail.ServiceType + '/' + this.reportDetail.ReportType;
    if (this.CheckExistingFile('review', this.reportDetail.FileName, this.reportDetail.ReportType, this.applicationInformation.ReviewReports)) {
      this.fileUploadRequest(BaseUrl + url, this.fileToUpload, this.reportDetail.ServiceCode, this.reportDetail.ServiceId).then(
        (result: any) => {
          let res = JSON.parse(result);
          let that = this;
          if (res.IsSuccess) {
            this.applicationInformation.ReviewReports.push(this.reportDetail);
            that.isLoader = false;
            swal('', res.Message);
            that.fileToUpload = [];
          } else {
            that.isLoader = false;
            swal('', res.Message);
          }
          this.reset();
        },
        (error) => {
        })
    }
  }

  DeleteReviewReports(Type: string, report: ReviewReportDetail, i) {
    if (Type == 'MST' || Type == 'FSA' || Type == 'FST' || Type == 'RR') {
      let index = this.applicationInformation.ReviewReports.findIndex(i => i.ServiceType == Type && i.ReportType == report.ReportType
        && i.Result == report.Result && i.FileName == report.FileName && i.ReportId == report.ReportId);
      if (index > -1) {
        if (report.ReportId) {
          this.applicationInformation.ReviewReports[index].IsDeleted = true;
        } else {
          this.applicationInformation.ReviewReports.splice(index, 1);
        }
      }
      this.reviewReportDetails.splice(i, 1);
    } else if (Type == 'ST') {
      let index = this.applicationInformation.Products[this.LogRecoredIndex].ReviewReports.findIndex(i => i.ServiceType == Type && i.ReportType == report.ReportType
        && i.Result == report.Result && i.FileName == report.FileName && i.ReportId == report.ReportId);
      if (index > -1) {
        if (report.ReportId) {
          this.applicationInformation.Products[this.LogRecoredIndex].ReviewReports[index].IsDeleted = true;
        } else {
          this.applicationInformation.Products[this.LogRecoredIndex].ReviewReports.splice(index, 1);
        }
      }
      this.reviewReportDetails.splice(i, 1);
    } else if (Type == 'FA') {
      let index = this.applicationInformation.Factories[this.LogRecoredIndex].ReviewReports.findIndex(i => i.ServiceType == Type && i.ReportType == report.ReportType
        && i.Result == report.Result && i.FileName == report.FileName && i.ReportId == report.ReportId);
      if (index > -1) {
        if (report.ReportId) {
          this.applicationInformation.Factories[this.LogRecoredIndex].ReviewReports[index].IsDeleted = true;
        } else {
          this.applicationInformation.Factories[this.LogRecoredIndex].ReviewReports.splice(index, 1);
        }
      }
      this.reviewReportDetails.splice(i, 1);
    }
  }

  editableInputFeild(CalledFrom: string, application: ApplicationInformation) {
    let result = false;
    if (CalledFrom == "inputField") {
      if (this.applicationInformation.ApplicationStatus == "Approved" ||
        this.applicationInformation.ApplicationStatus == "Terminated" ||
        this.applicationInformation.ApplicationStatus == "Suspended" ||
        this.applicationInformation.ApplicationStatus == "Rejected" ||
        this.applicationInformation.ApplicationStatus == "Observation" ||
        this.applicationInformation.ApplicationStatus == "Cancelled") {
        result = true;
      }
    } else if (CalledFrom == "Conditional") {
      if (this.applicationInformation.ApplicationStatus != 'Approved' && this.applicationInformation.ApplicationStatus != 'Rejected'
        && this.applicationInformation.ApplicationStatus != 'Cancelled' && this.applicationInformation.ApplicationStatus != 'Suspended'
        && this.applicationInformation.ApplicationStatus != 'Terminated' && this.applicationInformation.ApplicationStatus != 'Observation') {
        result = true;
      }
    } else if (CalledFrom == "HideUploadLink") {
      let ProductApprovedCount = application.Products.filter(i => i.IsApproved == true);
      let FactoryApprovedCount = application.Factories.filter(i => i.IsApproved == true);
      let TotalApprovedCount = ProductApprovedCount.length + FactoryApprovedCount.length;
      let TotalCount = application.Products.length + application.Factories.length;
      if (TotalApprovedCount == TotalCount) {
        result = true;

      }
    } else if (CalledFrom == "Submit") {
      if (this.applicationInformation.ApplicationStatus == 'Approved') {
        if(this.applicationInformation.CertificateExpiryDate >= this.applicationInformation.CurrentDate)
        result = true;
      }else{
        result = true;
      }
    }
    return result;
  }

  reset() {
    // (<any>$("input[type='file']")).val('')
    // (<any>$('#inputfileValue')).val('');
    this.fileUploader.nativeElement.inputfileValue= null;
  }

  CheckExistingFile(calledFrom, FileName, FileType, RefArray) {
    let result: boolean = true;
    if (calledFrom == 'FilledApplicationForm') {
      if (this.applicationInformation.FilledApplicationForm && this.applicationInformation.FilledApplicationForm != '') {
        let tempArray = this.applicationInformation.FilledApplicationForm.split('ÿ')
        let index = tempArray.findIndex(i => i == FileName)
        if (index > -1) {
          result = false
          this.reset();
        }
      }
    } else if (calledFrom == 'product' || calledFrom == 'factory') {
      let filterArray = RefArray.filter(i => i.DocumentType == FileType && i.DocumentName == FileName && i.IsDeleted == false)
      if (filterArray.length > 0) {
        result = false;
        this.isLoader = false;
        swal('', 'This file is already exist in this ' + calledFrom + ' type');
        this.reset();
      }
    } else if (calledFrom == 'review') {
      let filterArray = RefArray.filter(i => i.ServiceType == this.reportDetail.ServiceType && i.ReportType == this.reportDetail.ReportType
        && i.FileName == this.reportDetail.FileName)
      if (filterArray.length > 0) {
        result = false;
        this.isLoader = false;
        swal('', 'This file is already exist in this ' + calledFrom + ' type');
        this.reset();
      }
    }
    return result;
  }

  resetReviewReportsFileField(value){
    let temp ='#'+value;
  (<any>$(temp)).val('');
  }

}
