import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InternalReviewComponent } from 'app/green-product-assurance/internal-review/internal-review.component';

const routes: Routes = [{
  path:'',
  component:InternalReviewComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InternalReviewRoutingModule { }
