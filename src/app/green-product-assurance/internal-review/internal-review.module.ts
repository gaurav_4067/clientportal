
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InternalReviewRoutingModule } from './internal-review-routing.module';
import { InternalReviewComponent } from './internal-review.component';
import { FormsModule } from '@angular/forms';
import {TooltipModule} from "ngx-tooltip";
import {DateTimePickerModule} from 'ng-pick-datetime';
import { HeaderModule } from 'app/header/header.module';
 
// import { GpdocumentFilterArrayPipe } from 'app/pipe/gpdocument-filter-array.pipe';
import { Ng2PaginationModule } from 'ng2-pagination';
import { FormatBusinessCategoryModule } from 'app/helper/format-business-category/format-business-category.module';


@NgModule({
  imports: [
    CommonModule,
    InternalReviewRoutingModule,
    FormsModule,
    TooltipModule,
    HeaderModule,
    DateTimePickerModule,
    Ng2PaginationModule,
    FormatBusinessCategoryModule
  ],
  declarations: [InternalReviewComponent]
})












export class InternalReviewModule { }
