import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GreenProductAssuranceRoutingModule } from './green-product-assurance-routing.module';
import { NoContentModule } from 'app/no-content/no-content.module';

@NgModule({
  imports: [
    CommonModule,
    GreenProductAssuranceRoutingModule,
    NoContentModule,
  ],
  declarations: []
})
export class GreenProductAssuranceModule { }
