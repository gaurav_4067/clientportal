import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewApplicationComponent } from 'app/green-product-assurance/new-application/new-application.component';

const routes: Routes = [
  {
    path:'',
    component:NewApplicationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewApplicationRoutingModule { }
