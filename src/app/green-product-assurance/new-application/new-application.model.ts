
export class ApplicationInformation {
    constructor() {
        this.Products = new Array<ApplicationProductDetail>();
        this.Factories = new Array<ApplicationFactoryDetail>();
        this.ReviewReports = new Array<ReviewReportDetail>();
    }
    public ApplicationInfoMasterID: number;
    public ApplicationReferenceNo: string;
    public BaseReferenceNo: string;
    public TempReferenceNo: string;
    public VendorName: string;
    public VendorCode: string;
    public ContactPerson: string;
    public Title: string;
    public Telephone: string;
    public Email: string;
    public Address: string;
    public VendorName_Chinese: string;
    public Address_Chinese: string;;
    public VendorCode_Chinese: string;
    public ContactPerson_Chinese: string;
    public Title_Chinese: string;
    public Telephone_Chinese: string;
    public Email_Chinese: string;
    public Status: string;
    public ApplicationStatus: string;
    public CertificateIssuedDate: Date;
    public CertificateExpiryDate: Date;
    public CertificateNumber: string;
    public CompanyLogo: string;
    public CertificateFile: string;
    public AdditionalComment: string;
    public DocumentReviewerComment: string;
    public TechnicalReviewerComment: string;
    public FilledApplicationForm: string;
    public CancelledRemark: string;
    public RejectionRemark: string;
    public IsRejected: boolean;
    public IsCancelled: boolean;
    public RejectedTime: Date;
    public CancelledTime: Date;
    public Active: boolean;
    public CreatedBy: number;
    public CreatedDate: Date;
    public ModifiedBy: number;
    public ModifiedDate: Date;
    public ClientCompanyId: number;
    public ClientCompanyName: string;
    public ClientCompanyAddress1: string;
    public ClientCompanyAddress2: string;
    public ClientCompanyPhone: string;
    public ClientCompanyState: string;
    public ClientCompanyZip: string;
    public ClientCompanyCity: string;
    public IsFST_Approved: boolean;
    public IsFST_Rejected: boolean;
    public IsFSA_Approved: boolean;
    public IsFSA_Rejected: boolean;
    public IsMST_Approved: boolean;
    public IsMST_Rejected: boolean;
    public FST_Status: string;
    public FSA_Status: string;
    public MST_Status: string;
    public IsWithQRCode: boolean = true;
    public ApplicationType: string;
    public RevisedDate: Date;
    public RenewDate: Date;
    public RevisedApplicationCount: number;
    public ValidityStatus: string;
    public ValidityTime: string;
    public CurrentDate: Date;
    public Products: Array<ApplicationProductDetail>;
    public Factories: Array<ApplicationFactoryDetail>;
    public ReviewReports: Array<ReviewReportDetail>;

}

export class ApplicationProductDetail {
    constructor() {
        this.ProductDocuments = new Array<ProductDocument>();
        this.ReviewReports = new Array<ReviewReportDetail>();
    }
    public ApplicationProdDetailID: number;
    public ApplicationInfoMasterID: number;
    public ProductType: string;
    public ProductName: string;
    public ProductModelNo: string;
    public ProductWeight: string;
    public Total_No_Of_Factory: number
    public ProductSealNo: string;
    public ProductSealLogoName: string;
    public ProductDocumentStatus: boolean;
    public Active: boolean;
    public CreatedBy: number;
    public CreatedDate: Date;
    public ModifiedBy: number;
    public ModifiedDate: Date;
    public IsDeleted: boolean = false;
    public IsVerified: boolean = false;
    public STDocumentName: string;
    public IsRejected: boolean
    public IsApproved: boolean
    public IsSTApproved: boolean
    public IsSTRejected: boolean
    public ProductFolderName: string
    public ProductDocuments: Array<ProductDocument>;
    public ReviewReports: Array<ReviewReportDetail>;
}



export class ApplicationFactoryDetail {
    constructor() {
        this.FactoryDocuments = new Array<FactoryDocument>();
        this.ReviewReports = new Array<ReviewReportDetail>();
    }
    public ApplicationFactoryDetailID: number;
    public ApplicationInfoMasterID: number;
    public FactoryName: string;
    public FactoryAddress: string;
    public FactoryCode: string;
    public ProductionProcess: string;
    public HeadCounts: number;
    public FactoryDocumentStatus: boolean;
    public Area: string;
    public FactoryContactPerson: string;
    public Title: string;
    public FactoryTelephone: string;
    public FactoryEmail: string;
    public Active: boolean;
    public CreatedBy: number;
    public CreatedDate: Date;
    public ModifiedBy: number;
    public ModifiedDate: Date;
    public IsDeleted: boolean = false;
    public IsVerified: boolean = false;
    public FADocumentName: string;
    public IsRejected: boolean
    public IsApproved: boolean
    public IsFAApproved: boolean
    public IsFARejected: boolean
    public FactoryFolderName: string
    public FactoryDocuments: Array<FactoryDocument>;
    public ReviewReports: Array<ReviewReportDetail>;
}

export class FactoryDocument {
    public ApplicationFactoryDocumentID: number;
    public ApplicationFactoryDetailID: number;
    public DocumentType: string;
    public DocumentName: string;
    public IsVerified: boolean;
    public Active: boolean;
    public CreatedBy: number;
    public CreatedDate: Date;
    public ModifiedBy: number;
    public ModifiedDate: Date;
    public IsDeleted: boolean = false;
}

export class ProductDocument {
    public ApplicationProductDocumentID: number;
    public ApplicationProdDetailID: number;
    public DocumentType: string;
    public DocumentName: string;
    public IsVerified: boolean;
    public Active: boolean;
    public CreatedBy: number;
    public CreatedDate: Date;
    public ModifiedBy: number;
    public ModifiedDate: Date;
    public IsDeleted: boolean = false;
}

export class DeleteDocumentRequest {
    public ModelNo: string;
    public Guid: string;
    public Type: string;
    public DocumentType: string;
    public DocumentName: string;
    public DeleteIndividual: boolean;
}


export class ReviewReportDetail {
    public ReportId: number;
    public ReportType: string;
    public FileName: string;
    public ApplicationId: number;
    public ServiceType: string;
    public ServiceCode: string;
    public ServiceId: number;
    public Result: string;
    public Active: boolean;
    public IsDeleted: boolean = false;
    public CreatedBy: number;
    public CreatedDate: Date;
}