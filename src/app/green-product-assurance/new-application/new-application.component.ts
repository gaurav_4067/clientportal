
import { Component, OnInit, ViewChild,ElementRef } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import { Router } from '@angular/router';
import { ApplicationInformation, ApplicationProductDetail, ApplicationFactoryDetail, DeleteDocumentRequest } from './new-application.model'
import { NgForm } from '@angular/forms';
import { BaseUrl, DataService } from 'app/mts.service';
import { debug } from 'util';

declare let swal: any;

@Component({
  selector: 'app-new-application',
  templateUrl: './new-application.component.html',
  styleUrls: ['./new-application.component.css']
})
export class NewApplicationComponent implements OnInit {
  isLoader: boolean;
  @ViewChild('fileUpload') fileUploader: ElementRef;
  @ViewChild('productDetailForm') productDetailForm: NgForm;
  @ViewChild('factoryForm') factoryForm: NgForm;
  @ViewChild('applicantDetailForm') applicantDetailForm: NgForm;
  applicantDetailFormSubmitted: boolean = false;
  productDetailFormSubmitted: boolean = false;
  factoryDetailFormSubmitted: boolean = false;
  userCredential: any;
  userDetail: any;
  editProduct: boolean = false;
  editProductIndex: number;
  editFactory: boolean = false;
  editFactoryIndex: any;
  fileToUpload: any;
  validateProductModelNo: boolean = false;
  disableProductModelNo: boolean = false;
  validateFactoryName: boolean = false;
  disablefactoryCode: boolean = false;
  applicationInformation: ApplicationInformation;
  productInformation: ApplicationProductDetail;
  factoryInformation: ApplicationFactoryDetail;
  deleteDocumentRequest: DeleteDocumentRequest;
  ProductFolderName: string;
  FactoryFolderName: string;

  constructor(private http: Http,
    private dataService: DataService,
    private router: Router) {
    this.applicationInformation = new ApplicationInformation();
    this.productInformation = new ApplicationProductDetail();
    this.factoryInformation = new ApplicationFactoryDetail();
  }

  ngOnInit() {
    this.isLoader = true;
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    if (this.userDetail && this.userCredential) {
      if (this.userCredential.UserPermission.CreateGPApplication) {
        this.applicationInformation.CreatedBy = this.userCredential.UserId;
        this.dataService.get('GP_ApplicationRegistration/GenerateAndVerifyGuid')
          .subscribe(res => {
            if (res.IsSuccess) {
              this.isLoader = false;
              this.applicationInformation.TempReferenceNo = res.Data;
            } else {
              swal('', res.Message);
            }
          });
      } else {
        this.router.navigate(['./landing-page']);
      }
    } else {
      this.router.navigate(['./login']);
    }
  }

  //************methods for back and forth handling******************

  ContinueApplicantDetail() {
    this.applicantDetailFormSubmitted = true;
    if (this.applicationInformation.VendorName && this.applicationInformation.VendorCode && this.applicationInformation.Email) {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (!reg.test(this.applicationInformation.Email) == false) {
        (<any>$('#ApplicantDetails')).addClass('fadeOutLeft');
        (<any>$('#ApplicantDetails')).addClass('SlideOut1');
        (<any>$('#Products')).addClass('active');
        (<any>$('#ProductDetails')).removeClass('SlideOut');
      }
    }

  }

  ContinueProduct() {
    if (this.applicationInformation.Products.length > 0) {
      (<any>$('#ProductDetails')).addClass('fadeOutLeft');
      (<any>$('#ProductDetails')).addClass('SlideOut1');
      (<any>$('#Factoreis')).addClass('active');
      (<any>$('#FactoryDetails')).removeClass('SlideOut');
    } else {
      swal('', 'Please add atleast one product');
    }
  }

  ContinueFactory() {
    if (this.applicationInformation.Factories.length > 0) {
      (<any>$('#FactoryDetails')).addClass('fadeOutLeft');
      (<any>$('#FactoryDetails')).addClass('SlideOut1');
      (<any>$('#AditionalDetail')).addClass('active');
      (<any>$('#AditionalDetails')).removeClass('SlideOut');
    } else {
      swal('', 'Please add atleast one factory');
    }
  }

  additionalback() {
    (<any>$('#FactoryDetails')).removeClass('fadeOutLeft');
    (<any>$('#FactoryDetails')).addClass('fadeInLeft');
    (<any>$('#AditionalDetails')).removeClass('fadeOutLeft');
    (<any>$('#AditionalDetail')).removeClass('active');
    (<any>$('#ProductDetails')).addClass('SlideOut');
  }

  Factoryback() {
    (<any>$('#ProductDetails')).removeClass('fadeOutLeft');
    (<any>$('#ProductDetails')).addClass('fadeInLeft');
    (<any>$('#ProductDetails')).addClass('SlideOut');
    (<any>$('#FactoryDetails')).removeClass('fadeOutLeft');
    (<any>$('#Factoreis')).removeClass('active');
  }

  Productback() {
    (<any>$('#ApplicantDetails')).removeClass('fadeOutLeft');
    (<any>$('#ApplicantDetails')).addClass('fadeInLeft');
    (<any>$('#ProductDetails')).removeClass('fadeOutLeft');
    (<any>$('#Products')).removeClass('active');
    (<any>$('#ApplicantDetails')).addClass('SlideOut');
  }

  //************methods for manage product details******************
  pad2(n) {
    return (n < 10 ? '0' : '') + n;
  }
  addNewProductDetails() {
    this.productDetailFormSubmitted=false;
    this.editProduct = false;
    this.disableProductModelNo = false;
    this.validateProductModelNo = false;
    this.productInformation = new ApplicationProductDetail();
    let currentdate = new Date();
    let formatedDate = currentdate.getFullYear() +
      this.pad2(currentdate.getMonth() + 1) +
      this.pad2(currentdate.getDate()) +
      this.pad2(currentdate.getHours()) +
      this.pad2(currentdate.getMinutes()) +
      this.pad2(currentdate.getSeconds());
    this.productInformation.ProductFolderName = formatedDate;
    (<any>$('#productDetailModal')).modal('show');
  }

  EditProductDetail(i) {
    this.productDetailFormSubmitted=false;
    this.productInformation = new ApplicationProductDetail();
    this.productInformation = this.applicationInformation.Products[i];
    this.productInformation = JSON.parse(JSON.stringify(this.productInformation));
    this.editProductIndex = i;
    this.editProduct = true;
    this.disableProductModelNo = true;
    this.validateProductModelNo = false;
    (<any>$('#productDetailModal')).modal('show');
  }

  addProduct() { 
    this.productDetailFormSubmitted=true;
    if (this.productInformation.ProductType && this.productInformation.ProductName && this.productInformation.ProductModelNo
      && this.productInformation.ProductWeight) {
      if (this.ValidateProduct() && this.validateModelNo(this.productInformation.ProductModelNo)) {
        if (this.editProduct) {
          this.applicationInformation.Products[this.editProductIndex] = Object.assign(new ApplicationProductDetail(), this.productInformation);
          this.productDetailForm.resetForm(this.productInformation);
          this.productInformation = new ApplicationProductDetail();
          (<any>$('#productDetailModal .close')).click();
        } else {
          this.applicationInformation.Products.push(Object.assign(new ApplicationProductDetail(), this.productInformation));
          this.productDetailForm.resetForm(this.productInformation);
          this.productInformation = new ApplicationProductDetail();
          (<any>$('#productDetailModal .close')).click();
        }
      }
    }
  }

  ValidateProduct(): boolean {
    if (this.productInformation.ProductDocuments) {
      let ProdImageIndex = this.productInformation.ProductDocuments.findIndex(
        i => i.DocumentType == 'ProductImage'
      );
      if (ProdImageIndex > -1) {
      } else {
        swal('', 'Please upload atleast one product image');
        return false;
      }

      let BrochureIndex = this.productInformation.ProductDocuments.findIndex(
        i => i.DocumentType == 'Brochure'
      );
      if (BrochureIndex > -1) {
      } else {
        swal('', 'Please upload atleast one brochure');
        return false;
      }

      let BillOfMaterialIndex = this.productInformation.ProductDocuments.findIndex(
        i => i.DocumentType == 'BillOfMaterial'
      );
      if (BillOfMaterialIndex > -1) {
        return true;
      } else {
        swal('', 'Please upload atleast one bill of material');
        return false;
      }

    } else {
      swal('', 'Please upload atleast one product documnet for each type');
      return false;
    }
  }

  validateModelNo(value: string) {
    let result = true;
    let index = this.applicationInformation.Products.findIndex(
      i => i.ProductModelNo == value
    );
    if (index > -1 && !this.editProduct) {
      this.validateProductModelNo = true;
      result = false;
      swal('', 'Model No :' + value + ' is already exsit in product list');
    }
    else {
      this.validateProductModelNo = false;
    }
    var reg = /^[a-zA-Z0-9,{}.+=^&*()%$#@_+?!\//-|/-\s]*$/;
    if(reg.test(value) == false){
      result =false;
    }

    return result;
  }

  UploadProductDocumnets(event: any, calledFrom: any) {
    this.isLoader = true;
    this.fileToUpload = [];
    this.fileToUpload.push(event.target.files[0]);
    let docType = calledFrom
    let url = 'GP_ApplicationRegistration/UploadGpDocument/Product/' + docType + '/'
    if (this.validateFileExtension(calledFrom) && this.CheckExistingFile('product', event.target.files[0].name, docType, this.productInformation.ProductDocuments)) {
        this.fileUploadRequest(BaseUrl + url, this.fileToUpload, this.productInformation.ProductFolderName).then(
          (result: any) => {
            let res = JSON.parse(result);
            if (res.IsSuccess) {
              this.fileToUpload = [];
              let tempArray = {
                DocumentType: docType,
                DocumentName: event.target.files[0].name,
                ApplicationProductDocumentID: 0,
                ApplicationProdDetailID: 0,
                IsVerified: false,
                Active: false,
                CreatedBy: this.userCredential.UserId,
                CreatedDate: new Date(),
                ModifiedBy: 0,
                ModifiedDate: new Date(),
                IsDeleted: false
              }
              if (this.editProduct) {
                this.applicationInformation.Products[this.editProductIndex].ProductDocuments.push(tempArray);
              }
              this.productInformation.ProductDocuments.push(tempArray);
              this.isLoader = false;
              swal('', res.Message);
              this.reset();
            } else {
              this.isLoader = false;
              swal('', res.Message);
              this.reset();
            }
        
          },
          (error) => {
          })

    }
  }


  validateFileExtension(calledFrom): boolean {
    let result = true;
    if (calledFrom == 'ProductImage') {
      if (this.fileToUpload.length) {
        this.fileToUpload.forEach((file) => {
          if (file) {
            let extension = file.name.substr(file.name.lastIndexOf('.') + 1);
            if (['JPG', 'PNG', 'jpg', 'png'].indexOf(extension.toLowerCase()) > -1) {
            } else {
              this.isLoader = false;
              swal('', 'Please Upload only .JPG, .PNG, .jpg, .png files');
              result = false;
            }
            let Name = file.name.substr(0, file.name.lastIndexOf('.'));
            var reg = /^[a-zA-Z0-9-_\s()]+[a-zA-Z0-9-_\s()]*$/;
            if (reg.test(Name) == true) {
            } else {
              this.isLoader = false;
              swal('', 'file name with  special character is not allowed');
              result = false;
            }
          }
        })
      }
    }
    return result;
  }

  deleteProductDocument(index, type) {
    let ready = true;
    if (this.editProduct && type != 'CAP' && type != 'MaterialTestReport') {
      let tempArray = this.applicationInformation.Products[this.editProductIndex].ProductDocuments.filter(i => i.DocumentType == type)
      if (tempArray.length > 1) {
        ready = true;
      } else {
        ready = false;
        swal('', type + ' should be atleast one');
      }
    }
    if (ready) {
      this.deleteDocumentRequest = {
        ModelNo: this.productInformation.ProductFolderName,
        Guid: this.applicationInformation.TempReferenceNo,
        Type: 'Products',
        DocumentName: this.productInformation.ProductDocuments[index].DocumentName,
        DocumentType: this.productInformation.ProductDocuments[index].DocumentType,
        DeleteIndividual: true
      }
      this.dataService.post('GP_ApplicationRegistration/DeleteDocument', this.deleteDocumentRequest)
        .subscribe(response => {
          let res = JSON.parse(response);
          if (res.IsSuccess) {
            this.productInformation.ProductDocuments.splice(index, 1);
            if (this.editProduct) {
              this.applicationInformation.Products[this.editProductIndex].ProductDocuments.splice(index, 1);
            }
            swal('', res.Message);
          } else {
            swal('', res.Message);
          }
        });
    }

  }

  deleteProductDetail(index) {
    this.deleteDocumentRequest = {
      ModelNo: this.applicationInformation.Products[index].ProductFolderName,
      Guid: this.applicationInformation.TempReferenceNo,
      Type: 'Products',
      DocumentName: "",
      DocumentType: "",
      DeleteIndividual: false
    };
    this.dataService.post('GP_ApplicationRegistration/DeleteDocument', this.deleteDocumentRequest)
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          this.applicationInformation.Products.splice(index, 1)
          swal('', res.Message);
        } else {
          swal('', res.Message);
        }
      });
  }

  ResetProductForm() {
    this.productDetailForm.resetForm(this.productInformation);
    this.productInformation = new ApplicationProductDetail();
  }


  //************methods for manage factory detail******************

  addNewFactoryDetail() {
    this.factoryDetailFormSubmitted=false;
    this.editFactory = false;
    this.disablefactoryCode = false;
    this.validateFactoryName = false;
    this.factoryInformation = new ApplicationFactoryDetail();
    let currentdate = new Date();
    let formatedDate = currentdate.getFullYear() +
      this.pad2(currentdate.getMonth() + 1) +
      this.pad2(currentdate.getDate()) +
      this.pad2(currentdate.getHours()) +
      this.pad2(currentdate.getMinutes()) +
      this.pad2(currentdate.getSeconds());
    this.factoryInformation.FactoryFolderName = formatedDate;
    (<any>$('#FactoryDetailModal')).modal('show');
  }

  EditFactoryDetail(i) {
    this.factoryDetailFormSubmitted=false;
    this.factoryInformation = JSON.parse(JSON.stringify(this.applicationInformation.Factories[i]));
    this.editFactoryIndex = i
    this.editFactory = true;
    this.disablefactoryCode = true;
    this.validateFactoryName = false;
    (<any>$('#FactoryDetailModal')).modal('show');
  }

  addFactory() {
    this.factoryDetailFormSubmitted=true;
    if (this.factoryInformation.FactoryName && this.factoryInformation.FactoryCode) {
      if (this.ValidateFactory() && this.validateCode(this.factoryInformation.FactoryCode)) {
        if (this.editFactory) {
          this.applicationInformation.Factories[this.editFactoryIndex] = Object.assign(new ApplicationFactoryDetail(), this.factoryInformation);
          this.factoryForm.resetForm(this.factoryInformation);
          (<any>$('#FactoryDetailModal .close')).click();
        } else {
          this.applicationInformation.Factories.push(Object.assign(new ApplicationFactoryDetail(), this.factoryInformation));
          this.factoryForm.resetForm(this.factoryInformation);
          (<any>$('#FactoryDetailModal .close')).click();
        }
      }
    }
  }

  ValidateFactory(): boolean {
    if (this.factoryInformation.FactoryDocuments) {
      let FlowChartIndex = this.factoryInformation.FactoryDocuments.findIndex(
        i => i.DocumentType == 'FlowChart'
      );
      if (FlowChartIndex > -1) {
        return true;
      } else {
        swal('', 'Please upload atleast one  factory flow-chart');
        return false;
      }
    } else {
      swal('', 'Please upload atleast one factory documnet');
      return false;
    }
  }

  validateCode(value: string) {
    let result = true;
    this.validateFactoryName = false;
    let index = this.applicationInformation.Factories.findIndex(
      i => i.FactoryCode == value
    )
    if (index > -1 && !this.editFactory) {
      this.validateFactoryName = true;
      result = false;
      swal('', 'Factory Code :' + value + ' is already exsit in factory list');
    } else {
      this.validateFactoryName = false;
    }
    return result;
  }

  UploadFactoryDocument(event: any, calledFrom: any) {
    this.isLoader = true;
    this.fileToUpload = [];
    this.fileToUpload.push(event.target.files[0]);
    let docType = calledFrom
    let url = 'GP_ApplicationRegistration/UploadGpDocument/Factory/' + docType + '/'
    if (this.CheckExistingFile('factory', event.target.files[0].name, docType, this.factoryInformation.FactoryDocuments)) {
      this.fileUploadRequest(BaseUrl + url, this.fileToUpload, this.factoryInformation.FactoryFolderName).then(
        (result: any) => {
          let res = JSON.parse(result);
          if (res.IsSuccess) {
            this.fileToUpload = [];
            //  this.disablefactoryCode = true;
            let tempArray = {
              DocumentType: docType,
              DocumentName: event.target.files[0].name,
              ApplicationFactoryDocumentID: 0,
              ApplicationFactoryDetailID: 0,
              IsVerified: false,
              Active: false,
              CreatedBy: this.userCredential.UserId,
              CreatedDate: new Date(),
              ModifiedBy: 0,
              ModifiedDate: new Date(),
              IsDeleted: false
            }
            if (this.editFactory) {
              this.applicationInformation.Factories[this.editFactoryIndex].FactoryDocuments.push(tempArray);
            }
            this.factoryInformation.FactoryDocuments.push(tempArray);
            this.isLoader = false;
            swal('', res.Message);
            this.reset();
          } else {
            this.isLoader = false;
            swal('', res.Message);
            this.reset();
          }
        },
        (error) => {
        })
    }
  }

  deleteFactoryDocument(index, type) {
    let ready = true;
    if (this.editFactory && type == 'FlowChart') {
      let tempArray = this.applicationInformation.Factories[this.editFactoryIndex].FactoryDocuments.filter(i => i.DocumentType == type)
      if (tempArray.length > 1) {
        ready = true;
      } else {
        ready = false;
        swal('', type + ' should be atleast one');
      }
    }
    if (ready) {
      this.deleteDocumentRequest = {
        ModelNo: this.factoryInformation.FactoryFolderName,
        Guid: this.applicationInformation.TempReferenceNo,
        Type: 'Factories',
        DocumentName: this.factoryInformation.FactoryDocuments[index].DocumentName,
        DocumentType: this.factoryInformation.FactoryDocuments[index].DocumentType,
        DeleteIndividual: true
      }
      this.dataService.post('GP_ApplicationRegistration/DeleteDocument', this.deleteDocumentRequest)
        .subscribe(response => {
          let res = JSON.parse(response);
          if (res.IsSuccess) {
            this.factoryInformation.FactoryDocuments.splice(index, 1)
            if (this.editFactory) {
              this.applicationInformation.Factories[this.editFactoryIndex].FactoryDocuments.splice(index, 1);
            }
            swal('', res.Message)
          } else {
            swal('', res.Message)
          }
        });
    }
  }

  deleteFactoryDetail(index) {
    this.deleteDocumentRequest = {
      ModelNo: this.applicationInformation.Factories[index].FactoryFolderName,
      Guid: this.applicationInformation.TempReferenceNo,
      Type: 'Factories',
      DocumentName: "",
      DocumentType: "",
      DeleteIndividual: false
    };
    this.dataService.post('GP_ApplicationRegistration/DeleteDocument', this.deleteDocumentRequest)
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          this.applicationInformation.Factories.splice(index, 1);
          swal('', res.Message);
        } else {
          swal('', res.Message);
        }
      });
  }

  ResetFactoryForm() {
    this.factoryForm.resetForm(this.factoryInformation);
    this.factoryInformation = new ApplicationFactoryDetail();
  }

  //************other methods******************

  UplaodFilledApplicationForm(event: any) {
    this.isLoader = true;
    var url: string;
    let docType: string;
    this.fileToUpload = [];
    this.fileToUpload.push(event.target.files[0]);
    url = 'GP_ApplicationRegistration/UploadGpDocument/FA/FilledApplicationForm/';
    docType = 'FA';
    if (this.CheckExistingFile('FilledApplicationForm', event.target.files[0].name, '', [])) {
      this.fileUploadRequest(BaseUrl + url, this.fileToUpload, 0).then(
        (result: any) => {
          let res = JSON.parse(result);
          if (res.IsSuccess) {
            this.fileToUpload = [];
            // this.applicationInformation.FilledApplicationForm = event.target.files[0].name;
            if (!this.applicationInformation.FilledApplicationForm || this.applicationInformation.FilledApplicationForm == '') {
              this.applicationInformation.FilledApplicationForm = event.target.files[0].name;
            } else {
              this.applicationInformation.FilledApplicationForm = this.applicationInformation.FilledApplicationForm + 'ÿ' + event.target.files[0].name;
            }
            this.isLoader = false;
            swal('', res.Message);
            // this.reset();
          } else {
            swal('', res.Message);
            // this.reset();
          }
        })
    } else {
      this.isLoader = false;
      swal('', 'This file is already exist!');
    }
  }

  fileUploadRequest(url: string, files: Array<File>, code: any) {
    let thisObj = this;
    var formData: any = new FormData();
    let xhr = new XMLHttpRequest();
    for (let i = 0; i < files.length; i++) {
      formData.append('UploadReport-' + i, files[i]);
    }
    formData.append("Guid", this.applicationInformation.TempReferenceNo);
    formData.append("ModelNo", code);
    return new Promise((resolve, reject) => {

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            (<any>$('#inputfileValue')).val('');
            resolve(JSON.parse(xhr.response))
          }
          else {
            (<any>$('#inputfileValue')).val('');
            reject(JSON.parse(xhr.response));
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    })
  }

  saveApplicantInformation() {
    this.isLoader = true;
    this.applicationInformation.CreatedBy = this.userCredential.UserId,
      this.applicationInformation.ApplicationType = "Registered"
    this.dataService.post('GP_ApplicationRegistration/SubmitRegistration', this.applicationInformation)
      .subscribe(result => {
        let res = JSON.parse(result);
        if (res.IsSuccess) {
          let that = this;
          this.isLoader = false;
          swal({
            title: "",
            text: "<span style='color:#797979;font-size:18px;'>" + res.Message + "</span><br>"
              + "<span style='color:#787878;font-weight: 600;font-size:15px;'> Applicant Reference No. :</span><span style='color:#797979;font-weight: 600;font-size:15px;'> <b> " + " " + res.Data.ApplicationReferenceNo + "</b></span><br>",
            html: true,
            confirmButtonText: "OK",
            closeOnConfirm: true,
          }, function (isConfirm) {
            if (isConfirm) {
              that.router.navigate(['/green-product-assurance/internal-view']);
            }
          }
          );
        } else {
          this.isLoader = false;
          swal('', res.Message);
        }
      });
  }

  restrictEKey(event) {
    if (event.keyCode === 69 || event.keyCode === 190) {
      return false;
    }
  }

  validateSpace(value: string) {
    this.validateProductModelNo = false;
    this.validateFactoryName = false;
    if (value = 'Product')
      if (this.productInformation.ProductModelNo) {
        this.productInformation.ProductModelNo = this.productInformation.ProductModelNo.trim();
        this.productInformation.ProductModelNo = this.productInformation.ProductModelNo.replace(/  +/g, ' ');
      } else {
        if (this.factoryInformation.FactoryCode) {
          this.factoryInformation.FactoryCode = this.factoryInformation.FactoryCode.trim();
          this.factoryInformation.FactoryCode = this.factoryInformation.FactoryCode.replace(/  +/g, ' ');
        }
      }
  }

  removedfilledApplicationForm(docName) {
    this.deleteDocumentRequest = {
      ModelNo: '',
      Guid: this.applicationInformation.TempReferenceNo,
      Type: 'FA',
      DocumentName: docName,
      DocumentType: 'FilledApplicationForm',
      DeleteIndividual: true
    }
    this.dataService.post('GP_ApplicationRegistration/DeleteDocument', this.deleteDocumentRequest)
      .subscribe(response => {
        let res = JSON.parse(response);
        if (res.IsSuccess) {
          let splitArr = this.applicationInformation.FilledApplicationForm.split("ÿ");
          let index = splitArr.findIndex(i => i == docName);
          splitArr.splice(index, 1);
          this.applicationInformation.FilledApplicationForm = splitArr.join("ÿ");
          swal('', res.Message);
        } else {
          swal('', res.Message);
        }
      });
  }

  CheckExistingFile(calledFrom, FileName, FileType, RefArray) {
    let result: boolean = true;
    if (calledFrom == 'FilledApplicationForm') {
      if (this.applicationInformation.FilledApplicationForm && this.applicationInformation.FilledApplicationForm != '') {
        let tempArray = this.applicationInformation.FilledApplicationForm.split('ÿ')
        let index = tempArray.findIndex(i => i == FileName)
        if (index > -1) {
          result = false
          this.reset();
        }
      }
    } else if (calledFrom == 'product' || calledFrom == 'factory') {
      let filterArray = RefArray.filter(i => i.DocumentType == FileType && i.DocumentName == FileName)
      if (filterArray.length > 0) {
        result = false;
        this.isLoader = false;
        swal('', 'This file is already exist in this ' + calledFrom + ' type');
        this.reset();
      }
    }
    return result;
  }

  reset() {
  //  (<any>$("input[type='file']")).val(null);
  //  this.fileUploader.nativeElement.value = "";
  this.fileUploader.nativeElement.value = '';
 
  }

}

