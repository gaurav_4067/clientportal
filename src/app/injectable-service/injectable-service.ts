/**
 * Created by azmat on 26/12/16.
 */

import { UserModel } from '../common/userModel';
import { RoleModel } from '../common/roleModel';

export class InjectService {
    public userModel: UserModel;
    public roleModel:RoleModel;
    public additionalSearch:any;
}
