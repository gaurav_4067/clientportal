import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import {Router} from '@angular/router';
import {BaseUrl, DataService, ETRFUrl} from 'app/mts.service';

declare let swal: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [DataService],
})
export class LoginComponent implements OnInit {
  public user: any = {userName: '', password: ''};
  public userCredential: any;
  statusEtrf: any;
  isLoader: boolean;
  testingCompaniesCount: number;
  inspectionCompaniesCount: number;
  constructor(
      private dataService: DataService, public router: Router,
      private http: Http) {}

  ngOnInit() {
    this.userCredential = {UserId: 0, CompanyId: 0, UserCategoryId: 0};
  }
  login() {
    var that = this;
    this.isLoader = true;
    let UserCompanyIdCondition = 0;

    this.http
        .post(
            BaseUrl + 'user/Authentication',
            {Email: this.user.userName, Password: this.user.password})
        .subscribe(response => {
          let res = response.json();
          if (res.AssignedCompanyANDCategory.AssignedCompanyList.length > 0) {
            that.testingCompaniesCount =
                res.AssignedCompanyANDCategory.AssignedCompanyList
                    .filter(i => i.BusinessCategoryID === 1)
                    .length;
            that.inspectionCompaniesCount =
            res.AssignedCompanyANDCategory.AssignedCompanyList
                .filter(i => i.BusinessCategoryID === 2)
                .length;
          }
          UserCompanyIdCondition = res.UserCategoryId;
          this.isLoader = false;
          that.statusEtrf = res.Status;
          if (res.IsSuccess) {
            localStorage.setItem('userDetail', JSON.stringify(res));
            if (res.IsClient) {
              this.userCredential.UserId = res.UserId;
              this.userCredential.CompanyId = res.CompanyId;
              this.userCredential.UserCategoryId = res.UserCategoryId;
              this.userCredential.UserType = res.UserCategoryName;  //'Client';
              this.userCredential.UserCompanyId = res.CompanyId;
              this.userCredential.CompanyName = res.CompanyName;
              this.userCredential.CompanyLogo = ETRFUrl + res.CompanyLogo;
              if (res.AssignedCompanyANDCategory.length > 0) {
                this.userCredential.BusinessCategoryId =
                    res.AssignedCompanyANDCategory.AssignedBusinessCategory[0]
                        .BusinessCategoryId;
              } else {
                this.userCredential.BusinessCategoryId = 1;
              }
            }
            if (res.AssignedCompanyANDCategory.AssignedCompanyList.length ==
                    1 ||
                this.testingCompaniesCount == 1) {
              this.userCredential.UserId = res.UserId;
              this.userCredential.CompanyId =
                  res.AssignedCompanyANDCategory.AssignedCompanyList[0]
                      .CompanyId;
              this.userCredential.UserCategoryId = res.UserCategoryId;
              this.userCredential.UserType =
                  res.AssignedCompanyANDCategory.AssignedCompanyList[0]
                      .WorkingAs;
              this.userCredential.CompanyName =
                  res.AssignedCompanyANDCategory.AssignedCompanyList[0]
                      .CompanyName;
              this.userCredential.UserCompanyId = res.CompanyId;
              this.userCredential.BusinessCategoryId =
                  res.AssignedCompanyANDCategory.AssignedBusinessCategory[0]
                      .BusinessCategoryId;
              this.userCredential.BusinessCompanyId =
                  res.AssignedCompanyANDCategory.AssignedCompanyList[0]
                      .BusinessCompanyId;
              this.userCredential.CompanyLogo = ETRFUrl +
                  res.AssignedCompanyANDCategory.AssignedCompanyList[0]
                      .CompanyLogo;
            } else if (res.Status == 'Pending' && res.CompanyId == 10000) {
              // Ecommerece
              that.userCredential.UserId = res.UserId;
              that.userCredential.CompanyId = res.CompanyId;
              that.userCredential.BusinessCategoryId = 1;
              that.userCredential.CompanyId = res.CompanyId;
              that.userCredential.UserCategoryId = res.UserCategoryId;
              that.userCredential.UserType = res.UserCategoryName;
              that.userCredential.UserCompanyId = res.CompanyId;
              that.userCredential.CompanyName = res.CompanyName;
              that.userCredential.CompanyLogo = ETRFUrl + res.CompanyLogo;
            } else {
              if (!!localStorage.getItem('orderReviewRequest')) {
                // this.router.navigate(['/e-commerce/order-review']);
                // return;
              } else if (that.testingCompaniesCount > 1 || that.inspectionCompaniesCount > 1) {
                this.router.navigate(['./company']);
                return;
              } else {
                swal('', 'User Name/Password does not exists.');
                this.user.userName = null;
                this.user.password = null;
                return;
              }
            }
            this.dataService
                .get(
                    'user/GetUserPermissions?UserId=' +
                    that.userCredential.UserId + '&CompanyId=' +
                    that.userCredential.CompanyId + '&BusinessCategoryId=' +
                    that.userCredential.BusinessCategoryId)
                .subscribe(response => {
                  let res = response;
                  this.userCredential.UserPermission = res.UserPermission;
                  if (that.statusEtrf == 'Pending' &&
                      this.userCredential.CompanyId == 10000) {
                    res.UserPermission.ViewETRF = true;
                    res.UserPermission.ViewECommerce = true;
                  }
                  localStorage.setItem(
                      'mts_user_cred', JSON.stringify(this.userCredential));
                  if (res.UserPermission.ViewDashboard == false &&
                      UserCompanyIdCondition == 3) {
                    this.router.navigate(
                        ['./manage-document/other-report-upload']);
                  } else if (
                      that.statusEtrf == 'Pending' &&
                      (localStorage.getItem('orderReviewRequest') == null)) {
                    this.router.navigate(['/e-commerce']);
                    return;
                  }
                  if (!!localStorage.getItem('orderReviewRequest')) {
                    this.router.navigate(['./e-commerce/order-review']);
                    return;
                  } else if (res.UserPermission.ViewDashboard == false) {
                    if (this.userCredential.BusinessCategoryId == 5) {
                      this.GoToGreenProduct();
                    } else {
                      this.router.navigate(['./landing-page']);
                    }
                  } else {
                    this.router.navigate(['./dashboard']);
                  }
                });
          } else {
            swal('', res.Message);
            if(res.Data == 'email') {
              this.user.userName = '';
            } else if(res.Data == 'password'){
              this.user.password = '';
            } else {
              this.user.userName = '';
              this.user.password = '';
            }
          }
        })
  }


  GoToGreenProduct(){
    if(this.userCredential.UserPermission.GPInternalReview==true){
      this.router.navigate(['./green-product-assurance/internal-view']);
    }else if(this.userCredential.UserPermission.GPVendorView == true){
      this.router.navigate(['./green-product-assurance/vendor-view']);
    }else{
      this.router.navigate(['./landing-page']);
    }
  }
}
