import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientSelectionComponent } from 'app/login/client-selection/client-selection.component';

const routes: Routes = [{path:'', component:ClientSelectionComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientSelectionRoutingModule { }
