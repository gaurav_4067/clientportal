import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientSelectionRoutingModule } from './client-selection-routing.module';
import { ClientSelectionComponent } from './client-selection.component';
import { FormsModule } from '@angular/forms';
import { FooterModule } from 'app/footer/footer.module';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ClientSelectionRoutingModule,
    FooterModule,
    Ng2AutoCompleteModule
  ],
  declarations: [ClientSelectionComponent]
})
export class ClientSelectionModule { }
