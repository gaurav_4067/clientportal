import { Component, OnInit } from "@angular/core";
import { ETRFUrl, DataService } from "app/mts.service";
import { Router } from "@angular/router";
declare let swal: any;
@Component({
  selector: "app-client-selection",
  templateUrl: "./client-selection.component.html",
  styleUrls: ["./client-selection.component.css"]
})
export class ClientSelectionComponent implements OnInit {
  companyList: any;
  selectedCompany: number;
  selectedClientCompany:any;
  defaultCompanyLabel: any;
  userCredential: any;
  userDetail: any;
  UserProfileimg: any;
  BusinessCatId: any = 1;
  constructor(private dataService: DataService, public router: Router) {
    this.defaultCompanyLabel = {
      CompanyId: 0,
      CompanyName: "Select a Company"
    };
  }

  ngOnInit() {
    this.userCredential = {};
    this.selectedCompany = 0;
    var that = this;
    this.userDetail = JSON.parse(localStorage.getItem("userDetail"));
    if (
      this.userDetail.AssignedCompanyANDCategory.AssignedBusinessCategory
        .length > 1
    ) {
      this.companyList = [];
      this.userDetail.AssignedCompanyANDCategory.AssignedCompanyList.forEach(
        element => {
          if (element.BusinessCategoryID == 1) {
            that.companyList.push(element);
          }
          that.BusinessCatId = 1;
        }
      );
    } else {
      this.BusinessCatId = this.userDetail.AssignedCompanyANDCategory.AssignedBusinessCategory[0].BusinessCategoryId;
      this.companyList = this.userDetail.AssignedCompanyANDCategory.AssignedCompanyList.slice(
        0
      );
    }
    //this.companyList = this.userDetail.AssignedCompanyANDCategory.AssignedCompanyList;
    // this.companyList.splice(0, 0, this.defaultCompanyLabel);
    this.UserProfileimg = ETRFUrl + this.userDetail.ProfilePic;
  }

  goToDashboard() {
    if(this.selectedClientCompany){
      this.selectedCompany=this.selectedClientCompany.CompanyId;
    }
    if (this.selectedCompany > 0) {
      let companyId = this.selectedCompany;
      var that = this;
      let companyObj: any = {};
      let UserCompanyIdCondition = 0;
      this.companyList.forEach(function (obj) {
        if (obj.CompanyId == companyId) {
          companyObj = obj;
        }
      });
      this.userCredential.UserId = this.userDetail.UserId;
      this.userCredential.CompanyId = companyObj.CompanyId;
      this.userCredential.UserType = companyObj.WorkingAs;
      this.userCredential.UserCompanyId = this.userDetail.CompanyId;
      this.userCredential.CompanyName = companyObj.CompanyName;
      this.userDetail.CompanyName = companyObj.CompanyName;
      this.userCredential.CompanyLogo = ETRFUrl + companyObj.CompanyLogo;
      this.userCredential.BusinessCategoryId = this.BusinessCatId;
      this.userCredential.BusinessCompanyId = companyObj.BusinessCompanyId;
      localStorage.setItem("userDetail", JSON.stringify(this.userDetail));
      UserCompanyIdCondition = this.userDetail.UserCategoryId;
      this.dataService
        .get(
        "user/GetUserPermissions?UserId=" +
        this.userCredential.UserId +
        "&CompanyId=" +
        this.userCredential.CompanyId +
        "&BusinessCategoryId=" +
        this.BusinessCatId
        )
        .subscribe(res => {
          that.userCredential.UserPermission = res.UserPermission;
          localStorage.setItem(
            "mts_user_cred",
            JSON.stringify(that.userCredential)
          );
          localStorage.setItem("userDetail", JSON.stringify(that.userDetail));
          if (
            UserCompanyIdCondition == 3 &&
            res.UserPermission.ViewDashboard == false
          ) {
            this.router.navigate(["./manage-document/other-report-upload"]);
          } else if (res.UserPermission.ViewDashboard == false) {
            if (this.userCredential.BusinessCategoryId == 5) {
              this.GoToGreenProduct();
            } else {
              this.router.navigate(['./landing-page']);
            }
          } else {
            this.router.navigate(["./dashboard"]);
          }
        });
    } else {
      swal("", "Please select company.");
    }
  }

  cancel() {
    this.router.navigate(["/login"]);
  }

  logOut() {
    this.dataService
      .post("user/Logout", { UserId: this.userDetail.UserId })
      .subscribe(response => {
        if (response.IsSuccess) {
          localStorage.removeItem("mts_user_cred");
          localStorage.removeItem("userDetail");
          localStorage.removeItem("ServiceId");
          localStorage.removeItem("ServiceName");
          localStorage.removeItem("ecommerceEtrfId");

          this.router.navigate(["./login"]);
        }
      });
  }

  GoToGreenProduct() {
    if (this.userCredential.UserPermission.GPInternalReview == true) {
      this.router.navigate(['./green-product-assurance/internal-view']);
    } else if (this.userCredential.UserPermission.GPVendorView == true) {
      this.router.navigate(['./green-product-assurance/vendor-view']);
    } else {
      this.router.navigate(['./landing-page']);
    }
  }

  public renderCompany(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CompanyName}</div>
      </div>`;

    return html;
  }
  
}
