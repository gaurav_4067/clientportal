import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdiDashboardComponent } from './edi-dashboard.component';

describe('EdiDashboardComponent', () => {
  let component: EdiDashboardComponent;
  let fixture: ComponentFixture<EdiDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdiDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdiDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
