import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EdiDashboardComponent } from 'app/edi-dashboard/edi-dashboard.component';

const routes: Routes = [{
  path:'',
  component:EdiDashboardComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EdiDashboardRoutingModule { }
