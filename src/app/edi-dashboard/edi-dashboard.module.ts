import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EdiDashboardRoutingModule } from './edi-dashboard-routing.module';
import { EdiDashboardComponent } from './edi-dashboard.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { FormsModule } from '@angular/forms';
import { MdInputModule } from '@angular2-material/input';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { Ng2PaginationModule } from 'ng2-pagination';

@NgModule({
  imports: [
    CommonModule,
    EdiDashboardRoutingModule,
    HeaderModule,
    FooterModule,
    FormsModule,
    MdInputModule,
    MyDateRangePickerModule,
    Ng2PaginationModule

  ],
  declarations: [EdiDashboardComponent]
})
export class EdiDashboardModule { }
