import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService, ETRFUrl, BaseUrl } from '../mts.service';
import { SEARCH_MODEL } from './edi-dashboard.model';
import { Http, Response } from '@angular/http';
import { InjectService } from '../injectable-service/injectable-service';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { ActivatedRoute, Router } from '@angular/router';
declare let swal: any;

@Component({
  selector: 'app-edi-dashboard',
  templateUrl: './edi-dashboard.component.html',
  styleUrls: ['./edi-dashboard.component.css']
})
export class EdiDashboardComponent implements OnInit, OnDestroy {

  orderList: any;
  dateRange: any;
  private p: any;
  private totalItem: any;
  private itemPerPage: any;
  searchModal: any;
  searchText: any;
  userDetail: any;
  dynamicSearchColumns: any;
  dynamicGridColumns: any;
  isLoading: boolean;
  reportNo: any;
  additionalSearch: any;
  userCredential: any;
  columnList: any;
  searchParam: any;
  clientEmail: any;
  clientId: any;
  dateFrom: any;
  dateTo: any;
  Status: string = "";
  isDashboard: any = false;
  PageSizeDynamic: number = 10;
  clientEmailBulkEmail: any = [];
  en: any;
  graphData: any = [];
  countOut: any = 0;
  countPlaced: any = 0;
  countReceived: any = 0;

  constructor(private dataService: DataService, private injectService: InjectService, private route: ActivatedRoute, private router: Router) {
    this.searchModal = SEARCH_MODEL;
    this.searchModal.PageSize = this.PageSizeDynamic;
    this.searchModal.PageNumber = 1;
    this.searchModal.StartDate = '';
    this.searchModal.EndDate = '';
    this.searchModal.OrderBy = 'OBJ_ID';
    this.searchModal.Order = 'asc';
    this.dateRange = {};
    this.en = {
      firstDayOfWeek: 0,
      dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    };
    this.clientEmail = '';
  }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      if (this.userCredential.UserPermission.ViewEDIDashboard) {
        var that = this;
        this.clientEmailBulkEmail.push(that.userDetail.EmailId);
        this.searchModal.ClientId = this.userCredential.CompanyId;
        this.searchModal.UserType = this.userCredential.UserType;
        this.searchModal.UserCompanyId = this.userCredential.UserCompanyId;
        this.p = 1;
        this.itemPerPage = 1;
        this.totalItem = 0;
        this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
        /*below for solving bug ot searching and sorting*/
        this.searchParam = {};
        this.searchParam.SearchText = [{ LogicalOperator: "AND", ColumnName: "Status", Operator: 5, ColumnValue: "" }];
        this.searchModal.SearchText = [];
        /*ended*/

        this.additionalSearch = [{ LogicalOperator: "AND", ColumnName: "", Operator: "", ColumnValue: "" }];

        var that = this;
        this.route.params.subscribe(params => {

          that.searchModal.SearchText = JSON.parse(JSON.stringify(that.searchParam.SearchText));
          that.searchModal.SearchText[0].ColumnValue = params['search'];
        });

        this.isLoading = true;

        if (this.injectService.additionalSearch) {

          if (this.searchParam.SearchText.length > 3) {
            var count = this.searchParam.SearchText.length - 3;
            this.searchParam.SearchText.splice(3, count);
          }
          var that = this;
          this.injectService.additionalSearch.forEach(function (addObj) {
            that.searchParam.SearchText.push(addObj);
          });

          this.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));
          this.isDashboard = true;

        }
        else {
          this.isDashboard = false;
        }

        var thisobj = this;
        this.dataService.get('Order/GetMyOrderColumnsList?selectedCompany=' + this.searchModal.ClientId + '&FormId=4').subscribe(
          response => {
            if (response.IsSuccess) {

              thisobj.dynamicSearchColumns = response.SelectedSearchColumn;
              thisobj.dynamicGridColumns = response.SelectedGridColumn;
              thisobj.columnList = response.SelectedAdHocSearchColumn;
              thisobj.getOrderList();
            }
            else {

              thisobj.getOrderList();
              swal('', response.Message)
            }
          }
        );

        thisobj.dataService.get('EDIDashboard/ChartEDIOrders?ClientID=' + thisobj.searchModal.ClientId).subscribe(
          response => {

            if (response.IsSuccess) {

              thisobj.graphData = response.Data;
              for (let i = 0; i < response.Data.length; i++) {
                if (response.Data[i].Status == "Out") {
                  thisobj.countOut = response.Data[i].count
                }
                if (response.Data[i].Status == "Placed") {
                  thisobj.countPlaced = response.Data[i].count
                }
                if (response.Data[i].Status == "Received") {
                  thisobj.countReceived = response.Data[i].count
                }

              }

              this.createStatusChart();
            }
            else {
              thisobj.graphData = []
            }
          }
        );
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }


  resetSearchForm() {

    this.PageSizeDynamic = 10;
    (<any>$('.btnclearenabled')).click();
    this.searchParam.SearchText[0].ColumnValue = '';


    for (let i of this.dynamicSearchColumns) {
      i.ColumnValue = '';
    }


    for (let i in this.additionalSearch[0]) {
      this.additionalSearch[0].ColumnName = "";
      this.additionalSearch[0].ColumnValue = "";
      this.additionalSearch[0].LogicalOperator = "AND";
      this.additionalSearch[0].Operator = "";
    }

    (<any>$('.resetSelect')).prop('selectedIndex', 0);
    (<any>$('.resetDynamicInput')).val('');
    for (var i = this.additionalSearch.length; i > 0; i--) {

      this.additionalSearch.splice(i, 1);
    }
    this.PageSizeDynamic = 10;
    this.search(1);
  }

  selectSize() {
    this.search(1);
  }


  addSearch() {

    if (this.additionalSearch.length > 0) {
      if (this.additionalSearch[this.additionalSearch.length - 1].ColumnName && this.additionalSearch[this.additionalSearch.length - 1].LogicalOperator && this.additionalSearch[this.additionalSearch.length - 1].Operator) {
        this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
      }
    }
    else {
      this.additionalSearch.push({ LogicalOperator: "", ColumnName: "", Operator: "", ColumnValue: "" });
    }
  }

  removeSearch(index: number) {
    if (this.additionalSearch.length > 1) {
      //this.additionalSearch.splice(-1,1);
      this.additionalSearch.splice(index, 1);
    }
  }


  search(Type: any) {



    let dynamicSearchLength = 0;
    let thisObj = this;

    if (Type == 1) {
      this.isDashboard = false;
    }

    if (this.isDashboard) {
      if (this.injectService.additionalSearch) {

        if (this.searchParam.SearchText.length > 3) {
          var count = this.searchParam.SearchText.length - 3;
          this.searchParam.SearchText.splice(3, count);
        }

        var that = this;
        this.injectService.additionalSearch.forEach(function (addObj) {
          that.searchParam.SearchText.push(addObj);
        });

        this.searchModal.SearchText = JSON.parse(JSON.stringify(this.searchParam.SearchText));

      }
    }
    else {

      if (thisObj.dateRange.StartDate && thisObj.dateRange.EndDate) {
        thisObj.searchModal.StartDate = JSON.parse(JSON.stringify(thisObj.dateRange.StartDate));
        thisObj.searchModal.EndDate = JSON.parse(JSON.stringify(thisObj.dateRange.EndDate));
      }
      thisObj.searchModal.SearchText = JSON.parse(JSON.stringify(thisObj.searchParam.SearchText));

      if (thisObj.dynamicSearchColumns) {
        dynamicSearchLength = thisObj.dynamicSearchColumns.length;
        thisObj.searchModal.SearchText.splice(3, thisObj.searchModal.SearchText.length - 3);
        thisObj.dynamicSearchColumns.forEach(function (column) {
          thisObj.searchModal.SearchText.push({
            "LogicalOperator": "AND",
            "ColumnName": column.ColumnName,
            "Operator": 5,
            "ColumnValue": column.ColumnValue
          })
        })
      }

      if (thisObj.searchModal.SearchText.length > 3 + dynamicSearchLength) {
        let count = thisObj.searchModal.SearchText.length - 3 + dynamicSearchLength;
        thisObj.searchModal.SearchText.splice(3 + dynamicSearchLength, count);
      }
      thisObj.additionalSearch.forEach(function (addObj) {
        thisObj.searchModal.SearchText.push(addObj);
      })


    }
    this.searchModal.PageSize = this.PageSizeDynamic;
    thisObj.searchModal.PageNumber = 1;
    if (Type == 1) {
      thisObj.getOrderList();
    }
    else {
      // thisObj.ExportToExcelTrack();
    }
  }

  getPage(page: number) {

    this.searchModal.PageNumber = page;
    this.isLoading = true;
    this.dataService.post('EDIDashboard/GetAllEDIOrders', this.searchModal).subscribe(
      response => {
        this.isLoading = false;
        if (response.IsSuccess) {
          this.p = page;
          this.orderList = response.Dt;
        }
        else {
          swal('', response.Message);
        }
      }
    )
  }

  sorting(column: string) {

    this.searchModal.OrderBy = column;
    if (this.searchModal.Order == 'asc') {
      this.searchModal.Order = 'desc';
    }
    else {
      this.searchModal.Order = 'asc';
    }
    this.isLoading = true;
    var that = this;
    this.dataService.post('EDIDashboard/GetAllEDIOrders', that.searchModal).subscribe(
      response => {
        that.isLoading = false;
        if (response.IsSuccess) {
          that.orderList = response.Dt;
          that.totalItem = Math.ceil(response.TotalRecords / that.searchModal.PageSize);
        }
        else {
          swal('', 'Sorting is not enabled for this information');
        }
      }
    )
  }


  getOrderList() {

    this.isLoading = true;
    this.searchModal.PageSize = this.PageSizeDynamic;
    this.dataService.post('EDIDashboard/GetAllEDIOrders', this.searchModal).subscribe(
      response => {

        this.isLoading = false;
        if (response.IsSuccess) {

          this.orderList = response.Dt;
          this.totalItem = Math.ceil(response.TotalRecords / this.searchModal.PageSize);
          if (response.Dt.length > 0) {

            this.clientEmail = response.Dt[0].ClientEmail;
            this.clientId = response.Dt[0].ClientID;
          }


        }
        else {
          swal('', response.Message);
        }
      }
    )
  }

  dateChange(name: any) {

    if (name == "dateFrom") {
      var dd1 = this.dateFrom.getMonth() + 1 + '/' + this.dateFrom.getDate() + '/' + this.dateFrom.getYear();
      this.dateRange.StartDate = dd1;
    }
    if (name == "dateTo") {
      var dd2 = this.dateTo.getMonth() + 1 + '/' + this.dateTo.getDate() + '/' + this.dateTo.getYear();
      this.dateRange.EndDate = dd2;
    }

  }

  createStatusChart() {

    var chart = AmCharts.makeChart("ediChart", {
      "theme": "light",
      "type": "serial",

      "dataProvider": [{
        "Status": "Placed",
        "count": this.countPlaced,
        "color": "#67B7DC"
      }, {
        "Status": "Received",
        "count": this.countReceived,
        "color": "#FFA500"
      }, {
        "Status": "Out",
        "count": this.countOut,
        "color": "#84B761"

      }],

      "valueAxes": [{
        "axisAlpha": 0,
        "gridAlpha": 0.1,
        "labelsEnabled": false,
        "gridThickness": 0
      }],

      "graphs": [{
        "balloonText": "<span style='font-size:10px'><b>Total count in [[category]]:[[value]]</b> </span>",
        "fillAlphas": 1,
        "lineAlpha": 0.2,
        // "title": "Income",
        "type": "column",
        "labelText": "[[count]]",
        "labelPosition": "middle",
        "color": "#fff",
        "valueField": "count",
        "colorField": "color",
        "fixedColumnWidth": 35
      }],
      "depth3D": 0,
      "angle": 30,
      "rotate": true,
      "categoryField": "Status",
      "categoryAxis": {
        "gridThickness": 0,
        "fillAlpha": 0

      },

      "export": {
        "enabled": true
      }
    });

  }

  ngOnDestroy() {
    this.injectService.additionalSearch = [];
  }

}
