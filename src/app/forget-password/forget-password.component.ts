import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../mts.service';
import { Response } from '@angular/http';
declare let swal: any;

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  public user: any = { username: '', password: '' };
  userCredential: any;
  constructor(private dataService: DataService, public router: Router) {
  }

  ngOnInit() {

  }

  resetPassword() {
    this.dataService.getOrder('user/ForgotPassword?emailID=' + this.user.username).subscribe(
      res => {
        if (res.IsSuccess) {
          swal('', res.Message);
          this.router.navigate(['./login'])
        }
        else {
          swal('', res.Message);
        }
      }
    )
  }

}
