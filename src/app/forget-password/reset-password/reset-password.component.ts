import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../mts.service';
import { Response } from '@angular/http';
declare let swal: any;

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})

export class ResetPasswordComponent implements OnInit {
  public user: any = { pwd: '', confirmPwd: '' };
  userCredential: any;
  authToken: string;
  confirmPassMsg: string;
  passwordMsg: string;
  passwordValidation: boolean = true;
  confirmPasswordValidation: boolean = true;

  constructor(private dataService: DataService, public router: Router, public route: ActivatedRoute) {
  }
  ngOnInit() {
      this.authToken = '';
      this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
      if (this.route.queryParams) {
          this.route.queryParams.subscribe(params => {
              this.authToken = params['q'];
          });
      }
  }
  resetPassword() {
      if (this.user.pwd == "") {
          this.passwordValidation = false;
          this.passwordMsg = "Required";
      }

      if (this.user.confirmPwd == "") {
          this.confirmPasswordValidation = false;
          this.confirmPassMsg = "Required";
      }
      if (this.passwordValidation && this.confirmPasswordValidation) {
          this.dataService.signUpPost('User/ResetPassword', { Token: this.authToken, Password: this.user.confirmPwd }).subscribe(
              res => {

                  if (res.IsSuccess) {
                      swal('', res.Message);
                      this.router.navigate(['./login']);
                  }
                  else {
                      swal('', res.Message);
                  }
              }
          )
      }
  }

  checkValidation(format: any, value: any) {
      if (format == 'password') {
          if (value.length == 0) {
              this.passwordValidation = false;
              this.passwordMsg = "Required";
          }

          else if (value.length < 6 || value.length > 20) {
              this.passwordValidation = false;
              this.passwordMsg = "Password should be between 6 to 20 characters";
          }
          else {
              this.passwordValidation = true;
          }
      }

      if (format == 'confirmPassword') {
          if (value != this.user.pwd) {
              this.confirmPasswordValidation = false;
              this.confirmPassMsg = "Password do not match";
          }
          else {
              this.confirmPasswordValidation = true;
          }
      }

  }
}
