import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdInputModule } from '@angular2-material/input';
import { FormsModule } from '@angular/forms';
import { ForgetPasswordRoutingModule } from './forget-password-routing.module';
import { ForgetPasswordComponent } from './forget-password.component';

@NgModule({
  imports: [
    CommonModule,
    ForgetPasswordRoutingModule,
    MdInputModule,
    FormsModule
  ],
  declarations: [ForgetPasswordComponent]
})
export class ForgetPasswordModule { }
