import { Component, OnInit } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { Router } from "@angular/router";

import { BaseUrl, DataService, ETRFUrl } from "../mts.service";

import {
  ETRF_Saved_List,
  ETRF_Search_Model,
  SFTestRequest,
  BusinessLineClass,
  SF_EDI_Data,
  SF_EDI_Data_Extended
} from "./online-etrf.model";
import { debug } from "util";

declare let swal: any;
declare let jsPDF: any;
declare let html2canvas: any;

@Component({
  selector: "app-online-etrf",
  templateUrl: "./online-etrf.component.html",
  styleUrls: ["./online-etrf.component.css"]
})
export class OnlineEtrfComponent implements OnInit {
  invoiceList: Array<string>;
  selectedIndex: number;
  private p: any;
  private page: number = 1;
  private totalItem: any;
  private itemPerPage: any = 10;
  userDetail: any;
  userCredential: any;
  url: any;
  finalUrl: SafeResourceUrl;
  companyId: any;
  etrfList: any = [];
  isLoader: boolean;
  searchModel: any;
  dateFrom: any;
  dateTo: any;
  skuNumber: any;
  poNumber: any;
  comment: any;
  sampleReportNo: any;
  etrfNumber: any;
  Status: any;
  savedETRFList: any = [];
  // itemPerPage: any = 10;
  // totalItem: any;
  searchModal: any;
  en: any;
  Usertype: any = true;
  companyName: any = "";
  etrfTypes: any = [];
  etrfTypeId: number = 0;
  buttonText: string = "";
  sfTestRequest: SFTestRequest;
  EDI_Data_List: Array<SF_EDI_Data_Extended>;
  Filtered_EDI_Data_List: Array<SF_EDI_Data_Extended>;
  Selected_EDI_Data_List: any;
  isValidated: boolean = true;
  SKUColorAndPOList: Array<BusinessLineClass>;
  BusinessLineList: Array<string>;
  BusinessLines: any;
  KidsBusinessLines: any;
  KidsBusinessLineList: Array<string>;
  BrandSKUList: Array<string>;
  FabricIds: Array<string>;
  ColorList: Array<string>;
  PONumberList: Array<string>;
  IsBrandSKUAndColorTested: boolean = false;
  displayTable: boolean = false;
  columnList: any;

  selectedGridColumns: any;

  constructor(
    private dataService: DataService,
    private sanitizer: DomSanitizer,
    private router: Router
  ) {
    this.invoiceList = new Array<string>();
    this.sfTestRequest = new SFTestRequest();
    this.SKUColorAndPOList = new Array<BusinessLineClass>();
    this.KidsBusinessLineList = new Array<string>();
    this.BusinessLineList = new Array<string>();
    this.BrandSKUList = new Array<string>();
    this.ColorList = new Array<string>();
    this.FabricIds = new Array<string>();
    this.EDI_Data_List = new Array<SF_EDI_Data_Extended>();
    this.Filtered_EDI_Data_List = new Array<SF_EDI_Data_Extended>();
  }


  ngOnInit() {
    // if (!localStorage.getItem('userDetail')) {
    //     this.router.navigate(['/home']);
    // }
    // if(localStorage.getItem("ecommerceStatus")) {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userDetail && this.userCredential) {

      this.getTRFListingColumn();

      if (this.userCredential.BusinessCategoryId == 2 && this.userDetail.IsFCAI) {

      } else {
        if (!localStorage.getItem('CategoryId') ||
          localStorage.getItem('CategoryId') == null) {
          this.buttonText = 'Place New Order';
        }
        if (parseInt(localStorage.getItem('CategoryId')) == 1) {
          this.buttonText = 'Place New Test Request';
        }
        if (parseInt(localStorage.getItem('CategoryId')) == 2) {
          this.buttonText = 'Place New Inspection Booking';
        }
        // }
        localStorage.setItem('ecommerceStatus', 'False');
        localStorage.removeItem('CategoryId');

        this.BusinessLines = [
          { Name: "Mens", Value: "Mens", Index: 1 },
          { Name: "Womens", Value: "Womens", Index: 2 },
          { Name: "Kids <18mos", Value: "Kids", Index: 3 },
          { Name: "Kids 18-36mos", Value: "Kids", Index: 4 },
          { Name: "Kids >36mos", Value: "Kids", Index: 5 }
        ];

        this.KidsBusinessLines = [
          { Name: "Kids <18mos", Value: "Kids", Index: 3 },
          { Name: "Kids 18-36mos", Value: "Kids", Index: 4 },
          { Name: "Kids >36mos", Value: "Kids", Index: 5 }
        ];


        this.en = {
          firstDayOfWeek: 0,
          dayNames: [
            'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
            'Saturday'
          ],
          dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
          monthNames: [
            'January', 'February', 'March', 'April', 'May', 'June', 'July',
            'August', 'September', 'October', 'November', 'December'
          ],
          monthNamesShort: [
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct',
            'Nov', 'Dec'
          ]
        };
        this.searchModel = Object.assign({}, ETRF_Search_Model);

        ETRF_Saved_List.ClientId = this.userCredential.CompanyId;
        ETRF_Saved_List.SearchText = [];
        if (this.userCredential.UserCompanyId != 0) {
          ETRF_Saved_List.VendorId = this.userCredential.UserCompanyId;
        } else {
          ETRF_Saved_List.VendorId = this.userCredential.CompanyId;
        }
        ETRF_Saved_List.UserType = this.userCredential.UserType;
        if (this.userDetail.Status != 'Pending') {
          if (!this.userCredential.UserPermission.ViewETRF) {
            this.router.navigate(['./landing-page']);
          }
        }
        this.companyName = this.userCredential.CompanyName;

        this.url = ETRFUrl + 'ETRF/Home?a=' +
          JSON.parse(localStorage.getItem('userDetail')).AuthToken +
          '&c=' + this.userCredential.CompanyId;
        this.finalUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
        this.companyId = this.userCredential.CompanyId;
        var PageSize = ETRF_Saved_List.PageSize;
        var that = this;

        this.searchModal = ETRF_Saved_List;
        // this.searchModal.pageSize = 10,
        //     this.searchModal.PageNumber = 1,
        //     this.searchModal.orderBy = "",
        //     this.searchModal.order = "desc",
        //     this.searchModal.ClientId = 1,
        //     this.searchModal.totalRecords = ""

        that.isLoader = true;
        this.p = 1;
        if (that.userDetail.Status == 'Pending') {
          that.Usertype = false;
          that.searchModal.VendorId = that.userDetail.UserId;
          that.searchModal.ClientId = 10000;
          that.searchModal.EtrfTypeId = that.etrfTypeId;
          that.searchModal.CreatedBy = that.userCredential.UserId;
          that.dataService
            .post('DynamicFormCreation/EcommETRFSavedList', that.searchModal)
            .subscribe(r => {
              if (r.IsSuccess) {
                that.savedETRFList = JSON.parse(r.Data);
                that.savedETRFList.forEach((data, index) => {
                  if (data.WorkingStatus == 'C') {
                    data.Status = 'Canceled'
                  }
                });
                // that.totalItem = Math.ceil(r.TotalRecords /
                // that.searchModal.pageSize);
                that.totalItem = r.TotalRecords;
              }
              that.isLoader = false;
            })
        } else {
          that.searchModal.CreatedBy = that.userDetail.UserId;
          that.searchModal.EtrfTypeId = that.etrfTypeId;
          that.dataService
            .post('DynamicFormCreation/ETRFSavedList', that.searchModal)
            .subscribe(r => {
              if (r.IsSuccess) {
                that.savedETRFList = JSON.parse(r.Data);
                that.savedETRFList.forEach((data, index) => {
                  if (data.WorkingStatus == 'C') {
                    data.Status = 'Canceled'
                  }
                });
                that.totalItem = r.TotalRecords;
                // that.totalItem = Math.ceil(r.TotalRecords / that.itemPerPage);
              }
              that.isLoader = false;
            })
        }
        this.dataService.get('DynamicFormCreation/ETRFtypeList').subscribe(r => {
          if (r.IsSuccess) {
            var TypeList = [];
            if (that.userDetail.Status == 'Pending') {
              for (var i = 0; i < r.Data.length; i++) {
                if (r.Data[i].Value.toLowerCase().includes('ecom')) {
                  TypeList.push(r.Data[i]);
                }
              }
            } else {
              if (that.userDetail.AssignedCompanyANDCategory
                .AssignedBusinessCategory.length > 0) {
                for (var j = 0; j < that.userDetail.AssignedCompanyANDCategory
                  .AssignedBusinessCategory.length;
                  j++) {
                  if (that.userDetail.AssignedCompanyANDCategory
                    .AssignedBusinessCategory[j]
                    .BusinessCategoryName == 'Testing') {
                    TypeList.push(r.Data.filter(i => i.Value == 'Testing')[0]);
                  }
                  if (that.userDetail.AssignedCompanyANDCategory
                    .AssignedBusinessCategory[j]
                    .BusinessCategoryName == 'Inspection') {
                    TypeList.push(
                      r.Data.filter(i => i.Value == 'Inspection Service')[0]);
                  }
                  if (that.userDetail.AssignedCompanyANDCategory
                    .AssignedBusinessCategory[j]
                    .BusinessCategoryName == 'FCAI') {
                    TypeList.push(r.Data.filter(i => i.Value == 'FCAI')[0]);
                  }
                }
                if (that.userCredential.UserPermission.ViewECommerce) {
                  for (var i = 0; i < r.Data.length; i++) {
                    if (r.Data[i].Value.toLowerCase().includes('ecom')) {
                      TypeList.push(r.Data[i]);
                    }
                  }
                }
              }
            }
            that.etrfTypes = TypeList;
          }
        });
      }
    } else {
      this.router.navigate(['/login']);
    }
  }


  getTRFListingColumn() {

    this.isLoader = true;
    this.dataService
      .get(
        'Order/GetTRFListColumnsList?selectedCompany=' +
        this.userCredential.CompanyId +
        '&businessCategoryId=' + this.userCredential.BusinessCategoryId)
      .subscribe(r => {
        this.isLoader = false;
        this.columnList = r;
        if (this.columnList != null) {
          this.selectedGridColumns = this.columnList.SelectedGridColumn;
        }
      });

  }

  addNewETRF(e) {
    e.preventDefault();
    var that = this;
    if (that.companyId == 386 && that.userDetail.SF_TRF_AccessType) {
      this.sfTestRequest = new SFTestRequest();
      this.getSKUColorAndPOList();
    }
    else {
      this.redirectOnEtrfPage();
    }
    // if (that.companyId == 386 && that.userCredential.UserCompanyId == 96808) {
    //   this.sfTestRequest = new SFTestRequest();
    //   this.getSKUColorAndPOList();
    // } else {
    //   this.redirectOnEtrfPage();
    // }
  }

  redirectOnEtrfPage() {
    let that = this;
    this.isLoader = true;
    this.dataService
      .get(
        "DynamicFormCreation/GetETRFList?CompanyId=" +
        this.companyId +
        "&DivisionId=0"
      )
      .subscribe(r => {
        that.isLoader = false;
        that.etrfList = r.Data;

        if (that.etrfList.length > 1) {
          (<any>$("#etrfList")).modal("show");
        } else if (that.etrfList.length == 1) {
          if (
            that.companyId == 386 && that.userDetail.SF_TRF_AccessType == true
          ) {
            that.router.navigate([
              "/online-etrf/add-etrf",
              that.etrfList[0].FormId,
              that.dataService.SF_EDI_DATA_ID,
              that.dataService.SFSelectedBusinessLine,
              that.sfTestRequest.TestRequestType
            ]);
          } else {
            that.router.navigate([
              "/online-etrf/add-etrf",
              that.etrfList[0].FormId
            ]);
          }
        } else {
          swal("", "No e-TRF");
        }
      });
  }

  clickETRF(id: any, e: any) {
    e.preventDefault();
    if (
      this.userCredential.CompanyId == 386 &&
      this.userDetail.SF_TRF_AccessType == true
    ) {
      (<any>$("#etrfList")).modal("hide");
      this.router.navigate([
        "/online-etrf/add-etrf",
        id,
        this.dataService.SF_EDI_DATA_ID,
        this.dataService.SFSelectedBusinessLine,
        this.sfTestRequest.TestRequestType
      ]);
    } else {
      (<any>$("#etrfList")).modal("hide");
      this.router.navigate(["/online-etrf/add-etrf", id]);
    }
  }

  search() {
    var that = this;
    this.searchModal = ETRF_Saved_List;

    that.isLoader = true;
    this.p = 1;

    if (that.userDetail.Status == "Pending") {
      that.Usertype = false;
      that.searchModal.VendorId = that.userDetail.UserId;
      that.searchModal.ClientId = 10000;
      that.searchModal.PageNumber = 1;
      that.searchModal.EtrfTypeId = this.etrfTypeId;
      that.dataService
        .post("DynamicFormCreation/EcommETRFSavedList", that.searchModal)
        .subscribe(r => {
          if (r.IsSuccess) {
            that.savedETRFList = JSON.parse(r.Data);
            that.savedETRFList.forEach((data, index) => {
              if (data.WorkingStatus == "C") {
                data.Status = "Canceled";
              }
            });
            // that.totalItem = Math.ceil(r.TotalRecords /
            // that.searchModal.pageSize);
            that.totalItem = r.TotalRecords;
          }
          that.isLoader = false;
        });
    } else {
      that.searchModal.EtrfTypeId = this.etrfTypeId;
      that.searchModal.PageNumber = 1;
      that.dataService
        .post("DynamicFormCreation/ETRFSavedList", that.searchModal)
        .subscribe(r => {
          if (r.IsSuccess) {
            that.savedETRFList = JSON.parse(r.Data);
            that.savedETRFList.forEach((data, index) => {
              if (data.WorkingStatus == "C") {
                data.Status = "Canceled";
              }
            });
            // that.totalItem = Math.ceil(r.TotalRecords /
            // that.searchModal.pageSize);
            that.totalItem = r.TotalRecords;
          }
          that.isLoader = false;
        });
    }
  }

  reset() {
    this.dateFrom = "";
    this.dateTo = "";
    this.skuNumber = "";
    this.poNumber = "";
    this.comment = "";
    this.etrfNumber = "";
    this.sampleReportNo = "";
    this.Status = "";
    this.etrfTypeId = 0;
    ETRF_Saved_List.SearchText = [];
    this.search();
  }

  searchModelChange(val: any) {
    if (val == "dateFrom") {
      let present = false;
      ETRF_Search_Model.ColumnName = "ModifiedOn";
      ETRF_Search_Model.Operator = 7;
      ETRF_Search_Model.ColumnValue = this.dateFrom;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.Operator == 7) {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "dateTo") {
      let present = false;
      ETRF_Search_Model.ColumnName = "ModifiedOn";
      ETRF_Search_Model.Operator = 8;
      ETRF_Search_Model.ColumnValue = this.dateTo;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.Operator == 8) {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "skuNumber") {
      let present = false;
      ETRF_Search_Model.ColumnName = "E_TRF_SKU_Number";
      ETRF_Search_Model.Operator = 5;
      ETRF_Search_Model.ColumnValue = this.skuNumber;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "E_TRF_SKU_Number") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "poNumber") {
      let present = false;
      ETRF_Search_Model.ColumnName = "E_TRF_PO_Number";
      ETRF_Search_Model.Operator = 5;
      ETRF_Search_Model.ColumnValue = this.poNumber;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "E_TRF_PO_Number") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "comment") {
      let present = false;
      ETRF_Search_Model.ColumnName = "CP_additionalNote";
      ETRF_Search_Model.Operator = 5;
      ETRF_Search_Model.ColumnValue = this.comment;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "CP_additionalNote") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "sampleReportNo") {
      let present = false;
      ETRF_Search_Model.ColumnName = "SampleReportNo";
      ETRF_Search_Model.Operator = 5;
      ETRF_Search_Model.ColumnValue = this.sampleReportNo;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "SampleReportNo") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "etrfNumber") {
      let present = false;
      ETRF_Search_Model.ColumnName = "E_TRFID";
      ETRF_Search_Model.Operator = 5;
      ETRF_Search_Model.ColumnValue = this.etrfNumber;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "E_TRFID") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "Status") {
      let present = false;
      ETRF_Search_Model.ColumnName = "Status";
      ETRF_Search_Model.Operator = 1;
      ETRF_Search_Model.ColumnValue = this.Status;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "Status") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
  }

  pageChange(e: any) { }

  getPage(page: number) {
    var that = this;
    this.searchModal.PageNumber = page;
    this.isLoader = true;
    that.dataService
      .post("DynamicFormCreation/ETRFSavedList", that.searchModal)
      .subscribe(r => {
        if (r.IsSuccess) {
          that.savedETRFList = JSON.parse(r.Data);
          that.savedETRFList.forEach((data, index) => {
            if (data.WorkingStatus == "C") {
              data.Status = "Canceled";
            }
          });
          that.p = page;
          that.totalItem = r.TotalRecords;
          // that.totalItem = Math.ceil(r.TotalRecords /
          // that.searchModal.pageSize);
        }
        that.isLoader = false;
      });
  }

  GetEcommEtrf(Type: any) {
    if (Type == "Ecomm") {
      var that = this;
      this.searchModal = this.dataService.clone(ETRF_Saved_List);
      this.searchModal.VendorId = this.userDetail.UserId;
      this.searchModal.ClientId = 10000;
      that.isLoader = true;
      this.p = 1;
      that.dataService
        .post("DynamicFormCreation/EcommETRFSavedList", that.searchModal)
        .subscribe(r => {
          if (r.IsSuccess) {
            that.savedETRFList = JSON.parse(r.Data);
            that.savedETRFList.forEach((data, index) => {
              if (data.WorkingStatus == "C") {
                data.Status = "Canceled";
              }
            });
            // that.totalItem = Math.ceil(r.TotalRecords /
            // that.searchModal.pageSize);
            that.totalItem = r.TotalRecords;
          }
          that.isLoader = false;
        });
    } else if (Type == "Online") {
      this.search();
    }
  }
  CancelEtrf(id: any) {
    var that = this;
    swal(
      {
        title: "",
        text: `<span style=\'color:#4c4141;font-size:20px;font-weight:bold\'>Are you sure you want to cancel ETRF <br> "${id}" ?</span><br><br>`,
        html: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Proceed",
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function (isConfirm) {
        if (isConfirm) {
          that.isLoader = true;
          that.dataService
            .get("DynamicFormCreation/CancelEtrf?E_TRFNo=" + id)
            .subscribe(r => {
              that.isLoader = false;
              if (r.IsSuccess == true) {
                swal("", "Etrf Cancelled");
                that.search();
              } else {
                swal("", r.Message);
              }
            });
        } else {
        }
      }
    );
  }

  download(fileName: any, fileType: any, reportNo) {

    let downloadFileName = encodeURIComponent(fileName.trim());
    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim() + "&fileType=" + fileType + "&businessCategoryId=" + this.userCredential.BusinessCategoryId;
  }

  GetPDFList(id: any) {
    window.location.href =
      BaseUrl +
      "ManageDocuments/DownloadFile?FileName=ETRFPDF\\" +
      id +
      ".pdf&fileType=PDF&businessCategoryId=" +
      this.userCredential.BusinessCategoryId;
    // var name = id + '.pdf';
    // this.isLoader = true;
    // var that = this;
    // this.dataService.get('DynamicFormCreation/GetPDFList?E_TRFNo='+id+'&formID='+0).subscribe(
    //     r => {
    //         if(r.IsSuccess)
    //         {
    //             var imgSrc = r.Data;
    //             // var image = document.getElementById('result');
    //             (<any>$('#result')).css('display', 'block');
    //             (<any>$('#result')).attr('src', imgSrc);
    //             (<any>$('#result')).css('width', 1785+'px');
    //             document.getElementById('result').onload = function(){

    //                 var quotes = document.getElementById('result');
    //                 html2canvas(quotes, {
    //                     onrendered: function(canvas) {

    //                         var pdf = new jsPDF('p', 'pt', [1785, 2526]);

    //                         for (var i = 0; i <= quotes.clientHeight/2550;
    //                         i++)
    //                         {
    //                             var srcImg  = canvas;
    //                             var sX      = 0;
    //                             var sY      = 2526*i;
    //                             var sWidth  = 1785;
    //                             var sHeight = 2526;
    //                             var dX      = 0;
    //                             var dY      = 0;
    //                             var dWidth  = 1785;
    //                             var dHeight = 2526;

    //                             var onePageCanvas =
    //                             document.createElement("canvas");
    //                             onePageCanvas.width = 1785;
    //                             onePageCanvas.height = 2526;
    //                             var ctx = onePageCanvas.getContext('2d');

    //                             ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);
    //                             var canvasDataURL =
    //                             onePageCanvas.toDataURL("image/jpeg", 1.0);

    //                             var width         = onePageCanvas.width;
    //                             var height        =
    //                             onePageCanvas.clientHeight;

    //                             //! If we're on anything other than the first
    //                             page,
    //                             // add another page
    //                             if (i > 0)
    //                             {
    //                                 pdf.addPage(1785, 2526); //8.5" x 11" in
    //                                 pts (in*72)
    //                             }

    //                             //! now we declare that we're working on that
    //                             page pdf.setPage(i+1);

    //                             //! now we add content to that page!
    //                             pdf.addImage(canvasDataURL, 'jpeg', 0.1, 0,
    //                             width, height, null);
    //                         }

    //                         pdf.save(name);
    //                         (<any>$('#result')).attr('src', '');
    //                         (<any>$('#result')).css('display', 'none');
    //                         that.isLoader = false;
    //                     }
    //                 })
    //             }
    //         }
    //     })
  }

  CopyEtrf(id: any) {
    var that = this;
    this.isLoader = true;
    this.dataService
      .get(
        "DynamicFormCreation/CopyEtrf?E_TRFNo=" +
        id +
        "&UserId=" +
        this.userCredential.UserId
      )
      .subscribe(r => {
        that.isLoader = false;
        if (r.IsSuccess == true) {
          // swal('', 'Etrf Cancelled');
          // that.search();
          this.router.navigate(["/online-etrf/edit-etrf", r.Data]);
        } else {
          swal("", r.Message);
        }
      });
  }

  PrintInvoice(e) {
    this.selectedIndex = null;
    var that = this;
    this.isLoader = true;
    this.dataService
      .get("RequestAddtionalTestItem/GetAllInvoicesName/" + e)
      .subscribe(r => {
        that.isLoader = false;
        if (r.IsSuccess == true) {
          that.invoiceList = r.Data.split(",");
          if (that.invoiceList.length == 1) {
            window.location.href =
              BaseUrl +
              "ManageDocuments/DownloadSampleFile?Folder=INVOICEECOMPDF&FileName=" +
              that.invoiceList[0];
            this.isLoader = false;
          } else {
            (<any>$("#invoiceList")).modal("show");
          }
        } else {
          swal("", r.Message);
        }
      });
  }

  downloadInvoice() {
    let fileName = this.invoiceList[this.selectedIndex];
    window.location.href =
      BaseUrl +
      "ManageDocuments/DownloadSampleFile?Folder=INVOICEECOMPDF&FileName=" +
      fileName;
    (<any>$("#invoiceList")).modal("hide");
  }

  retestEtrf(etrfId: string) {
    //debugger;
    let etrfObject = this.savedETRFList.filter(
      i => i.E_TRFID === etrfId && i.Status === "Report Issued"
    );
    if (etrfObject && etrfObject.length > 0 && etrfObject[0].SampleReportNo) {
      var that = this;
      swal(
        {
          title: "",
          text: `<span style=\'color:#4c4141;font-size:20px;font-weight:bold\'>Would you like to Re-Test <br> "${etrfId}" ?</span><br><br>`,
          html: true,
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Proceed",
          cancelButtonText: "Cancel",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function (isConfirm) {
          if (isConfirm) {
            that.isLoader = true;
            that.dataService
              .get(
                "DynamicFormCreation/RetestEtrf?E_TRFNo=" +
                etrfId +
                "&UserId=" +
                that.userCredential.UserId
              )
              .subscribe(r => {
                that.isLoader = false;
                if (r.IsSuccess) {
                  that.router.navigate(["/online-etrf/edit-etrf", r.Data]);
                } else {
                  swal("", r.Message);
                }
              });
          }
        }
      );
    } else {
      swal("", "Testing still in process!!");
    }
  }

  changeTestRequestType(id: number) {
    this.EDI_Data_List.forEach(element => {
      element.IsSelected = false;
    });

    this.Selected_EDI_Data_List = this.EDI_Data_List.filter(i => i.IsSelected == true);

    this.sfTestRequest = new SFTestRequest();
    this.sfTestRequest.BusinessLine = "";
    this.sfTestRequest.BrandSKU = "";
    this.sfTestRequest.Garment_Color = "";
    this.sfTestRequest.PONumber = "";
    this.sfTestRequest.FabricId = "";
    this.sfTestRequest.TestRequestType = id;

    if (this.sfTestRequest.TestRequestType == 4) {
      this.KidsBusinessLineList = [];
      this.KidsBusinessLineList = this.KidsBusinessLines;
    }

    this.Filtered_EDI_Data_List = new Array<SF_EDI_Data_Extended>();

    this.BrandSKUList = new Array<string>();
    this.ColorList = new Array<string>();
    this.PONumberList = new Array<string>();
    this.FabricIds = new Array<string>();
    this.validateCheckboxFromCode();
  }


  submitSFPopupForm() {
    //debugger;
    this.isLoader = true;

    let selectedEDIData = this.EDI_Data_List.filter(
      i => i.IsSelected == true
    );

    let selectedEDIDATAId = [];
    selectedEDIData.forEach(element => {
      selectedEDIDATAId.push(element.SF_EDI_DATA_ID);
    });

    this.dataService.SF_EDI_DATA_ID = selectedEDIDATAId.toString();
    this.dataService.SFSelectedBusinessLine = this.sfTestRequest.BusinessLine;
    (<any>$("#sfPopup")).modal("hide");
    this.redirectOnEtrfPage();
  }

  // submitSFPopupForm() {
  //   //debugger;
  //   this.isLoader = true;

  //   let selectedEDIData = this.Filtered_EDI_Data_List.filter(
  //     i => i.IsSelected == true
  //   );
  //   console.log(selectedEDIData.length);
  //   let selectedEDIDATAId = [];
  //   selectedEDIData.forEach(element => {
  //     selectedEDIDATAId.push(element.SF_EDI_DATA_ID);
  //   });
  //   //debugger;

  //   let selectedBL = this.BusinessLines.filter(
  //     i => i.Index == parseInt(this.sfTestRequest.BusinessLine, 10)
  //   )[0];
  //   let blInfo = this.sfTestRequest.BusinessLine;
  //   this.sfTestRequest.BusinessLine = selectedBL.Name;
  //   this.sfTestRequest.ClientId = this.userCredential.CompanyId;
  //   this.sfTestRequest.VendorId =
  //     this.userCredential.UserCompanyId == 0
  //       ? this.userCredential.CompanyId
  //       : this.userCredential.UserCompanyId;
  //   this.dataService
  //     .post("DynamicFormCreation/VerifySFPopupForm", this.sfTestRequest)
  //     .subscribe(
  //       res => {
  //         this.isLoader = false;
  //         if (res.IsSuccess) {
  //           (<any>$("#sfPopup")).modal("hide");
  //           this.dataService.SF_EDI_DATA_ID = res.Data != null ? res.Data : "";
  //           this.dataService.SFSelectedBusinessLine = this.sfTestRequest.BusinessLine;
  //           this.redirectOnEtrfPage();
  //         } else {
  //           this.sfTestRequest.BusinessLine = blInfo;
  //           swal("", res.Message);
  //         }
  //       },
  //       err => {
  //         this.sfTestRequest.BusinessLine = blInfo;
  //         this.isLoader = false;
  //       }
  //     );
  // }

  getSKUColorAndPOList() {
    this.isLoader = true;
    let VendorId =
      this.userDetail.VendorId == null ? 0 : this.userDetail.VendorId;
    this.dataService
      .get("DynamicFormCreation/GetSKUColorAndPOList?VendorId=" + VendorId)
      .subscribe(
        res => {
          this.isLoader = false;
          (<any>$("#sfPopup")).modal("show");
          if (res.IsSuccess) {
            this.EDI_Data_List = res.Data;
            this.Filtered_EDI_Data_List = new Array<SF_EDI_Data_Extended>();
            this.Selected_EDI_Data_List = this.EDI_Data_List.filter(i => i.IsSelected == true);
            // this.SKUColorAndPOList = res.Data;
            // this.BusinessLineList = new Array<string>();
            // this.BrandSKUList = new Array<string>();
            // this.ColorList = new Array<string>();
            // this.PONumberList = new Array<string>();
            // this.SKUColorAndPOList.forEach(element => {
            //   this.BusinessLineList.push(element.BusinessLine);
            // });
          } else {
            swal("", res.Message);
          }
        },
        err => {
          this.isLoader = false;
          swal("", err.Message);
        }
      );
  }

  // getSKUColorAndPOList() {
  //   this.isLoader = true;
  //   let VendorId =
  //     this.userDetail.VendorId == null ? 0 : this.userDetail.VendorId;
  //   this.dataService
  //     .get("DynamicFormCreation/GetSKUColorAndPOList?VendorId=" + VendorId)
  //     .subscribe(
  //       res => {
  //         this.isLoader = false;
  //         (<any>$("#sfPopup")).modal("show");
  //         if (res.IsSuccess) {
  //           this.SKUColorAndPOList = res.Data;
  //           this.BusinessLineList = new Array<string>();
  //           this.BrandSKUList = new Array<string>();
  //           this.ColorList = new Array<string>();
  //           this.PONumberList = new Array<string>();
  //           this.SKUColorAndPOList.forEach(element => {
  //             this.BusinessLineList.push(element.BusinessLine);
  //           });
  //         } else {
  //           swal("", res.Message);
  //         }
  //       },
  //       err => {
  //         this.isLoader = false;
  //         swal("", err.Message);
  //       }
  //     );
  // }

  public renderList(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data}</div>
      </div>`;

    return html;
  }


  updateAutocomplete(key: string) {
    this.displayTable = false;
    //this.validateCheckbox();
    //this.resetFilterEDIDataList();

    // this.Filtered_EDI_Data_List.forEach(element => {
    //   element.IsSelected = false;
    // });

    // this.EDI_Data_List.forEach(element => {
    //   element.IsSelected = false;
    // });
    if (key == "businessLine") {
      this.EDI_Data_List.forEach(element => {
        element.IsSelected = false;
      });
    }
    this.validateCheckboxFromCode();
    //this.isValidated = true;

    if (
      this.sfTestRequest.TestRequestType == 1 ||
      this.sfTestRequest.TestRequestType == 3
    ) {
      this.filterEDIDataForGarmentAndBoth(key);
    } else if (this.sfTestRequest.TestRequestType == 2) {
      this.filterEDIDataForFabric(key);
    } else if (this.sfTestRequest.TestRequestType == 4) {
      this.filterEDIDataForInk(key);
    }
    // this.Filtered_EDI_Data_List.forEach(element => {
    //   element.IsSelected=false;
    // });
    // this.validateCheckbox();
  }

  filterEDIDataForGarmentAndBoth(key: string) {
    let selectedBL = this.BusinessLines.filter(
      i => i.Index == parseInt(this.sfTestRequest.BusinessLine, 10)
    )[0];

    switch (key) {
      case "businessLine":
        this.Filtered_EDI_Data_List = new Array<SF_EDI_Data_Extended>();
        this.BrandSKUList = new Array<string>();
        this.FabricIds = new Array<string>();
        this.ColorList = new Array<string>();
        this.PONumberList = new Array<string>();

        this.sfTestRequest.BrandSKU = "";
        this.sfTestRequest.Garment_Color = "";
        this.sfTestRequest.PONumber = "";
        this.sfTestRequest.FabricId = "";
        this.filterGarmentEDIDataByBusinessLine(selectedBL);
        // if(selectedBL.Value.toLowerCase()=="mens" ||selectedBL.Value.toLowerCase()=="womens" ){
        //   this.displayTable=true;
        // }
        break;

      case "sku":
        this.ColorList = new Array<string>();
        this.PONumberList = new Array<string>();
        this.FabricIds = new Array<string>();

        this.sfTestRequest.Garment_Color = "";
        this.sfTestRequest.PONumber = "";
        this.sfTestRequest.FabricId = "";
        if (this.sfTestRequest.BrandSKU) {
          this.filterGarmentEDIDataByBrandSKU(selectedBL);
        } else {
          this.filterGarmentEDIDataByBusinessLine(selectedBL);
        }

        break;

      case "color":

        this.PONumberList = new Array<string>();
        this.FabricIds = new Array<string>();

        this.sfTestRequest.PONumber = "";
        this.sfTestRequest.FabricId = "";
        if (this.sfTestRequest.Garment_Color) {
          this.filterGarmentEDIDataByColor(selectedBL);
        } else {
          // this.filterGarmentEDIDataByBrandSKU(selectedBL);
          this.filterGarmentEDIDataByBrandSKUOnChange(selectedBL);
        }

        break;

      case "PO":
        if (this.sfTestRequest.PONumber) {
          // if (selectedBL.Value.toLowerCase() == "mens" || selectedBL.Value.toLowerCase() == "womens") {
          //   this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
          //     i =>
          //       (i.Business_Line == selectedBL.Value &&
          //         i.Brand_Sku == this.sfTestRequest.BrandSKU &&
          //         i.Garment_Color == this.sfTestRequest.Garment_Color &&
          //         i.PO_Number == this.sfTestRequest.PONumber) || i.IsSelected == true //&&
          //     //(i.Fabric_ID == null || i.Fabric_ID == "")
          //   );
          // }
          // else {
          //   this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
          //     i =>
          //       i.Business_Line == selectedBL.Value &&
          //       i.Brand_Sku == this.sfTestRequest.BrandSKU &&
          //       i.Garment_Color == this.sfTestRequest.Garment_Color &&
          //       i.PO_Number == this.sfTestRequest.PONumber
          //   );
          // }
          this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
            i =>
              i.Business_Line == selectedBL.Value &&
              i.Brand_Sku == this.sfTestRequest.BrandSKU &&
              i.Garment_Color == this.sfTestRequest.Garment_Color &&
              i.PO_Number == this.sfTestRequest.PONumber
          );
          this.displayTable = true;
        } else {
          this.filterGarmentEDIDataByColor(selectedBL);
        }

        break;

      default:
        break;
    }
  }

  private filterGarmentEDIDataByColor(selectedBL: any) {
    // this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
    //   i =>
    //     (i.Business_Line == selectedBL.Value &&
    //       i.Brand_Sku == this.sfTestRequest.BrandSKU &&
    //       i.Garment_Color == this.sfTestRequest.Garment_Color) || i.IsSelected == true //&&
    //   // i.PO_Number != null &&
    //   // i.PO_Number != "" &&
    //   //(i.Fabric_ID == null || i.Fabric_ID == "")
    // );

    // if (selectedBL.Value.toLowerCase() == "mens" || selectedBL.Value.toLowerCase() == "womens") {
    //   this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
    //     i =>
    //       (i.Business_Line == selectedBL.Value &&
    //         i.Brand_Sku == this.sfTestRequest.BrandSKU &&
    //         i.Garment_Color == this.sfTestRequest.Garment_Color) || i.IsSelected == true
    //   );
    // }
    // else {

    //   this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
    //     i =>
    //       i.Business_Line == selectedBL.Value &&
    //       i.Brand_Sku == this.sfTestRequest.BrandSKU &&
    //       i.Garment_Color == this.sfTestRequest.Garment_Color
    //   );
    // }
    this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
      i =>
        (i.Business_Line == selectedBL.Value &&
          i.Brand_Sku == this.sfTestRequest.BrandSKU &&
          i.Garment_Color == this.sfTestRequest.Garment_Color) || i.IsSelected == true
    );


    this.PONumberList = new Array<string>();
    this.Filtered_EDI_Data_List.forEach(element => {
      if (element.PO_Number) {
        this.PONumberList.push(element.PO_Number);
      }
    });

    this.PONumberList = this.PONumberList.filter(this.onlyUnique);
    this.displayTable = true;
  }

  private filterGarmentEDIDataByBrandSKU(selectedBL: any) {

    if (selectedBL.Value.toLowerCase() == "mens" || selectedBL.Value.toLowerCase() == "womens") {
      this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
        i =>
          (i.Business_Line == selectedBL.Value &&
            i.Brand_Sku == this.sfTestRequest.BrandSKU) || i.IsSelected == true
      );
    }
    else {
      this.EDI_Data_List.forEach(element => {
        element.IsSelected = false;
      });
      this.Selected_EDI_Data_List = this.EDI_Data_List.filter(i => i.IsSelected == true);
      this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
        i =>
          i.Business_Line == selectedBL.Value &&
          i.Brand_Sku == this.sfTestRequest.BrandSKU
      );
    }

    this.ColorList = new Array<string>();
    if (this.Filtered_EDI_Data_List.length > 0) {
      this.Filtered_EDI_Data_List.forEach(element => {
        this.ColorList.push(element.Garment_Color);
      });
      this.ColorList = this.ColorList.filter(this.onlyUnique);
    }
    this.displayTable = true;
    this.validateCheckboxFromCode();
  }

  private filterGarmentEDIDataByBrandSKUOnChange(selectedBL: any) {
    this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
      i =>
        (i.Business_Line == selectedBL.Value &&
          i.Brand_Sku == this.sfTestRequest.BrandSKU) || i.IsSelected == true
    );

    this.ColorList = new Array<string>();
    if (this.Filtered_EDI_Data_List.length > 0) {
      this.Filtered_EDI_Data_List.forEach(element => {
        this.ColorList.push(element.Garment_Color);
      });
      this.ColorList = this.ColorList.filter(this.onlyUnique);
    }
    this.displayTable = true;
    this.validateCheckboxFromCode();
  }

  private filterGarmentEDIDataByBusinessLine(selectedBL: any) {

    if (selectedBL.Value.toLowerCase() == "mens" || selectedBL.Value.toLowerCase() == "womens") {
      this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
        i =>
          i.Business_Line == selectedBL.Value //&&
        // i.PO_Number != null &&
        // i.PO_Number != ""  
      );
      this.displayTable = true;
    }
    else {
      this.EDI_Data_List.forEach(element => {
        element.IsSelected = false;
      });
      this.Selected_EDI_Data_List = this.EDI_Data_List.filter(i => i.IsSelected == true);
      this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
        i =>
          i.Business_Line == selectedBL.Value //&&
        // i.PO_Number != null &&
        // i.PO_Number != ""  
      );
    }


    this.BrandSKUList = new Array<string>();
    this.Filtered_EDI_Data_List.forEach(element => {
      this.BrandSKUList.push(element.Brand_Sku);
      //element.IsSelected = false;
    });

    this.BrandSKUList = this.BrandSKUList.filter(this.onlyUnique);
    this.validateCheckboxFromCode();
  }

  filterEDIDataForFabric(key: string) {
    //debugger
    let selectedBL = this.BusinessLines.filter(
      i => i.Index == parseInt(this.sfTestRequest.BusinessLine, 10)
    )[0];

    switch (key) {
      case "businessLine":
        this.Filtered_EDI_Data_List = new Array<SF_EDI_Data_Extended>();
        this.BrandSKUList = new Array<string>();
        this.FabricIds = new Array<string>();
        this.ColorList = new Array<string>();
        this.PONumberList = new Array<string>();

        this.sfTestRequest.BrandSKU = "";
        this.sfTestRequest.Garment_Color = "";
        this.sfTestRequest.PONumber = "";
        this.sfTestRequest.FabricId = "";

        this.filterFabricEDIDataByBusinessLine(selectedBL);

        break;

      case "sku":
        this.ColorList = new Array<string>();
        this.PONumberList = new Array<string>();
        this.FabricIds = new Array<string>();

        this.sfTestRequest.Garment_Color = "";
        this.sfTestRequest.PONumber = "";
        this.sfTestRequest.FabricId = "";

        if (this.sfTestRequest.BrandSKU) {
          this.filterFabricEDIDataByBrandSKU(selectedBL);
        } else {
          this.filterFabricEDIDataByBusinessLine(selectedBL);
        }

        break;

      case "color":
        this.PONumberList = new Array<string>();
        this.FabricIds = new Array<string>();

        this.sfTestRequest.PONumber = "";
        this.sfTestRequest.FabricId = "";

        if (this.sfTestRequest.Garment_Color) {
          this.filterFabricEDIDataByColor(selectedBL);
        } else {
          this.filterFabricEDIDataByBrandSKUOnChange(selectedBL);
        }

        break;

      case "fabricId":
        if (this.sfTestRequest.FabricId) {
          this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
            i =>
              (i.Business_Line == selectedBL.Value &&
                i.Brand_Sku == this.sfTestRequest.BrandSKU &&
                i.Garment_Color == this.sfTestRequest.Garment_Color &&
                i.Fabric_ID == this.sfTestRequest.FabricId) || i.IsSelected == true
          );
          this.displayTable = true;
        } else {
          this.filterFabricEDIDataByColor(selectedBL);
        }

        break;

      default:
        break;
    }
  }

  private filterFabricEDIDataByColor(selBL: any) {
    this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
      i =>
        (i.Business_Line == selBL.Value &&
          i.Brand_Sku == this.sfTestRequest.BrandSKU &&
          i.Garment_Color == this.sfTestRequest.Garment_Color) || i.IsSelected == true
    );

    this.FabricIds = new Array<string>();

    this.Filtered_EDI_Data_List.forEach(element => {
      if (element.Fabric_ID) {
        this.FabricIds.push(element.Fabric_ID);
      }
    });
    this.FabricIds = this.FabricIds.filter(this.onlyUnique);
    this.displayTable = true;
  }

  private filterFabricEDIDataByBusinessLine(selectedBL: any) {
    // this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
    //   i =>
    //     i.Business_Line == selectedBL.Value //&&
    //   // i.Fabric_ID != null &&
    //   // i.Fabric_ID != ""
    // );

    if (selectedBL.Value.toLowerCase() == "mens" || selectedBL.Value.toLowerCase() == "womens") {
      this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
        i =>
          i.Business_Line == selectedBL.Value
      );
      this.displayTable = true;
    }
    else {
      this.EDI_Data_List.forEach(element => {
        element.IsSelected = false;
      });
      this.Selected_EDI_Data_List = this.EDI_Data_List.filter(i => i.IsSelected == true);
      this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
        i =>
          i.Business_Line == selectedBL.Value
      );
    }

    this.BrandSKUList = new Array<string>();
    this.Filtered_EDI_Data_List.forEach(element => {
      this.BrandSKUList.push(element.Brand_Sku);
      //element.IsSelected = false;
    });

    this.BrandSKUList = this.BrandSKUList.filter(this.onlyUnique);
    this.validateCheckboxFromCode();
  }

  private filterFabricEDIDataByBrandSKU(sbl: any) {
    this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
      i =>
        (i.Business_Line == sbl.Value &&
          i.Brand_Sku == this.sfTestRequest.BrandSKU) || i.IsSelected == true//&&
      //(i.Fabric_ID != null && i.Fabric_ID != "")
    );

    this.ColorList = new Array<string>();

    if (this.Filtered_EDI_Data_List.length > 0) {
      this.Filtered_EDI_Data_List.forEach(element => {
        this.ColorList.push(element.Garment_Color);
      });
      this.ColorList = this.ColorList.filter(this.onlyUnique);
    }
    this.displayTable = true;
    this.validateCheckboxFromCode();
  }

  private filterFabricEDIDataByBrandSKUOnChange(sbl: any) {
    this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
      i =>
        (i.Business_Line == sbl.Value &&
          i.Brand_Sku == this.sfTestRequest.BrandSKU) || i.IsSelected == true
    );

    this.ColorList = new Array<string>();

    if (this.Filtered_EDI_Data_List.length > 0) {
      this.Filtered_EDI_Data_List.forEach(element => {
        this.ColorList.push(element.Garment_Color);
      });
      this.ColorList = this.ColorList.filter(this.onlyUnique);
    }
    this.displayTable = true;
    this.validateCheckboxFromCode();
  }

  filterEDIDataForInk(key: string) {
    let selectedBL = this.BusinessLines.filter(
      i => i.Index == parseInt(this.sfTestRequest.BusinessLine, 10)
    )[0];

    switch (key) {
      case "businessLine":
        this.Filtered_EDI_Data_List = new Array<SF_EDI_Data_Extended>();
        this.BrandSKUList = new Array<string>();
        this.FabricIds = new Array<string>();
        this.ColorList = new Array<string>();
        this.PONumberList = new Array<string>();

        this.sfTestRequest.BrandSKU = "";
        this.sfTestRequest.Garment_Color = "";
        this.sfTestRequest.PONumber = "";
        this.sfTestRequest.FabricId = "";

        this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
          i => i.Business_Line == selectedBL.Value
        );

        this.Filtered_EDI_Data_List.forEach(element => {
          this.BrandSKUList.push(element.Brand_Sku);
          //element.IsSelected = false;
        });

        this.BrandSKUList = this.BrandSKUList.filter(this.onlyUnique);
        this.validateCheckboxFromCode();
        break;

      case "sku":
        this.ColorList = new Array<string>();
        this.PONumberList = new Array<string>();
        this.FabricIds = new Array<string>();

        this.sfTestRequest.Garment_Color = "";
        this.sfTestRequest.PONumber = "";
        this.sfTestRequest.FabricId = "";

        if (this.sfTestRequest.BrandSKU) {
          this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
            i =>
              (i.Business_Line == selectedBL.Value &&
                i.Brand_Sku == this.sfTestRequest.BrandSKU) || i.IsSelected == true
          );
          this.displayTable = true;
          //this.validateCheckboxFromCode();
        } else {
          this.EDI_Data_List.forEach(element => {
            element.IsSelected = false;
          });
          this.Selected_EDI_Data_List = this.EDI_Data_List.filter(i => i.IsSelected == true);
          this.Filtered_EDI_Data_List = this.EDI_Data_List.filter(
            i => i.Business_Line == selectedBL.Value
          );
          this.BrandSKUList = new Array<string>();
          this.Filtered_EDI_Data_List.forEach(element => {
            this.BrandSKUList.push(element.Brand_Sku);
          });

          this.BrandSKUList = this.BrandSKUList.filter(this.onlyUnique);
          this.displayTable = false;
        }


        this.validateCheckboxFromCode();
        break;

      default:
        break;
    }
  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  // updateAutocomplete(key: string) {
  //   //debugger

  //   switch (key) {
  //     case "businessLine":
  //     this.BrandSKUList = new Array<string>();
  //       this.FabricIds = new Array<string>();
  //       this.ColorList = new Array<string>();
  //       this.PONumberList = new Array<string>();
  //       this.sfTestRequest.BrandSKU = "";
  //       this.sfTestRequest.Color = "";
  //       this.sfTestRequest.PONumber = "";
  //       this.sfTestRequest.FabricId = "";
  //       let selectedBL= this.BusinessLines.filter(i=>i.Index==parseInt(this.sfTestRequest.BusinessLine,10))[0];
  //       let brandSKUIndex = this.SKUColorAndPOList.findIndex(i => i.BusinessLine == selectedBL.Value);
  //       if (brandSKUIndex > -1) {
  //         this.BrandSKUList = this.SKUColorAndPOList[brandSKUIndex].BrandSKUList.map(i => i.BrandSKU);
  //       }
  //       break;

  //     case "sku":

  //     this.ColorList = new Array<string>();
  //     this.PONumberList = new Array<string>();
  //     this.FabricIds = new Array<string>();
  //       this.sfTestRequest.Color = "";
  //       this.sfTestRequest.PONumber = "";
  //       this.sfTestRequest.FabricId="";
  //       let sbl= this.BusinessLines.filter(i=>i.Index==parseInt(this.sfTestRequest.BusinessLine,10))[0];
  //       let brandSKUArray = this.SKUColorAndPOList.filter(i => i.BusinessLine == sbl.Value);
  //       if (brandSKUArray.length > 0) {
  //         let colorArray = brandSKUArray[0].BrandSKUList.filter(i => i.BrandSKU == this.sfTestRequest.BrandSKU);
  //         if (colorArray.length > 0) {
  //           this.ColorList = colorArray[0].ColorList.map(i => i.Color);
  //         }
  //       }
  //       break;

  //     case "color":
  //       this.PONumberList = new Array<string>();
  //       this.FabricIds = new Array<string>();
  //       this.sfTestRequest.PONumber = "";
  //       this.sfTestRequest.FabricId="";
  //       let selBL= this.BusinessLines.filter(i=>i.Index==parseInt(this.sfTestRequest.BusinessLine,10))[0];
  //       let brandSKUColorArray = this.SKUColorAndPOList.filter(i => i.BusinessLine == selBL.Value);
  //       if (brandSKUColorArray.length > 0) {
  //         let colorColorArray = brandSKUColorArray[0].BrandSKUList.filter(i => i.BrandSKU == this.sfTestRequest.BrandSKU);
  //         if (colorColorArray.length > 0) {
  //           let poArray = colorColorArray[0].ColorList;
  //           if (poArray.length > 0) {
  //             let poIndex = poArray.findIndex(i => i.Color == this.sfTestRequest.Color);
  //             if (poIndex > -1) {
  //               //this.validateBrandSKUAndColor();
  //               this.PONumberList = poArray[poIndex].PONumberList;
  //             }
  //           }
  //         }
  //       }
  //       break;

  //       case "fabricId":
  //       //debugger
  //       this.sfTestRequest.PONumber = "";
  //       this.PONumberList = new Array<string>();
  //       let selBLInfo= this.BusinessLines.filter(i=>i.Index==parseInt(this.sfTestRequest.BusinessLine,10))[0];
  //       let selectedBusinessLineInfo = this.SKUColorAndPOList.filter(i => i.BusinessLine == selBLInfo.Value);
  //       if (selectedBusinessLineInfo.length > 0) {
  //         let selectedBrandSKUInfo = selectedBusinessLineInfo[0].BrandSKUList.filter(i => i.BrandSKU == this.sfTestRequest.BrandSKU);
  //         if (selectedBrandSKUInfo.length > 0) {

  //           let colorArray = selectedBrandSKUInfo[0].ColorList;
  //           if (colorArray.length > 0) {
  //             let fabricIndex = colorArray.findIndex(i => i.Color == this.sfTestRequest.Color);
  //             if (fabricIndex > -1) {
  //               this.FabricIds = colorArray[fabricIndex].FabricIds;
  //               //this.validateBrandSKUAndColor();
  //             }
  //           }

  //         //  let fabridIdList= selectedBrandSKUInfo[0].FabricIds;
  //         //  if(fabridIdList.length>0){
  //         //   this.FabricIds = selectedBrandSKUInfo[0].FabricIds;
  //         //   this.validateBrandSKUAndColor();
  //         //  }
  //         }
  //       }
  //       break;

  //     default:
  //       break;
  //   }
  // }

  validateBrandSKUAndColor() {
    this.isLoader = true;
    this.sfTestRequest.ClientId = this.userCredential.CompanyId;
    this.sfTestRequest.VendorId =
      this.userCredential.UserCompanyId == 0
        ? this.userCredential.CompanyId
        : this.userCredential.UserCompanyId;
    this.dataService
      .post("DynamicFormCreation/VerifySFPopupForm", this.sfTestRequest)
      .subscribe(
        res => {
          this.isLoader = false;
          if (res.IsSuccess) {
            // swal('','Selected PO will be autopopulated!');
          } else {
          }
        },
        err => {
          this.isLoader = false;
        }
      );
  }

  validateCheckbox(ediData: any, evt) {

    let isValidated = false;
    let ediDataIndex = this.EDI_Data_List.findIndex(i => i.SF_EDI_DATA_ID == ediData.SF_EDI_DATA_ID);
    if (evt.target.checked) {
      if (this.sfTestRequest.TestRequestType == 1 || this.sfTestRequest.TestRequestType == 3) {
        //IsGarment & IsCombined validation
        if (ediData.IsGarment || ediData.IsCombined) {
          isValidated = true;
        }
      }
      else if (this.sfTestRequest.TestRequestType == 2) {
        //IsFabric validation
        isValidated = ediData.IsFabric;
      }
      // else if (this.sfTestRequest.TestRequestType == 3) {
      //   //IsCombined validation
      //   isValidated = ediData.IsCombined;
      // }
      else if (this.sfTestRequest.TestRequestType == 4) {
        //IsInk validation
        isValidated = ediData.IsInk;
      }
    }
    else {
      // let ediDataIndex = this.EDI_Data_List.findIndex(i => i.SF_EDI_DATA_ID == ediData.SF_EDI_DATA_ID);

      if (evt.target.checked) {
        this.EDI_Data_List[ediDataIndex].IsSelected = true;
      }
      else {
        this.EDI_Data_List[ediDataIndex].IsSelected = false;
      }
    }

    if (isValidated == true) {
      var that = this;
      swal(
        {
          title: "",
          text: `<span style=\'color:#4c4141;font-size:20px;font-weight:bold\'>This SKU has already been submitted! Do you still want to re-submit!! </span><br><br>`,
          html: true,
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function (isConfirm) {
          if (isConfirm) {
            if (evt.target.checked) {
              that.EDI_Data_List[ediDataIndex].IsSelected = true;
            }
            else {
              that.EDI_Data_List[ediDataIndex].IsSelected = false;
            }
            // this.Selected_EDI_Data_List = this.EDI_Data_List.filter(i => i.IsSelected == true);

          } else {
            that.EDI_Data_List[ediDataIndex].IsSelected = false;
            that.Selected_EDI_Data_List = that.EDI_Data_List.filter(i => i.IsSelected == true);
          }
        }
      );
    }



    let selectedCount = this.EDI_Data_List.filter(
      i => i.IsSelected == true
    ).length;

    if (selectedCount > 0) {
      this.isValidated = false;
    } else {
      this.isValidated = true;
    }
    this.Selected_EDI_Data_List = this.EDI_Data_List.filter(i => i.IsSelected == true);
  }


  validateCheckboxFromCode() {
    let selectedCount = this.EDI_Data_List.filter(
      i => i.IsSelected == true
    ).length;
    if (selectedCount > 0) {
      this.isValidated = false;

    } else {
      this.isValidated = true;
    }
    this.Selected_EDI_Data_List = this.EDI_Data_List.filter(i => i.IsSelected == true);
  }

  resetFilterEDIDataList() {
    this.Filtered_EDI_Data_List = new Array<SF_EDI_Data_Extended>();

    this.EDI_Data_List.forEach(element => {
      element.IsSelected = false;
    });
  }

  viewSelectedSKU() {
    this.Selected_EDI_Data_List = this.EDI_Data_List.filter(i => i.IsSelected == true);
    (<any>$("#selectedSku")).modal("show");
  }

  public renderData(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data}</div>
      </div>`;

    return html;
  }
}
