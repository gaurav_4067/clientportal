export const ETRF_Saved_List = {
  PageSize: 10,
  UserType: "",
  PageNumber: 1,
  OrderBy: "",
  Order: "desc",
  ClientId: 1,
  CreatedBy: 0,
  VendorId: 1,
  SearchText: []
};

export const ETRF_Search_Model = {
  ColumnName: "",
  Operator: 1,
  ColumnValue: ""
};

export class SFTestRequest {
  constructor(
    public TestRequestType?: number,
    public FabricId?: string,
    public BusinessLine?: string,
    public BrandSKU?: string,
    public Garment_Color?: string,
    public PONumber?: string,
    public VendorId?: number,
    public ClientId?: number
  ) {
    this.TestRequestType = TestRequestType || 1;
    this.FabricId = FabricId || "";
    this.BusinessLine = BusinessLine || "";
    this.BrandSKU = BrandSKU || "";
    this.Garment_Color = Garment_Color || "";
    this.PONumber = PONumber || "";
    this.VendorId = VendorId || 0;
    this.ClientId = ClientId || 0;
  }
}

export class BusinessLineClass {
  constructor() {
    this.BrandSKUList = new Array<BrandSKUClass>();
  }
  public BusinessLine: string;
  public BrandSKUList: Array<BrandSKUClass>;
}

export class BrandSKUClass {
  constructor() {
    this.ColorList = new Array<ColorClass>();
  }
  public BrandSKU: string;
  public ColorList: Array<ColorClass>;
}

export class ColorClass {
  constructor() {
    this.PONumberList = new Array<string>();
    this.FabricIds = new Array<string>();
  }
  public Color: string;
  public FabricIds: Array<string>;
  public PONumberList: Array<string>;
}

export class SF_EDI_Data {
  public SF_EDI_DATA_ID: number;
  public Season: string;
  public Fiscal_Month: string;
  public Fiscal_Year: string;
  public Date_Added: Date;
  public Fabric_ID: string;
  public Brand_Sku: string;
  public Style_Variant_ID: number;
  public PO_Number: string;
  public Buy_Type: string;
  public Vendor_ID: number;
  public Vendor: string;
  public ETA: Date;
  public Shipment_Date: Date;
  public Cancel_Date: Date;
  public Ordered_Status: string;
  public Product_Type_Department: string;
  public Product_Category_Class: string;
  public SF_Color: string;
  public Brand_Name: string;
  public Stitch_Fix_Style_Name: string;
  public Item_Description: string;
  public Garment_Color: string;
  public Fabric_Color: string;
  public Business_Line: string;
  public Gender_Intent: string;
  public CreatedDate: Date;
  public ModifiedDate: Date;
  public Status: boolean;
  public StatusModifiedDate: Date;
  public IsGarment:boolean;
  public IsFabric:boolean;
  public IsCombined:boolean;
  public IsInk:boolean;
}

export class SF_EDI_Data_Extended extends SF_EDI_Data {
  public IsSelected: boolean = false;
  public IsAvailable: boolean = true;
}
