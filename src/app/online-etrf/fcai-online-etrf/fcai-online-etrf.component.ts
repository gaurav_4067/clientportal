import { Component, OnInit } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { Router } from "@angular/router";

import { BaseUrl, DataService, ETRFUrl } from "../../mts.service";

import { ETRF_Saved_List, ETRF_Search_Model, FCAIOnlineServiceMapping, FCAI_Cancelled_History } from "./fca-online.model";

declare let swal: any;

@Component({
  selector: 'app-fcai-online-etrf',
  templateUrl: './fcai-online-etrf.component.html',
  styleUrls: ['./fcai-online-etrf.component.css']
})
export class FcaiOnlineEtrfComponent implements OnInit {
  invoiceList: Array<string>;
  selectedIndex: number;
  private p: any;
  private totalItem: any;
  private itemPerPage: any = 10;
  userDetail: any;
  userCredential: any;
  url: any;
  finalUrl: SafeResourceUrl;
  companyId: any;
  etrfList: any = [];
  isLoader: boolean;
  searchModel: any;
  dateFrom: any;
  dateTo: any;
  skuNumber: any;
  poNumber: any;
  etrfNumber: any;
  Status: any;
  savedETRFList: any = [];
  // itemPerPage: any = 10;
  // totalItem: any;
  searchModal: any;
  en: any;
  Usertype: any = true;
  companyName: any = "";
  etrfTypes: any = [];
  etrfTypeId: number = 0;
  buttonText: string = "";
  selectedFcaiServiceProduct = 2;
  FCAIServiceMappingList: Array<FCAIOnlineServiceMapping>;
  isInspection = false;
  applicant: string;
  vendor: string;
  factory: string;
  productDescription: string;
  ApplicantCompanyList: string[];
  VendorCompanyList: string[];
  FactoryCompanyList: string[];
  isCancelled: boolean = false;
  cancelEtrfObject: FCAI_Cancelled_History;
  previousRevisionList: any[] = [];
  constructor(
    private dataService: DataService,
    private sanitizer: DomSanitizer,
    private router: Router
  ) {
    this.invoiceList = new Array<string>();
    this.FCAIServiceMappingList = new Array<FCAIOnlineServiceMapping>();
    this.cancelEtrfObject = new FCAI_Cancelled_History();
  }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem("mts_user_cred"));
    this.userDetail = JSON.parse(localStorage.getItem("userDetail"));
    if (this.userDetail && this.userCredential) {
      if (
        !localStorage.getItem("CategoryId") ||
        localStorage.getItem("CategoryId") == null
      ) {
        this.buttonText = "Place New Order";
      }
      if (parseInt(localStorage.getItem("CategoryId")) == 2) {
        this.buttonText = "Place New Inspection Booking";
      }
      localStorage.setItem("ecommerceStatus", "False");
      localStorage.removeItem("CategoryId");
      this.en = {
        firstDayOfWeek: 0,
        dayNames: [
          "Sunday",
          "Monday",
          "Tuesday",
          "Wednesday",
          "Thursday",
          "Friday",
          "Saturday"
        ],
        dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        monthNames: [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December"
        ],
        monthNamesShort: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ]
      };
      this.searchModel = Object.assign({}, ETRF_Search_Model);

      ETRF_Saved_List.ClientId = this.userCredential.CompanyId;
      ETRF_Saved_List.SearchText = [];
      ETRF_Saved_List.PageNumber = 1;
      if (this.userCredential.UserCompanyId != 0) {
        ETRF_Saved_List.VendorId = this.userCredential.UserCompanyId;
      } else {
        ETRF_Saved_List.VendorId = this.userCredential.CompanyId;
      }
      ETRF_Saved_List.UserType = this.userCredential.UserType;
      if (this.userDetail.Status != "Pending") {
        if (!this.userCredential.UserPermission.ViewETRF) {
          this.router.navigate(["./landing-page"]);
        }
      }
      this.companyName = this.userCredential.CompanyName;

      this.url =
        ETRFUrl +
        "ETRF/Home?a=" +
        JSON.parse(localStorage.getItem("userDetail")).AuthToken +
        "&c=" +
        this.userCredential.CompanyId;
      this.finalUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
      this.companyId = this.userCredential.CompanyId;
      var PageSize = ETRF_Saved_List.PageSize;
      var that = this;
      this.searchModal = ETRF_Saved_List;
      that.isLoader = true;
      this.p = 1;
      // if (that.userDetail.Status == "Pending") {
      //   that.Usertype = false;
      //   that.searchModal.VendorId = that.userDetail.UserId;
      //   that.searchModal.ClientId = 10000;
      //   that.searchModal.EtrfTypeId = that.etrfTypeId;
      //   that.searchModal.CreatedBy = that.userCredential.UserId;
      //   that.dataService
      //     .post("DynamicFormCreation/EcommETRFSavedList", that.searchModal)
      //     .subscribe(r => {
      //       if (r.IsSuccess) {
      //         that.savedETRFList = JSON.parse(r.Data);
      //         that.savedETRFList.forEach((data, index) => {
      //           if (data.WorkingStatus == "C") {
      //             data.Status = "Canceled";
      //           }
      //         });
      //         that.totalItem = r.TotalRecords;
      //       }
      //       that.isLoader = false;
      //     });
      // } else {
      this.addStatusFilter();
      that.searchModal.CreatedBy = that.userDetail.UserId;
      that.searchModal.EtrfTypeId = that.etrfTypeId;
      that.dataService
        .post("DynamicFormCreation/FCAIOnlineETRFSavedList", that.searchModal)
        .subscribe(r => {
          if (r.IsSuccess) {
            that.savedETRFList = JSON.parse(r.Data);
            that.savedETRFList.forEach((data, index) => {
              if (data.WorkingStatus == "C") {
                data.Status = "Canceled";
              }
            });
            that.totalItem = r.TotalRecords;
          }
          that.isLoader = false;
        });
      // }
      // this.dataService.get("DynamicFormCreation/ETRFtypeList").subscribe(r => {
      //   if (r.IsSuccess) {
      //     var TypeList = [];
      //     if (that.userDetail.Status == "Pending") {
      //       for (var i = 0; i < r.Data.length; i++) {
      //         if (r.Data[i].Value.toLowerCase().includes("ecom")) {
      //           TypeList.push(r.Data[i]);
      //         }
      //       }
      //     } else {
      //       if (
      //         that.userDetail.AssignedCompanyANDCategory
      //           .AssignedBusinessCategory.length > 0
      //       ) {
      //         for (
      //           var j = 0;
      //           j <
      //           that.userDetail.AssignedCompanyANDCategory
      //             .AssignedBusinessCategory.length;
      //           j++
      //         ) {
      //           if (
      //             that.userDetail.AssignedCompanyANDCategory
      //               .AssignedBusinessCategory[j].BusinessCategoryName ==
      //             "Testing"
      //           ) {
      //             TypeList.push(r.Data.filter(i => i.Value == "Testing")[0]);
      //           }
      //           if (
      //             that.userDetail.AssignedCompanyANDCategory
      //               .AssignedBusinessCategory[j].BusinessCategoryName ==
      //             "Inspection"
      //           ) {
      //             TypeList.push(
      //               r.Data.filter(i => i.Value == "Inspection Service")[0]
      //             );
      //           }
      //           if (
      //             that.userDetail.AssignedCompanyANDCategory
      //               .AssignedBusinessCategory[j].BusinessCategoryName == "FCAI"
      //           ) {
      //             TypeList.push(r.Data.filter(i => i.Value == "FCAI")[0]);
      //           }
      //         }
      //         if (that.userCredential.UserPermission.ViewECommerce) {
      //           for (var i = 0; i < r.Data.length; i++) {
      //             if (r.Data[i].Value.toLowerCase().includes("ecom")) {
      //               TypeList.push(r.Data[i]);
      //             }
      //           }
      //         }
      //       }
      //     }
      //     that.etrfTypes = TypeList;
      //   }
      // });
      // if (
      //   this.userCredential.BusinessCategoryId == 2 &&
      //   this.userDetail.IsFCAI
      // ) {
      this.getApplicantVendorFactoryList();
      this.getFCAIServiceInfo();
      // }
    } else {
      this.router.navigate(["/login"]);
    }
  }

  addNewETRF(e) {
    e.preventDefault();
    var that = this;
    if (
      that.userCredential.BusinessCategoryId == 2 &&
      that.FCAIServiceMappingList.length > 0
    ) {
      (<any>$("#serviceSelection")).modal("show");
    } else {
      this.openAddNewEtrf(that);
    }
  }

  private openAddNewEtrf(that: this) {
    that.isLoader = true;
    that.dataService
      .get(
        "DynamicFormCreation/GetETRFList?CompanyId=" +
        that.companyId +
        "&DivisionId=0"
      )
      .subscribe(r => {
        that.isLoader = false;
        that.etrfList = r.Data;
        if (that.etrfList.length > 1) {
          (<any>$("#etrfList")).modal("show");
        } else if (that.etrfList.length == 1) {
          that.router.navigate([
            "/online-etrf/add-etrf",
            that.etrfList[0].FormId
          ]);
        } else {
          swal("", "No e-TRF");
        }
      });
  }

  clickETRF(id: any, e: any) {
    e.preventDefault();
    (<any>$("#etrfList")).modal("hide");
    this.router.navigate(["/online-etrf/add-etrf", id]);
  }

  search() {
    var that = this;
    this.searchModal = ETRF_Saved_List;

    that.isLoader = true;
    this.p = 1;

    // if (that.userDetail.Status == "Pending") {
    //   that.Usertype = false;
    //   that.searchModal.VendorId = that.userDetail.UserId;
    //   that.searchModal.ClientId = 10000;
    //   that.searchModal.PageNumber = 1;
    //   that.searchModal.EtrfTypeId = this.etrfTypeId;
    //   that.dataService
    //     .post("DynamicFormCreation/EcommETRFSavedList", that.searchModal)
    //     .subscribe(r => {
    //       if (r.IsSuccess) {
    //         that.savedETRFList = JSON.parse(r.Data);
    //         that.savedETRFList.forEach((data, index) => {
    //           if (data.WorkingStatus == "C") {
    //             data.Status = "Canceled";
    //           }
    //         });
    //         // that.totalItem = Math.ceil(r.TotalRecords /
    //         // that.searchModal.pageSize);
    //         that.totalItem = r.TotalRecords;
    //       }
    //       that.isLoader = false;
    //     });
    // } else {
    this.addStatusFilter();
    that.searchModal.EtrfTypeId = this.etrfTypeId;
    that.searchModal.PageNumber = 1;
    that.dataService
      .post("DynamicFormCreation/FCAIOnlineETRFSavedList", that.searchModal)
      .subscribe(r => {
        if (r.IsSuccess) {
          this.isCancelled = this.Status == 'C' ? true : false;
          that.savedETRFList = JSON.parse(r.Data);
          that.savedETRFList.forEach((data, index) => {
            if (data.WorkingStatus == "C") {
              data.Status = "Canceled";
            }
          });
          // that.totalItem = Math.ceil(r.TotalRecords /
          // that.searchModal.pageSize);
          that.totalItem = r.TotalRecords;
        }
        that.isLoader = false;
      });
    // }
  }

  reset() {
    this.dateFrom = "";
    this.dateTo = "";
    this.skuNumber = "";
    this.poNumber = "";
    this.etrfNumber = "";
    this.Status = "";
    this.etrfTypeId = 0;
    this.applicant = "";
    this.vendor = "";
    this.factory = "";
    this.productDescription = "";
    ETRF_Saved_List.SearchText = [];
    this.search();
  }

  searchModelChange(val: any) {
    if (val == "dateFrom") {
      let present = false;
      ETRF_Search_Model.ColumnName = "ModifiedOn";
      ETRF_Search_Model.Operator = 7;
      ETRF_Search_Model.ColumnValue = this.dateFrom;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.Operator == 7) {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "dateTo") {
      let present = false;
      ETRF_Search_Model.ColumnName = "ModifiedOn";
      ETRF_Search_Model.Operator = 8;
      ETRF_Search_Model.ColumnValue = this.dateTo;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.Operator == 8) {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "skuNumber") {
      let present = false;
      ETRF_Search_Model.ColumnName = "E_TRF_SKU_Number";
      ETRF_Search_Model.Operator = 5;
      ETRF_Search_Model.ColumnValue = this.skuNumber;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "E_TRF_SKU_Number") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }

      if (this.userCredential.BusinessCategoryId == 2 &&
        this.userDetail.IsFCAI) {
        let presentNew = false;
        ETRF_Search_Model.ColumnName = "TRF_Costco_Item_No";
        ETRF_Search_Model.Operator = 5;
        ETRF_Search_Model.ColumnValue = this.skuNumber;
        ETRF_Search_Model.LogicalOperator = "OR";
        ETRF_Saved_List.SearchText.forEach((data, index) => {
          if (data.ColumnName == "TRF_Costco_Item_No") {
            ETRF_Saved_List.SearchText.splice(
              index,
              1,
              Object.assign({}, ETRF_Search_Model)
            );
            presentNew = true;
          }
        });
        if (presentNew == false) {
          ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
        }
      }
      ETRF_Search_Model.LogicalOperator = "";
    }
    if (val == "poNumber") {
      let present = false;
      ETRF_Search_Model.ColumnName = "E_TRF_PO_Number";
      ETRF_Search_Model.Operator = 5;
      ETRF_Search_Model.ColumnValue = this.poNumber;

      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "E_TRF_PO_Number") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
      if (this.userCredential.BusinessCategoryId == 2 &&
        this.userDetail.IsFCAI) {
        let presentNew = false;
        ETRF_Search_Model.ColumnName = "TRF_PO_Number";
        ETRF_Search_Model.Operator = 5;
        ETRF_Search_Model.ColumnValue = this.poNumber;
        ETRF_Search_Model.LogicalOperator = "OR";
        ETRF_Saved_List.SearchText.forEach((data, index) => {
          if (data.ColumnName == "TRF_PO_Number") {
            ETRF_Saved_List.SearchText.splice(
              index,
              1, Object.assign({}, ETRF_Search_Model)
            );
            presentNew = true;
          }
        });
        if (presentNew == false) {
          ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
        }
      }
      ETRF_Search_Model.LogicalOperator = "";
    }
    if (val == "productDescription") {
      let present = false;
      ETRF_Search_Model.ColumnName = "TRF_Product_Description";
      ETRF_Search_Model.Operator = 5;
      ETRF_Search_Model.ColumnValue = this.productDescription;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == 'TRF_Product_Description') {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "etrfNumber") {
      let present = false;
      ETRF_Search_Model.ColumnName = "(E_TRFID + ISNULL(Revision,''))";
      ETRF_Search_Model.Operator = 5;
      ETRF_Search_Model.ColumnValue = this.etrfNumber;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "(E_TRFID + ISNULL(Revision,''))") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "Status") {
      let present = false;
      if (this.Status != "C") {
        ETRF_Search_Model.ColumnName = "Status";
      } else {
        ETRF_Search_Model.ColumnName = "WorkingStatus";
      }
      ETRF_Search_Model.Operator = 1;
      ETRF_Search_Model.ColumnValue = this.Status;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "Status" || data.ColumnName == "WorkingStatus") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "applicant") {
      let present = false;
      ETRF_Search_Model.ColumnName = "TRF_Applicant_Name";
      ETRF_Search_Model.Operator = 1;
      ETRF_Search_Model.ColumnValue = this.applicant;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "TRF_Applicant_Name") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "vendor") {
      let present = false;
      ETRF_Search_Model.ColumnName = "TRF_Vendor_Name";
      ETRF_Search_Model.Operator = 1;
      ETRF_Search_Model.ColumnValue = this.vendor;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "TRF_Vendor_Name") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
    if (val == "factory") {
      let present = false;
      ETRF_Search_Model.ColumnName = "TRF_Factory_Name";
      ETRF_Search_Model.Operator = 1;
      ETRF_Search_Model.ColumnValue = this.factory;
      ETRF_Saved_List.SearchText.forEach((data, index) => {
        if (data.ColumnName == "TRF_Factory_Name") {
          ETRF_Saved_List.SearchText.splice(
            index,
            1,
            Object.assign({}, ETRF_Search_Model)
          );
          present = true;
        }
      });
      if (present == false) {
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    }
  }

  // pageChange(e: any) {}

  getPage(page: number) {
    var that = this;
    this.searchModal.PageNumber = page;
    this.isLoader = true;
    that.dataService
      .post("DynamicFormCreation/FCAIOnlineETRFSavedList", that.searchModal)
      .subscribe(r => {
        if (r.IsSuccess) {
          that.savedETRFList = JSON.parse(r.Data);
          that.savedETRFList.forEach((data, index) => {
            if (data.WorkingStatus == "C") {
              data.Status = "Canceled";
            }
          });
          that.p = page;
          that.totalItem = r.TotalRecords;
        }
        that.isLoader = false;
      });
  }

  // GetEcommEtrf(Type: any) {
  //   if (Type == "Ecomm") {
  //     var that = this;
  //     this.searchModal = this.dataService.clone(ETRF_Saved_List);
  //     this.searchModal.VendorId = this.userDetail.UserId;
  //     this.searchModal.ClientId = 10000;
  //     that.isLoader = true;
  //     this.p = 1;
  //     that.dataService
  //       .post("DynamicFormCreation/EcommETRFSavedList", that.searchModal)
  //       .subscribe(r => {
  //         if (r.IsSuccess) {
  //           that.savedETRFList = JSON.parse(r.Data);
  //           that.savedETRFList.forEach((data, index) => {
  //             if (data.WorkingStatus == "C") {
  //               data.Status = "Canceled";
  //             }
  //           });
  //           // that.totalItem = Math.ceil(r.TotalRecords /
  //           // that.searchModal.pageSize);
  //           that.totalItem = r.TotalRecords;
  //         }
  //         that.isLoader = false;
  //       });
  //   } else if (Type == "Online") {
  //     this.search();
  //   }
  // }

  CancelEtrf(id: any) {
    this.cancelEtrfObject.TRF_No = id;
    var that = this;
    swal(
      {
        title: '',
        text:
          `<span style=\'color:#4c4141;font-size:20px;font-weight:bold\'>Are you sure you want to cancel <br> Booking Form "${id}" ?</span><br><br>`,
        html: true,
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Proceed',
        cancelButtonText: 'Cancel',
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function (isConfirm) {
        if (isConfirm) {
          (<any>$("#cancelEtrf")).modal("show");
        } else {
        }
      });
  }

  GetPDFList(id: any) {
    window.location.href =
      BaseUrl + "ManageDocuments/DownloadFile?FileName=ETRFPDF\\" + id + ".pdf&fileType=PDF&businessCategoryId="+this.userCredential.BusinessCategoryId;
  }

  CopyEtrf(id: any) {
    var that = this;
    this.isLoader = true;
    this.dataService
      .get(
        "DynamicFormCreation/CopyEtrf?E_TRFNo=" +
        id +
        "&UserId=" +
        this.userCredential.UserId
      )
      .subscribe(r => {
        that.isLoader = false;
        if (r.IsSuccess == true) {
          this.router.navigate(["/online-etrf/edit-etrf", r.Data]);
        } else {
          swal("", r.Message);
        }
      });
  }

  // PrintInvoice(e) {
  //   this.selectedIndex = null;
  //   var that = this;
  //   this.isLoader = true;
  //   this.dataService
  //     .get("RequestAddtionalTestItem/GetAllInvoicesName/" + e)
  //     .subscribe(r => {
  //       that.isLoader = false;
  //       if (r.IsSuccess == true) {
  //         that.invoiceList = r.Data.split(",");
  //         if (that.invoiceList.length == 1) {
  //           window.location.href =
  //             BaseUrl +
  //             "ManageDocuments/DownloadSampleFile?Folder=INVOICEECOMPDF&FileName=" +
  //             that.invoiceList[0];
  //           this.isLoader = false;
  //         } else {
  //           (<any>$("#invoiceList")).modal("show");
  //         }
  //       } else {
  //         swal("", r.Message);
  //       }
  //     });
  // }

  // downloadInvoice() {
  //   let fileName = this.invoiceList[this.selectedIndex];
  //   window.location.href =
  //     BaseUrl +
  //     "ManageDocuments/DownloadSampleFile?Folder=INVOICEECOMPDF&FileName=" +
  //     fileName;
  //   (<any>$("#invoiceList")).modal("hide");
  // }

  getSelectedService(type: number) {
    (<any>$("#serviceSelection")).modal("hide");
    if (type > 0) {
      let fcaiObject = this.FCAIServiceMappingList.filter(
        i => i.ServiceId == type
      );
      if (fcaiObject.length > 0) {
        this.router.navigate(["/online-etrf/add-etrf", fcaiObject[0].FormId]);
      } else {
        swal("", "No e-TRF");
      }
    } else {
      this.openAddNewEtrf(this);
    }
  }

  getFCAIServiceInfo() {
    this.dataService
      .get(
        "FCAIOnline/GetFCAIServiceMapping/" +
        this.userCredential.UserId +
        "/" +
        this.companyId
      )
      .subscribe(
        res => {
          if (res.IsSuccess) {
            this.FCAIServiceMappingList = res.Data;
            this.isInspection = res.IsInspection;
          } else {
            swal("", res.Message);
          }
        },
        err => { }
      );
  }

  getApplicantVendorFactoryList() {
    this.dataService
      .get(
        "FCAIOnline/GetApplicantVendorFactoryList/" +
        this.userCredential.UserId +
        "/" +
        this.userCredential.CompanyId +
        "/" +
        this.userCredential.UserType +
        "/" + this.userCredential.UserCompanyId
      )
      .subscribe(
        res => {
          if (res.IsSuccess) {
            this.ApplicantCompanyList = res.Data.ApplicantCompany;
            this.VendorCompanyList = res.Data.VendorCompany;
            this.FactoryCompanyList = res.Data.FactoryCompany;
          } else {
            swal('', res.Message);
          }
        },
        err => { }
      );
  }

  saveCancelEtrfInfo() {
    if (this.cancelEtrfObject.Reason_For_Cancellation.trim() != '' && this.cancelEtrfObject.Remark.trim() != '') {
      this.isLoader = true;
      this.cancelEtrfObject.Cancelled_By = this.userCredential.UserId;
      this.dataService.post('DynamicFormCreation/FCAIOnlineCancelEtrf', this.cancelEtrfObject)
        .subscribe(r => {
          this.isLoader = false;
          if (r.IsSuccess == true) {
            (<any>$("#cancelEtrf")).modal("hide");
            swal('', 'Etrf Cancelled');
            this.search();
          } else {
            this.isLoader = false;
            swal('', r.Message);
          }
        })
    } else {
      swal('', 'Please fill all informartion!');
    }
  }

  resetCancelEtrfPopup() {
    this.cancelEtrfObject = new FCAI_Cancelled_History();
  }

  addStatusFilter() {
    if (ETRF_Saved_List.SearchText.length > 0) {
      let index = ETRF_Saved_List.SearchText.findIndex(i => (i.ColumnName == "Status" || i.ColumnName == "WorkingStatus"));
      if (index == -1) {
        ETRF_Search_Model.ColumnName = "WorkingStatus";
        ETRF_Search_Model.Operator = 2;
        ETRF_Search_Model.ColumnValue = 'C';
        ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
      }
    } else {
      ETRF_Search_Model.ColumnName = "WorkingStatus";
      ETRF_Search_Model.Operator = 2;
      ETRF_Search_Model.ColumnValue = 'C';
      ETRF_Saved_List.SearchText.push(Object.assign({}, ETRF_Search_Model));
    }
  }

  openPreviousEtrfRevisions(etrfId: string) {
    this.isLoader = true;
    this.dataService.get('FCAIOnline/GetPreviousEtrfRevisions/' + etrfId).subscribe(
      res => {
        this.isLoader = false;
        if (res.IsSuccess) {
          this.previousRevisionList = res.Data;
          (<any>$("#previousEtrfRevisions")).modal("show");
        }
      },
      err => {
        this.isLoader = false;
      });
  }

}

