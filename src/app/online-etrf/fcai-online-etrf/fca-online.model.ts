export const ETRF_Saved_List = {
    "PageSize": 10,
    "UserType": "",
    "PageNumber": 1,
    "OrderBy": "",
    "Order": "desc",
    "ClientId": 1,
    "CreatedBy": 0,
    "VendorId": 1,
    "SearchText": []
}

export const ETRF_Search_Model = {
    "ColumnName": "",
    "Operator": 1,
    "ColumnValue": "",
    "LogicalOperator":""
}

export class FCAIOnlineServiceMapping {
    constructor(public Id?:number,public UserId?:number,public ClientId?:number,public ServiceId?:number, public FormId?:number, public Active?:boolean) {
        this.Id = Id || 0;
        this.ClientId = ClientId || 0;
        this.ServiceId = ServiceId || 0;
        this.FormId = FormId || 0;
        this.Active = Active || false;
    }
}

export class CancelEtrf {
    constructor(public ReasonForCancellation?:String, public Remark?:String, public CancelEtrfId?:number, public UserId?:number) {
        this.ReasonForCancellation = ReasonForCancellation || '';
        this.Remark = Remark || '';
        this.CancelEtrfId = CancelEtrfId || 0;
        this.UserId = UserId || 0;
    }
}

export class FCAI_Cancelled_History {
    constructor(public Cancelled_HistoryID?:number, public TRF_No?:number, public Cancel_Date?:Date, public Approved_By?:number, public Cancelled_By?:number, public Reason_For_Cancellation?:string, public Remark?:string) {
        this.Cancelled_HistoryID = Cancelled_HistoryID || 0;
        this.TRF_No = TRF_No || 0;
        this.Cancel_Date = Cancel_Date || new Date();
        this.Approved_By = Approved_By || 0;
        this.Cancelled_By = Cancelled_By || 0;
        this.Reason_For_Cancellation = Reason_For_Cancellation || '';
        this.Remark = Remark || '';
    }
}