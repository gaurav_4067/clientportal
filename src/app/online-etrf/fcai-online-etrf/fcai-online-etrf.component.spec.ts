import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FcaiOnlineEtrfComponent } from './fcai-online-etrf.component';

describe('FcaiOnlineEtrfComponent', () => {
  let component: FcaiOnlineEtrfComponent;
  let fixture: ComponentFixture<FcaiOnlineEtrfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FcaiOnlineEtrfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FcaiOnlineEtrfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
