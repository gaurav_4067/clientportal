import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MdInputModule } from '@angular2-material/input';
import { Ng2PaginationModule } from 'ng2-pagination';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { FooterModule } from 'app/footer/footer.module';
import { HeaderModule } from 'app/header/header.module';
import { OnlineEtrfRoutingModule } from '../online-etrf-routing.module';
import { FcaiOnlineEtrfComponent } from './fcai-online-etrf.component';
import { MaterialModule, MdCoreModule } from '@angular/material';
import { AddEtrfModule } from '../add-etrf/add-etrf.module';


@NgModule({
  imports: [
    CommonModule, OnlineEtrfRoutingModule, HeaderModule, FooterModule,
    AddEtrfModule, FormsModule, DateTimePickerModule, MdInputModule,
    MdCoreModule, MaterialModule, Ng2PaginationModule
  ],
  declarations: [FcaiOnlineEtrfComponent],
  exports: [FcaiOnlineEtrfComponent]
})
export class FcaiOnlineEtrfModule { }