import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineEtrfComponent } from './online-etrf.component';

describe('OnlineEtrfComponent', () => {
  let component: OnlineEtrfComponent;
  let fixture: ComponentFixture<OnlineEtrfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineEtrfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineEtrfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
