import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  Output,
  ViewChild
} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import domtoimage from "dom-to-image";
import { BaseUrl, DataService } from "../../mts.service";

import { ETRF_Model, ImagesPDF } from './add-etrf.model';
import { EtrfConfigurationRoutingModule } from 'app/admin/etrf-configuration/etrf-configuration-routing.module';

import { SF_EDI_Data } from "../online-etrf.model";

// import * as html2canvas from 'html2canvas';
// import * as jsPDF from 'jspdf';
declare let swal: any;
declare let html2canvas: any;
declare let jsPDF: any;

@Component({
  selector: "app-add-etrf",
  templateUrl: "./add-etrf.component.html",
  styleUrls: ["./add-etrf.component.css"]
})
export class AddEtrfComponent implements OnInit {
  vendorId: number;
  ediDataList: Array<SF_EDI_Data>;


  fieldValue: string = "";

  @Input() data;
  testRequestType: any;
  constructor(
    public router: Router,
    private dataService: DataService,
    private sanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute
  ) {
    this.dataService.DynamicDropDependentValue = [];
    this.ediDataList = new Array<SF_EDI_Data>();
  }
  isLoader: boolean;
  GenratedETRFNo: any = "";
  isSendEmail: boolean = false;
  ShowFieldId: any;
  ETRFsection: any = [];
  formName: any = "";
  EcomStatus: any = true;
  ServiceName: any;
  Json: any;
  isEditCase: any = false;
  breedcrumbName: any;
  IsAdditionalNotesSection: any = false;
  AdditionalNote: any = "";
  companyId: any;
  etrfIdOnSave: any = "";
  pdfLink: any = "";
  ETRFID: any = "";
  Ref_ETRF_ID: string = "";
  ImgBaseArr: any = [];
  companyName: any = "";
  ready: any;
  requireCount: any;
  companyLogo: any;
  statusEcomm: any;
  userDetails: any;
  userCredentials: any;
  calledFrom: any;
  ETRFType: any = 1;
  EcommETRFtitle: any;
  mappingId: any;
  UploadFileInspection: any = [];
  BusinessId: any;
  ngOnInit() {
    this.userDetails = JSON.parse(localStorage.getItem("userDetail"));
    this.userCredentials = JSON.parse(localStorage.getItem("mts_user_cred"));
    if (this.userCredentials && this.userDetails) {
      this.statusEcomm = localStorage.getItem("ecommerceStatus");
      var Service = localStorage.getItem("ServiceName");
      this.companyLogo = this.userCredentials.CompanyId;
      this.companyName = this.userCredentials.CompanyName;
      this.EcommETRFtitle = this.dataService.EcommETRFtitle;
      //(<any>$("body")).unbind("mousewheel");
      if (this.userCredentials.UserCompanyId < 1) {
        this.companyId = this.userCredentials.CompanyId;
      } else {
        this.companyId = this.userCredentials.UserCompanyId;
      }
      if (this.statusEcomm == "true") {
        this.EcomStatus = false;
        this.ServiceName = Service.replace('"', "").replace('"', "");
      }
      ETRF_Model.CreatedBy = this.userDetails.UserId;
      var browserURL = window.location.href;
      if (browserURL.includes("online-etrf/edit-etrf")) {
        if (!this.userCredentials.UserPermission.AddNewETRF) {
          this.router.navigate(["./landing-page"]);
        }
      }
      if (browserURL.includes("online-etrf/edit-etrf")) {
        if (!this.userCredentials.UserPermission.EditETRF) {
          this.router.navigate(["./landing-page"]);
        }
      }


      if (browserURL.includes("e-commerce")) {
        this.dataService.imageSizeForEcommEtrf = true;
        var that = this;
        that.isLoader = true;
        var LabelID = "";

        if (localStorage.getItem("CategoryId") == "2") {
          this.data.DivisionID = 9;
          this.mappingId = 0;
        } else {
          // this.mappingId =
          // that.dataService.selectedPackages[0].ServicePackageMappingId;
          this.mappingId = 0;
        }

        that.dataService
          .get(
            "DynamicFormCreation/GetEcommerceETRF?CompanyId=10000&DivisionId=" +
            this.data.DivisionID +
            "&EccomDynamicID=" +
            this.mappingId
          )
          .subscribe(r => {
            if (that.dataService.EcommETRFData.IsEcomm == true) {
              r.Data.ETRFsection.forEach((data, index) => {
                if (
                  data.dependentOnUniqueID.toLowerCase() == "pdf" &&
                  data.SectionName.toLowerCase() ==
                  "Test Request Form".toLowerCase()
                ) {
                  data.addFieldsArr.forEach((Fields, ind) => {
                    if (Fields.columnName == "E_TRF_Applicant_Address") {
                      Fields.defaultValue = that.dataService.EcommETRFData.report_Document.trim();
                      Fields.Value = that.dataService.EcommETRFData.report_Document.trim();
                    } else if (
                      Fields.columnName == "E_TRF_CountryofDestination"
                    ) {
                      Fields.defaultValue = that.dataService.EcommETRFData.countryofdestination.trim();
                      Fields.Value = that.dataService.EcommETRFData.countryofdestination.trim();
                    } else if (Fields.columnName == "CP_Age_Grade") {
                      Fields.defaultValue = that.dataService.EcommETRFData.agegrade.trim();
                      Fields.Value = that.dataService.EcommETRFData.agegrade.trim();
                    }
                  });
                }
                if (
                  data.dependentOnUniqueID.toLowerCase() == "pdf" &&
                  data.SectionName.toLowerCase() ==
                  "Packages/Individual Test Items For Testing".toLowerCase()
                ) {
                  data.addFieldsArr.forEach((Fields, ind) => {
                    if (Fields.selectTable == "T_E_TRF_Label") {
                      LabelID = "container-" + Fields.uniqueId;
                      that.dataService.EcommPackageUniqueId = LabelID;
                    }
                  });
                }
                if (
                  data.dependentOnUniqueID.toLowerCase() == "pdf" &&
                  data.SectionName.toLowerCase() ==
                  "Address Information".toLowerCase()
                ) {
                  data.addFieldsArr.forEach((Fields, ind) => {
                    if (Fields.selectTable == "T_E_TRF_Label") {
                      LabelID = "container-" + Fields.uniqueId;
                      that.dataService.EcommApplicatntAndLabInfo = LabelID;
                    }
                  });
                }
              });
            }
            that.Json = r.Data;
            that.ETRFsection = r.Data.ETRFsection;
            that.IsAdditionalNotesSection = r.Data.IsAdditionalNotesSection;
            that.AdditionalNote = r.Data.AdditionalNote;
            that.setAdditionalNote();
            that.formName = r.Data.FormName;
            that.isLoader = false;
            ETRF_Model.Json = r.Data;
            ETRF_Model.FormID = r.Data.FormId;
            ETRF_Model.ClientID = r.Data.Clientid;
            that.breedcrumbName = "Add New Test Request";
            ETRF_Model.E_TRFID = "";
            that.ETRFType = r.Data.ETRFType;
            // (<any>$('#' + LabelID)).find('.field-label').html('<h2
            // class="city">Tokyo</h2><p>Tokyo is the capital of Japan.</p>');
          });
      } else if (browserURL.includes("online-etrf/add-etrf")) {


        this.activatedRoute.params.subscribe(params => {
          let SFEDIDataId = params["sfEDIId"];
          var ReportNo = params["reportNo"];
          let SFBusinessLine = params["sfBusinessLine"];
          this.testRequestType = params["trType"];          
          this.dataService.imageSizeForEcommEtrf = false;
          var url = window.location.href.split("/");
          // var ReportNo = url[url.length - 3];
          // let SFEDIDataId = url[url.length - 2];
          this.dataService.SF_EDI_DATA_ID = SFEDIDataId;
          //let SFBusinessLine = url[url.length - 1];

          var that = this;
          that.isLoader = true;
          let serviceUrl = "";
          let sfEdiDataUrl = "";
          if (
            (that.userCredentials.CompanyId == 386 && that.userDetails.SF_TRF_AccessType == true)
          ) {
            serviceUrl =
              "DynamicFormCreation/GetConfiguration?FormID=" +
              ReportNo +
              "&ClientCompanyid=" +
              that.companyId +
              "&UserId=" +
              this.userDetails.UserId +
              "&SFEDIDataId=" +
              SFEDIDataId +
              "&BusinessLine=" +
              SFBusinessLine;

            sfEdiDataUrl =
              "DynamicFormCreation/GetSFEdiData?ediDataId=" +
              SFEDIDataId;


          } else {

            //let formId = url[url.length - 1];
            let formId = params["reportNo"];
            serviceUrl =
              "DynamicFormCreation/GetConfiguration?FormID=" +
              formId +
              "&ClientCompanyid=" +
              that.companyId +
              "&UserId=" +
              this.userDetails.UserId;
          }

          // that.dataService.get(sfEdiDataUrl).subscribe(r => {
          //   this.ediDataList = r.Data;
          // });


          that.dataService.get(serviceUrl).subscribe(r => {
            that.Json = r.Data;
            that.ETRFsection = r.Data.ETRFsection;
            if (
              that.userCredentials.CompanyId == 386 &&
              that.userDetails.SF_TRF_AccessType == true
            ) {
              that.ETRFsection.forEach((data, index) => {
                data.addFieldsArr.forEach((Fields, ind) => {
                  if (Fields.columnName == "CP_B1") {
                    if (Fields.Value) {
                      that.ediDataList = JSON.parse(Fields.Value);
                    }
                  }
                })
              });
            }

            that.IsAdditionalNotesSection = r.Data.IsAdditionalNotesSection;
            that.AdditionalNote = r.Data.AdditionalNote;
            that.setAdditionalNote();
            // (<any>$("#additionalNote")).append('<p>'+ that.AdditionalNote +
            // '</p>');
            that.formName = r.Data.FormName;
            that.isLoader = false;
            ETRF_Model.Json = r.Data;
            ETRF_Model.FormID = r.Data.FormId;
            ETRF_Model.ClientID = r.Data.Clientid;
            that.breedcrumbName = r.Data.BussinessId == 4 ? 'Add New Booking Request' : 'Add New Test Request';
            ETRF_Model.E_TRFID = '';
            that.ETRFType = r.ETRFType;
            that.dataService.currentFormId = r.Data.FormId;
            that.BusinessId = r.Data.BussinessId;
          })

          //   that.ETRFsection = r.Data.ETRFsection;
          //   that.IsAdditionalNotesSection = r.Data.IsAdditionalNotesSection;
          //   that.AdditionalNote = r.Data.AdditionalNote;
          //   that.setAdditionalNote();
          //   // (<any>$("#additionalNote")).append('<p>'+ that.AdditionalNote +
          //   // '</p>');
          //   that.formName = r.Data.FormName;
          //   that.isLoader = false;
          //   ETRF_Model.Json = r.Data;
          //   ETRF_Model.FormID = r.Data.FormId;
          //   ETRF_Model.ClientID = r.Data.Clientid;
          //   that.breedcrumbName = "Add New Test Request";
          //   ETRF_Model.E_TRFID = "";
          //   that.ETRFType = r.ETRFType;
          //   that.dataService.currentFormId = r.Data.FormId;
          // });
        });
      } else {

        this.activatedRoute.params.subscribe(params => {
          this.dataService.imageSizeForEcommEtrf = false;
          this.isEditCase = true;
          var url = window.location.href.split("/");
          // Here report number is ETRF Id
          // var ReportNo = url[url.length - 1];
          var ReportNo = params["reportNo"];
          this.ETRFID = ReportNo;
          var that = this;
          that.isLoader = true;
          that.dataService
            .get("DynamicFormCreation/GetSaveETRF?E_TRFID=" + ReportNo)
            .subscribe(r => {
              that.Json = JSON.parse(r.Data[0].Json);
              that.ETRFsection = that.Json.ETRFsection;
              if (
                that.userCredentials.CompanyId == 386 &&
                that.userDetails.SF_TRF_AccessType == true
              ) {
                that.ETRFsection.forEach((data, index) => {
                  data.addFieldsArr.forEach((Fields, ind) => {
                    if (Fields.columnName == "CP_B1") {
                      if (Fields.Value) {
                        that.ediDataList = JSON.parse(Fields.Value);
                      }
                    }
                  })
                });
              }
              that.formName = that.Json.FormName;
              that.IsAdditionalNotesSection = r.Data.IsAdditionalNotesSection;
              that.AdditionalNote = r.Data.AdditionalNote;
              that.Ref_ETRF_ID = r.Data[0].Ref_ETRF_ID;
              that.setAdditionalNote();
              // (<any>$("#additionalNote")).append('<p>'+ that.AdditionalNote +
              // '</p>');
              that.isLoader = false;
              ETRF_Model.Json = that.Json;
              ETRF_Model.FormID = that.Json.FormId;
              ETRF_Model.ClientID = that.Json.Clientid;
              ETRF_Model.E_TRFID = ReportNo;
              that.vendorId = r.Data[0].VendorId;

              that.breedcrumbName = that.Json.BussinessId == 4 ? 'Edit Booking Form' : 'EDIT E-TRF';
              // that.breedcrumbName = 'EDIT E-TRF';

              // that.breedcrumbName = "EDIT E-TRF";
              that.dataService.currentFormId = that.Json.FormId;
              if (that.Json.BussinessId == 4) {
                that.ETRFID = that.ETRFID + r.Data[0].Revision;
              }
              that.BusinessId = that.Json.BussinessId;
            });
        });
      }
    } else {
      this.router.navigate(["/login"]);
    }


  }

  bindDataFromShowFields(obj) {
    var that = this;
    var value;
    var ShowFieldId;
    this.ETRFsection.forEach(data => {
      data.addFieldsArr.forEach(SectionData => {
        // ShowFieldId = SectionData.uniqueId;
        that.dataService.DynamicDropValue.forEach(DropValuedata => {
          // if(DropValuedata.ids)
          var value = DropValuedata;
          if (
            DropValuedata[SectionData.uniqueId] ||
            DropValuedata[SectionData.uniqueId] == ""
          ) {
            SectionData.Value = DropValuedata[SectionData.uniqueId];
          }
        });
        // SectionData.Value = value;
      });
    });

    this.dataService.DynamicDropValue.forEach(data => { });
    // alert('called');
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
  setAdditionalNote() {
    if (this.AdditionalNote) {
      (<any>$("#additionalNote")).css("display", "block");
      (<any>$("#additionalNote .sectionContainer-etrf")).append(
        "<p>" + this.AdditionalNote + "</p>"
      );
    } else {
      (<any>$("#additionalNote")).css("display", "none");
    }
  }
  getClass(sectionObj: any) {
    if (sectionObj.dependentOnUniqueID) {
      if (
        (<any>$("#" + sectionObj.dependentOnUniqueID)).prop("checked") == true
      ) {
        return "";
      } else {
        return "hidden-section " + sectionObj.dependentOnUniqueID;
      }
    }
  }
  checkValidation(data: any) {
    var that = this;
    var count = 0;
    var countValidation = 0;

    (<any>$(".required")).each(function () {
      count++;
      var name = "";
      var x = (<any>$(this)).prop("tagName");
      if (x.toLowerCase() == "input") {
        if ((<any>$(this)).is(":radio")) {
          name = "radio";
        } else if ((<any>$(this)).is(":checkbox")) {
          name = "checkbox";
        }
      }
      if (x.toLowerCase() == "owl-date-time") {
        // var y = (<any>$(this)).find('.owl-dateTime-input').val();
        // if (!y) {
        //     countValidation++;
        //     that.isLoader = false;
        //     (<any>$(this)).addClass('no-val');
        //     this.ready = false;
        //     that.dataService.ready = false;
        //     // swal('', 'Please Fill all required fields');
        //     // return false;
        // }
        // else {
        //     (<any>$("input[ng-reflect-name=" + id +
        //     "]")).removeClass('no-val');
        // }

        var y = (<any>$(this)).find(".owl-dateTime-input").val();
        if (!y) {
          countValidation++;
          that.isLoader = false;
          (<any>$(this)).addClass("no-val");
          this.ready = false;
          that.dataService.ready = false;
          (<any>$(this))
            .find(".owl-dateTime-inputWrapper")
            .css("border", "2px solid #a71212");
          (<any>$(this))
            .find(".owl-dateTime-inputWrapper")
            .css("border-radius", "5px");
        } else {
          (<any>$(this))
            .find(".owl-dateTime-inputWrapper")
            .css("border", "0px solid #a71212");
          (<any>$(this))
            .find(".owl-dateTime-inputWrapper")
            .css("border-radius", "0px");
        }
      } else if (name == "radio") {
        var id = (<any>$(this)).attr("id").split("-")[0];
        var countNew = (<any>$("input[data-loc=" + id + "]")).length;
        var yy = false;
        for (var i = 0; i < countNew; i++) {
          if (
            (<any>$("input[id=" + id + "-" + i + "]")).prop("checked") == true
          ) {
            yy = true;
          }
        }
        // var y = (<any>$("input[ng-reflect-name=" + id +
        // "]")).prop('checked');//(<any>$("input[ng-reflect-name=" + id +
        // "]:checked")).val();
        if (!yy) {
          countValidation++;
          that.isLoader = false;
          // (<any>$(this)).addClass('no-val');
          (<any>$("input[data-loc=" + id + "]")).addClass("no-val");
          this.ready = false;
          that.dataService.ready = false;
          // swal('', 'Please Fill all required fields');
          // return false;
        } else {
          (<any>$("input[data-loc=" + id + "]")).removeClass("no-val");
        }
      } else if (name == "checkbox") {
        var id = (<any>$(this)).attr("id").split("-")[0];
        var y = (<any>$("input[name=" + id + "]:checked")).length;
        if (!y) {
          countValidation++;
          that.isLoader = false;
          (<any>$("input[name=" + id + "]")).addClass("no-val");
          // (<any>$(this)).addClass('no-val');
          this.ready = false;
          that.dataService.ready = false;
          // swal('', 'Please Fill all required fields');
          // return false;
        } else {
          (<any>$("input[name=" + id + "]")).removeClass("no-val");
        }
      } else {
        // if (!(<any>$(this)).val()) {
        //     countValidation++;
        //     that.isLoader = false;
        //     (<any>$(this)).addClass('no-val');
        //     this.ready = false;
        //     that.dataService.ready = false;
        //     // swal('', 'Please Fill all required fields');
        //     // return false;
        // }
        // else {
        //     (<any>$(this)).removeClass('no-val');
        // }
        if (!(<any>$(this)).val() && !(<any>$(this)).attr("readonly")) {
          (<any>$(this)).css("border", "2px solid #a71212");
          this.ready = false;
          that.dataService.ready = false;
          countValidation++;
          that.isLoader = false;
        } else {
          (<any>$(this)).css("border", "1px solid #ccc");
        }
      }
      if (countValidation > 0 && count == that.requireCount) {
        swal("", "Please Fill all required fields");
        return false;
      }
      if (count == that.requireCount) {
        that.ready = true;
        that.save(data);
      }
    });
  }

  prepareForPdf() {
    var that = this;
    (<any>$("html,body")).animate(
      { scrollTop: (<any>$("header")).offset().top },
      "slow"
    );
    // (<any>$("body")).bind("mousewheel", function () {
    //     return false;
    //         // });

    that.getUploadedFileName();

    (<any>$(".UploadFileInsp")).each(function () {
      var data = (<any>$(this)).attr("ng-reflect-tooltip-text");
      that.UploadFileInspection.push(data);
    });

    var thatnew = this;
    (<any>$("#Form-Header")).css("border", "2px solid #000");
    (<any>$(".mts-logo")).css("border-right", "2px solid #000");
    (<any>$(".section-etrf")).css("border", "2px solid #000");
    (<any>$(".form-control")).css({ border: "2px solid #444", color: "#000" });
    (<any>$(".field-label")).css({ "font-size": "13px", color: "#000" });
    (<any>$(".sub-sectionHeader-etrf")).css({
      "font-size": "16px",
      "border-bottom": "2px solid #000",
      color: "#000"
    });
    (<any>$(".section-header-etrf")).css({
      "font-size": "16px",
      "border-bottom": "2px solid #000",
      color: "#000"
    });
    (<any>$(".radio-container input")).css("border", "2px solid #000");
    (<any>$(".radio-container span")).css({
      "font-size": "16px",
      color: "#000"
    });
    (<any>$(".checkbox-label")).addClass("onPdf");
    (<any>$(".checkbox-label")).css({ "font-size": "16px", color: "#000" });
    (<any>$(".hide-pdf")).css("display", "none");
    (<any>$(".readonly")).css("display", "block");
    (<any>$(".pdf")).css("display", "block");
    var min = 0;
    var i = 1;
    var h2 = 0;
    var thatPDF;
    var h1 = (<any>$("#form-wrapper")).offset().top;
    if (this.statusEcomm == "true") {
      (<any>$("#form-wrapper")).css("max-width", 1170 + "px");
      this.TextAreaResize().then(() => {
        (<any>$(".section-etrf")).each(function () {
          var that = this;
          var h = Math.ceil((<any>$(that)).offset().top);
          // var id = that.id;
          // var DivHeight = (<any>$('#' + id)).height();
          if (Math.abs(h) <= 2010 * i && h != 0) {
            h2 = h;
            thatPDF = that;
          }
          if (Math.abs(h) >= 2010 * i) {
            // var h2 = thatnew.findHeight(that);
            // var h2 =
            // Math.ceil((<any>$(that)).closest('.section-etrf').prev().offset().top);
            var mHeight = 2015 * i - Math.abs(h2);
            (<any>$(thatPDF))
              .closest(".section-etrf")
              .css("margin-top", Math.abs(mHeight + 300 - min) + "px");
            i++;
            min += 5;
          }
        });
      });
    } else {
      (<any>$("#form-wrapper")).css("max-width", 1240 + "px");
      this.TextAreaResize().then(() => {
        (<any>$(".section-etrf")).each(function () {
          var that = this;
          var h = Math.ceil((<any>$(that)).offset().top);
          if (Math.abs(h) <= 1844 * i && h != 0) {
            h2 = h;
            thatPDF = that;
          }
          if (Math.abs(h) >= 1844 * i) {
            // var h2 = thatnew.findHeight(that);
            // var h2 =
            // Math.ceil((<any>$(that)).closest('.section-etrf').prev().offset().top);
            var mHeight = 1847 * i - Math.abs(h2);
            (<any>$(thatPDF))
              .closest(".section-etrf")
              .css("margin-top", Math.abs(mHeight + 150 + 50 - min) + "px");
            i++;
            min += 10;
          }
        });
      });
      // this.TextAreaResize().then(() => {
      //     (<any>$('.section-etrf')).each(function () {
      //         var that = this;
      //         var h = Math.ceil((<any>$(that)).offset().top);
      //         var id = that.id;
      //         var DivHeight = (<any>$('#' + id)).height();
      //         if (Math.abs(h) <= (1844 * i) && h != 0) {
      //             h2 = h;
      //             thatPDF = that;
      //         }
      //         if (Math.abs(h) >= (1844 * i)) {
      //             //var h2 = thatnew.findHeight(that);
      //             //var h2 =
      //             Math.ceil((<any>$(that)).closest('.section-etrf').prev().offset().top);
      //             var mHeight =
      //             (<any>$(thatPDF)).closest('.section-etrf').height();//(1847
      //             * i) - Math.abs(h2);
      //             (<any>$(thatPDF)).closest('.section-etrf').css('margin-top',
      //             Math.abs(mHeight + 100 + 50 - min) + 'px'); i++; min += 5;
      //         }
      //     });
      // })
    }
  }

  saveETRF(calledFrom = "etrf") {
    this.calledFrom = calledFrom;
    // Splite pdf as per section
    this.isLoader = true;
    var that = this;
    //that.ETRFsection = r.Data.ETRFsection;
    if (
      that.userCredentials.CompanyId == 386 &&
      that.userDetails.SF_TRF_AccessType == true
    ) {
      that.ETRFsection.forEach((data1, index) => {
        data1.addFieldsArr.forEach((Fields, ind) => {
          if (Fields.columnName == "CP_B1") {
            Fields.Value = JSON.stringify(that.ediDataList);
            //that.ediDataList=JSON.parse(Fields.Value);
          }
        })
      });
    }

    var data = this.dataService.clone(ETRF_Model);
    this.isSendEmail = data.Json.IsSendEmail;
    if (typeof data.Json != "string") {
      data.Json = JSON.stringify(data.Json);
    }

    this.requireCount = (<any>$(".required")).length;
    var count = 0;
    if (this.requireCount > 0) {
      // Code Commented by rohit to Make an common Funtion for Validation

      (<any>$(".required")).each(function () {
        count++;
        var x = (<any>$(this)).prop("tagName");
        if (x.toLowerCase() == "owl-date-time") {
          var y = (<any>$(this)).find(".owl-dateTime-input").val();
          if (!y) {
            (<any>$(this))
              .find(".owl-dateTime-inputWrapper")
              .css("border", "2px solid #a71212");
            (<any>$(this))
              .find(".owl-dateTime-inputWrapper")
              .css("border-radius", "5px");
          } else {
            (<any>$(this))
              .find(".owl-dateTime-inputWrapper")
              .css("border", "0px solid #a71212");
            (<any>$(this))
              .find(".owl-dateTime-inputWrapper")
              .css("border-radius", "0px");
          }
        } else if (!(<any>$(this)).val() && !(<any>$(this)).attr("readonly")) {
          (<any>$(this)).css("border", "2px solid #a71212");
        } else {
          (<any>$(this)).css("border", "1px solid #ccc");
        }
        if (count == that.requireCount) {
          that.checkValidation(data);
        }
      });

      // End

      // that.checkValidation(data);
    } else {
      this.ready = true;
      this.save(data);
    }
  }

  removeAllCss() {
    (<any>$("#Form-Header")).css("border", "1px solid #ccc");
    (<any>$(".mts-logo")).css("border-right", "1px solid #ccc");
    (<any>$(".section-etrf")).css("border", "1px solid #ccc");
    (<any>$(".form-control")).css({ border: "1px solid #ccc", color: "#555" });
    (<any>$(".field-label")).css({ "font-size": "16px", color: "#555" });
    (<any>$(".sub-sectionHeader-etrf")).css({
      "font-size": "14px",
      "border-bottom": "1px solid #ccc",
      color: "#555"
    });
    (<any>$(".section-header-etrf")).css({
      "font-size": "14px",
      "border-bottom": "1px solid #ccc",
      color: "#555"
    });
    (<any>$(".radio-container input")).css("border", "1px solid #ccc");
    (<any>$(".radio-container span")).css({
      "font-size": "14px",
      color: "#555"
    });
    (<any>$(".checkbox-label")).addClass("onPdf");
    (<any>$(".checkbox-label")).css({ "font-size": "14px", color: "#555" });
    (<any>$(".hide-pdf")).css("display", "block");
    (<any>$(".readonly")).css("display", "none");
    (<any>$(".pdf")).css("display", "none");
    (<any>$("body")).unbind("mousewheel");
    (<any>$("#form-wrapper")).css("max-width", "100%");
    (<any>$(".section-etrf")).css("margin-top", "");
  }

  TextAreaResize() {
    return new Promise((resolve, reject) => {
      (<any>$("textarea")).each(function () {
        var that = this;
        (<any>$("#" + that.id)).height(
          (<any>$("#" + that.id)).prop("scrollHeight")
        );
      });
      resolve();
    });
  }
  save(data: any) {
    this.prepareForPdf();
    var EcommerceIdnew = localStorage.getItem("ecommerceEtrfId");
    var that = this;
    if (this.isEditCase) {
      data.VendorId = this.vendorId;
    } else {
      if (this.userCredentials.UserCompanyId != 0) {
        data.VendorId = this.userCredentials.UserCompanyId;
      } else {
        data.VendorId = this.userCredentials.CompanyId;
      }
    }
    var ecommSaveService;
    if (that.calledFrom != "ecommerce") {
      ecommSaveService = "DynamicFormCreation/SaveTRF";
    } else {
      if (EcommerceIdnew != "") {
        data.E_TRFID = EcommerceIdnew;
      } else {
        data.E_TRFID = "";
      }
      ecommSaveService = "DynamicFormCreation/TempSaveTRF";
    }
    if (this.ready) {
      //debugger;
      // that.isLoader = true;
      that.dataService.post(ecommSaveService, data).subscribe(r => {
        that.isLoader = false;
        if (r.IsSuccess) {
          var id = r.Message.split(" ")[1];
          that.etrfIdOnSave = id;
          that.ETRFID = id;
          that.getImages();
          // that.removeAllCss();
          //debugger;
          // this.userDetails = JSON.parse(localStorage.getItem("userDetail"));
          let userCredential = JSON.parse(
            localStorage.getItem("mts_user_cred")
          );
          if (
            userCredential.CompanyId == 386 &&
            this.userDetails.SF_TRF_AccessType == true
          ) {
            var currentUrl = window.location.href;
            if (currentUrl.includes("online-etrf/add-etrf")) {
              that.updateSFEDIStatus();
            }
          }
        }
      });
    } else {
      that.isLoader = false;
    }
  }

  updateSFEDIStatus() {
    
    let that = this;
    this.dataService
      .get(
        "DynamicFormCreation/UpdateSFEDIDataStatus?ediDataId=" +
        that.dataService.SF_EDI_DATA_ID + "&etrfNumber=" + that.ETRFID+"&trType="+this.testRequestType
      )
      .subscribe(r => {
        that.isLoader = false;
        if (r.IsSuccess) {
        }
      });
  }
  getPDF(action: any, etrfID: any) {
    var that = this;
    that.isLoader = true;
    var name = this.ETRFID + ".pdf";
    (<any>$("html")).css("opacity", "0.1");
    (<any>$("#ecommStylesheet")).attr("disabled", "disabled");
    // setTimeout(function () {
    //     html2canvas(document.getElementById('form-wrapper'),{
    //     logging: true,
    //     profile: true,
    //     useCORS: true}).then(function (canvas) {
    //         var img = canvas.toDataURL("image/png", 0.9);
    //         // (<any>$('#result')).css('display', 'block');
    //         // (<any>$('#result')).css('position', 'relative');
    //         // (<any>$('#result')).css('top', '200px');
    //         // (<any>$('#result')).attr('src', img);
    //         setTimeout(function () {
    //             that.pdfAction(action, etrfID, img);
    //         }, 100)
    //     }
    //     )
    // }, 10000)
    html2canvas(document.getElementById("form-wrapper"), {
      logging: true,
      profile: true,
      useCORS: true
    }).then(function (canvas) {
      var img = canvas.toDataURL("image/png", 0.9);
      (<any>$("#ecommStylesheet")).removeAttr("disabled");
      setTimeout(function () {
        that.pdfAction(action, etrfID, img);
      }, 1000);
    });
  }

  pdfAction(action: any, etrfID: any, img: any) {
    var that = this;
    this.GenratedETRFNo = etrfID;
    (<any>$("html")).css("opacity", "1");
    (<any>$("#form-wrapper")).css("max-width", "100%");
    if (action == "upload") {
      var model = {
        E_TRFID: etrfID,
        PDFString: img,
        CreatedBy: ETRF_Model.CreatedBy,
        FormID: 0
      };
      that.dataService
        .post("DynamicFormCreation/SavedPDFList", model)
        .subscribe(r => {
          if (r.IsSuccess) {
            that.isLoader = false;
            that.removeAllCss();
            if (that.isSendEmail == true) {
              (<any>$("body")).unbind("mousewheel");
              // (<any>$('#SendEmail')).modal('show');
              that.SendEmail();
              // that.router.navigate(['/eTrf']);
              (<any>$("body")).unbind("mousewheel");
              if (that.calledFrom != "ecommerce") {
                that.router.navigate(["/online-etrf"]);
              } else {
                localStorage.setItem("ecommerceEtrfId", etrfID);
              }
            } else {
              (<any>$("body")).unbind("mousewheel");
              if (that.calledFrom != "ecommerce") {
                that.router.navigate(["/online-etrf"]);
              } else {
                localStorage.setItem("ecommerceEtrfId", etrfID);
              }
            }
          }
        });
    }

    // when call from popup
    else if (action == "modal") {
      var model = {
        E_TRFID: that.ETRFID,
        PDFString: img,
        CreatedBy: ETRF_Model.CreatedBy,
        FormID: 0
      };
      that.dataService
        .post("DynamicFormCreation/SavedPDFList", model)
        .subscribe(r => {
          if (r.IsSuccess) {
            that.isLoader = false;
            if (that.isSendEmail == true) {
              window.location.href =
                BaseUrl +
                "ManageDocuments/DownloadFile?FileName=ETRFPDF\\" +
                r.Data.trim() +
                "&fileType=PDF&businessCategoryId=" +
                that.userCredentials.BusinessCategoryId;
              (<any>$("body")).unbind("mousewheel");
              that.removeAllCss();
              // (<any>$('#SendEmail')).modal('show');
              this.SendEmail();
              that.router.navigate(["/online-etrf"]);
            } else {
              window.location.href =
                BaseUrl +
                "ManageDocuments/DownloadFile?FileName=ETRFPDF\\" +
                r.Data.trim() +
                "&fileType=PDF&businessCategoryId=" +
                that.userCredentials.BusinessCategoryId;
              // True Scroll
              (<any>$("body")).unbind("mousewheel");
              that.removeAllCss();
              that.router.navigate(["/online-etrf"]);
            }
          } else {
            that.isLoader = false;
          }
        });
    }
  }

  getImages() {
    this.isLoader = true;
    var that = this;
    var getBase64Modal = [];
    var count = 1;
    var obj = ImagesPDF;
    (<any>$(".file-img")).each(function () {
      var x = Object.assign({}, obj);
      if ((<any>$(this)).attr("src") != "") {
        var src = (<any>$(this)).attr("src");
        var fileName = src.split("/");
        fileName = fileName[fileName.length - 1];
        x.Sequence = count;
        x.ImageUrl = src;
        x.ImageName = fileName;
        (<any>$(this)).addClass(count.toString());
      } else {
        x.Sequence = count;
        x.ImageUrl = "";
        x.ImageName = "";
        (<any>$(this)).addClass(count.toString());
      }
      getBase64Modal.push(x);
      count++;
    });

    this.dataService
      .post("DynamicFormCreation/Base64Img", getBase64Modal)
      .subscribe(r => {
        if (r.IsSuccess) {
          that.ImgBaseArr = r.Data;
          for (let i = 0; i < r.Data.length; i++) {
            if (r.Data[i].ImgBase64.trim() != "") {
              (<any>$(".file-img." + r.Data[i].Sequence.toString())).attr(
                "src",
                "data:image/png;base64," + r.Data[i].ImgBase64
              );
            } else {
              (<any>$(".file-img." + r.Data[i].Sequence.toString())).remove();
            }
          }
          that.isLoader = false;
          if (that.calledFrom != "ecommerce") {
            (<any>$("#myModal")).modal("show");
          } else {
            that.isLoader = true;
            setTimeout(function () {
              that.getPDF("upload", that.ETRFID);
            }, 500);
          }
        } else {
          if (that.calledFrom != "ecommerce") {
            (<any>$("#myModal")).modal("show");
          } else {
            that.isLoader = true;
            that.getPDF("upload", that.ETRFID);
          }
        }
      });
  }

  getPDFModal() {
    (<any>$("#myModal")).modal("hide");
    this.getPDF("modal", null);
  }

  proceed() {
    (<any>$("#myModal")).modal("hide");
    this.getPDF("upload", this.ETRFID);
  }
  SendEmail() {
    var that = this;
    // (<any>$('#SendEmail')).modal('hide');
    // if (this.calledFrom != "ecommerce") {
    //     this.router.navigate(['/eTrf']);
    // }
    var MailInsp = {};
    MailInsp["ETRFID"] = that.ETRFID;
    MailInsp["EmailID"] = this.userDetails.EmailId;
    MailInsp["CreatedBy"] = this.userCredentials.UserId;
    MailInsp["attachedFiles"] = this.UploadFileInspection;
    if (that.calledFrom == "ecommerce") {
      this.dataService.ecommInspectionSendEmail = MailInsp;
    } else {
      that.dataService
        .post("DynamicFormCreation/SendEmailFci", MailInsp)
        .subscribe(r => {
          // that.dataService.post('DynamicFormCreation/SavedPDFList',
          // model).subscribe(
        });
    }
  }
  // SendEmailcancel() {
  //     (<any>$('#SendEmail')).modal('hide');
  //     if (this.calledFrom != "ecommerce") {
  //         this.router.navigate(['/eTrf']);
  //     }
  // }


  getUploadedFileName() {
    let that = this;
    that.UploadFileInspection = [];
    let controlId = (<any>$('.UploadFileInsp')).attr('id');
    if (controlId) {
      controlId = controlId.split('-')[0];
      let jsonObject = JSON.parse(JSON.stringify(ETRF_Model.Json));
      jsonObject.ETRFsection.forEach(section => {
        if (section.addTableArr.length > 0) {
          section.addTableArr.forEach(table => {
            if (table.addFieldsArr.length > 0) {
              table.addFieldsArr.forEach((field, index) => {
                if (field.uniqueId.includes(controlId) && field.Value) {
                  let fileIndex = index;
                  that.UploadFileInspection.push(field.Value);
                  if (table.addDataArr && table.addDataArr.length > 0) {
                    table.addDataArr.forEach(element => {
                      that.UploadFileInspection.push(element[fileIndex]);
                    });
                  }
                }
              });
            }
          });
        }
      });
    }
  }

  updateData() {
    let dd = this.dataService.fieldValue;
    let index = this.ediDataList.findIndex(i => i.SF_EDI_DATA_ID == this.dataService.ediDataFieldId);
    let type = this.dataService.fieldType;
    this.ediDataList[index][type.toString()] = dd;
    (<any>$('#UpdateData')).modal('hide');
  }
}


