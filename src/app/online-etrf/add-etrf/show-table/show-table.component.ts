import { Component, OnInit, Input } from '@angular/core';
import { NgStyle } from '@angular/common';

@Component({
  selector: 'app-show-table',
  templateUrl: './show-table.component.html',
  styleUrls: ['./show-table.component.css']
})
export class ShowTableComponent implements OnInit {

  @Input() tableArrObj;
  @Input() sectionObj;
  @Input() tableIndex;

  tableId: any;
  fieldsArr: any = [];
  newAddDataArr: any = [];
  addDataArr: any = [];

  ngOnInit() {

    if (this.tableArrObj.addDataArr) {

    }
    else {
      this.tableArrObj.addDataArr = [];

    }

    this.tableId = this.tableArrObj.uniqueId;
    this.fieldsArr = this.tableArrObj.addFieldsArr;
  }

  addToTable(fieldsArr: any) {
    var temp = [];
    var index = this.tableArrObj.addDataArr.length + 1;
    fieldsArr.forEach((data) => {
      // It is used to set the id of each field in row
      if (data.uniqueId.includes('-')) {
        let id = data.uniqueId.split('-');
        data.uniqueId = id[0] + "-" + index;
        (<any>$('#' + id[0])).val('');
        (<any>$('#' + id[0] + '-container img')).attr('src', '');
        (<any>$('#' + id[0] + '-container')).css('display', 'none');
      }
      else {
        (<any>$('#' + data.uniqueId)).val('');
        (<any>$('#' + data.uniqueId + '-container img')).attr('src', '');
        (<any>$('#' + data.uniqueId + '-containerFile i')).css('display', 'none');
        (<any>$('#' + data.uniqueId + '-container')).css('display', 'none');
        data.uniqueId = data.uniqueId + "-" + index;
      }

      var x = Object.assign({}, data);
      if (x.Value == "") {
        x.Value = " ";
      }
      temp.push(x.Value);
      data.Value = " ";
    })
    if (temp[0].trim() != "") {
      this.tableArrObj.addDataArr.push(temp);
    }
    //this.tableArrObj.addDataArr.push(temp);
  }

  removeToTable(index: any) {
    this.tableArrObj.addDataArr.splice(index, 1);
  }
}
