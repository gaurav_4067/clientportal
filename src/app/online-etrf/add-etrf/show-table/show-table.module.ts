import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowTableComponent } from './show-table.component';
import { ShowFieldModule } from 'app/online-etrf/add-etrf/show-field/show-field.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ShowFieldModule,
    FormsModule
  ],
  declarations: [ShowTableComponent],
  exports:[ShowTableComponent]
})
export class ShowTableModule { }
