import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {FooterModule} from 'app/footer/footer.module';
import {HeaderModule} from 'app/header/header.module';
import {ShowFieldModule} from 'app/online-etrf/add-etrf/show-field/show-field.module';
import {ShowSubsectionModule} from 'app/online-etrf/add-etrf/show-subsection/show-subsection.module';
import {ShowTableModule} from 'app/online-etrf/add-etrf/show-table/show-table.module';

import {AddEtrfRoutingModule} from './add-etrf-routing.module';
import {AddEtrfComponent} from './add-etrf.component';

@NgModule({
  imports: [
    CommonModule,
    // AddEtrfRoutingModule,
    HeaderModule, FooterModule, ShowFieldModule, ShowSubsectionModule,
    ShowTableModule, FormsModule
  ],
  declarations: [AddEtrfComponent],
  exports: [AddEtrfComponent]
})
export class AddEtrfModule {
}
