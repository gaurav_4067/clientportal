import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ChangeDetectorRef
} from "@angular/core";
import { Router } from "@angular/router";
import {
  IMultiSelectOption,
  IMultiSelectTexts
} from "angular-2-dropdown-multiselect/src/multiselect-dropdown";
import { Ng2AutoCompleteModule } from "ng2-auto-complete";

import { DataService, ETRFUrl } from "../../../mts.service";
import { forEach } from "@angular/router/src/utils/collection";
import { SF_EDI_Data } from "app/online-etrf/online-etrf.model";
 
declare let swal: any;

@Component({
  selector: "app-show-field",
  templateUrl: "./show-field.component.html",
  styleUrls: ["./show-field.component.css"]
})
export class ShowFieldComponent implements OnInit, AfterViewInit {
  @Input()
  ediDataList;
  @Input()
  ediDataList1;
  @Input()
  fieldObj;
  @Input()
  sectionObj;
  @Input()
  modelVal;
  @Input()
  calledFrom;
  @Input()
  index;
  @Input()
  innerIndex;
  @Input()
  tableIndex;
  @Input()
  tableObj;
  @Output()
  bindDataFromShowFields = new EventEmitter();
  //ediDataList1:Array<SF_EDI_Data>;
  fieldLabel: any = "";
  validationArr: any = [];
  isLoader: boolean;
  width: any;
  fieldId: any;
  fieldType: any;
  isRequired: any = false;
  inputType: any = "text";
  Required: any = "";
  ReadOnly: any = "";
  optionList: any = [];
  checkboxWithText: any = false;
  checkboxListOther: any = false;
  checkboxOther: any = false;
  radioOther: any = false;
  fileNames: any;
  fileToUploads: any;
  minRange: any = "";
  maxRange: any = "";
  dependent: any;
  dependentOn: any;
  Classattr: any;
  isChecked: any = false;
  private defaultText: any = { defaultTitle: "--Select Option--" };
  imagePath: any = ETRFUrl + "/Files/TempTRFImage";
  radioDependentIds: any = [];
  multiSelectModel: any = [];
  imageURL: any = "";
  tooltip: any = "";
  en: any;
  DynamicDropValue: any = [];
  DynamicDropDependentValue: any = [];
  container: any = "container";
  containerFile: any = "containerFile";
  fieldhref: any = "";
  fieldanchorLabel: any = "";
  checkboxWithTextArea: boolean = false;
  userDetails: any;
  userCredentials: any;
  constructor(
    private dataService: DataService,
    private router: Router,
    private cdf: ChangeDetectorRef
  ) {
    //this.ediDataList1=new Array<SF_EDI_Data>();
  }

  ngOnInit() {
    
    this.userDetails = JSON.parse(localStorage.getItem("userDetail"));
    this.userCredentials = JSON.parse(localStorage.getItem("mts_user_cred"));
    this.en = {
      firstDayOfWeek: 0,
      dayNames: [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
      ],
      dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      monthNames: [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
      ],
      monthNamesShort: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
      ]
    };

    this.dataService.ediDataList1=this.ediDataList1;
    // Get field details from FieldObj
    this.fieldLabel = this.fieldObj.enterLabelName;
    this.validationArr = this.fieldObj.fieldValidationArr;
    this.width = this.fieldObj.width;
    this.fieldType = this.fieldObj.selectControlType;
    this.dependent = this.fieldObj.dependentUniqueID;
    this.dependentOn = this.fieldObj.dependentOnUniqueID;
    this.fieldhref = this.fieldObj.defaultValue;
    this.fieldanchorLabel = this.fieldObj.option;
    // If field has validation Array
    if (this.validationArr.length > 0) {
      this.validationArr.forEach((data, i) => {
        if (
          data.validationType == "required" ||
          data.validationType == "readonly"
        ) {
          if (data.validationType == "required") {
            this.isRequired = true;
            this.Required = "required";
          } else if (data.validationType == "readonly") {
            this.ReadOnly = "readonly";
          }
        } else if (data.validationType == "range") {
          this.minRange = data.min;
          this.maxRange = data.max;
        } else {
          this.inputType = data.validationType;
        }
      });
    }

    // In case of Multi-Select
    if (this.fieldObj.option.length) {
      if (this.fieldObj.option.indexOf("|") != -1) {
        this.optionList = this.fieldObj.option.split("|");
      } else {
        this.optionList.push(this.fieldObj.option);
      }
      // Case of Multi-Select
      if (this.fieldType == 4) {
        this.optionList = this.optionList.map((data, i) => {
          return {
            id: data.trim(),
            name: data.trim()
          };
        });

        if (this.fieldObj.Value == "") {
          this.multiSelectModel = [];
        } else if (this.fieldObj.Value) {
          this.multiSelectModel = this.fieldObj.Value.split(",");
        }

        if (this.modelVal == "") {
          this.multiSelectModel = [];
        } else if (this.modelVal) {
          this.multiSelectModel = this.modelVal.split(",");
        }
      }
    }

    // Case of Image
    if (this.fieldType == 16) {
      if (this.modelVal) {
        this.imageURL = this.modelVal;
      } else if (this.fieldObj.Value) {
        this.imageURL = this.fieldObj.Value;
      }
    }

    // case of PDF File
    if (this.fieldType == 17) {
      if (this.modelVal) {
        this.tooltip = this.modelVal;
      } else if (this.fieldObj.Value) {
        this.tooltip = this.fieldObj.Value;
      }
    }

    // Called from table
    if (this.modelVal || this.modelVal == " " || this.modelVal == "") {
      if (this.fieldObj.uniqueId.includes("-")) {
        var id = this.fieldObj.uniqueId.split("-")[0];
        this.fieldObj.uniqueId = id + "-" + this.index;
      } else {
        this.fieldObj.uniqueId = this.fieldObj.uniqueId + "-" + this.index;
      }
    } // When not called from table
    else {
      if (this.fieldObj.uniqueId.includes("-")) {
        var id = this.fieldObj.uniqueId.split("-")[0];
        this.fieldObj.uniqueId = id;
      }
    }

    // Get field details from FieldObj
    this.fieldId = this.fieldObj.uniqueId;

    // Case of Radio Button
    if (this.fieldType == 13 || this.fieldType == 6) {
      if (this.dependent) {
        this.radioDependentIds = this.dependent.split("|");
      }
      if (this.fieldObj.Value && this.fieldObj.Value != "null") {
        if (
          this.fieldObj.Value.toLowerCase().trim() == "other" ||
          this.fieldObj.Value.toLowerCase().trim() ==
            "others (please specify)" ||
          this.fieldObj.Value.toLowerCase().trim() ==
            "Other (Not for protocol test)"
        ) {
          this.radioOther = true;
        } else {
          this.radioOther = false;
        }
      }
    }

    // If field is dependent on some other field
    if (this.dependentOn) {
      // check if dependentOn field has prop "checked" = true
      if ((<any>$("#" + this.dependentOn)).prop("checked")) {
        // (<any>$('#'+this.fieldId)).css('display','block');
        this.ReadOnly = "";
      }
      // check if dependentOn field has prop "checked" = false
      else {
        // (<any>$('#'+this.fieldId)).css('display','none');
        this.ReadOnly = "readonly";
        this.fieldObj.Value = "";
      }
    }
    if (this.fieldType == 2) {
      var that = this;
      setTimeout(function() {
        (<any>$("#" + that.fieldObj.uniqueId)).height(
          (<any>$("#" + that.fieldObj.uniqueId))[0].scrollHeight
        );
      }, 0);
    }
  }

  ngAfterViewInit() {
    // Case of Radio Button
    if (this.fieldType == 13 || this.fieldType == 6) {
      if (this.fieldObj.Value && this.fieldObj.Value != "null") {
        let index;
        let that = this;
        if (this.optionList.length > 0) {
          this.optionList.forEach((data, i) => {
            if (data.trim() == that.fieldObj.Value.trim()) {
              index = i;
              return;
            }
          });
        }

        (<any>$("#" + this.fieldId + "-" + index)).prop("checked", true);
      }
    }

    // If field has width
    if (this.width) {
      (<any>$("#container-" + this.fieldId)).css("width", this.width);
    } else {
      (<any>$("#container-" + this.fieldId)).css("width", "100%");
    }
    if (this.fieldObj.className) {
      if (this.fieldType != 3 && this.fieldType != 21) {
        var split = this.fieldObj.className.split("|");
        if (split.length > 1) {
          split.forEach(element => {
            (<any>$("#" + this.fieldObj.uniqueId)).addClass(element);
          });
        } else {
          (<any>$("#" + this.fieldObj.uniqueId)).attr(
            "DynamicClass",
            this.fieldObj.className
          );
        }
      } else {
        // condition for dropdown
        var split = this.fieldObj.className.split("|");
        // if (split.length > 1)
        {
          (<any>$("#" + this.fieldObj.uniqueId)).attr("DynamicClass", split[1]);
          (<any>$("#" + this.fieldObj.uniqueId)).addClass(split[0]);
        }
        if (
          this.fieldObj.Value != "" &&
          (this.router.url.split("/")[2].toLowerCase() ==
            "edit-ETRF".toLowerCase() ||
            this.router.url.split("/")[2].toLowerCase() ==
              "add-etrf".toLowerCase())
        ) {
          //added add-trf comparision to pre populate value in dropdown in add-trf case for SF
          // this.editDropDownoption(this);
          var classSelect = split[1];
          var classValue = this.fieldObj.Value;
          this.dataService.DynamicDropDependentValue.forEach(element => {
            if (element.class.toLowerCase() == split[0].toLowerCase()) {
              this.editDropDownoption(
                element.classVal,
                element.class,
                element.uniqueID,
                this.fieldObj
              );
              this.fieldObj.uniqueId;
            }
          });
          if (classSelect) {
            this.dataService.DynamicDropDependentValue.push({
              classVal: classValue,
              class: classSelect,
              uniqueID: this.fieldObj.uniqueId
            });
          }
          // debugger;
          // this.dataService.DynamicDropValue.forEach(element => {
          //   if (element.class.toLowerCase() == split[0].toLowerCase()) {
          //     this.editDropDownoption(
          //       element.classVal, element.class, element.uniqueID,
          //       this.fieldObj);
          //     this.fieldObj.uniqueId
          //   }
          // });
        }

        // fixed for console issue
        // this.dataService.DynamicDropValue.forEach(element => {
        // 	if (element.class.toLowerCase() == split[0].toLowerCase()) {
        // 		this.editDropDownoption(element.classVal, element.class,
        // element.uniqueID, this.fieldObj.Value);
        // this.fieldObj.uniqueId
        // 	}
        // });
        // End

        // else {
        // 	(<any>$('#' + this.fieldObj.uniqueId)).attr("DynamicClass",
        // split[0]);
        // }
      }
    }

    // In case of checkbox (add and edit)
    if (this.fieldType == 8 || this.fieldType == 15) {
      // When there is no value option
      var indexOfOther = this.optionList.indexOf("other");

      // (<any>$('#' + this.fieldObj.uniqueId)).attr("DynamicClass",
      // this.fieldObj.className);
      if (this.optionList.length == 0) {
        // if Value present, that value must be fieldId
        if (this.fieldObj.Value) {
          (<any>$("#" + this.fieldId)).prop("checked", true);
        }
      } else {
        // If Option has some value and "Value" field has something
        if (this.fieldObj.Value) {
          if (this.fieldObj.Value.indexOf("|") != -1) {
            var fields = this.fieldObj.Value.split("|");
            var that = this;
            fields.forEach(data => {
              var id = data.trim();
              if (id) {
                (<any>$("#" + id)).trigger("click");
              }
            });
          } else {
            var id = this.fieldObj.Value.trim();
            if (id) {
              (<any>$("#" + id)).trigger("click");
            }
          }
        }
      }
    }

    // In case of checkbox with text
    if (this.fieldType == 9) {
      if (this.fieldObj.Value) {
        (<any>$("#" + this.fieldObj.Value)).trigger("click");
      }
    }

    // Case of Image
    if (this.fieldType == 16) {
    }

    // if (this.fieldType == 18) {
    // 	console.log(this);
    // }

    if (this.dependentOn) {
      setTimeout(function() {
        if ((<any>$("#" + this.dependentOn)).prop("checked") == true) {
          (<any>$("#" + this.fieldId)).css("display", "block");
        }
      }, 0);
    }
    if (this.fieldType == 5) {
      if (this.fieldObj.enterLabelName == "Sample Size") {
        // 	that.IsSpecial = 0;
        // }
        // else {
        // 	that.IsSpecial = 1;
        (<any>$("#" + this.fieldObj.uniqueID)).val(
          (<any>$("#" + this.fieldObj.uniqueID + " option:eq(0)")).val()
        );
      }
    }

    if (this.fieldType == 19) {
      if (this.fieldObj.Value) {
        (<any>$("#" + this.fieldObj.Value)).trigger("click");
        var that = this;
        setTimeout(function() {
          (<any>$("#" + that.fieldObj.uniqueId + "-1")).height(
            (<any>$("#" + that.fieldObj.uniqueId + "-1"))[0].scrollHeight
          );
        }, 0);
      }
    }

    this.cdf.detectChanges();
  }

  GetParse(json){    
   this.ediDataList= JSON.parse(json);

  }

  editDropDownoption(
    classVal: any,
    clas: any,
    uniqueID: any,
    fieldObject: any
  ) {
    var that = this;
    this.Classattr = (<any>$("#" + uniqueID)).attr("DynamicClass");
    if (
      typeof this.Classattr !== typeof undefined &&
      this.Classattr !== false &&
      this.Classattr !== ""
    ) {
      var ID = (<any>$("." + this.Classattr)).attr("id");
      var ChangeValue = classVal;
      if (fieldObject.selectControlType == 21 && this.router.url.split("/")[2].toLowerCase() !=
      "add-etrf".toLowerCase()) {
        let agentValue = "";
        var classObject = that.dataService.DynamicDropDependentValue.find(
          i => i.class === "agentvendor"
        );
        if (classObject) {
          agentValue = classObject.classVal;
        }
        if (ChangeValue != "NA") {
          this.isLoader = true;
          that.dataService
            .get(
              "DynamicFormCreation/applyDependencyMapping?uniqueID=" +
                ID +
                "&ChangeValue=" +
                encodeURIComponent(ChangeValue) +
                "&UserId=" +
                that.userDetails.UserId +
                "&FormId=" +
                that.dataService.currentFormId +
                "&FirstChangeValue=" +
                encodeURIComponent(agentValue)
            )
            .subscribe(r => {
              this.isLoader = false;
              if (r.IsSuccess) {
                if (r.Data.length > 0) {
                  var option = "";
                  (<any>$("#" + ID)).find("option").remove();
                  (<any>$("#" + ID)).append(
                    '<option value="">Please select</option>'
                  );
                  r.Data.forEach(element => {
                    (<any>$("#" + ID)).append(
                      '<option value="' +
                        element.Value +
                        '">' +
                        element.Value +
                        "</option>"
                    );
                  });
                  (<any>$("#" + ID)).val(fieldObject.Value);
                } 
              } 
              else {
              }
            });
        }
      } else {
        that.dataService
          .get(
            "DynamicFormCreation/applyDependency?uniqueID=" +
              ID +
              "&ChangeValue=" +
              ChangeValue
          )
          .subscribe(r => {
            this.isLoader = false;
            if (r.IsSuccess) {
              var option = "";
              (<any>$("#" + ID)).find("option").remove();
              (<any>$("#" + ID)).append(
                '<option value="">Please select</option>'
              );
              r.Data.forEach(element => {
                // if (selectedValue == element.Value) {
                // 	alert(element.Value)
                // }
                (<any>$("#" + ID)).append(
                  '<option value="' +
                    element.Value +
                    '">' +
                    element.Value +
                    "</option>"
                );
              });
              (<any>$("#" + ID)).val(fieldObject.Value);
            } else {
            }
          });
      }
    }
  }
  textareaChange(e: any, fieldId: any, innerIndex: any) {
    // if (innerIndex) {
    // 	var id = fieldId + innerIndex;
    // }
    // else {
    // 	var id = fieldId;
    // }
    // (<any>$('#' + id)).height((<any>$('#' + id)).prop('scrollHeight'));
    // var textarea = document.getElementById(id);
    // textarea.style.cssText = 'height:auto; padding:0';
    // textarea.style.cssText = 'height:' + textarea.scrollHeight + 'px';
  }

  radioClick(label: any, fieldId: any, fieldObj: any, e: any) {
    var that = this;
    this.Classattr = (<any>$("#" + fieldId)).attr("dynamicclass");
    var classCreated = this.Classattr;
    if (
      typeof this.Classattr !== typeof undefined &&
      this.Classattr !== false
    ) {
      if (label.toLowerCase() != "others") {
        var userCredential = JSON.parse(localStorage.getItem("mts_user_cred"));
        var companyId = userCredential.UserId;
        that.dataService
          .get("DynamicFormCreation/VenderInfo?UserId=" + companyId)
          .subscribe(r => {
            if (r.IsSuccess) {
              var VendorInfo = [];
              that.Classattr = classCreated;
              var FirstName = (<any>(
                $("." + that.Classattr + ".FirstName")
              )).attr("id");
              var MiddleName = (<any>(
                $("." + that.Classattr + ".MiddleName")
              )).attr("id");
              var LastName = (<any>$("." + that.Classattr + ".LastName")).attr(
                "id"
              );
              var CompanyName = (<any>(
                $("." + that.Classattr + ".CompanyName")
              )).attr("id");
              var CompanyAddress = (<any>(
                $("." + that.Classattr + ".CompanyAddress")
              )).attr("id");
              // dropdown
              var dropCountryId = (<any>$("select." + that.Classattr)).attr(
                "id"
              );
              var CountryId = dropCountryId;
              var StateId = (<any>(
                $(
                  "select." + (<any>$("#" + dropCountryId)).attr("dynamicclass")
                )
              )).attr("id");
              var ID = StateId;
              var ChangeValue = r.Data.CountryId;
              this.isLoader = true;
              var stateSelected = r.Data.StateId;
              that.dataService
                .get(
                  "DynamicFormCreation/applyDependency?uniqueID=" +
                    ID +
                    "&ChangeValue=" +
                    ChangeValue +
                    "&UserId=" +
                    this.userDetails.UserId +
                    "&FormId=" +
                    that.router.url.split("/")[3]
                )
                .subscribe(r => {
                  this.isLoader = false;
                  if (r.IsSuccess) {
                    var option = "";
                    (<any>$("#" + ID)).find("option").remove();
                    (<any>$("#" + ID)).append("<option>select value</option>");
                    r.Data.forEach(element => {
                      (<any>$("#" + ID)).append(
                        '<option value="' +
                          element.Value +
                          '">' +
                          element.Value +
                          "</option>"
                      );
                    });
                    (<any>$("#" + ID)).val(stateSelected);
                  } else {
                  }
                });
              // End dropdown
              var TelephoneNo = (<any>(
                $("." + that.Classattr + ".TelephoneNo")
              )).attr("id");
              var Email = (<any>$("." + that.Classattr + ".Email")).attr("id");
              var MobileNo = (<any>$("." + that.Classattr + ".MobileNo")).attr(
                "id"
              );

              VendorInfo.push({ [FirstName]: r.Data.FirstName });
              VendorInfo.push({ [MiddleName]: r.Data.MiddleName });
              VendorInfo.push({ [LastName]: r.Data.LastName });
              VendorInfo.push({ [CompanyName]: r.Data.CompanyName });
              VendorInfo.push({ [CompanyAddress]: r.Data.CompanyAddress });
              VendorInfo.push({ [CountryId]: r.Data.CountryId });
              VendorInfo.push({ [StateId]: r.Data.StateId });
              VendorInfo.push({ [TelephoneNo]: r.Data.TelephoneNo });
              VendorInfo.push({ [Email]: r.Data.Email });
              VendorInfo.push({ [MobileNo]: r.Data.MobileNo });

              that.dataService.DynamicDropValue = VendorInfo;
              that.bindDataFromShowFields.emit(
                that.dataService.DynamicDropValue
              );

              (<any>$("." + that.Classattr + ".FirstName")).val(
                r.Data.FirstName
              );
              (<any>$("." + that.Classattr + ".MiddleName")).val(
                r.Data.MiddleName
              );
              (<any>$("." + that.Classattr + ".LastName")).val(r.Data.LastName);
              (<any>$("." + that.Classattr + ".CompanyName")).val(
                r.Data.CompanyName
              );
              (<any>$("." + that.Classattr + ".CompanyAddress")).val(
                r.Data.CompanyAddress
              );
              (<any>$("." + that.Classattr + ".CountryId")).val(
                r.Data.CountryId
              );
              (<any>$("." + that.Classattr + ".StateId")).val(r.Data.StateId);
              (<any>$("." + that.Classattr + ".TelephoneNo")).val(r.Data.phone);
              (<any>$("." + that.Classattr + ".Email")).val(r.Data.Email);
              (<any>$("." + that.Classattr + ".MobileNo")).val(r.Data.MobileNo);
            }
          });
      } else {
        var VendorInfo = [];
        var FirstName = (<any>$("." + that.Classattr + ".FirstName")).attr(
          "id"
        );
        var MiddleName = (<any>$("." + that.Classattr + ".MiddleName")).attr(
          "id"
        );
        var LastName = (<any>$("." + that.Classattr + ".LastName")).attr("id");
        var CompanyName = (<any>$("." + that.Classattr + ".CompanyName")).attr(
          "id"
        );
        var CompanyAddress = (<any>(
          $("." + that.Classattr + ".CompanyAddress")
        )).attr("id");
        // dropdown
        var dropCountryId = (<any>$("select." + that.Classattr)).attr("id");
        var CountryId = dropCountryId;
        var StateId = (<any>(
          $("select." + (<any>$("#" + dropCountryId)).attr("dynamicclass"))
        )).attr("id");
        // End dropdown
        var TelephoneNo = (<any>$("." + that.Classattr + ".TelephoneNo")).attr(
          "id"
        );
        var Email = (<any>$("." + that.Classattr + ".Email")).attr("id");
        var MobileNo = (<any>$("." + that.Classattr + ".MobileNo")).attr("id");
        VendorInfo.push({ [FirstName]: "" });
        VendorInfo.push({ [MiddleName]: "" });
        VendorInfo.push({ [LastName]: "" });
        VendorInfo.push({ [CompanyName]: "" });
        VendorInfo.push({ [CompanyAddress]: "" });
        VendorInfo.push({ [CountryId]: "" });
        VendorInfo.push({ [StateId]: "" });
        VendorInfo.push({ [TelephoneNo]: "" });
        VendorInfo.push({ [Email]: "" });
        VendorInfo.push({ [MobileNo]: "" });
        that.dataService.DynamicDropValue = VendorInfo;
        that.bindDataFromShowFields.emit(that.dataService.DynamicDropValue);
        (<any>$("." + that.Classattr + ".FirstName")).val("");
        (<any>$("." + that.Classattr + ".MiddleName")).val("");
        (<any>$("." + that.Classattr + ".LastName")).val("");
        (<any>$("." + that.Classattr + ".CompanyName")).val("");
        (<any>$("." + that.Classattr + ".CompanyAddress")).val("");
        (<any>$("." + that.Classattr + ".CountryId")).val("");
        (<any>$("." + that.Classattr + ".StateId")).val("");
        (<any>$("." + that.Classattr + ".TelephoneNo")).val("");
        (<any>$("." + that.Classattr + ".Email")).val("");
        (<any>$("." + that.Classattr + ".MobileNo")).val("");
      }
    }
  }

  Checkbox(fieldId: any, fieldObj: any, fieldLabel: any, index: any, e: any) {
    this.bindCheckbox(fieldId, fieldObj, fieldLabel, index, null);
    var newId: any;
    if (index != null) {
      newId = fieldId + "-" + index;
    } else {
      newId = fieldId;
    }
    if (fieldLabel) {
      if (
        fieldLabel.toLowerCase().trim() == "other" ||
        fieldLabel.toLowerCase().trim() == "others (please specify)" ||
        this.fieldObj.Value.toLowerCase().trim() ==
          "Other (Not for protocol test)"
      ) {
        if ((<any>$("#" + newId)).prop("checked") == true) {
          var that = this;
          setTimeout(function() {
            that.checkboxOther = true;
          }, 0);
        } else {
          this.checkboxOther = false;
          fieldObj.Value1 = "";
        }
      }
    }
  }

  bindCheckbox(
    fieldId: any,
    fieldObj: any,
    fieldLabel: any,
    index: any,
    e: any
  ) {
    // If checkbox have options
    var that = this;
    if (index != null) {
      var id = fieldId + "-" + index;

      // If checkbox is checked
      if ((<any>$("#" + id + ":checked")).prop("checked")) {
        var fieldValue = id;
        if (!fieldObj.Value.includes(fieldValue)) {
          fieldObj.Value = fieldObj.Value + id.toString() + " | ";
        }

        if (this.dependent) {
          var dependentId;
          if (this.dependent.indexOf("|") != -1) {
            var x = this.dependent.split("|");
            dependentId = x[index].trim();
          } else {
            dependentId = this.dependent.trim();
          }

          var y = dependentId.indexOf("Section");
          if (y != -1) {
            (<any>$("#" + dependentId)).css("display", "block");
            if ((<any>$("#" + dependentId)).hasClass("hidden")) {
              (<any>$("#" + dependentId)).removeClass("hidden");
            }
          } else {
            var tagname = document.getElementById(dependentId).tagName;
            if (tagname.toLowerCase() == "input") {
              (<any>$("#" + dependentId)).attr("readonly", false);
              (<any>$("#" + dependentId)).attr("disabled", false);
            } else {
              (<any>$("#" + dependentId + " input")).attr("readonly", false);
              (<any>$("#" + dependentId + " input")).attr("disabled", false);
            }
          }
        }
      }
      // If checkbox is unchecked
      else {
        if (fieldObj.Value) {
          var tempArr = fieldObj.Value.split("|");
          tempArr.forEach((data, i) => {
            if (data.trim() == id) {
              tempArr.splice(i, 1);
            }
          });
          if (this.fieldObj.dependentUniqueID) {
            var dependentId = fieldObj.dependentUniqueID
              .split("|")
              [index].trim();
            tempArr.splice(index, 1);
            var y = dependentId.indexOf("Section");
            if (y != -1) {
              (<any>$("#" + dependentId)).addClass("hidden");
            } else {
              var tagname = document.getElementById(dependentId).tagName;
              if (tagname.toLowerCase() == "input") {
                (<any>$("#" + dependentId)).attr("readonly", true);
                (<any>$("#" + dependentId)).attr("disabled", true);
                (<any>$("#" + dependentId)).val("");
              } else {
                (<any>$("#" + dependentId + " input")).attr("readonly", true);
                (<any>$("#" + dependentId + " input")).attr("disabled", true);
                (<any>$("#" + dependentId + " input")).prop("checked", false);
                (<any>$("#" + dependentId + " input")).val("");
              }
            }
          }

          fieldObj.Value = tempArr.join(" | ");
        }
      }
    }

    // If single checkbox
    else {
      // If checked
      var Multidependent = this.dependent.split(",");
      Multidependent.forEach(element => {
        if ((<any>$("#" + fieldId)).prop("checked")) {
          fieldObj.Value = fieldId;
          if (element) {
            // alert("rohit");
            // (<any>$('#' + this.dependent)).css('display', 'block');
            // if ((<any>$('#' + this.dependent)).hasClass('hidden')) {
            // 	(<any>$('#' + this.dependent)).removeClass('hidden');
            // }
            (<any>$("#" + element)).attr("readonly", false);
          }
        }
        // If unchecked
        else {
          fieldObj.Value = "";
          if (element) {
            // alert("rohit");
            (<any>$("#" + element)).attr("readonly", true);
            // (<any>$('#' + this.dependent)).addClass('hidden');
            //  (<any>$('#' + element)).find("input[type=text], textarea").val("
            //  ");
            (<any>$("#" + element)).val(" ");
          }
        }
      });

      this.Classattr = (<any>$("#" + fieldId)).attr("dynamicclass");
      if (
        typeof this.Classattr !== typeof undefined &&
        this.Classattr !== false
      ) {
        if ((<any>$("#" + fieldId)).prop("checked")) {
          var userCredential = JSON.parse(
            localStorage.getItem("mts_user_cred")
          );
          var companyId = userCredential.UserId;
          // if (userCredential.UserCompanyId < 1) {
          // 	companyId = userCredential.UserId;
          // }
          // else {
          // 	companyId = userCredential.UserId;
          // }
          that.dataService
            .get("DynamicFormCreation/VenderInfo?UserId=" + companyId)
            .subscribe(r => {
              if (r.IsSuccess) {
                var VendorInfo = [];
                var FirstName = (<any>(
                  $("." + that.Classattr + ".FirstName")
                )).attr("id");
                var MiddleName = (<any>(
                  $("." + that.Classattr + ".MiddleName")
                )).attr("id");
                var LastName = (<any>(
                  $("." + that.Classattr + ".LastName")
                )).attr("id");
                var CompanyName = (<any>(
                  $("." + that.Classattr + ".CompanyName")
                )).attr("id");
                var CompanyAddress = (<any>(
                  $("." + that.Classattr + ".CompanyAddress")
                )).attr("id");
                var FullName = (<any>(
                  $("." + that.Classattr + ".FullName")
                )).attr("id");
                // dropdown
                var dropCountryId = (<any>$("select." + that.Classattr)).attr(
                  "id"
                );
                var CountryId = dropCountryId;
                var CityId = (<any>$("." + that.Classattr + ".CityId")).attr(
                  "id"
                );
                var StateId = (<any>(
                  $(
                    "select." +
                      (<any>$("#" + dropCountryId)).attr("dynamicclass")
                  )
                )).attr("id");
                var ID = StateId;
                var ChangeValue = r.Data.CountryId;
                this.isLoader = true;
                var stateSelected = r.Data.StateId;
                that.dataService
                  .get(
                    "DynamicFormCreation/applyDependency?uniqueID=" +
                      ID +
                      "&ChangeValue=" +
                      ChangeValue +
                      "&UserId=" +
                      this.userDetails.UserId +
                      "&FormId=" +
                      that.router.url.split("/")[3]
                  )
                  .subscribe(r => {
                    this.isLoader = false;
                    if (r.IsSuccess) {
                      var option = "";
                      (<any>$("#" + ID)).find("option").remove();
                      (<any>$("#" + ID)).append(
                        "<option>select value</option>"
                      );

                      r.Data.forEach(element => {
                        var NewID = element.Value.replace(/ /g, "_");
                        // alert(ID);
                        // ID=ID.;
                        (<any>$("#" + ID)).append(
                          '<option value="' +
                            NewID +
                            '">' +
                            element.Value +
                            "</option>"
                        );
                      });
                      stateSelected = stateSelected.trim().replace(/ /g, "_");
                      (<any>$("#" + ID)).val(stateSelected);
                    } else {
                    }
                  });
                // End dropdown
                var TelephoneNo = (<any>(
                  $("." + that.Classattr + ".TelephoneNo")
                )).attr("id");
                var Email = (<any>$("." + that.Classattr + ".Email")).attr(
                  "id"
                );
                var MobileNo = (<any>(
                  $("." + that.Classattr + ".MobileNo")
                )).attr("id");

                VendorInfo.push({ [FirstName]: r.Data.FirstName });
                VendorInfo.push({ [MiddleName]: r.Data.MiddleName });
                VendorInfo.push({ [LastName]: r.Data.LastName });
                VendorInfo.push({ [CompanyName]: r.Data.CompanyName });
                VendorInfo.push({ [CompanyAddress]: r.Data.CompanyAddress });
                VendorInfo.push({ [CountryId]: r.Data.CountryId });
                VendorInfo.push({ [StateId]: r.Data.StateId });
                VendorInfo.push({ [TelephoneNo]: r.Data.TelephoneNo });
                VendorInfo.push({ [Email]: r.Data.Email });
                VendorInfo.push({ [MobileNo]: r.Data.MobileNo });
                VendorInfo.push({ [CityId]: r.Data.CityId });
                VendorInfo.push({ [FullName]: r.Data.FullName });

                that.dataService.DynamicDropValue = VendorInfo;
                that.bindDataFromShowFields.emit(
                  that.dataService.DynamicDropValue
                );

                (<any>$("." + that.Classattr + ".FirstName")).val(
                  r.Data.FirstName
                );
                (<any>$("." + that.Classattr + ".MiddleName")).val(
                  r.Data.MiddleName
                );
                (<any>$("." + that.Classattr + ".LastName")).val(
                  r.Data.LastName
                );
                (<any>$("." + that.Classattr + ".CompanyName")).val(
                  r.Data.CompanyName
                );
                (<any>$("." + that.Classattr + ".CompanyAddress")).val(
                  r.Data.CompanyAddress
                );
                (<any>$("." + that.Classattr + ".CountryId")).val(
                  r.Data.CountryId
                );
                (<any>$("." + that.Classattr + ".StateId")).val(r.Data.StateId);
                (<any>$("." + that.Classattr + ".TelephoneNo")).val(
                  r.Data.TelephoneNo
                );
                (<any>$("." + that.Classattr + ".Email")).val(r.Data.Email);
                (<any>$("." + that.Classattr + ".MobileNo")).val(
                  r.Data.MobileNo
                );
                (<any>$("." + that.Classattr + ".CityId")).val(r.Data.CityId);
                (<any>$("." + that.Classattr + ".FullName")).val(
                  r.Data.FullName
                );
              }
            });
        } else {
          var VendorInfo = [];
          var FullName = (<any>$("." + that.Classattr + ".FullName")).attr(
            "id"
          );
          var CityId = (<any>$("." + that.Classattr + ".CityId")).attr("id");
          var FirstName = (<any>$("." + that.Classattr + ".FirstName")).attr(
            "id"
          );
          var MiddleName = (<any>$("." + that.Classattr + ".MiddleName")).attr(
            "id"
          );
          var LastName = (<any>$("." + that.Classattr + ".LastName")).attr(
            "id"
          );
          var CompanyName = (<any>(
            $("." + that.Classattr + ".CompanyName")
          )).attr("id");
          var CompanyAddress = (<any>(
            $("." + that.Classattr + ".CompanyAddress")
          )).attr("id");
          // var CountryId = (<any>$('.' + that.Classattr +
          // '.CountryId')).attr('id'); var StateId = (<any>$('.' +
          // that.Classattr + '.StateId')).attr('id');
          // dropdown
          var dropCountryId = (<any>$("select." + that.Classattr)).attr("id");
          var CountryId = dropCountryId;
          var StateId = (<any>(
            $("select." + (<any>$("#" + dropCountryId)).attr("dynamicclass"))
          )).attr("id");
          // End dropdown
          var TelephoneNo = (<any>(
            $("." + that.Classattr + ".TelephoneNo")
          )).attr("id");
          var Email = (<any>$("." + that.Classattr + ".Email")).attr("id");
          var MobileNo = (<any>$("." + that.Classattr + ".MobileNo")).attr(
            "id"
          );

          VendorInfo.push({ [FirstName]: "" });
          VendorInfo.push({ [MiddleName]: "" });
          VendorInfo.push({ [LastName]: "" });
          VendorInfo.push({ [CompanyName]: "" });
          VendorInfo.push({ [CompanyAddress]: "" });
          VendorInfo.push({ [CountryId]: "" });
          VendorInfo.push({ [StateId]: "" });
          VendorInfo.push({ [TelephoneNo]: "" });
          VendorInfo.push({ [Email]: "" });
          VendorInfo.push({ [MobileNo]: "" });
          VendorInfo.push({ [CityId]: "" });
          VendorInfo.push({ [FullName]: "" });
          that.dataService.DynamicDropValue = VendorInfo;
          that.bindDataFromShowFields.emit(that.dataService.DynamicDropValue);

          (<any>$("." + that.Classattr + ".FirstName")).val("");
          (<any>$("." + that.Classattr + ".MiddleName")).val("");
          (<any>$("." + that.Classattr + ".LastName")).val("");
          (<any>$("." + that.Classattr + ".CompanyName")).val("");
          (<any>$("." + that.Classattr + ".CompanyAddress")).val("");
          (<any>$("." + that.Classattr + ".CountryId")).val("");
          (<any>$("." + that.Classattr + ".StateId")).val("");
          (<any>$("." + that.Classattr + ".TelephoneNo")).val("");
          (<any>$("." + that.Classattr + ".Email")).val("");
          (<any>$("." + that.Classattr + ".MobileNo")).val("");
          (<any>$("." + that.Classattr + ".CityId")).val("");
          (<any>$("." + that.Classattr + ".FullName")).val("");
        }
      }
      // if ((<any>$("#" + fieldId)).prop('checked')) {
      // 	fieldObj.Value = fieldId;
      // 	if (this.dependent) {
      // 		// (<any>$('#' + this.dependent)).css('display',
      // 'block');
      // 		// if ((<any>$('#' +
      // this.dependent)).hasClass('hidden'))
      // {
      // 		// 	(<any>$('#' +
      // this.dependent)).removeClass('hidden');
      // 		// }
      // 		(<any>$('#' + this.dependent)).attr('readonly', false);
      // 	}
      // }
      // // If unchecked
      // else {
      // 	fieldObj.Value = "";
      // 	if (this.dependent) {
      // 		(<any>$('#' + this.dependent)).attr('readonly', true);
      // 		// (<any>$('#' + this.dependent)).addClass('hidden');
      // 		// (<any>$('#' +
      // this.dependent)).find("input[type=text], textarea").val(" ");
      // 	}
      // }
    }
  }

  checkBoxWithText(id: any, fieldObj: any) {
    if ((<any>$("#" + id)).prop("checked") == true) {
      this.checkboxWithText = true;

      fieldObj.Value = id.toString();
    } else {
      this.checkboxWithText = false;
      fieldObj.Value = "";
      fieldObj.Value1 = "";
    }
  }

  checkBoxListText(id: any, fieldLabel: any, index: any, fieldObj: any) {
    this.bindCheckbox(id, fieldObj, null, index, null);
    var newId = id + "-" + index;
    if (
      fieldLabel.toLowerCase().trim() == "other" ||
      fieldLabel.toLowerCase().trim() == "others (please specify)" ||
      this.fieldObj.Value.toLowerCase().trim() ==
        "Other (Not for protocol test)"
    ) {
      if ((<any>$("#" + newId)).prop("checked") == true) {
        this.checkboxListOther = true;
      } else {
        this.checkboxListOther = false;
        fieldObj.Value1 = "";
      }
    }
  }

  checkDependent(id: any, label: any, fieldId: any, fieldObj: any, e: any) {
    this.radioClick(label, fieldId, fieldObj, e);
    if (id) {
      var id = id.trim();
      if (this.radioDependentIds.length > 0) {
        this.radioDependentIds.forEach(data => {
          if (data != "null") {
            if (data.trim().includes(",")) {
              var ids = data.trim().split(",");
              ids.forEach(newData => {
                if (<any>$("#" + newData.trim())) {
                  var x = newData.trim().indexOf("Section");
                  if (x != -1) {
                    if ((<any>$("#" + newData.trim())).hasClass("hidden")) {
                    } else {
                      (<any>$("#" + newData.trim())).addClass("hidden");
                    }
                  } else {
                    var tagname = document.getElementById(newData.trim())
                      .tagName;
                    if (tagname.toLowerCase() == "input") {
                      (<any>$("#" + newData.trim())).attr("readonly", true);
                      (<any>$("#" + newData.trim())).val("");
                    } else {
                      (<any>$("#" + newData.trim() + " input")).attr(
                        "readonly",
                        true
                      );
                      (<any>$("#" + newData.trim() + " input")).prop(
                        "checked",
                        false
                      );
                      (<any>$("#" + newData.trim() + " input")).attr(
                        "disabled",
                        true
                      );
                    }
                  }
                }
              });
            } else {
              if (<any>$("#" + data.trim())) {
                var x = data.trim().indexOf("Section");
                if (x != -1) {
                  if ((<any>$("#" + data.trim())).hasClass("hidden")) {
                  } else {
                    (<any>$("#" + data.trim())).addClass("hidden");
                  }
                } else {
                  var tagname = document.getElementById(data.trim()).tagName;
                  if (tagname.toLowerCase() == "input") {
                    (<any>$("#" + data.trim())).attr("readonly", true);
                    (<any>$("#" + data.trim())).val("");
                  } else {
                    (<any>$("#" + data.trim() + " input")).attr(
                      "readonly",
                      true
                    );
                    (<any>$("#" + data.trim() + " input")).prop(
                      "checked",
                      false
                    );
                    (<any>$("#" + data.trim() + " input")).attr(
                      "disabled",
                      true
                    );
                  }
                }
              }
            }
          }
        });
      }
      if (id.includes(",")) {
        var newIds = id.split(",");
        newIds.forEach(newId => {
          var y = newId.trim().indexOf("Section");
          if (y != -1) {
            (<any>$("#" + newId)).css("display", "block");
            if ((<any>$("#" + newId)).hasClass("hidden")) {
              (<any>$("#" + newId)).removeClass("hidden");
            }
          } else {
            var tagName = document.getElementById(newId.trim()).tagName;
            if (tagName.toLowerCase() == "input") {
              (<any>$("#" + newId.trim())).attr("readonly", false);
              (<any>$("#" + newId.trim())).attr("disabled", false);
            } else {
              (<any>$("#" + newId.trim() + " input")).attr("readonly", false);
              (<any>$("#" + newId.trim() + " input")).attr("disabled", false);
            }
          }
        });
      } else if (id != "null") {
        var y = id.trim().indexOf("Section");
        if (y != -1) {
          (<any>$("#" + id)).css("display", "block");
          if ((<any>$("#" + id)).hasClass("hidden")) {
            (<any>$("#" + id)).removeClass("hidden");
          }
        } else {
          var tagName = document.getElementById(id.trim()).tagName;
          if (tagName.toLowerCase() == "input") {
            (<any>$("#" + id.trim())).attr("readonly", false);
            (<any>$("#" + id.trim())).attr("disabled", false);
          } else {
            (<any>$("#" + id.trim() + " input")).attr("readonly", false);
            (<any>$("#" + id.trim() + " input")).attr("disabled", false);
          }
        }
      }
    }
    if (label && label != "null") {
      if (
        label.toLowerCase().trim() == "other" ||
        label.toLowerCase().trim() == "others (please specify)" ||
        this.fieldObj.Value.toLowerCase().trim() ==
          "Other (Not for protocol test)"
      ) {
        this.radioOther = true;
      } else {
        this.radioOther = false;
      }
    }
  }

  checkVal(id: any) {
    if ((<any>$("#" + id)).hasClass("no-val")) {
      (<any>$("#" + id)).removeClass("no-val");
    }
  }

  numericRange(model: any, min: any, max: any, fieldType: any) {
    if (fieldType == "number") {
      if (min && max) {
        if (parseInt(model) < parseInt(min)) {
          if (this.calledFrom == "table") {
            this.modelVal = "";
          } else {
            this.fieldObj.Value = "";
          }
          swal("", "Please enter number between " + min + " and " + max);
        }
        if (parseInt(model) > parseInt(max)) {
          if (this.calledFrom == "table") {
            this.modelVal = "";
          } else {
            this.fieldObj.Value = "";
          }

          swal("", "Please enter number between " + min + " and " + max);
        }
      }
    }
    if (fieldType == "email") {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (reg.test(model) == false) {
        if (this.calledFrom == "table") {
          this.modelVal = "";
        } else {
          this.fieldObj.Value = "";
        }

        swal("", "Please enter valid email id");
      }
    }
  }

  fileChangeEvent(event: any, fieldObj: any, tableObj: any) {
    var fileNamesArr = [];
    var formData = new FormData();
    if (event.target.files.length > 0) {
      for (let i = 0; i < event.target.files.length; i++) {
        if (
          event.target.files[0].type == "image/jpeg" ||
          event.target.files[0].type == "image/png"
        ) {
          fileNamesArr.push(event.target.files[i].name);
        } else {
          (<any>$("#" + fieldObj.uniqueId)).val("");
          swal("", "Please add only images");
          return;
        }
      }
      // this.fieldObj.Value = fileNamesArr.join(',');
      formData.append("UploadedImage", event.target.files[0]);
      var that = this;

      that.isLoader = true;
      var id = event.currentTarget.id;
      var returnObj = this.dataService.postFile(
        "DynamicFormCreation/SaveImage",
        formData
      );
      returnObj.done(function(xhr, textStatus) {
        that.isLoader = false;
        if (textStatus == "success") {
          var imageURL = that.imagePath + "/" + xhr.Data;
          if (that.calledFrom == "table") {
            // id = id +'-'+ this.index;
            tableObj.addDataArr[that.index][that.innerIndex] = imageURL;
            that.imageURL = imageURL.toString();
            (<any>$("#" + id + "-container")).css("display", "block");
          } else {
            fieldObj.Value = imageURL.toString();
            that.imageURL = imageURL.toString();
            (<any>$("#" + id + "-container")).css("display", "block");
          }
        }
      });
    }
  }

  UploadFiles(event: any, fieldObj: any, tableObj: any) {
    var that = this;
    var fileNamesArr = [];
    var formData = new FormData();
    if (event.target.files.length > 0) {
      this.fieldObj.Value = fileNamesArr.join(",");
      formData.append("UploadedFile", event.target.files[0]);
      // var that = this;
      that.isLoader = true;
      var id = event.currentTarget.id;
      var returnObj = this.dataService.postFile(
        "DynamicFormCreation/SaveFiles",
        formData
      );
      returnObj.done(function(xhr, textStatus) {
        that.isLoader = false;
        // var imageURL = that.imagePath + "/" + xhr.Data;
        if (xhr.IsSuccess == true) {
          (<any>$("#" + id + "-containerFile")).css("display", "block");
          fieldObj.Value = xhr.Data;
          if (that.dataService.SendEmailEtrfFile != "") {
            that.dataService.SendEmailEtrfFile =
              that.dataService.SendEmailEtrfFile + "," + xhr.Data;
          } else {
            that.dataService.SendEmailEtrfFile = xhr.Data;
          }
          that.tooltip = xhr.Data;
        } else {
          swal("", xhr.Message);
        }
      });
    }
  }

  // dropdown change//rohit
  tableChange(index: any, innerIndex: any, e: any, formobj: any = {}) {
    var that = this;
    this.Classattr = (<any>$("#" + e.currentTarget.id)).attr("DynamicClass");
    if (this.calledFrom == "table") {
      this.sectionObj.addTableArr[this.tableIndex].addDataArr[index][
        innerIndex
      ] = e.target.value;
    } else if (
      typeof this.Classattr !== typeof undefined &&
      this.Classattr !== false &&
      this.Classattr !== ""
    ) {
      var ID = (<any>$("." + this.Classattr)).attr("id");
      var ChangeValue = e.currentTarget.value;
      // alert(ID + "      " + ChangeValue);
      this.isLoader = true;
      that.dataService
        .get(
          "DynamicFormCreation/applyDependency?uniqueID=" +
            ID +
            "&ChangeValue=" +
            ChangeValue
        )
        .subscribe(r => {
          this.isLoader = false;
          if (r.IsSuccess) {
            var option = "";
            (<any>$("#" + ID)).find("option").remove();
            (<any>$("#" + ID)).append(
              '<option value="">Please select</option>'
            );
            r.Data.forEach(element => {
              (<any>$("#" + ID)).append(
                '<option value="' +
                  element.Value +
                  '">' +
                  element.Value +
                  "</option>"
              );
            });
            that.cleanOption(ID);
          } else {
          }
        });
    }
  }

  cleanOption(id: any) {
    // alert(id);
    var dynamic = (<any>$("#" + id)).attr("dynamicclass");
    if (dynamic == "") {
      return;
    }
    (<any>$("." + dynamic)).find("option").remove();
    (<any>$("." + dynamic)).append('<option value="">Please select</option>');
    this.cleanOption((<any>$("." + dynamic)).attr("id"));
  }

  multiSelectChange(Obj: any) {
    if (this.modelVal) {
      if (this.multiSelectModel.length > 0) {
        this.modelVal = this.multiSelectModel.join(",");
      } else {
        this.modelVal = "";
      }
    } else {
      if (this.multiSelectModel.length > 0) {
        Obj.Value = this.multiSelectModel.join(",");
      } else {
        Obj.Value = "";
      }
    }
  }

  //fieldType = 19 //Checkbox with TextArea
  checkBoxWithTextArea(id: any, fieldObj: any) {
    if ((<any>$("#" + id)).prop("checked") == true) {
      this.checkboxWithTextArea = true;

      fieldObj.Value = id.toString();
    } else {
      this.checkboxWithTextArea = false;
      fieldObj.Value = "";
      fieldObj.Value1 = "";
    }
  }

  //FieldType = 21 //Dropdown For SF Agent,Vendor and Factory
  tableChangeForMappingDropdown(
    index: any,
    innerIndex: any,
    e: any,
    formobj: any = {}
  ) {
    var that = this;
    let agentValue = "";
    this.Classattr = (<any>$("#" + e.currentTarget.id)).attr("DynamicClass");
    let classSplit = formobj.className.split("|");
    if (classSplit.length > 1 && classSplit[0] != "" && classSplit[1] != "") {
      agentValue = (<any>$("[dynamicclass=" + classSplit[0] + "]")).val();
    }
    if (this.calledFrom == "table") {
      this.sectionObj.addTableArr[this.tableIndex].addDataArr[index][
        innerIndex
      ] = e.target.value;
    } else if (
      typeof this.Classattr !== typeof undefined &&
      this.Classattr !== false &&
      this.Classattr !== ""
    ) {
      var ID = (<any>$("." + this.Classattr)).attr("id");
      var ChangeValue = e.currentTarget.value;
      this.isLoader = true;
      that.dataService
        .get(
          "DynamicFormCreation/applyDependencyMapping?uniqueID=" +
            ID +
            "&ChangeValue=" +
            encodeURIComponent(ChangeValue) +
            "&UserId=" +
            this.userDetails.UserId +
            "&FormId=" +
            that.dataService.currentFormId +
            "&FirstChangeValue=" +
            encodeURIComponent(agentValue)
        )
        .subscribe(r => {
          this.isLoader = false;
          if (r.IsSuccess) {
            if (r.Data.length > 0) {
              (<any>$("#" + ID)).find("option").remove();
              (<any>$("#" + ID)).append(
                '<option value="">Please select</option>'
              );
              r.Data.forEach((element, index) => {
                (<any>$("#" + ID)).append(
                  '<option value="' +
                    element.Value +
                    '">' +
                    element.Value +
                    "</option>"
                );
              });
            } else {
              (<any>$("#" + ID)).find("option").remove();
              (<any>$("#" + ID)).append(
                '<option value="">Please select</option>'
              );
              if (ChangeValue != "") {
                (<any>$("#" + ID)).append('<option value="NA">NA</option>');
              }
            }
            that.cleanOption(ID);
          } else {
          }
        });
    }
  }

  // getArr(fieldObjValue){
  //   this.dataService.ediDataList1=new Array<SF_EDI_Data>();
  //   this.dataService.ediDataList1=JSON.parse(fieldObjValue);

  //   return this.dataService.ediDataList1;
  // }

  update(ediData,fieldName,caption,index){
    index=this.ediDataList1.findIndex(i=>i.SF_EDI_DATA_ID==ediData.SF_EDI_DATA_ID);
    this.dataService.fieldType=fieldName;
    this.dataService.caption=caption;
    let selectedEDIData=this.ediDataList1[index];
    this.dataService.ediDataFieldId=ediData.SF_EDI_DATA_ID;
    if(selectedEDIData[fieldName]){
    this.dataService.fieldValue=selectedEDIData[fieldName].trim();
    }else{
      this.dataService.fieldValue="";
    }
     
    (<any>$('#UpdateData')).modal('show');
  }
}
