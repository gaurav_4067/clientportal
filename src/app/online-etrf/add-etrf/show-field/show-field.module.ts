import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowFieldComponent } from './show-field.component';
import { FormsModule } from '@angular/forms';
import { DateTimePickerModule } from 'ng-pick-datetime';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
// import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
// import { MdInputModule } from '@angular2-material/input';
import { MdCoreModule } from '@angular2-material/core';
import { MaterialModule } from '@angular/material';
import {TooltipModule} from "ngx-tooltip";
import { FormatBusinessCategoryModule } from 'app/helper/format-business-category/format-business-category.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DateTimePickerModule,
    MultiselectDropdownModule,
    // Ng2AutoCompleteModule,
    MdCoreModule,
    MaterialModule,
    TooltipModule,
    FormatBusinessCategoryModule
  ],
  declarations: [ShowFieldComponent],
  exports: [ShowFieldComponent]
})
export class ShowFieldModule { }
