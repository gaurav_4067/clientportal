import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowSubsectionComponent } from './show-subsection.component';

describe('ShowSubsectionComponent', () => {
  let component: ShowSubsectionComponent;
  let fixture: ComponentFixture<ShowSubsectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowSubsectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowSubsectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
