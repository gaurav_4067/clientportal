import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowSubsectionComponent } from './show-subsection.component';
import { ShowFieldModule } from 'app/online-etrf/add-etrf/show-field/show-field.module';

@NgModule({
  imports: [
    CommonModule,
    ShowFieldModule
  ],
  declarations: [ShowSubsectionComponent],
  exports:[ShowSubsectionComponent]
})
export class ShowSubsectionModule { }
