
import { Component, Input, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-show-subsection',
  templateUrl: './show-subsection.component.html',
  styleUrls: ['./show-subsection.component.css']
})
export class ShowSubsectionComponent implements OnInit, AfterViewInit {

  @Input() subSectionObj;
  width: any;
  subSectionId: any;
  subSectionName: any;
  addFieldsArr: any = [];

  ngOnInit() {
    this.width = this.subSectionObj.width;
    this.subSectionId = this.subSectionObj.uniqueId;
    this.subSectionName = this.subSectionObj.SubSectionName;
    this.addFieldsArr = this.subSectionObj.addFieldsArr;
  }

  ngAfterViewInit() {
    if (this.width) {
      (<any>$('#' + this.subSectionId)).css('width', this.width);
    }
    else {
      (<any>$('#' + this.subSectionId)).css('width', '100%');
    }
  }
}