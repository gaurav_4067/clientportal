import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddEtrfComponent} from 'app/online-etrf/add-etrf/add-etrf.component';

const routes: Routes = [{path: 'etrf', component: AddEtrfComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class AddEtrfRoutingModule {
}
