import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEtrfComponent } from './add-etrf.component';

describe('AddEtrfComponent', () => {
  let component: AddEtrfComponent;
  let fixture: ComponentFixture<AddEtrfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEtrfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEtrfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
