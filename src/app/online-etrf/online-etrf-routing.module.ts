import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AddEtrfComponent} from './add-etrf/add-etrf.component';
import {OnlineEtrfComponent} from './online-etrf.component';

const routes: Routes = [{
  path: '',
  children: [
    {path: '', component: OnlineEtrfComponent},
    {path: 'add-etrf/:reportNo/:sfEDIId', component: AddEtrfComponent},
    {path: 'add-etrf/:reportNo/:sfEDIId/:sfBusinessLine/:trType', component: AddEtrfComponent},
    {path: 'add-etrf/:reportNo', component: AddEtrfComponent},
    {path: 'edit-etrf/:reportNo', component: AddEtrfComponent}
  ]
}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class OnlineEtrfRoutingModule {
}
