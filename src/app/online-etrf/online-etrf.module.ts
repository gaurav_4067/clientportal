import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';
import {MdCoreModule} from '@angular2-material/core';
import {MdInputModule} from '@angular2-material/input';
import {FooterModule} from 'app/footer/footer.module';
import {HeaderModule} from 'app/header/header.module';
import {DateTimePickerModule} from 'ng-pick-datetime';
import {Ng2PaginationModule} from 'ng2-pagination';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import {TooltipModule} from "ngx-tooltip";
import {AddEtrfModule} from './add-etrf/add-etrf.module';
import {OnlineEtrfRoutingModule} from './online-etrf-routing.module';
import {OnlineEtrfComponent} from './online-etrf.component';
 
import { FcaiOnlineEtrfModule } from './fcai-online-etrf/fcai-online-etrf.module';
 
import { Md2Module } from 'md2';
import { EdiDataManipulationPipePipe } from 'app/pipe/edi-data-manipulation-pipe.pipe';
 
 

@NgModule({
  imports: [
    CommonModule, OnlineEtrfRoutingModule, HeaderModule, FooterModule,
    AddEtrfModule, FormsModule, DateTimePickerModule, MdInputModule,
    MdCoreModule, MaterialModule, Ng2PaginationModule,TooltipModule,Ng2AutoCompleteModule,FcaiOnlineEtrfModule
  ],
  declarations: [OnlineEtrfComponent,EdiDataManipulationPipePipe]
})
export class OnlineEtrfModule {
}
