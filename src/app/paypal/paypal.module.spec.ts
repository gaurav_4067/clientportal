import { PaypalModule } from './paypal.module';

describe('PaypalModule', () => {
  let paypalModule: PaypalModule;

  beforeEach(() => {
    paypalModule = new PaypalModule();
  });

  it('should create an instance', () => {
    expect(paypalModule).toBeTruthy();
  });
});
