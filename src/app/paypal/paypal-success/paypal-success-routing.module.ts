import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PaypalSuccessComponent} from './paypal-success.component';

const routes: Routes = [{path: '', component: PaypalSuccessComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class PaypalSuccessRoutingModule {
}
