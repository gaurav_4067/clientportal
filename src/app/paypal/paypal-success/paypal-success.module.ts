import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaypalSuccessRoutingModule } from './paypal-success-routing.module';
import { PaypalSuccessComponent } from './paypal-success.component';

@NgModule({
  imports: [
    CommonModule,
    PaypalSuccessRoutingModule
  ],
  declarations: [PaypalSuccessComponent]
})
export class PaypalSuccessModule { }
