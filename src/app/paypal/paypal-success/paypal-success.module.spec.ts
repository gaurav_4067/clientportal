import { PaypalSuccessModule } from './paypal-success.module';

describe('PaypalSuccessModule', () => {
  let paypalSuccessModule: PaypalSuccessModule;

  beforeEach(() => {
    paypalSuccessModule = new PaypalSuccessModule();
  });

  it('should create an instance', () => {
    expect(paypalSuccessModule).toBeTruthy();
  });
});
