import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BaseUrl, DataService } from '../../mts.service';

@Component({
  selector: 'app-paypal-success',
  templateUrl: './paypal-success.component.html',
  styleUrls: ['./paypal-success.component.css']
})
export class PaypalSuccessComponent implements OnInit {
  categoryId: string;
  Price: any;
  eTRFNo: any;
  isLoader: boolean;
  invoiceFileName: string;
  // MailInsp: any;
  ngOnInit(): void { }

  public message: string = 'Payment Processing ... ';
  constructor(
    private router: Router, private activatedRoute: ActivatedRoute,
    private dataService: DataService) {
    (<any>$('[data-toggle="tooltip"]')).tooltip();
    this.categoryId = localStorage.getItem('CategoryId');
    // this.MailInsp = this.dataService.ecommInspectionSendEmail;
    /**
     * Added by Satyam Tyagi, payment require.
     */
    this.activatedRoute.queryParams.subscribe(params => {
      //http://localhost:3000/#/paypalSuccess?paymentId=PAY-69H88013FT309984WLJJFU6A&token=EC-2SG594528L921910X&PayerID=VQTPJ7D6DGBK4
      let paymentId = params['paymentId'];
      let token = params['token'];
      let PayerID = params['PayerID'];
      var EtrfOldID = localStorage.getItem('ecommerceEtrfId');
      var that = this;
      that.isLoader = true;
      this.dataService
        .get(
          'Payment/ProcessPayment?paymentId=' + paymentId + '&token=' +
          token + '&PayerID=' + PayerID + '&EtrfOldID=' + EtrfOldID)
        .subscribe(response => {
          localStorage.removeItem('ServiceId');
          localStorage.removeItem('ServiceName');
          localStorage.removeItem('ecommerceEtrfId');
          localStorage.removeItem('ecommerceStatus');
          window.history.pushState(null, '', window.location.href);
          window.onpopstate = function () {
            window.history.pushState(null, '', window.location.href);
          };
          if (response.IsSuccess) {
            that.dataService.ready = false;
            this.message = response.Message;
            that.Price = response.Data.Price;
            that.eTRFNo = response.Data.EtrfNo;
            that.invoiceFileName = response.Data.InvoiceFileName;
            if (!localStorage.getItem('orderReviewRequest')) {
              that.isLoader = false;
            } else {
              this.updateRequestStatus();
            }
          } else {
            that.isLoader = false;
            that.dataService.ready = false;
            this.router.navigate(['/paypal/paypal-cancel']);
          }
        });
    });

    /**
     * Added by Satyam Tyagi, no need for payment.
     */

    // this.activatedRoute.params.subscribe(params => {
    //   let paymentId = params['paymentId'];
    //   let token = params['token'];
    //   let PayerID = params['PayerID'];
    //   let Amount = params['amount'];
    //   var EtrfOldID = localStorage.getItem('ecommerceEtrfId');
    //   var that = this;
    //   that.isLoader = true;
    //   this.dataService
    //     .get(
    //       'Payment/ProcessPaymentTest?paymentId=' + paymentId +
    //       '&token=' + token + '&PayerID=' + PayerID +
    //       '&EtrfOldID=' + EtrfOldID + '&Amount=' + Amount)
    //     .subscribe(response => {
    //       localStorage.removeItem('ServiceId');
    //       localStorage.removeItem('ServiceName');
    //       localStorage.removeItem('ecommerceEtrfId');
    //       localStorage.removeItem('ecommerceStatus');
    //       window.history.pushState(null, '', window.location.href);
    //       window.onpopstate = function () {
    //         window.history.pushState(null, '', window.location.href);
    //       };
    //       if (response.IsSuccess) {
    //         that.dataService.ready = false;
    //         this.message = response.Message;
    //         that.Price = response.Data.Price;
    //         that.eTRFNo = response.Data.EtrfNo;
    //         that.invoiceFileName = response.Data.InvoiceFileName;
    //         if (!localStorage.getItem('orderReviewRequest')) {
    //           that.isLoader = false;
    //         } else {
    //           this.updateRequestStatus();
    //         }
    //       } else {
    //         that.isLoader = false;
    //         that.dataService.ready = false;
    //         this.router.navigate(['/paypal/paypal-cancel']);
    //       }
    //     });
    // });
  }

  PrintEtrf(e) {
    window.location.href = BaseUrl +
      'ManageDocuments/DownloadFile?FileName=ETRFPDF\\' + e + '.pdf&fileType=PDF&businessCategoryId=' + this.categoryId;
  }

  TrackOrder() {
    this.router.navigate(['/online-etrf']);
  }
  PrintInvoice(e) {
    window.location.href = BaseUrl +
      'ManageDocuments/DownloadSampleFile?Folder=INVOICEECOMPDF&FileName=' + e ;
  }

  updateRequestStatus() {
    this.dataService
      .get(
        'RequestAddtionalTestItem/UpdateLinkStatus/' +
        (JSON.parse(localStorage.getItem('orderReviewRequest'))
          .EncryptString))
      .subscribe(response => {
        if (response.IsSuccess) {
          this.isLoader = false;
        } else {
          this.isLoader = false;
        }
      })
  }
}
