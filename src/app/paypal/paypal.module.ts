import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaypalRoutingModule } from './paypal-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PaypalRoutingModule
  ],
  declarations: []
})
export class PaypalModule { }
