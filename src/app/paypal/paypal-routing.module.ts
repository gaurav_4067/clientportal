import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'paypal-success',
      loadChildren: () => new Promise(
          resolve => {
              (require as any)
                  .ensure(
                      [],
                      require => {
                        resolve(
                            require('./paypal-success/paypal-success.module')
                                .PaypalSuccessModule);
                      })})
    },
    {
      path: 'paypal-success/:paymentId/:token/:PayerID/:amount',
      loadChildren: () => new Promise(
          resolve => {
              (require as any)
                  .ensure(
                      [],
                      require => {
                        resolve(
                            require('./paypal-success/paypal-success.module')
                                .PaypalSuccessModule);
                      })})
    },
    {
      path: 'paypal-cancel',
      loadChildren: () => new Promise(
          resolve => {(require as any)
                          .ensure(
                              [],
                              require => {
                                resolve(
                                    require(
                                        './paypal-cancel/paypal-cancel.module')
                                        .PaypalCancelModule);
                              })})

    }
  ]
}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class PaypalRoutingModule {
}
