import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaypalCancelRoutingModule } from './paypal-cancel-routing.module';
import { PaypalCancelComponent } from './paypal-cancel.component';

@NgModule({
  imports: [
    CommonModule,
    PaypalCancelRoutingModule
  ],
  declarations: [PaypalCancelComponent]
})
export class PaypalCancelModule { }
