import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PaypalCancelComponent} from './paypal-cancel.component';

const routes: Routes = [{path: '', component: PaypalCancelComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class PaypalCancelRoutingModule {
}
