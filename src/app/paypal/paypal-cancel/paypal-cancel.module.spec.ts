import { PaypalCancelModule } from './paypal-cancel.module';

describe('PaypalCancelModule', () => {
  let paypalCancelModule: PaypalCancelModule;

  beforeEach(() => {
    paypalCancelModule = new PaypalCancelModule();
  });

  it('should create an instance', () => {
    expect(paypalCancelModule).toBeTruthy();
  });
});
