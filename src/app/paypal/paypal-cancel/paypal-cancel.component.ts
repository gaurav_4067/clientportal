import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../../mts.service';

@Component({
  selector: 'app-paypal-cancel',
  templateUrl: './paypal-cancel.component.html',
  styleUrls: ['./paypal-cancel.component.css']
})
export class PaypalCancelComponent implements OnInit {
  ngOnInit(): void {
    window.history.pushState(null, '', window.location.href);
    window.onpopstate = function() {
      window.history.pushState(null, '', window.location.href);
    };
  }

  constructor(
      private activatedRoute: ActivatedRoute,
      private dataService: DataService) {
    this.activatedRoute.queryParams.subscribe(params => {
      // http://localhost:3000/#/paypalSuccess?paymentId=PAY-69H88013FT309984WLJJFU6A&token=EC-2SG594528L921910X&PayerID=VQTPJ7D6DGBK4
      let token = params['token'];
      if (token != '' && token != undefined) {
        this.dataService.get('Payment/CancelPayment?token=' + token)
            .subscribe(response => {});
      } else {
      }
    });
  }
}
