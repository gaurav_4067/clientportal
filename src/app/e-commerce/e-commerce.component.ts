import { ChangeDetectorRef, Component, EventEmitter, OnInit, ViewChild, Pipe, Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from '../mts.service';

import { OrderReviewComponent } from './services/services-group/service/package/order-review/order-review.component';
import { PackageComponent } from './services/services-group/service/package/package.component';
import { EcomPromoOfferList } from 'app/e-commerce/services/EcomPromoOfferModel';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';

declare let $: any;
declare let swal: any;

@Component({
  selector: 'app-e-commerce',
  templateUrl: './e-commerce.component.html',
  styleUrls: ['./e-commerce.component.css']
})
export class ECommerceComponent implements OnInit {
  promoCode: string;
  selectedServiceText: string;
  @ViewChild(OrderReviewComponent) orderReviewComponent: OrderReviewComponent;
  @ViewChild(PackageComponent) packageComponent: PackageComponent;
  @ViewChild('etrf') etrf;
  public EtrfSave = new EventEmitter();
  userCredential: any;
  etrfIdOnSave: any;
  userDetail: any;
  DynamicHtml: any;
  isLoader: boolean;
  entitledOfferList: Array<EcomPromoOfferList>;
  offerButtonText: string = "Apply Offer";
  Enquiry = { 'EnquiryBy': 0, 'Subject': '', 'Comments': '', 'EnquiryType': '' }

  subjectModal: any;
  commentsModal: any;

  data: any = { ready: false, devisionId: 0, showSelectPackages: false, Flag: 1 };
  userDetails: any;
  constructor(
    private cdRef: ChangeDetectorRef, private router: Router,
    private dataService: DataService) {
    this.selectedServiceText = this.dataService.selectedServiceText;
    this.entitledOfferList = new Array<EcomPromoOfferList>();
  }

  public calculateOrderTotal() {
    this.orderReviewComponent.calculatePrice(
      this.dataService.selectedPackages[0].PackageType);
    (<any>$('#EtrfModal')).modal('hide');
  }

  public resetSelection() {
    this.data.ready = false;
    this.dataService.ready = false;
    this.packageComponent.resetSelection();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetails = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetails) {
      if (this.userCredential.UserPermission.ViewECommerce) {
        (<any>$('[data-toggle="tooltip"]')).tooltip();
        this.data.Flag = 3;
        this.data.Flag = 1;
        if (window.location.href.includes('e-commerce-home')) {
          window.location.href =
            window.location.href.replace('e-commerce-home', 'e-commerce');
        }
        this.dataService.registeredCompanyName = this.userCredential.CompanyName;
        if (this.userCredential.CompanyId == 10000) {
          this.dataService
            .get('User/GetCompanyName?UserId=' + this.userCredential.UserId)
            .subscribe(response => {
              if (response.IsSuccess) {
                this.dataService.registeredCompanyName = response.Data;
              }
            });
        }

        // this.GetAllEntitledPromoOffer();


        localStorage.setItem('ecommerceStatus', 'true');
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
    // (<any>$('#Form-Header')).css('border', '1px solid #ccc');
    // (<any>$('.mts-logo')).css('border-right', '1px solid #ccc');
    // (<any>$('.section-etrf')).css('border', '1px solid #ccc');
    // (<any>$('.form-control')).css({ 'border': '1px solid #ccc', 'color':
    // '#555' });
    // (<any>$('.field-label')).css({ 'font-size': '16px', 'color': '#555' });
    // (<any>$('.sub-sectionHeader-etrf')).css({ 'font-size': '14px',
    // 'border-bottom': '1px solid #ccc', 'color': '#555' });
    // (<any>$('.section-header-etrf')).css({ 'font-size': '14px',
    // 'border-bottom': '1px solid #ccc', 'color': '#555' });
    // (<any>$('.radio-container input')).css('border', '1px solid #ccc');
    // (<any>$('.radio-container span')).css({ 'font-size': '14px', 'color':
    // '#555' });
    // (<any>$('.checkbox-label')).addClass('onPdf');
    // (<any>$('.checkbox-label')).css({ 'font-size': '14px', 'color': '#555'
    // });
    // (<any>$('.hide-pdf')).css('display', 'none');
    // (<any>$('.readonly')).css('display', 'block');
    // (<any>$('.pdf')).css('display', 'none');
  }

  onLoadFunc(iframe: any) {
    //<any>("header")).hidden();
    $('#IframeE-trf custom-header').css('display', 'none');
    // var head = $("#IframeE-trf").contents().find("head");
    // var css = '<style display: none;></style>';
    // $(head).append(css);
    // alert("rohit");
  }

  GetAllEntitledPromoOffer() {
    let that = this;
    let request = { UserId: this.userCredential.UserId, currentDate: this.getFormattedDate(new Date()) };
    this.dataService.post('ManageOffer/GetAllEntitledOffer', request).subscribe(
      response => {
        if (response.IsSuccess) {
          that.data.baseEntitledOfferList = response.Data;
          that.data.entitledOfferList = response.Data;
        }
      });
  }

  getFormattedDate(date: Date) {
    return ((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear());
  }

  EcommerceTrfId() {
    var that = this;
    setTimeout(function () {
      var EcommerceId = localStorage.getItem('ecommerceEtrfId');
      if (EcommerceId == null) {
        that.EcommerceTrfId();
      } else {
        that.etrfIdOnSave = EcommerceId;
        (<any>$('#etrf-continue')).trigger('click');
        //(<any>$('#EtrfModal')).modal('show');
      }
    }, 500);
  }



  // Send Enquiry By Modals
  public sendEnquiry() {
    this.isLoader = true;
    var that = this;
    var that = this;
    var EnquiryBy = this.userDetails.UserId;
    if (EnquiryBy) {
      this.Enquiry.EnquiryBy = this.userDetails.UserId;
    }
    if (this.subjectModal) {
      this.Enquiry.Subject = this.subjectModal;
    } else {
      this.isLoader = false;
      swal('', 'Please enter subject.');
      return;
    }

    if (this.commentsModal) {
      this.Enquiry.Comments = this.commentsModal;
    } else {
      this.isLoader = false;
      swal('', 'Please enter comments.');
      return;
    }

    this.dataService.post('Package/SendEnquiry', this.Enquiry)
      .subscribe(response => {
        if (response.IsSuccess) {
          swal('', response.Message);
          that.isLoader = false;
          (<any>$('#Enquirysubject')).val('');
          (<any>$('#Enquirycomments')).val('');
          that.commentsModal = '';
          that.subjectModal = '';
          $('#EnquiryForm').modal('hide');
        } else {
          swal('', response.Message);
          that.isLoader = false;
        }
      });
  }



  createHtml() {
    if (this.dataService.EcommPackageDetails.IsPackages == true) {
      var TestDetails = '<ul>';
      this.DynamicHtml =
        '<div style="font-weight:bold;border: 3px solid #333;margin: 0 auto;padding: 15px;width: 100% !important;"><h3><b>Package Selected</b></h3><hr style="border-top: 2px solid #333;"/>';
      this.DynamicHtml = this.DynamicHtml + '<h4><b>' +
        this.dataService.EcommPackageDetails.PackageName + '</b></h4>';
      this.dataService.EcommPackageDetails.PackageTestName.forEach(element => {
        TestDetails = TestDetails + '<li>' + element + '</li>';
      });
      TestDetails = TestDetails + '</ul>';
      this.DynamicHtml = this.DynamicHtml + TestDetails + '</div>';
    } else {
      var TestDetails = '<ul>';
      this.DynamicHtml =
        '<div style="font-weight:bold;border: 3px solid #333;margin: 0 auto;padding: 15px;width: 100% !important;"><h3><b>test Selected</b></h3><hr style="border-top: 2px solid #333;"/>';
      // this.DynamicHtml = '<div
      // style="font-weight:bold;border:solid"><h3><b>test Selected</b></h3>';
      this.dataService.EcommPackageDetails.IndividualTestItems.forEach(
        element => {
          TestDetails = TestDetails + '<li>' + element + '</li>';
        });
      TestDetails = TestDetails + '</ul>';

      this.DynamicHtml = this.DynamicHtml + TestDetails + '</div>';
    }
    if (this.dataService.EcommPackageUniqueId.length > 1) {
      (<any>$('#' + this.dataService.EcommPackageUniqueId))
        .find('.field-label')
        .html(this.DynamicHtml);
      $('#' + this.dataService.EcommApplicatntAndLabInfo)
        .find('.field-label')
        .html($('#ETRFApplicantInfo').html());
      $('#' + this.dataService.EcommApplicatntAndLabInfo)
        .find('.field-label')
        .css('width', '100%');
    }
  }

  saveEtrf() {
    // (<any>$('.pdf')).css('display', 'block');
    this.dataService.ready = true;
    this.createHtml();
    localStorage.removeItem('ecommerceEtrfId');
    this.etrf.saveETRF('ecommerce');
    this.EcommerceTrfId();
    this.data.ready = true;
  }


  openOfferPopUp() {
    (<any>$('#OfferFormsssssss')).modal('show');
  }

  closeOfferPopUp() {
    (<any>$('#OfferForm')).modal('hide');
    this.data.ShowApplyButton = false;
  }

  applyDiscount(index) {
    let selectedOffer = this.data.entitledOfferList[index];
    let ids = selectedOffer.BusinessCategoryId.split(',');

    if (ids.includes(this.data.selectedServiceInfo.categoryId.toString())) {
      this.data.SelectedOffer = this.data.entitledOfferList[index];
      this.data.offerApplied = true;
      this.data.offerPercent = this.data.entitledOfferList[index].OfferPercentDiscount;
      this.data.finalDueAmount = this.data.total / 100 * this.data.offerPercent;
      (<any>$('#OfferForm')).modal('hide');
      this.data.ShowApplyButton = false;
    }
    else {
      swal("", "This offer is only valid for " + selectedOffer.BusinessCategoryName + " services.")
    }
  }

  validatePromoOfferField(evt) {
    var keycode = evt.charCode || evt.keyCode;

    if (!!this.promoCode) {
      this.promoCode = this.promoCode.trim();
      this.promoCode = this.promoCode.replace(/  +/g, '');
    }
  }

  validatePromoCode() {
    if (this.promoCode) {
      this.isLoader = true;
      let request = { UserId: this.userCredential.UserId, CurrentDate: this.getFormattedDate(new Date()), PromoCode: this.promoCode };
      this.dataService.post("ManageOffer/ValidatePromoCode", request).subscribe(res => {
        if (res.IsSuccess) {
          let selectedOffer = res.Data;
          if (selectedOffer != null) {
            
            let ids = selectedOffer.BusinessCategoryId.split(',');

            if (ids.includes(this.data.selectedServiceInfo.categoryId.toString())) {
              this.data.SelectedOffer = selectedOffer;
              this.data.offerApplied = true;
              this.data.offerPercent = selectedOffer.OfferPercentDiscount;
              this.data.finalDueAmount = this.data.total / 100 * this.data.offerPercent;
              (<any>$('#ApplyCodeForm')).modal('hide');
              this.promoCode = "";
              //this.data.ShowApplyButton = false;
            }
            else {
              swal("", "This offer is only valid for " + selectedOffer.BusinessCategoryName + " services.")
            }

          }
          else {
            swal("", res.Message);

          }
          this.isLoader = false;
        }
        else {
          swal('', res.Message);
          this.isLoader = false;
        }
      })
    }
    else {
      swal('', "Please enter any promo code");
    }
  }

  resetModel() {
    this.promoCode = "";
  }
}
