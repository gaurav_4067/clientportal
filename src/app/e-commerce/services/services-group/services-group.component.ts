import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { DataService } from '../../../mts.service';
declare let swal: any;

@Component({
  selector: 'app-services-group',
  templateUrl: './services-group.component.html',
  styleUrls: ['./services-group.component.css']
})
export class ServicesGroupComponent implements OnInit {
  @Input() offerDescriptionIndex: any;  
  @Input() group: any;
  @Input() data: any;
  @Output() groupSelected = new EventEmitter();
  userDetail: any;
  Enquiry = { 'EnquiryBy': 0, 'Subject': '', 'Comments': '', 'EnquiryType': '' };
  subjectModal: any;
  commentsModal: any;
  constructor(public dataService: DataService) { }


  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.group.services.forEach(element => {
      if (element.OfferActive) {
        this.group.showOfferHeader = true;
        this.group.OfferDescription = element.OfferDescription;
      }
    });
  }

  public selectGroup(group) {
    if (!group.isSelected) {
      this.groupSelected.emit(group.id);
    }
  }
  public sendEnquiry() {
    var that = this;
    var EnquiryBy = this.userDetail.UserId;
    if (EnquiryBy) { this.Enquiry.EnquiryBy = this.userDetail.UserId; }
    if (this.subjectModal) {
      this.Enquiry.Subject = this.subjectModal;
    }
    else {

      swal('', "Please enter subject.");
      return;
    }

    if (this.commentsModal) {
      this.Enquiry.Comments = this.commentsModal;
    }
    else {

      swal('', "Please enter comments.");
      return;
    }

    this.dataService.post('Package/SendEnquiry', this.Enquiry).subscribe(
      response => {
        if (response.IsSuccess) {
          swal('', response.Message);

          (<any>$('#Enquirysubject')).val('');
          (<any>$('#Enquirycomments')).val('');
          (<any>$('#ServiceEnquiry')).modal('hide');
        } else {
          swal('', response.Message);

        }
      }
    );
  }
  closeEnquiry() {

    this.subjectModal = "";
    this.commentsModal = "";
    (<any>$('#ServiceEnquiry')).modal('hide');

  }

  displayApplyButton(isDisplay) { 
    this.data.ShowApplyButton = isDisplay;
  }
}
