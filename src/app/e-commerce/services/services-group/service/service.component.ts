import {Component, Input, OnInit} from '@angular/core';

import {DataService} from '../../../../mts.service';


declare let swal: any;
@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})

export class ServiceComponent implements OnInit {
  @Input() mtsService: any;
  
  @Input() data: any;
  CountryOfDestination: any;
  AgeGrade: any;
  ProductType: any;
  Data: any = {
    "ServiceID": 1,
    "CountryID": 1,
    "AgeGroupID": 1,
    "ProductTypeID": 1
  };
  constructor(public dataService: DataService) { }

  ngOnInit() {

  }
  FilterPackage(mtsService) {
    if (mtsService.categoryId == 1) {
      this.dataService.selectedServiceText = "Select Testing Packages / Individual Test Items";
    }
    else if (mtsService.categoryId == 2) {
      this.dataService.selectedServiceText = "Select Inspection Product";
    }
    else if (mtsService.categoryId == 3) {
      this.dataService.selectedServiceText = "Select Audit Packages / Individual Test Items";
    }
    var that = this;
    this.data.showSelectPackages = true;
    this.data.selectedServiceInfo = mtsService;
    // this.dataService.get('Package/ServiceBasedPackage?serviceId=' + mtsService.id).subscribe(
    //   response => {
    //     if (response.IsSuccess) {
    //       this.AgeGrade = response.Data.Age;
    //       this.CountryOfDestination = response.Data.Country;
    //       this.ProductType = response.Data.Product;
    //       that.dataService.post('Package/PackageFiltered', that.data).subscribe(
    //         response => {
    //           
    //         }
    //       );

    //     }
    //     else {
    //       swal('', response.Message)
    //     }
    //   }
    // );
    // alert(mtsService.id);
    localStorage.setItem('ServiceId', JSON.stringify(mtsService.id));
    localStorage.setItem('CategoryId', JSON.stringify(mtsService.categoryId));
    this.data.DivisionID = mtsService.id;
    // use for Packages title with Service Name
    localStorage.setItem('ServiceName', JSON.stringify(mtsService.name));
    this.data.DivisionName = mtsService.name;
    localStorage.removeItem('ecommerceEtrfId');
  }
}
