import { Component, Input, OnInit, DoCheck } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../../../../../mts.service';

declare let swal: any;

@Component({
  selector: 'app-order-review',
  templateUrl: './order-review.component.html',
  styleUrls: ['./order-review.component.css']
})
export class OrderReviewComponent implements OnInit, DoCheck {
  offerPercent: any;
  offerApplied: boolean = false;
  categoryId: any;
  qtyText: string;
  priceText: string;
  @Input() data;
  total: number = 0.00;
  grandTotal: number = 0.00;
  tax: number = 0.00;
  termandcond: any = false;
  userOrderDetails: {
    CompanyName: '',
    FirstName: '',
    LastName: '',
    Address: '',
    mobile: '',

  };
  userDetails: any;
  disablePayment: any;
  EcommerceId: any;
  isLoader: boolean;
  paymentObject: any = {
    'packages': [],
    'serviceId': '',
    'tempETRFId': '',
    'itemsInformation': {}
  };
  headerText: any;
  colorText: any = '';
  count: any;
  public deleteColorwayQty(event, colorWayToDelete) {    
    var count = 0;
    for (var i = 0; i < this.dataService.selectedPackages.length; i++) {
      var colorTotal = (this.dataService.selectedPackages[i].ColorwayPrice * this.dataService.selectedPackages[i].ColorwayQty)
      // var PackageTotalPrice = this.dataService.selectedPackages[i].Price - colorTotal;
      this.dataService.selectedPackages[i].FinalPrice = this.dataService.selectedPackages[i].Price;//PackageTotalPrice;
    }
    colorWayToDelete.ColorwayQty = 0;
    colorWayToDelete.ColorwayPrice = 0;
    for (var i = 0; i < this.dataService.selectedPackages.length; i++) {
      if (this.dataService.selectedPackages[i].ColorwayQty > 0) {
        count = count + 1;
      }
    }
    if (count == 0) {
      this.colorText = false;
    }
    this.total = 0.00;
    this.calculatePrice(this.dataService.selectedPackages[0].PackageType);
    //colorWayToDelete.ColorwayQty = 0;
    //   this.disablePayment = true;
  }

  public deletePackage(event, pakageToDelete) {

    var thisObj = this;
    swal({
      title: "",
      text: "<span style='color:#ef770e;font-size:20px;'>Are you sure to delete this package ?</span><br><br>",
      html: true,
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Proceed",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
      function (isConfirm) {

        if (isConfirm) {
          if (thisObj.dataService.selectedPackages.length == 1) {
            thisObj.dataService.get('Package/CancelOrderedPackages?E_TRFID=' + localStorage.getItem('ecommerceEtrfId')).subscribe(
              response => {

                if (response.IsSuccess) {
                  thisObj.dataService.selectedPackages = thisObj.dataService.selectedPackages.filter(x => x.Sno != pakageToDelete.Sno)
                  thisObj.reset();
                  if (thisObj.dataService.selectedPackages.length > 0) {
                    thisObj.calculatePrice(this.dataService.selectedPackages[0].PackageType);
                  }
                  // this.disablePayment = true;

                  if (thisObj.dataService.selectedPackages.length == 0) {

                    thisObj.disablePayment = true;
                  }
                }

              }
            );
          }
          else {
            thisObj.dataService.selectedPackages = thisObj.dataService.selectedPackages.filter(x => x.Sno != pakageToDelete.Sno)
            thisObj.reset();
            thisObj.calculatePrice(this.dataService.selectedPackages[0].PackageType);
          }
        } else {
        }
      });
  }

  public calculatePrice(packageType: string) {
    this.total = 0.00;
    if (packageType == "P" || packageType == "T") {
      try {
        for (var i = 0; i < this.dataService.selectedPackages.length; i++) {

          var colorTotal = (this.dataService.selectedPackages[i].ColorwayPrice * this.dataService.selectedPackages[i].ColorwayQty)
          var PackageTotalPrice = (this.dataService.selectedPackages[i].Price * this.dataService.selectedPackages[i].PackageQty) + colorTotal;
          this.total = this.total + PackageTotalPrice;
          this.dataService.selectedPackages[i].FinalPrice = PackageTotalPrice;
        }
        this.tax = 0.0 * this.total;
        this.grandTotal = this.total + this.tax;
      }
      catch (ex) { console.log(ex); }
    }
    else if (packageType == "I") {
      this.total = this.dataService.selectedPackages[0].TotalCost;
      this.tax = 0.0 * this.total;
      this.grandTotal = this.total + this.tax;
    }
    this.data.total = this.total;
  }

  public InitiatePayment() {
    var that = this;
    that.isLoader = true;
    if (!that.data.SelectedOffer) {
      that.data.SelectedOffer = {};
      that.data.SelectedOffer.OfferDetailId = 0;
    }
    if (that.data.categoryId == 1) {
      that.paymentObject.packages = that.dataService.selectedPackages.map(x => {
        return {
          "PackageId": x.Id, "Type": x.PackageType, "LocationId": x.SelectedLocation,
          "AgeGrade": x.AgeGrade, "CountryID": x.CountryID, "ProductType": x.ProductType,
          "ServiceID": x.ServiceID, "ColorwayQty": x.ColorwayQty, "Quantity": x.PackageQty,
          "ServicePackageMappingId": x.ServicePackageMappingId, "ItemMappingId": x.ItemMappingId,
          "OfferId": that.data.SelectedOffer.OfferDetailId
        };
      });
      that.paymentObject.serviceId = that.dataService.selectedService.id;
      that.paymentObject.tempETRFId = localStorage.getItem('ecommerceEtrfId');
      that.paymentObject.itemsInformation = [];
    }
    if (that.data.categoryId == 2) {
      that.paymentObject.packages = that.dataService.selectedPackages.map(x => {
        return {
          "PackageId": x.ProductId, "Type": x.PackageType, "LocationId": x.SelectedLocation,
          "AgeGrade": 0, "CountryID": x.SelectedCountry, "ProductType": 0,
          "ServiceID": that.data.categoryId, "ColorwayQty": 0, "Quantity": 1,
          "ServicePackageMappingId": 0, "ItemMappingId": 0,
          "OfferId": that.data.SelectedOffer.OfferDetailId
        }
      });
      that.paymentObject.serviceId = that.dataService.selectedService.id;
      that.paymentObject.tempETRFId = localStorage.getItem('ecommerceEtrfId');
      that.paymentObject.itemsInformation = this.dataService.selectedPackages;
    }

    /**
    * Added by Satyam Tyagi, payment require.  
    */
    that.dataService.post("Payment/InitiatePayment", that.paymentObject).subscribe(response => {
      if (response.IsSuccess) {
        window.location.href = response.Data;
        that.isLoader = false;
      }
      else {

        that.router.navigate(["/paypal/paypal-cancel"]);
        that.isLoader = false;
      }

    });

    /**
    * Added by Satyam Tyagi, no need for payment.  
    */
    // that.dataService.post("Payment/InitiatePaymentTest", that.paymentObject).subscribe(response => {
    //   if (response.IsSuccess) {
    //     var paymentId = "PAY-69H88013FT309984WLJJFU6A";
    //     var token = response.Data.token;
    //     var PayerID = "VQTPJ7D6DGBK4";
    //     var amount = response.Data.amount
    //     that.isLoader = false;
    //     this.router.navigate(['/paypal/paypal-success/' + paymentId + '/' + token + '/' + PayerID + '/' + amount]);
    //   }
    //   else {

    //     that.router.navigate(["/paypal/paypal-cancel"]);
    //     that.isLoader = false;
    //   }

    // });
  }

  public reset() {
    this.total = 0.0;
    this.tax = 0.0;
    this.grandTotal = 0.0;
  }
  public backClick() {
    this.data.offerApplied = false;
    this.data.SelectedOffer = {};
    var that = this;
    that.dataService.ready = false;
    //this.data.ready=true   
    that.reset();
    if (that.dataService.selectedPackages.length == 0) {
      location.reload();
    }

  }

  public openTerms() {
    (<any>$('#myModalTerms')).modal('show');
  }
  public termsClick(event) {

    if (event.target.checked && this.grandTotal > 0) {
      this.disablePayment = false;
    } else {
      this.disablePayment = true;
    }

  }

  constructor(public router: Router, public dataService: DataService) {
    this.dataService = dataService;
  }

  ngDoCheck() {
    if (this.dataService.selectedPackages.length > 0) {
      if (this.dataService.selectedPackages[0].PackageType == "P") {
        this.calculatePrice(this.dataService.selectedPackages[0].PackageType);
        this.headerText = "Package (s)"
        this.qtyText = "Package (s) Qty";
        this.priceText = "Package (s) Price (USD)";
        this.count = this.dataService.selectedPackages.filter(x => x.ColorwayQty > 0);

        if (this.count != undefined && this.count.length > 0) {
          this.colorText = "Color";
        } else {
          this.colorText = false;
        }
      }
      if (this.dataService.selectedPackages[0].PackageType == "T") {
        this.calculatePrice(this.dataService.selectedPackages[0].PackageType);
        this.headerText = "Test (s) Name"
        this.qtyText = "Test (s) Qty";
        this.priceText = "Test (s) Price (USD)";
        this.count = this.dataService.selectedPackages.filter(x => x.ColorwayQty > 0);

        if (this.count != undefined && this.count.length > 0) {
          this.colorText = "Color";
        } else {
          this.colorText = false;
        }
      }
      if (this.dataService.selectedPackages[0].PackageType == "I") {
        this.calculatePrice(this.dataService.selectedPackages[0].PackageType);
        this.headerText = 'Product';
        this.qtyText = 'Sample Size';
        this.colorText = false;
      }
      if (this.dataService.modalOpened == 'termsOfUse') {
        (<any>$('#termsOfUseOrder')).modal('show');
      }
      if (this.dataService.modalOpened == 'privacyPolicy') {
        (<any>$('#privacyPolicyOrder')).modal('show');
      }
    }
    var that = this;
    (<any>$('#flux')).bind('scroll', function () {

      if ((<any>$(this)).scrollTop() + (<any>$(this)).innerHeight() >= (<any>$(this)[0]).scrollHeight - 50) {
        // that.termandcond = true;
        (<any>$('#termsCondition')).attr('disabled', false);
      }
    })


    if (this.data.entitledOfferList) {

      if (this.data.entitledOfferList.length == 1 && this.data.selectedServiceInfo) {
        let selectedOffer = this.data.entitledOfferList[0];
        if (this.data.total >= selectedOffer.MinTransactionAmount) {
          if (!this.data.entitledOfferList[0].IsOfferRedeemed) {
            //  let selectedOffer = this.data.entitledOfferList[0];
            let ids = selectedOffer.BusinessCategoryId.split(',');

            if (ids.includes(this.data.selectedServiceInfo.categoryId.toString())) {
              this.data.SelectedOffer = this.data.entitledOfferList[0];
              this.data.offerApplied = true;
              this.data.offerPercent = this.data.entitledOfferList[0].OfferPercentDiscount;
              this.data.finalDueAmount = this.data.total / 100 * this.data.offerPercent;
            }
          }
        }
      }
    }
  }

  ngOnInit() {
    (<any>$('[data-toggle="tooltip"]')).tooltip();
    this.userDetails = JSON.parse(localStorage.getItem('userDetail'));
    this.categoryId = JSON.parse(localStorage.getItem('CategoryId'));
    this.disablePayment = true;
    this.EcommerceId = localStorage.getItem('ecommerceEtrfId');



  }

  restrictUser(event) {

    event.preventDefault();
  }

  restrictBackspace(event) {
    if (event.keyCode === 8) {
      return false;
    }
  }

  evaluateOffer() {
    if (this.data.SelectedOffer) {
      if (this.data.total <= this.data.SelectedOffer.MinTransactionAmount) {
        this.data.offerApplied = false;
        // this.data.ShowApplyButton = true;

        this.data.SelectedOffer = {};
        this.data.SelectedOffer.OfferDetailId = 0;
      }
    }
    // else {
    //   this.data.offerApplied = false;
    //   this.data.SelectedOffer = {};

    // }
  }
  closeModal() {
    this.dataService.modalOpened = "";
  }

  // openOfferPopUp() {
  //   (<any>$('#OfferForm')).modal('show');
  //   this.data.ShowApplyButton = true;
  // }

  openOfferPopUp() {
    (<any>$('#ApplyCodeForm')).modal('show');
    // this.data.ShowApplyButton = true;
  }

  closeOfferPopUp() {
    (<any>$('#OfferForm')).modal('hide');
    this.data.ShowApplyButton = false;
  }
}
