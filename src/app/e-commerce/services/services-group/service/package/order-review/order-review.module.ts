import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {OrderReviewRoutingModule} from './order-review-routing.module';
import {OrderReviewComponent} from './order-review.component';
import {TooltipModule} from "ngx-tooltip";

@NgModule({
  imports: [CommonModule, FormsModule,TooltipModule, OrderReviewRoutingModule],
  declarations: [OrderReviewComponent],
  exports: [OrderReviewComponent]
})
export class OrderReviewModule {
}
