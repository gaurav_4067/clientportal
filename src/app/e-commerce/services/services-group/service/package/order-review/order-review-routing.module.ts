import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {OrderReviewComponent} from './order-review.component';

const routes: Routes =
    [{path: 'order-review-orignal', component: OrderReviewComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class OrderReviewRoutingModule {
}
