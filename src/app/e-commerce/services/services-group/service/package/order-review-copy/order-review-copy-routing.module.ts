import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {OrderReviewCopyComponent} from './order-review-copy.component';

const routes: Routes = [{path: '', component: OrderReviewCopyComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class OrderReviewCopyRoutingModule {
}
