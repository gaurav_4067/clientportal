export class RequestData {
  public EncryptString: string;
  public UserId: string;
}

export class RequestAddtionalTestItem {
  public RequestAddtionalTestItem() {
    this.ApplicantInfo = new ApplicantInfo();
    this.LabInfo = new LabAddressInfo();
    this.TestItems = new Array<PackageTest>();
  }
  public PackageOrderId: number;
  public UserId: number;
  public ServiceId: number;
  public Temp_TRF_ID: string;
  public Status: string;
  public LocationId: number;
  public ApplicantInfo: ApplicantInfo;
  public LabInfo: LabAddressInfo;
  public TestItems: Array<PackageTest>;
  public Remarks: string;
  public Message: string;
}

export class ApplicantInfo {
  public Name: string;
  public CompanyName: string;
  public Address1: string;
  public Address2: string;
  public Phone: string;
  public Email: string;
  public Fax: string;
  public City: string;
  public State: string;
  public Country: string;
}

export class LabAddressInfo {
  public Name: string;
  public Address1: string;
  public Address2: string;
  public Phone: string;
  public Email: string;
  public Fax: string;
  public CityStateCountry: string;
  public CompanyLogo: string;
}

export class PackageLocation {
  public GroupId: number;
  public CountryDescription: string;
  public CountryCode: string;
  public Address1: string;
  public Address2: string;
  public City: string;
  public Street: string;
  public State: string;
  public ZipCode: string;
  public Phone: string;
  public Fax: string;
  public Mob: string;
  public Email: string;
  public PackagePrice: number;
  public ColorwayPrice: number;
  public IsDefault: boolean;
}

export class PackageTest {
  public Sno: number;
  public Id: number;
  public Code: string;
  public Name: string;
  public Price: number;
  public Quantity: number;
  public PackageType: string;
  public Locations: Array<PackageLocation>;
  public SelectedLocation: number;
  public AgeGrade: number;
  public ProductType: number;
  public ServiceID: number;
  public CountryID: number;
  public ServiceName: string;
  public ColorwayQty: number;
  public ColorwayPrice: number;
  public ServicePackageMappingId: number;
  public FinalPrice: number;
  public LabId: number;
  public ItemMappingId: number;
  public AgeGradeName: string;
  public CountryCode: string;
}