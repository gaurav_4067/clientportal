import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {ECommerceHeaderModule} from '../../../../../../e-commerce-header/e-commerce-header.module';
import {FooterModule} from '../../../../../../footer/footer.module';

import {OrderReviewCopyRoutingModule} from './order-review-copy-routing.module';
import {OrderReviewCopyComponent} from './order-review-copy.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, ECommerceHeaderModule, FooterModule,
    OrderReviewCopyRoutingModule
  ],
  declarations: [OrderReviewCopyComponent]
})
export class OrderReviewCopyModule {
}
