import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {DataService} from '../../../../../../mts.service';

import {RequestAddtionalTestItem, RequestData} from './order-review-copy';

declare let swal: any;

@Component({
  selector: 'app-order-review-copy',
  templateUrl: './order-review-copy.component.html',
  styleUrls: ['./order-review-copy.component.css']
})
export class OrderReviewCopyComponent implements OnInit {
  userDetails: any;
  userCredential: any;
  isLoader: boolean;
  colorText: string;
  count: any;
  qtyText: string;
  priceText: string;
  headerText: string;
  grandTotal: number = 0.00;
  tax: number = 0.00;
  disablePayment: boolean;
  selectedPackages: any[] = [];
  orderReviewInformation: RequestAddtionalTestItem;
  total: number = 0.00;
  decryptObject: RequestData;
  constructor(
      private dataService: DataService, private router: Router,
      private activatedRoute: ActivatedRoute) {
    this.orderReviewInformation = new RequestAddtionalTestItem();
    this.decryptObject = new RequestData();
  }

  ngOnInit() {
    (<any>$('[data-toggle="tooltip"]')).tooltip();
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetails = JSON.parse(localStorage.getItem('userDetail'));
    this.activatedRoute.params.subscribe(params => {
      if (!!params['data']) {
        this.decryptObject.EncryptString = params['data'];
        this.decryptObject.UserId = params['id'];
        localStorage.setItem(
            'orderReviewRequest', JSON.stringify(this.decryptObject));
      } else {
        this.decryptObject =
            JSON.parse(localStorage.getItem('orderReviewRequest'));
      }
    });
    if (!this.userDetails || !this.userCredential) {
      this.router.navigate(['/login']);
    } else {
      if (!this.decryptObject || this.decryptObject == null) {
        this.router.navigate(['/landing-page']);
      } else {
        if (this.decryptObject.UserId != this.userDetails.UserId) {
          localStorage.removeItem('orderReviewRequest');
          this.router.navigate(['/login']);
          swal('', 'Invalid User');
        } else {
          this.isLoader = true;
          this.decryptRequest();
          this.disablePayment = true;
        }
      }
    }
  }

  ngDoCheck() {
    if (!!this.orderReviewInformation.TestItems) {
      if (this.orderReviewInformation.TestItems[0].PackageType == 'P') {
        this.headerText = 'Package (s)';
        this.qtyText = 'Package (s) Qty';
        this.priceText = 'Package (s) Price (USD)';
      } else {
        this.headerText = 'Test (s) Name';
        this.qtyText = 'Test (s) Qty';
        this.priceText = 'Test (s) Price (USD)';
      }
      this.count =
          this.orderReviewInformation.TestItems.filter(x => x.ColorwayQty > 0);

      if (this.count != undefined && this.count.length > 0) {
        this.colorText = 'Color';
      } else {
        this.colorText = '';
      }
      (<any>$('#flux')).bind('scroll', function() {
        if ((<any>$(this)).scrollTop() + (<any>$(this)).innerHeight() >=
            (<any>$(this)[0]).scrollHeight - 50) {
          (<any>$('#termsCondition')).attr('disabled', false);
        }
      });
      this.calculatePrice();
    }
  }

  termsClick(event) {
    if (event.target.checked) {
      this.disablePayment = false;
    } else {
      this.disablePayment = true;
    }
  }

  calculatePrice() {
    this.total = 0.00;
    try {
      for (var i = 0; i < this.orderReviewInformation.TestItems.length; i++) {
        var colorTotal =
            (this.orderReviewInformation.TestItems[i].ColorwayPrice *
             this.orderReviewInformation.TestItems[i].ColorwayQty)
        var PackageTotalPrice =
            (this.orderReviewInformation.TestItems[i].Price *
             this.orderReviewInformation.TestItems[i].Quantity) +
            colorTotal;
        this.total = this.total + PackageTotalPrice;
        this.orderReviewInformation.TestItems[i].FinalPrice = PackageTotalPrice;
      }
      this.tax = 0.0 * this.total;
      this.grandTotal = this.total + this.tax;
    } catch (ex) {
      console.log(ex);
    }
  }

  decryptRequest() {
    this.dataService
        .post('RequestAddtionalTestItem/ValidateRequestId', this.decryptObject)
        .subscribe(response => {
          if (response.IsSuccess) {
            this.orderReviewInformation = response;
            this.isLoader = false;
          } else {
            localStorage.removeItem('orderReviewRequest');
            this.router.navigate(['./login']);
            swal('', 'Link has been expired.');
          }
        }, err => {});
  }

  public reset() {
    this.total = 0.0;
    this.tax = 0.0;
    this.grandTotal = 0.0;
  }

  public InitiatePayment() {
    var that = this;
    that.isLoader = true;
    let data = that.orderReviewInformation.TestItems.map(x => {
      return {
        'PackageId': x.Id,
        'Type': x.PackageType,
        'LocationId': that.orderReviewInformation.LocationId,
        'AgeGrade': x.AgeGrade,
        'CountryID': x.CountryID,
        'ProductType': x.ProductType,
        'ServiceID': x.ServiceID,
        'ColorwayQty': x.ColorwayQty,
        'Quantity': x.Quantity,
        'ServicePackageMappingId': x.ServicePackageMappingId,
        'ItemMappingId': x.ItemMappingId
      };
    })
    /**
     * Added by Satyam Tyagi, payment require.
     */
    that.dataService
        .post('Payment/InitiatePayment', {
          'packages': data,
          'serviceId': that.orderReviewInformation.ServiceId,
          'tempETRFId': that.orderReviewInformation.Temp_TRF_ID
        })
        .subscribe(response => {
          if (response.IsSuccess) {
            window.location.href = response.Data;
            that.isLoader = false;
          } else {
            that.router.navigate(['/paypal/paypal-cancel']);
            that.isLoader = false;
          }
        });

    /**
     * Added by Satyam Tyagi, no need for payment.
     */
    // that.dataService
    //     .post('Payment/InitiatePaymentTest', {
    //       'packages': data,
    //       'serviceId': that.orderReviewInformation.ServiceId,
    //       'tempETRFId': that.orderReviewInformation.Temp_TRF_ID
    //     })
    //     .subscribe(response => {
    //       if (response.IsSuccess) {
    //         var paymentId = 'PAY-69H88013FT309984WLJJFU6A';
    //         var token = response.Data.token;
    //         var PayerID = 'VQTPJ7D6DGBK4';
    //         var amount = response.Data.amount
    //         that.isLoader = false;
    //         this.router.navigate(
    //             ['/paypal/paypal-success/' + paymentId + '/' + token + '/' +
    //              PayerID + '/' + amount]);
    //       } else {
    //         that.router.navigate(['/paypal/paypal-cancel']);
    //         that.isLoader = false;
    //       }
    //     });
  }
}
