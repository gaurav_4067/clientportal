import { OrderReviewCopyModule } from './order-review-copy.module';

describe('OrderReviewCopyModule', () => {
  let orderReviewCopyModule: OrderReviewCopyModule;

  beforeEach(() => {
    orderReviewCopyModule = new OrderReviewCopyModule();
  });

  it('should create an instance', () => {
    expect(orderReviewCopyModule).toBeTruthy();
  });
});
