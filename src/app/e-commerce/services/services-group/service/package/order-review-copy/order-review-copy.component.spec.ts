import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderReviewCopyComponent } from './order-review-copy.component';

describe('OrderReviewCopyComponent', () => {
  let component: OrderReviewCopyComponent;
  let fixture: ComponentFixture<OrderReviewCopyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderReviewCopyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderReviewCopyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
