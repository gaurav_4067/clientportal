import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';

import { DataService } from '../../../../../mts.service';

import { Insp_LotSize, Insp_Product_Extended, Insp_ProductCategory, Insp_ProductSubCategory, InspectionModel } from './package';

declare let swal: any;
declare let $: any;

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.css']
})
export class PackageComponent implements OnInit {
  @Input() data;
  packages = [];
  packageToView: any;
  isAnyPackageSelected: boolean = false;
  activePopupPackage: any;
  AgeGradeModal: any;
  ProductTypeModal: any;
  LiningModal: any;
  CountryName: any;
  serviceID: any;
  ServiceName: any;
  PackageType: any;

  // FCAIPackage: any;
  // InspectionPackage: any;
  isLoader: boolean;
  PackageData: any = {
    'ServiceID': 0,
    'CountryID': '',
    'AgeGroupID': 0,
    'ProductTypeID': 0,
    'LiningID': '',
    'LabId': 0
  };
  CountryOfDestination: any;
  AgeGrade: any;
  ProductType: any;
  Lining: any = [];
  userDetail: any;
  Enquiry = { 'EnquiryBy': 0, 'Subject': '', 'Comments': '', 'EnquiryType': '' };
  subjectModal: any;
  commentsModal: any;
  testFilter = { 'packagesModal': '', 'IndividualTestItemsModal': '' };
  packagesModal: any;
  IndividualTestItemsModal: any;

  individualFlag: any;
  individualLocation: any = [];
  individualSelect: any = [];
  individualLocationId: string = '';
  noteMessage: any;
  enquiryheader: any;
  individualPackages: any = [];
  packageSection: boolean = true;
  individualSection: boolean = false;
  showcolor: any;
  showMessageColor: any;
  ColorwayPrice: any = '';
  colorwayData: any;
  colorPopUpPackage: any;
  colorQuantityRange: any;

  // Inspection Service parameters
  categoryId: number;
  productCategories: Array<Insp_ProductCategory>;
  productList: Array<Insp_Product_Extended>;
  countryList: any = [];
  provinceList: any = [];
  locationList: any = [];
  sampleList: any = [];
  acceptanceLevelList: any = [];
  inspectionLevelList: any = [];
  productObject: InspectionModel;
  samlpleSizeList: any = [];
  isAnotherProductSelected: boolean = false;
  selectedProduct: string;
  isSubmitted: boolean = false;
  shipmentQuantity: Array<Insp_LotSize>;
  selectedProductIndex: any;
  selectPackageColorwayPrice: any;
  selectedPackagePrice: any;

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  public resetSelection() {
    // this.packages.forEach(x => x.isSelected = false);
    this.isAnyPackageSelected = true;
    // this.data.categoryId = 1;
  }

  public selectionCheck(event, pkg) {
    var that = this;
    if (this.individualFlag) {
      this.dataService.EcommPackageDetails.IsPackages = false;
      this.dataService.EcommPackageDetails.PackageName = '';
      this.dataService.EcommPackageDetails.PackageTestName = [];

      this.individualSelect.push(pkg.Id);
      this.dataService.EcommPackageDetails.IndividualTestItems.push(pkg.Name);
      if (this.individualLocationId != '' &&
        this.individualLocationId != undefined) {
        this.isAnyPackageSelected =
          this.individualPackages.some(x => x.isSelected);
      }
    } else {
      if (event.target.checked) {
        this.selectedPackagePrice = pkg.Price;
        this.selectPackageColorwayPrice = pkg.ColorwayPrice;
        this.dataService.EcommPackageDetails.IsPackages = true;
        this.dataService.EcommPackageDetails.IndividualTestItems = [];
        if (pkg.isSelectedPacakge != false) {
          pkg.Locations = pkg.PackageLocations;
          this.packageSection = true;
          this.activePopupPackage = pkg;
          let index = pkg.Locations.findIndex(i => i.IsLowest == true);
          if (index > -1) {
            this.activePopupPackage.SelectedLocation =
              pkg.Locations[index].GroupId;
          }
          (<any>$('#myModal')).modal('show');

          // this.dataService.get('Package/PackageLoction?ItemId=' + 1 +
          // "&ServiceId=" + pkg.ServiceID + "&MappingId=" +
          // pkg.ServicePackageMappingId).subscribe(
          //   response => {
          //     if (response.IsSuccess) {
          //       pkg.Locations = response.Data;
          //       this.packageSection = true;
          //       this.activePopupPackage = pkg;
          //       let index = pkg.Locations.findIndex(i => i.IsDefault ==
          //       true); if (index > -1) {
          //         this.activePopupPackage.SelectedLocation =
          //         pkg.Locations[index].GroupId;
          //       }
          //       (<any>$('#myModal')).modal('show');
          //     }
          //     else {
          //       swal('', response.Message)
          //     }
          //   }
          // );
        }
        // $("#locationPopover").popover('reposition');
        // if (pkg.isSelectedPacakge && pkg.SelectedLocation === 0) {
        //    pkg.isSelectedPacakge = false;
        // }
        // else {
        //   this.isAnyPackageSelected = this.packages.some(x =>
        //   x.isSelectedPacakge); pkg.SelectedLocation = 0; pkg.ColorwayQty =
        //   0;
        // }
      } else {
        pkg.isSelectedPacakge = false;
        pkg.ColorwayPrice = this.selectPackageColorwayPrice;
        pkg.Price = this.selectedPackagePrice;
        this.isAnyPackageSelected =
          this.packages.some(x => x.isSelectedPacakge);
        pkg.SelectedLocation = 0;
        pkg.ColorwayQty = 0;
      }
    }
    this.dataService
      .get(
      'Package/GetPackagesTests?ServicePackageMappingId=' +
      pkg.ServicePackageMappingId)
      .subscribe(response => {
        that.dataService.EcommPackageDetails.PackageTestName = response.Tests;
        that.dataService.EcommPackageDetails.PackageName = pkg.Name;
      });
  }

  closeModal() {
    // let index = this.packages.findIndex(i => i.isSelectedPacakge == true);
    // if (index > -1) {
    //   this.packages[index].isSelectedPacakge = false;
    // }
    this.isAnyPackageSelected = this.packages.some(x => x.isSelectedPacakge);
  }

  public InitiatePayment() { }

  public selectPackage(pkg, locationId) {
    pkg.isSelectedPacakge = true;
    pkg.SelectedLocation = locationId;
    let object = pkg.Locations.find(i => i.GroupId == locationId);
    this.activePopupPackage.Price = object.PackagePrice;
    this.activePopupPackage.ColorwayPrice = object.ColorwayPrice;
    this.isAnyPackageSelected = this.packages.some(x => x.isSelectedPacakge);
    this.packages.filter(i => {
      if (!i.isSelectedPacakge) {
        i.ColorwayQty = 0
      }
    });
    (<any>$('#myModal')).modal('hide');
  }

  public open(event, packageToView) {
    this.packageToView = packageToView;
    if (this.packageToView.items === undefined) {
      this.dataService
        .get(
        'Package/GetPackagesTests?ServicePackageMappingId=' +
        packageToView.ServicePackageMappingId)
        .subscribe(response => {
          this.packageToView.items = response.Tests;
        });
    }
  }

  public SaveSelectedPackages(event) {
   
    this.dataService.selectedPackages = [];
    this.data.ready = true;
    if (this.individualFlag) {
      this.dataService.selectedPackages =
        this.individualPackages.filter(x => x.isSelected === true);
      this.data.locationInfo = this.individualLocation.filter(
        i => i.GroupId == this.individualLocationId);
      for (var key in this.dataService.selectedPackages) {
        this.dataService.selectedPackages[key].SelectedLocation =
          this.individualLocationId;
        this.dataService.selectedPackages[key].PackageQty = 1;
      }

    } else {
      this.dataService.selectedPackages =
        this.packages.filter(x => x.isSelectedPacakge === true);
      this.dataService.selectedPackages.forEach(function (obj) {
        obj.PackageQty = 1;
      });
      this.data.locationInfo =
        this.dataService.selectedPackages[0].Locations.filter(
          i => i.GroupId ==
            this.dataService.selectedPackages[0].SelectedLocation);
    }
    if (!this.isAnyPackageSelected) event.stopPropagation();
  }

  SaveSelectedProduct(event) {
    
    this.dataService.selectedPackages = [];
    this.data.ready = true;
    this.dataService.selectedPackages.push(this.productObject);
    this.dataService.selectedPackages[0].FinalPrice =
      this.productObject.TotalCost;
    let locationObject = [{
      'CountryDescription': this.productObject.SelectedCountryName,
      'City': this.productObject.SelectedLocationName,
      'State': this.productObject.SelectedProvinceName
    }];
    this.data.locationInfo = locationObject;
  }

  public BackFilter() {
    this.data.offerApplied = false;
	this.data.SelectedOffer = {};
    this.dataService.selectedServiceText = 'Select Packages';
    this.data.showSelectPackages = false;
    this.data.selectedServiceInfo = {};
  }
  // Test Filter for packages IndividualTestItemsModal
  public TestFilter() {
    this.isLoader = true;
    var that = this;
    // if (this.packagesModal != undefined) {this.PackageData.LiningID =
    // this.packagesModal;} if (this.IndividualTestItemsModal != undefined)
    // {this.PackageData.LiningID = this.IndividualTestItemsModal;}
  }

  // Send Enquiry By Modals
  public sendEnquiry() {
    this.isLoader = true;
    var that = this;
    var EnquiryBy = this.userDetail.UserId;
    if (EnquiryBy) {
      this.Enquiry.EnquiryBy = this.userDetail.UserId;
    }
    if (this.subjectModal) {
      this.Enquiry.Subject = this.subjectModal;
    } else {
      this.isLoader = false;
      swal('', 'Please enter subject.');
      return;
    }

    if (this.commentsModal) {
      this.Enquiry.Comments = this.commentsModal;
    } else {
      this.isLoader = false;
      swal('', 'Please enter comments.');
      return;
    }

    this.dataService.post('Package/SendEnquiry', this.Enquiry)
      .subscribe(response => {
        if (response.IsSuccess) {
          swal('', response.Message);
          that.isLoader = false;
          (<any>$('#Enquirysubject')).val('');
          (<any>$('#Enquirycomments')).val('');
          that.commentsModal = '';
          that.subjectModal = '';
          $('#Enquiry').modal('hide');
        } else {
          swal('', response.Message);
          that.isLoader = false;
        }
      });
  }
  ///--------------------------
  public SubmitFilter(prodata) {
    if (prodata) {
      this.PackageData.LiningID = '';
      this.Lining = [];
      this.dataService
        .get('Package/GetLiningMaster?ProductTypeID=' + prodata.Id)
        .subscribe(response => {
          if (response.Data != undefined && response.Data.length > 0) {
            this.Lining = response.Data;
          } else {
            this.Lining = [];
          }
        });
    }
    this.isLoader = true;
    var that = this;
    this.isAnyPackageSelected = false;
    var CountryID = '';
    var countryName = '';
    this.CountryOfDestination.forEach((data) => {
      if (data.IsSelected == true) {
        if (CountryID == undefined) {
          CountryID = data.Id + ',';
          countryName = data.CountryName + ',';
        } else {
          CountryID = CountryID + data.Id + ',';
          countryName = data.CountryName + ',';
        }
      } else {
        if (CountryID == undefined) {
          CountryID = '';
        }
      }
    })

    if (CountryID != undefined) {
      this.PackageData.CountryID = CountryID.slice(0, -1);
      this.dataService.EcommETRFData.countryofdestination =
        countryName.slice(0, -1);
    }

    var LiningID = '';
    this.Lining.forEach((data) => {
      if (data.IsSelected == true) {
        if (LiningID == undefined) {
          LiningID = data.Id + ',';
        } else {
          LiningID = LiningID + data.Id + ',';
        }
      }
    })
    if (LiningID != undefined) {
      this.PackageData.LiningID = LiningID.slice(0, -1);
    }

    // if (this.LiningModal != undefined) {
    //   this.PackageData.LiningID = this.LiningModal.split('Lining')[1];
    // }

    if (this.ProductTypeModal != undefined) {
      this.PackageData.ProductTypeID =
        this.ProductTypeModal.split('ProductType')[1];
    }
    if (this.AgeGradeModal != undefined) {
      this.PackageData.AgeGroupID = this.AgeGradeModal.split('AgeGrade')[1];
      this.dataService.EcommETRFData.agegrade =
        $('.' + this.AgeGradeModal).text()
    }

    this.dataService.post('Package/PackageFiltered', this.PackageData)
      .subscribe(response => {
        that.isLoader = false;
        this.packages = response.PackagesAndTests;
        this.individualFlag = false;
        this.Enquiry.EnquiryType = 'Enquiry for Packages';
        this.individualSection = false;
        this.packageSection = true;
      });
  }

  public IndividualTest() {
    // this.dataService.EcommPackageDetails.IsPackages = false;
    this.individualLocationId = '';
    this.isLoader = true;
    this.dataService
      .get(
      'Package/PackageLoction?ItemId=' + 2 +
      '&ServiceId=' + this.data.DivisionID + '&MappingId=0')
      .subscribe(response => {
        if (response.IsSuccess) {
          this.individualLocation = response.Data;
          this.individualLocationId =
            this.individualLocation.find(i => i.IsDefault == true).GroupId;
          this.dataService
            .get(
            'Package/IndividualTests?serviceId=' +
            this.PackageData.ServiceID +
            '&locationId=' + this.PackageData.LabId)
            .subscribe(response => {
              this.isLoader = false;
              this.individualPackages = response.PackagesAndTests;
              this.individualFlag = true;
              this.noteMessage = 'Selected Location : ' +
                this.individualLocation.find(i => i.IsDefault == true)
                  .CountryCode;
              this.showcolor = true;
              this.showMessageColor = true;
              this.Enquiry.EnquiryType =
                'Enquiry for Individual Test Items';
              this.individualSection = true;
              this.packageSection = false;
              this.packages.length = 0;
              this.isAnyPackageSelected = false;
            });
        } else {
          swal('', response.Message);
        }
      });
  }

  public individualModal() {
    this.dataService
      .get(
      'Package/PackageLoction?ItemId=' + 2 +
      '&ServiceId=' + this.data.DivisionID + '&MappingId=0')
      .subscribe(response => {
        if (response.IsSuccess) {
          this.individualLocation = response.Data;
          if (this.individualLocationId == '') {
            this.individualLocationId =
              this.individualLocation.find(i => i.IsDefault == true)
                .GroupId;
          } else {
            (<any>$('#myModalIndividual')).modal('show');
          }
        } else {
          swal('', response.Message);
        }
      });
  }
  public selectIndividualTest(locationId, countryname) {
    this.individualSelect = [];
    this.isAnyPackageSelected = false;
    this.individualLocationId = locationId;
    this.dataService
      .get(
      'Package/IndividualTests?serviceId=' + this.PackageData.ServiceID +
      '&locationId=' + this.individualLocationId)
      .subscribe(response => {
        this.individualPackages = response.PackagesAndTests;
      });
    this.showcolor = true;
    this.showMessageColor = false;
    this.data.locationInfo =
      this.individualLocation.filter(i => i.GroupId == locationId);
    this.noteMessage = 'Selected Location : ' + countryname;

    if (this.individualSelect.length != '0')
      this.isAnyPackageSelected =
        this.individualPackages.some(x => x.isSelected);
    (<any>$('#myModalIndividual')).modal('hide');
  }

  public opencolorway(event, packageData) {
    if (event.target.value == 'yes') {
      if (!this.individualFlag && this.isAnyPackageSelected &&
        !packageData.isSelectedPacakge) {
        return false;
      }
      this.colorPopUpPackage = packageData;
      this.ColorwayPrice = packageData.ColorwayPrice;
      this.colorwayData =
        (packageData.ColorwayQty == 0) ? 1 : packageData.ColorwayQty;
      (<any>$('#AddColorway')).modal('show');
    } else {
      packageData.ColorwayQty = 0;
      this.colorPopUpPackage = packageData;
      this.ColorwayPrice = packageData.ColorwayPrice;
      this.colorwayData = 0;
    }
  }
  public colorwaySubmit(pkg) {
    if (this.colorwayData == undefined) {
      this.colorwayData = '';
    }
    if ((this.colorwayData < 1 || this.colorwayData > 9999)) {
      this.colorQuantityRange = 'Quantity must be between 1 & 9999';
      return false;
    } else {
      this.colorQuantityRange = '';
    }
    pkg.ColorwayQty = this.colorwayData;
    this.colorwayData = '';
    (<any>$('#AddColorway')).modal('hide');
  }
  public closeColorWay() {
    $('#B' + this.colorPopUpPackage.Sno).prop('checked', true);
    this.colorwayData = '';
    this.colorQuantityRange = '';
    (<any>$('#AddColorway')).modal('hide');
  }

  constructor(
    public dataService: DataService, private cdRef: ChangeDetectorRef) {
    this.productCategories = new Array<Insp_ProductCategory>();
    this.productList = new Array<Insp_Product_Extended>();
    this.productObject = new InspectionModel();
    this.shipmentQuantity = new Array<Insp_LotSize>();
  }

  ngOnInit() {
    (<any>$('[data-toggle="tooltip"]')).tooltip();
    this.data.Flag = 2;
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.serviceID = JSON.parse(localStorage.getItem('ServiceId'));
    this.categoryId = JSON.parse(localStorage.getItem('CategoryId'));
    this.getAllLocation();
    this.individualFlag = false;
    this.Enquiry.EnquiryType = 'Enquiry for Packages';
    this.dataService.EcommETRFData.report_Document = this.userDetail.Address;
    this.packagesModal = '1';
    this.packageSection = true;
    this.getAcceptanceLevel();
    this.getManDays();

    // if (this.individualSelect.length > 0 && this.individualLocationId) {
    //   this.isAnyPackageSelected = this.individualPackages.some(x =>
    //   x.isSelected);
    // }

    // For on scroll sidebarFilter fixed
    //  $(window).scroll(function () {
    //   if ($(this).scrollTop() > 150) {
    //    $('.sidebarFilter').addClass('fixed');
    //   } else if ($(this).scrollTop() < 150) {
    //    $('.sidebarFilter').removeClass('fixed');
    //   }
    //  });


    // this.dataService.get("Package/GetAllPackagesAndTests").subscribe(response
    // => {
    //   this.packages = response.PackagesAndTests;
    // });
    // JSON.parse(localStorage.getItem("ServiceId"))
    // alert("test");
    // this.dataService.get('Package/ServiceBasedPackage?serviceId='
    // +JSON.parse(localStorage.getItem("ServiceId"))).subscribe(
    //   response => {
    //     if (response.IsSuccess) {

    //       this.AgeGrade = response.Data.Age;
    //       this.CountryOfDestination = response.Data.Country;
    //       this.ProductType = response.Data.Product;
    //     }
    //     else {
    //       swal('', response.Message)
    //     }
    //   }
    // );
    this.dataService.EcommETRFData.IsEcomm = true;
    this.dataService.EcommETRFData.agegrade = 'Any';
    this.isLoader = true;
    var that = this;
    this.data.categoryId = this.categoryId;
    this.ServiceName = JSON.parse(localStorage.getItem('ServiceName'));
    if (this.categoryId == 1) {
      this.dataService
        .get('Package/ServiceBasedPackage?serviceId=' + this.serviceID)
        .subscribe(response => {
          if (response.IsSuccess) {
            this.AgeGrade = response.Data.Age;
            this.CountryOfDestination = response.Data.Country;
            this.ProductType = response.Data.Product;
            this.PackageType = response.Data.Type;
            that.PackageData.ServiceID = this.serviceID;
            if (that.PackageData.ServiceID == 2) {
              that.PackageData.AgeGroupID = 3;
              setTimeout(function () {
                (<any>$('#AgeGrade0')).prop('checked', true);
              }, 1000)


              // $("#AgeGrade4").prop("checked", true);
            }
            that.dataService.post('Package/PackageFiltered', that.PackageData)
              .subscribe(response => {
                that.isLoader = false;
                this.packages = response.PackagesAndTests;
              });

          } else {
            swal('', response.Message)
          }
        });
    } else {
      this.dataService
        .get('ManageInspection/GetProductCategoryTree/' + this.categoryId)
        .subscribe(response => {
          that.isLoader = false;
          this.productCategories = response;
          this.filterProducts(
            this.productCategories[0].Product_Category_Name,
            this.productCategories[0].SubCategories[0]);
        });
    }
  }

  filterProducts(
    productCategoryName: string, product: Insp_ProductSubCategory) {
    this.selectedProduct =
      productCategoryName + product.Product_SubCategory_Name;
    this.productList = new Array<Insp_Product_Extended>();
    this.productObject = new InspectionModel();
    this.isAnotherProductSelected = false;
    product.Products.forEach(element => {
      this.productList.push({
        Product_SubCategoryID: element.Product_SubCategoryID,
        ProductID: element.ProductID,
        Product_Name: element.Product_Name,
        Sample_Size_Permanday: element.Sample_Size_Permanday,
        IsSelected: false,
        Active: element.Active,
        Created_Date: element.Created_Date,
        Modified_date: element.Modified_date,
        Price: '0'
      });
    });
  }

  selectionProduct(selectedIndex, evt) {
    // this.isLoader = true;
    this.productObject.SelectedProductIndex = selectedIndex;
    if (evt.target.checked) {
      this.isLoader = true;
      this.productObject.DefaultSampleSizePermanday =
        this.productList[selectedIndex].Sample_Size_Permanday;
      this.productObject.ProductId = this.productList[selectedIndex].ProductID;
      this.isAnotherProductSelected =
        this.productList[selectedIndex].IsSelected = true;
      this.samlpleSizeList = this.samlpleSizeList.filter(
        i => i.Sample_Size >= this.productObject.DefaultSampleSizePermanday);
      this.productObject.SampleSize =
        this.productObject.DefaultSampleSizePermanday;
      this.dataService
        .get(
        'ManageInspection/GetProductLotSize/' +
        this.productObject.ProductId + '/' +
        this.productObject.InspectionLevel)
        .subscribe(
        response => {
          this.shipmentQuantity = response;
          this.productObject.ShipmentQty =
            this.shipmentQuantity
              .filter(
              i => i.Sample_Size ==
                this.productObject.SampleSize.toString())[0]
              .Lotsize_ID;
          this.isLoader = false;
          (<any>$('#ProductDetailModal')).modal('show');
        },
        err => {
          this.isLoader = false;
        });
    } else {
      this.isAnotherProductSelected = false;
      this.productList[selectedIndex].IsSelected = false;
      this.productList[selectedIndex].Price = '0';
      this.productList[selectedIndex].Sample_Size_Permanday =
        this.productObject.DefaultSampleSizePermanday;
      this.productObject = new InspectionModel();
      this.provinceList = [];
      this.locationList = [];
    }
  }

  getShipmentAndSampleSize() {
    this.dataService
      .get(
      'ManageInspection/GetProductLotSize/' +
      this.productObject.ProductId + '/' +
      this.productObject.InspectionLevel)
      .subscribe(
      response => {
        this.shipmentQuantity = response;
        this.productObject.ShipmentQty =
          this.shipmentQuantity
            .filter(
            i => i.Sample_Size ==
              this.productObject.SampleSize.toString())[0]
            .Lotsize_ID;
        // this.isLoader = false;
        // (<any>$('#ProductDetailModal')).modal('show');
      },
      err => {
        // this.isLoader = false;
      });
  }

  setValue(type: string) {
    if (type == 'country') {
      let countryObject = this.countryList.filter(
        i => i.CountryID == this.productObject.SelectedCountry);
      let productId = this.productObject.ProductId;
      this.provinceList = countryObject[0].States;
      let defaultPermandayValue = this.productObject.DefaultSampleSizePermanday;
      let defaultSelectedPackageIndex = this.productObject.SelectedProductIndex;
      this.productObject = new InspectionModel();
      this.productObject.ProductId = productId;
      this.productObject.DefaultSampleSizePermanday = defaultPermandayValue;
      this.productObject.SampleSize =
        this.productObject.DefaultSampleSizePermanday;
      this.getShipmentAndSampleSize();
      // this.productObject.ShipmentQty = this.shipmentQuantity.filter(i =>
      // i.Sample_Size ==
      // this.productObject.SampleSize.toString())[0].Lotsize_ID;
      this.productObject.SelectedCountry = countryObject[0].CountryID;
      this.productObject.SelectedCountryName = countryObject[0].Country_Name;
      this.productObject.SelectedProductIndex = defaultSelectedPackageIndex;
      this.productObject.MandayCost = countryObject[0].InspManday.Manday_Rate;
      this.productObject.UpdatedMandayCost =
        countryObject[0].InspManday.Manday_Rate;
      this.locationList = [];
    }
    if (type == 'province') {
      this.productObject.SelectedLocation = 0;
      this.locationList =
        this.provinceList
          .filter(
          i => i.ProvinceID == this.productObject.SelectedProvince)[0]
          .Locations;
      this.productObject.SelectedProvinceName =
        this.provinceList
          .filter(
          i => i.ProvinceID == this.productObject.SelectedProvince)[0]
          .Province;
    }
    if (type == 'location') {
      let object = this.locationList.filter(
        i => i.LocationID == this.productObject.SelectedLocation)[0];
      this.productObject.SelectedLocationName = object.Location_Name;
      this.productObject.TravelCost = object.Travel_USD;
      this.productObject.HotelCost = object.Hotel_USD;
    }
    if (type == 'sampleSize') {
      this.productObject.SampleSize = parseInt(
        this.shipmentQuantity
          .filter(i => i.Lotsize_ID == this.productObject.ShipmentQty)[0]
          .Sample_Size);
    }
    if (type == 'inspection') {
      this.isLoader = true;
      this.dataService
        .get(
        'ManageInspection/GetProductLotSize/' +
        this.productObject.ProductId + '/' +
        this.productObject.InspectionLevel)
        .subscribe(
        response => {
          this.shipmentQuantity = response;
          this.productObject.ShipmentQty =
            this.shipmentQuantity[0].Lotsize_ID;
          this.productObject.SampleSize =
            parseInt(this.shipmentQuantity[0].Sample_Size);
          this.isLoader = false;
        },
        err => {
          this.isLoader = false;
        });
    }
  }

  getAllLocation() {
    this.dataService.get('ManageInspection/GetAllLocations/' + this.categoryId)
      .subscribe(response => {
        this.countryList = response;
      });
  }

  getAcceptanceLevel() {
    this.dataService.get('ManageInspection/GetAQLLevels')
      .subscribe(response => {
        this.acceptanceLevelList = response.AcceptanceLevels;
        this.inspectionLevelList = response.InspectionLevels;
      });
  }

  getManDays() {
    this.dataService.get('ManageInspection/GetAQLSampleSizeMandayList/1')
      .subscribe(response => {
        this.samlpleSizeList = response;
      });
  }

  submitShipmentDetails() {
    this.isSubmitted = true;
    if (this.productObject.SelectedCountry == 0 ||
      this.productObject.SelectedProvince == 0 ||
      this.productObject.SelectedLocation == 0 ||
      this.productObject.InspectionLevel == 0) {
    } else {
      // if (this.productObject.SampleSize >=
      // this.productObject.DefaultSampleSizePermanday) {
      this.productObject.ProductId =
        this.productList[this.productObject.SelectedProductIndex].ProductID;
      this.productObject.Name =
        this.productList[this.productObject.SelectedProductIndex]
          .Product_Name;
      this.productObject.ServiceName = this.data.DivisionName;
      this.productList[this.productObject.SelectedProductIndex].Price =
        parseFloat(this.productObject.TotalCost.toFixed(2)).toFixed(2);
      this.productList[this.productObject.SelectedProductIndex]
        .Sample_Size_Permanday = this.productObject.SampleSize;
      this.isSubmitted = false;
      (<any>$('#ProductDetailModal')).modal('hide');
      // }
    }
  }

  resetShipmentDetails() {
    this.isSubmitted = false;
    this.productList[this.productObject.SelectedProductIndex].IsSelected =
      false;
    this.isAnotherProductSelected = false;
    this.productObject = new InspectionModel();
    this.provinceList = [];
    this.locationList = [];
  }

  getTotalCost() {
    // this.productObject.RequiredManday =
    // parseFloat((this.productObject.ShipmentQty/this.productObject.DefaultSampleSizePermanday).toFixed(2));
    this.productObject.TotalCost =
      ((this.productObject.RequiredManday * this.productObject.MandayCost) +
        this.productObject.TravelCost + this.productObject.HotelCost);
    return (parseFloat(this.productObject.TotalCost.toFixed(2)));
  }

  restrictNumeric(e) {
    let input;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) {
      return false;
    }
    if (e.which === 0) {
      return true;
    }
    if (e.which < 33) {
      return true;
    }
    input = String.fromCharCode(e.which);
    return !!/[\d\s]/.test(input);
  }

  getRequiredManday() {
    let value = Math.floor(
      this.productObject.SampleSize /
      this.productObject.DefaultSampleSizePermanday);
    if (value == 0) {
      this.productObject.RequiredManday = 1;
    } else {
      this.productObject.RequiredManday = Math.floor(
        this.productObject.SampleSize /
        this.productObject.DefaultSampleSizePermanday)
    }
    return this.productObject.RequiredManday;
  }

  getManday() {
    this.productObject.UpdatedMandayCost = parseFloat(
      (this.productObject.RequiredManday * this.productObject.MandayCost)
        .toFixed(2));
    return this.productObject.UpdatedMandayCost;
  }

  getIndividualTotalCost() {
    if (this.individualFlag) {
      var packages = this.individualPackages.filter(x => x.isSelected === true);
      var total = 0;
      if (packages.length > 0) {
        packages.forEach(element => {
          total = total + element.Price;
        });
      }
      return total.toFixed(2);
    }
  }

  OpenSimplePlanModal() {
    (<any>$('#SimplePlanModal')).modal('show');
  }

  CloseSimplePlanModal() {
    (<any>$('#SimplePlanModal')).modal('hide');
  }

  OpenAQLModal() {
    (<any>$('#AQLModal')).modal('show');
  }

  CloseAQLModal() {
    (<any>$('#AQLModal')).modal('hide');
  }

  displayApplyButton(isDisplay) {
    this.data.ShowApplyButton = isDisplay;
  }
}
