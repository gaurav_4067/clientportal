import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {PackageRoutingModule} from './package-routing.module';
import {PackageComponent} from './package.component';

@NgModule({
  imports: [CommonModule, FormsModule, PackageRoutingModule],
  declarations: [PackageComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [PackageComponent]
})
export class PackageModule {
}
