export class Insp_ProductCategory {
  public Insp_ProductCategory() {
    this.SubCategories = new Array<Insp_ProductSubCategory>();
  }

  public Product_CategoryID: number;
  public Product_Category_Name: string;
  public Business_type: number;
  public Active: boolean;
  public Created_Date: Date;
  public Modified_date: Date;
  public SubCategories: Array<Insp_ProductSubCategory>;
}



export class Insp_ProductSubCategory {
  public Insp_ProductSubCategory() {
    this.Products = new Array<Insp_Product>();
  }
  public Product_CategoryID: number;
  public Product_SubCategoryID: number;
  public Product_SubCategory_Name: string;
  public Active: boolean;
  public Created_Date: Date;
  public Modified_date: Date;
  public Products: Array<Insp_Product>;
}

export class Insp_Product {
  public Product_SubCategoryID: number;
  public ProductID: number;
  public Product_Name: string;
  public Sample_Size_Permanday: number;
  public Active: boolean;
  public Created_Date: Date;
  public Modified_date: Date;
}

export class Insp_Product_Extended extends Insp_Product {
  public IsSelected: boolean;
  public Price: string;
}

export class Insp_LotSize {
  public Lotsize_ID: number;
  public Lotsize_Range: string;
  public Sample_Size: string;
  public Critical_Defects: string;
  public Major_Defects: string;
  public Minor_Defects: string;
  public Active: boolean;
  public Modified_date: Date;
  public Created_Date: Date;
}

export class InspectionModel {
  constructor() {
    this.MandayCost = 0;
    this.PackageType = 'I';
    this.UpdatedMandayCost = 0;
    this.TotalCost = 0;
    this.RequiredManday = 0;
    this.InspectionLevel = 2;
    this.SelectedCountry = 0;
    this.SelectedProvince = 0;
    this.SelectedLocation = 0;
    this.AcceptanceLevelCritical = 0;
    this.AcceptanceLevelMajor = 10;
    this.AcceptanceLevelMinor = 11;
    this.DefaultSampleSizePermanday = 0;
  }
  ProductId: number;
  MandayCost: number;
  PackageType: string;
  Name: string;
  ServiceName: string;
  UpdatedMandayCost: number;
  TravelCost: number;
  HotelCost: number;
  TotalCost: number;
  RequiredManday: number;
  SelectedCountryName: string;
  SelectedProvinceName: string;
  SelectedLocationName: string;
  SelectedCountry: number;
  SelectedProvince: number;
  SelectedLocation: number;
  InspectionLevel: number;
  AcceptanceLevelCritical: number;
  AcceptanceLevelMajor: number;
  AcceptanceLevelMinor: number;
  SelectedProductIndex: number;
  DefaultSampleSizePermanday: number;
  SampleSize: number;
  ShipmentQty: number;
}