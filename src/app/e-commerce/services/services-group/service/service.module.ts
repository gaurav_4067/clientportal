import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {ServiceRoutingModule} from './service-routing.module';
import {ServiceComponent} from './service.component';

@NgModule({
  imports: [CommonModule, ServiceRoutingModule],
  declarations: [ServiceComponent],
  exports: [ServiceComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ServiceModule {
}
