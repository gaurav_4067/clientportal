import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ServicesGroupComponent} from './services-group.component';

const routes: Routes =
    [{path: 'services-group', component: ServicesGroupComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class ServicesGroupRoutingModule {
}
