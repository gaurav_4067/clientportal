import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {ServiceModule} from './service/service.module';
import {ServicesGroupRoutingModule} from './services-group-routing.module';
import {ServicesGroupComponent} from './services-group.component';

@NgModule({
  imports: [CommonModule, ServiceModule, ServicesGroupRoutingModule],
  declarations: [ServicesGroupComponent],
  exports: [ServicesGroupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class ServicesGroupModule {
}
