 

export class EcomPromoOfferList {
    public BusinessCategoryName: string;
    public BusinessCategoryId: number;
    public OfferDetailId: number;
    public OfferTypeId: number;
    public OfferType: string;
    public OfferName: string;
    public MinTransactionAmount: number;
    public MaxTransactionAmount: number;
    public OfferPercentDiscount: number;
    public ValidFrom: Date
    public ValidTo: Date
    public OfferDescription: string;
    public OfferShortDescription: string;
    public OfferCode: string;
    public ClientId: number;
    public UserId: number;
    public ProductId: number;
    public Active: boolean;
  }