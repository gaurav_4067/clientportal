import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ServicesGroupModule } from './services-group/services-group.module';
import { ServicesRoutingModule } from './services-routing.module';
import { ServicesComponent } from './services.component';

@NgModule({
  imports:
    [CommonModule, FormsModule, ServicesGroupModule, ServicesRoutingModule],
  declarations: [ServicesComponent],
  exports: [ServicesComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ServicesModule {
}
