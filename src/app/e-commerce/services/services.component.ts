import { Component, Input, OnInit } from '@angular/core';

import { BaseUrl, DataService, ETRFUrl } from '../../mts.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  serviceGroups = [];
  isLoader: boolean;
  userManuals: string[] = [];
  @Input() data: any;

  public groupSelectedHandler(id) {
    this.serviceGroups.forEach(x => {
      if (x.id !== id) {
        x.isSelected = false;
      }
    })
  }
  constructor(public dataService: DataService) { }

  ngOnInit() {
    (<any>$('[data-toggle="tooltip"]')).tooltip();
    this.isLoader = true;
    var baseurl = ETRFUrl + "Files/EcommerceImages/";
    this.dataService.get('Package/GetAllServices').subscribe(
      response => {
        this.isLoader = false;
        // alert("rohit");
        var data = response;

        var unique = {};
        this.serviceGroups = [];
        for (var i in data.Services) {
          if (typeof (unique[data.Services[i].Category]) == "undefined") {
            this.serviceGroups.push({ id: data.Services[i].Category.replace(' ', '_'), name: data.Services[i].Category, services: [], isSelected: false, OfferDescription: data.Services[i].OfferDescription, showHeader: false });
          }
          unique[data.Services[i].Category] = '';
        }

        this.serviceGroups[0].isSelected = true;

        for (var j = 0; j < data.Services.length; j++) {
          var sg = this.serviceGroups.find(s => s.name == data.Services[j].Category);
          var valid = this.offerValidation(data.Services[j]);
          let offerId = 0
          if (valid) {
            offerId = data.Services[j].OfferId;
          }

          sg.services.push({
            id: data.Services[j].ServiceId,
            name: data.Services[j].Name,
            body: data.Services[j].Description,
            URL: baseurl + data.Services[j].ServiceImageURL,
            categoryId: data.Services[j].CategoryId,
            OfferId: offerId,
            OfferName: data.Services[j].OfferName,
            OfferDescription: data.Services[j].OfferDescription,
            OfferShortDescription: data.Services[j].OfferShortDescription,
            OfferPercent: data.Services[j].OfferPercent,
            ValidFrom: data.Services[j].OfferValidFrom,
            ValidTo: data.Services[j].OfferValidTo,
            OfferActive: valid
          })
        }
      });
  }

  offerValidation(serviceObject): any {
    var b = false;
    if (serviceObject.OfferActive) {
      var dateFrom = this.getFormattedDate(new Date(serviceObject.OfferValidFrom));
      var dateTo = this.getFormattedDate(new Date(serviceObject.OfferValidTo));
      var currentDate = this.getFormattedDate(new Date());
      var from = new Date(dateFrom);
      var to = new Date(dateTo);
      var check = new Date(currentDate);
      if (from <= check) {
        if (to >= check) {
          b = true;
        } else {
          b = false;
        }
      } else {
        b = false;
      }
    }
    return b;
  }
  getFormattedDate(date: Date) {
    return ((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear());
  }

  openUserManual(): void {
    this.isLoader = true;
    this.dataService.get('Package/GetEcommUserManual')
      .subscribe(
        response => {
          if (response.IsSuccess) {
            this.userManuals = response.Data;
            this.isLoader = false;
            (<any>$('#ecomm-user-manual')).modal('show');
          } else {
            this.isLoader = false;
          }
        },
        error => {
          this.isLoader = false;
        });
  }

  downloadManual(fileName: string): void {
    window.location.href =
      BaseUrl + 'Package/DownloadManual?ManualName=' + fileName;
    setTimeout(() => {
      (<any>$('#ecomm-user-manual')).modal('hide');
    }, 200);
  }
}
