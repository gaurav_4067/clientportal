import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {ECommerceHeaderModule} from '../e-commerce-header/e-commerce-header.module';
import {FooterModule} from '../footer/footer.module';
import {AddEtrfModule} from '../online-etrf/add-etrf/add-etrf.module';

import {ECommerceRoutingModule} from './e-commerce-routing.module';
import {ECommerceComponent} from './e-commerce.component';
import {OrderReviewModule} from './services/services-group/service/package/order-review/order-review.module';
import {PackageModule} from './services/services-group/service/package/package.module';
import {ServicesModule} from './services/services.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, ECommerceHeaderModule, OrderReviewModule,
    AddEtrfModule, FooterModule, PackageModule, ServicesModule,
    ECommerceRoutingModule
  ],
  declarations: [ECommerceComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ECommerceModule {
}
