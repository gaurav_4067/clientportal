import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ECommerceComponent} from './e-commerce.component';

const routes: Routes = [{
  path: '',
  children: [
    {path: '', component: ECommerceComponent}, {
      path: 'order-review',
      loadChildren: () => new Promise(
          resolve => {
              (require as any)
                  .ensure(
                      [],
                      require => {
                        resolve(
                            require(
                                './services/services-group/service/package/order-review-copy/order-review-copy.module')
                                .OrderReviewCopyModule);
                      })})
    },
    {
      path: 'order-review/:data/:id',
      loadChildren: () => new Promise(
          resolve => {
              (require as any)
                  .ensure(
                      [],
                      require => {
                        resolve(
                            require(
                                './services/services-group/service/package/order-review-copy/order-review-copy.module')
                                .OrderReviewCopyModule);
                      })})
    }
  ]
}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class ECommerceRoutingModule {
}
