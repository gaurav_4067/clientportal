import { ECommerceModule } from './e-commerce.module';

describe('ECommerceModule', () => {
  let eCommerceModule: ECommerceModule;

  beforeEach(() => {
    eCommerceModule = new ECommerceModule();
  });

  it('should create an instance', () => {
    expect(eCommerceModule).toBeTruthy();
  });
});
