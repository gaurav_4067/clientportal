import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MyOrderConfigurationComponent } from './my-order-configuration.component';

const routes: Routes = [
  { path: '', component: MyOrderConfigurationComponent }
];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class MyOrderConfigurationRoutingModule {
}
