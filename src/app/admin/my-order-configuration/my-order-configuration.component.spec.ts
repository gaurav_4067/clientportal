import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyOrderConfigurationComponent } from './my-order-configuration.component';

describe('MyOrderConfigurationComponent', () => {
  let component: MyOrderConfigurationComponent;
  let fixture: ComponentFixture<MyOrderConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyOrderConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyOrderConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
