import { MyOrderConfigurationModule } from './my-order-configuration.module';

describe('MyOrderConfigurationModule', () => {
  let myOrderConfigurationModule: MyOrderConfigurationModule;

  beforeEach(() => {
    myOrderConfigurationModule = new MyOrderConfigurationModule();
  });

  it('should create an instance', () => {
    expect(myOrderConfigurationModule).toBeTruthy();
  });
});
