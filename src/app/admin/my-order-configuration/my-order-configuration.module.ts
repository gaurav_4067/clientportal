import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';
import {MdInputModule} from '@angular2-material/input';
import {Ng2AutoCompleteModule} from 'ng2-auto-complete';

import {FooterModule} from '../../footer/footer.module';
import {HeaderModule} from '../../header/header.module';

import {MyOrderConfigurationRoutingModule} from './my-order-configuration-routing.module';
import {MyOrderConfigurationComponent} from './my-order-configuration.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, FooterModule, HeaderModule,
    Ng2AutoCompleteModule, MyOrderConfigurationRoutingModule, MdInputModule,
    MaterialModule
  ],
  declarations: [MyOrderConfigurationComponent]
})
export class MyOrderConfigurationModule {
}
