import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoContentComponent } from '../no-content/no-content.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: NoContentComponent
      }, {
        path: 'client-configuartion-list',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./client-configuration-list/client-configuration-list.module').ClientConfigurationListModule);
          })
        })
      },
      {
        path: 'manage-roles',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./manage-roles/manage-roles.module').ManageRolesModule);
          })
        })
      },
      {
        path: 'manage-users',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./manage-users/manage-users.module').ManageUsersModule);
          })
        })
      },
      {
        path: 'company-mapping',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./company-mapping/company-mapping.module').CompanyMappingModule);
          })
        })
      },
      {
        path: 'my-order-configuration',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./my-order-configuration/my-order-configuration.module').MyOrderConfigurationModule);
          })
        })
      },
      {
        path: 'order-analysis-configuration',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./order-analysis-configuration/order-analysis-configuration.module').OrderAnalysisConfigurationModule);
          })
        })
      },
      {
        path: 'etrf-configuration',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./etrf-configuration/etrf-configuration.module').EtrfConfigurationModule);
          })
        })
      },
      {
        path: 'manage-promooffer',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./manage-promooffer/manage-promooffer.module').ManagePromoofferModule);
          })
        })
      },  
      {
        path: 'ecommerce-user',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./ecommerce-user/ecommerce-user.module').EcommerceUserModule);
          })
        })
      },
      {
        path: 'etrf-listing-configuartion',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./etrf-listing/etrf-listing.module').EtrfListingModule);
          })
        })
      },
      {
        path: 'manage-order-configuration-type',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./manage-order-configuration-type/manage-order-configuration-type.module').ManageOrderConfigurationTypeModule);
          })
        })
      }



    ]
  }
];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class AdminRoutingModule {
}
