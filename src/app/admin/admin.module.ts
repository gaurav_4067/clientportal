import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminRoutingModule } from './admin-routing.module';
import { NoContentModule } from '../no-content/no-content.module';

@NgModule({
  imports: [
    NoContentModule, AdminRoutingModule
  ],
  declarations: []
})
export class AdminModule {
}
