import { Component, OnInit } from '@angular/core';
import { DataService, BaseUrl } from '../../mts.service';
import { Router } from '@angular/router';
import {
  SearchModel, ClientConfigurationList, ConfigurationList, CompanyList,
  ConfiguredUser, UserSearchModel, MappedCompanyList
} from './client-configuration-list.model';

@Component({
  selector: 'app-client-configuration-list',
  templateUrl: './client-configuration-list.component.html',
  styleUrls: ['./client-configuration-list.component.css']
})
export class ClientConfigurationListComponent implements OnInit {
  searchModel: SearchModel;
  configurationList: ConfigurationList;
  configuredUserList: Array<ConfiguredUser>;
  companyList: Array<CompanyList>;
  userSearchModel: UserSearchModel;
  vendorList: Array<MappedCompanyList>;
  agentList: Array<MappedCompanyList>;
  manufacturerList: Array<MappedCompanyList>;
  clientList: Array<MappedCompanyList>;
  public p: any = 1;
  private totalItem: number;
  private itemPerPage: 1;
  CompanyName: string;
  isLoader: boolean;
  userCredential: any;
  userDetail: any;
  totalRecord: number;
  currentPage: number = 1;
  paginationPageSize: number = 1;

  constructor(
    private dataService: DataService,
    private router: Router) {
    this.searchModel = new SearchModel();
    this.configurationList = new ConfigurationList();
    this.companyList = new Array<CompanyList>();
    this.configuredUserList = new Array<ConfiguredUser>();
    this.userSearchModel = new UserSearchModel();
    this.vendorList = Array<MappedCompanyList>();
    this.agentList = Array<MappedCompanyList>();
    this.manufacturerList = Array<MappedCompanyList>();
    this.clientList = Array<MappedCompanyList>();
  }
  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      if (this.userCredential.UserPermission.ViewUserList) {
        this.searchModel.CompanyName = '';
        this.searchModel.BusinessCategory = 0;
        this.searchModel.ReleaseStatus = 0;
        this.searchModel.ItemPerPage = 10;
        this.searchModel.PageNumber = 1;
        this.itemPerPage = 1;
        this.isLoader = true;
        this.dataService.get('Master/GetCompanyList?BusinessCategoryId=' + this.userCredential.BusinessCategoryId).subscribe(r => {
          this.companyList = r.CompanyList;
        });
        this.GetClientConfigurationList();
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  resetSearchForm() {
    this.CompanyName = '';
    this.searchModel = new SearchModel();
    this.search();
  }

  public renderCompany(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CompanyName}</div>
      </div>`;

    return html;
  }

  GetClientConfigurationList() {
    this.dataService.post('ClientConfiguration/GetClientConfigurationList', this.searchModel).subscribe(r => {
      this.isLoader = false;
      let that = this;
      that.configurationList.clientConfigurationList = r.ClientConfigurationList;
      that.totalItem = Math.ceil(r.TotalRecord / that.searchModel.ItemPerPage);
      that.p = this.searchModel.PageNumber;
    });
  }

  search() {
    this.isLoader = true;
    this.searchModel.PageNumber = 1;
    this.GetClientConfigurationList();
  }

  getPage(page: number) {
    this.isLoader = true;
    this.searchModel.PageNumber = page;
    this.GetClientConfigurationList();
  }

  getBusinessCategory() {
    var that = this;
    this.isLoader = true;
    this.dataService
      .get(
      'Master/GetCompanyList?BusinessCategoryId=' +
      this.searchModel.BusinessCategory)
      .subscribe(r => {
        that.isLoader = false;
        this.searchModel.CompanyId = '';
        this.CompanyName = '';
        this.companyList = r.CompanyList;
      });
  }

  onCompanyChange(Company) {
    if (Company.CompanyId) {
      this.searchModel.CompanyId = Company.CompanyId
    }
  }

  ConfiguiredClientList(client) {
    this.isLoader = true;
    this.userSearchModel.PageNumber = 1
    this.userSearchModel.CompanyId = client.CompanyId;
    this.userSearchModel.BusinessCategoryId = client.BusinessCategory;
    this.dataService.post('ClientConfiguration/getConfiguredUserList', this.userSearchModel).subscribe(r => {
      this.configuredUserList = r.UserList;
      this.totalRecord = Math.ceil(r.TotalRecords / this.userSearchModel.PageSize);
      (<any>$('#CongfiguredClientList')).modal('show');
      this.currentPage = this.userSearchModel.PageNumber
      this.isLoader = false;
    });
  }

  viewRole(id: number) {
    var link = window.location.href.split('/#/')[0];
    window.open(link + '/#/admin/manage-roles/view-role/' + id);
  }

  getPopUpPage(page: number) {
    this.isLoader = true;
    this.userSearchModel.PageNumber = page;
    this.dataService.post('ClientConfiguration/getConfiguredUserList', this.userSearchModel).subscribe(r => {
      this.configuredUserList = r.UserList;
      this.totalRecord = Math.ceil(r.TotalRecords / this.userSearchModel.PageSize);
      this.isLoader = false;
      this.currentPage = this.userSearchModel.PageNumber
    });
  }

  CompanyMappingInfo(client) {
    this.isLoader = true;
    this.dataService
      .get(
      'ClientConfiguration/GetCompanyMapping?CompanyId=' + client.CompanyId +
      '&BusinessCategoryId=' + client.BusinessCategory)
      .subscribe(r => {
        this.isLoader = false;
        this.agentList = r.AgentList;
        this.vendorList = r.VendorList;
        this.manufacturerList = r.ManufactureList;
        (<any>$('#MappedCompanyList')).modal('show');
      });
  }

  viewClientList(Type: String, client) {
    if (Type == "Client") {
      this.CompanyMappingInfo(client);
    } else {
      this.isLoader = true;
      var obj = {
        'SelectedCompanyType': Type,
        'CompanyId': client.CompanyId,
        'BusinessCategoryId': client.BusinessCategory
      }
      this.dataService.post('ClientConfiguration/getClientList', obj).subscribe(r => {
        this.clientList = r;
        (<any>$('#ComapnyType')).modal('show');
        this.isLoader = false;
      });
    }
  }

  exportToExcel() {
    this.isLoader=true;
    this.searchModel.PageNumber = 1;
    this.dataService.post('ClientConfiguration/ExportToExcelClientConfigurationList', this.searchModel).subscribe(r => {
      if (r.IsSuccess) {
        this.isLoader = false;
        let downloadFileName = encodeURIComponent(r.Data);
        window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + r.Data + "&fileType=excel&businessCategoryId="+this.userCredential.BusinessCategoryId;
      }
    });
  }

  ExportToExcelUserList() {
    this.isLoader = true;
    this.userSearchModel.PageNumber = 1;
    this.dataService.post('ClientConfiguration/ExportToExcelConfiguratedUserList', this.userSearchModel).subscribe(r => {
      if (r.IsSuccess) {
        this.isLoader = false;
        let downloadFileName = encodeURIComponent(r.Data);
        window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + r.Data + "&fileType=excel&businessCategoryId="+this.userCredential.BusinessCategoryId;
      }
    });
  }

}
