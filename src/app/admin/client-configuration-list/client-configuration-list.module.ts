import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientConfigurationListRoutingModule } from './client-configuration-list-routing.module';
import { ClientConfigurationListComponent } from './client-configuration-list.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { ManageUserRoutingModule } from 'app/admin/manage-users/manage-users-routing.module';
import { FormsModule } from '@angular/forms';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { Ng2PaginationModule } from 'ng2-pagination';
import { MaterialModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    FooterModule,
    FormsModule,
    MaterialModule,
    Ng2AutoCompleteModule,
    Ng2PaginationModule,
    ClientConfigurationListRoutingModule
  ],
  declarations: [ClientConfigurationListComponent]
})
export class ClientConfigurationListModule { }
