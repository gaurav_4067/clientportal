
export class SearchModel {
    public PageNumber: number = 1;
    public ItemPerPage: number = 10;
    public BusinessCategory: number = 0;
    public ReleaseStatus: Number = 0;
    public CompanyName: String = '';
    public Order: string = 'asc';
    public OrderBy: string = 'CompanyName';
    public CompanyId: any = '';
};


export class ConfigurationList {
    public ConfigurationList() {
        this.clientConfigurationList = new Array<ClientConfigurationList>();
    }
    public TotalRecord: number;
    public clientConfigurationList: Array<ClientConfigurationList>
}

export class ClientConfigurationList {
    public SNO: number;
    public CompanyId: number;
    public BusinessCategory: number;
    public BusinessCategoryName: string;
    public ReleaseStatus: string;
    public CompanyName: String;
    public ReleaseDate: any;
    public isVendor: boolean;
    public isAgent: boolean;
    public isManufacturer: boolean;
    public CompanyType: boolean;
}

export class CompanyList {
    public CompanyId: any;
    public CompanyName: string;
}

export class ConfiguredUser {
    public ConfiguredUser() {
        this.UserRoles = new Array<UserRole>();
    }
    public UserId: number;
    public UserName: string;
    public BusinessCategoryId: number;
    public CountryName: string;
    public CityName: string;
    public UserCreatedDate: string;
    public UserCategoryName: string;
    public UserRoles: Array<UserRole>;
}

export class UserRole {
    public RoleId: number;
    public RoleName: string;
}

export class UserSearchModel {
    public CompanyId: number;
    public BusinessCategoryId: number;
    public Order: string = 'asc'
    public orderBy: string = 'UserName'
    public PageNumber: number = 1;
    public PageSize: number = 10;
}

export class MappedCompanyList {
    public CompanyId: number;
    public CompanyName: string;
    public Email: string;
    public CompanyAddress: string;
    public LaborityEmail: string;
    public ContactNo: string;
}



