import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientConfigurationListComponent } from './client-configuration-list.component';

describe('ClientConfigurationListComponent', () => {
  let component: ClientConfigurationListComponent;
  let fixture: ComponentFixture<ClientConfigurationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientConfigurationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientConfigurationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
