import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientConfigurationListComponent } from 'app/admin/client-configuration-list/client-configuration-list.component';

const routes: Routes = [{
  path:'',
  component:ClientConfigurationListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientConfigurationListRoutingModule { }
