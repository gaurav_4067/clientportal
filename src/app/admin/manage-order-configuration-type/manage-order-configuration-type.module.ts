import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageOrderConfigurationTypeRoutingModule } from './manage-order-configuration-type-routing.module';
import { ManageOrderConfigurationTypeComponent } from './manage-order-configuration-type.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { FormsModule } from '@angular/forms';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

@NgModule({
  imports: [
    CommonModule,
    ManageOrderConfigurationTypeRoutingModule,
    HeaderModule,
    FooterModule,
    FormsModule,
    Ng2AutoCompleteModule
  ],
  declarations: [ManageOrderConfigurationTypeComponent]
})
export class ManageOrderConfigurationTypeModule { }
