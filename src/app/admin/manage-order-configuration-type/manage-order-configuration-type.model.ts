export class CompanyConfigurationTypeList {
    public CompanyId: number;
    public CompanyName: string;
    public BusinessCategoryId: number;
    public BusinessCategoryName: string;
    public ConfigurationType: number;
    public ActiveConfiguration: number;
    public ConfigurationVersion: string;
}