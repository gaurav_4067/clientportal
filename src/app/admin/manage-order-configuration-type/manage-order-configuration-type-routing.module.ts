import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageOrderConfigurationTypeComponent } from 'app/admin/manage-order-configuration-type/manage-order-configuration-type.component';

const routes: Routes = [{
  path:'',component:ManageOrderConfigurationTypeComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageOrderConfigurationTypeRoutingModule { }
