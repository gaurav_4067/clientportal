import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageOrderConfigurationTypeComponent } from './manage-order-configuration-type.component';

describe('ManageOrderConfigurationTypeComponent', () => {
  let component: ManageOrderConfigurationTypeComponent;
  let fixture: ComponentFixture<ManageOrderConfigurationTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageOrderConfigurationTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageOrderConfigurationTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
