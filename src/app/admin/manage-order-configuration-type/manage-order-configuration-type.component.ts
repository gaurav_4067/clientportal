import { Component, OnInit } from '@angular/core';
import { DataService } from '../../mts.service';
import { Router } from '@angular/router';
import { CompanyConfigurationTypeList } from './manage-order-configuration-type.model'

declare let swal: any;

@Component({
  selector: 'app-manage-order-configuration-type',
  templateUrl: './manage-order-configuration-type.component.html',
  styleUrls: ['./manage-order-configuration-type.component.css']
})
export class ManageOrderConfigurationTypeComponent implements OnInit {
  companyList: any;
  userDetail: any;
  userCredential: any;
  isLoader: boolean;

  public selectedBusinessCategory: any = 0;
  public selectedCompany: any;
  bussinessCategories: any = [];
  companyConfigurationTypeList: Array<CompanyConfigurationTypeList>;

  constructor(private dataService: DataService, private router: Router) {
    this.companyConfigurationTypeList = new Array<CompanyConfigurationTypeList>();
  }

  ngOnInit() {
    this.isLoader = true;
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      if (this.userCredential.UserPermission.ViewManageOrderConfigurationType) {
        this.dataService.get('DynamicFormCreation/BussinessType').subscribe(r => {
          if (r.IsSuccess) {
            this.bussinessCategories = r.Data;
            this.selectedBusinessCategory = this.userCredential.BusinessCategoryId;
            this.GetCompanyList();
          }
        });

      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  GetCompanyList() {
    this.dataService
      .get(
      'master/GetCompanyList?BusinessCategoryId=' +
      this.selectedBusinessCategory)
      .subscribe(r => {
        this.companyList = r.CompanyList;
        this.isLoader = false;
      });
  }

  public renderCompany(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CompanyName}</div>
      </div>`;
    return html;
  }

  getBusinessCategory() {
    var that = this;
    this.isLoader = true;
    this.selectedCompany = '';
    this.GetCompanyList();
  }


  GetCompanyConfigurationType() {
    this.dataService
      .get(
      'master/GetCompanyConfigurationType?BusinessCategoryId=' +
      this.selectedBusinessCategory + '&CompanyId=' + this.selectedCompany.CompanyId)
      .subscribe(r => {
        this.isLoader = false;
        this.companyConfigurationTypeList = r.Data;
      });
  }

  resetParameters() {
    this.selectedBusinessCategory = this.userCredential.BusinessCategoryId;
    this.selectedCompany = '';
    this.companyConfigurationTypeList = new Array<CompanyConfigurationTypeList>();
    this.GetCompanyList();
  }

  UpdateConfigurationType(company) {
    this.isLoader = true;
    let obj = {
      CompanyId: company.CompanyId,
      ConfigurationType: company.ConfigurationType,
      BusinessCategoryId: company.BusinessCategoryId,
      ModifiedBy: this.userCredential.UserId
    }
    this.dataService
      .post(
      'master/UpdateConfigurationType', obj)
      .subscribe(r => {
        this.GetCompanyConfigurationType();
        this.isLoader = false;
        swal('', r.Message);
      });
  }

  GetStatus(Company) {
    let result
    if (Company.ActiveConfiguration == Company.ConfigurationType) {
      result = "Active"
    }
    else if (Company.ActiveConfiguration != Company.ConfigurationType) {
      result = "Inactive"
    }
    return result;
  }

}
