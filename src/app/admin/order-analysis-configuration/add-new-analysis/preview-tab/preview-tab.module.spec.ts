import { PreviewTabModule } from './preview-tab.module';

describe('PreviewTabModule', () => {
  let previewTabModule: PreviewTabModule;

  beforeEach(() => {
    previewTabModule = new PreviewTabModule();
  });

  it('should create an instance', () => {
    expect(previewTabModule).toBeTruthy();
  });
});
