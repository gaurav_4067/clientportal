import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

import {DataService} from '../../../../mts.service';
import {AddNewChartModel} from '../add-new-analysis';

declare let swal: any;

@Component({
  selector: 'app-preview-tab',
  templateUrl: './preview-tab.component.html',
  styleUrls: ['./preview-tab.component.css']
})
export class PreviewTabComponent implements OnInit {
  @Output() nextButtonClick = new EventEmitter();
  @Output() prevButtonClick = new EventEmitter();

  // graphJson:any;
  // graphName: any;
  // legendPosition: any;
  // data: any = [];
  isLoader: boolean;
  isMultigraph: any = false;

  constructor(private router: Router, private dataService: DataService) {
    // this.graphJson = JSON.parse(AddNewChartModel.GraphJsons);
    // this.graphName = AddNewChartModel.GraphName;
    // this.legendPosition = (this.graphJson.GraphLegendPosition).toLowerCase();
  }

  ngOnInit() {
    if (AddNewChartModel.isMultipleGraph) {
      this.isMultigraph = true;
    } else {
      this.isMultigraph = false;
    }


    // var dummyData = [{
    //   "country": "USA",
    //   "visits": 3025,
    //   "color": "#FF0F00"
    // }, {
    //   "country": "China",
    //   "visits": 1882,
    //   "color": "#FF6600"
    // }, {
    //   "country": "Japan",
    //   "visits": 1809,
    //   "color": "#FF9E01"
    // }, {
    //   "country": "Germany",
    //   "visits": 1322,
    //   "color": "#FCD202"
    // }, {
    //   "country": "UK",
    //   "visits": 1122,
    //   "color": "#F8FF01"
    // }, {
    //   "country": "France",
    //   "visits": 1114,
    //   "color": "#B0DE09"
    // }, {
    //   "country": "India",
    //   "visits": 984,
    //   "color": "#04D215"
    // }, {
    //   "country": "Spain",
    //   "visits": 711,
    //   "color": "#0D8ECF"
    // }, {
    //   "country": "Netherlands",
    //   "visits": 665,
    //   "color": "#0D52D1"
    // }, {
    //   "country": "Russia",
    //   "visits": 580,
    //   "color": "#2A0CD0"
    // }, {
    //   "country": "South Korea",
    //   "visits": 443,
    //   "color": "#8A0CCF"
    // }, {
    //   "country": "Canada",
    //   "visits": 441,
    //   "color": "#CD0D74"
    // }];

    // var dummyBarStackData = [{
    //       "year": "2003",
    //       "europe": 2.5,
    //       "namerica": 2.5,
    //       "asia": 2.1,
    //       "lamerica": 0.3,
    //       "meast": 0.2,
    //       "africa": 0.1
    //   }, {
    //       "year": "2004",
    //       "europe": 2.6,
    //       "namerica": 2.7,
    //       "asia": 2.2,
    //       "lamerica": 0.3,
    //       "meast": 0.3,
    //       "africa": 0.1
    //   }, {
    //       "year": "2005",
    //       "europe": 2.8,
    //       "namerica": 2.9,
    //       "asia": 2.4,
    //       "lamerica": 0.3,
    //       "meast": 0.3,
    //       "africa": 0.1
    //   }, {
    //       "year": "2006",
    //       "europe": 2.9,
    //       "namerica": 1.9,
    //       "asia": 2.8,
    //       "lamerica": 1.3,
    //       "meast": 0.9,
    //       "africa": 0.4
    //   }, {
    //       "year": "2007",
    //       "europe": 1.8,
    //       "namerica": 1.9,
    //       "asia": 2.1,
    //       "lamerica": 0.4,
    //       "meast": 0.8,
    //       "africa": 0.2
    //   }];

    //   // ready the json data as per x-axis and y-axis name
    //   var xAxis = this.graphJson.GraphXAxis;
    //   var yAxis = this.graphJson.GraphYAxis;

    //   if(xAxis != "" && yAxis != "")
    //   {
    //     dummyData.forEach((content)=>{
    //       let obj = {};
    //       obj[xAxis] = content.country;
    //       obj[yAxis] = content.visits;
    //       obj["color"] = content.color;
    //       this.data.push(obj);
    //     })
    //   }

    //   // call the graph
    //   if(AddNewChartModel.ChartType == "1")
    //   {
    //     this.showBarChart(this.data);
    //   }
    //   else if(AddNewChartModel.ChartType == "2")
    //   {
    //     this.showBarStackChart(dummyBarStackData);
    //   }
    //   else if(AddNewChartModel.ChartType == "3")
    //   {
    //     this.showPieChart(this.data);
    //   }
    //   else if(AddNewChartModel.ChartType == "4")
    //   {
    //     this.showDonutChart(this.data);
    //   }
    //   //this.showBarChart(dummyData);
  }

  // showBarChart(chartData: any)
  // {
  //   AmCharts.addInitHandler(function(chart) {
  //   if (!chart.legend || !chart.legend.enabled ||
  //   !chart.legend.generateFromData)
  //   {
  //     return;
  //   }

  //   var categoryField = chart.categoryField;
  //   var colorField = chart.graphs[0].lineColorField ||
  //   chart.graphs[0].fillColorsField; var legendData =
  //   chart.dataProvider.map(function(data) {
  //     var markerData = {
  //       "title": data[categoryField] + ": " +
  //       data[chart.graphs[0].valueField], "color": data[colorField]
  //     };
  //     if (!markerData.color)
  //     {
  //       markerData.color = chart.graphs[0].lineColor;
  //     }
  //     return markerData;
  //   });

  //   chart.legend.data = legendData;
  //   }, ["serial"]);

  //   var chart = AmCharts.makeChart("chartContainer", {
  //   "theme": "light",
  //   "type": "serial",
  //   "startDuration": 1,
  //   "titles": [{
  //     "text": this.graphName,
  //     "size": 16
  //   }],
  //   "dataProvider": chartData,
  //   "legend": {
  //     "generateFromData": true,
  //     "position": this.legendPosition
  //   },
  //   "valueAxes": [{
  //     "position": "left",
  //     "title": this.graphJson.GraphXAxis
  //   }],
  //   "graphs": [{
  //     "balloonText": "[[category]]: <b>[[value]]</b>",
  //     "fillColorsField": "color",
  //     "fillAlphas": 1,
  //     "lineAlpha": 0.1,
  //     "type": "column",
  //     "valueField": this.graphJson.GraphYAxis
  //   }],
  //   "depth3D": this.graphJson.GraphAppearance == "2D" ? 0 : 20,
  //   "angle": this.graphJson.GraphAppearance == "2D" ? 0 : 30,
  //   "chartCursor": {
  //     "categoryBalloonEnabled": false,
  //     "cursorAlpha": 0,
  //     "zoomable": false
  //   },
  //   "categoryField": this.graphJson.GraphXAxis,
  //   "categoryAxis": {
  //     "gridPosition": "start",
  //     "labelRotation": 90,
  //     "title": this.graphJson.GraphYAxis
  //   },
  //   "export": {
  //     "enabled": true
  //   }
  //   });
  // }

  // showPieChart(chartData: any)
  // {
  //   var chart = AmCharts.makeChart( "chartContainer", {
  //     "type": "pie",
  //     "theme": "light",
  //     "titles": [{
  //       "text": this.graphName,
  //       "size": 16
  //     }],
  //     "dataProvider": chartData,
  //     "legend": {
  //       "position": this.legendPosition
  //     },
  //     "valueField": this.graphJson.GraphYAxis,
  //     "titleField": this.graphJson.GraphXAxis,
  //     "outlineAlpha": 0.4,
  //     "depth3D": this.graphJson.GraphAppearance == "2D" ? 0 : 15,
  //     "balloonText": "[[title]]<br><span
  //     style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  //     "angle": this.graphJson.GraphAppearance == "2D" ? 0 : 30,
  //     "export": {
  //       "enabled": true
  //     }
  //   });
  // }

  // showDonutChart(chartData: any)
  // {
  //   var chart = AmCharts.makeChart( "chartContainer", {
  //     "type": "pie",
  //     "theme": "light",
  //     "titles": [ {
  //       "text": this.graphName,
  //       "size": 16
  //     } ],
  //     "dataProvider": chartData,
  //     "legend": {
  //       "position": this.legendPosition
  //     },
  //     "valueField": this.graphJson.GraphYAxis,
  //     "titleField": this.graphJson.GraphXAxis,
  //     "startEffect": "elastic",
  //     "startDuration": 2,
  //     "labelRadius": 15,
  //     "innerRadius": "50%",
  //     "depth3D": this.graphJson.GraphAppearance == "2D" ? 0 : 10,
  //     "balloonText": "[[title]]<br><span
  //     style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  //     "angle": this.graphJson.GraphAppearance == "2D" ? 0 : 15,
  //     "export": {
  //       "enabled": true
  //     }
  //   });
  // }

  // showBarStackChart(chartData: any)
  // {
  //   var chart = AmCharts.makeChart("chartContainer", {
  //   "type": "serial",
  //   "theme": "light",
  //   "titles": [ {
  //       "text": this.graphName,
  //       "size": 16
  //     } ],
  //   "legend": {
  //       "autoMargins": false,
  //       "borderAlpha": 0,
  //       "equalWidths": false,
  //       "horizontalGap": 10,
  //       "markerSize": 10,
  //       "useGraphSettings": true,
  //       "valueAlign": "left",
  //       "valueWidth": 0,
  //       "position": this.legendPosition
  //   },
  //   "dataProvider": chartData,
  //   "valueAxes": [{
  //       "stackType": "100%",
  //       "axisAlpha": 0,
  //       "gridAlpha": 0,
  //       "labelsEnabled": false,
  //       "position": "left",
  //       "title": this.graphJson.GraphXAxis
  //   }],
  //   "graphs": [{
  //       "balloonText": "[[title]], [[category]]<br><span
  //       style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
  //       "fillAlphas": 0.9,
  //       "fontSize": 11,
  //       "labelText": "[[percents]]%",
  //       "lineAlpha": 0.5,
  //       "title": "Europe",
  //       "type": "column",
  //       "valueField": "europe"
  //   }, {
  //       "balloonText": "[[title]], [[category]]<br><span
  //       style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
  //       "fillAlphas": 0.9,
  //       "fontSize": 11,
  //       "labelText": "[[percents]]%",
  //       "lineAlpha": 0.5,
  //       "title": "North America",
  //       "type": "column",
  //       "valueField": "namerica"
  //   }, {
  //       "balloonText": "[[title]], [[category]]<br><span
  //       style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
  //       "fillAlphas": 0.9,
  //       "fontSize": 11,
  //       "labelText": "[[percents]]%",
  //       "lineAlpha": 0.5,
  //       "title": "Asia-Pacific",
  //       "type": "column",
  //       "valueField": "asia"
  //   }, {
  //       "balloonText": "[[title]], [[category]]<br><span
  //       style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
  //       "fillAlphas": 0.9,
  //       "fontSize": 11,
  //       "labelText": "[[percents]]%",
  //       "lineAlpha": 0.5,
  //       "title": "Latin America",
  //       "type": "column",
  //       "valueField": "lamerica"
  //   }, {
  //       "balloonText": "[[title]], [[category]]<br><span
  //       style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
  //       "fillAlphas": 0.9,
  //       "fontSize": 11,
  //       "labelText": "[[percents]]%",
  //       "lineAlpha": 0.5,
  //       "title": "Middle-East",
  //       "type": "column",
  //       "valueField": "meast"
  //   }, {
  //       "balloonText": "[[title]], [[category]]<br><span
  //       style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
  //       "fillAlphas": 0.9,
  //       "fontSize": 11,
  //       "labelText": "[[percents]]%",
  //       "lineAlpha": 0.5,
  //       "title": "Africa",
  //       "type": "column",
  //       "valueField": "africa"
  //   }],
  //   "marginTop": 40,
  //   "marginRight": 0,
  //   "marginLeft": 50,
  //   "marginBottom": 70,
  //   "autoMargins": false,
  //   "depth3D": this.graphJson.GraphAppearance == "2D" ? 0 : 20,
  //   "angle": this.graphJson.GraphAppearance == "2D" ? 0 : 10,
  //   "categoryField": "year",
  //   "categoryAxis": {
  //       "gridPosition": "start",
  //       "axisAlpha": 0,
  //       "gridAlpha": 0,
  //       "title": this.graphJson.GraphYAxis
  //   },
  //   "export": {
  //     "enabled": true
  //    }
  //   });
  // }

  showNextTab() {
    this.nextButtonClick.emit();
  }

  showPrevTab() {
    if (AddNewChartModel.isMultipleGraph == true) {
      this.prevButtonClick.emit('#querySecond');
    } else {
      this.prevButtonClick.emit('#query');
    }
  }

  save() {
    var that = this;
    that.isLoader = true;
    this.dataService.post('ManageGraph/SaveConfiguration', AddNewChartModel)
        .subscribe(response => {
          that.isLoader = false;
          if (response.IsSuccess) {
            swal({title: '', text: response.Message}, function() {
              that.router.navigate(['/admin/order-analysis-configuration']);
              swal.close();
            });
          } else {
            swal('', response.Message)
          }
        });
  }
}
