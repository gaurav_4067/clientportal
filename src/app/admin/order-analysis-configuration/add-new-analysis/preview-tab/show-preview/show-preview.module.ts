import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {ShowPreviewRoutingModule} from './show-preview-routing.module';
import {ShowPreviewComponent} from './show-preview.component';

@NgModule({
  imports: [CommonModule, ShowPreviewRoutingModule],
  declarations: [ShowPreviewComponent],
  exports: [ShowPreviewComponent]
})
export class ShowPreviewModule {
}
