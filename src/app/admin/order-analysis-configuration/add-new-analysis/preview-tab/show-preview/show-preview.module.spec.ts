import { ShowPreviewModule } from './show-preview.module';

describe('ShowPreviewModule', () => {
  let showPreviewModule: ShowPreviewModule;

  beforeEach(() => {
    showPreviewModule = new ShowPreviewModule();
  });

  it('should create an instance', () => {
    expect(showPreviewModule).toBeTruthy();
  });
});
