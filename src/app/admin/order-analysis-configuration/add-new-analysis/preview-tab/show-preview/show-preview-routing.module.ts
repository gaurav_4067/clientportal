import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ShowPreviewComponent} from './show-preview.component';

const routes: Routes =
    [{path: 'show-preview', component: ShowPreviewComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class ShowPreviewRoutingModule {
}
