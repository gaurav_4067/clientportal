import {Component, Input, OnInit} from '@angular/core';

import {AddNewChartModel} from '../../add-new-analysis';

@Component({
  selector: 'app-show-preview',
  templateUrl: './show-preview.component.html',
  styleUrls: ['./show-preview.component.css']
})
export class ShowPreviewComponent implements OnInit {
  graphClass: any = '';
  summaryClass: any = '';
  additionalSummaryClass: any = '';
  graphSummaryWrapper: any = '';
  graphTitle: any = 'Graph';
  summaryTitle: any = 'Summary';
  additionalSummaryTitle: any = 'Additional Graph Summary';
  graphName: any = '';

  @Input() count;

  ngOnInit() {
    if (this.count == 1) {
      this.graphName = AddNewChartModel.GraphName;
      if (AddNewChartModel.GraphType == '1') {
        if (AddNewChartModel.IsAdditionalSummary == true) {
          if (AddNewChartModel.GraphAdditionalSummaryPosition == '1') {
            this.graphClass = 'show-left-half margin-right';
            this.summaryClass = 'show-right-half margin-left';
            this.additionalSummaryClass = 'show-none';
            this.graphSummaryWrapper = '';
            this.summaryTitle = 'Additional Graph Summary';
          }
          if (AddNewChartModel.GraphAdditionalSummaryPosition == '2') {
            this.graphClass = 'show-left-half margin-left';
            this.summaryClass = 'show-right-half margin-right';
            this.additionalSummaryClass = 'show-none';
            this.graphSummaryWrapper = 'row-reverse';
            this.summaryTitle = 'Additional Graph Summary';
          }
          if (AddNewChartModel.GraphAdditionalSummaryPosition == '3') {
            this.graphClass = 'show-top-full';
            this.summaryClass = 'show-bottom-full';
            this.additionalSummaryClass = 'show-none';
            this.graphSummaryWrapper = 'col';
            this.summaryTitle = 'Additional Graph Summary';
          }
          if (AddNewChartModel.GraphAdditionalSummaryPosition == '4') {
            this.graphClass = 'show-top-full';
            this.summaryClass = 'show-bottom-full';
            this.additionalSummaryClass = 'show-none';
            this.graphSummaryWrapper = 'col-reverse';
            this.summaryTitle = 'Additional Graph Summary';
          }
        } else {
          this.graphClass = 'show-full';
          this.summaryClass = 'show-none';
          this.additionalSummaryClass = 'show-none';
        }
      } else if (AddNewChartModel.GraphType == '2') {
        if (AddNewChartModel.IsAdditionalSummary == true) {
          this.graphClass = 'show-none';
          this.summaryClass = 'show-full';
          this.summaryTitle = 'Summary';
          this.additionalSummaryClass = '';
        } else {
          this.graphClass = 'show-none';
          this.summaryClass = 'show-full';
          this.summaryTitle = 'Summary';
          this.additionalSummaryClass = 'show-none';
        }
      } else if (AddNewChartModel.GraphType == '3') {
        if (AddNewChartModel.GraphPosition == '1') {
          this.graphClass = 'show-left-half margin-right';
          this.summaryClass = 'show-right-half margin-left';
          this.graphSummaryWrapper = '';
          this.summaryTitle = 'Summary';
          if (AddNewChartModel.IsAdditionalSummary == true) {
            this.additionalSummaryClass = '';
          } else {
            this.additionalSummaryClass = 'show-none';
          }
        } else if (AddNewChartModel.GraphPosition == '2') {
          this.graphClass = 'show-left-half margin-left';
          this.summaryClass = 'show-right-half margin-right';
          this.graphSummaryWrapper = 'row-reverse';
          this.summaryTitle = 'Summary';
          if (AddNewChartModel.IsAdditionalSummary == true) {
            this.additionalSummaryClass = '';
          } else {
            this.additionalSummaryClass = 'show-none';
          }
        } else if (AddNewChartModel.GraphPosition == '3') {
          this.graphClass = 'show-top-full';
          this.summaryClass = 'show-bottom-full';
          this.graphSummaryWrapper = 'col';
          this.summaryTitle = 'Summary';
          if (AddNewChartModel.IsAdditionalSummary == true) {
            this.additionalSummaryClass = '';
          } else {
            this.additionalSummaryClass = 'show-none';
          }
        } else if (AddNewChartModel.GraphPosition == '4') {
          this.graphClass = 'show-top-full';
          this.summaryClass = 'show-bottom-full';
          this.graphSummaryWrapper = 'col-reverse';
          this.summaryTitle = 'Summary';
          if (AddNewChartModel.IsAdditionalSummary == true) {
            this.additionalSummaryClass = '';
          } else {
            this.additionalSummaryClass = 'show-none';
          }
        }
      }
    } else if (this.count == 2) {
      this.graphName = AddNewChartModel.GraphName2;
      if (AddNewChartModel.GraphType2 == '1') {
        if (AddNewChartModel.IsAdditionalSummary2 == true) {
          if (AddNewChartModel.GraphAdditionalSummaryPosition2 == '1') {
            this.graphClass = 'show-left-half margin-right';
            this.summaryClass = 'show-right-half margin-left';
            this.additionalSummaryClass = 'show-none';
            this.graphSummaryWrapper = '';
            this.summaryTitle = 'Additional Graph Summary';
          }
          if (AddNewChartModel.GraphAdditionalSummaryPosition2 == '2') {
            this.graphClass = 'show-left-half margin-left';
            this.summaryClass = 'show-right-half margin-right';
            this.additionalSummaryClass = 'show-none';
            this.graphSummaryWrapper = 'row-reverse';
            this.summaryTitle = 'Additional Graph Summary';
          }
          if (AddNewChartModel.GraphAdditionalSummaryPosition2 == '3') {
            this.graphClass = 'show-top-full';
            this.summaryClass = 'show-bottom-full';
            this.additionalSummaryClass = 'show-none';
            this.graphSummaryWrapper = 'col';
            this.summaryTitle = 'Additional Graph Summary';
          }
          if (AddNewChartModel.GraphAdditionalSummaryPosition2 == '4') {
            this.graphClass = 'show-top-full';
            this.summaryClass = 'show-bottom-full';
            this.additionalSummaryClass = 'show-none';
            this.graphSummaryWrapper = 'col-reverse';
            this.summaryTitle = 'Additional Graph Summary';
          }
        } else {
          this.graphClass = 'show-full';
          this.summaryClass = 'show-none';
          this.additionalSummaryClass = 'show-none';
        }
      } else if (AddNewChartModel.GraphType2 == '2') {
        this.graphClass = 'show-none';
        this.summaryClass = 'show-full';
        this.summaryTitle = 'Summary';
        if (AddNewChartModel.IsAdditionalSummary2 == true) {
          this.additionalSummaryClass = '';
        } else {
          this.additionalSummaryClass = 'show-none';
        }
      } else if (AddNewChartModel.GraphType2 == '3') {
        if (AddNewChartModel.GraphPosition == '1') {
          this.graphClass = 'show-left-half margin-right';
          this.summaryClass = 'show-right-half margin-left';
          this.graphSummaryWrapper = '';
          this.summaryTitle = 'Summary';
          if (AddNewChartModel.IsAdditionalSummary2 == true) {
            this.additionalSummaryClass = '';
          } else {
            this.additionalSummaryClass = 'show-none';
          }
        } else if (AddNewChartModel.GraphPosition == '2') {
          this.graphClass = 'show-left-half margin-left';
          this.summaryClass = 'show-right-half margin-right';
          this.graphSummaryWrapper = 'row-reverse';
          this.summaryTitle = 'Summary';
          if (AddNewChartModel.IsAdditionalSummary2 == true) {
            this.additionalSummaryClass = '';
          } else {
            this.additionalSummaryClass = 'show-none';
          }
        } else if (AddNewChartModel.GraphPosition == '3') {
          this.graphClass = 'show-top-full';
          this.summaryClass = 'show-bottom-full';
          this.graphSummaryWrapper = 'col';
          this.summaryTitle = 'Summary';
          if (AddNewChartModel.IsAdditionalSummary2 == true) {
            this.additionalSummaryClass = '';
          } else {
            this.additionalSummaryClass = 'show-none';
          }
        } else if (AddNewChartModel.GraphPosition == '4') {
          this.graphClass = 'show-top-full';
          this.summaryClass = 'show-bottom-full';
          this.graphSummaryWrapper = 'col-reverse';
          this.summaryTitle = 'Summary';
          if (AddNewChartModel.IsAdditionalSummary2 == true) {
            this.additionalSummaryClass = '';
          } else {
            this.additionalSummaryClass = 'show-none';
          }
        }
      }
    }
  }
}
