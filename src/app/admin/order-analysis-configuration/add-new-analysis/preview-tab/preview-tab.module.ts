import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {PreviewTabRoutingModule} from './preview-tab-routing.module';
import {PreviewTabComponent} from './preview-tab.component';
import {ShowPreviewModule} from './show-preview/show-preview.module';

@NgModule({
  imports: [CommonModule, ShowPreviewModule, PreviewTabRoutingModule],
  declarations: [PreviewTabComponent],
  exports: [PreviewTabComponent]
})
export class PreviewTabModule {
}
