import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PreviewTabComponent} from './preview-tab.component';

const routes: Routes = [{path: 'preview-tab', component: PreviewTabComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class PreviewTabRoutingModule {
}
