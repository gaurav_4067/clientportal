import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewAnalysisComponent } from './add-new-analysis.component';

describe('AddNewAnalysisComponent', () => {
  let component: AddNewAnalysisComponent;
  let fixture: ComponentFixture<AddNewAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
