import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddNewAnalysisComponent } from './add-new-analysis.component';

const routes: Routes = [{ path: '', component: AddNewAnalysisComponent }];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class AddNewAnalysisRoutingModule {
}
