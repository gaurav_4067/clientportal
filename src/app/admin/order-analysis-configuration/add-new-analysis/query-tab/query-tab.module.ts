import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';
import {MdCoreModule} from '@angular2-material/core';
import {MdInputModule} from '@angular2-material/input';
import {Md2Module} from 'md2';

import {QueryTabRoutingModule} from './query-tab-routing.module';
import {QueryTabComponent} from './query-tab.component';
import {UnionModule} from './union/union.module';

@NgModule({
  imports: [
    CommonModule, ReactiveFormsModule, FormsModule, MdInputModule,
    Md2Module.forRoot(), MdCoreModule, MaterialModule, UnionModule,
    QueryTabRoutingModule
  ],
  declarations: [QueryTabComponent],
  exports: [QueryTabComponent]
})
export class QueryTabModule {
}
