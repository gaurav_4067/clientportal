import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

declare let swal: any;

@Component({
  selector: 'app-union',
  templateUrl: './union.component.html',
  styleUrls: ['./union.component.css']
})
export class UnionComponent implements OnInit {
  unionRound: any = 0;
  addUnionIndex: any = '';
  unionObj: any;
  unionArray: any = [];
  unionIndex: any = 0;
  showClauseField: any = false;
  isBetween: any = false;
  dynamicCol: any = [];
  dynamicColAlias: any = [];
  unionControl: FormControl;
  filteredUnionOptions: any;


  @Input() leftlist: any;
  @Input() index: any;
  @Input() unionlistarr: any;
  @Input() dynamiccolarr: any;
  @Output() removeParentUnion: EventEmitter<string> = new EventEmitter();

  ngOnInit() {
    this.unionObj = {
      'column': '',
      'operator': '',
      'value': '',
      'value1': '',
      'value2': '',
      'clause': '',
      'index': ''
    }

    if (this.unionlistarr[this.index].length > 0) {
      this.showClauseField = true;
    }
    else {
      this.showClauseField = false;
    }

    this.dynamiccolarr.forEach((data) => {
      if (data.includes('As')) {
        var x = data.split('As');
        let y = x[0].split('\'');
        var z =
            x[1].substring(x[1].lastIndexOf('[') + 1, x[1].lastIndexOf(']'));
        this.dynamicCol.push(y[1]);
        this.dynamicColAlias.push(z);
      }
    })
  }

  ngOnChanges(changes: any) {
    this.unionControl = new FormControl();
    var data = changes.leftlist.currentValue;
    if (data.length) {
      this.filteredUnionOptions =
          this.unionControl.valueChanges.startWith(null).map(
              val => val ? this.filter(val) : this.leftlist.slice());
    }
  }

  filter(val: string): any {
    return this.leftlist.filter(
        option => new RegExp(`^${val}`, 'gi').test(option));
  }
  changeDynamicCol(e, index: any) {
    if (e.target.value != '') {
      var val = '\'' + e.target.value + '\' As [' + e.target.value + ']';
      this.dynamicColAlias[index] = e.target.value;
      // var val = this.dynamiccolarr[index];
      this.dynamiccolarr.splice(index, 1, val);
    } else {
      this.dynamicColAlias[index] = '';
      this.dynamiccolarr.splice(index, 1);
    }
  }

  dynamicColAliasChange(e, index: any) {
    var val = this.dynamiccolarr[index];
    var data = val.split('As');
    if (e.target.value != '') {
      data[1] = e.target.value;
    } else {
      data[1] = data[0];
    }

    var newVal = data[0] + ' As [' + data[1] + ']';
    this.dynamiccolarr.splice(index, 1, newVal);
  }

  removeUnion(outerIndex: any) {
    this.unionlistarr.splice(outerIndex, 1);
    this.dynamiccolarr.splice(outerIndex, 1);
    // alert(this.dynamiccolarr.length);
    // this.removeParentUnion.emit();
  }

  validateUnionCondition(data: any, index: any) {
    var validateResult = true;
    if (data.column == '' || data.operator == '') {
      validateResult = false;
    } else {
      if (data.clause == '' && index > 0) {
        validateResult = false;
      }
      if (data.operator != 'Between' && data.value == '') {
        validateResult = false;
      } else if (
          data.operator == 'Between' &&
          (data.value1 == '' || data.value2 == '')) {
        validateResult = false;
      }
    }

    return validateResult;
  }

  // 'union' code goes here
  // -----------------------------------------------------
  addUnion(outerIndex: any) {
    var rowObj = Object.assign({}, this.unionObj);
    var innerIndex = rowObj.index;
    var validateResult = this.validateUnionCondition(
        rowObj, typeof innerIndex == 'number' ? innerIndex : this.unionRound);

    if (validateResult == true) {
      // this.unionArray.push(rowObj);
      var temp = this.unionlistarr[outerIndex];

      if (innerIndex === '') {
        temp.push(rowObj)
      } else {
        temp.splice(innerIndex, 1, rowObj);
        this.unionObj.index = '';
      }

      this.unionlistarr.splice(outerIndex, 1, temp);

      // clearing all value from this.whereConditonObj object (holding 'where'
      // values)
      this.unionObj.column = '';
      this.unionObj.operator = '';
      this.unionObj.value = '';
      this.unionObj.value1 = '';
      this.unionObj.value2 = '';
      this.unionObj.clause = '';
      this.unionObj.index = '';

      this.addUnionIndex = '';
      this.unionRound++;
      this.isBetween = false;

      if (this.unionlistarr[outerIndex].length > 0) {
        this.showClauseField = true;
      } else {
        this.showClauseField = false;
      }
    } else {
      swal('', 'Please Fill All Fields');
    }
  }

  deleteUnion(e, innerIndex: any, outerIndex: any) {
    e.preventDefault();

    this.unionlistarr[outerIndex].splice(innerIndex, 1);
    this.unionRound--;
    if (this.unionlistarr[outerIndex].length == 1) {
      this.unionlistarr[outerIndex][0].clause = '';
    }
    if (this.unionlistarr[outerIndex].length == 0) {
      this.showClauseField = false;
    } else {
      this.showClauseField = true;
    }
  }

  editUnion(e, innerIndex: any, outerIndex: any) {
    if (innerIndex == 0) {
      this.showClauseField = false;
    }
    e.preventDefault();

    this.unionObj.index = innerIndex;
    this.unionIndex = innerIndex;
    var data = Object.assign({}, this.unionlistarr[outerIndex][innerIndex]);

    if (data.operator == 'Between') {
      this.isBetween = true;
    } else {
      this.isBetween = false;
    }

    this.unionObj.column = data.column;
    this.unionObj.operator = data.operator;
    this.unionObj.value = data.value;
    this.unionObj.value1 = data.value1;
    this.unionObj.value2 = data.value2;
    this.unionObj.clause = data.clause;
  }

  operatorChange() {
    var val = this.unionObj.operator;
    if (val == 'Between') {
      this.isBetween = true;
      this.unionObj.value = '';
    } else {
      this.isBetween = false;
      this.unionObj.value1 = '';
      this.unionObj.value2 = '';
    }
  }
}
