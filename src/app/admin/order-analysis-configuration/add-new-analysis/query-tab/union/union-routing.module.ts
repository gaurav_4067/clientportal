import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {UnionComponent} from './union.component';

const routes: Routes = [{path: 'union', component: UnionComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class UnionRoutingModule {
}
