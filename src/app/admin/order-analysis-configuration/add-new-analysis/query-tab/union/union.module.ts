import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';
import {MdInputModule} from '@angular2-material/input';
import {Md2Module, MdCoreModule} from 'md2';

import {UnionRoutingModule} from './union-routing.module';
import {UnionComponent} from './union.component';

@NgModule({
  imports: [
    CommonModule, ReactiveFormsModule, FormsModule, MdInputModule,
    Md2Module.forRoot(), MdCoreModule, MaterialModule, UnionRoutingModule
  ],
  declarations: [UnionComponent],
  exports: [UnionComponent]
})
export class UnionModule {
}
