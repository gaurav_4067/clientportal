import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Router} from '@angular/router';

import {DataService} from '../../../../mts.service';
import {AddNewChartModel} from '../add-new-analysis';

import {QueryTabModel} from './query-tab';

declare let swal: any;

@Component({
  selector: 'app-query-tab',
  templateUrl: './query-tab.component.html',
  styleUrls: ['./query-tab.component.css']
})
export class QueryTabComponent implements OnInit {
  tableShow: number;
  @Output() nextButtonClick = new EventEmitter();
  @Output() prevButtonClick = new EventEmitter();
  @Output() sendList = new EventEmitter();
  @Input() queryCount;

  constructor(private dataService: DataService, private router: Router) {}

  selectedTable: any = '';
  selectedFuction: any;
  selectedColumn: any;
  leftList: any = [];
  leftListArr: any = [];
  rightSelectColumn: any = [];
  whereConditonObj: any;
  whereConditionArr: any;
  unionListArr: any = [];
  unionObj: any;
  unionArr: any;
  orderByArr: any;
  orderByObj: any;
  isLoader: boolean;
  myControl: FormControl;
  filteredClients: any;

  public defaultSelectedCompany: any;
  private GetGraphColumns: any = [];
  private selectedSearchColumns: any;
  public tempselectedAvailG: any;
  public tempSelectedGridC: any = '';
  public AliasInput: any;

  orderBySelect: any;
  orderByColumn: any;
  orderByValue: any;
  query: any;

  top: any;
  userCredential: any;
  whereRound: any = 0;
  unionRound: any = 0;
  addWhereIndex: any = '';
  addUnionIndex: any = '';
  model: any;
  writequery: any = false;
  queryData: any;
  isBetween: any = false;
  dynamicColumn: any;
  isDynamicColumn: any = false;
  dynamicColArr: any = [];
  orderByAlias: any = [];
  WherePop: any = [];
  graphAdditionalQuery: any = false;
  additionQueryValidated: any = false;

  graphType: any;
  selectFunctionLastAlias: any = '';
  filteredOptions: any;
  whereList: any = [];

  ngOnInit() {
    this.tableShow = AddNewChartModel.BusinessCategoryId;
    this.myControl = new FormControl();

    if (this.queryCount == '1') {
      // Check Graph Type for Additional Graph Summary Position
      this.graphType = AddNewChartModel.GraphType;

      // Check for Write Query or Make Query
      if ((AddNewChartModel.GraphID != 0 &&
           AddNewChartModel.GraphQueryJson != '') ||
          (AddNewChartModel.GraphID != 0 &&
           AddNewChartModel.IsUserQuery == true)) {
        this.setValueForQuery1();
      } else if (
          (AddNewChartModel.GraphID == 0 &&
           AddNewChartModel.GraphQueryJson != '') ||
          (AddNewChartModel.GraphID == 0 &&
           AddNewChartModel.IsUserQuery == true)) {
        this.setValueForQuery1();
      } else {
        this.resetFields('ngInit');
      }
    }

    else if (this.queryCount == '2') {
      // Check Graph Type for Additional Graph Summary Position
      this.graphType = AddNewChartModel.GraphType2;

      // Check for Write Query or Make Query
      if ((AddNewChartModel.GraphID != 0 &&
           AddNewChartModel.GraphQueryJson2 != '') ||
          (AddNewChartModel.GraphID != 0 &&
           AddNewChartModel.IsUserQuery2 == true)) {
        this.setValueForQuery2();
      } else if (
          (AddNewChartModel.GraphID == 0 &&
           AddNewChartModel.GraphQueryJson2 != '') ||
          (AddNewChartModel.GraphID == 0 &&
           AddNewChartModel.IsUserQuery2 == true)) {
        this.setValueForQuery2();
      } else {
        this.resetFields('ngInit');
      }
    }

    this.whereConditonObj = {
      'column': '',
      'operator': '',
      'value': '',
      'clause': '',
      'value1': '',
      'value2': ''
    }

                            this.unionObj = {
      'column': '',
      'operator': '',
      'value': '',
      'clause': ''
    }

                                            this.orderByObj = {
      'orderBySelect': '',
      'orderByColumn': ''
    }
  }

  filter(val: string): any {
    return this.whereList.filter(
        option => new RegExp(`^${val}`, 'gi').test(option));
  }

  setValueForQuery1() {
    QueryTabModel.IsUserQuery = AddNewChartModel.IsUserQuery;
    QueryTabModel.IsAdditionalSummary = AddNewChartModel.IsAdditionalSummary;
    QueryTabModel.GraphAdditionalSummaryPosition =
        AddNewChartModel.GraphAdditionalSummaryPosition;

    // Additinal Graph Summary Position
    if (QueryTabModel.IsAdditionalSummary == true) {
      setTimeout(function() {
        (<any>$('#additionalSummary')).trigger('click');
        QueryTabModel.GrpahAdditionalSummary =
            AddNewChartModel.GrpahAdditionalSummary;
      }, 500)
    } else {
      QueryTabModel.GrpahAdditionalSummary = 'Select' +
          '\n' +
          'where @statictext';
    }

    // Write Query Case
    if (QueryTabModel.IsUserQuery == true) {
      this.writequery = true;
      setTimeout(function() {
        (<any>$('#writequery')).trigger('click');
      }, 100);
      this.queryData = AddNewChartModel.GraphQuery;
      this.model = QueryTabModel;
    }

    // Make Query Case
    else {
      this.writequery = false;
      this.queryData = 'Select' +
          '\n' +
          'where @statictext';
      QueryTabModel.GraphQueryJson =
          JSON.parse(AddNewChartModel.GraphQueryJson);
      this.graphType = AddNewChartModel.GraphType;
      this.model = QueryTabModel;
      this.selectedTable = this.model.GraphQueryJson.selectTable;
      var id = parseInt(this.selectedTable);

      // API hit for left column list
      var that = this;
      that.isLoader = true;
      that.dataService.get('ManageGraph/GetGraphColumns?TableID=' + id)
          .subscribe(r => {
            that.leftList = r.GetGraphColumns.map((data, index) => {
              return Object.assign({}, data);
              // return data.ColumnName;
            });

            that.whereList = r.GetGraphColumns.map((data, index) => {
              return data.ColumnName;
            });

            that.autoCompleteBind(that);

            var tempList = r.GetGraphColumns.map((data, index) => {
              return Object.assign({}, data);
            });

            if (that.model.GraphQueryJson.rightSelectedList.length) {
              that.model.GraphQueryJson.rightSelectedList.forEach((data) => {
                let val = data.ColumnName.split(' As ');
                tempList.forEach((innerData, index) => {
                  var LoginMonthIndex: any = '';
                  if (val[0] != innerData.ColumnName) {
                    let val = Object.assign({}, innerData);
                    that.GetGraphColumns.push(val);
                    if (innerData.ColumnName == 'LoginMonth') {
                      LoginMonthIndex = that.GetGraphColumns.length - 1;
                    }
                  } else {
                    tempList.splice(index, 1);
                  }

                  if (LoginMonthIndex != '' &&
                      val[0].trim() ==
                          'CONCAT(CONVERT(VARCHAR(3),DATENAME(mm,DATEADD(mm,LoginMonth,-1))),\' \',RIGHT(LoginYear, 2))') {
                    that.GetGraphColumns.splice(LoginMonthIndex, 1);
                  }
                });
              });
            } else {
              that.GetGraphColumns = r.GetGraphColumns.map((data, index) => {
                return Object.assign({}, data);
              });
            }

            // that.dataService.addNewChartColumnList = that.GetGraphColumns;
            this.sendList.emit(that.leftList);
            that.isLoader = false;
          });

      this.bindToModel();
    }
  }

  setValueForQuery2() {
    QueryTabModel.IsUserQuery = AddNewChartModel.IsUserQuery2;
    QueryTabModel.IsAdditionalSummary = AddNewChartModel.IsAdditionalSummary2;
    QueryTabModel.GraphAdditionalSummaryPosition =
        AddNewChartModel.GraphAdditionalSummaryPosition2;

    // Additinal Graph Summary Position
    if (QueryTabModel.IsAdditionalSummary == true) {
      setTimeout(function() {
        (<any>$('#additionalSummary')).trigger('click');
        QueryTabModel.GrpahAdditionalSummary =
            AddNewChartModel.GrpahAdditionalSummary2;
      }, 500)
    } else {
      QueryTabModel.GrpahAdditionalSummary = 'Select' +
          '\n' +
          'where @statictext';
    }

    // Write Query Case
    if (QueryTabModel.IsUserQuery == true) {
      this.writequery = true;
      setTimeout(function() {
        (<any>$('#writequery')).trigger('click');
      }, 100);
      this.queryData = AddNewChartModel.GraphQuery2;
      this.model = QueryTabModel;
    }

    // Make Query Case
    else {
      this.writequery = false;
      this.queryData = 'Select' +
          '\n' +
          'where @statictext';
      QueryTabModel.GraphQueryJson =
          JSON.parse(AddNewChartModel.GraphQueryJson2);
      this.graphType = AddNewChartModel.GraphType2;
      this.model = QueryTabModel;
      this.selectedTable = this.model.GraphQueryJson.selectTable;
      var id = parseInt(this.selectedTable);

      // API hit for left column list
      var that = this;
      that.isLoader = true;
      that.dataService.get('ManageGraph/GetGraphColumns?TableID=' + id)
          .subscribe(r => {
            that.leftList = r.GetGraphColumns.map((data, index) => {
              return Object.assign({}, data);
            });

            that.whereList = r.GetGraphColumns.map((data, index) => {
              return data.ColumnName;
            });

            that.autoCompleteBind(that);

            var tempList = r.GetGraphColumns.map((data, index) => {
              return Object.assign({}, data);
            });

            if (this.model.GraphQueryJson.rightSelectedList.length) {
              this.model.GraphQueryJson.rightSelectedList.forEach((data) => {
                let val = data.ColumnName.split(' As ');
                tempList.forEach((innerData, index) => {
                  // if (val[0] == "DateName( month , DateAdd( month ,
                  // LoginMonth , -1 ) )") {

                  // }
                  // else if (val[0] != innerData.ColumnName) {
                  //   let val = Object.assign({}, innerData);
                  //   that.GetGraphColumns.push(val);
                  // }
                  // else {
                  //   tempList.splice(index, 1);
                  // }

                  var LoginMonthIndex: any = '';
                  if (val[0] != innerData.ColumnName) {
                    let val = Object.assign({}, innerData);
                    that.GetGraphColumns.push(val);
                    if (innerData.ColumnName == 'LoginMonth') {
                      LoginMonthIndex = that.GetGraphColumns.length - 1;
                    }
                  } else {
                    tempList.splice(index, 1);
                  }

                  if (LoginMonthIndex != '' &&
                      val[0].trim() ==
                          'CONCAT(CONVERT(VARCHAR(3),DATENAME(mm,DATEADD(mm,LoginMonth,-1))),\' \',RIGHT(LoginYear, 2))') {
                    that.GetGraphColumns.splice(LoginMonthIndex, 1);
                  }
                });
              });
            } else {
              that.GetGraphColumns = r.GetGraphColumns.map((data, index) => {
                return Object.assign({}, data);
              });
            }

            // that.dataService.addNewChartColumnList = that.GetGraphColumns;
            this.sendList.emit(that.leftList);
            that.isLoader = false;
          })

              this.bindToModel();
    }
  }

  autoCompleteBind(that: any) {
    that.filteredOptions = that.myControl.valueChanges.startWith(null).map(
        val => val ? that.filter(val) : that.whereList.slice());
  }

  bindToModel() {
    this.top = this.model.GraphQueryJson.top;  // Top-left Top
    this.dynamicColumn =
        this.model.GraphQueryJson.dynamicColumns;  // Top-Right Coloumn Names
    this.whereConditionArr =
        this.model.GraphQueryJson.whereCondition;  // Array of where condition
    this.whereRound = this.whereConditionArr.length;
    this.selectedFuction =
        this.model.GraphQueryJson.selectFunction;  // Top-Right Select Function
    this.selectedColumn = this.model.GraphQueryJson
                              .selectColumn;  // Top-Right Select Column [Model]

    this.model.GraphQueryJson.rightSelectedList.forEach((data) => {
      let val = data.ColumnName.split(' As ');
      if (val[0] ==
              'CONCAT(CONVERT(VARCHAR(3),DATENAME(mm,DATEADD(mm,LoginMonth,-1))),\' \',RIGHT(LoginYear, 2))' ||
          val[0] == 'LoginMonth') {
        this.rightSelectColumn.push('LoginMonth');
      } else {
        this.rightSelectColumn.push(val[0]);
      }
    });

    this.dynamicColArr = this.model.GraphQueryJson.dynamicColArr;
    this.leftList =
        this.model.GraphQueryJson
            .leftColumnList;  // Left Column List for Top, Where & Union
    this.selectedSearchColumns =
        this.model.GraphQueryJson
            .rightSelectedList;  // Top Right selected Columns
    this.unionListArr =
        this.model.GraphQueryJson.unionConditon;  // Array of Union Condition
    this.orderByArr =
        this.model.GraphQueryJson.orderBy;  // Array of Order By Condition

    if (this.model.GraphQueryJson.selectFunctionAlias != '') {
      this.orderByAlias.push(this.model.GraphQueryJson.selectFunctionAlias);
      this.selectFunctionLastAlias =
          this.model.GraphQueryJson.selectFunctionAlias;
    }
    if (this.selectedSearchColumns.length) {
      this.selectedSearchColumns.forEach((data) => {
        if (data.ColumnName.includes('As')) {
          var x = data.ColumnName.split('As');
          var y =
              x[1].substring(x[1].lastIndexOf('[') + 1, x[1].lastIndexOf(']'));
          this.orderByAlias.push(y);
        }
      })
    }
  }

  resetFields(val: any) {
    QueryTabModel.GraphQuery = '';
    QueryTabModel.IsUserQuery = false;
    if (val != 'writeQuery') {
      QueryTabModel.GrpahAdditionalSummary = 'Select' +
          '\n' +
          'where @statictext';
    }
    QueryTabModel.IsAdditionalSummary = false;
    QueryTabModel.GraphQueryJson = {
      'selectTable': '',
      'top': '',
      'dynamicColumns': '',
      'rightSelectedList': [],
      'selectFunction': '',
      'selectFunctionAlias': '',
      'selectColumnAlias': '',
      'selectColumn': '',
      'whereCondition': [],
      'unionConditon': [],
      'orderBy': [],
      'dynamicColArr': []
    }

                                   this.model = QueryTabModel;
    this.writequery = false;
    this.selectedTable =
        this.model.GraphQueryJson.selectTable;  // Top-Left Select Table
    this.GetGraphColumns = [];
    this.dynamicColumn =
        this.model.GraphQueryJson.dynamicColumns;  // Top-Right Coloumn Names
    this.whereConditionArr =
        this.model.GraphQueryJson.whereCondition;  // Array of where condition
    this.selectedFuction =
        this.model.GraphQueryJson.selectFunction;  // Top-Right Select Function
    this.selectedColumn = this.model.GraphQueryJson
                              .selectColumn;  // Top-Right Select Column [Model]
    this.rightSelectColumn = [];              // Top-Right Select Column [Array]
    this.leftList =
        this.model.GraphQueryJson
            .leftColumnList;  // Left Column List for Top, Where & Union
    this.selectedSearchColumns =
        this.model.GraphQueryJson
            .rightSelectedList;  // Top Right selected Columns
    this.unionArr =
        this.model.GraphQueryJson.unionConditon;  // Array of Union Condition
    this.orderByArr =
        this.model.GraphQueryJson.orderBy;  // Array of Order By Condition
    this.query = this.model.GraphQuery;
  }

  veiwAndNext() {
    this.viewQuery('view');
  }

  bindingModel() {
    if (this.writequery == false) {
      this.model.GraphQueryJson.selectTable = this.selectedTable;
      this.model.GraphQueryJson.top = this.top;
      this.model.GraphQueryJson.dynamicColumns = this.dynamicColumn;
      this.model.GraphQueryJson.whereCondition = this.whereConditionArr;
      this.model.GraphQueryJson.unionConditon = this.unionListArr;
      this.model.GraphQueryJson.selectFunction = this.selectedFuction;
      this.model.GraphQueryJson.selectColumn = this.selectedColumn;
      this.model.GraphQueryJson.rightSelectedList = this.selectedSearchColumns;
      this.model.GraphQueryJson.orderBy = this.orderByArr;
      this.model.GraphQueryJson.dynamicColArr = this.dynamicColArr;

      if (this.queryCount == '1') {
        AddNewChartModel.GraphQuery = this.query;
        AddNewChartModel.IsAdditionalSummary = this.model.IsAdditionalSummary;
        AddNewChartModel.IsUserQuery = this.model.IsUserQuery;
        AddNewChartModel.GraphAdditionalSummaryPosition =
            this.model.GraphAdditionalSummaryPosition;
        if (this.model.GrpahAdditionalSummary ===
            'Select' +
                '\n' +
                'where @statictext') {
          AddNewChartModel.GrpahAdditionalSummary = '';
        } else {
          AddNewChartModel.GrpahAdditionalSummary =
              this.model.GrpahAdditionalSummary;
        }

        AddNewChartModel.GraphQueryJson =
            JSON.stringify(this.model.GraphQueryJson);
        AddNewChartModel.WherePop = this.WherePop;
      } else if (this.queryCount == '2') {
        AddNewChartModel.GraphQuery2 = this.query;
        AddNewChartModel.IsAdditionalSummary2 = this.model.IsAdditionalSummary;
        AddNewChartModel.IsUserQuery2 = this.model.IsUserQuery;
        AddNewChartModel.GraphAdditionalSummaryPosition2 =
            this.model.GraphAdditionalSummaryPosition;
        if (this.model.GrpahAdditionalSummary ===
            'Select' +
                '\n' +
                'where @statictext') {
          AddNewChartModel.GrpahAdditionalSummary2 = '';
        } else {
          AddNewChartModel.GrpahAdditionalSummary2 =
              this.model.GrpahAdditionalSummary;
        }
        AddNewChartModel.GraphQueryJson2 =
            JSON.stringify(this.model.GraphQueryJson);
        AddNewChartModel.WherePop2 = this.WherePop;
      }
    } else if (this.writequery == true) {
      if (this.queryData == '') {
        swal('', 'Please Write Query');
      } else {
        if (this.queryCount == '1') {
          AddNewChartModel.GraphQuery = this.queryData;
          AddNewChartModel.IsUserQuery = this.model.IsUserQuery;
          AddNewChartModel.GraphQueryJson = '';
          AddNewChartModel.IsAdditionalSummary = this.model.IsAdditionalSummary;
          if (this.model.GrpahAdditionalSummary ===
              'Select' +
                  '\n' +
                  'where @statictext') {
            AddNewChartModel.GrpahAdditionalSummary = '';
          } else {
            AddNewChartModel.GrpahAdditionalSummary =
                this.model.GrpahAdditionalSummary;
          }
          AddNewChartModel.GraphAdditionalSummaryPosition =
              this.model.GraphAdditionalSummaryPosition;
          AddNewChartModel.WherePop = this.WherePop;
        } else if (this.queryCount == '2') {
          AddNewChartModel.GraphQuery2 = this.queryData;
          AddNewChartModel.IsUserQuery2 = this.model.IsUserQuery;
          AddNewChartModel.GraphQueryJson2 = '';
          AddNewChartModel.IsAdditionalSummary2 =
              this.model.IsAdditionalSummary;
          if (this.model.GrpahAdditionalSummary ===
              'Select' +
                  '\n' +
                  'where @statictext') {
            AddNewChartModel.GrpahAdditionalSummary2 = '';
          } else {
            AddNewChartModel.GrpahAdditionalSummary2 =
                this.model.GrpahAdditionalSummary;
          }
          AddNewChartModel.GraphAdditionalSummaryPosition2 =
              this.model.GraphAdditionalSummaryPosition;
          AddNewChartModel.WherePop2 = this.WherePop;
        }
      }
    }
  }

  showNextTab(val: any) {
    this.bindingModel();
    if (this.graphAdditionalQuery == true &&
        this.additionQueryValidated == false) {
      swal('', 'Please validate Additional Graph Query First');
    } else {
      // calling next tab showing function
      this.nextButtonClick.emit(
          {list: this.GetGraphColumns, queryCount: this.queryCount});
    }
  }

  showPrevTab() {
    this.prevButtonClick.emit({count: this.queryCount});
  }

  dynamicColumnChange() {
    if (this.dynamicColumn == '') {
      this.isDynamicColumn = false;
      this.model.GraphQueryJson.selectColumnAlias = this.dynamicColumn;
    } else {
      this.isDynamicColumn = true;
      this.model.GraphQueryJson.selectColumnAlias = this.dynamicColumn;
    }
  }

  Add() {
    if (this.tempSelectedGridC != '') {
      let svalue = this.tempSelectedGridC;
      for (var j = 0; j < this.GetGraphColumns.length; j++) {
        if (this.GetGraphColumns[j].ColumnName === svalue) {
          if (this.AliasInput) {
            if (svalue == 'LoginMonth') {
              svalue =
                  'CONCAT(CONVERT(VARCHAR(3),DATENAME(mm,DATEADD(mm,LoginMonth,-1))),\' \',RIGHT(LoginYear, 2))';
            }
            this.GetGraphColumns[j].ColumnName =
                svalue + ' As [' + this.AliasInput + ']';
            this.GetGraphColumns[j].ColumnAliasName = this.AliasInput;
            this.orderByAlias.push(this.AliasInput);
          } else {
            if (svalue == 'LoginMonth') {
              svalue =
                  'CONCAT(CONVERT(VARCHAR(3),DATENAME(mm,DATEADD(mm,LoginMonth,-1))),\' \',RIGHT(LoginYear, 2))';
              this.GetGraphColumns[j].ColumnName = svalue + ' As [LoginMonth] ';
              this.GetGraphColumns[j].ColumnAliasName = 'LoginMonth';
              this.orderByAlias.push('LoginMonth');
            } else {
              this.GetGraphColumns[j].ColumnName =
                  svalue + ' As [' + svalue + ']';
              this.GetGraphColumns[j].ColumnAliasName = svalue;
              this.orderByAlias.push(svalue);
            }
          }

          this.selectedSearchColumns.push(this.GetGraphColumns[j]);
          this.rightSelectColumn.push(svalue);
          this.GetGraphColumns.splice(j, 1);
          this.AliasInput = '';
          break;
        }
      }
    }

    this.tempSelectedGridC = '';
    (<any>$('#myModalOne')).modal('hide');
  }

  Remove() {
    for (var i = 0; i < this.tempselectedAvailG.length; i++) {
      let svalue = this.tempselectedAvailG[i];
      for (var j = 0; j < this.selectedSearchColumns.length; j++) {
        if (this.selectedSearchColumns[j].ColumnName === svalue) {
          var columnName = svalue.split(' As ')[0];
          if (columnName ==
              'CONCAT(CONVERT(VARCHAR(3),DATENAME(mm,DATEADD(mm,LoginMonth,-1))),\' \',RIGHT(LoginYear, 2))') {
            this.selectedSearchColumns[j].ColumnName = 'LoginMonth';
          } else {
            this.selectedSearchColumns[j].ColumnName = svalue.split(' As ')[0];
          }

          this.selectedSearchColumns[j].ColumnAliasName = '';
          this.GetGraphColumns.push(this.selectedSearchColumns[j]);
          this.selectedSearchColumns.splice(j, 1);

          this.rightSelectColumn.splice(j, 1);
          // Removing From Order By
          var index =
              this.orderByAlias
                  .indexOf(svalue.substring(
                      svalue.lastIndexOf('[') + 1, svalue.lastIndexOf(']')))

                      this.orderByAlias.splice(index, 1);
          break;
        }
      }
    }
    this.tempselectedAvailG = [];
  }

  getColumnList() {
    this.defaultSelectedCompany = {
      'CompanyId': 0,
      'CompanyName': '--Select Company--'
    };
    this.isLoader = true;
    var that = this;
    that.dataService
        .get('ManageGraph/GetGraphColumns?TableID=' + this.selectedTable)
        .subscribe(r => {
          that.leftList = r.GetGraphColumns.map((data, index) => {
            return Object.assign({}, data);
          });

          that.whereList = r.GetGraphColumns.map((data, index) => {
            return data.ColumnName;
          });


          that.autoCompleteBind(that);
          // that.filteredClients = that.myControl.valueChanges
          // .startWith(null)
          // .map(user => user && typeof user === 'object' ? user.ColumnName :
          // user) .map(ColumnName => ColumnName ? that.filter(ColumnName) :
          // that.leftList.slice());

          that.GetGraphColumns = r.GetGraphColumns.map((data, index) => {
            return Object.assign({}, data);
          });

          // that.dataService.addNewChartColumnList = that.GetGraphColumns;
          this.sendList.emit(that.leftList);
          that.selectedSearchColumns = [];
          that.rightSelectColumn = [];
          that.isLoader = false;
        })
  }

  // filter(ColumnName: string): any {
  //       return this.leftList.filter(option => new RegExp(`^${ColumnName}`,
  //       'gi').test(option.ColumnName));
  //   }

  onKeyPress(e) {
    var val = (e.target.value).toLowerCase();
  }

  selectFunctionChange(e: any) {
    this.model.GraphQueryJson.selectFunctionAlias = this.selectedFuction;
    if (this.selectFunctionLastAlias == '') {
      this.orderByAlias.push(this.selectedFuction);
    } else {
      var val = this.selectFunctionLastAlias;
      var index = this.orderByAlias.indexOf(val);
      this.orderByAlias.splice(index, 1);
    }

    this.selectFunctionLastAlias =
        this.model.GraphQueryJson.selectFunctionAlias;
    // this.index = this.orderByAlias.length - 1;

    // else
    // {
    //   this.orderByAlias.push(this.selectedFuction);
    //   this.index = this.orderByAlias.length - 1;
    // }


    // if (this.selectedFuction == "Count")
    // {
    //   if(this.model.GraphQueryJson.selectFunctionAlias == "")
    //   {
    //     this.orderByAlias.push("Count");
    //   }
    //   else
    //   {
    //     this.orderByAlias.push(this.model.GraphQueryJson.selectFunctionAlias);
    //   }
    // }
    // else
    // {
    //   this.orderByAlias.forEach((data, index) => {
    //     if (data == "Count")
    //     {
    //       this.orderByAlias.splice(index, 1);
    //     }
    //   })
    // }
  }

  selectFunctionAliasChange(e: any) {
    // var val = this.selectedFuction;

    if (this.model.GraphQueryJson.selectFunctionAlias != '') {
      var val = this.selectFunctionLastAlias;
      var index = this.orderByAlias.indexOf(val);
      this.orderByAlias.splice(index, 1);
      this.orderByAlias.push(this.model.GraphQueryJson.selectFunctionAlias);
      this.selectFunctionLastAlias =
          this.model.GraphQueryJson.selectFunctionAlias;
    } else {
      this.model.GraphQueryJson.selectFunctionAlias =
          this.selectFunctionLastAlias;
      swal('', 'Can\'t be empty');
    }
  }

  topChange() {
    var val = this.top;
    QueryTabModel.GraphQueryJson.top = val;
  }

  hideModel() {
    (<any>$('#AliasInput')).val('');
    (<any>$('#myModalOne')).modal('hide');
  }

  showModel() {
    if (this.tempSelectedGridC != '') {
      this.AliasInput = this.tempSelectedGridC;
      (<any>$('#myModalOne')).modal('show');
      //(<any>$('#md-input-3')).focus();
    } else {
      swal('', 'Please select value from column list');
    }
  }

  operatorChange() {
    var val = this.whereConditonObj.operator;
    if (val == 'Between') {
      this.isBetween = true;
      this.whereConditonObj.value = '';
    } else {
      this.isBetween = false;
      this.whereConditonObj.value1 = '';
      this.whereConditonObj.value2 = '';
    }
  }

  additionalSummary() {
    if ((<any>$('#additionalSummary')).prop('checked') == true) {
      this.graphAdditionalQuery = true;
      this.model.IsAdditionalSummary = true;
    } else {
      this.graphAdditionalQuery = false;
      this.model.IsAdditionalSummary = false;
      this.model.GrpahAdditionalSummary = 'Select' +
          '\n' +
          'where @statictext';
    }
  }

  writeQuery() {
    if ((<any>$('#writequery')).prop('checked') == true) {
      this.resetFields('writeQuery');
      this.writequery = true;
      this.query = '';
      this.model.GraphQuery = '';
      this.model.IsUserQuery = true;
    } else {
      this.writequery = false;
      this.queryData = 'Select' +
          '\n' +
          'where @statictext';
      this.model.IsUserQuery = false;
      // if (this.model.GrpahAdditionalSummary != "Select" + "\n" + "where
      // @statictext" && this.model.GrpahAdditionalSummary != "") {
      //   setTimeout(function () {
      //     (<any>$('#additionalSummary')).prop('checked', true);
      //   }, 500)
      // }
    }
  }

  checkClick() {
    if ((<any>$('#union')).prop('checked') == true) {
      var length = this.unionListArr.length;
      this.unionListArr.splice(length, 1, []);
      this.dynamicColArr.push('');
      (<any>$('#union')).prop('checked', false);
    }
  }

  removeUnion(index: any) {
    (<any>$('.alwaysChecked')).each(function() {
      if ((<any>$(this)).prop('checked') == false) {
        (<any>$('.alwaysChecked')).prop('checked', true);
      }
    });
    this.unionListArr.splice(index, 1);
  }

  // 'where' code goes here ---------------------------------------

  validateWhereCondition(data: any, index: any) {
    var validateResult = true;
    if (data.column == '' || data.operator == '') {
      validateResult = false;
    } else {
      if (data.clause == '' && index > 0) {
        validateResult = false;
      }
      if (data.operator != 'Between' && data.value == '') {
        validateResult = false;
      } else if (
          data.operator == 'Between' &&
          (data.value1 == '' || data.value2 == '')) {
        validateResult = false;
      }
    }

    return validateResult;
  }

  addWhereCondition() {
    var rowObj = Object.assign({}, this.whereConditonObj);
    var validateResult = this.validateWhereCondition(
        rowObj,
        typeof this.addWhereIndex == 'number' ? this.addWhereIndex :
                                                this.whereRound);
    if (validateResult == true) {
      if (this.addWhereIndex === '') {
        this.whereConditionArr.push(rowObj);
      } else {
        this.whereConditionArr.splice(this.addWhereIndex, 1, rowObj);
      }

      // clearing all value from this.whereConditonObj object (holding 'where'
      // values)
      for (let i in this.whereConditonObj) {
        this.whereConditonObj[i] = '';
      }
      this.addWhereIndex = '';
      this.whereRound++;
      this.isBetween = false;
    } else {
      swal('', 'Please Fill All Fields');
    }
  }

  deleteWhereConditon(e, index: any) {
    e.preventDefault();
    this.whereConditionArr.splice(index, 1);

    if (this.whereConditionArr.length > 0) {
      this.whereConditionArr[0].clause = '';
    }
    this.whereRound--;
  }

  editWhereCondition(e, index: any) {
    e.preventDefault();
    var data = Object.assign({}, this.whereConditionArr[index]);

    if (data.operator == 'Between') {
      this.isBetween = true;
    } else {
      this.isBetween = false;
    }

    this.whereConditonObj.column = data.column;
    this.whereConditonObj.operator = data.operator;
    this.whereConditonObj.value = data.value;
    this.whereConditonObj.value1 = data.value1;
    this.whereConditonObj.value2 = data.value2;
    if (index == 0) {
      this.whereConditonObj.clause = '';
    } else {
      this.whereConditonObj.clause = data.clause;
    }
    this.addWhereIndex = index;
    // this.whereConditionArr.splice(index, 1);
    this.whereRound--;
  }

  // 'union' code goes here
  // -----------------------------------------------------
  addUnion() {
    var rowObj = Object.assign({}, this.unionObj);
    if (this.addUnionIndex === '') {
      this.unionArr.push(rowObj);
    } else {
      this.unionArr.splice(this.addUnionIndex, 1, rowObj);
    }

    // clearing all value from this.whereConditonObj object (holding 'where'
    // values)
    for (let i in this.unionObj) {
      this.unionObj[i] = '';
    }
    this.addUnionIndex = '';
    this.unionRound++;
  }

  deleteUnion(e, index: any) {
    e.preventDefault();
    this.unionArr.splice(index, 1);
    this.unionRound--;
  }

  editUnion(e, index: any) {
    e.preventDefault();
    var data = Object.assign({}, this.unionArr[index]);
    this.unionObj.column = data.column;
    this.unionObj.operator = data.operator;
    this.unionObj.value = data.value;
    this.unionObj.clause = data.clause;
    this.addUnionIndex = index;
    // this.unionArr.splice(index, 1);
    this.unionRound--;
  }

  // OrderBy code goes here ---------------------------------------------------

  addOrderBy() {
    var rowObj = Object.assign({}, this.orderByObj);
    if (rowObj.orderBySelect != '' && rowObj.orderByColumn != '') {
      this.orderByArr.push(rowObj);

      // clearing all value from this.whereConditonObj object (holding 'where'
      // values)
      for (let i in this.orderByObj) {
        this.orderByObj[i] = '';
      }
    } else {
      swal('', 'Please Fill All Fields');
    }
  }

  deleteOrderBy(e, index: any) {
    e.preventDefault();
    this.orderByArr.splice(index, 1);
  }

  editOrderBy(e, index: any) {
    e.preventDefault();
    var data = Object.assign({}, this.orderByArr[index]);
    this.orderByObj.orderBySelect = data.orderBySelect;
    this.orderByObj.orderByColumn = data.orderByColumn;
    this.orderByObj.orderByValue = data.orderByValue;
    this.orderByArr.splice(index, 1);
  }

  additionalSummayChange() {
    this.additionQueryValidated = false;
  }

  // Ordr By code goes here
  // -----------------------------------------------------

  viewQuery(val: any) {
    if (this.graphAdditionalQuery == true &&
        this.additionQueryValidated == false) {
      swal('', 'Please validate Additional Graph Query First');
    } else {
      this.WherePop = [];
      this.query = 'Select ' + (this.top ? ' top ' + this.top + ' ' : '');
      var whereQuery = '';
      var unionQuery = ' union ';
      var orderByQuery = ' Order By ';
      var functionAlias = '';
      var leftSelectHalf = '';
      var rightSelectHalf = '';

      var selectFunction = this.selectedFuction;
      var selectFunctionAlias = this.model.GraphQueryJson.selectFunctionAlias;
      var selectColumn = this.selectedColumn;
      var tableName = this.selectedTable;
      var groupByArr = [];

      // Create Array of Objects of all right selected column (contains column
      // name with alias)
      var columnList = this.selectedSearchColumns.map((data, index) => {
        var x = data.ColumnName.split(' As ');
        if (x[0] ==
            'CONCAT(CONVERT(VARCHAR(3),DATENAME(mm,DATEADD(mm,LoginMonth,-1))),\' \',RIGHT(LoginYear, 2))') {
          return ({
            columnName: 'LoginMonth',
            name: data.ColumnName,
            alias: data.ColumnAliasName
          });
        } else {
          return ({
            columnName: x[0],
            name: data.ColumnName,
            alias: data.ColumnAliasName
          });
        }
      })

      // Assign columnList array to groupby array
      groupByArr = columnList;

      // if count is selected in select function
      // We check 'selectFunction' first because if selectFuction is other than
      // Count, then the columnName will be remove from the query on which that
      // function is applied
      if (selectFunction == 'Count') {
        // If columnList contains more than 1 Object
        if (columnList.length > 1) {
          var newArr = columnList.map((data, index) => {return data.name})
          var x = newArr.join(', ');
          this.query = this.query + ' ' + x;
        }

        // If columnList contain only single Object
        else if (columnList.length == 1) {
          var x = columnList[0].name;
          this.query = this.query + ' ' + x;
        }

        leftSelectHalf = this.query;

        // Store Dynamic Column Alias Name
        var colAliasName = this.model.GraphQueryJson.selectColumnAlias;

        if (columnList.length > 0 && this.dynamicColumn) {
          this.query = this.query + ', \'' + this.dynamicColumn + '\' as [' +
              (colAliasName ? colAliasName : this.dynamicColumn) + ']';
        } else if (columnList.length == 0 && this.dynamicColumn) {
          this.query = this.query + ' \'' + this.dynamicColumn + '\' as [' +
              (colAliasName ? colAliasName : this.dynamicColumn) + '] ';
        }

        if (columnList.length > 0 || this.dynamicColumn) {
          this.query = this.query + ', ' + selectFunction + '(*) As [' +
              (selectFunctionAlias ? selectFunctionAlias : selectFunction) +
              '] ';
        } else {
          this.query = this.query + ' ' + selectFunction + '(*) As [' +
              (selectFunctionAlias ? selectFunctionAlias : selectFunction) +
              '] ';
        }

        if (tableName == '1') {
          this.query =
              this.query + ' from CSolution.T_DynamicOrder where @statictext ';
          rightSelectHalf = ' ' + selectFunction + '(*) As [' +
              (selectFunctionAlias ? selectFunctionAlias : selectFunction) +
              '] from CSolution.T_DynamicOrder where @statictext ';
        } else if (tableName == '2') {
          this.query = this.query +
              ' from CSolution.T_DynamicOrderTesting where @statictext ';
          rightSelectHalf = ' ' + selectFunction + '(*) As [' +
              (selectFunctionAlias ? selectFunctionAlias : selectFunction) +
              '] from CSolution.T_DynamicOrderTesting where @statictext ';
        } else if (tableName == '3') {
          this.query = this.query +
              ' from CSolution.LIMS_SubstanceView where @statictext ';
          rightSelectHalf = ' ' + selectFunction + '(*) As [' +
              (selectFunctionAlias ? selectFunctionAlias : selectFunction) +
              '] from CSolution.LIMS_SubstanceView where @statictext ';
        } else if (tableName == '4') {
          this.query = this.query +
              ' from CSolution.View_Ins_Booking_Detail where @statictext ';
          rightSelectHalf = ' ' + selectFunction + '(*) As [' +
              (selectFunctionAlias ? selectFunctionAlias : selectFunction) +
              '] from CSolution.LIMS_SubstanceView where @statictext ';
        }
      }

      // If other than count is selected in select function
      else if (selectFunction && selectColumn && selectFunction != 'Count') {
        // Store all objects stored in columnList to tempArr
        var tempArr = columnList.map((data) => {
          return Object.assign({}, data);
        })

        // Remove selected column (on which function is applied) from tempArr
        tempArr.forEach((data, index) => {
          if (data.name.includes(selectColumn)) {
            columnList.splice(index, 1);
          }
        })

        if (columnList.length > 1) {
          var newArr = columnList.map((data, index) => {return data.name})

          var x = newArr.join(', ');
          this.query = this.query + ' ' + x;
        }
        else if (columnList.length == 1) {
          var x = columnList[0].name;
          this.query = this.query + ' ' + x;
        }

        leftSelectHalf = this.query;

        // dynamic column
        if (columnList.length > 0 && this.dynamicColumn) {
          this.query = this.query + ', \'' + this.dynamicColumn + '\' as [' +
              (colAliasName ? colAliasName : this.dynamicColumn) + '] ';
        } else if (columnList.length == 0 && this.dynamicColumn) {
          this.query = this.query + ' \'' + this.dynamicColumn + '\' as [' +
              (colAliasName ? colAliasName : this.dynamicColumn) + '] ';
        }

        var unionFunctionTemp;
        tempArr.forEach((data, index) => {
          if (data.name.includes(selectColumn)) {
            if (columnList.length > 0 || this.dynamicColumn) {
              this.query = this.query + ', ' + selectFunction + '(' +
                  selectColumn + ') As [' +
                  (selectFunctionAlias ? selectFunctionAlias : data.alias) +
                  '] ';
            } else {
              this.query = this.query + ' ' + selectFunction + '(' +
                  selectColumn + ') As [' +
                  (selectFunctionAlias ? selectFunctionAlias : data.alias) +
                  '] ';
            }
            unionFunctionTemp = ' ' + selectFunction + '(' + selectColumn +
                ') As [' +
                (selectFunctionAlias ? selectFunctionAlias : data.alias) + '] ';
          }
        })

        if (tableName == '1') {
          this.query =
              this.query + ' from CSolution.T_DynamicOrder where @statictext ';
          rightSelectHalf = (unionFunctionTemp ? unionFunctionTemp : '') +
              ' from CSolution.T_DynamicOrder where @statictext ';
        }
        else if (tableName == '2') {
          this.query = this.query +
              ' from CSolution.T_DynamicOrderTesting where @statictext ';
          rightSelectHalf = (unionFunctionTemp ? unionFunctionTemp : '') +
              ' from CSolution.T_DynamicOrderTesting where @statictext ';
        }
        else if (tableName == '4') {
          this.query = this.query +
              ' from CSolution.View_Ins_Booking_Detail where @statictext ';
          rightSelectHalf = (unionFunctionTemp ? unionFunctionTemp : '') +
              ' from CSolution.View_Ins_Booking_Detail where @statictext ';
        }
      } else if (selectFunction && !selectColumn && selectFunction != 'Count') {
        swal('', 'Please Select Column in order to apply Function');
        return;
      } else {
        if (columnList.length > 1) {
          var newArr = columnList.map((data, index) => {return data.name})

          var x = newArr.join(', ');
          this.query = this.query + ' ' + x;
        } else if (columnList.length == 1) {
          var x = columnList[0].name;
          this.query = this.query + ' ' + x;
        }

        leftSelectHalf = this.query;

        // dynamic column
        if (columnList.length > 0 && this.dynamicColumn) {
          this.query = this.query + ', \'' + this.dynamicColumn + '\' as [' +
              (colAliasName ? colAliasName : this.dynamicColumn) + '] ';
        } else if (columnList.length == 0 && this.dynamicColumn) {
          this.query = this.query + ' \'' + this.dynamicColumn + '\' as [' +
              (colAliasName ? colAliasName : this.dynamicColumn) + '] ';
        }

        if (tableName == '1') {
          this.query =
              this.query + ' from CSolution.T_DynamicOrder where @statictext ';
          rightSelectHalf = ' from CSolution.T_DynamicOrder where @statictext ';
        } else if (tableName == '2') {
          this.query = this.query +
              ' from CSolution.T_DynamicOrderTesting where @statictext ';
          rightSelectHalf =
              ' from CSolution.T_DynamicOrderTesting where @statictext ';
        } else if (tableName == '4') {
          this.query = this.query +
              ' from CSolution.View_Ins_Booking_Detail where @statictext ';
          rightSelectHalf =
              ' from CSolution.View_Ins_Booking_Detail where @statictext ';
        }
      }

      var selectQuery = this.query;

      // Adding where statement in query
      var whereArr = this.whereConditionArr;

      if (whereArr.length > 0) {
        whereQuery = whereQuery + ' And ';
        whereArr.forEach((data, index) => {
          if (data.clause) {
            whereQuery = whereQuery + ' ' + data.clause + ' ';
          }

          whereQuery = whereQuery + data.column;
          if (data.operator == 'Equal') {
            whereQuery = whereQuery + ' = \'' + data.value + '\'';
          } else if (data.operator == 'Not Equal') {
            whereQuery = whereQuery + ' != \'' + data.value + '\'';
          } else if (data.operator == 'Less Then') {
            whereQuery = whereQuery + ' < \'' + data.value + '\'';
          } else if (data.operator == 'Greater Then') {
            whereQuery = whereQuery + ' > \'' + data.value + '\'';
          } else if (data.operator == 'Like') {
            whereQuery = whereQuery + ' like \'%' + data.value + '%\'';
          } else if (data.operator == 'Between') {
            whereQuery = whereQuery + ' between \'' + data.value1 +
                '\' and \'' + data.value2 + '\'';
          } else if (data.operator == 'In') {
            let finalValue = '';
            if (data.value) {
              if (data.value.includes(',')) {
                let val = data.value.split(',');
                for (let i = 0; i < val.length; i++) {
                  if (i != (val.length - 1)) {
                    finalValue = finalValue + '\'' + val[i].trim() + '\',';
                  } else {
                    finalValue = finalValue + '\'' + val[i].trim() + '\'';
                  }
                }
              } else {
                finalValue = data.value;
              }
            }
            whereQuery = whereQuery + ' in (' + finalValue + ')';
          }
        });
        this.WherePop.push('where @statictext' + whereQuery);
        this.query = this.query + whereQuery;
      }

      // group by code comes here
      if (groupByArr.length > 0 && this.rightSelectColumn.length > 0) {
        this.query = this.query + ' group by ';

        this.rightSelectColumn.forEach((val) => {
          if (val ==
                  'CONCAT(CONVERT(VARCHAR(3),DATENAME(mm,DATEADD(mm,LoginMonth,-1))),\' \',RIGHT(LoginYear, 2))' ||
              val == 'LoginMonth') {
            this.query = this.query + 'LoginYear, ';
          }
        })
        columnList.forEach((data, index) => {
          this.query = this.query + data.columnName;
          if (index !== (groupByArr.length - 1)) {
            this.query = this.query + ', ';
          }
        })
      }

      // Adding Union Query ---------------------------------------
      var unionArr = this.unionListArr;

      if (unionArr.length > 0) {
        //-----------------------------------------------------------------------------
        var count = 1;
        //-----------------------------------------------------------------------------
        unionArr
            .forEach((innerArr, outerindex) => {
              unionQuery = unionQuery + leftSelectHalf;
              if (columnList.length > 0 && this.dynamicColArr[outerindex]) {
                unionQuery =
                    unionQuery + ', ' + this.dynamicColArr[outerindex] + ' ';
              } else if (
                  columnList.length == 0 && this.dynamicColArr[outerindex]) {
                unionQuery =
                    unionQuery + ' ' + this.dynamicColArr[outerindex] + ' ';
              }
              if (selectFunction == 'Count') {
                unionQuery = unionQuery + ', ' + rightSelectHalf;
              } else if (
                  selectFunction && selectFunction != 'Count' && selectColumn &&
                  columnList.length > 0) {
                unionQuery = unionQuery + ', ' + rightSelectHalf;
              } else {
                unionQuery = unionQuery + rightSelectHalf;
              }

              unionQuery = unionQuery + ' And ';
              innerArr.forEach((data, innerindex) => {
                if (data.clause) {
                  unionQuery = unionQuery + ' ' + data.clause + ' ';
                }

                unionQuery = unionQuery + data.column;
                if (data.operator == 'Equal') {
                  unionQuery = unionQuery + ' = \'' + data.value + '\'';
                } else if (data.operator == 'Not Equal') {
                  unionQuery = unionQuery + ' != \'' + data.value + '\'';
                } else if (data.operator == 'Less Then') {
                  unionQuery = unionQuery + ' < \'' + data.value + '\'';
                } else if (data.operator == 'Greater Then') {
                  unionQuery = unionQuery + ' > \'' + data.value + '\'';
                } else if (data.operator == 'Like') {
                  unionQuery = unionQuery + ' like \'%' + data.value + '%\'';
                } else if (data.operator == 'Between') {
                  unionQuery = unionQuery + ' between \'' + data.value1 +
                      '\' and \'' + data.value2 + '\'';
                } else if (data.operator == 'In') {
                  let finalValue = '';
                  if (data.value) {
                    if (data.value.includes(',')) {
                      let val = data.value.split(',');
                      for (let i = 0; i < val.length; i++) {
                        if (i != (val.length - 1)) {
                          finalValue =
                              finalValue + '\'' + val[i].trim() + '\',';
                        } else {
                          finalValue = finalValue + '\'' + val[i].trim() + '\'';
                        }
                      }
                    } else {
                      finalValue = data.value;
                    }
                  }
                  unionQuery = unionQuery + ' in (' + finalValue + ')';
                }
              });
              this.WherePop.push(
                  'where @statictext' +
                  unionQuery.split('where @statictext')[count]);
              count++;

              if (groupByArr.length > 0 && this.rightSelectColumn.length > 0) {
                unionQuery = unionQuery + ' group by ';
                this.rightSelectColumn.forEach((val) => {
                  if (val ==
                          'CONCAT(CONVERT(VARCHAR(3),DATENAME(mm,DATEADD(mm,LoginMonth,-1))),\' \',RIGHT(LoginYear, 2))' ||
                      val == 'LoginMonth') {
                    unionQuery = unionQuery + 'LoginYear, ';
                  }
                })
                columnList.forEach((data, index) => {
                  unionQuery = unionQuery + data.columnName;
                  if (index !== (columnList.length - 1)) {
                    unionQuery = unionQuery + ', ';
                  }
                })
              }

              if (outerindex !== (unionArr.length - 1)) {
                unionQuery = unionQuery + ' union ';
              }
            })

                this.query = this.query + unionQuery;
      }
      // Order By Query --------------------------------------

      var orderbyArr = this.orderByArr;
      if (orderbyArr.length) {
        orderbyArr.forEach((data, index) => {
          orderByQuery = orderByQuery + ' [' + data.orderByColumn + '] ' +
              data.orderBySelect;
          if (index != (orderbyArr.length - 1)) {
            orderByQuery = orderByQuery + ', ';
          }
        });
        this.query = this.query + orderByQuery;
      }

      if (val == 'view') {
        (<any>$('#showQueryModal')).modal('show');
      }
    }
  }


  save() {
    this.bindingModel();

    if (this.graphAdditionalQuery == true &&
        this.additionQueryValidated == false) {
      swal('', 'Please validate Additional Graph Query First');
    } else {
      var that = this;
      that.isLoader = true;
      this.dataService.post('ManageGraph/SaveConfiguration', AddNewChartModel)
          .subscribe(response => {
            if (response.IsSuccess) {
              that.isLoader = false;
              swal({title: '', text: response.Message}, function() {
                that.router.navigate(['/admin/order-analysis-configuration']);
                swal.close();
              });
            } else {
              that.isLoader = false;
              swal('', response.Message)
            }
          });
    }
  }

  validateQuery(val: any) {
    // Validation of Write Query
    if (val == 'writeQuery') {
      if (this.graphAdditionalQuery == true &&
          this.additionQueryValidated == false) {
        swal('', 'Please validate Additional Graph Query First');
      } else {
        var data = {'Query': this.queryData}

        var that = this;
        that.isLoader = true;
        this.dataService.post('ManageGraph/ValidatingQuery', data)
            .subscribe(response => {
              if (response.IsSuccess) {
                that.isLoader = false;
                // swal('', "Query Validated Successfully");
                swal(
                    {
                      title: '',
                      text: 'Query Validated Successfully.',
                      showCancelButton: true,
                      confirmButtonColor: '#DD6B55',
                      confirmButtonText: 'Proceed',
                      closeOnConfirm: true,
                      closeOnCancel: false
                    },
                    function(isConfirm) {
                      if (isConfirm) {
                        swal.close();
                        setTimeout(function() {
                          that.showNextTab(val);
                        }, 400)
                      }
                    });
              } else {
                that.isLoader = false;
                swal('', response.Message);
                (<any>$('#showQueryModal')).modal('hide');
              }
            });
      }
    }

    // Validation of Additional Graph Query
    if (val == 'additionalQuery') {
      var data = {'Query': this.model.GrpahAdditionalSummary};
      var that = this;
      that.isLoader = true;
      this.dataService.post('ManageGraph/ValidatingQuery', data)
          .subscribe(response => {
            if (response.IsSuccess) {
              that.isLoader = false;
              // swal('', "Query Validated Successfully");
              swal('', 'Query Validated Successfully.');
              this.additionQueryValidated = true;
            } else {
              that.isLoader = false;
              swal('', response.Message);
            }
          });
    }

    // Validation of Make Query
    if (val == 'makeQuery') {
      var data = {'Query': this.query}

      var that = this;
      that.isLoader = true;
      this.dataService.post('ManageGraph/ValidatingQuery', data)
          .subscribe(response => {
            if (response.IsSuccess) {
              that.isLoader = false;
              // swal('', "Query Validated Successfully");
              swal(
                  {
                    title: '',
                    text: 'Query Validated Successfully.',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Proceed',
                    closeOnConfirm: true,
                    closeOnCancel: false
                  },
                  function(isConfirm) {
                    if (isConfirm) {
                      swal.close();
                      (<any>$('#showQueryModal')).modal('toggle');
                      setTimeout(function() {
                        that.showNextTab(val);
                      }, 400)
                    } else {
                      swal.close();
                      (<any>$('#showQueryModal')).modal('toggle');
                    }
                  });
            } else {
              that.isLoader = false;
              swal('', response.Message);
              (<any>$('#showQueryModal')).modal('hide');
            }
          });
    }
  }
}
