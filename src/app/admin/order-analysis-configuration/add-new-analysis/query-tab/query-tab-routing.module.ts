import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {QueryTabComponent} from './query-tab.component';

const routes: Routes = [{path: 'query-tab', component: QueryTabComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class QueryTabRoutingModule {
}
