export const QueryTabModel: any = {
  'GraphQuery': '',
  'IsUserQuery': false,
  'IsAdditionalSummary': false,
  'GrpahAdditionalSummary': '',
  'GraphAdditionalSummaryPosition': '1',
  'GraphQueryJson': {
    'selectTable': '',
    'top': '',
    'dynamicColumns': '',
    'rightSelectedList': [],
    'selectFunction': '',
    'selectFunctionAlias': '',
    'selectColumn': '',
    'selectColumnAlias': '',
    'whereCondition': [],
    'unionConditon': [],
    'orderBy': [],
    'dynamicColArr': []
  }
}