import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

import {DataService} from '../../../../mts.service';
import {AddNewChartModel} from '../add-new-analysis';

declare let swal: any;

@Component({
  selector: 'app-permission-tab',
  templateUrl: './permission-tab.component.html',
  styleUrls: ['./permission-tab.component.css']
})
export class PermissionTabComponent implements OnInit {
  DivShow: boolean = false;
  @Output() prevButtonClick = new EventEmitter();
  companyList: any;
  SelectedClient: Array<any> = [];
  isLoader: boolean;

  constructor(private dataService: DataService, private router: Router) {}

  ngOnInit() {
    if (AddNewChartModel.GraphID != 0) {
      if (AddNewChartModel.IsInternalUser == true) {
        (<any>$('#0')).prop('checked', true);
      }
      if (AddNewChartModel.ClientID.length > 1) {
        (<any>$('#externalUsers')).trigger('click');
      }
    } else {
    }
  }

  showPrevTab() {
    this.prevButtonClick.emit();
  }

  ExternalShow() {
    this.DivShow == false ? this.DivShow = true : this.DivShow = false;
    if ((<any>$('#externalUsers')).prop('checked') == true) {
      this.DivShow = true;
      var that = this;
      that.isLoader = true;
      that.dataService
          .get(
              'master/GetCompanyList?BusinessCategoryId=' +
              AddNewChartModel.BusinessCategoryId)
          .subscribe(r => {
            if (r.CompanyList.length > 0) {
              that.companyList = r.CompanyList;
              setTimeout(function() {
                if (AddNewChartModel.ClientID.length) {
                  AddNewChartModel.ClientID.forEach((data) => {
                    let id = '#' + data.toString();
                    (<any>$(id)).prop('checked', true);
                  })
                }
              }, 1000)
            }
            that.isLoader = false;
          })
    } else {
      this.DivShow = false;
    }
  }

  checkedStudents(value) {
    if ((<HTMLInputElement>document.getElementById(value)).checked === true) {
      this.SelectedClient.push(value);
      AddNewChartModel.IsInternalUser = true;
    } else if (
        (<HTMLInputElement>document.getElementById(value)).checked === false) {
      let indexx = this.SelectedClient.indexOf(value);
      this.SelectedClient.splice(indexx, 1);
      AddNewChartModel.IsInternalUser = false;
    }
  }

  save() {
    AddNewChartModel.ClientID = this.SelectedClient;
    if (AddNewChartModel.IsInternalUser == true ||
        this.SelectedClient.length > 0) {
      AddNewChartModel.Status = true;
    } else {
      AddNewChartModel.Status = false;
    }
    var that = this;
    that.isLoader = true;
    this.dataService.post('ManageGraph/SaveConfiguration', AddNewChartModel)
        .subscribe(response => {
          if (response.IsSuccess) {
            that.isLoader = false;
            swal({title: '', text: response.Message}, function() {
              that.router.navigate(['/admin/order-analysis-configuration']);
              swal.close();
            });
          } else {
            swal('', response.Message)
          }
        });
  }
}
