import { PermissionTabModule } from './permission-tab.module';

describe('PermissionTabModule', () => {
  let permissionTabModule: PermissionTabModule;

  beforeEach(() => {
    permissionTabModule = new PermissionTabModule();
  });

  it('should create an instance', () => {
    expect(permissionTabModule).toBeTruthy();
  });
});
