import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {PermissionTabRoutingModule} from './permission-tab-routing.module';
import {PermissionTabComponent} from './permission-tab.component';

@NgModule({
  imports: [CommonModule, PermissionTabRoutingModule],
  declarations: [PermissionTabComponent],
  exports: [PermissionTabComponent]
})
export class PermissionTabModule {
}
