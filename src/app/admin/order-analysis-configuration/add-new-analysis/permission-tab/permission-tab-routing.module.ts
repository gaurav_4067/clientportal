import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PermissionTabComponent} from './permission-tab.component';

const routes: Routes =
    [{path: 'permission-tab', component: PermissionTabComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class PermissionTabRoutingModule {
}
