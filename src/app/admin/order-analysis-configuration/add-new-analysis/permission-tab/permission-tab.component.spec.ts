import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionTabComponent } from './permission-tab.component';

describe('PermissionTabComponent', () => {
  let component: PermissionTabComponent;
  let fixture: ComponentFixture<PermissionTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
