import { Component, OnInit } from '@angular/core';

import { DataService } from '../../../mts.service';

import { AddNewChartModel } from './add-new-analysis';
import { DesignTabModel } from './design-tab/design-tab';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-new-analysis',
  templateUrl: './add-new-analysis.component.html',
  styleUrls: ['./add-new-analysis.component.css']
})
export class AddNewAnalysisComponent implements OnInit {
  selectedAppearance: String;
  selectedLegend: String;
  GetColumnList: any;
  x: any;
  showPreviewTab: any = false;
  showHeaderTab: any = false;
  showDesignTab: any = false;
  showQueryTab: any = false;
  showPermissionTab: any = false;
  isMultipleGraph: any = false;
  showQueryTwoTab: any = false;
  CreatedBy: any;
  isLoader: boolean;
  firstGraphName: any = '';
  secondGraphName: any = '';
  userDetail: any;
  userCredential: any;
  constructor(private dataService: DataService, private router: Router) {
    this.x = DesignTabModel;
  }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userCredential = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      this.CreatedBy = this.userDetail.UserId;
      var url = (window.location.href).split('/');
      var id = parseInt(url[url.length - 1]);
      var that = this;

      if (id > 0) {
        that.isLoader = true;
        this.dataService.get('ManageGraph/EditConfiguration?GraphID=' + id)
          .subscribe(r => {
            if (r.IsSuccess) {
              AddNewChartModel.GraphID = r.GraphID;
              AddNewChartModel.GraphTitle = r.GraphTitle;
              AddNewChartModel.isMultipleGraph = r.isMultipleGraph;
              this.isMultipleGraph = r.isMultipleGraph;
              AddNewChartModel.GraphName = r.GraphName;
              this.firstGraphName = r.GraphName;
              AddNewChartModel.GraphName2 = r.GraphName2;
              this.secondGraphName = r.GraphName2;
              AddNewChartModel.GraphJsons = r.GraphJsons;
              AddNewChartModel.GraphJsons2 = r.GraphJsons2;
              AddNewChartModel.GraphQuery = r.GraphQuery;
              AddNewChartModel.GraphQuery2 = r.GraphQuery2;
              AddNewChartModel.GraphQueryJson = r.GraphQueryJson
              AddNewChartModel.GraphQueryJson2 = r.GraphQueryJson2;
              AddNewChartModel.GraphType = r.GraphType;
              AddNewChartModel.GraphType2 = r.GraphType2;
              AddNewChartModel.ChartType = r.ChartType;
              AddNewChartModel.ChartType2 = r.ChartType2;
              AddNewChartModel.GraphPosition = r.GraphPosition;
              AddNewChartModel.IsAdditionalSummary = r.IsAdditionalSummary;
              AddNewChartModel.IsAdditionalSummary2 = r.IsAdditionalSummary2;
              AddNewChartModel.GrpahAdditionalSummary =
                r.GrpahAdditionalSummary;
              AddNewChartModel.GrpahAdditionalSummary2 =
                r.GrpahAdditionalSummary2;
              AddNewChartModel.GraphAdditionalSummaryPosition =
                r.GraphAdditionalSummaryPosition;
              AddNewChartModel.GraphAdditionalSummaryPosition2 =
                r.GraphAdditionalSummaryPosition2;
              AddNewChartModel.IsUserQuery = r.IsUserQuery;
              AddNewChartModel.IsUserQuery2 = r.IsUserQuery2;
              AddNewChartModel.IsInternalUser = r.IsInternalUser;
              AddNewChartModel.WherePop = r.WherePop;
              AddNewChartModel.WherePop2 = r.WherePop2;
              AddNewChartModel.CreatedBy = r.CreatedBy;
              AddNewChartModel.ClientID = r.ClientID;
              AddNewChartModel.showTotal = r.showTotal;
              AddNewChartModel.showTotal2 = r.showTotal2;
              AddNewChartModel.OrderAnalysisGraphSearchColumn =
                r.OrderAnalysisGraphSearchColumn;
              AddNewChartModel.BusinessCategoryId = r.BusinessCategoryId;
              AddNewChartModel.Status = r.Status;
              this.showDesignTab = true;
              that.isLoader = false;
            } else {
              AddNewChartModel.GraphID = 0;
              AddNewChartModel.GraphTitle = '';
              AddNewChartModel.isMultipleGraph = false;
              AddNewChartModel.GraphName = '';
              AddNewChartModel.GraphName2 = '';
              AddNewChartModel.GraphJsons = '';
              AddNewChartModel.GraphJsons2 = '';
              AddNewChartModel.GraphQuery = '';
              AddNewChartModel.GraphQuery2 = '';
              AddNewChartModel.GraphQueryJson = '';
              AddNewChartModel.GraphQueryJson2 = '';
              AddNewChartModel.GraphType = '';
              AddNewChartModel.GraphType2 = '';
              AddNewChartModel.ChartType = '';
              AddNewChartModel.ChartType2 = '';
              AddNewChartModel.GraphPosition = '';
              AddNewChartModel.IsAdditionalSummary = false;
              AddNewChartModel.IsAdditionalSummary2 = false;
              AddNewChartModel.GrpahAdditionalSummary = '';
              AddNewChartModel.GrpahAdditionalSummary2 = '';
              AddNewChartModel.GraphAdditionalSummaryPosition = '1';
              AddNewChartModel.GraphAdditionalSummaryPosition2 = '1';
              AddNewChartModel.IsUserQuery = false;
              AddNewChartModel.IsUserQuery2 = false;
              AddNewChartModel.IsInternalUser = false;
              AddNewChartModel.showTotal = false;
              AddNewChartModel.showTotal2 = false;
              AddNewChartModel.WherePop = [];
              AddNewChartModel.WherePop2 = [];
              AddNewChartModel.CreatedBy = this.CreatedBy;
              AddNewChartModel.ClientID = [];
              AddNewChartModel.OrderAnalysisGraphSearchColumn = [];
              AddNewChartModel.Status = false;
              AddNewChartModel.BusinessCategoryId =
                that.userDetail.BusinessCategoryId;
              that.isLoader = false;
            }
          });
      } else {
        AddNewChartModel.GraphID = 0;
        AddNewChartModel.GraphTitle = '';
        AddNewChartModel.isMultipleGraph = false;
        AddNewChartModel.GraphName = '';
        AddNewChartModel.GraphName2 = '';
        AddNewChartModel.GraphJsons = '';
        AddNewChartModel.GraphJsons2 = '';
        AddNewChartModel.GraphQuery = '';
        AddNewChartModel.GraphQuery2 = '';
        AddNewChartModel.GraphQueryJson = '';
        AddNewChartModel.GraphQueryJson2 = '';
        AddNewChartModel.GraphType = '';
        AddNewChartModel.GraphType2 = '';
        AddNewChartModel.ChartType = '';
        AddNewChartModel.ChartType2 = '';
        AddNewChartModel.GraphPosition = '';
        AddNewChartModel.IsAdditionalSummary = false;
        AddNewChartModel.IsAdditionalSummary2 = false;
        AddNewChartModel.GrpahAdditionalSummary = '';
        AddNewChartModel.GrpahAdditionalSummary2 = '';
        AddNewChartModel.GraphAdditionalSummaryPosition = '1';
        AddNewChartModel.GraphAdditionalSummaryPosition2 = '1';
        AddNewChartModel.IsUserQuery = false;
        AddNewChartModel.IsUserQuery2 = false;
        AddNewChartModel.IsInternalUser = false;
        AddNewChartModel.WherePop = [];
        AddNewChartModel.WherePop2 = [];
        AddNewChartModel.CreatedBy = this.CreatedBy;
        AddNewChartModel.ClientID = [];
        AddNewChartModel.OrderAnalysisGraphSearchColumn = [];
        AddNewChartModel.BusinessCategoryId = that.userDetail.BusinessCategoryId;
        AddNewChartModel.Status = false;
        this.showDesignTab = true;
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  getList(data: any) {
    this.GetColumnList = data;
  }

  showNextQueryTab(val: any) {
    this.isMultipleGraph = val;
  }

  showNextTab(val: any) {
    if (val.id == '#query') {
      this.showQueryTab = true;
      this.isMultipleGraph = val.isMultipleField;
      this.firstGraphName = val.firstGraphName;
      this.secondGraphName = val.secondGraphName;
      val = val.id;
    }

    if (this.isMultipleGraph == false) {
      if (val.queryCount == '1') {
        this.showQueryTab = false;
        this.showPreviewTab = true;
        val = '#previewChart';
      }
    } else {
      if (val.queryCount == '1') {
        this.showQueryTab = false;
        this.showQueryTwoTab = true;
        val = '#querySecond';
      }

      else if (val.queryCount == '2') {
        this.showPreviewTab = true;
        this.showQueryTab = false;
        val = '#previewChart';
      }
    }

    if (val == '#previewChart') {
      this.showPreviewTab = true;
    } else if (val == '#searchCriteria') {
      this.showHeaderTab = true;
    } else if (val == '#permissions') {
      this.showPermissionTab = true;
    }

    var tabid = '#tablist a[href="' + val + '"]';
    (<any>$(tabid)).tab('show');
  }

  showPrevTab(val: any) {
    if (val == '#design') {
      this.showQueryTab = false;
      this.showDesignTab = true;
    }
    if (val.count == '1') {
      this.showDesignTab = true;
      this.showQueryTab = false;
      val = '#design';
    }
    if (val.count == '2') {
      this.showQueryTab = true;
      this.showQueryTwoTab = false;
      val = '#query';
    }
    if (val == '#query') {
      this.showQueryTab = true;
      this.showPreviewTab = false;
    }
    if (val == '#querySecond') {
      this.showQueryTwoTab = true;
      this.showPreviewTab = false;
    }
    var tabid = '#tablist a[href="' + val + '"]';
    (<any>$(tabid)).tab('show');
  }
}
