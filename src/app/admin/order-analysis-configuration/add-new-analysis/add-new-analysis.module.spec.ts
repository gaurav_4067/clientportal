import { AddNewAnalysisModule } from './add-new-analysis.module';

describe('AddNewAnalysisModule', () => {
  let addNewAnalysisModule: AddNewAnalysisModule;

  beforeEach(() => {
    addNewAnalysisModule = new AddNewAnalysisModule();
  });

  it('should create an instance', () => {
    expect(addNewAnalysisModule).toBeTruthy();
  });
});
