import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DesignTabComponent} from './design-tab.component';

const routes: Routes = [{path: 'design-tab', component: DesignTabComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class DesignTabRoutingModule {
}
