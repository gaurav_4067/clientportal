import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

import {DataService} from '../../../../mts.service';
import {AddNewChartModel} from '../add-new-analysis';

import {DesignTabModel} from './design-tab';

declare let swal: any;
@Component({
  selector: 'app-design-tab',
  templateUrl: './design-tab.component.html',
  styleUrls: ['./design-tab.component.css']
})
export class DesignTabComponent implements OnInit {
  userDetail: any;
  bussinessCategories: any;
  selectedCategory: any;
  @Output() nextButtonClick = new EventEmitter();
  @Output() validate = new EventEmitter();
  @Output() nextQueryTab = new EventEmitter();

  constructor(private dataService: DataService, private router: Router) {
    this.userDetail = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.selectedCategory = this.userDetail.BusinessCategoryId;
  }

  model: any;
  validateResult: any;
  showMultiGraph: any = false;
  isLoader: boolean;

  ngOnInit() {
    // In case of Edit
    if (AddNewChartModel.GraphID != 0) {
      this.selectedCategory = AddNewChartModel.BusinessCategoryId;
      DesignTabModel.isMultipleGraph = AddNewChartModel.isMultipleGraph;
      DesignTabModel.GraphTitle = AddNewChartModel.GraphTitle;
      DesignTabModel.GraphName = AddNewChartModel.GraphName;
      DesignTabModel.GraphType = AddNewChartModel.GraphType;
      DesignTabModel.ChartType = AddNewChartModel.ChartType;
      DesignTabModel.GraphJsons = JSON.parse(AddNewChartModel.GraphJsons);
      DesignTabModel.GraphName2 = AddNewChartModel.GraphName2;
      DesignTabModel.GraphType2 = AddNewChartModel.GraphType2;
      DesignTabModel.ChartType2 = AddNewChartModel.ChartType2;
      DesignTabModel.GraphJsons2 = JSON.parse(AddNewChartModel.GraphJsons2);
      DesignTabModel.showTotal = AddNewChartModel.showTotal;
      DesignTabModel.showTotal2 = AddNewChartModel.showTotal2;
      DesignTabModel.GraphPosition = AddNewChartModel.GraphPosition;
      this.model = DesignTabModel;

      if (DesignTabModel.isMultipleGraph == true) {
        (<any>$('#showMultiple')).trigger('click');
      }

      if (DesignTabModel.GraphJsons.showRatio == true) {
        setTimeout(function() {
          (<any>$('#showRatio')).trigger('click');
        }, 500)
      }

      if (DesignTabModel.GraphJsons2.showRatio == true) {
        setTimeout(function() {
          (<any>$('#showRatio1')).trigger('click');
        }, 500)
      }

      if (DesignTabModel.showTotal == true) {
        setTimeout(function() {
          (<any>$('#showTotal')).trigger('click');
        }, 500)
      }

      if (DesignTabModel.showTotal2 == true) {
        setTimeout(function() {
          (<any>$('#showTotal1')).trigger('click');
        }, 500)
      }
    }

    // In case of Add New
    else {
      DesignTabModel.isMultipleGraph = false, DesignTabModel.GraphTitle = '';
      DesignTabModel.GraphName = '';
      DesignTabModel.GraphName2 = '';
      DesignTabModel.GraphJsons = {
        'GraphXAxis': '',
        'GraphYAxis': '',
        'GraphAppearance': '2D',
        'GraphLegendPosition': 'Top',
        'showRatio': false,
        'othersInclude': ''
      };
      DesignTabModel.GraphJsons2 = {
        'GraphXAxis': '',
        'GraphYAxis': '',
        'GraphAppearance': '2D',
        'GraphLegendPosition': 'Top',
        'showRatio': false,
        'othersInclude': ''
      };
      DesignTabModel.GraphType = '1';
      DesignTabModel.GraphType2 = '1';
      DesignTabModel.ChartType = '1';
      DesignTabModel.ChartType2 = '1';
      DesignTabModel.showTotal = false;
      DesignTabModel.showTotal2 = false;
      DesignTabModel.GraphPosition = '1';

      this.model = DesignTabModel;
    }
    this.dataService.get('DynamicFormCreation/BussinessType').subscribe(r => {
      if (r.IsSuccess) {
        this.bussinessCategories = r.Data;
      }
    });
  }

  showRatio() {
    if ((<any>$('#showRatio')).prop('checked') == true) {
      this.model.GraphJsons.showRatio = true;
    } else {
      this.model.GraphJsons.showRatio = false;
    }
  }

  showRatio1() {
    if ((<any>$('#showRatio1')).prop('checked') == true) {
      this.model.GraphJsons2.showRatio = true;
    } else {
      this.model.GraphJsons2.showRatio = false;
    }
  }

  showTotal() {
    if ((<any>$('#showTotal')).prop('checked') == true) {
      this.model.showTotal = true;
    } else {
      this.model.showTotal = false;
    }
  }

  showTotal1() {
    if ((<any>$('#showTotal1')).prop('checked') == true) {
      this.model.showTotal2 = true;
    } else {
      this.model.showTotal2 = false;
    }
  }

  bindToModel() {
    AddNewChartModel.isMultipleGraph = this.model.isMultipleGraph;
    AddNewChartModel.GraphTitle = this.model.GraphTitle;
    AddNewChartModel.GraphName = this.model.GraphName;
    AddNewChartModel.GraphType = this.model.GraphType;
    AddNewChartModel.ChartType = this.model.ChartType;
    AddNewChartModel.GraphJsons = JSON.stringify(this.model.GraphJsons);
    AddNewChartModel.GraphName2 = this.model.GraphName2;
    AddNewChartModel.GraphType2 = this.model.GraphType2;
    AddNewChartModel.ChartType2 = this.model.ChartType2;
    AddNewChartModel.GraphJsons2 = JSON.stringify(this.model.GraphJsons2);
    AddNewChartModel.GraphPosition = this.model.GraphPosition;
    AddNewChartModel.showTotal = this.model.showTotal;
    AddNewChartModel.showTotal2 = this.model.showTotal2;
    AddNewChartModel.BusinessCategoryId = this.selectedCategory;
  }

  showNextTab() {
    this.validateResult = this.getValidate(this.model);
    if (this.validateResult.success == false) {
      (<any>$('input[required]')).each(function() {
        this.focus();
        this.blur();
      })
    } else {
      this.bindToModel();
      this.nextButtonClick.emit({
        id: '#query',
        isMultipleField: this.model.isMultipleGraph,
        firstGraphName: this.model.GraphName,
        secondGraphName: this.model.GraphName2
      });
    }
  }

  graphTypeChange() {
    if (this.model.GraphType == '2') {
      this.model.GraphName = '';
      this.model.ChartType = '1';
      this.model.GraphJsons = {
        'GraphXAxis': '',
        'GraphYAxis': '',
        'GraphAppearance': '2D',
        'GraphLegendPosition': 'Top',
        'showRatio': false,
        'othersInclude': ''
      }
    }
  }

  graphType2Change() {
    if (this.model.GraphType2 == '2') {
      this.model.GraphName2 = '';
      this.model.ChartType2 = '1';
      this.model.GraphJsons2 = {
        'GraphXAxis': '',
        'GraphYAxis': '',
        'GraphAppearance': '2D',
        'GraphLegendPosition': 'Top',
        'showRatio': false,
        'othersInclude': ''
      }
    }
  }

  save() {
    this.validateResult = this.getValidate(this.model);
    if (this.validateResult.success == false) {
      (<any>$('input[required]')).each(function() {
        this.focus();
        this.blur();
      })
    } else {
      this.bindToModel();
      var that = this;
      that.isLoader = true;
      this.dataService.post('ManageGraph/SaveConfiguration', AddNewChartModel)
          .subscribe(response => {
            if (response.IsSuccess) {
              that.isLoader = false;
              swal({title: '', text: response.Message}, function() {
                that.router.navigate(['/admin/order-analysis-configuration']);
                swal.close();
              });
            } else {
              that.isLoader = false;
              swal('', response.Message)
            }
          });
    }
  }

  showMultipleGraph() {
    if ((<any>$('#showMultiple')).prop('checked') == true) {
      this.showMultiGraph = true;
      this.model.isMultipleGraph = true;
      this.nextQueryTab.emit(this.model.isMultipleGraph);
    } else {
      this.showMultiGraph = false;
      this.model.isMultipleGraph = false;
      this.nextQueryTab.emit(this.model.isMultipleGraph);

      this.model.GraphName2 = '';
      this.model.GraphJsons2 = {
        'GraphXAxis': '',
        'GraphYAxis': '',
        'GraphAppearance': '2D',
        'GraphLegendPosition': 'Top'
      };
      this.model.GraphQuery2 = '';
      this.model.GraphType2 = '1';
      this.model.ChartType2 = '1';
    }
  }


  getValidate(data: any) {
    var validate = {
      success: true
    }

    if (data.GraphTitle == '') {
      validate.success = false;
    }

    if (this.model.GraphType != 2 && this.showMultiGraph == false) {
      if (data.GraphName == '') {
        validate.success = false;
      }
      if (data.GraphJsons.GraphXAxis == '') {
        validate.success = false;
      }
      if (data.GraphJsons.GraphYAxis == '') {
        validate.success = false;
      }
    } else if (
        this.model.GraphType != 2 && this.showMultiGraph == true &&
        this.model.GraphType2 != 2) {
      if (data.GraphName == '' || data.GraphName2 == '') {
        validate.success = false;
      }
      if (data.GraphJsons.GraphXAxis == '' ||
          data.GraphJsons2.GraphXAxis == '') {
        validate.success = false;
      }
      if (data.GraphJsons.GraphYAxis == '' ||
          data.GraphJsons2.GraphYAxis == '') {
        validate.success = false;
      }
    } else if (
        this.model.GraphType != 2 && this.showMultiGraph == true &&
        this.model.GraphType2 == 2) {
      if (data.GraphName == '') {
        validate.success = false;
      }
      if (data.GraphJsons.GraphXAxis == '') {
        validate.success = false;
      }
      if (data.GraphJsons.GraphYAxis == '') {
        validate.success = false;
      }
    } else if (
        this.model.GraphType == 2 && this.showMultiGraph == true &&
        this.model.GraphType2 != 2) {
      if (data.GraphName2 == '') {
        validate.success = false;
      }
      if (data.GraphJsons2.GraphXAxis == '') {
        validate.success = false;
      }
      if (data.GraphJsons2.GraphYAxis == '') {
        validate.success = false;
      }
    }
    return validate;
  }
}
