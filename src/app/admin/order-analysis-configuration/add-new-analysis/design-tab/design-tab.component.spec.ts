import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignTabComponent } from './design-tab.component';

describe('DesignTabComponent', () => {
  let component: DesignTabComponent;
  let fixture: ComponentFixture<DesignTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
