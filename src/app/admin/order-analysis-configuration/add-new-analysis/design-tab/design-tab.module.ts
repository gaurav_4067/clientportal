import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MaterialModule, MdCoreModule, MdInputModule} from '@angular/material';
import {Md2Module} from 'md2';

import {DesignTabRoutingModule} from './design-tab-routing.module';
import {DesignTabComponent} from './design-tab.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, MdInputModule, Md2Module.forRoot(), MdCoreModule,
    MaterialModule, DesignTabRoutingModule
  ],
  declarations: [DesignTabComponent],
  exports: [DesignTabComponent]
})
export class DesignTabModule {
}
