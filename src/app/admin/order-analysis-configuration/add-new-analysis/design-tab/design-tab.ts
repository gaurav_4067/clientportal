export const DesignTabModel: any = {
  'isMultipleGraph': false,
  'GraphTitle': '',
  'GraphName': '',
  'GraphName2': '',
  'GraphJsons': {
    'GraphXAxis': '',
    'GraphYAxis': '',
    'GraphAppearance': '2D',
    'GraphLegendPosition': 'Top',
    'showRatio': false,
    'othersInclude': ''
  },
  'GraphJsons2': {
    'GraphXAxis': '',
    'GraphYAxis': '',
    'GraphAppearance': '2D',
    'GraphLegendPosition': 'Top',
    'showRatio': false,
    'othersInclude': ''
  },

  'showTotal': false,
  'showTotal2': false,
  'GraphType': '1',
  'GraphType2': '1',
  'ChartType': '1',
  'ChartType2': '1',
  'GraphPosition': '1'
};