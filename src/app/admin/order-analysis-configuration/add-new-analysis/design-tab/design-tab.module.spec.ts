import { DesignTabModule } from './design-tab.module';

describe('DesignTabModule', () => {
  let designTabModule: DesignTabModule;

  beforeEach(() => {
    designTabModule = new DesignTabModule();
  });

  it('should create an instance', () => {
    expect(designTabModule).toBeTruthy();
  });
});
