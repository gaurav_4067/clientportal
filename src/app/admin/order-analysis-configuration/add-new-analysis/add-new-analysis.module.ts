import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MaterialModule, MdCoreModule} from '@angular/material';
import {MdInputModule} from '@angular2-material/input';
import {Md2Module} from 'md2';

import {FooterModule} from '../../../footer/footer.module';
import {HeaderModule} from '../../../header/header.module';

import {AddNewAnalysisRoutingModule} from './add-new-analysis-routing.module';
import {AddNewAnalysisComponent} from './add-new-analysis.component';
import {DesignTabComponent} from './design-tab/design-tab.component';
import {DesignTabModule} from './design-tab/design-tab.module';
import {PermissionTabModule} from './permission-tab/permission-tab.module';
import {PreviewTabModule} from './preview-tab/preview-tab.module';
import {QueryTabModule} from './query-tab/query-tab.module';
import {SearchTabModule} from './search-tab/search-tab.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, HeaderModule, FooterModule, MaterialModule,
    Md2Module.forRoot(), MdInputModule, MdCoreModule, DesignTabModule,
    PermissionTabModule, PreviewTabModule, QueryTabModule, SearchTabModule,
    AddNewAnalysisRoutingModule
  ],
  declarations: [AddNewAnalysisComponent]
})
export class AddNewAnalysisModule {
}
