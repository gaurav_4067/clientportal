import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {SearchTabComponent} from './search-tab.component';

const routes: Routes = [{path: 'search-tab', component: SearchTabComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class SearchTabRoutingModule {
}
