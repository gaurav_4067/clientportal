import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MdAutocompleteModule, MdCoreModule, MdInputModule} from '@angular/material';

import {SearchTabRoutingModule} from './search-tab-routing.module';
import {SearchTabComponent} from './search-tab.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, MdCoreModule, MdAutocompleteModule,
    ReactiveFormsModule, MdInputModule, SearchTabRoutingModule
  ],
  declarations: [SearchTabComponent],
  exports: [SearchTabComponent]
})
export class SearchTabModule {
}
