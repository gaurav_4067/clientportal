import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { DataService } from '../../../../mts.service';
import { AddNewChartModel } from '../add-new-analysis';

declare let swal: any;

@Component({
  selector: 'app-search-tab',
  templateUrl: './search-tab.component.html',
  styleUrls: ['./search-tab.component.css']
})
export class SearchTabComponent implements OnInit {
  public defaultSelectedCompany: any;
  isLoader: boolean;
  private GetGraphColumns: any;
  private selectedSearchColumns: any;
  userCredential: any;
  public tempselectedAvailG: any;
  public tempSelectedGridC: any;
  public AliasInput: any;
  myControl = new FormControl();
  filteredOptions: any;

  searchObj: any = {
    ColumnName: '',
    Caption: '',
    FieldType: '2',
    TableName: '',
    GraphColumnID: 0
  };

  searchCriteriaArr: any = [];
  selectFieldArr: any = [];

  constructor(private dataService: DataService, private router: Router) { }

  @Output() nextButtonClick = new EventEmitter();
  @Output() prevButtonClick = new EventEmitter();
  @Input() getcolumnlist;

  ngOnInit() {
    if (AddNewChartModel.GraphID != 0) {
      this.searchCriteriaArr = AddNewChartModel.OrderAnalysisGraphSearchColumn;
    } else {
      this.searchCriteriaArr = [];
    }

    var that = this;
    that.isLoader = true;
    this.dataService.get('OrderAnalysis/TableMaster')
      .subscribe(r => {
        this.selectFieldArr = r.Data;
        that.isLoader = false;
      })

    this.getcolumnlist = this.getcolumnlist
      .map((data) => {
        return data.ColumnName;
      })

    this.filteredOptions =
      this.myControl.valueChanges.startWith(null).map(
        val => val ? this.filter(val) : this.getcolumnlist.slice());
  }

  filter(val: string): string[] {
    return this.getcolumnlist.filter(
      option => new RegExp(`^${val}`, 'gi').test(option));
  }

  showNextTab() {
    this.bindToModel();
    this.nextButtonClick.emit();
  }

  bindToModel() {
    AddNewChartModel.OrderAnalysisGraphSearchColumn = this.searchCriteriaArr;
  }

  showPrevTab() {
    this.prevButtonClick.emit();
  }

  save() {
    this.bindToModel();
    var that = this;
    that.isLoader = true;
    this.dataService.post('ManageGraph/SaveConfiguration', AddNewChartModel)
      .subscribe(response => {
        if (response.IsSuccess) {
          that.isLoader = false;
          swal({ title: '', text: response.Message }, function () {
            that.router.navigate(['/admin/order-analysis-configuration']);
            swal.close();
          });
        } else {
          that.isLoader = false;
          swal('', response.Message)
        }
      });
  }

  validate(data: any) {
    var validateResult: boolean = true;

    if (data.ColumnName == '' || data.Caption == '') {
      validateResult = false;
    } else {
      if (data.FieldType == 1 && data.TableName == '') {
        validateResult = false;
      }
    }
    return validateResult;
  }

  addToTable() {
    var newArr = Object.assign({}, this.searchObj);
    var validateResult = this.validate(newArr);
    if (validateResult == true) {
      if (newArr.FieldType == 1) {
        newArr.FieldType = 'Dropdown';
      } else if (newArr.FieldType == 2) {
        newArr.FieldType = 'Textbox';
      } else if (newArr.FieldType == 3) {
        newArr.FieldType = 'Date Range picker';
      }
      this.searchCriteriaArr.push(newArr);

      this.searchObj.ColumnName = '';
      this.searchObj.Caption = '';
      this.searchObj.FieldType = '2';
      this.searchObj.TableName = '';
      this.searchObj.GraphColumnID = 0;
    } else {
      swal('', 'Please Fill All Fields');
    }
  }

  editToTable(e, index: any) {
    e.preventDefault();
    var obj = Object.assign({}, this.searchCriteriaArr[index]);
    this.searchObj.ColumnName = obj.ColumnName;
    this.searchObj.Caption = obj.Caption;

    if (obj.FieldType == 'Dropdown') {
      obj.FieldType = '1';
    } else if (obj.FieldType == 'Textbox') {
      obj.FieldType = '2';
    } else if (obj.FieldType == 'Date Range picker') {
      obj.FieldType = '3';
    }
    this.searchObj.FieldType = obj.FieldType;
    this.searchObj.TableName = obj.TableName;
    this.searchObj.GraphColumnID = obj.GraphColumnID;
    this.searchCriteriaArr.splice(index, 1);
  }

  deleteFromTable(e, index: any) {
    e.preventDefault();
    var that = this;
    swal(
      {
        title: '',
        text:
          '<span style=\'color:#ef770e;font-size:20px;\'>Are you sure?</span><br><br>',
        html: true,
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Proceed',
        cancelButtonText: 'Cancel',
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function (isConfirm) {
        that.searchCriteriaArr.splice(index, 1);
      });
  }
}
