import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderAnalysisConfigurationComponent } from './order-analysis-configuration.component';

const routes: Routes = [{
    path: '',
    children: [
        { path: '', component: OrderAnalysisConfigurationComponent },
        {
            path: 'add-new-analysis/:id',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './add-new-analysis/add-new-analysis.module')
                                        .AddNewAnalysisModule);
                            })
                })
        },
        {
            path: 'edit-analysis/:id',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './add-new-analysis/add-new-analysis.module')
                                        .AddNewAnalysisModule);
                            })
                })
        },
    ]
}];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class OrderAnalysisConfigurationRoutingModule {
}
