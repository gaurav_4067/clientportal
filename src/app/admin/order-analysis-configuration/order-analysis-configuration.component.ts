import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { InjectService } from '../../injectable-service/injectable-service';
import { DataService } from '../../mts.service';

import { SEARCH_MODEL } from './order-analysis-configuration';
declare let swal: any;

@Component({
  selector: 'app-order-analysis-configuration',
  templateUrl: './order-analysis-configuration.component.html',
  styleUrls: ['./order-analysis-configuration.component.css']
})
export class OrderAnalysisConfigurationComponent implements OnInit {
  selectedBussinessCategory: number;
  bussinessCategories: any;
  private p: any;
  private itemPerPage: any;
  showsetting: boolean;
  public defaultSelectedCompany: any;
  isLoader: boolean;
  companyList: any;
  userCredential: any;
  public selectedCompany: any;
  sortType: string;
  totalItem: number;
  roleManagementModel: any;
  searchModal: any;
  searchText: any;
  GraphData: any;
  OrderList: any;
  count: any = 0;
  clientCtrl: FormControl;
  filteredClients: any;
  clientList: any = [];
  appearance: any = ['2D', '3D'];
  graphType: any = { 1: 'Bar', 2: 'Bar Stack', 3: 'Pie', 4: 'Donut' }
  userDetail: any;
  AutoCompleteReset: any;


  constructor(
    private dataService: DataService, private injectService: InjectService,
    public router: Router) {
    this.p = 1;
    this.itemPerPage = 10;
    this.showsetting = false;
  }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      if (this.userCredential.UserPermission.ViewOrderAnalysisconfiguration) {
        this.p = 1;
        this.sortType = 'asc';
        this.itemPerPage = 1;
        this.totalItem = 0;
        this.searchModal = SEARCH_MODEL;
        this.searchModal.pageSize = 10;
        this.searchModal.PageNumber = 1;
        this.searchModal.orderBy = '';
        this.searchModal.order = 'asc';
        this.searchModal.ClientId = 0;
        this.searchModal.totalRecords = '';
        this.searchModal.businessCategoryId =
          this.userCredential.BusinessCategoryId;
        this.isLoader = true;
        // this.dataService.post('ManageGraph/GetGraphList',
        // this.searchModal).subscribe(
        //     r => {
        //         if (r.IsSuccess) {

        //             this.GraphData = r.GraphsList;
        //             this.itemPerPage = 1;
        //             this.totalItem = Math.ceil(r.TotalRecords /
        //             this.searchModal.pageSize);
        //         }
        //     }
        // );
        this.dataService.get('DynamicFormCreation/BussinessType').subscribe(r => {
          if (r.IsSuccess) {
            this.bussinessCategories = r.Data;
          }
        });
        // this.defaultSelectedCompany = {
        //   'CompanyId': 0,
        //   'CompanyName': 'Select Client'
        // };
        var that = this;
        that.getCountries();
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  getCountries() {
    this.isLoader = true;
    this.searchModal.ClientId = 0;
    this.dataService
      .get(
        'master/GetCompanyList?BusinessCategoryId=' +
        this.searchModal.businessCategoryId)
      .subscribe(r => {
        if (r.CompanyList.length > 0) {
          this.companyList = r.CompanyList;
          // this.companyList.splice(0, 0, this.defaultSelectedCompany);
          this.clientList = this.companyList.map(
            (data, index) => {
              return ({ name: data.CompanyName, id: data.CompanyId })
            });
          this.clientCtrl = new FormControl();
          this.filteredClients =
            this.clientCtrl.valueChanges.startWith(null)
              .map(
                user =>
                  user && typeof user === 'object' ? user.name : user)
              .map(
                name =>
                  name ? this.filter(name) : this.clientList.slice());
          this.isLoader = false;
        } else {
          this.companyList = [];
          this.clientList = [];
          this.isLoader = false;
        }
      });
    this.search();
  }

  filter(name: string): any {
    return this.clientList.filter(
      option => new RegExp(`^${name}`, 'gi').test(option.name));
  }

  selectedClient(id: any) {
    this.searchModal.ClientId = id;
    // console.log("idddsds",id);
    // this.count++;
    // if (this.count == 1) {
    //     this.searchModal.ClientId = id;
    // }
    // else if (this.count >= 2) {
    //     if (this.count % 2 == 0) {
    //         this.searchModal.ClientId = id;
    //     }
    //     else {
    //         this.count = 1;
    //     }
    // }
  }

  reset() {
    this.searchModal.ClientId = 0;
    this.searchModal.businessCategoryId =
      this.userCredential.BusinessCategoryId;
    this.AutoCompleteReset = null;
    this.search();
  }

  sorting(column: string) {
    this.searchModal.OrderBy = column;
    if (this.searchModal.Order == 'asc') {
      this.searchModal.Order = 'desc';
    } else {
      this.searchModal.Order = 'asc';
    }
    this.isLoader = true;
    this.dataService.post('ManageGraph/GetGraphList', this.searchModal)
      .subscribe(r => {
        this.isLoader = false;
        if (r.IsSuccess) {
          this.GraphData = r.GraphsList;
        } else {
          swal('', r.Message);
        }
      })
  }

  getPage(page: number) {
    this.searchModal.PageNumber = page;
    this.isLoader = true;
    this.dataService.post('ManageGraph/GetGraphList', this.searchModal)
      .subscribe(r => {
        this.isLoader = false;
        if (r.IsSuccess) {
          this.p = page;
          this.GraphData = r.GraphsList;
        } else {
          swal('', r.Message);
        }
      })
  }

  search() {
    this.isLoader = true;
    this.dataService.post('ManageGraph/GetGraphList', this.searchModal)
      .subscribe(r => {
        this.isLoader = false;
        this.GraphData = r.GraphsList;
        this.itemPerPage = 1;
        this.totalItem =
          Math.ceil(r.TotalRecords / this.searchModal.pageSize);
      });
  }

  deleteRole(GraphId) {
    var that = this;
    swal(
      {
        title: '',
        text:
          '<span style=\'color:#ef770e;font-size:20px;\'>Are you sure?</span><br><br>',
        html: true,
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Proceed',
        cancelButtonText: 'Cancel',
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function (isConfirm) {
        if (isConfirm) {
          that.dataService
            .get(
              'ManageGraph/DeleteConfiguration?GraphId=' + GraphId +
              '&UserId=' + that.userCredential.UserId)
            .subscribe(r => {
              if (r.IsSuccess == true) {
                that.p = 1;
                that.sortType = 'asc';
                that.itemPerPage = 1;
                that.totalItem = 0;
                that.searchModal = SEARCH_MODEL;
                that.dataService
                  .post('ManageGraph/GetGraphList', that.searchModal)
                  .subscribe(r => {
                    if (r.IsSuccess) {
                      that.GraphData = r.GraphsList;
                      that.itemPerPage = 1;
                      that.totalItem = Math.ceil(
                        r.TotalRecords / that.searchModal.pageSize);
                    }
                  });
                swal('', r.Message);
              }
            });
        } else {
        }
      });
  }

  EditRole(GraphId) {
    // this.dataService.get('ManageGraph/EditConfiguration?GraphID='+GraphId).subscribe(
    //     r => {
    //         if (r.IsSuccess) {
    //             this.GraphData = r.GraphsList;
    //             this.itemPerPage = 1;
    //             this.totalItem = Math.ceil(r.TotalRecords /
    //             this.searchModal.pageSize);
    //         }
    //     }
    // );
    this.dataService.EditGraphConfiguration = GraphId;
    this.router.navigate([this.router.url + '/edit-analysis', GraphId]);
  }
}
