
export const ROLE_MANAGEMENT_MODEL = {
  'orderTracking': {
    'AddNew': false,
    'View': false,
    'Edit': false,
    'Delete': false,
    'DocumentsUpload': false,
    'DocumentsDownload': false,
    'Graph1': false,
    'Graph2': false
  },
  'orderTracking2': {
    'AddNew': false,
    'View': false,
    'Edit': false,
    'Delete': false,
    'DocumentsUpload': false,
    'DocumentsDownload': false,
    'Graph1': false,
    'Graph2': false
  },
  'orderAnalysis': {
    'AddNew': false,
    'View': false,
    'Edit': false,
    'Delete': false,
    'DocumentsUpload': false,
    'DocumentsDownload': false,
    'Graph1': false,
    'Graph2': false
  },
  'productDeposition': {
    'AddNew': false,
    'View': false,
    'Edit': false,
    'Delete': false,
    'DocumentsUpload': false,
    'DocumentsDownload': false,
    'Graph1': false,
    'Graph2': false
  },
  'orderCommunication': {
    'AddNew': false,
    'View': false,
    'Edit': false,
    'Delete': false,
    'DocumentsUpload': false,
    'DocumentsDownload': false,
    'Graph1': false,
    'Graph2': false
  },
  'dashboard': {
    'AddNew': false,
    'View': false,
    'Edit': false,
    'Delete': false,
    'DocumentsUpload': false,
    'DocumentsDownload': false,
    'Graph1': false,
    'Graph2': false
  }
};

export const SEARCH_MODEL = {
  'pageSize': '10',
  'PageNumber': '1',
  'orderBy': '',
  'order': 'asc',
  'ClientId': '0',
  'totalRecords': ''
};
export const SEARCH_TEXT = {
  'RoleName': ''
}
