import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderAnalysisConfigurationComponent } from './order-analysis-configuration.component';

describe('OrderAnalysisConfigurationComponent', () => {
  let component: OrderAnalysisConfigurationComponent;
  let fixture: ComponentFixture<OrderAnalysisConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderAnalysisConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderAnalysisConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
