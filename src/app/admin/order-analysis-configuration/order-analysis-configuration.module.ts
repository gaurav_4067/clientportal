import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { Ng2PaginationModule } from 'ng2-pagination';

import { FooterModule } from '../../footer/footer.module';
import { HeaderModule } from '../../header/header.module';

import { AddNewAnalysisModule } from './add-new-analysis/add-new-analysis.module';
import { OrderAnalysisConfigurationRoutingModule } from './order-analysis-configuration-routing.module';
import { OrderAnalysisConfigurationComponent } from './order-analysis-configuration.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, FooterModule, HeaderModule, MaterialModule,
    ReactiveFormsModule, Ng2PaginationModule,
    OrderAnalysisConfigurationRoutingModule
  ],
  declarations: [OrderAnalysisConfigurationComponent]
})
export class OrderAnalysisConfigurationModule {
}
