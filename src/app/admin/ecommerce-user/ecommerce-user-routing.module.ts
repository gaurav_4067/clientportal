import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EcommerceUserComponent } from 'app/admin/ecommerce-user/ecommerce-user.component';

const routes: Routes = [
  {path :'',component :EcommerceUserComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EcommerceUserRoutingModule { }
