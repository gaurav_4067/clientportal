
export class SearhUserRequest{
    public UserName:string;
    public CompanyName:string;
    public CountryName:string;
    public OrderBy:string='UserId';
    public Order:string='desc';
}
export class EcommerceUserList{
    public UserId:number;
    public UserName :string;
    public Name :string;
    public FirstName :string;
    public MiddleName :string;
    public LastName :string;
    public CompanyName :string;
    public CompanyAddress :string;
    public CountryName :string;
    public StateName :string;
    public CityName :string;
    public Telephone :string;
    public Email :string;
    public Mobile :string;
    public CreatedName :string;
    public CreatedDate :string;
    public ZipCode :number;
    public PlacedOrder :number;
}