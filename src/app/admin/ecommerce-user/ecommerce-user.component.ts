import { Component, OnInit } from '@angular/core';
import { DataService, BaseUrl } from '../../mts.service';
import {SearhUserRequest,EcommerceUserList} from './ecommerce-user.model';

@Component({
  selector: 'app-ecommerce-user',
  templateUrl: './ecommerce-user.component.html',
  styleUrls: ['./ecommerce-user.component.css']
})
export class EcommerceUserComponent implements OnInit {
  isLoader:boolean;
  userCredential:any;
  userDetail:any;
  searhUserRequest:SearhUserRequest;
  ecommerceUserList:EcommerceUserList;


  constructor(private dataService:DataService) {
    this.searhUserRequest= new SearhUserRequest();
    this.ecommerceUserList= new EcommerceUserList();
   }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.getUserList();
  }


  getUserList(){
    this.isLoader=true;
    this.dataService
    .post(
      'User/GetAllEcomUserList',this.searhUserRequest)
    .subscribe(r => {
      this.ecommerceUserList=r.EcommerceUsers
      this.isLoader=false;
    })
  }

  resetSearchForm(){
    this.isLoader=true;
    this.searhUserRequest=new SearhUserRequest();
   this.getUserList();
  }

  exportToExcel(){
    this.isLoader=true;
    this.dataService
    .post(
      'User/ExportToExcelEcomUserList',this.searhUserRequest)
    .subscribe(r => {
      if (r.IsSuccess) {
        this.isLoader = false;
        let downloadFileName = encodeURIComponent(r.Data);
        window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + r.Data + "&fileType=excel&businessCategoryId="+this.userCredential.BusinessCategoryId;
      }
    })

  }

}
