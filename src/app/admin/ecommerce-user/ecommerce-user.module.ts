import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EcommerceUserRoutingModule } from './ecommerce-user-routing.module';
import { EcommerceUserComponent } from './ecommerce-user.component';
import { FormsModule } from '@angular/forms';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    EcommerceUserRoutingModule,
    FormsModule,
    HeaderModule, 
    FooterModule,
  ],
  declarations: [EcommerceUserComponent]
})
export class EcommerceUserModule { }
