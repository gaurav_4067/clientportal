import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Md2Module } from 'md2';
import { Ng2PaginationModule } from 'ng2-pagination';

import { FooterModule } from '../../footer/footer.module';
import { HeaderModule } from '../../header/header.module';
import { ManageRolesRoutingModule } from './manage-roles-routing.module';
import { ManageRolesComponent } from './manage-roles.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FooterModule,
    HeaderModule,
    Ng2PaginationModule,
    Md2Module.forRoot(),
    ManageRolesRoutingModule,
  ],
  declarations: [ManageRolesComponent]
})
export class ManageRolesModule {
}
