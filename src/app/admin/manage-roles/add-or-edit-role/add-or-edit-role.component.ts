import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IMultiSelectOption, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

import { InjectService } from '../../../injectable-service/injectable-service';
import { DataService } from '../../../mts.service';

declare let swal: any;
@Component({
  selector: 'app-add-or-edit-role',
  templateUrl: './add-or-edit-role.component.html',
  styleUrls: ['./add-or-edit-role.component.css']
})
export class AddOrEditRoleComponent implements OnInit {
  roleId: number;
  private permission: any;
  private permissionList: any;
  private radioChecked: any;
  private companyList: IMultiSelectOption[];
  private categoryList: any;
  public selectedCompany: any;
  private selectedCategory: string;
  private viewName: string;
  private selectCompanyText:
    IMultiSelectTexts = { defaultTitle: 'Select Company' };
  isLoader: boolean;
  userCredential: any;
  userDetail: any;
  onlyView:boolean=false;

  constructor(
    private dataService: DataService, private injectService: InjectService,
    private router: Router) { }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      if (this.userCredential.UserPermission.EditRole && this.userCredential.UserPermission.NewRole) {
        this.roleId = 0;
        this.viewName = 'Add';
        if (this.injectService.roleModel) {
          this.roleId = this.injectService.roleModel.RoleId;
          this.viewName = 'Edit';
        }
        if (window.location.href.includes('admin/manage-roles/view-role')) {
        var Url = window.location.href;
        var data = window.location.href.split('/');
        this.roleId = Number(data[data.length - 1]);
        this.viewName = 'View';
        this.onlyView=true;
        }
        // this.roleModel=this.injectService.roleModel;
        // this.selectedCategory='MTS Internal User';
        this.selectedCompany = [];
        this.companyList = [];
        this.categoryList = [];
        this.permission = {};
        this.radioChecked = 'AllCompanies';
        this.permission.CategoryPermissionList = [];
        this.isLoader = true;
        this.dataService.get('master/GetRolePermissions?RoleId=' + this.roleId)
          .subscribe(r => {
            this.isLoader = false;
            this.permission = r;
            for (var i = 0; i < this.permission.RoleForCompany.length; i++) {
              this.selectedCompany.push(
                this.permission.RoleForCompany[i].CompanyId);
            }
            if (this.permission.AllCompanies == false) {
              this.radioChecked = 'companySpecific';
            }
            this.permissionList =
              this.permission.CategoryPermissionList[0].PermissionList;
          });
        this.dataService.get('master/GetCompanyList').subscribe(r => {
          var list = [];
          // this.companyList=r.CompanyList;//JSON.parse(r.toString());
          r.CompanyList.forEach(function (company) {
            list.push({ id: company.CompanyId, name: company.CompanyName })
          });
          this.companyList = list;
        });
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
    // if (!this.userCredential.UserPermission.EditRole) {
    //   this.router.navigate(['/landing-page']);
    // }
    // if (!this.userCredential.UserPermission.NewRole) {
    //   this.router.navigate(['/landing-page']);
    // }
    /*this.dataService.get('master/GetUserCategory').subscribe(
        r => {
            this.categoryList=JSON.parse(r.toString());
        }
    );*/
  }
  saveRole() {
    if (!this.permission.RoleName) {
      swal('', 'Please Select Role Name');
      return;
    }

    var selectedCompanyList = [];
    if (this.radioChecked == 'AllCompanies') {
      this.permission.AllCompanies = true;
      selectedCompanyList = [];
    } else {
      if (this.selectedCompany) {
        this.selectedCompany.forEach(function (companyId) {
          selectedCompanyList.push({ CompanyId: companyId });
        })
      } else {
        swal('', 'Please Select Company');
        return;
      }
    }
    this.permission.RoleForCompany = selectedCompanyList;
    this.isLoader = true;
    this.permission.Createdby = this.userCredential.UserId;
    this.dataService.post('master/SaveRole', this.permission).subscribe(r => {
      this.isLoader = false;
      if (r.IsSuccess) {
        this.router.navigate(['/admin/manage-roles']);
      } else {
        swal('', r.Message);
      }
    });
  }
}
