import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddOrEditRoleComponent } from './add-or-edit-role.component';

const routes: Routes = [
  {
    path: '',
    component: AddOrEditRoleComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddOrEditRoleRoutingModule { }
