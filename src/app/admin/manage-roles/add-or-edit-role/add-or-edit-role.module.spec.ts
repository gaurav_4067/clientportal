import { AddOrEditRoleModule } from './add-or-edit-role.module';

describe('AddOrEditRoleModule', () => {
  let addOrEditRoleModule: AddOrEditRoleModule;

  beforeEach(() => {
    addOrEditRoleModule = new AddOrEditRoleModule();
  });

  it('should create an instance', () => {
    expect(addOrEditRoleModule).toBeTruthy();
  });
});
