import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddOrEditRoleRoutingModule } from './add-or-edit-role-routing.module';
import { AddOrEditRoleComponent } from './add-or-edit-role.component';
import { FormsModule } from '@angular/forms';
import { FooterModule } from '../../../footer/footer.module';
import { HeaderModule } from '../../../header/header.module';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { MaterialModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FooterModule,
    HeaderModule,
    MaterialModule,
    MultiselectDropdownModule,
    AddOrEditRoleRoutingModule
  ],
  declarations: [AddOrEditRoleComponent]
})
export class AddOrEditRoleModule { }
