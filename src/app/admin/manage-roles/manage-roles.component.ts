import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Hero } from '../../common/model_1';
import { RoleModel } from '../../common/roleModel';
import { InjectService } from '../../injectable-service/injectable-service';
import { DataService } from '../../mts.service';

import { ROLE_MANAGEMENT_MODEL, SEARCH_MODEL, SEARCH_TEXT } from './role';

declare let swal: any;

@Component({
  selector: 'app-manage-roles',
  templateUrl: './manage-roles.component.html',
  styleUrls: ['./manage-roles.component.css']
})
export class ManageRolesComponent implements OnInit, OnDestroy {
  RoleDetailCurrentPage:number=1;
  RoleDetailPageSize:number=20;
  TotalRoles:number;
  permissionList: any;
  radioChecked: string;
  selectedCompany: any;
  permission: any;
  hero: Hero;
  roleModel: RoleModel;
  roleId: number;
  private p: any;
  private totalItem: number;
  private itemPerPage: any;
  roleManagementModel: any;
  roleList: any;
  roleDropdown: any;
  searchModal: any;
  searchText: any;
  sortType: string;
  public selectedRole: any;
  isLoader: boolean;
  userCredential: any;
  userDetail: any;

  constructor(
    private dataService: DataService, private injectService: InjectService,
    public router: Router) {
    this.selectedRole = '';
  }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      if (this.userCredential.UserPermission.ViewRole) {
        this.p = 1;
        this.sortType = 'asc';
        // this.hero = this.injectService.hero;
        this.itemPerPage = 1;
        this.totalItem = 0;
        this.roleManagementModel = ROLE_MANAGEMENT_MODEL;
        this.searchModal = SEARCH_MODEL;
        this.searchText = SEARCH_TEXT;
        this.searchModal.pageSize = 10;
        this.searchModal.pageNumber = 1;
        this.searchModal.searchText = '';
        this.searchModal.totalRecords = 0;
        this.isLoader = true;
        this.dataService
          .get(
            'master/RoleList?PageSize=' + this.searchModal.pageSize +
            '&PageNumber=1&OrderBy=RoleName&Order=asc&SearchText=&TotalRecords=0')
          .subscribe(r => {
            this.isLoader = false;
            this.roleList = r.RoleList;
            this.itemPerPage = 1;
            this.totalItem =
              Math.ceil(r.TotalRecords / this.searchModal.pageSize);
          });
        this.dataService
          .get(
            'master/RoleList?PageSize=10000&PageNumber=1&OrderBy=RoleName&Order=asc&SearchText=&TotalRecords=0')
          .subscribe(r => {
            this.roleDropdown = r.RoleList;
          })
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  resetSearchForm() {
    (<any>$('.md2-autocomplete-trigger svg')).trigger('click');
    this.selectedRole = '';
    (<any>$('.md2-autocomplete-input')).val('');
    (<any>$('.md2-autocomplete-trigger')).find('span').removeClass('has-value');
    // this.searchText.RoleName = '';
    //(<any> $('#role-auto input')).target.value = '';
    this.search();
  }

  search() {
    // this.searchModal.pageNumber=1;
    // this.searchModal.totalRecords=0;
    this.searchText.RoleName = this.selectedRole;

    this.searchModal.PageNumber = 1;
    // this.dataService.post('master/RoleList',this.searchModal).subscribe(
    this.dataService
      .get(
        'master/RoleList?PageSize=' + this.searchModal.pageSize +
        '&PageNumber=1&OrderBy=RoleName&Order=asc&SearchText=' +
        JSON.stringify(this.searchText) + '&TotalRecords=0')
      .subscribe(r => {
        this.p = 1;
        this.roleList = r.RoleList;
        this.totalItem =
          Math.ceil(r.TotalRecords / this.searchModal.pageSize);
        // this.totalItem=2;
      })
  }
  getPage(page: number) {
    var searchText = '';
    if (this.searchText.RoleName) {
      searchText = JSON.stringify(this.searchText);
    }
    this.dataService
      .get(
        'master/RoleList?PageSize=' + this.searchModal.pageSize +
        '&PageNumber=' + page + '&OrderBy=RoleName&Order=' + this.sortType +
        '&SearchText=' + searchText + '&TotalRecords=0')
      .subscribe(r => {
        this.p = page;
        this.itemPerPage = 1;
        this.roleList = r.RoleList;
      })
  }
  sorting(column: string) {
    if (this.sortType == 'asc') {
      this.sortType = 'desc';
      this.roleList.sort(function (a, b) {
        if (a.RoleName > b.RoleName) return -1;
        if (a.RoleName < b.RoleName) return 1;
        return 0;
      });
    } else {
      this.sortType = 'asc';
      this.roleList.sort(function (a, b) {
        if (a.RoleName < b.RoleName) return -1;
        if (a.RoleName > b.RoleName) return 1;
        return 0;
      });
    }
  }
  editRole(id: number) {
    this.roleModel = { RoleId: id, RoleName: '' };
    this.router.navigate([this.router.url + '/edit-role']);
  }
  deleteRole(id: number) {
    var userDetail = JSON.parse(localStorage.getItem('userDetail'));
    var thisObj = this;
    swal(
      {
        title: '',
        text:
          '<span style=\'color:#ef770e;font-size:20px;\'>Are you sure?</span><br><br>',
        html: true,
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Proceed',
        cancelButtonText: 'Cancel',
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function (isConfirm) {
        if (isConfirm) {
          // thid
          thisObj.dataService
            .get(
              'master/DeleteRole?RoleId=' + id +
              '&UserId=' + userDetail.UserId)
            .subscribe(r => {
              if (r.IsSuccess) {
                swal('', r.Message);
                thisObj.dataService
                  .get(
                    'master/RoleList?PageSize=' +
                    thisObj.searchModal.pageSize +
                    '&PageNumber=' + thisObj.p +
                    '&OrderBy=RoleName&Order=asc&SearchText=&TotalRecords=0')
                  .subscribe(r => {
                    // this.itemPerPage=1;
                    thisObj.totalItem = Math.ceil(
                      r.TotalRecords / thisObj.searchModal.pageSize);
                    thisObj.roleList = r.RoleList;
                  })
              } else {
                swal('', r.Message);
              }
            })
        } else {
        }
      });
  }

  ViewRoleDetail() {
    this.isLoader = true;
    this.RoleDetailCurrentPage=1;
    this.dataService.get('master/GetRoleAndModulePermissionList?PageSize='+ this.RoleDetailPageSize + '&PageNumber='+  this.RoleDetailCurrentPage)
    .subscribe(r => {   
      (<any>$('#RoleDetail')).modal('show');
      let res = JSON.parse(r);
      this.permission = JSON.parse(res).Data;
      this.TotalRoles =  JSON.parse(res).TotalRecords;
      this.isLoader = false;
    });
  
  }

  getPermissionPage(page: number){
    this.isLoader = true;
    this.RoleDetailCurrentPage=page;
    this.dataService.get('master/GetRoleAndModulePermissionList?PageSize='+ this.RoleDetailPageSize + '&PageNumber='+ this.RoleDetailCurrentPage)
    .subscribe(r => {   
      let res = JSON.parse(r);
      this.permission = JSON.parse(res).Data;
      this.TotalRoles =  JSON.parse(res).TotalRecords;
      this.isLoader = false;
    });
  }

  changeClass() {
    (<any>$('.open_in')).toggleClass('close_out');
    (<any>$('.filterSearchLabel')).toggle();
  }
  ngOnDestroy() {
    this.injectService.roleModel = this.roleModel;
  }
}
