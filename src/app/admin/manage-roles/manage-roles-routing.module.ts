import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ManageRolesComponent } from './manage-roles.component';

const routes: Routes = [{
    path: '',
    children: [
        { path: '', component: ManageRolesComponent }, {
            path: 'add-role',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './add-or-edit-role/add-or-edit-role.module')
                                        .AddOrEditRoleModule);
                            })
                })
        },
        {
            path: 'edit-role',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './add-or-edit-role/add-or-edit-role.module')
                                        .AddOrEditRoleModule);
                            })
                })
        },
        {
            path: 'view-role/:roleId',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                        .ensure(
                            [],
                            require => {
                                resolve(
                                    require(
                                        './add-or-edit-role/add-or-edit-role.module')
                                        .AddOrEditRoleModule);
                            })
                })
        }
    ]
}];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class ManageRolesRoutingModule {
}
