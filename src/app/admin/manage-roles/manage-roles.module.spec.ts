import {ManageRolesModule} from './manage-roles.module';

describe('ManageRoleModule', () => {
  let manageRolesModule: ManageRolesModule;

  beforeEach(() => {
    manageRolesModule = new ManageRolesModule();
  });

  it('should create an instance', () => {
    expect(manageRolesModule).toBeTruthy();
  });
});
