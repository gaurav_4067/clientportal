import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagePromoofferComponent } from 'app/admin/manage-promooffer/manage-promooffer.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: ManagePromoofferComponent
    }, {
      path: 'add-offer',
      loadChildren: () => new Promise(
        resolve => {
          (require as any)
            .ensure(
            [],
            require => {
              resolve(
                require(
                  './add-offer/add-offer.module')
                  .AddOfferModule);
            })
        })
    },
    {
      path: 'edit-offer/:OfferDetailId',
      loadChildren: () => new Promise(
        resolve => {
          (require as any)
            .ensure(
            [],
            require => {
              resolve(
                require(
                  './add-offer/add-offer.module')
                  .AddOfferModule);
            })
        })
    }
  ]
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagePromoofferRoutingModule { }
