export class OfferTypeList {
    public OfferTypeId: number;
    public OfferTypeName: string;
    public OfferTypeDescription: string;
    public OfferTypePrefix: string;
}

export class OfferSearchRequest {
    public OfferName: String = "";
    public OfferTypeId: Number = 0;
    public BusinessCategoryId: number = 0;
}

export class BussinessCategories {
    public Id: number;
    public Value: string;
    public SelectedCategory: boolean;
}

export class PromotionalMail {
    public OtherRecipient: string = '';
    public Subject: string = '';
    public Body: string = '';
    public ScheduleType: string;
    public Once: boolean = true;
    public IsEmailSend: boolean = false;
    public EmailType: number;
    public OfferId: number;
    public CreatedBy: number;
}

export class EcomPromoOfferList extends PromotionalMail {
    public BusinessCategoryName: string;
    public BusinessCategoryId: string;
    public OfferDetailId: number;
    public OfferTypeId: number;
    public OfferType: string;
    public OfferName: string;
    public MinTransactionAmount: number;
    public MaxTransactionAmount: number;
    public OfferPercentDiscount: number;
    public ValidFrom: Date
    public ValidTo: Date
    public OfferDescription: string;
    public OfferShortDescription: string;
    public OfferCode: string;
    public ClientId: number;
    public UserId: number;
    public ProductId: number;
    public Active: boolean;

}

export class PreviewMailTemplate {
    public Body: string;
    public Subject: string;
}
