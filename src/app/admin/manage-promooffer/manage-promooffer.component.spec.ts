import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePromoofferComponent } from './manage-promooffer.component';

describe('ManagePromoofferComponent', () => {
  let component: ManagePromoofferComponent;
  let fixture: ComponentFixture<ManagePromoofferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePromoofferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePromoofferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
