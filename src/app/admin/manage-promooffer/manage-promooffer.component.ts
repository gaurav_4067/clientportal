import { Component, OnInit } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import { Router } from "@angular/router";
import { InjectService } from "../../injectable-service/injectable-service";
import { DataService, BaseUrl } from "../../mts.service";
import {
  OfferTypeList,
  EcomPromoOfferList,
  BussinessCategories,
  OfferSearchRequest,
  PromotionalMail,
  PreviewMailTemplate
} from "./manage-promooffer.model";
import { FilterArrayPipe } from "app/pipe/filter-array.pipe";
import { FormatDatePipe } from "app/pipe/format-date.pipe";
import { SearhUserRequest, EcommerceUserList } from "app/admin/ecommerce-user/ecommerce-user.model";
declare let swal: any;

@Component({
  selector: "app-manage-promooffer",

  templateUrl: "./manage-promooffer.component.html",
  styleUrls: ["./manage-promooffer.component.css"],
  providers: [FilterArrayPipe, FormatDatePipe]
})
export class ManagePromoofferComponent implements OnInit {
  selectedEmailIndex: number;
  isLoader: boolean = false;
  sortType: string;
  userDetail: any;
  userCredential: any;
  offerSearchRequest: OfferSearchRequest;
  bussinessCategories: Array<BussinessCategories>;
  offerTypeList: Array<OfferTypeList>;
  ecomPromoOfferList: Array<EcomPromoOfferList>;
  promotionalMail: PromotionalMail;
  otherRecipientsEmailIds: string[] = [];
  isEmailValid: boolean = true;
  otherRecipientEmail: string = '';
  previewMailTemplate: PreviewMailTemplate;
  searhUserRequest: SearhUserRequest;
  ecommerceUserList: Array<EcommerceUserList>;

  constructor(
    private dataService: DataService,
    private injectService: InjectService,
    private http: Http,
    public router: Router

  ) {
    this.offerTypeList = new Array<OfferTypeList>();
    this.ecomPromoOfferList = new Array<EcomPromoOfferList>();
    this.bussinessCategories = new Array<BussinessCategories>();
    this.offerSearchRequest = new OfferSearchRequest();
    this.promotionalMail = new PromotionalMail();
    this.previewMailTemplate = new PreviewMailTemplate();
    this.searhUserRequest = new SearhUserRequest();
    this.ecommerceUserList = new Array<EcommerceUserList>();
  }

  ngOnInit() {
    this.promotionalMail.ScheduleType = "Hourly";
    this.isLoader = true;
    this.userCredential = JSON.parse(localStorage.getItem("mts_user_cred"));
    this.userDetail = JSON.parse(localStorage.getItem("userDetail"));
    this.dataService
      .get("ManageOffer/GetOfferTypesAndBusinessCategory")
      .subscribe(r => {
        this.offerTypeList = r.OfferTypes;
        this.bussinessCategories = r.BusinessCategories;
      });
    this.getPromoOffer();
    this.getUserList();
  }

  getUserList() {
    
    this.isLoader = true;
    this.dataService
      .post(
      'User/GetAllEcomUserList', this.searhUserRequest)
      .subscribe(r => {
        this.ecommerceUserList = r.EcommerceUsers
        this.isLoader = false;
      })
  }

  getPromoOffer() {
    this.isLoader = true;
    this.dataService
      .get("ManageOffer/GetAllPromoOffer")
      .subscribe(r => {
        this.ecomPromoOfferList = r.Data;
        this.isLoader = false;
      });
  }

  DeletePromoOffer(id) {
    let i = this.ecomPromoOfferList.findIndex(i => i.OfferDetailId == id);
    this.isLoader = true;
    let headers = new Headers({ "Content-Type": "application/json" });
    this.http
      .get(
      BaseUrl +
      "ManageOffer/DeletePromoOffer/" +
      this.ecomPromoOfferList[i].OfferDetailId,
      { headers }
      )
      .subscribe(r => {
        let response = r.json();
        if (response.IsSuccess) {
          swal("", response.Message);
          this.ecomPromoOfferList.splice(i, 1);
          this.isLoader = false;
        }
      });
  }

  EnableDisablePromoOffer(id, isActive) {
    let i = this.ecomPromoOfferList.findIndex(i => i.OfferDetailId == id);
    this.isLoader = true;
    this.dataService
      .get(
      "ManageOffer/EnableDisablePromoOffer/" +
      this.ecomPromoOfferList[i].OfferDetailId +
      "/" +
      isActive
      )
      .subscribe(r => {
        if (r.IsSuccess) {
          swal("", r.Message);
          if (this.ecomPromoOfferList[i].Active) {
            this.ecomPromoOfferList[i].Active = false;
            this.isLoader = false;
          } else {
            this.ecomPromoOfferList[i].Active = true;
            this.isLoader = false;
          }
        } else {
          swal("", r.Message);
          this.isLoader = false;
        }
      });
  }

  resetSearchForm() {
    this.isLoader = true;
    this.offerSearchRequest = new OfferSearchRequest();
    this.getPromoOffer();
  }

  SetValues() {
    // this.promotionalMail.Days = '';
    // this.promotionalMail.Hours = '';
    // this.promotionalMail.Week = '';
  }

  ResetMailValues() {
    this.promotionalMail = new PromotionalMail();
    this.otherRecipientsEmailIds = [];
    this.otherRecipientEmail = '';
    //this.promotionalMail.ScheduleType = 'Hourly';
  }

  viewAvailableOfferFor() {
    (<any>$("#OfferAvailableFor")).modal("show");
  }

  openPromotionalOfferMail(id, emailType) {
    
    //string.join(this.ecommerceUserList.Email)

    let ecomUserEmailString = Array.prototype.map.call(this.ecommerceUserList, s => s.Email).toString(); // "A,B,C"
    //this.promotionalMail.OtherRecipient = ecomUserEmailString;
    let index = this.ecomPromoOfferList.findIndex(i => i.OfferDetailId == id);
    this.selectedEmailIndex = index;
    this.promotionalMail = new PromotionalMail();
    this.promotionalMail.EmailType = emailType;
    this.isEmailValid = true;
    this.promotionalMail.CreatedBy = this.userCredential.UserId;
    if (index > -1) {


      let selectedOffer = this.ecomPromoOfferList[index];
      this.promotionalMail.OfferId = this.ecomPromoOfferList[
        index
      ].OfferDetailId;
      this.promotionalMail.Body = this.ecomPromoOfferList[index].Body ? this.ecomPromoOfferList[index].Body : '';
      this.promotionalMail.OtherRecipient = this.ecomPromoOfferList[
        index
      ].OtherRecipient ? this.ecomPromoOfferList[
        index
      ].OtherRecipient : '';
      if (this.promotionalMail.OtherRecipient) {
        this.otherRecipientsEmailIds = this.promotionalMail.OtherRecipient.split(',');
        this.promotionalMail.OtherRecipient = '';
      } else {
        this.otherRecipientsEmailIds = [];
      }
      this.promotionalMail.IsEmailSend = this.ecomPromoOfferList[
        index
      ].IsEmailSend;
      // this.promotionalMail.Subject = this.ecomPromoOfferList[index].Subject;

      let fromDate = this.buildDate(selectedOffer.ValidFrom);
      let toDate = this.buildDate(selectedOffer.ValidTo);


      // if (!this.promotionalMail.Subject) {
      this.promotionalMail.Subject =
        selectedOffer.OfferPercentDiscount +
        "% off " +
        selectedOffer.OfferName +
        " (" +
        fromDate +
        " To " +
        toDate +
        ")";
      if (this.otherRecipientsEmailIds.length > 0) {
        var array3 = this.arrayUnique(this.otherRecipientsEmailIds.concat(ecomUserEmailString.split(',')));
        this.otherRecipientsEmailIds = array3;
      }
      else {
        this.otherRecipientsEmailIds = ecomUserEmailString.split(',');
      }
      // }
    } else {
      this.promotionalMail.OfferId = 0;
      this.promotionalMail.IsEmailSend = true;
      this.otherRecipientsEmailIds = ecomUserEmailString.split(',');
    }
    let len = this.otherRecipientsEmailIds.length;
    (<any>$("#PrmotionalOfferMail")).modal("show");
  }

  arrayUnique(array) {
    var a = array.concat();
    for (var i = 0; i < a.length; ++i) {
      for (var j = i + 1; j < a.length; ++j) {
        if (a[i] === a[j])
          a.splice(j--, 1);
      }
    }

    return a;
  }

  buildDate(newDate) {
    let date = new Date(newDate)
    let offset = date.getTimezoneOffset();
    var timeOffsetInMS: number = offset * 60000;
    // if (offset > 0) {
    date.setTime(date.getTime() + timeOffsetInMS + (480 * 60000));
    // }
    // else {
    //   date.setTime(date.getTime() - timeOffsetInMS + (480 * 60000));
    // }
    return ((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear());
  }

  validateCheckbox(evt) {
    if (evt.target.checked) {
      this.promotionalMail.IsEmailSend = true;
    } else {
      this.promotionalMail.IsEmailSend = false;
    }
  }

  SendEmail() {
    if (this.otherRecipientEmail.trim() != '') {
      if (!this.addOtherRecipient()) {
        swal('', 'Please enter valid email');
        return;
      }
    }
    else if (this.promotionalMail.Subject.trim() == '') {
      swal("", "Please enter mail subject.");
      return;
    } else if (this.promotionalMail.Body.trim() == '') {
      swal("", "Please enter mail body.");
      return;
    }
    if (
      this.promotionalMail.Subject.trim() ||
      this.promotionalMail.Body.trim()
    ) {
      // this.promotionalMail.OtherRecipient = this.otherRecipientsEmailIds.reduce((previousValue,currentValue) => previousValue + ',' + currentValue);
      this.promotionalMail.OtherRecipient = this.otherRecipientsEmailIds.join(',');
      this.isLoader = true;
      this.dataService
        .post("ManageOffer/SendEmailTemplate", this.promotionalMail)
        .subscribe(r => {
          // this.ecomPromoOfferList = r.Data;
          this.isLoader = false;
          if (r.IsSuccess) {
            this.otherRecipientsEmailIds = [];
            // if(this.promotionalMail.IsEmailSend){
            //    this.ecomPromoOfferList[this.selectedEmailIndex].IsEmailSend=true;

            // }
            // else{
            //   this.ecomPromoOfferList[this.selectedEmailIndex].IsEmailSend=false;
            // }
            (<any>$("#PrmotionalOfferMail")).modal("hide");
            swal("", r.Message);
            this.getPromoOffer();
          }
        });
    }
  }


  GetEmailDataTemplate() {
    this.previewMailTemplate = new PreviewMailTemplate();
    if (this.otherRecipientEmail.trim() != '') {
      if (!this.addOtherRecipient()) {
        swal('', 'Please enter valid email');
        return;
      }
    }
    else if (this.promotionalMail.Subject.trim() == '') {
      swal("", "Please enter mail subject.");
      return;
    } else if (this.promotionalMail.Body.trim() == '') {
      swal("", "Please enter mail body.");
      return;
    }
    if (
      this.promotionalMail.Subject.trim() ||
      this.promotionalMail.Body.trim()
    ) {
      // this.promotionalMail.OtherRecipient = this.otherRecipientsEmailIds.reduce((previousValue,currentValue) => previousValue + ',' + currentValue);
      this.promotionalMail.OtherRecipient = this.otherRecipientsEmailIds.join(',');
      this.isLoader = true;
      this.dataService
        .post("ManageOffer/GetPreviewContent", this.promotionalMail)
        .subscribe(r => {
          // this.ecomPromoOfferList = r.Data;
          this.isLoader = false;
          if (r.IsSuccess) {
            let response = r.Data;
            this.previewMailTemplate.Body = response.Body;
            this.previewMailTemplate.Subject = response.Subject;
            (<any>$("#PreviewMail")).modal("show");
          }
        });
    }
  }

  closePreviewMail() {
    (<any>$("#PreviewMail")).modal("hide");
  }

  handleOtherRecipientinputfeild(evt) {
    var keycode = evt.charCode || evt.keyCode;
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    this.otherRecipientEmail = this.otherRecipientEmail.trim();
    if (keycode == 188) {
      this.otherRecipientEmail = this.otherRecipientEmail.substring(0, this.otherRecipientEmail.length - 1);
    }

    if (this.otherRecipientEmail) {
      if (reg.test(this.otherRecipientEmail) == true && (keycode == 32 || keycode == 13 || keycode == 188)) {
        this.otherRecipientsEmailIds.push(this.otherRecipientEmail);
        this.otherRecipientEmail = '';
        this.isEmailValid = true;
      }
    }
  }

  addOtherRecipient(): boolean {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (this.otherRecipientEmail) {
      if (reg.test(this.otherRecipientEmail) == true) {
        this.isEmailValid = true;
        this.otherRecipientsEmailIds.push(this.otherRecipientEmail)
        this.otherRecipientEmail = "";
        return true;
      } else {
        this.isEmailValid = false;
        return false;
      }
    }
  }

  removeEmail(emailId) {    
    let index = this.otherRecipientsEmailIds.findIndex(i => i == emailId);
    this.otherRecipientsEmailIds.splice(index, 1);
  }

}
