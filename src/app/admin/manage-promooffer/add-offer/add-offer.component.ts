import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, Params } from "@angular/router";
import { InjectService } from "../../../injectable-service/injectable-service";
import { DataService } from "../../../mts.service";
import {
  ClientList,
  UserList,
  ProductList,
  EcomPromoOfferRequest,
  BussinessCategories
} from "./add-offer.model";
import {
  OfferTypeList,
  ValidateMessage,
  ValidationObj
} from "./add-offer.model";
import { ActivatedRoute } from "@angular/router";
import { DatePicker } from "angular2-datetimepicker";
import { NgForm } from "@angular/forms";
declare let swal: any;

@Component({
  selector: "app-add-offer",
  templateUrl: "./add-offer.component.html",
  styleUrls: ["./add-offer.component.css"]
})
export class AddOfferComponent implements OnInit {
  model: any;
  isValid: boolean = false;
  isLoader: boolean = false;
  editOfferScreen: boolean = false;
  showOfferType: boolean = false;
  userDetail: any;
  userCredential: any;
  offerTypeList: Array<OfferTypeList>;
  addOfferModel: EcomPromoOfferRequest;
  EcomPromoOfferRequestList: Array<EcomPromoOfferRequest>;
  bussinessCategories: Array<BussinessCategories>;
  clientList: Array<ClientList>;
  userList: Array<UserList>;
  productList: Array<ProductList>;
  validationObj: ValidationObj;
  validateMessage: ValidateMessage;
  private clientText: any = { defaultTitle: "--Select client--" };
  private userText: any = { defaultTitle: "--Select user--" };
  private productText: any = { defaultTitle: "--Select product--" };
  dateFrom: Date = new Date();
  dateTo: Date = new Date();
  validDateSelection = {
    bigBanner: true,
    timePicker: false,
    format: "dd-MM-yyyy",
    defaultOpen: false
  };

  minValue: any;

  whiteSpace = /^[^\s]+(\s+[^\s]+)*$/;
  // numberPattern = /^[0-9]+$/;
  // numberPatternWithoutZero = /^[1-9]+$/;
  constructor(
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.offerTypeList = new Array<OfferTypeList>();
    this.clientList = new Array<ClientList>();
    this.userList = new Array<UserList>();
    this.productList = new Array<ProductList>();
    this.EcomPromoOfferRequestList = new Array<EcomPromoOfferRequest>();
    this.addOfferModel = new EcomPromoOfferRequest();
    this.bussinessCategories = new Array<BussinessCategories>();
    this.validateMessage = new ValidateMessage();
    this.validationObj = new ValidationObj();
    DatePicker.prototype.ngOnInit = function () {
      this.settings = Object.assign(this.defaultSettings, this.settings);
      if (this.settings.defaultOpen) {
        this.popover = true;
      }
      this.date = new Date();
    };
    
    // this.addOfferModel.ValidFrom = JSON.parse(
    //   JSON.stringify(
    //     new Date("dd-MM-yyyy")
    //   )
    // );
    // this.addOfferModel.ValidTo = JSON.parse(
    //   JSON.stringify(new Date("dd-MM-yyyy"))
    // );

    let validFrom = this.getFormattedDate(new Date());
    this.addOfferModel.ValidFrom = new Date(validFrom);
    this.minValue = this.addOfferModel.ValidFrom;
    let validTo = this.getFormattedDate(new Date());
    this.addOfferModel.ValidTo = new Date(validTo);
  }

  ngOnInit() {
    this.isLoader = true;
    this.userCredential = JSON.parse(localStorage.getItem("mts_user_cred"));
    this.userDetail = JSON.parse(localStorage.getItem("userDetail"));
    this.dataService
      .get("ManageOffer/GetOfferTypesAndBusinessCategory")
      .subscribe(r => {
        this.offerTypeList = r.OfferTypes;
        this.bussinessCategories = r.BusinessCategories;
        let that = this;
        this.activatedRoute.params.subscribe((params: Params) => {
          let OfferDetailId = params["OfferDetailId"];
          if (OfferDetailId) {
            //************for edit Offer*********
            that.dataService
              .get("ManageOffer/GetPromoOffer/" + OfferDetailId)
              .subscribe(r => {
                that.addOfferModel = new EcomPromoOfferRequest();
                that.addOfferModel = r;
                let validFrom = that.addOfferModel.ValidFrom;
                validFrom = new Date(validFrom);
                let validTo = that.addOfferModel.ValidTo;
                validTo = new Date(validTo);
                that.addOfferModel.ValidFrom = new Date(this.buildDate(validFrom));
                that.addOfferModel.ValidTo = new Date(this.buildDate(validTo));

                that.editOfferScreen = true;
                that.bussinessCategories.forEach(category => {
                  that.addOfferModel.BusinessCategories.forEach(
                    modelBusinessCategory => {
                      if (category.Id == modelBusinessCategory) {
                        category.SelectedCategory = true;
                      }
                    }
                  );
                });
                this.selectOfferType();
                that.addOfferModel.ModifiedBy = this.userCredential.UserId;
              });
          } else {
            //for Add new Offer
            this.bussinessCategories[0].SelectedCategory = true;
          }
        });
        that.isLoader = false;
      });
  }

  getFormattedDate(date: Date) {
    return ((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear());
  }

  buildDate(newDate) {
    let date = new Date(newDate)
    let offset = date.getTimezoneOffset();
    var timeOffsetInMS: number = offset * 60000;
    date.setTime(date.getTime() + timeOffsetInMS + (480 * 60000));
    return ((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear());
  }

  selectionCheck(evt, index) {
    if (evt.target.checked) {
      this.bussinessCategories[index].SelectedCategory = true;
    } else {
      this.bussinessCategories[index].SelectedCategory = false;
    }
    let selectedIndex = this.bussinessCategories.findIndex(
      i => i.SelectedCategory == true
    );
    if (selectedIndex == -1) {
      this.validationObj.BusinessCategory = false;
    } else {
      this.validationObj.BusinessCategory = true;
    }
  }

  checkValidation(required: any, format: any, value: any, objField: any) {
    if (required) {
      if (!value) {
        this.validationObj[objField] = false;
      } else {
        this.validationObj[objField] = true;
      }
    }
    if (objField == "MaxTransactionAmount" && value) {
      if (this.addOfferModel.MinTransactionAmount) {
        if (
          this.addOfferModel.MaxTransactionAmount >
          this.addOfferModel.MinTransactionAmount
        ) {
          this.validationObj[objField] = true;
        } else {
          this.validationObj[objField] = false;
          this.validateMessage.MaxTransactionAmountMsg =
            "Maximum amount should be greater than minimum amount";
        }
      }
    }
  }

  addUpdateOffer(addForm: NgForm) {
    let checkedCategory = this.bussinessCategories.filter(
      i => i.SelectedCategory == true
    );
    this.addOfferModel.BusinessCategories = new Array<number>();
    checkedCategory.forEach(element => {
      this.addOfferModel.BusinessCategories.push(element.Id);
    });
    if (
      this.addOfferModel.OfferPercentDiscount <= 0 ||
      this.addOfferModel.OfferTypeId <= 0 ||
      this.addOfferModel.ForNewUser <= 0 ||
      this.addOfferModel.ForOneTimeUse <= 0 ||
      this.addOfferModel.BusinessCategories.length <= 0
    ) {
      return false;
    } else {
      if (this.editOfferScreen) {
        this.EcomPromoOfferRequestList.push(this.addOfferModel);
        this.saveOffer();
      } else {
        this.addOfferModel.CreatedBy = this.userCredential.UserId;
        let index = this.offerTypeList.findIndex(
          j => j.OfferTypeId == this.addOfferModel.OfferTypeId
        );
        if (index > -1) {
          this.addOfferModel.OfferTypeName = this.offerTypeList[
            index
          ].OfferTypeName;
        }

        this.addOfferModel.OfferCode=this.addOfferModel.OfferCode.toUpperCase();
        // copying current addOfferModel into new EcomPromoOfferRequest.
        const addOfferModelCopy = Object.assign(
          new EcomPromoOfferRequest(),
          this.addOfferModel
        );
        this.EcomPromoOfferRequestList.push(addOfferModelCopy);
        addForm.resetForm(this.addOfferModel);
        // update UI after form reset
        this.changeDetectorRef.detectChanges();
        this.ResetOffer();
      }
    }
  }

  onDateSelect(evt, isFromDate) {

    let customDate = this.getFormattedDate(evt);
    let currentDate = this.getFormattedDate(new Date());

    if (new Date(customDate) < new Date(currentDate)) {
      if (isFromDate) {
        this.addOfferModel.ValidFrom = new Date(currentDate);
      }
      else {
        this.addOfferModel.ValidTo = new Date(currentDate);
      }
    }
    else {

      if (isFromDate) {
        this.addOfferModel.ValidFrom = new Date(customDate);
      }
      else {
        this.addOfferModel.ValidTo = new Date(customDate);
      }
    }
  }

  ResetOffer() {
    this.addOfferModel = new EcomPromoOfferRequest();
    this.validationObj = new ValidationObj();
    this.bussinessCategories.forEach(category => {
      if (category.Id == 1) {
        category.SelectedCategory = true;
      } else {
        category.SelectedCategory = false;
      }
    });
  }

  RemoveOffer(i) {
    this.EcomPromoOfferRequestList.splice(i, 1);
  }

  saveOffer() {
    this.dataService
      .post("ManageOffer/SavePromoOffer", this.EcomPromoOfferRequestList)
      .subscribe(r => {
        if (r.IsSuccess) {
          this.router.navigate(["/admin/manage-promooffer"]);
          swal("", r.Message);
          this.EcomPromoOfferRequestList = new Array<EcomPromoOfferRequest>();
        }
      });
  }

  selectOfferType() {
    if (
      this.addOfferModel.OfferTypeId == 1 ||
      this.addOfferModel.OfferTypeId == 5 ||
      this.addOfferModel.OfferTypeId == 6
    ) {
      this.showOfferType = true;
    } else {
      this.showOfferType = false;
    }
  }

  validateSelectUseDropdown() {
    if (this.addOfferModel.ForNewUser == 1) {
      this.addOfferModel.ForOneTimeUse = 1;
    }
  }

  resetModel() {
    if (this.addOfferModel.ForNewUser == 0) {
      this.addOfferModel.ForOneTimeUse = 0;
    }
  }

  compareDate(): boolean {
    if (
      new Date(this.addOfferModel.ValidTo) <
      new Date(this.addOfferModel.ValidFrom)
    ) {
      return false;
    } else {
      return true;
    }
  }

  validateValidFrom(): boolean {
    
    if (new Date(this.addOfferModel.ValidFrom) < new Date(this.getFormattedDate(new Date()))) {
      return false;
    } else {
      return true;
    }
  }

  restrictEKey(event) {
    if (event.keyCode === 69) {
      return false;
    }
  }
}
