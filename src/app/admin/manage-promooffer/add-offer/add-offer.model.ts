export class OfferTypeList {
  public OfferTypeId: number;
  public OfferTypeName: string;
  public OfferTypeDescription: string;
  public OfferTypePrefix: string;
}

export class ClientList {
  id: number;
  name: string;
}

export class UserList {
  id: number;
  name: string;
}

export class ProductList {
  id: number;
  name: string;
}

export class BussinessCategories {
  public Id: number;
  public Value: string;
  public SelectedCategory: boolean;
}

export class EcomPromoOfferRequest {
  constructor() {
    this.BusinessCategories = new Array<number>();
  }
  public OfferDetailId: number = 0;
  public OfferTypeId: number = 0;
  public OfferTypeName: string = "";
  public OfferDescription: string = "";
  public OfferShortDescription: string = "";
  public OfferName: string = "";
  public OfferTitle: string = "";
  public OfferCode: string = "";
  public ClientId: Array<number> = new Array<number>();
  public UserId: Array<number> = new Array<number>();
  public ProductId: Array<number> = new Array<number>();
  public OfferPercentDiscount: number;
  public MinTransactionAmount: number;
  public MaxTransactionAmount: number;
  public Active: Boolean = false;
  public CreatedBy: number = 0;
  public ValidFrom: Date = new Date();
  public ValidTo: Date = new Date();
  public CreatedOn: Date = new Date();
  public ModifiedBy: number = 0;
  public ModifiedOn: string = "";
  public BusinessCategories: Array<number> = new Array<number>();
  public BusinessCategoryId: number = 0;
  public ForNewUser: number = 0;
  public ForOneTimeUse: number = 0;
}

export class ValidateMessage {
  public OfferNameMsg: string = "Please Enter Offer Name";
  public OfferTitleMsg: string = "Please Enter Offer Title";
  public OfferCodeMsg: string = "Please Enter Offer Code ";
  public OfferPercentDiscountMsg: string = "Please Enter Offer Discount";
  public MinTransactionAmountMsg: string = "Please Enter Minimum amount";
  public MaxTransactionAmountMsg: string = "Please Enter Maximum amount";
  public OfferDescriptionMsg: string = "Please Enter Offer description";
  public OfferShortDescriptionMsg: string = "Please Enter short description";
  public BusinessCategoryMsg: string = "Please select any service";
  public OfferTypeMsg: string = "Please Select  any Offer Type";
  public ValidToMsg: string = " Valid To should be greater than Valid from";
}

export class ValidationObj {
  public OfferName: Boolean = true;
  public OfferCode: Boolean = true;
  public OfferPercentDiscount: Boolean = true;
  public MinTransactionAmount: Boolean = true;
  public MaxTransactionAmount: Boolean = true;
  public OfferShortDescription: Boolean = true;
  public OfferDescription: Boolean = true;
  public BusinessCategory: Boolean = true;
  public OfferType: Boolean = true;
  public ValidTo: Boolean = true;
}
