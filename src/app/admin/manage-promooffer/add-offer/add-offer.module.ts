import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddOfferRoutingModule } from './add-offer-routing.module';
import { AddOfferComponent } from './add-offer.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {FormsModule} from '@angular/forms';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

@NgModule({
  imports: [
    CommonModule,
    AddOfferRoutingModule,
    HeaderModule,
    FooterModule,
    MultiselectDropdownModule,
    FormsModule,
    AngularDateTimePickerModule
  ],
  declarations: [AddOfferComponent]
})
export class AddOfferModule { }
