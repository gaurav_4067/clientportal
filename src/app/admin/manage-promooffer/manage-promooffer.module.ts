import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { ManagePromoofferRoutingModule } from './manage-promooffer-routing.module';
import { ManagePromoofferComponent } from './manage-promooffer.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { FilterArrayPipe } from 'app/pipe/filter-array.pipe';
import { FormatDatePipe } from 'app/pipe/format-date.pipe';
 
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ManagePromoofferRoutingModule,
    HeaderModule, 
    FooterModule,
    Ng2AutoCompleteModule
  ],
  declarations: [ManagePromoofferComponent,
  FilterArrayPipe, FormatDatePipe
]
})
export class ManagePromoofferModule { }
