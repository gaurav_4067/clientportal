export class ComapnyMapping {
  CompanyId: number;
  ManufactureId: number[];
  VendorId: number[];
  AgentId: number[];
  CreatedBy: number;
  BusinessCategoryId: number;
}




export class CompanyMappingMatrix
{
  public MatrixId:number;
  public BusinessCategoryId:number;
  public CategoryName:string;
  public ClientCompanyId:number;
  public ClientCompanyName:string;
  public AgentId:number;
  public AgentName:string;
  public ManufacturerId:number;
  public ManufacturerName:string;
  public VendorId:number;
  public VendorName:string;
  public Active:boolean;
  public CreatedBy:number;
  public CreatedDate:Date;
  public ModifiedBy:number;
  public ModifiedDate:Date;
}


export class MatrixMappingModel
{
  public MatrixId:number=0;
  public BusinessCategoryId:number =0;
  public CategoryName:string;
  public ClientCompanyId:number=0;
  public ClientCompanyName:string;
  public AgentId:number=0;
  public AgentName:string;
  public ManufacturerId:number=0;
  public ManufacturerName:string;
  public VendorId:number=0;
  public VendorName:string;
  public Active:boolean;
  public CreatedBy:number;
  public CreatedDate:Date;
  public ModifiedBy:number;
  public ModifiedDate:Date; 
}

export class CompnayListArray 
{
  mappingCompanyClientList: any = [];
  mappingAgentList: any = [];
  mappingVendorList: any = [];
  mappingManufacturerList: any = [];
}
