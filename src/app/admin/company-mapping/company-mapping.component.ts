import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../mts.service';
import { ComapnyMapping, CompanyMappingMatrix, MatrixMappingModel, CompnayListArray } from './comapny-mapping';
import { LabAddressInfo } from 'app/e-commerce/services/services-group/service/package/order-review-copy/order-review-copy';
import { MappedCompanyList } from 'app/admin/client-configuration-list/client-configuration-list.model';

declare let swal: any;

@Component({
  selector: 'app-company-mapping',
  templateUrl: './company-mapping.component.html',
  styleUrls: ['./company-mapping.component.css'],
  providers: [ComapnyMapping]
})
export class CompanyMappingComponent implements OnInit {
  AllMatrixMappingArray: Array<CompanyMappingMatrix>;
  MatrixMappingArray: Array<CompanyMappingMatrix>;
  addMatrixMappingModel: MatrixMappingModel;
  CompnayListArray: CompnayListArray;
  EnableMatrixMapping: boolean = false;
  mappedClient: any;
  mappedAgent: any;
  mappedVendors: any;
  mappedManufacturer: any;
  SelectedBusinessId:number=0;
  clientVendorList: Array<MappedCompanyList>;
  clientAgentList: Array<MappedCompanyList>;
  clientManufacturerList: Array<MappedCompanyList>;


  inputCompany: any;
  companyList: any = [];
  isLoader: boolean;
  userCredential: any;
  selectedCategory: any = '0';
  private vendors: any = [];
  private manufacturers: any = [];
  private agents: any = [];
  private vendorText: any = { defaultTitle: '--Select Vendor--' };
  private manufacturerText: any = { defaultTitle: '--Select Manufacturer--' };
  private agentText: any = { defaultTitle: '--Select Agent--' };
  userDetail: any;

  constructor(
    private dataService: DataService, private mappingModal: ComapnyMapping,
    private router: Router) {
    this.MatrixMappingArray = new Array<CompanyMappingMatrix>();
    this.addMatrixMappingModel = new MatrixMappingModel();
    this.CompnayListArray = new CompnayListArray();
    this.AllMatrixMappingArray = new Array<CompanyMappingMatrix>();
  }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userDetail && this.userCredential) {
      if (this.userCredential.UserPermission.EditCompanyMapping) {
        this.mappingModal.CompanyId = 0;
        this.inputCompany = '';
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  saveCompanyMapping() {
    if (!this.inputCompany.CompanyId || this.selectedCategory == '0') {
      swal('', 'Please Select Category/Company.');
    } else {
      this.mappingModal.CompanyId = this.inputCompany.CompanyId;
      this.mappingModal.CreatedBy = this.userCredential.UserId;
      this.isLoader = true;
      this.dataService.post('Master/SaveCompanyMapping', this.mappingModal)
        .subscribe(r => {
          this.isLoader = false;
          swal('', r.Message);
          this.removeMapping();
        });
    }
  }

  public renderCompany(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CompanyName}</div>
      </div>`;

    return html;
  }

  removeMapping() {
    this.mappingModal.AgentId = [];
    this.mappingModal.ManufactureId = [];
    this.mappingModal.VendorId = [];
    this.mappingModal.CompanyId = 0;
    this.inputCompany = '';
    this.selectedCategory = '0';
    this.clientAgentList = [];
    this.clientVendorList = [];
    this.clientManufacturerList = [];
  }

  getCompanyVendors(Comapany) {
    if (Comapany.CompanyId) {
      var companyId = Comapany.CompanyId;
      this.isLoader = true;
      this.dataService
        .get(
        'Master/GetCompanyMapping?CompanyId=' + companyId +
        '&BusinessCategoryId=' + this.selectedCategory)
        .subscribe(r => {
          this.mappingModal.VendorId = r.VendorId;
          this.mappingModal.ManufactureId = r.ManufactureId;
          this.mappingModal.AgentId = r.AgentId;
          // this.isLoader = false;
        });
      this.CompanyMappingInfo(companyId, this.selectedCategory);
    }
  }

  categoryChange() {
    this.companyList = [];
    this.isLoader = true;
    this.inputCompany = '';
    this.mappingModal.CompanyId = 0;
    this.mappingModal.BusinessCategoryId = this.selectedCategory;
    this.dataService
      .get(
      'master/GetCompanyList?BusinessCategoryId=' + this.selectedCategory)
      .subscribe(r => {
        this.isLoader = false;
        var list = [];
        r.CompanyList.forEach(function (company) {
          list.push({ id: company.CompanyId, name: company.CompanyName })
        });
        this.vendors = list;
        this.manufacturers = list;
        this.agents = list;
        this.companyList = r.CompanyList;
      });
  }

  BusinesscategoryChange() {
    this.isLoader = true;
    this.CompnayListArray = new CompnayListArray();
    this.mappedClient = '';
    this.mappedAgent = '';
    this.mappedVendors = '';
    this.mappedManufacturer = '';
    this.dataService
      .get(
      'master/GetCompanyList?BusinessCategoryId=' + this.SelectedBusinessId)
      .subscribe(r => {
        this.CompnayListArray.mappingCompanyClientList = r.CompanyList;
        this.isLoader = false;
        this.CompnayListArray.mappingAgentList = r.CompanyList;
        this.CompnayListArray.mappingVendorList = r.CompanyList;
        this.CompnayListArray.mappingManufacturerList = r.CompanyList;
      });
  }

  addCompanymapping() {
    this.addMatrixMappingModel = new MatrixMappingModel();
    this.addMatrixMappingModel.BusinessCategoryId=this.SelectedBusinessId;
    if (this.addMatrixMappingModel.BusinessCategoryId == 1) {
      this.addMatrixMappingModel.CategoryName = "Testing";
    } else if (this.addMatrixMappingModel.BusinessCategoryId == 2) {
      this.addMatrixMappingModel.CategoryName = "Inspection";
    }
    this.addMatrixMappingModel.CreatedBy = this.userCredential.UserId;
    this.addMatrixMappingModel.ModifiedBy = this.userCredential.UserId;

    if (this.mappedClient) {
      this.addMatrixMappingModel.ClientCompanyId = this.mappedClient.CompanyId;
      this.addMatrixMappingModel.ClientCompanyName = this.mappedClient.CompanyName;
    }
    if (this.mappedAgent) {
      this.addMatrixMappingModel.AgentId = this.mappedAgent.CompanyId;
      this.addMatrixMappingModel.AgentName = this.mappedAgent.CompanyName;
    }
    if (this.mappedVendors) {
      this.addMatrixMappingModel.VendorId = this.mappedVendors.CompanyId;
      this.addMatrixMappingModel.VendorName = this.mappedVendors.CompanyName;
    }
    if (this.mappedManufacturer) {
      this.addMatrixMappingModel.ManufacturerId = this.mappedManufacturer.CompanyId;
      this.addMatrixMappingModel.ManufacturerName = this.mappedManufacturer.CompanyName;
    }
    if (this.addMatrixMappingModel.BusinessCategoryId > 0 && this.addMatrixMappingModel.ClientCompanyId > 0 &&
      (this.addMatrixMappingModel.AgentId > 0 || this.addMatrixMappingModel.VendorId > 0 || this.addMatrixMappingModel.ManufacturerId > 0)) {
      let index = this.AllMatrixMappingArray.findIndex(i => i.ClientCompanyId == this.addMatrixMappingModel.ClientCompanyId && i.AgentId == this.addMatrixMappingModel.AgentId
        && i.BusinessCategoryId == this.addMatrixMappingModel.BusinessCategoryId && i.VendorId == this.addMatrixMappingModel.VendorId && i.ManufacturerId == this.addMatrixMappingModel.ManufacturerId)
      if (index > -1) {
        swal("", "Company mapping has already exist")
      } else {
        this.SaveCompanyMappingMatrix(this.addMatrixMappingModel);
      }
    }
  }

  ResetCompanyMatrix() {
    this.SelectedBusinessId=0;
    this.mappedClient = '';
    this.mappedAgent = '';
    this.mappedVendors = '';
    this.mappedManufacturer = '';
    this.addMatrixMappingModel = new MatrixMappingModel();
  }

  SaveCompanyMappingMatrix(model) {
    this.dataService.post('Master/SaveCompanyMappingMatrix', model)
      .subscribe(r => {
        this.getMatrixMapping();
        this.isLoader = false;
        swal('', r.Message);             
      });
  }

  RemoveMappingMatrix(MatrixId, index) {
    let that = this;
    swal({
      title: "",
      text: "<span style='color:#ef770e;font-size:20px;'>Are you sure to delete this mapping ?</span><br><br>",
      html: true,
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: true,
    },
      function (isConfirm) {
        if (isConfirm) {
          console.log("delete",that)         
          that.isLoader = true;
          that.dataService.get('Master/DeleteMappingMatrix?MatrixId=' + MatrixId)
          .subscribe(r => {
            if(r.IsSuccess){
              that.AllMatrixMappingArray.splice(index, 1);
            }
              that.isLoader = false;
              swal('', r.Message);
            });
        } else {
        }
      });
  }

  onCompnayListChange(Type) {
    if (Type == 'Client') {
      this.mappedAgent = '';
      this.mappedVendors = '';
      this.mappedManufacturer = '';
    } else if (Type == 'Agent') {
      this.mappedVendors = '';
      this.mappedManufacturer = '';
    } else if (Type == 'Vendor') {
      this.mappedManufacturer = '';
    }
  }

  CompanyMappingInfo(CompnayId, CategoryId) {
    this.isLoader = true;
    this.dataService
      .get(
      'ClientConfiguration/GetCompanyMapping?CompanyId=' + CompnayId +
      '&BusinessCategoryId=' + CategoryId)
      .subscribe(r => {
        this.clientAgentList = r.AgentList;
        this.clientVendorList = r.VendorList;
        this.clientManufacturerList = r.ManufactureList;
        this.isLoader = false;
      });
  }

  showHideButton(value) {
    this.EnableMatrixMapping = false;
    this.ResetCompanyMatrix();
  }

  getMatrixMapping() {
    this.EnableMatrixMapping = true;
    // this.AllMatrixMappingArray = new Array<CompanyMappingMatrix>();
    this.isLoader = true;
    this.dataService
      .get(
      'Master/GetCompanyMappingMatrix')
      .subscribe(r => {
        this.AllMatrixMappingArray = r.Data;
        this.isLoader = false;
      });
  }

}