import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompanyMappingComponent } from './company-mapping.component';

const routes: Routes =
    [{ path: '', component: CompanyMappingComponent }];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class CompanyMappingRoutingModule {
}
