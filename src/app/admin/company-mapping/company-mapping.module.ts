import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {Ng2AutoCompleteModule} from 'ng2-auto-complete';

import {FooterModule} from '../../footer/footer.module';
import {HeaderModule} from '../../header/header.module';

import {CompanyMappingRoutingModule} from './company-mapping-routing.module';
import {CompanyMappingComponent} from './company-mapping.component';
import { TabsModule } from 'ngx-tabs';

@NgModule({
  imports: [
    CommonModule, FormsModule, FooterModule, HeaderModule,
    Ng2AutoCompleteModule, MultiselectDropdownModule,
    CompanyMappingRoutingModule,
    TabsModule
  ],
  declarations: [CompanyMappingComponent]
})
export class CompanyMappingModule {
}
