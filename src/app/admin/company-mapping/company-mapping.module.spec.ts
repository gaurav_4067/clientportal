import {CompanyMappingModule} from './company-mapping.module';

describe('CompanyMappingModule', () => {
  let companyMappingModule: CompanyMappingModule;

  beforeEach(() => {
    companyMappingModule = new CompanyMappingModule();
  });

  it('should create an instance', () => {
    expect(companyMappingModule).toBeTruthy();
  });
});
