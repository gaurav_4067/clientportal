import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EtrfListingRoutingModule } from './etrf-listing-routing.module';
import { EtrfListingComponent } from './etrf-listing.component';
import { HeaderModule } from 'app/header/header.module';
import { FooterModule } from 'app/footer/footer.module';
import { FormsModule } from '@angular/forms';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { MdInputModule, MaterialModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    EtrfListingRoutingModule,
    FooterModule, HeaderModule,FormsModule,Ng2AutoCompleteModule,MdInputModule,
    MaterialModule
  ],
  declarations: [EtrfListingComponent]
})
export class EtrfListingModule { }
