import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EtrfListingComponent } from './etrf-listing.component';

const routes: Routes = [
  { path: '', component: EtrfListingComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EtrfListingRoutingModule { }
