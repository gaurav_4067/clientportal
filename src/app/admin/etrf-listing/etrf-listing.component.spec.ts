import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtrfListingComponent } from './etrf-listing.component';

describe('EtrfListingComponent', () => {
  let component: EtrfListingComponent;
  let fixture: ComponentFixture<EtrfListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtrfListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtrfListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
