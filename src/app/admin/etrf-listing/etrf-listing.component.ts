import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/mts.service';
import { Router } from '@angular/router';
import { InjectService } from 'app/injectable-service/injectable-service';
declare let swal: any;

@Component({
  selector: 'app-etrf-listing',
  templateUrl: './etrf-listing.component.html',
  styleUrls: ['./etrf-listing.component.css']
})
 
export class EtrfListingComponent implements OnInit {
  private p: any;
  private totalItem: any;
  private itemPerPage: any;
  private columnList: any;
  private availableSearchColumns: any;
  private availableAdhocColumns: any;
  private selectedAdhocColumns: any;
  private selectedSearchColumns: any;
  private availableGridColumns: any;
  private selectedGridColumns: any;

  public selectedCompany: any;
  public tempSelectedSearchA: any;
  public tempSelectedSearchC: any;
  public tempselectedAvailC: any;
  public tempSelectedGridC: any;
  public tempselectedAvailG: any;
  public tempselectedAvailA: any;
  public defaultSelectedCompany: any;
  isLoader: boolean;
  userCredential: any;
  currentDiv: any = 0;
  AliasInput: any;
  companyList: any;
  showsetting: boolean;
  MyOrder: any;
  bussinessCategories: any = [];
  selectedBussinessCategory: any;
  inputCompany: any;
  dateFormatData: Array<any>;
  dateFormatId: number;
  inputCompanyNew: any;
  selectedBussinessCategoryNew: any;
  companyListNew: any;
  userDetail: any;
  constructor(
    private dataService: DataService, private injectService: InjectService,
    public router: Router) {
    this.dateFormatData = [
      { id: 1, value: 'dd-MM-yyyy' }, { id: 2, value: 'MM-dd-yyyy' },
      { id: 3, value: 'yyyy-MM-dd' }, { id: 4, value: 'dd-MMM-yyyy' },
      { id: 5, value: 'MMM-dd-yyyy' }, { id: 6, value: 'MMMM dd, yyyy' },
      { id: 7, value: 'MMMM-dd-yyyy' }
    ];
    this.dateFormatId = 0;
    this.p = 1;
    this.itemPerPage = 10;
    this.showsetting = false;
    this.MyOrder = 0;
    this.selectedCompany = 0;
  }
  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
     // if (this.userCredential.UserPermission.ViewOrderTrackingConfiguration) {
        this.defaultSelectedCompany = {
          'CompanyId': 0,
          'CompanyName': '--Select Company--'
        };
        this.isLoader = true;
       
        this.dataService.get('DynamicFormCreation/BussinessType').subscribe(r => {
          if (r.IsSuccess) {
            this.bussinessCategories = r.Data;
            this.selectedBussinessCategory = this.userCredential.BusinessCategoryId;
            this.getCompanyList();
          }
        });
      // } else {
      //   this.router.navigate(['/landing-page']);
      // }
    } else {
      this.router.navigate(['/login']);
    }
  }

  select(value) {
    this.selectedCompany = value;
    // this.search();
  }

  public renderCompany(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CompanyName}</div>
      </div>`;

    return html;
  }

  getCompanyList() {
    this.inputCompany = '';
    this.isLoader = true;
    if (this.selectedBussinessCategory > 0) {
      this.dataService
        .get(
          'master/GetCompanyList?BusinessCategoryId=' +
          this.selectedBussinessCategory)
        .subscribe(r => {
          this.isLoader = false;
          this.companyList = r.CompanyList;
          // this.companyList.splice(0, 0, this.defaultSelectedCompany);
          this.selectedCompany = 0;
        });
    }
  }

  search() {
    if (!this.inputCompany.CompanyId || this.selectedBussinessCategory == 0) {
      swal('', 'Please Select All Fields!');
    } else {
      this.selectedCompany = this.inputCompany.CompanyId;
      this.isLoader = true;
      this.dataService
        .get(
          'Order/GetTRFListColumnsList?selectedCompany=' +
          this.selectedCompany + 
          '&businessCategoryId=' + this.selectedBussinessCategory)
        .subscribe(r => {
          this.isLoader = false;
          this.columnList = r;
          if (this.columnList != null) {
            this.availableGridColumns = this.columnList.AvailableGridColumn;
            this.availableSearchColumns =
              this.columnList.AvailableSearchColumn;
            this.availableAdhocColumns =
              this.columnList.AvailableAdHocSearchColumn;
            this.selectedAdhocColumns =
              this.columnList.SelectedAdHocSearchColumn;
            this.selectedGridColumns = this.columnList.SelectedGridColumn;
            this.selectedSearchColumns = this.columnList.SelectedSearchColumn;
            this.showsetting = true;
          }
        });
    }
  }
  showModel() {
    (<any>$('#myModalOne')).modal('show');
    (<any>$('#md-input-3')).focus();
  }
  hideModel() {
    (<any>$('#AliasInput')).val('');
    (<any>$('#myModalOne')).modal('hide');
  }
  add(option) {
    if (this.tempSelectedGridC != '' && this.tempSelectedGridC != undefined) {
      (<any>$('#myModalOne')).modal('show');
      this.currentDiv = option;
      if (this.tempSelectedGridC) {
        let svalue = this.tempSelectedGridC;
        for (var j = 0; j < this.availableGridColumns.length; j++) {
          if (this.availableGridColumns[j].ColumnId === svalue) {
            this.AliasInput = this.availableGridColumns[j].ColumnName;
            break;
          }
        }
      }


      (<any>$('#md-input-3')).focus();
    } else if (
      this.tempSelectedSearchA != '' &&
      this.tempSelectedSearchA != undefined) {
      (<any>$('#myModalOne')).modal('show');
      this.currentDiv = option;
      if (this.tempSelectedSearchA) {
        let svalue = this.tempSelectedSearchA;
        for (var j = 0; j < this.availableAdhocColumns.length; j++) {
          if (this.availableAdhocColumns[j].ColumnId === svalue) {
            this.AliasInput = this.availableAdhocColumns[j].ColumnName;
          }
        }
      }
      (<any>$('#md-input-3')).focus();
    } else if (this.tempSelectedSearchC != '' && this.tempSelectedSearchC != undefined) {
      (<any>$('#myModalOne')).modal('show');
      this.currentDiv = option;
      if (this.tempSelectedSearchC) {
        let svalue = this.tempSelectedSearchC;
        for (var j = 0; j < this.availableSearchColumns.length; j++) {
          if (this.availableSearchColumns[j].ColumnId === svalue) {
            this.AliasInput = this.availableSearchColumns[j].ColumnName;
            break;
          }
        }
      }
      (<any>$('#md-input-3')).focus();
    } else {
      swal('', 'Please select one value.');
    }
  }
  AddColumn() {
    // (<any>$('#myModalOne')).modal('show');
    if (this.AliasInput == '') {
      swal('', 'Please add Caption name.');
      return 0;
    }
    if (this.currentDiv == 1) {
      // for (var i = 0; i < this.tempSelectedSearchC.length; i++)
      if (this.tempSelectedSearchC) {
        let svalue = this.tempSelectedSearchC;
        for (var j = 0; j < this.availableSearchColumns.length; j++) {
          if (this.availableSearchColumns[j].ColumnId === svalue) {
            this.availableSearchColumns[j].ColumnDataValue = this.AliasInput;
            // this.availableSearchColumns[j].ColumnName =
            // this.availableSearchColumns[j].ColumnName + " As " +
            // this.AliasInput;
            this.selectedSearchColumns.push(this.availableSearchColumns[j]);
            this.selectedSearchColumns[this.selectedSearchColumns.length - 1]
              .SequenceNo = this.selectedSearchColumns.length;
            this.availableSearchColumns.splice(j, 1);
            break;
          }
        }
      }
      this.tempSelectedSearchC = [];

    } else if (this.currentDiv == 2) {
      if (this.tempSelectedGridC) {
        let svalue = this.tempSelectedGridC;
        for (var j = 0; j < this.availableGridColumns.length; j++) {
          if (this.availableGridColumns[j].ColumnId === svalue) {
            this.availableGridColumns[j].ColumnDataValue = this.AliasInput;
            // this.availableGridColumns[j].ColumnName =
            // this.availableGridColumns[j].ColumnName + " As " +
            // this.AliasInput;
            this.selectedGridColumns.push(this.availableGridColumns[j]);
            this.selectedGridColumns[this.selectedGridColumns.length - 1]
              .SequenceNo = this.selectedGridColumns.length;
            this.availableGridColumns.splice(j, 1);
            break;
          }
        }
      }
      this.tempSelectedGridC = [];
    } else if (this.currentDiv == 3) {
      if (this.tempSelectedSearchA) {
        let svalue = this.tempSelectedSearchA;
        for (var j = 0; j < this.availableAdhocColumns.length; j++) {
          if (this.availableAdhocColumns[j].ColumnId === svalue) {
            this.availableAdhocColumns[j].ColumnDataValue = this.AliasInput;
            // this.availableAdhocColumns[j].ColumnName =
            // this.availableAdhocColumns[j].ColumnName + " As " +
            // this.AliasInput;
            this.selectedAdhocColumns.push(this.availableAdhocColumns[j]);
            this.selectedAdhocColumns[this.selectedAdhocColumns.length - 1]
              .SequenceNo = this.selectedAdhocColumns.length;
            this.availableAdhocColumns.splice(j, 1);
            break;
          }
        }
      }
      this.tempSelectedSearchA = [];
    }
    (<any>$('#myModalOne')).modal('hide');
    this.AliasInput = '';
  }
  remove(option) {
    if ((this.tempselectedAvailC && this.tempselectedAvailC.length > 0) || (this.tempselectedAvailG && this.tempselectedAvailG.length > 0) || (this.tempselectedAvailA && this.tempselectedAvailA.length > 0)) {
      if (option == 1) {
        for (var i = 0; i < this.tempselectedAvailC.length; i++) {
          let svalue = this.tempselectedAvailC[i];
          for (var j = 0; j < this.selectedSearchColumns.length; j++) {
            if (this.selectedSearchColumns[j].ColumnId === svalue) {
              this.selectedSearchColumns[j].ColumnName =
                this.selectedSearchColumns[j].ColumnName.split(' As ')[0];
              this.availableSearchColumns.push(this.selectedSearchColumns[j]);
              this.selectedSearchColumns.splice(j, 1);
              break;
            }
          }
        }
        this.tempselectedAvailC = [];
      } else if (option == 2) {
        for (var i = 0; i < this.tempselectedAvailG.length; i++) {
          let svalue = this.tempselectedAvailG[i];
          for (var j = 0; j < this.selectedGridColumns.length; j++) {
            if (this.selectedGridColumns[j].ColumnId === svalue) {
              this.selectedGridColumns[j].ColumnName =
                this.selectedGridColumns[j].ColumnName.split(' As ')[0];
              this.availableGridColumns.push(this.selectedGridColumns[j]);
              this.selectedGridColumns.splice(j, 1);
              break;
            }
          }
        }
        this.tempselectedAvailG = [];
      } else if (option == 3) {
        for (var i = 0; i < this.tempselectedAvailA.length; i++) {
          let svalue = this.tempselectedAvailA[i];
          for (var j = 0; j < this.selectedAdhocColumns.length; j++) {
            if (this.selectedAdhocColumns[j].ColumnId === svalue) {
              this.selectedAdhocColumns[j].ColumnName =
                this.selectedAdhocColumns[j].ColumnName.split(' As ')[0];
              this.availableAdhocColumns.push(this.selectedAdhocColumns[j]);
              this.selectedAdhocColumns.splice(j, 1);
              break;
            }
          }
        }
        this.tempselectedAvailA = [];
      }
    } else {
      swal('', 'Please select one value.');
    }
  }
  up(option: any) {
    if (option == 1) {
      // this.tempselectedAvailC;
      if (this.tempselectedAvailC && this.tempselectedAvailC.length == 1) {
        let svalue = this.tempselectedAvailC;
        for (var j = 0; j < this.selectedSearchColumns.length; j++) {
          if (svalue == this.selectedSearchColumns[j].ColumnId) {
            if (j != 0) {
              var befor = this.selectedSearchColumns[j - 1];
              var valueCur = this.selectedSearchColumns[j];
              this.selectedSearchColumns[j] = befor;
              this.selectedSearchColumns[j - 1] = valueCur;
              this.selectedSearchColumns[j].SequenceNo = j + 1;
              this.selectedSearchColumns[j - 1].SequenceNo = j;
            }
          }
        }
      } else {
        swal('', 'Please select one value.')
      }
    } else if (option == 2) {
      // this.tempselectedAvailG;
      if (this.tempselectedAvailG.length && this.tempselectedAvailG.length == 1) {
        let svalue = this.tempselectedAvailG;
        for (var j = 0; j < this.selectedGridColumns.length; j++) {
          if (svalue == this.selectedGridColumns[j].ColumnId) {
            if (j != 0) {
              var befor = this.selectedGridColumns[j - 1];
              var valueCur = this.selectedGridColumns[j];
              this.selectedGridColumns[j] = befor;
              this.selectedGridColumns[j - 1] = valueCur;
              this.selectedGridColumns[j].SequenceNo = j + 1;
              this.selectedGridColumns[j - 1].SequenceNo = j;
            }
          }
        }
      } else {
        swal('', 'Please select one value.')
      }
    } else if (option == 3) {
      // this.tempselectedAvailA;
      if (this.tempselectedAvailA && this.tempselectedAvailA.length == 1) {
        let svalue = this.tempselectedAvailA;
        for (var j = 0; j < this.selectedAdhocColumns.length; j++) {
          if (svalue == this.selectedAdhocColumns[j].ColumnId) {
            if (j != 0) {
              var befor = this.selectedAdhocColumns[j - 1];
              var valueCur = this.selectedAdhocColumns[j];
              this.selectedAdhocColumns[j] = befor;
              this.selectedAdhocColumns[j - 1] = valueCur;
              this.selectedAdhocColumns[j].SequenceNo = j + 1;
              this.selectedAdhocColumns[j - 1].SequenceNo = j;
            }
          }
        }
      } else {
        swal('', 'Please select one value.')
      }
    }
  }
  down(option: any) {
    var count = 0;
    if (option == 1) {
      // this.tempselectedAvailC;
      if (this.tempselectedAvailC && this.tempselectedAvailC.length == 1) {
        let svalue = this.tempselectedAvailC;
        for (var j = 0; j < this.selectedSearchColumns.length; j++) {
          if (svalue == this.selectedSearchColumns[j].ColumnId && count == 0) {
            if (j + 1 != this.selectedSearchColumns.length) {
              var After = this.selectedSearchColumns[j + 1];
              var valueCur = this.selectedSearchColumns[j];
              this.selectedSearchColumns[j] = After;
              this.selectedSearchColumns[j + 1] = valueCur;
              this.selectedSearchColumns[j].SequenceNo = j + 1;
              this.selectedSearchColumns[j + 1].SequenceNo = j + 2
              count++;
            }
          }
        }
      } else {
        swal('', 'Please select one value.')
      }
    } else if (option == 2) {
      // this.tempselectedAvailG;
      if (this.tempselectedAvailG && this.tempselectedAvailG.length == 1) {
        let svalue = this.tempselectedAvailG;
        for (var j = 0; j < this.selectedGridColumns.length; j++) {
          if (svalue == this.selectedGridColumns[j].ColumnId && count == 0) {
            if (j + 1 != this.selectedGridColumns.length) {
              var After = this.selectedGridColumns[j + 1];
              var valueCur = this.selectedGridColumns[j];
              this.selectedGridColumns[j] = After;
              this.selectedGridColumns[j + 1] = valueCur;
              this.selectedGridColumns[j].SequenceNo = j + 1;
              this.selectedGridColumns[j + 1].SequenceNo = j + 2;
              count++;
            }
          }
        }
      } else {
        swal('', 'Please select one value.')
      }
    } else if (option == 3) {
      // this.tempselectedAvailA;
      if (this.tempselectedAvailA && this.tempselectedAvailA.length == 1) {
        let svalue = this.tempselectedAvailA;
        for (var j = 0; j < this.selectedAdhocColumns.length; j++) {
          if (svalue == this.selectedAdhocColumns[j].ColumnId && count == 0) {
            if (j + 1 != this.selectedAdhocColumns.length) {
              var After = this.selectedAdhocColumns[j + 1];
              var valueCur = this.selectedAdhocColumns[j];
              this.selectedAdhocColumns[j] = After;
              this.selectedAdhocColumns[j + 1] = valueCur;
              this.selectedAdhocColumns[j].SequenceNo = j + 1;
              this.selectedAdhocColumns[j + 1].SequenceNo = j + 2;
              count++;
            }
          }
        }
      } else {
        swal('', 'Please select one value.')
      }
    }
  }
  SaveSearchConfig() {
    this.isLoader = true;
    let data: any = {};
    data.SelectedCompany = this.selectedCompany;
    //data.SelectedSearchColumn = this.selectedSearchColumns;
    data.SelectedGridColumn = this.selectedGridColumns;
    //data.SelectedAdHocSearchColumn = this.selectedAdhocColumns;
    //data.FormId = this.MyOrder;
    this.dataService.post('Order/SaveTRFListingDynamicColumn', data).subscribe(r => {
      if (r.IsSuccess) {
        swal('', r.Message);
        this.isLoader = false;
      } else {
        swal('', r.Message);
        this.isLoader = false;
      }
    });
  }

  dateFormat() {
    this.selectedBussinessCategoryNew = this.userCredential.BusinessCategoryId;
    this.getCompany();
    (<any>$('#date-format')).modal('show');
  }

  closeModel() {
    (<any>$('#date-format')).modal('hide');
    this.inputCompanyNew = '';
    this.selectedBussinessCategoryNew = this.userCredential.BusinessCategoryId;
    this.dateFormatId = 0;
    this.companyListNew = [];
  }

  saveDateFormat() {
    if (!this.inputCompanyNew.CompanyId ||
      this.selectedBussinessCategoryNew == 0 || this.dateFormatId == 0) {
      swal('', 'Please Select All Fields');
    } else {
      var format =
        this.dateFormatData.filter(i => i.id == this.dateFormatId)[0].value;
      var obj = { CompanyId: this.inputCompanyNew.CompanyId, DateFormat: format };
      this.dataService.post('Master/UpdateDateFormat', obj)
        .subscribe(
          res => {
            if (res.IsSuccess) {
              this.isLoader = false;
              this.closeModel();
              swal('', res.Message);
            } else {
              this.isLoader = false;
              swal('', res.Message);
            }
          },
          err => {
            this.isLoader = false;
            swal('', err.Message);
          });
    }
  }

  getDateFormat() {
    if (!!this.inputCompanyNew.CompanyId) {
      let value =
        this.companyListNew
          .filter(i => i.CompanyId == this.inputCompanyNew.CompanyId)[0]
          .DateFormat;
      if (value == '') {
        this.dateFormatId = 0;
      } else {
        this.dateFormatId =
          this.dateFormatData.filter(i => i.value == value)[0].id;
      }
    }
  }

  getCompany() {
    this.isLoader = true;
    this.dateFormatId = 0;
    this.inputCompanyNew = '';
    if (this.selectedBussinessCategoryNew > 0) {
      this.dataService
        .get(
          'master/GetCompanyList?BusinessCategoryId=' +
          this.selectedBussinessCategoryNew)
        .subscribe(r => {
          this.isLoader = false;
          this.companyListNew = r.CompanyList;
        });
    }
  }
}