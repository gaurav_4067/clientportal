import { EtrfConfigurationModule } from './etrf-configuration.module';

describe('EtrfConfigurationModule', () => {
  let etrfConfigurationModule: EtrfConfigurationModule;

  beforeEach(() => {
    etrfConfigurationModule = new EtrfConfigurationModule();
  });

  it('should create an instance', () => {
    expect(etrfConfigurationModule).toBeTruthy();
  });
});
