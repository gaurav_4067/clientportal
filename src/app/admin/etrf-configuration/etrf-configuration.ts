export const SEARCH_MODEL = {
  'PageSize': 10,
  'PageNumber': 1,
  'OrderBy': 'ModifiedOn',
  'Order': 'desc',
  'SearchText': [
    {'ColumnName': 'ClientID', 'Operator': 1, 'ColumnValue': ''},
    {'ColumnName': 'FormName', 'Operator': 5, 'ColumnValue': ''}
  ]
}
