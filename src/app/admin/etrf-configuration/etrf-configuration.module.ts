import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule, MdAutocompleteModule} from '@angular/material';
import {MdInputModule} from '@angular2-material/input';
import {Ng2PaginationModule} from 'ng2-pagination';

import {FooterModule} from '../../footer/footer.module';
import {HeaderModule} from '../../header/header.module';

import {EtrfConfigurationRoutingModule} from './etrf-configuration-routing.module';
import {EtrfConfigurationComponent} from './etrf-configuration.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, HeaderModule, FooterModule, MdAutocompleteModule,
    MaterialModule, MdInputModule, EtrfConfigurationRoutingModule,
    Ng2PaginationModule, ReactiveFormsModule
  ],
  declarations: [EtrfConfigurationComponent]
})
export class EtrfConfigurationModule {
}
