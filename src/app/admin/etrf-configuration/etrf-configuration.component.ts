import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { DataService } from '../../mts.service';

import { SEARCH_MODEL } from './etrf-configuration';
import { Http } from '@angular/http';

declare let swal: any;

@Component({
  selector: 'app-etrf-configuration',
  templateUrl: './etrf-configuration.component.html',
  styleUrls: ['./etrf-configuration.component.css']
})
export class EtrfConfigurationComponent implements OnInit {
  totalItem: number;
  p: any;
  clientList: any = [];
  clientCtrl: FormControl;
  filteredClients: any;
  itemPerPage: any;
  isLoader: boolean;
  count: any = 0;
  searchModal: any;
  AutoCompleteReset: any = '';
  etrfList: any = [];
  importFormId: any = '';
  importFormIdLIMS: any = '';
  importsourceDbNameLIMS: any = '';
  LIMSTrfList: any;
  EtrfFormID: any;
  userCredential: any;
  selectedBusinessCategory: any;
  userDetail: any;

  constructor(private dataService: DataService, public router: Router, private http: Http) { }

  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userDetail && this.userCredential) {
      if (this.userCredential.UserPermission.ViewETRFconfiguration) {
        this.searchModal = SEARCH_MODEL;
        this.searchModal.PageSize = 10;
        this.searchModal.PageNumber = 1;
        this.searchModal.OrderBy = 'ModifiedOn';
        this.searchModal.Order = 'desc';
        this.searchModal.SearchText = [
          { 'ColumnName': 'ClientID', 'Operator': 1, 'ColumnValue': '' },
          { 'ColumnName': 'FormName', 'Operator': 5, 'ColumnValue': '' },
          { 'ColumnName': 'BusinessCategoryId', 'Operator': 1, 'ColumnValue': '' }
        ];
        this.p = 1;
        this.itemPerPage = 1;
        this.totalItem = 0;
        var that = this;
        this.isLoader = true;
        that.dataService.get('master/GetCompanyList').subscribe(r => {
          if (r.CompanyList.length > 0) {
            that.clientList = r.CompanyList.map(
              (data,
                index) => { return ({ name: data.CompanyName, id: data.CompanyId }) })
            that.clientCtrl = new FormControl();
            that.filteredClients =
              that.clientCtrl.valueChanges.startWith(null)
                .map(
                  user => user && typeof user === 'object' ? user.name : user)
                .map(
                  name => name ? this.filter(name) : this.clientList.slice());
          }
        });

        this.getETRFList();
        this.selectedBusinessCategory = 0;
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  getBusinessCategory() {
    var that = this;
    this.isLoader = true;
    this.searchModal.SearchText[2].ColumnValue = this.selectedBusinessCategory;
    that.dataService
      .get(
        'master/GetCompanyList?BusinessCategoryId=' +
        this.selectedBusinessCategory)
      .subscribe(r => {
        that.isLoader = false;
        // if (r.CompanyList.length > 0)
        {
          that.clientList = r.CompanyList.map(
            (data, index) => {
              return ({ name: data.CompanyName, id: data.CompanyId })
            })
          that.clientCtrl = new FormControl();
          that.filteredClients =
            that.clientCtrl.valueChanges.startWith(null)
              .map(
                user =>
                  user && typeof user === 'object' ? user.name : user)
              .map(
                name =>
                  name ? this.filter(name) : this.clientList.slice());
        }
      });
  }

  getETRFList() {
    this.isLoader = true;
    this.dataService
      .post('DynamicFormCreation/ManageFormList', this.searchModal)
      .subscribe(r => {
        if (r.IsSuccess) {
          // this.p =  r.TotalRecords;
          this.etrfList = JSON.parse(r.Data);
          this.totalItem =
            Math.ceil(r.TotalRecords / this.searchModal.PageSize);
        }
        this.isLoader = false;
      });
  }

  getPage(page: number) {
    this.searchModal.PageNumber = page;
    this.isLoader = true;
    this.dataService
      .post('DynamicFormCreation/ManageFormList', this.searchModal)
      .subscribe(response => {
        this.isLoader = false;
        if (response.IsSuccess) {
          this.p = page;
          this.etrfList = JSON.parse(response.Data);
        } else {
          swal('', response.Message);
        }
      })
  }

  sorting(val: any) {
    if (this.searchModal.Order == 'asc') {
      this.searchModal.Order = 'desc';
    } else {
      this.searchModal.Order = 'asc';
    }
    this.searchModal.OrderBy = val;
    this.getETRFList();
  }

  filter(name: string): any {
    return this.clientList.filter(
      option => new RegExp(`^${name}`, 'gi').test(option.name));
  }

  selectedClient(id: any) {
    this.count++;
    if (this.count == 1) {
      this.searchModal.SearchText[0].ColumnValue = id;
    } else if (this.count >= 2) {
      if (this.count % 2 == 0) {
        this.searchModal.SearchText[0].ColumnValue = id;
      } else {
        this.count = 1;
      }
    }
  }

  submitTRF() {
    (<any>$('#import-etrf')).modal('hide');
    var that = this;

    if (this.importFormId) {
      this.router.navigate(
        [this.router.url + '/import-etrf', that.importFormId])
    }
  }

  submitTRFLIMS() {
    (<any>$('#import-etrf-LIMS')).modal('hide');
    var that = this;

    // var formId = that.EtrfFormID;
    that.importsourceDbNameLIMS = '';
    if (that.EtrfFormID) {
      (<any>$('.RadioEtrf')).prop('checked', false);
      this.router.navigate([
        this.router.url + '/import-lims-etrf', that.EtrfFormID,
        that.importsourceDbNameLIMS
      ])
    }
    // if (this.importFormIdLIMS) {
    //     this.router.navigate(['/importLimsETRF', that.importFormIdLIMS])
    // }
  }

  activeDeactiveETRF(id: any, index: any) {
    var that = this;
    that.isLoader = true;
    that.dataService.get('DynamicFormCreation/UpdateTrf?TRFID=' + id)
      .subscribe(r => {
        that.isLoader = false;
        if (r.IsSuccess) {
          if (that.etrfList[index].Status == true) {
            that.etrfList[index].Status = false;
          } else {
            that.etrfList[index].Status = true;
          }
          swal('', r.Message);
        } else {
          swal('', r.Message);
        }
      });
  }

  search() {
    this.getETRFList();
  }

  reset() {
    this.searchModal.e_TRF_Name = '';
    this.AutoCompleteReset = null;
    this.searchModal.ClientId = 0;
    this.selectedBusinessCategory = 0;
    this.searchModal.SearchText[1].ColumnValue = '';
    this.searchModal.SearchText[2].ColumnValue = '';
    this.searchModal.SearchText[0].ColumnValue = '';
    this.getETRFList();
  }
  ImportEtrfLIMS() {
    var that = this;

    that.dataService.get('DynamicFormCreation/LIMSConfiguredTrfList')
      .subscribe(r => {
        that.isLoader = false;
        if (r.IsSuccess) {
          that.LIMSTrfList = r.Data;
          (<any>$('#import-etrf-LIMS')).modal('show');
          (<any>$('.RadioEtrf')).prop('checked', false);
          that.EtrfFormID = '';
        } else {
          swal('', r.Message);
        }
      });
  }

  updateETRFToProduction(formId, index) {
    //this.dataService.get('https://clientportal.mts-global.com/ClientSolutionService/api/DynamicFormCreation/GetConfiguration?FormId='+formId+'&ClientCompanyid=0').subscribe(
    this.dataService.get('DynamicFormCreation/GetConfigurationFromLocalDB?FormId=' + formId + '&ClientCompanyid=0').subscribe(
      r => {
        let response = r;
        if (r.IsSuccess) {
          let formConfig = response.Data;
          if (formConfig) {
            this.http.post(this.dataService.updateETRFUrl + 'DynamicFormCreation/SaveConfigurationToOtherDB', formConfig).subscribe(
              updateResponse => {
                this.isLoader = false;
                let resp = updateResponse.json();
                if (resp.IsSuccess) {
                  swal("", resp.Message);
                }
              }
            );
          }
        }
        this.isLoader = false;
      }
    );
  }
}
