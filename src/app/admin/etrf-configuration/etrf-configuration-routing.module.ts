import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EtrfConfigurationComponent } from './etrf-configuration.component';

const routes: Routes = [{
    path: '',
    children: [
        { path: '', component: EtrfConfigurationComponent }, {
            path: 'add-new-etrf',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                    .ensure(
                        [],
                        require => {
                            resolve(
                                require(
                                    './add-new-etrf/add-new-etrf.module')
                                    .AddNewEtrfModule);
                        })
                })
        },
        {
            path: 'edit-etrf/:id',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                    .ensure(
                        [],
                        require => {
                            resolve(
                                require(
                                    './add-new-etrf/add-new-etrf.module')
                                    .AddNewEtrfModule);
                        })
                })
        },
        {
            path: 'copy-etrf/:id',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                    .ensure(
                        [],
                        require => {
                            resolve(
                                require(
                                    './add-new-etrf/add-new-etrf.module')
                                    .AddNewEtrfModule);
                        })
                })
        },
        {
            path: 'import-etrf/:id',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                    .ensure(
                        [],
                        require => {
                            resolve(
                                require(
                                    './add-new-etrf/add-new-etrf.module')
                                    .AddNewEtrfModule);
                        })
                })
        },
        {
            path: 'import-lims-etrf/:src_db/:id',
            loadChildren: () => new Promise(
                resolve => {
                    (require as any)
                    .ensure(
                        [],
                        require => {
                            resolve(
                                require(
                                    './add-new-etrf/add-new-etrf.module')
                                    .AddNewEtrfModule);
                        })
                })
        }
    ]
}];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class EtrfConfigurationRoutingModule {
}
