import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AddNewEtrfComponent} from './add-new-etrf.component';

const routes: Routes = [{path: '', component: AddNewEtrfComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class AddNewEtrfRoutingModule {
}
