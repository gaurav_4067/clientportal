import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewEtrfComponent } from './add-new-etrf.component';

describe('AddNewEtrfComponent', () => {
  let component: AddNewEtrfComponent;
  let fixture: ComponentFixture<AddNewEtrfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewEtrfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewEtrfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
