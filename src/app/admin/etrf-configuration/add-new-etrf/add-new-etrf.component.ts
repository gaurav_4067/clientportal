import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { DataService } from '../../../mts.service';

import { ADD_SECTION, ETRF_SECTION_MODEL } from './add-new-etrf';

declare let swal: any;

@Component({
  selector: 'app-add-new-etrf',
  templateUrl: './add-new-etrf.component.html',
  styleUrls: ['./add-new-etrf.component.css']
})
export class AddNewEtrfComponent implements OnInit {
  userCredential: any;
  userDetail: any;
  constructor(private dataService: DataService, public router: Router) {
    this.addSectionModel = Object.assign({}, ADD_SECTION);
  }

  clientList: any = [];
  BussinessCtrl: FormControl;
  clientCtrl: FormControl;
  filteredClients: any;
  filteredBussiness: any;
  typeList: any;
  FilteredDivision: any;
  isLoader: boolean;
  count: any = 0;
  ETRFsection: any = [];
  addSectionModel: any;
  finalModel: any;
  counterBusinessCat: any = true;
  AutoCompleteReset: any;
  editCase: any = false;
  importCase: any = false;
  copyCase: any = false;
  sectionShowButton: any = null;
  userId: any;
  TableList: any = [];
  MasterTableList: any = [];
  ComponentList: any = {};
  ready: any = false;

  ngOnInit() {
    // get current logged-in user id
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    if (this.userCredential && this.userDetail) {
      this.userId = this.userDetail.UserId;
      this.isLoader = true;
      var that = this;
      that.dataService.get('DynamicFormCreation/GetComponentList')
        .subscribe(r => {
          if (r.IsSuccess) {
            for (let i in r.Data) {
              r.Data[i].forEach((data) => {
                data.className = '';
              })
            }
            that.ComponentList = r.Data;
            if (url.indexOf('edit-etrf') != -1 ||
              url.indexOf('copy-etrf') != -1 ||
              url.indexOf('import-etrf') != -1) {
              var id = url[url.length - 1];
              that.dataService
                .get('DynamicFormCreation/GetUsedComponentList?FormId=' + id)
                .subscribe(r => {
                  var usedComponentList = r.Data;
                  for (let i in usedComponentList) {
                    if (usedComponentList[i] != null) {
                      usedComponentList[i].forEach(
                        (data) => {
                          that.ComponentList[i].forEach((val) => {
                            if (data.ColumnName == val.ColumnName) {
                              val.className = 'used';
                              return;
                            }
                          })
                        })
                    }
                  }
                  // Ready to render the fields (used in .html file)
                  that.ready = true;
                  // that.isLoader = false;
                })
            } else {
              that.ready = true;
              // that.isLoader = false;
            }
          }
        })
      that.dataService.get('DynamicFormCreation/BussinessType').subscribe(r => {
        if (r.IsSuccess) {
          that.filteredBussiness = r.Data;
        }
      });
      that.dataService.get('DynamicFormCreation/ETRFtypeList').subscribe(r => {
        if (r.IsSuccess) {
          that.typeList = r.Data;
        }
      });
      that.dataService.get('DynamicFormCreation/TableList').subscribe(r => {
        if (r.IsSuccess) {
          that.TableList = r.Data;
        }
      })
      that.dataService.get('DynamicFormCreation/MaterTable').subscribe(r => {
        if (r.IsSuccess) {
          that.MasterTableList = r.Data;
        }
      })
      // check if it is a case of 'edit' or 'new etrf' or 'Copy etrf'
      var url = window.location.href.split('/');
      if (url.indexOf('add-new-etrf') != -1) {
        // New ETRF case, assign default values to 'finalModel'
        this.finalModel = null;
        this.finalModel = ETRF_SECTION_MODEL;
        // Clearning Model
        this.finalModel.FormName = '';
        this.finalModel.Description = '';
        this.finalModel.ClientAlias = '';
        this.finalModel.CreatedBy = this.userId;
        this.finalModel.ModifiedBy = this.userId;
        var that = this;
        setTimeout(function () {
          that.isLoader = false;
        }, 5000)
      } else if (url.indexOf('edit-etrf') != -1) {
        // Edit ETRF case, get id of etrf from url and then get its data and
        // assign to 'finalModel'
        var id = url[url.length - 1];
        this.editCase = true;
        // Till response we set the default values in order to remove errors of
        // ngModel
        this.finalModel = ETRF_SECTION_MODEL;
        this.fetchFormData('edit', id);
      } else if (url.indexOf('copy-etrf') != -1) {
        var id = url[url.length - 1];
        this.editCase = true;
        this.copyCase = true;
        this.finalModel = ETRF_SECTION_MODEL;
        this.fetchFormData('copy', id);
      } else if (url.indexOf('import-etrf') != -1) {
        var id = url[url.length - 1];
        this.editCase = true;
        this.importCase = true;
        this.finalModel = ETRF_SECTION_MODEL;
        this.importData(id);
      } else if (url.indexOf('import-lims-etrf') != -1) {
        // this.isLoader = false;
        var id = url[url.length - 2];
        var SourceDB = url[url.length - 1];
        this.editCase = true;
        this.importCase = true;
        this.finalModel = ETRF_SECTION_MODEL;
        this.importLimsData(id, SourceDB);
      }
    } else {
      this.router.navigate(['/login']);
    }
    // this.userId = this.userDetail.UserId;

    // // get Client List and bind it to autocomplete
    // var that = this;
    // this.isLoader = true;

    // Code Commented by Rohit choudhary to fix Issue :- Client load list on
    // edit or add new configuration according to Bussiness category

    // that.dataService.get('master/GetCompanyList').subscribe(
    //     r => {
    //         if (r.CompanyList.length > 0) {
    //             that.clientList = r.CompanyList.map((data, index) => {
    //                 return (
    //                     {
    //                         name: data.CompanyName,
    //                         id: data.CompanyId
    //                     }
    //                 )
    //             })
    //             that.clientCtrl = new FormControl();
    //             that.filteredClients = that.clientCtrl.valueChanges
    //                 .startWith(null)
    //                 .map(user => user && typeof user === 'object' ? user.name
    //                 : user) .map(name => name ? that.filter(name) :
    //                 that.clientList.slice());

    //         }
    //     }
    // );



    // Get the Table and component list for AddFields
  }



  getCompanyListAsPerBussinessCat(businessCatId: any) {
    var that = this;
    that.isLoader = true;
    that.dataService
      .get('master/GetCompanyList?BusinessCategoryId=' + businessCatId)
      .subscribe(r => {
        that.isLoader = false;
        if (r.CompanyList.length > 0) {
          that.clientList = r.CompanyList.map(
            (data, index) => {
              return ({ name: data.CompanyName, id: data.CompanyId })
            })
          that.clientCtrl = new FormControl();
          that.filteredClients =
            that.clientCtrl.valueChanges.startWith(null)
              .map(
                user =>
                  user && typeof user === 'object' ? user.name : user)
              .map(
                name =>
                  name ? that.filter(name) : that.clientList.slice());
        }
      });
  }

  OnBussinessChange(BussinessId: any) {
    if (this.counterBusinessCat == true) {
      var that = this;
      this.getCompanyListAsPerBussinessCat(BussinessId);
      this.dataService
        .get('DynamicFormCreation/DivisionList?Category=' + BussinessId)
        .subscribe(r => {
          if (r.IsSuccess) {
            that.FilteredDivision = r.Data;
          }
        });
    } else {
      this.counterBusinessCat = true;
    }
  }

  importLimsData(id: any, Dbsource: any) {
    var that = this;
    this.dataService
      .get(
        '/DynamicFormCreation/ExportTRFConfig?sourceDbName=' + Dbsource +
        '&formId=' + id)
      .subscribe(r => {
        if (r.IsSuccess) {
          that.finalModel = r.Response;
          that.AutoCompleteReset = r.Response.ClientName;
          if (r.Response.IsAdditionalNotesSection == true) {
            (<any>$('#additionalNote')).trigger('click');
          }
          if (r.Response.IsSendEmail == true) {
            (<any>$('#sendEmailAddEtrf')).trigger('click');
          }
          if (r.Response.DataSaveType) {
            if (r.Response.DataSaveType == 1) {
              that.sectionShowButton = 3;
            }
            if (r.Response.DataSaveType == 2) {
              that.sectionShowButton = 2;
            }
            if (r.Response.DataSaveType == 3) {
              that.sectionShowButton = 1;
            }
          }
          that.ETRFsection = r.Response.ETRFsection;
          that.finalModel.CreatedBy = this.userId;
          that.finalModel.ModifiedBy = this.userId;
        } else {
          that.isLoader = false;
          swal({ title: '', text: r.Message }, function () {
            that.router.navigate(['/admin/etrf-configuration']);
            swal.close();
          });
        }
        setTimeout(function () {
          that.isLoader = false;
        }, 10000)
      })
  }


  importData(id: any) {
    var that = this;
    this.dataService
      .getStatic(
        '/DynamicFormCreation/GetConfiguration?FormID=' + id +
        '&ClientCompanyid=0')
      .subscribe(r => {
        if (r.IsSuccess) {
          that.finalModel = r.Data;
          that.AutoCompleteReset = r.Data.ClientName;
          if (r.Data.IsAdditionalNotesSection == true) {
            (<any>$('#additionalNote')).trigger('click');
          }
          if (r.Data.IsSendEmail == true) {
            (<any>$('#sendEmailAddEtrf')).trigger('click');
          }
          if (r.Data.DataSaveType) {
            if (r.Data.DataSaveType == 1) {
              that.sectionShowButton = 3;
            }
            if (r.Data.DataSaveType == 2) {
              that.sectionShowButton = 2;
            }
            if (r.Data.DataSaveType == 3) {
              that.sectionShowButton = 1;
            }
          }
          if (r.Data.BussinessId != '') {
            that.OnBussinessChange(r.Data.BussinessId);
          }
          that.ETRFsection = r.Data.ETRFsection;
          that.finalModel.CreatedBy = this.userId;
          that.finalModel.ModifiedBy = this.userId;
        } else {
          that.isLoader = false;
          swal({ title: '', text: r.Message }, function () {
            that.router.navigate(['/admin/etrf-configuration']);
            swal.close();
          });
        }
        setTimeout(function () {
          that.isLoader = false;
        }, 10000)
      })
  }

  fetchFormData(val: any, id: any) {
    var that = this;
    that.isLoader = true;
    that.dataService
      .get(
        'DynamicFormCreation/GetConfiguration?FormID=' + id +
        '&ClientCompanyid=' + 0)
      .subscribe(r => {
        if (r.IsSuccess) {
          that.counterBusinessCat = false;
          var x = new Promise((resolve, reject) => {
            that.getCompanyListAsPerBussinessCat(r.Data.BussinessId);

            this.dataService
              .get(
                'DynamicFormCreation/DivisionList?Category=' +
                r.Data.BussinessId)
              .subscribe(r => {
                if (r.IsSuccess) {
                  that.FilteredDivision = r.Data;
                }
              });
            resolve();
          }).then((data) => {
            that.finalModel = r.Data;
            that.AutoCompleteReset = r.Data.ClientName;
            if (r.Data.IsAdditionalNotesSection == true) {
              (<any>$('#additionalNote')).trigger('click');
            }
            if (r.Data.BussinessId != '') {
              that.OnBussinessChange(r.Data.BussinessId);
            }
            if (r.Data.IsSendEmail == true) {
              (<any>$('#sendEmailAddEtrf')).trigger('click');
            }
            if (r.Data.DataSaveType) {
              if (r.Data.DataSaveType == 1) {
                that.sectionShowButton = 3;
              }
              if (r.Data.DataSaveType == 2) {
                that.sectionShowButton = 2;
              }
              if (r.Data.DataSaveType == 3) {
                that.sectionShowButton = 1;
              }
            }
            that.ETRFsection = r.Data.ETRFsection;
            that.finalModel.CreatedBy = this.userId;
            that.finalModel.ModifiedBy = this.userId;
            if (that.finalModel.ETRFType != null) {
              that.finalModel.ETRFType = that.finalModel.ETRFType.toString();
            }
            if (val == 'copy') {
              that.finalModel.FormId = 0;
              that.finalModel.FormName = that.finalModel.FormName + '_Copy';
            }
          })
        } else {
          // swal('', r.Message);
          that.isLoader = false;
          swal({ title: '', text: r.Message }, function () {
            that.router.navigate(['/admin/etrf-configuration']);
            swal.close();
          });
        }
        setTimeout(function () {
          that.isLoader = false;
        }, 10000)
      })
  }

  filter(name: string): any {
    return this.clientList.filter(
      option => new RegExp(`^${name}`, 'gi').test(option.name));
  }

  selectedClient(id: any) {
    this.count++;
    if (this.count == 1) {
      this.finalModel.Clientid = id;
    } else if (this.count >= 2) {
      if (this.count % 2 == 0) {
        this.finalModel.Clientid = id;
      } else {
        this.count = 1;
      }
    }
  }

  addSection() {
    this.addSectionModel = {
      'SectionId': 0,
      'SectionName': '',
      'uniqueId': '',
      'dependentOnUniqueID': '',
      'IsVisible': true,
      'Sequence': null,
      'subSectionArr': [],
      'addFieldsArr': [],
      'addTableArr': []
    };
    this.addSectionModel.uniqueId = 'Section_' + Date.now().toString();
    var temp = Object.assign({}, this.addSectionModel);
    this.ETRFsection.push(temp);

    window.scrollTo(0, document.body.scrollHeight);
  }

  additionalNoteChange() {
    if ((<any>$('#additionalNote')).prop('checked') == true) {
      this.finalModel.IsAdditionalNotesSection = true;
    } else {
      this.finalModel.IsAdditionalNotesSection = false;
      this.finalModel.AdditionalNote = '';
    }
  }

  sendEmailChange() {
    if ((<any>$('#sendEmailAddEtrf')).prop('checked') == true) {
      this.finalModel.IsSendEmail = true;
    } else {
      this.finalModel.IsSendEmail = false;
    }
  }

  saveETRF(val: any) {
    var error = false;
    this.ETRFsection.forEach((data, index) => {
      data.Sequence = index;

      if (data.subSectionArr.length == 0 && data.addTableArr.length == 0 &&
        data.addFieldsArr.length == 0) {
        reject(data);
      }

      if (data.subSectionArr && data.subSectionArr.length > 0) {
        data.subSectionArr.forEach((innerData, innerIndex) => {
          innerData.Sequence = innerIndex;
          if (innerData.addFieldsArr && innerData.addFieldsArr.length > 0) {
            innerData.addFieldsArr.forEach((nestedData, nestedIndex) => {
              nestedData.Sequence = nestedIndex;
              if (!nestedData.selectTable) {
                reject(nestedData);
              }
            })
          } else {
            reject(innerData);
          }
        })
      } else if (data.addTableArr && data.addTableArr.length > 0) {
        data.addTableArr.forEach((innerData, innerIndex) => {
          innerData.Sequence = innerIndex;
          if (innerData.addFieldsArr && innerData.addFieldsArr.length > 0) {
            innerData.addFieldsArr.forEach((nestedData, nestedIndex) => {
              nestedData.Sequence = nestedIndex;
              if (!nestedData.selectTable) {
                reject(nestedData);
              }
            })
          } else {
            reject(innerData);
          }
        })
      } else if (data.addFieldsArr && data.addFieldsArr.length > 0) {
        data.addFieldsArr.forEach((innerData, innerIndex) => {
          innerData.Sequence = innerIndex;
          if (!innerData.selectTable) {
            reject(innerData);
          }
        })
      }
    })

    function reject(nestedData: any) {
      swal(
        '',
        'There is an Empty Section/Sub-Section/Table/Field of ID: ' +
        nestedData.uniqueId);
      error = true;
      return;
    }

    if (error == false) {
      this.finalModel.ETRFsection = this.ETRFsection;

      var that = this;
      that.isLoader = true;
      if (val == 'save') {
        that.dataService
          .post('DynamicFormCreation/SaveConfiguration', this.finalModel)
          .subscribe(r => {
            that.isLoader = false;
            if (r.IsSuccess) {
              swal(
                { title: '', text: r.Message },

                function () {
                  that.router.navigate(['/admin/etrf-configuration']);
                  swal.close();
                });
            } else {
              swal('', r.Message);
            }
          })
      } else {
        that.dataService
          .post('DynamicFormCreation/UpdateConfiguration', this.finalModel)
          .subscribe(r => {
            that.isLoader = false;
            if (r.IsSuccess) {
              swal(
                { title: '', text: r.Message },

                function () {
                  that.router.navigate(['/admin/etrf-configuration']);
                  swal.close();
                });
            } else {
              swal('', r.Message);
            }
          })
      }
    }
  }
}
