import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';
import {MdInputModule} from '@angular2-material/input';

import {FooterModule} from '../../../footer/footer.module';
import {HeaderModule} from '../../../header/header.module';

import {AddNewEtrfRoutingModule} from './add-new-etrf-routing.module';
import {AddNewEtrfComponent} from './add-new-etrf.component';
import {AddSectionModule} from './add-section/add-section.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, HeaderModule, FooterModule, ReactiveFormsModule,
    MaterialModule, MdInputModule, AddSectionModule, AddNewEtrfRoutingModule
  ],
  declarations: [AddNewEtrfComponent]
})
export class AddNewEtrfModule {
}
