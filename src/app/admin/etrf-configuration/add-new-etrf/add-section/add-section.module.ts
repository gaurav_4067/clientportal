import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';

import {AddFieldComponent} from './add-field/add-field.component';
import {AddFieldModule} from './add-field/add-field.module';
import {AddSectionRoutingModule} from './add-section-routing.module';
import {AddSectionComponent} from './add-section.component';
import {AddSubSectionModule} from './add-sub-section/add-sub-section.module';
import {AddTableModule} from './add-table/add-table.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, MaterialModule, AddSubSectionModule,
    AddFieldModule, AddTableModule
  ],
  declarations: [AddSectionComponent],
  exports: [AddSectionComponent]
})
export class AddSectionModule {
}
