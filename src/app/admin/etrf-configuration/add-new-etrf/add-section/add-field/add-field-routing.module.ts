import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AddFieldComponent} from './add-field.component';

const routes: Routes = [{path: 'add-field', component: AddFieldComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class AddFieldRoutingModule {
}
