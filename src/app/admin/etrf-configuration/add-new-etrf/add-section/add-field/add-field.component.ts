import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

declare let swal: any;

@Component({
  selector: 'add-fields',
  templateUrl: './add-field.component.html',
  styleUrls: ['./add-field.component.css']
})
export class AddFieldComponent implements OnInit {
  constructor(public router: Router) {}

  @Input() ETRFsection;
  @Input() sectionIndex;
  @Input() addFieldsIndex;
  @Input() subSectionIndex;
  @Input() addTableIndex;
  @Input() calledFrom;
  @Input() editCase;
  @Input() ComponentList;
  @Input() TableList;
  @Input() MasterTableList;
  // Because Add Fields can be called from Section and Sub-Section

  @Output() setToDefaultSection = new EventEmitter();

  model: any;
  tableList: any = [];
  componentList: any = [];
  componentListSelected: any = {};
  isLoader: boolean;

  validationField: any = {'validationType': '', 'min': '', 'max': ''};
  fieldValidationArr: any = [];
  previousComponent: any = '';
  previousTableName: any = '';

  ngOnInit() {
    // Setting up and empty Component List Object that will hold the selected
    // component value var prop = Object.keys(this.ComponentList); for(let i of
    // prop)
    // {
    // 	this.componentListSelected[i] = [];
    // }

    if (this.editCase == true) {
      if (this.calledFrom == 'section') {
        if (this.ETRFsection[this.sectionIndex]
                .addFieldsArr[this.addFieldsIndex]
                .selectTable) {
          this.componentList =
              this.ComponentList[this.ETRFsection[this.sectionIndex]
                                     .addFieldsArr[this.addFieldsIndex]
                                     .selectTable];
          this.ETRFsection[this.sectionIndex]
              .addFieldsArr[this.addFieldsIndex]
              .selectControlType = this.ETRFsection[this.sectionIndex]
                                       .addFieldsArr[this.addFieldsIndex]
                                       .selectControlType.toString();
        }
      } else if (this.calledFrom == 'sub-section') {
        if (this.ETRFsection[this.sectionIndex]
                .subSectionArr[this.subSectionIndex]
                .addFieldsArr[this.addFieldsIndex]
                .selectTable) {
          this.componentList =
              this.ComponentList[this.ETRFsection[this.sectionIndex]
                                     .subSectionArr[this.subSectionIndex]
                                     .addFieldsArr[this.addFieldsIndex]
                                     .selectTable];

          this.ETRFsection[this.sectionIndex]
              .subSectionArr[this.subSectionIndex]
              .addFieldsArr[this.addFieldsIndex]
              .selectControlType = this.ETRFsection[this.sectionIndex]
                                       .subSectionArr[this.subSectionIndex]
                                       .addFieldsArr[this.addFieldsIndex]
                                       .selectControlType.toString();
        }
      } else if (this.calledFrom == 'table') {
        this.componentList =
            this.ComponentList[this.ETRFsection[this.sectionIndex]
                                   .addTableArr[this.addTableIndex]
                                   .addFieldsArr[this.addFieldsIndex]
                                   .selectTable];

        this.ETRFsection[this.sectionIndex]
            .addTableArr[this.addTableIndex]
            .addFieldsArr[this.addFieldsIndex]
            .selectControlType = this.ETRFsection[this.sectionIndex]
                                     .addTableArr[this.addTableIndex]
                                     .addFieldsArr[this.addFieldsIndex]
                                     .selectControlType.toString();
      }
    }
  }

  onSelectTableChange() {
    if (this.calledFrom == 'section') {
      if (this.ETRFsection[this.sectionIndex]
              .addFieldsArr[this.addFieldsIndex]
              .selectTable) {
        this.componentList =
            this.ComponentList[this.ETRFsection[this.sectionIndex]
                                   .addFieldsArr[this.addFieldsIndex]
                                   .selectTable];
        this.ETRFsection[this.sectionIndex]
            .addFieldsArr[this.addFieldsIndex]
            .selectComponent = null;
      } else {
        this.componentList = null;
      }
    } else if (this.calledFrom == 'sub-section') {
      if (this.ETRFsection[this.sectionIndex]
              .subSectionArr[this.subSectionIndex]
              .addFieldsArr[this.addFieldsIndex]
              .selectTable) {
        this.componentList =
            this.ComponentList[this.ETRFsection[this.sectionIndex]
                                   .subSectionArr[this.subSectionIndex]
                                   .addFieldsArr[this.addFieldsIndex]
                                   .selectTable];
        this.ETRFsection[this.sectionIndex]
            .subSectionArr[this.subSectionIndex]
            .addFieldsArr[this.addFieldsIndex]
            .selectComponent = null;
      } else {
        this.componentList = null;
      }
    } else if (this.calledFrom == 'table') {
      if (this.ETRFsection[this.sectionIndex]
              .addTableArr[this.addTableIndex]
              .addFieldsArr[this.addFieldsIndex]
              .selectTable) {
        this.componentList =
            this.ComponentList[this.ETRFsection[this.sectionIndex]
                                   .addTableArr[this.addTableIndex]
                                   .addFieldsArr[this.addFieldsIndex]
                                   .selectTable];
        this.ETRFsection[this.sectionIndex]
            .addTableArr[this.addTableIndex]
            .addFieldsArr[this.addFieldsIndex]
            .selectComponent = null;
      } else {
        this.componentList = null;
      }
    }
  }

  masterTableChange() {
    if (this.calledFrom == 'section') {
      let tableName = this.ETRFsection[this.sectionIndex]
                          .addFieldsArr[this.addFieldsIndex]
                          .TableMaster;
      this.ETRFsection[this.sectionIndex]
          .addFieldsArr[this.addFieldsIndex]
          .selectQuery = this.getMasterTableQuery(tableName);
    } else if (this.calledFrom == 'sub-section') {
      let tableName = this.ETRFsection[this.sectionIndex]
                          .subSectionArr[this.subSectionIndex]
                          .addFieldsArr[this.addFieldsIndex]
                          .TableMaster;
      this.ETRFsection[this.sectionIndex]
          .subSectionArr[this.subSectionIndex]
          .addFieldsArr[this.addFieldsIndex]
          .selectQuery = this.getMasterTableQuery(tableName);
    } else if (this.calledFrom == 'table') {
      let tableName = this.ETRFsection[this.sectionIndex]
                          .addTableArr[this.addTableIndex]
                          .addFieldsArr[this.addFieldsIndex]
                          .TableMaster;
      this.ETRFsection[this.sectionIndex]
          .addTableArr[this.addTableIndex]
          .addFieldsArr[this.addFieldsIndex]
          .selectQuery = this.getMasterTableQuery(tableName);
    }
  }

  getMasterTableQuery(tableName: any) {
    var Query = '';
    if (tableName) {
      this.MasterTableList.forEach((data) => {
        if (data.TAbleName == tableName) {
          Query = data.Query;
          return;
        }
      })
    }
    return Query;
  }

  beforeTableChange(oldTableName: any) {
    if (this.calledFrom == 'section') {
      var previousComponent = this.ETRFsection[this.sectionIndex]
                                  .addFieldsArr[this.addFieldsIndex]
                                  .selectComponent;
      if (previousComponent) {
        this.componentList.forEach((data, i) => {
          if (data.ComponentName == previousComponent) {
            data.className = '';
          }
        })
      }

    } else if (this.calledFrom == 'sub-section') {
      var previousComponent = this.ETRFsection[this.sectionIndex]
                                  .subSectionArr[this.subSectionIndex]
                                  .addFieldsArr[this.addFieldsIndex]
                                  .selectComponent;
      if (previousComponent) {
        this.componentList.forEach((data, i) => {
          if (data.ComponentName == previousComponent) {
            data.className = '';
          }
        })
      }

    } else if (this.calledFrom == 'table') {
      var previousComponent = this.ETRFsection[this.sectionIndex]
                                  .addTableArr[this.addTableIndex]
                                  .addFieldsArr[this.addFieldsIndex]
                                  .selectComponent;
      if (previousComponent) {
        this.componentList.forEach((data, i) => {
          if (data.ComponentName == previousComponent) {
            data.className = '';
          }
        })
      }
    }
  }

  onComponentBeforeChange(oldVal: any, tableName: any) {
    if (this.calledFrom == 'section') {
      if (oldVal && tableName) {
        this.previousComponent = oldVal;
        this.previousTableName = tableName;
      }
    } else if (this.calledFrom == 'sub-section') {
      if (oldVal && tableName) {
        this.previousComponent = oldVal;
        this.previousTableName = tableName;
      }
    } else if (this.calledFrom == 'table') {
      if (oldVal && tableName) {
        this.previousComponent = oldVal;
        this.previousTableName = tableName;
      }
    }
  }

  onComponentChange() {
    if (this.calledFrom == 'section') {
      var tableName = this.ETRFsection[this.sectionIndex]
                          .addFieldsArr[this.addFieldsIndex]
                          .selectTable;

      // Removing 'used' className from the
      if (this.previousTableName == tableName) {
        this.componentList.forEach((data, i) => {
          if (data.ComponentName == this.previousComponent) {
            data.className = '';
          }
        })
      }

      this.componentList
          .forEach((data, i) => {
            if (data.ComponentName ==
                this.ETRFsection[this.sectionIndex]
                    .addFieldsArr[this.addFieldsIndex]
                    .selectComponent) {
              data.className = 'used';
            }
          })

              this.componentList.forEach((obj) => {
                if (obj.ComponentName ==
                    this.ETRFsection[this.sectionIndex]
                        .addFieldsArr[this.addFieldsIndex]
                        .selectComponent) {
                  this.ETRFsection[this.sectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .enterLabelName = obj.LabelCaption;
                  this.ETRFsection[this.sectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .columnName = obj.ColumnName;
                  this.ETRFsection[this.sectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .defaultValue = obj.DefaultValue;
                  this.ETRFsection[this.sectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .dependentOnSection = obj.DependentOnSeection;
                  this.ETRFsection[this.sectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .dependentSection = obj.DependentSection;
                  this.ETRFsection[this.sectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .option = obj.Options;
                  this.ETRFsection[this.sectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .selectControlType = obj.ControlType.toString();
                  this.ETRFsection[this.sectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .width = '';
                }
              })


    } else if (this.calledFrom == 'sub-section') {
      var tableName = this.ETRFsection[this.sectionIndex]
                          .subSectionArr[this.subSectionIndex]
                          .addFieldsArr[this.addFieldsIndex]
                          .selectTable;

      if (this.previousTableName == tableName) {
        this.componentList.forEach((data, i) => {
          if (data.ComponentName == this.previousComponent) {
            data.className = '';
          }
        })
      }

      this.componentList
          .forEach((data, i) => {
            if (data.ComponentName ==
                this.ETRFsection[this.sectionIndex]
                    .subSectionArr[this.subSectionIndex]
                    .addFieldsArr[this.addFieldsIndex]
                    .selectComponent) {
              data.className = 'used';
            }
          })

              this.componentList.forEach((obj) => {
                if (obj.ComponentName ==
                    this.ETRFsection[this.sectionIndex]
                        .subSectionArr[this.subSectionIndex]
                        .addFieldsArr[this.addFieldsIndex]
                        .selectComponent) {
                  this.ETRFsection[this.sectionIndex]
                      .subSectionArr[this.subSectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .enterLabelName = obj.LabelCaption;
                  this.ETRFsection[this.sectionIndex]
                      .subSectionArr[this.subSectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .columnName = obj.ColumnName;
                  this.ETRFsection[this.sectionIndex]
                      .subSectionArr[this.subSectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .defaultValue = obj.DefaultValue;
                  this.ETRFsection[this.sectionIndex]
                      .subSectionArr[this.subSectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .dependentOnSection = obj.DependentOnSeection;
                  this.ETRFsection[this.sectionIndex]
                      .subSectionArr[this.subSectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .dependentSection = obj.DependentSection;
                  this.ETRFsection[this.sectionIndex]
                      .subSectionArr[this.subSectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .option = obj.Options;
                  this.ETRFsection[this.sectionIndex]
                      .subSectionArr[this.subSectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .selectControlType = obj.ControlType.toString();
                  this.ETRFsection[this.sectionIndex]
                      .subSectionArr[this.subSectionIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .width = '';
                }
              })

    } else if (this.calledFrom == 'table') {
      var tableName = this.ETRFsection[this.sectionIndex]
                          .addTableArr[this.addTableIndex]
                          .addFieldsArr[this.addFieldsIndex]
                          .selectTable;

      if (this.previousTableName == tableName) {
        this.componentList.forEach((data, i) => {
          if (data.ComponentName == this.previousComponent) {
            data.className = '';
          }
        })
      }

      this.componentList
          .forEach((data, i) => {
            if (data.ComponentName ==
                this.ETRFsection[this.sectionIndex]
                    .addTableArr[this.addTableIndex]
                    .addFieldsArr[this.addFieldsIndex]
                    .selectComponent) {
              data.className = 'used';
            }
          })

              this.componentList.forEach((obj) => {
                if (obj.ComponentName ==
                    this.ETRFsection[this.sectionIndex]
                        .addTableArr[this.addTableIndex]
                        .addFieldsArr[this.addFieldsIndex]
                        .selectComponent) {
                  this.ETRFsection[this.sectionIndex]
                      .addTableArr[this.addTableIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .enterLabelName = obj.LabelCaption;
                  this.ETRFsection[this.sectionIndex]
                      .addTableArr[this.addTableIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .columnName = obj.ColumnName;
                  this.ETRFsection[this.sectionIndex]
                      .addTableArr[this.addTableIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .defaultValue = obj.DefaultValue;
                  this.ETRFsection[this.sectionIndex]
                      .addTableArr[this.addTableIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .dependentOnSection = obj.DependentOnSeection;
                  this.ETRFsection[this.sectionIndex]
                      .addTableArr[this.addTableIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .dependentSection = obj.DependentSection;
                  this.ETRFsection[this.sectionIndex]
                      .addTableArr[this.addTableIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .option = obj.Options;
                  this.ETRFsection[this.sectionIndex]
                      .addTableArr[this.addTableIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .selectControlType = obj.ControlType.toString();
                  this.ETRFsection[this.sectionIndex]
                      .addTableArr[this.addTableIndex]
                      .addFieldsArr[this.addFieldsIndex]
                      .width = '';
                }
              })
    }
  }

  bindToFinalModel() {
    if (this.calledFrom == 'section') {
      this.ETRFsection[this.sectionIndex].addFieldsArr[this.addFieldsIndex] =
          Object.assign({}, this.model);
    } else if (this.calledFrom == 'sub-section') {
      this.ETRFsection[this.sectionIndex]
          .subSectionArr[this.subSectionIndex]
          .addFieldsArr[this.addFieldsIndex] = Object.assign({}, this.model);
    } else if (this.calledFrom == 'table') {
      this.ETRFsection[this.sectionIndex]
          .addTableArr[this.addTableIndex]
          .addFieldsArr[this.addFieldsIndex] = Object.assign({}, this.model);
    }
  }


  getTableList() {
    // First Bind the model to Final Model
    // this.bindToFinalModel();

    // Call service for Table List
  }

  moveUp(index: any) {
    if (this.calledFrom == 'section') {
      if (index > 0) {
        var temp = this.ETRFsection[this.sectionIndex].addFieldsArr[index - 1];
        this.ETRFsection[this.sectionIndex].addFieldsArr.splice(
            index - 1, 0,
            this.ETRFsection[this.sectionIndex].addFieldsArr[index]);
        this.ETRFsection[this.sectionIndex].addFieldsArr.splice(index + 1, 1);
      } else {
        swal('', 'stop');
      }
    } else if (this.calledFrom == 'sub-section') {
      if (index > 0) {
        var temp = this.ETRFsection[this.sectionIndex]
                       .subSectionArr[this.subSectionIndex]
                       .addFieldsArr[index - 1];
        this.ETRFsection[this.sectionIndex]
            .subSectionArr[this.subSectionIndex]
            .addFieldsArr.splice(
                index - 1, 0,
                this.ETRFsection[this.sectionIndex]
                    .subSectionArr[this.subSectionIndex]
                    .addFieldsArr[index]);
        this.ETRFsection[this.sectionIndex]
            .subSectionArr[this.subSectionIndex]
            .addFieldsArr.splice(index + 1, 1);
      } else {
        swal('', 'stop');
      }
    } else if (this.calledFrom == 'table') {
      if (index > 0) {
        var temp = this.ETRFsection[this.sectionIndex]
                       .addTableArr[this.addTableIndex]
                       .addFieldsArr[index - 1];
        this.ETRFsection[this.sectionIndex]
            .addTableArr[this.addTableIndex]
            .addFieldsArr.splice(
                index - 1, 0,
                this.ETRFsection[this.sectionIndex]
                    .addTableArr[this.addTableIndex]
                    .addFieldsArr[index]);
        this.ETRFsection[this.sectionIndex]
            .addTableArr[this.addTableIndex]
            .addFieldsArr.splice(index + 1, 1);
      } else {
        swal('', 'stop');
      }
    }
  }

  moveDown(index: any) {
    if (this.calledFrom == 'section') {
      if (index < this.ETRFsection[this.sectionIndex].addFieldsArr.length - 1) {
        var temp = this.ETRFsection[this.sectionIndex].addFieldsArr[index + 1];
        this.ETRFsection[this.sectionIndex].addFieldsArr.splice(
            index + 1, 1,
            this.ETRFsection[this.sectionIndex].addFieldsArr[index]);
        this.ETRFsection[this.sectionIndex].addFieldsArr.splice(index, 1, temp);

      } else {
        swal('', 'stop');
      }
    } else if (this.calledFrom == 'sub-section') {
      if (index < this.ETRFsection[this.sectionIndex]
                      .subSectionArr[this.subSectionIndex]
                      .addFieldsArr.length -
              1) {
        var temp = this.ETRFsection[this.sectionIndex]
                       .subSectionArr[this.subSectionIndex]
                       .addFieldsArr[index + 1];
        this.ETRFsection[this.sectionIndex]
            .subSectionArr[this.subSectionIndex]
            .addFieldsArr.splice(
                index + 1, 1,
                this.ETRFsection[this.sectionIndex]
                    .subSectionArr[this.subSectionIndex]
                    .addFieldsArr[index]);
        this.ETRFsection[this.sectionIndex]
            .subSectionArr[this.subSectionIndex]
            .addFieldsArr.splice(index, 1, temp);

      } else {
        swal('', 'stop');
      }
    } else if (this.calledFrom == 'table') {
      if (index < this.ETRFsection[this.sectionIndex]
                      .addTableArr[this.addTableIndex]
                      .addFieldsArr.length -
              1) {
        var temp = this.ETRFsection[this.sectionIndex]
                       .addTableArr[this.addTableIndex]
                       .addFieldsArr[index + 1];
        this.ETRFsection[this.sectionIndex]
            .addTableArr[this.addTableIndex]
            .addFieldsArr.splice(
                index + 1, 1,
                this.ETRFsection[this.sectionIndex]
                    .addTableArr[this.addTableIndex]
                    .addFieldsArr[index]);
        this.ETRFsection[this.sectionIndex]
            .addTableArr[this.addTableIndex]
            .addFieldsArr.splice(index, 1, temp);

      } else {
        swal('', 'stop');
      }
    }
  }

  removeUsedClassFromComponent(path: any, index: any) {
    var componentName = path.selectComponent;
    var tableName = path.selectTable;
    this.componentList.forEach((data, i) => {
      if (data.ComponentName == componentName) {
        if (data.className) {
          data.className = '';
        }

        // this.ComponentList[tableName].push(data);
        this.componentList.forEach((val, innerIndex) => {
          if (val.ComponentName == componentName) {
            this.componentList.splice(innerIndex, 1, data);
          }
        });
        this.ComponentList[tableName].forEach((val, innerIndex) => {
          if (val.ComponentName == componentName) {
            this.ComponentList[tableName].splice(innerIndex, 1, data);
          }
        })

        index = i;
      }
    })
  }

  removeField(index: any) {
    if (this.calledFrom == 'section') {
      if (this.ETRFsection[this.sectionIndex]
              .addFieldsArr[index]
              .selectComponent) {
        this.removeUsedClassFromComponent(
            this.ETRFsection[this.sectionIndex].addFieldsArr[index], index);
      }
      this.ETRFsection[this.sectionIndex].addFieldsArr.splice(index, 1);
      if (this.ETRFsection[this.sectionIndex].addFieldsArr.length == 0) {
        this.setToDefaultSection.emit();
      }
    } else if (this.calledFrom == 'sub-section') {
      if (this.ETRFsection[this.sectionIndex]
              .subSectionArr[this.subSectionIndex]
              .addFieldsArr[index]
              .selectComponent) {
        this.removeUsedClassFromComponent(
            this.ETRFsection[this.sectionIndex]
                .subSectionArr[this.subSectionIndex]
                .addFieldsArr[index],
            index);
      }

      this.ETRFsection[this.sectionIndex]
          .subSectionArr[this.subSectionIndex]
          .addFieldsArr.splice(index, 1);
      if (this.ETRFsection[this.sectionIndex]
              .subSectionArr[this.subSectionIndex]
              .addFieldsArr.length == 0) {
        this.setToDefaultSection.emit();
      }
    } else if (this.calledFrom == 'table') {
      if (this.ETRFsection[this.sectionIndex]
              .addTableArr[this.addTableIndex]
              .addFieldsArr[index]
              .selectComponent) {
        this.removeUsedClassFromComponent(
            this.ETRFsection[this.sectionIndex]
                .addTableArr[this.addTableIndex]
                .addFieldsArr[index],
            index);
      }

      this.ETRFsection[this.sectionIndex]
          .addTableArr[this.addTableIndex]
          .addFieldsArr.splice(index, 1);
      if (this.ETRFsection[this.sectionIndex]
              .addTableArr[this.addTableIndex]
              .addFieldsArr.length == 0) {
        this.setToDefaultSection.emit();
      }
    }
  }

  setValidation(index: any) {
    if (this.calledFrom == 'section') {
      if (this.ETRFsection[this.sectionIndex]
              .addFieldsArr[index]
              .fieldValidationArr.length > 0) {
        this.fieldValidationArr = this.ETRFsection[this.sectionIndex]
                                      .addFieldsArr[index]
                                      .fieldValidationArr;
      } else {
        this.fieldValidationArr = [];
      }
    } else if (this.calledFrom == 'sub-section') {
      if (this.ETRFsection[this.sectionIndex]
              .subSectionArr[this.subSectionIndex]
              .addFieldsArr[index]
              .fieldValidationArr.length > 0) {
        this.fieldValidationArr = this.ETRFsection[this.sectionIndex]
                                      .subSectionArr[this.subSectionIndex]
                                      .addFieldsArr[index]
                                      .fieldValidationArr;
      } else {
        this.fieldValidationArr = [];
      }
    } else if (this.calledFrom == 'table') {
      if (this.ETRFsection[this.sectionIndex]
              .addTableArr[this.addTableIndex]
              .addFieldsArr[index]
              .fieldValidationArr.length > 0) {
        this.fieldValidationArr = this.ETRFsection[this.sectionIndex]
                                      .addTableArr[this.addTableIndex]
                                      .addFieldsArr[index]
                                      .fieldValidationArr;
      } else {
        this.fieldValidationArr = [];
      }
    }

    (<any>$(
         '#addValidationModal-' + this.sectionIndex + '-' + this.calledFrom +
         '' + (this.subSectionIndex ? this.subSectionIndex : '') + '' +
         (this.addTableIndex ? this.addTableIndex : '') + '-' +
         this.addFieldsIndex))
        .modal('show');
  }

  hideValidationModal(index: any, sectionIndex: any) {
    (<any>$(
         '#addValidationModal-' + this.sectionIndex + '-' + this.calledFrom +
         '' + (this.subSectionIndex ? this.subSectionIndex : '') + '' +
         (this.addTableIndex ? this.addTableIndex : '') + '-' +
         this.addFieldsIndex))
        .modal('hide');
  }

  addValidation(data: any) {
    if (data.validationType) {
      if (this.fieldValidationArr.length > 0) {
        this.fieldValidationArr.forEach((innerData) => {
          if (innerData.validationType == data.validationType) {
            swal('', 'This Validation is already applied');
          } else {
            this.fieldValidationArr.push(Object.assign({}, data));
          }
        })
      } else {
        this.fieldValidationArr.push(Object.assign({}, data));
      }

      this.validationField = {'validationType': '', 'min': '', 'max': ''};
    }
  }

  deleteValidation(index: any) {
    this.fieldValidationArr.splice(index, 1);
  }

  mergeValidationToField(index: any) {
    if (this.calledFrom == 'section') {
      this.fieldValidationArr.forEach((data) => {
        this.ETRFsection[this.sectionIndex]
            .addFieldsArr[index]
            .fieldValidationArr = this.fieldValidationArr;
      });
      this.fieldValidationArr = [];
      this.hideValidationModal(index, this.sectionIndex);
    } else if (this.calledFrom == 'sub-section') {
      this.fieldValidationArr
          .forEach((data) => {
            this.ETRFsection[this.sectionIndex]
                .subSectionArr[this.subSectionIndex]
                .addFieldsArr[index]
                .fieldValidationArr = this.fieldValidationArr;
          })

              this.fieldValidationArr = [];
      this.hideValidationModal(index, this.sectionIndex);
    } else if (this.calledFrom == 'table') {
      this.fieldValidationArr
          .forEach((data) => {
            this.ETRFsection[this.sectionIndex]
                .addTableArr[this.addTableIndex]
                .addFieldsArr[index]
                .fieldValidationArr = this.fieldValidationArr;
          })

              this.fieldValidationArr = [];
      this.hideValidationModal(index, this.sectionIndex);
    }
  }
}
