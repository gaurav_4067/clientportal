import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AddFieldRoutingModule} from './add-field-routing.module';
import {AddFieldComponent} from './add-field.component';

@NgModule({
  imports: [CommonModule, FormsModule, AddFieldRoutingModule],
  declarations: [AddFieldComponent],
  exports: [AddFieldComponent]
})
export class AddFieldModule {
}
