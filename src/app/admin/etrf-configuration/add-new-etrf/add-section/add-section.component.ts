import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {ADD_SUB_SECTION, ADD_TABLE} from './add-section';

declare let swal: any;

@Component({
  selector: 'app-add-section',
  templateUrl: './add-section.component.html',
  styleUrls: ['./add-section.component.css']
})
export class AddSectionComponent implements OnInit {
  @Input() ETRFsection;
  @Input() sectionIndex;
  @Input() sectionShowButton;
  @Input() editCase;
  @Input() TableList;
  @Input() ComponentList;
  @Input() MasterTableList;

  subsectionsArr: any = [];  // Sub-Section Array in  Add Section
  addFieldsArr: any = [];    // Add-Fields Array in Add Section
  addTableArr: any = [];     // Add-Table Array in Add Section
  // sectionShowButton: any = null;
  sectionTempIndex: any;

  addSubSectionModal: any;
  addFieldsModal: any;
  addTableModal: any;
  isLoader: boolean;

  constructor(public router: Router) {}

  ngOnInit() {
    this.addSubSectionModal = Object.assign({}, ADD_SUB_SECTION);
    // this.addFieldsModal = Object.assign({}, ADD_FIELDS);
    this.addTableModal = Object.assign({}, ADD_TABLE);

    if (this.sectionShowButton == 1) {
      this.sectionShowButton = 3;
    } else if (this.sectionShowButton == 2) {
      this.sectionShowButton = 1;
    } else if (this.sectionShowButton == 3) {
      this.sectionShowButton = 2;
    } else {
      this.sectionShowButton = null;
    }

    var that = this;
    if (this.editCase == true &&
        this.ETRFsection[this.sectionIndex].subSectionArr.length > 0) {
      this.subsectionsArr = JSON.parse(
          JSON.stringify(that.ETRFsection[that.sectionIndex].subSectionArr));
      this.addTableArr = JSON.parse(
          JSON.stringify(that.ETRFsection[that.sectionIndex].addTableArr));
      this.addFieldsArr = JSON.parse(
          JSON.stringify(that.ETRFsection[that.sectionIndex].addFieldsArr));
    } else if (
        this.editCase == true &&
        this.ETRFsection[this.sectionIndex].addTableArr.length > 0) {
      this.subsectionsArr = JSON.parse(
          JSON.stringify(that.ETRFsection[that.sectionIndex].subSectionArr));
      this.addTableArr = JSON.parse(
          JSON.stringify(that.ETRFsection[that.sectionIndex].addTableArr));
      this.addFieldsArr = JSON.parse(
          JSON.stringify(that.ETRFsection[that.sectionIndex].addFieldsArr));
    } else if (
        this.editCase == true &&
        this.ETRFsection[this.sectionIndex].addFieldsArr.length > 0) {
      this.subsectionsArr = JSON.parse(
          JSON.stringify(that.ETRFsection[that.sectionIndex].subSectionArr));
      this.addTableArr = JSON.parse(
          JSON.stringify(that.ETRFsection[that.sectionIndex].addTableArr));
      this.addFieldsArr = JSON.parse(
          JSON.stringify(that.ETRFsection[that.sectionIndex].addFieldsArr));
    }
    if (this.editCase == true &&
        this.ETRFsection[this.sectionIndex].subSectionArr.length == 0) {
      // Assign Default two Subsection modal
      this.subsectionsArr.push(Object.assign({}, {
        'SubsectionId': 0,
        'SubSectionName': '',
        'Sequence': null,
        'width': '',
        'uniqueId': 'Sub-Section_' + Date.now().toString(),
        'sectionShowButton': null,
        'addFieldsArr': [],
        'addTableArr': []
      }))

      var that = this;
      setTimeout(function() {
        that.subsectionsArr.push(Object.assign({}, {
          'SubsectionId': 0,
          'SubSectionName': '',
          'Sequence': null,
          'width': '',
          'uniqueId': 'Sub-Section_' + Date.now().toString(),
          'sectionShowButton': null,
          'addFieldsArr': [],
          'addTableArr': []
        }))
      }, 10)
    } else if (this.editCase == false) {
      // Assign Default two Subsection modal
      this.subsectionsArr.push(Object.assign({}, {
        'SubsectionId': 0,
        'SubSectionName': '',
        'Sequence': null,
        'width': '',
        'uniqueId': 'Sub-Section_' + Date.now().toString(),
        'sectionShowButton': null,
        'addFieldsArr': [],
        'addTableArr': []
      }))

      var that = this;
      setTimeout(function() {
        that.subsectionsArr.push(Object.assign({}, {
          'SubsectionId': 0,
          'SubSectionName': '',
          'Sequence': null,
          'width': '',
          'uniqueId': 'Sub-Section_' + Date.now().toString(),
          'sectionShowButton': null,
          'addFieldsArr': [],
          'addTableArr': []
        }))
      }, 10)
    }
  }

  delete(index: any, id: any) {
    if ((<any>$('#' + id)).find('add-fields').length > 0) {
      this.isLoader = true;
      var thatObj = this;
      var count = 1;
      var i = 0;
      (<any>$('#' + id)).find('add-fields').each(function() {
        var that = this;
        setTimeout(function() {
          (<any>$(that)).find('.col-md-2 .btn-danger').trigger('click');
        }, i)

        if (count == (<any>$('#' + id)).find('add-fields').length) {
          setTimeout(function() {
            thatObj.ETRFsection.splice(index, 1);
            thatObj.isLoader = false;
          }, i)
        }
        count++;
        i = i + 5;
      })
    } else {
      this.ETRFsection.splice(index, 1);
    }
  }

  checkDisable(sectionIndex: any) {
    var result;
    if ((this.ETRFsection[sectionIndex].subSectionArr.length == 0 ||
         this.ETRFsection[sectionIndex].subSectionArr.length == 2) &&
        this.ETRFsection[sectionIndex].addFieldsArr.length == 0 &&
        this.ETRFsection[sectionIndex].addTableArr.length == 0) {
      result = false;
    } else {
      result = true;
    }

    return result;
  }

  moveUp(index: any) {
    if (index > 0) {
      var temp = this.ETRFsection[index - 1];
      this.ETRFsection.splice(index - 1, 0, this.ETRFsection[index]);
      this.ETRFsection.splice(index + 1, 1);
    } else {
      swal('', 'No more section to move up');
    }
  }

  moveDown(index: any) {
    if (index < this.ETRFsection.length - 1) {
      var temp = this.ETRFsection[index + 1];
      this.ETRFsection.splice(index + 1, 1, this.ETRFsection[index]);
      this.ETRFsection.splice(index, 1, temp);

    } else {
      swal('', 'No more section to move down');
    }
  }

  showSubsectionModal(index: any) {
    this.sectionTempIndex = index;
    (<any>$('#addSubSectionModal-' + index)).modal('show');
  }

  moreSubsection() {
    this.addSubSectionModal = {
      'SubsectionId': 0,
      'SubSectionName': '',
      'Sequence': null,
      'width': '',
      'uniqueId': '',
      'sectionShowButton': null,
      'addFieldsArr': [],
      'addTableArr': []
    };
    this.addSubSectionModal.uniqueId = 'Sub-Section_' + Date.now().toString();
    this.subsectionsArr.push(Object.assign({}, this.addSubSectionModal));
  }

  removeSubSection(index: any) {
    this.subsectionsArr.splice(index, 1);
  }

  hideSubSectionModal(index: any) {
    (<any>$('#addSubSectionModal-' + index)).modal('hide');
  }

  addSubSection() {
    this.sectionShowButton = 1;
    var x = 50;
    this.ETRFsection[this.sectionTempIndex].subSectionArr =
        this.subsectionsArr.map((data) => {
          return Object.assign({}, data);
        });
    this.hideSubSectionModal(this.sectionTempIndex);
  }

  addTable(index: any) {
    this.sectionShowButton = 2;
    this.addTableModal = Object.assign({}, {
      'TableName': '',
      'SubTabelId': 0,
      'Sequence': null,
      'uniqueId': '',
      'addFieldsArr': ''
    });
    this.addTableModal.uniqueId = 'Table_' + Date.now().toString();
    this.addTableArr.push(Object.assign({}, this.addTableModal));
    this.ETRFsection[index].addTableArr = this.addTableArr;
  }

  addFields(index: any) {
    this.sectionShowButton = 3;
    this.addFieldsModal = {
      'selectTable': null,
      'selectComponent': null,
      'componentID': 0,
      'Sequence': null,
      'uniqueId': '',
      'enterLabelName': '',
      'selectControlType': '',
      'textOnOption': '',
      'columnName': '',
      'defaultValue': '',
      'dependentOnUniqueID': '',
      'dependentUniqueID': '',
      'TableMaster': null,
      'selectQuery': '',
      'option': '',
      'width': '',
      'fieldValidationArr': [],
      'IsVisible': true,
      // added by rohit tp add addclass
      'className': ''
    };
    this.addFieldsModal.uniqueId = 'Field_' + Date.now().toString();
    this.addFieldsArr.push(Object.assign({}, this.addFieldsModal));
    this.ETRFsection[index].addFieldsArr = this.addFieldsArr;
  }

  setToDefaultSection() {
    this.sectionShowButton = null;
  }
}
