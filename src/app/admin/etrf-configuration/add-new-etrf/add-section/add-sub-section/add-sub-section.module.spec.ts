import { AddSubSectionModule } from './add-sub-section.module';

describe('AddSubSectionModule', () => {
  let addSubSectionModule: AddSubSectionModule;

  beforeEach(() => {
    addSubSectionModule = new AddSubSectionModule();
  });

  it('should create an instance', () => {
    expect(addSubSectionModule).toBeTruthy();
  });
});
