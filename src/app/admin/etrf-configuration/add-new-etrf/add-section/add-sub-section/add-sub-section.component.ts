import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {ADD_SUBSECTION} from './add-sub-section';

declare let swal: any;

@Component({
  selector: 'sub-section',
  templateUrl: './add-sub-section.component.html',
  styleUrls: ['./add-sub-section.component.css']
})
export class AddSubSectionComponent implements OnInit {
  constructor(public router: Router) {}

  @Input() ETRFsection;
  @Input() sectionIndex;
  @Input() subSectionIndex;
  @Input() addFieldsIndex;
  @Input() addTableIndex;
  @Input() editCase;
  @Input() ComponentList;
  @Input() TableList;
  @Input() MasterTableList;

  showFields: any = null;  // this is used for hold 'Add Table' or 'Add Field'
  addFieldsArr: any = [];
  addTableArr: any = [];
  addSubSection: any;
  addFieldModal: any;
  isLoader: boolean;

  ngOnInit() {
    this.addSubSection = Object.assign({}, ADD_SUBSECTION);
    // this.addFieldModal = Object.assign({}, ADD_FIELDS);

    if (this.editCase == true) {
      this.addFieldsArr = this.ETRFsection[this.sectionIndex]
                              .subSectionArr[this.subSectionIndex]
                              .addFieldsArr;
      this.addTableArr = this.ETRFsection[this.sectionIndex]
                             .subSectionArr[this.subSectionIndex]
                             .addTableArr;
    }
  }

  moveUp(index: any) {
    if (index > 0) {
      var temp = this.ETRFsection[this.sectionIndex].subSectionArr[index - 1];
      this.ETRFsection[this.sectionIndex].subSectionArr.splice(
          index - 1, 0,
          this.ETRFsection[this.sectionIndex].subSectionArr[index]);
      this.ETRFsection[this.sectionIndex].subSectionArr.splice(index + 1, 1);
    } else {
      swal('', 'stop');
    }
  }

  moveDown(index: any) {
    if (index < this.ETRFsection[this.sectionIndex].subSectionArr.length - 1) {
      var temp = this.ETRFsection[this.sectionIndex].subSectionArr[index + 1];
      this.ETRFsection[this.sectionIndex].subSectionArr.splice(
          index + 1, 1,
          this.ETRFsection[this.sectionIndex].subSectionArr[index]);
      this.ETRFsection[this.sectionIndex].subSectionArr.splice(index, 1, temp);

    } else {
      swal('', 'stop');
    }
  }

  remove(index: any, id: any) {
    if (this.ETRFsection[this.sectionIndex].subSectionArr.length > 2) {
      // this.ETRFsection[this.sectionIndex].subSectionArr.splice(index, 1);
      if ((<any>$('#' + id)).find('add-fields').length > 0) {
        this.isLoader = true;
        var thatObj = this;
        var count = 1;
        var i = 0;
        (<any>$('#' + id)).find('add-fields').each(function() {
          var that = this;
          setTimeout(function() {
            (<any>$(that)).find('.col-md-2 .btn-danger').trigger('click');
          }, i)

          if (count == (<any>$('#' + id)).find('add-fields').length) {
            setTimeout(function() {
              thatObj.ETRFsection[thatObj.sectionIndex].subSectionArr.splice(
                  index, 1);
              thatObj.isLoader = false;
            }, i)
          }
          count++;
          i = i + 5;
        })
      } else {
        this.ETRFsection[this.sectionIndex].subSectionArr.splice(index, 1);
      }
    } else {
      swal('', 'Minimum Two Sub-Section is required');
    }
  }

  addFields(index: any) {
    this.showFields = 2;
    this.addFieldModal = Object.assign({}, {
      'selectTable': null,
      'selectComponent': null,
      'componentID': 0,
      'Sequence': null,
      'uniqueId': '',
      'enterLabelName': '',
      'selectControlType': '',
      'textOnOption': '',
      'columnName': '',
      'defaultValue': '',
      'dependentOnUniqueID': '',
      'dependentUniqueID': '',
      'selectQuery': '',
      'TableMaster': null,
      'option': '',
      'width': '',
      'fieldValidationArr': [],
      'IsVisible': true,
      // added by rohit tp add addclass
      'className': ''
    });
    this.addFieldModal.uniqueId = 'Field_' + Date.now().toString();
    this.addFieldsArr.push(Object.assign({}, this.addFieldModal));
    this.ETRFsection[this.sectionIndex].subSectionArr[index].addFieldsArr =
        this.addFieldsArr;
  }

  // addTable(index: any)
  // {
  // 	this.showFields = 1;
  // 	this.addTableArr.push({});
  // 	this.ETRFsection[this.sectionIndex].subSectionArr[index].addTableArr =
  // this.addTableArr;
  // }
}
