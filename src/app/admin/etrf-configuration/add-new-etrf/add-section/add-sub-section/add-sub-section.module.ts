import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';

import {AddFieldModule} from '../add-field/add-field.module';

import {AddSubSectionRoutingModule} from './add-sub-section-routing.module';
import {AddSubSectionComponent} from './add-sub-section.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, AddFieldModule, AddSubSectionRoutingModule,
    MaterialModule
  ],
  declarations: [AddSubSectionComponent],
  exports: [AddSubSectionComponent]
})
export class AddSubSectionModule {
}
