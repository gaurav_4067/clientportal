import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AddSubSectionComponent} from './add-sub-section.component';

const routes: Routes =
    [{path: 'add-sub-section', component: AddSubSectionComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class AddSubSectionRoutingModule {
}
