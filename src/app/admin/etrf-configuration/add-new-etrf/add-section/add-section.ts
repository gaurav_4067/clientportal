export const ADD_SUB_SECTION = {
  'SubsectionId': 0,
  'SubSectionName': '',
  'Sequence': null,
  'width': '',
  'uniqueId': '',
  'sectionShowButton': null,
  'addFieldsArr': [],
  'addTableArr': []
}

export const ADD_TABLE = {
  'TableName': '',
  'SubTabelId': 0,
  'Sequence': null,
  'uniqueId': '',
  'addFieldsArr': []
}