import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AddTableComponent} from './add-table.component';

const routes: Routes = [{path: 'table', component: AddTableComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class AddTableRoutingModule {
}
