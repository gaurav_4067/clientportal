import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';

import {AddFieldModule} from '../add-field/add-field.module';

import {AddTableRoutingModule} from './add-table-routing.module';
import {AddTableComponent} from './add-table.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, MaterialModule, AddTableRoutingModule,
    AddFieldModule
  ],
  declarations: [AddTableComponent],
  exports: [AddTableComponent]
})
export class AddTableModule {
}
