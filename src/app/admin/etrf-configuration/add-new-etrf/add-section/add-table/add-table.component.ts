import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

declare let swal: any;

@Component({
  selector: 'add-table',
  templateUrl: './add-table.component.html',
  styleUrls: ['./add-table.component.css']
})
export class AddTableComponent implements OnInit {
  constructor(public router: Router) {}

  @Input() ETRFsection;
  @Input() sectionIndex;
  @Input() subSectionIndex;
  @Input() addTableIndex;
  @Input() editCase;
  @Input() ComponentList;
  @Input() TableList;
  @Input() MasterTableList;
  @Output() setToDefaultSection = new EventEmitter();

  addFieldsArr: any = [];
  addFieldsModal: any;
  isLoader: boolean;

  ngOnInit() {
    // this.addFieldsModal = Object.assign({}, ADD_FIELDS);

    if (this.editCase == true) {
      if (this.ETRFsection[this.sectionIndex]
              .addTableArr[this.addTableIndex]
              .addFieldsArr) {
        this.addFieldsArr = this.ETRFsection[this.sectionIndex]
                                .addTableArr[this.addTableIndex]
                                .addFieldsArr;
      }
    }
  }

  moveUp(index: any) {
    if (index > 0) {
      var temp = this.ETRFsection[this.sectionIndex].addTableArr[index - 1];
      this.ETRFsection[this.sectionIndex].addTableArr.splice(
          index - 1, 0, this.ETRFsection[this.sectionIndex].addTableArr[index]);
      this.ETRFsection[this.sectionIndex].addTableArr.splice(index + 1, 1);
    } else {
      swal('', 'stop');
    }
  }

  moveDown(index: any) {
    if (index < this.ETRFsection[this.sectionIndex].addTableArr.length - 1) {
      var temp = this.ETRFsection[this.sectionIndex].addTableArr[index + 1];
      this.ETRFsection[this.sectionIndex].addTableArr.splice(
          index + 1, 1, this.ETRFsection[this.sectionIndex].addTableArr[index]);
      this.ETRFsection[this.sectionIndex].addTableArr.splice(index, 1, temp);
    } else {
      swal('', 'stop');
    }
  }

  removeTable(e: any, index: any, id: any) {
    if ((<any>$('#' + id)).find('add-fields').length > 0) {
      this.isLoader = true;
      var thatObj = this;
      var count = 1;
      var i = 0;
      (<any>$('#' + id)).find('add-fields').each(function() {
        var that = this;
        setTimeout(function() {
          (<any>$(that)).find('.col-md-2 .btn-danger').trigger('click');
        }, i)

        if (count == (<any>$('#' + id)).find('add-fields').length) {
          setTimeout(function() {
            thatObj.ETRFsection[thatObj.sectionIndex].addTableArr.splice(
                index, 1);
            thatObj.isLoader = false;
            if (thatObj.ETRFsection[thatObj.sectionIndex].addTableArr.length ==
                0) {
              thatObj.setToDefaultSection.emit();
            }
          }, i)
        }
        count++;
        i = i + 5;
      })
    } else {
      this.ETRFsection[this.sectionIndex].addTableArr.splice(index, 1);
      if (this.ETRFsection[this.sectionIndex].addTableArr.length == 0) {
        this.setToDefaultSection.emit();
      }
    }
    // this.ETRFsection[this.sectionIndex].addTableArr.splice(index, 1);
  }

  addFields(index: any) {
    this.addFieldsModal = {
      'selectTable': null,
      'selectComponent': null,
      'componentID': 0,
      'Sequence': null,
      'uniqueId': '',
      'enterLabelName': '',
      'selectControlType': '',
      'textOnOption': '',
      'columnName': '',
      'defaultValue': '',
      'dependentOnUniqueID': '',
      'dependentUniqueID': '',
      'TableMaster': null,
      'selectQuery': '',
      'option': '',
      'width': '',
      'fieldValidationArr': [],
      'IsVisible': true,
      // added by rohit tp add addclass
      'className': ''
    };
    this.addFieldsModal.uniqueId = 'Field_' + Date.now().toString();
    this.addFieldsArr.push(Object.assign({}, this.addFieldsModal));
    this.ETRFsection[this.sectionIndex].addTableArr[index].addFieldsArr =
        this.addFieldsArr;
  }
}
