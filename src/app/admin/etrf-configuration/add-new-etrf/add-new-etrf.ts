export const ADD_SECTION = {
  'SectionId': 0,
  'SectionName': '',
  'uniqueId': '',
  'BussinessId': '0',
  'DivisionID': '0',
  'dependentOnUniqueID': '',
  'Sequence': null,
  'IsVisible': true,
  'subSectionArr': [],
  'addFieldsArr': [],
  'addTableArr': []
}

export const ETRF_SECTION_MODEL = {
  'FormId': 0,
  'Clientid': 0,
  'FormName': '',
  'Description': '',
  'ETRFType': 0,
  'Status': true,
  'IsSendEmail': false,
  'IsAdditionalNotesSection': false,
  'AdditionalNote': '',
  'ClientAlias': '',
  'CreatedBy': '',
  'CreatedOn': '',
  'ModifiedBy': '',
  'ModifiedOn': '',
  'ETRFsection': []
}