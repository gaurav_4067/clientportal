import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtrfConfigurationComponent } from './etrf-configuration.component';

describe('EtrfConfigurationComponent', () => {
  let component: EtrfConfigurationComponent;
  let fixture: ComponentFixture<EtrfConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtrfConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtrfConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
