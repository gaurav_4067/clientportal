import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserModel } from '../../common/userModel';
import { InjectService } from '../../injectable-service/injectable-service';
import { DataService } from '../../mts.service';

import { SEARCH_MODEL } from './user';

declare let swal: any;

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUsersComponent implements OnInit, OnDestroy {
  userEmail: any;
  userPassword: string;
  userStatus: string;
  userModel: UserModel;
  private p: any;
  private totalItem: number;
  private itemPerPage: any;
  userList: any;
  searchModal: any;
  searchText: any;
  companyList: any;
  categoryList: any;
  sortType: string;
  orderBy: string;

  /*Search parameter*/
  public selectedBusinessCategory: any;
  public selectedCompany: any;
  public selectedCategory: number;
  public selectedStatus: number;
  public selectedEmail: any;
  /*end*/
  /*For Pop Up*/
  selectedBusinessCategoryId: any = 0;
  popUpcompanyList: any = [];
  popUpSelectedCompanyId: any;
  selectedUserCategoryId: any = 0;
  selectedUser: any;
  /*end*/
  categoryDefaultStr: any;
  isLoader: boolean;
  userCredential: any;
  clientId: any;
  limsCompanyList: any = [];
  clientPortalCompanyList: any = [];
  isOtherClientChecked: boolean = false;
  importedClientList: any = [];
  selectedClientIndex: number;
  userDetail: any;
  constructor(
    private dataService: DataService, private injectService: InjectService,
    private router: Router) {
    this.categoryDefaultStr = {
      'UserCategoryId': 0,
      'UserCategoryName': 'Select a Category'
    };
  }
  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userCredential && this.userDetail) {
      if (this.userCredential.UserPermission.ViewUserList) {
        this.p = 1;
        this.sortType = 'asc';
        this.orderBy = 'FirstName';
        this.itemPerPage = 1;
        this.totalItem = 0;
        // this.roleManagementModel=ROLE_MANAGEMENT_MODEL;
        this.searchModal = SEARCH_MODEL;
        this.searchText = Object;  // SEARCH_MANAGE_USER;
        this.selectedStatus = 0;

        this.searchModal.pageSize = 10;
        this.searchModal.pageNumber = 1;
        this.searchModal.searchText = '';
        this.searchText = {};
        // this.searchModal.totalRecords=2;
        this.selectedCategory = 0;
        this.selectedBusinessCategory =
          0;  // this.userCredential.BusinessCategoryId;

        this.isLoader = true;
        this.dataService
          .get(
            'User/GetAllUserList?PageSize=' + this.searchModal.pageSize +
            '&PageNumber=1&OrderBy=FirstName&Order=asc&SearchText=&TotalRecords=0')
          .subscribe(r => {
            this.isLoader = false;
            this.userList = r.UserList;
            this.itemPerPage = 1;
            this.totalItem =
              Math.ceil(r.TotalRecords / this.searchModal.pageSize);
          });
        this.dataService
          .get(
            'master/GetCompanyList?BusinessCategoryId=' +
            this.userCredential.BusinessCategoryId)
          .subscribe(r => {
            this.companyList = r.CompanyList;
          });
        this.dataService.get('master/GetUserCategory').subscribe(r => {
          this.categoryList = r.UserCategoryList;
          this.categoryList.splice(0, 0, this.categoryDefaultStr);
        });
      } else {
        this.router.navigate(['/landing-page']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  resetSearchForm() {
    this.selectedBusinessCategory = 0;
    this.selectedCompany = '';
    this.selectedEmail = '';
    this.selectedCategory = 0;
    this.selectedStatus = 0;
    this.search();
  }

  public renderCompany(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CompanyName}</div>
      </div>`;

    return html;
  }

  search() {
    this.searchText = {};
    if (this.selectedCompany) {
      this.searchText['CompanyId'] = this.selectedCompany.CompanyId;
    }
    if (this.selectedCategory) {
      this.searchText['UserCategoryId'] = this.selectedCategory;
    }
    if (this.selectedStatus > 0) {
      this.searchText['Status'] = this.selectedStatus;
    }
    if (this.selectedEmail) {
      this.searchText['Email'] = this.selectedEmail;
    }

    if (this.selectedBusinessCategory) {
      this.searchText['BusinessCategoryId'] = this.selectedBusinessCategory;
    }

    this.isLoader = true;
    this.searchModal.PageNumber = 1;

    this.dataService
      .get(
        'User/GetAllUserList?PageSize=' + this.searchModal.pageSize +
        '&PageNumber=1&OrderBy=FirstName&Order=' + this.sortType +
        '&SearchText=' + JSON.stringify(this.searchText) +
        '&TotalRecords=0')
      .subscribe(r => {
        this.isLoader = false;
        this.p = 1;
        this.userList = r.UserList;
        this.totalItem =
          Math.ceil(r.TotalRecords / this.searchModal.pageSize);
      })
  }
  getPage(page: number) {
    this.dataService
      .get(
        'User/GetAllUserList?PageSize=' + this.searchModal.pageSize +
        '&PageNumber=' + page + '&OrderBy=' + this.orderBy + '&Order=' +
        this.sortType + '&SearchText=' + JSON.stringify(this.searchText) +
        '&TotalRecords=0')
      .subscribe(r => {
        this.p = page;
        // this.itemPerPage=1;
        this.totalItem =
          Math.ceil(r.TotalRecords / this.searchModal.pageSize);
        this.userList = r.UserList;
      })
  }
  sorting(column: string) {
    this.orderBy = column;
    if (this.sortType == 'asc') {
      this.sortType = 'desc';
    } else {
      this.sortType = 'asc';
    }
    this.dataService
      .get(
        'User/GetAllUserList?PageSize=' + this.searchModal.pageSize +
        '&PageNumber=' + this.p + '&OrderBy=' + column + '&Order=' +
        this.sortType + '&SearchText=' + JSON.stringify(this.searchText) +
        '&TotalRecords=0')
      .subscribe(r => {
        // this.itemPerPage=1;
        this.totalItem =
          Math.ceil(r.TotalRecords / this.searchModal.pageSize);
        this.userList = r.UserList;
      })
  }
  deleteUser(userId: number) {
    var userDetail = JSON.parse(localStorage.getItem('userDetail'));



    var thisObj = this;
    swal(
      {
        title: '',
        text:
          '<span style=\'color:#ef770e;font-size:20px;\'>Are you sure?</span><br><br>',
        html: true,
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Proceed',
        cancelButtonText: 'Cancel',
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function (isConfirm) {
        if (isConfirm) {
          // thid
          thisObj.dataService
            .get(
              'user/DeleteUser?UserId=' + userId +
              '&DeletedBy=' + userDetail.UserId)
            .subscribe(r => {
              if (r.IsSuccess) {
                swal('', r.Message);
                thisObj.dataService
                  .get(
                    'User/GetAllUserList?PageSize=' +
                    thisObj.searchModal.pageSize + '&PageNumber=' +
                    thisObj.p + '&OrderBy=' + thisObj.orderBy +
                    '&Order=asc&SearchText=&TotalRecords=0')
                  .subscribe(r => {
                    thisObj.userList = r.UserList;
                    thisObj.totalItem = Math.ceil(
                      r.TotalRecords / thisObj.searchModal.pageSize);
                  })
              } else {
                swal('', r.Message);
              }
            })
        } else {
        }
      });
  }
  assignRole(userId: number) {
    this.userModel = { UserId: userId, UserName: '' };

    this.router.navigate([this.router.url + '/assign-role']);
  }
  ngOnDestroy() {
    this.injectService.userModel = this.userModel;
  }

  // importClient() {

  //     if (this.clientId == null || !this.clientId) {
  //         swal('', 'Please enter client id');
  //     } else {
  //         var regex = /^[1-9]([0-9]{0,45}$)/;
  //         if (regex.test(this.clientId.toString()) == false) {
  //             swal('', 'Please enter client id in correct format');
  //         } else {
  //             this.dataService.get('master/CheckClientInCompanyMaster?ClientId='
  //             + this.clientId).subscribe(
  //                 res => {
  //                     if (res.IsSuccess && res.Data > 0) {
  //                         var thisObj = this;
  //                         swal({
  //                             title: "",
  //                             text: "<span
  //                             style='color:#ef770e;font-size:20px;'>This
  //                             client already exists, Do you want to update
  //                             it?</span><br><br>", html: true,
  //                             showCancelButton: true,
  //                             confirmButtonColor: "#DD6B55",
  //                             confirmButtonText: "Proceed",
  //                             cancelButtonText: "Cancel",
  //                             closeOnConfirm: true,
  //                             closeOnCancel: true
  //                         },
  //                             function (isConfirm) {
  //                                 if (isConfirm) {
  //                                     thisObj.dataService.get('master/UpdateClientInCompanyMaster?ClientId='
  //                                     + thisObj.clientId).subscribe(
  //                                         r => {
  //                                             if (r.IsSuccess) {
  //                                                 thisObj.closeModal();
  //                                                 swal('', r.Message);
  //                                             }
  //                                             else {
  //                                                 thisObj.closeModal();
  //                                                 swal('', r.Message);
  //                                             }
  //                                         }
  //                                     )
  //                                 } else {
  //                                 }
  //                             });
  //                     } else if (res.IsSuccess) {
  //                         this.closeModal();
  //                         swal('', 'Client imported Succcessfully!!');
  //                     } else {
  //                         swal('', res.Message);
  //                     }
  //                 });
  //         }
  //     }
  // }

  closeModal() {
    (<any>$('#import-client')).modal('hide');
    this.clientId = null;
    this.limsCompanyList = [];
    this.clientPortalCompanyList = [];
  }

  getBusinessCategory() {
    var that = this;
    this.isLoader = true;
    this.selectedCompany = '';
    this.dataService
      .get(
        'master/GetCompanyList?BusinessCategoryId=' +
        this.selectedBusinessCategory)
      .subscribe(r => {
        that.isLoader = false;
        this.companyList = r.CompanyList;
      });
  }

  openPopUp(index) {
    this.selectedUser = this.userList[index].UserId;
  }

  CompanyListOnPopUp() {
    var that = this;
    this.isLoader = true;
    this.popUpSelectedCompanyId = '';
    this.dataService
      .get(
        'master/GetCompanyList?BusinessCategoryId=' +
        this.selectedBusinessCategoryId)
      .subscribe(r => {
        that.isLoader = false;
        this.popUpcompanyList = r.CompanyList;

        this.dataService
          .get(
            'ManageInspection/GetCompanyInfo/' + that.selectedUser + '/' +
            that.selectedBusinessCategoryId)
          .subscribe(r => {
            //  that.selectedBusinessCategoryId = 2;
            // that.CompanyListOnPopUp();
            that.popUpSelectedCompanyId = r.Data;
          });
      });
  }

  openManagePermissionPopup(index) {
    let that = this;
    this.selectedUser = this.userList[index].UserId;
    // this.dataService.get('ManageInspection/GetCompanyInfo/' +
    // this.selectedUser+"/"+this.userList[index].BusinessCategoryId).subscribe(
    //     r => {
    //         that.selectedBusinessCategoryId = 2;
    //         that.CompanyListOnPopUp();
    //         console.log(r.Data);
    //         that.popUpSelectedCompanyId = r.Data;
    //     }
    // );
  }

  resetPopup() {
    this.selectedBusinessCategoryId = 0;
    this.popUpcompanyList = [];
    this.popUpSelectedCompanyId = '';
    this.selectedUserCategoryId = 0;
  }

  updateCompanyInfoForInspection() {
    let that = this;
    if (this.selectedUser && this.popUpSelectedCompanyId.CompanyId) {
      that.isLoader = true;
      this.dataService
        .get(
          'ManageInspection/UpdateCompanyInfoForInspection/' +
          this.selectedUser + '/' + this.popUpSelectedCompanyId.CompanyId+'/'+this.selectedBusinessCategoryId)
        .subscribe(r => {
          that.isLoader = false;
          if (r.IsSuccess) {
            (<any>$('#inspectionPermission')).modal('hide');
          }
          swal('', r.Message);
          let companyInfo = r.Data;
          let index =
            that.userList.findIndex(i => i.UserId == that.selectedUser);
          that.userList[index].CompanyName = companyInfo.CompanyName;
          this.resetPopup();
        });
    } else {
      that.isLoader = false;
      swal('', 'Please Select Any Company.');
    }
  }

  clientverifiaction() {
    if (this.popUpSelectedCompanyId && this.selectedUserCategoryId) {
      var obj = {
        CompanyName: this.popUpSelectedCompanyId.CompanyName,
        CompanyId: this.popUpSelectedCompanyId.CompanyId,
        SelectedUserId: this.selectedUser,
        CategoryId: this.selectedUserCategoryId,
        UserId: this.userCredential.UserId
      };
      this.dataService.post('user/VerifyUser', obj).subscribe(r => {
        (<any>$('#Verifiaction')).modal('hide');
        this.search();
        this.resetPopup();
      })
    } else {
      swal('', 'Please Select Company Or Category');
    }
  }

  searchClient() {
    this.isOtherClientChecked = false;
    if (this.clientId.replace(/^\s+/g, '') != '') {
      this.isLoader = true;
      this.dataService
        .get(
          'Master/GetLIMSAndClientPortalClient?SearchText=' + encodeURIComponent(this.clientId))
        .subscribe(res => {
          if (res.IsSuccess) {
            this.limsCompanyList = res.Data.LimsClients;
            this.clientPortalCompanyList = res.Data.ClientPortalClients;
            this.isLoader = false;
          } else {
            this.isLoader = false;
            swal('', res.Message);
          }
        });
    } else {
      swal('', 'Please Enter Correct Client');
      this.clientId = null;
    }
  }


  selectionCheck(event, index) {
    if (event.target.checked) {
      this.selectedClientIndex = index;
      this.importedClientList.push(this.limsCompanyList[index]);
      this.isOtherClientChecked = true;
    } else {
      this.importedClientList = [];
      this.isOtherClientChecked = false;
    }
  }

  importClient() {
    if (this.importedClientList.length > 0) {
      let id = this.clientPortalCompanyList.findIndex(
        i => i.CompanyId == this.importedClientList[0].CompanyId);
      if (id > -1) {
        swal('', 'Selected Client Already Exists In Client Portal!!');
      } else {
        this.isLoader = true;
        this.dataService
          .post('Master/AddClientInClientPortal', this.importedClientList[0])
          .subscribe(res => {
            if (res.IsSuccess) {
              this.isLoader = false;
              (<any>$('#import-client')).modal('hide');
              this.resetclient();
              swal('', res.Message);
            } else {
              this.isLoader = false;
            }
          });
      }
    } else {
      swal('', 'Please Select Client!!');
    }
  }

  resetclient() {
    this.clientId = null;
    this.limsCompanyList = [];
    this.importedClientList = [];
    this.isOtherClientChecked = false;
    this.clientPortalCompanyList = [];
  }


  GetPassword(userId,i)
  {
this.isLoader=true;
this.userEmail=this.userList[i].UserName;
    this.dataService
    .get('User/GetUserPassword?UserId=' + userId)
    .subscribe(response => {
            if (response.IsSuccess) {
              // console.log("Password",response.Data);
              this.userPassword=response.Data;
              this.isLoader=false;
              (<any>$('#PasswordModal')).modal('show');
            }
          });

  }
}
