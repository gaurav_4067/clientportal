export class AssignRole {
    AssignedRoles: any;
    AssignedCompanyId: number;
    AssignedCompanyName: string;
    AssignedBusinessCategoryID: number;
    AssignedBusinessCategoryName: string;
}
