import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { InjectService } from '../../../injectable-service/injectable-service';
import { DataService } from '../../../mts.service';

import { AssignRole } from './assign-role';

declare let swal: any;

@Component({
  selector: 'app-assign-role',
  templateUrl: './assign-role.component.html',
  styleUrls: ['./assign-role.component.css'],
  providers: [AssignRole]
})
export class AssignRoleComponent implements OnInit {
  createdBy: number;
  assignedRoleListData: any;
  selectedCompany: number;
  selectedClientCompany:any;
  selectedRole: any;
  selectedCategory: number;
  companyList: any;
  mappedCompanyList: any;
  categoryList: any;
  hideModal: boolean;
  isEditRole: boolean;
  selectedBusinessCategory: any;

  selectedClient: number = 0;
  selectedTRFPermissionType: number = 0;
  selectedUserId: any;

  private roleList: any;
  private selectRoleText: any = { defaultTitle: 'Select Role' };
  postAssignRole: any;
  isLoader: boolean;
  userCredential: any;
  userDetail: any;
  constructor(
    private dataService: DataService, private injectService: InjectService,
    private router: Router, private assignRoleModel: AssignRole) { }
  ngOnInit() {
    this.userCredential = JSON.parse(localStorage.getItem('mts_user_cred'));
    this.userDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (this.userDetail && this.userCredential) {
      if (this.userCredential.UserPermission.AssignRole) {
        this.selectedCategory = 0;
        this.selectedCompany = 0;
        this.hideModal = false;
        this.createdBy = this.userCredential.UserId;
        this.postAssignRole = {};
        this.assignedRoleListData = {};
        // this.assignedRoleListData.UserCategoryId=0;
        this.isLoader = true;
        var that = this;
        if (this.injectService.userModel) {
          this.dataService
            .get(
              'user/GetUserAssignedRole?UserId=' +
              this.injectService.userModel.UserId)
            .subscribe(r => {
              // this.isLoader = false;
              this.assignedRoleListData = r;
              this.assignedRoleListData.AssignedRoleList.forEach(function (item) {
                var assignedRoles = '';
                item.AssignedRoles.forEach(function (role) {
                  assignedRoles += role.RoleName + ',';
                })

                item['assignedRoleStringyFy'] =
                  assignedRoles.substr(0, assignedRoles.length - 1);
              })
              that.selectedBusinessCategory = r.busniessCategoryId;

              that.dataService
                .get(
                  'master/GetCompanyList?BusinessCategoryId=' +
                  r.busniessCategoryId)
                .subscribe(r => {
                  that.companyList = r.CompanyList;
                  that.companyList.splice(
                    0, 0, { 'CompanyId': 0, 'CompanyName': 'Select Company' });
                });
              that.dataService.get('master/GetUserCategory').subscribe(r => {
                that.categoryList = r.UserCategoryList;
                that.categoryList.splice(
                  0, 0,
                  { 'UserCategoryId': 0, 'UserCategoryName': 'Select Category' });
              });
              that.dataService
                .get(
                  'master/RoleList?PageSize=10000&PageNumber=1&OrderBy=RoleName&Order=asc&SearchText=&TotalRecords=0')
                .subscribe(r => {
                  that.isLoader = false;
                  var list = [];
                  r.RoleList.forEach(function (company) {
                    list.push({ id: company.RoleId, name: company.RoleName })
                  })
                  that.roleList = list;
                });
              this.getBusinessCategory();
            })
        } else {
          this.router.navigate(['/landing-page']);
        }
      } else {
        this.router.navigate(['/login']);
      }
    }

    // this.dataService.get('master/GetCompanyList').subscribe(
    //     r => {
    //         this.companyList = r.CompanyList;
    //         this.companyList.splice(0, 0, { "CompanyId": 0, "CompanyName":
    //         "Select Company" });
    //     }
    // );
    // this.dataService.get('master/GetUserCategory').subscribe(
    //     r => {
    //         this.categoryList = r.UserCategoryList;
    //         this.categoryList.splice(0, 0, { "UserCategoryId": 0,
    //         "UserCategoryName": "Select Category" });
    //     }
    // );
    // this.dataService.get('master/RoleList?PageSize=10000&PageNumber=1&OrderBy=RoleName&Order=asc&SearchText=&TotalRecords=0').subscribe(
    //     r => {
    //         this.isLoader = false;
    //         var list = [];
    //         r.RoleList.forEach(function (company) {
    //             list.push({ id: company.RoleId, name: company.RoleName })
    //         })
    //         this.roleList = list;
    //     }
    // )
  }
  saveRole() { }

  categoryChange() {
    // alert(this.selectedCategory);
    var that = this;
    if (this.assignedRoleListData.UserCategoryId) {
      this.isLoader = true;
      // this.selectedCategory = 0;
      this.selectedCompany = 0;
      this.dataService
        .get(
          'user/GetMappedCompanyList?UserCategoryId=' +
          this.assignedRoleListData.UserCategoryId +
          '&CompanyId=' + this.assignedRoleListData.UserCompanyId +
          '&BusinessCategoryId=' + that.selectedCategory)
        .subscribe(r => {
          this.isLoader = false;
          (<any>$('#assignRole')).modal('show');
          that.mappedCompanyList = r.CompanyList;
          // that.mappedCompanyList.splice(
          //   0, 0, { 'CompanyId': 0, 'CompanyName': 'Select Company' });
        });
    }
  }

  modalPopUp() {
    this.isEditRole = false;
    if (this.assignedRoleListData.UserCategoryId > 1) {
      if (this.assignedRoleListData.UserCompanyId > 0) {
      } else {
        swal('', 'Please Select Company');
        return;
      }
      this.selectedCategory = this.selectedBusinessCategory;
      this.categoryChange();
    }


    (<any>$('#assignRole')).modal('show');
    // this.categoryChange();
    // if (this.assignedRoleListData.UserCategoryId) {
    //     this.isLoader = true;
    //     this.selectedCategory = 0;
    //     this.selectedCompany = 0;
    //     this.dataService.get('user/GetMappedCompanyList?UserCategoryId=' +
    //     this.assignedRoleListData.UserCategoryId + '&CompanyId=' +
    //     this.assignedRoleListData.UserCompanyId + '&BusinessCategoryId=' +
    //     this.selectedCategory).subscribe(
    //         r => {
    //             this.isLoader = false;
    //             (<any>$('#assignRole')).modal('show');
    //             this.mappedCompanyList = r.CompanyList;
    //             this.mappedCompanyList.splice(0, 0, { "CompanyId": 0,
    //             "CompanyName": "Select Company" });
    //         }
    //     );
    // }
  }

  assignRole(action: string) {
    this.selectedCompany=this.selectedClientCompany.CompanyId;
    if (!this.selectedRole || !this.selectedCategory || !this.selectedCompany) {
      swal('', 'Please select all fields.');
      return;
    }

    if (this.selectedRole) {
      var roleList = [];
      this.selectedRole.forEach(function (roleId) {
        roleList.push({ RoleId: roleId });
      });
      this.assignRoleModel.AssignedRoles = roleList;
    }
    if (this.selectedCategory) {
      this.assignRoleModel.AssignedBusinessCategoryID = this.selectedCategory;
    }
    if (this.selectedCompany) {
      this.assignRoleModel.AssignedCompanyId = this.selectedCompany;
    }

    this.postAssignRole.UserCategoryId =
      this.assignedRoleListData.UserCategoryId;
    this.postAssignRole.UserCompanyId = this.assignedRoleListData.UserCompanyId;
    this.postAssignRole.UserId = this.injectService.userModel.UserId;
    this.postAssignRole.AssignedRoleList = [];
    this.postAssignRole.AssignedRoleList[0] = this.assignRoleModel;
    this.postAssignRole.CreatedBy = this.createdBy;
    this.postAssignRole.busniessCategoryId = this.selectedCategory;



    if (action == 'delete') {
      var thisObj = this;
      swal(
        {
          title: '',
          text:
            '<span style=\'color:#ef770e;font-size:20px;\'>Are you sure?</span><br><br>',
          html: true,
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Proceed',
          cancelButtonText: 'Cancel',
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function (isConfirm) {
          if (isConfirm) {
            // thid
            thisObj.isLoader = true;
            thisObj.dataService
              .post('user/DeleteUserRole', thisObj.postAssignRole)
              .subscribe(r => {
                thisObj.isLoader = false;

                if (r.IsSuccess) {
                  thisObj.selectedRole = [];
                  thisObj.selectedCategory = null;
                  thisObj.selectedCompany = null;
                  thisObj.selectedClientCompany='';
                  thisObj.dataService
                    .get(
                      'user/GetUserAssignedRole?UserId=' +
                      thisObj.injectService.userModel.UserId)
                    .subscribe(r => {
                      thisObj.assignedRoleListData.AssignedRoleList =
                        r.AssignedRoleList;
                      thisObj.assignedRoleListData.AssignedRoleList
                        .forEach(function (item) {
                          var assignedRoles = '';
                          item.AssignedRoles.forEach(function (role) {
                            assignedRoles += role.RoleName + ',';
                          })

                          item['assignedRoleStringyFy'] =
                            assignedRoles.substr(
                              0, assignedRoles.length - 1);
                        })
                    })
                } else {
                  swal('', r.Message);
                }
              })
          } else {
          }
        });


    }

    else {
      this.isLoader = true;
      this.dataService.post('user/AssignUserRole', this.postAssignRole)
        .subscribe(r => {
          this.isLoader = false;
          (<any>$('#assignRole')).modal('hide');
          var that = this;
          if (r.IsSuccess) {
            this.selectedRole = [];
            this.selectedCategory = null;
            this.selectedCompany = null;
            this.selectedClientCompany='';
            this.dataService
              .get(
                'user/GetUserAssignedRole?UserId=' +
                this.injectService.userModel.UserId)
              .subscribe(r => {
                that.assignedRoleListData.AssignedRoleList =
                  r.AssignedRoleList;
                that.assignedRoleListData.AssignedRoleList.forEach(function (
                  item) {
                  var assignedRoles = '';
                  item.AssignedRoles.forEach(function (role) {
                    assignedRoles += role.RoleName + ',';
                  })

                  item['assignedRoleStringyFy'] =
                    assignedRoles.substr(0, assignedRoles.length - 1);
                })
              })
          } else {
            swal('', r.Message);
          }
        })
    }
  }

  editRole(user) {
    this.isEditRole = true;
    this.selectedRole = [];
    let list = [];
    user.AssignedRoles.forEach(function (role) {
      list.push(role.RoleId);
    });
    this.selectedRole = list;
    this.selectedCategory = user.AssignedBusinessCategoryID;
    this.dataService
      .get(
        'user/GetMappedCompanyList?UserCategoryId=' +
        this.userDetail.UserCategoryId +
        '&CompanyId=' + this.assignedRoleListData.UserCompanyId +
        '&BusinessCategoryId=' + this.selectedCategory)
      .subscribe(r => {
        this.isLoader = false;
        (<any>$('#assignRole')).modal('show');
        this.mappedCompanyList = r.CompanyList;
        // this.mappedCompanyList.splice(
        //   0, 0, { 'CompanyId': 0, 'CompanyName': 'Select Company' });
          let index = this.mappedCompanyList.findIndex(i => i.CompanyId == user.AssignedCompanyId);
          if (index > -1) {
            this.selectedClientCompany = this.mappedCompanyList[index];
          }
      });
    this.selectedCompany = user.AssignedCompanyId;
  }

  deleteRole(user) {
    this.selectedCompany = user.AssignedCompanyId;
    this.selectedRole = [];
    let list = [];
    user.AssignedRoles.forEach(function (role) {
      list.push(role.RoleId);
    });
    this.selectedRole = list;
    this.selectedCategory = user.AssignedBusinessCategoryID;

    this.assignRole('delete');
  }

  closeDialog() {
    this.selectedRole = [];
    this.selectedClientCompany='';
    this.selectedCategory = null;
    this.selectedCompany = null;
    (<any>$('#assignRole')).modal('hide');
  }
  getBusinessCategory() {
    var that = this;
    this.isLoader = true;
    this.dataService
      .get(
        'master/GetCompanyList?BusinessCategoryId=' +
        this.selectedBusinessCategory)
      .subscribe(r => {
        that.isLoader = false;
        that.companyList = r.CompanyList;
        that.companyList.splice(
          0, 0, { 'CompanyId': 0, 'CompanyName': 'Select Company' });
        if (that.assignedRoleListData.UserCategoryId != 1) {
          this.dataService
            .get(
              'ManageInspection/GetCompanyInfo/' +
              this.assignedRoleListData.UserId + '/' +
              this.selectedBusinessCategory)
            .subscribe(r => {
              if (r.Data == null) {
                that.assignedRoleListData.UserCompanyId = 0;
              } else {
                that.assignedRoleListData.UserCompanyId = r.Data.CompanyId;
              }
            });
        }
      });
  }

  openSFTRFPermissionPopup( ) {
    this.selectedUserId=this.injectService.userModel.UserId;
    this.selectedClient = 0;
    this.selectedTRFPermissionType = 0;
    (<any>$('#sfPopupPermission')).modal('show');
  }

  updateSFTRFTypePermission() {
    this.isLoader = true;
    //alert(this.selectedClient + "   test   " + this.selectedTRFPermissionType);
    this.dataService
      .get('SF/UpdateSFTRFTypePermission/' + this.selectedTRFPermissionType + '/' + this.selectedUserId)
      .subscribe(res => {
        if (res.IsSuccess) {
          swal('', res.Message);
          this.isLoader = false;
        } else {
          this.isLoader = false;
        }
        (<any>$('#sfPopupPermission')).modal('hide');
      });
  }

  public renderCompany(data: any): string {
    const html = `
      <div class="data-row">
        <div class="col-2">${data.CompanyName}</div>
      </div>`;

    return html;
  }
}
