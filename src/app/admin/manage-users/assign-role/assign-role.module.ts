import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssignRoleRoutingModule } from './assign-role-routing.module';
import { AssignRoleComponent } from './assign-role.component';
import { FooterModule } from '../../../footer/footer.module';
import { HeaderModule } from '../../../header/header.module';
import { FormsModule } from '@angular/forms';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

@NgModule({
  imports: [
    CommonModule,
    FooterModule,
    FormsModule,
    HeaderModule,
    MultiselectDropdownModule,
    AssignRoleRoutingModule,
    Ng2AutoCompleteModule
  ],
  declarations: [AssignRoleComponent]
})
export class AssignRoleModule { }
