import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageUsersComponent } from './manage-users.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ManageUsersComponent,
      },
      {
        path: 'assign-role',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], require => {
            resolve(require('./assign-role/assign-role.module').AssignRoleModule);
          })
        })
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageUserRoutingModule { }
