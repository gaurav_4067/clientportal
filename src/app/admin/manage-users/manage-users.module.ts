import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageUserRoutingModule } from './manage-users-routing.module';
import { ManageUsersComponent } from './manage-users.component';
import { FooterModule } from '../../footer/footer.module';
import { HeaderModule } from '../../header/header.module';
import { FormsModule } from '@angular/forms';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { Ng2PaginationModule } from 'ng2-pagination';
import { MaterialModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FooterModule,
    HeaderModule,
    MaterialModule,
    Ng2AutoCompleteModule,
    Ng2PaginationModule,
    ManageUserRoutingModule
  ],
  declarations: [ManageUsersComponent]
})
export class ManageUsersModule { }
