import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderInspectionComponent } from 'app/web-link/order-inspection/order-inspection.component';

const routes: Routes = [{
  path:'',
  component:OrderInspectionComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderInspectionRoutingModule { }
