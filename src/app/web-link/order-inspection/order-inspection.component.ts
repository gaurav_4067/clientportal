import { Component, OnInit } from '@angular/core';
import { DataService, ETRFUrl, BaseUrl } from '../../mts.service';
import { Router } from '@angular/router';
declare let swal: any;

@Component({
  selector: 'app-order-inspection',
  templateUrl: './order-inspection.component.html',
  styleUrls: ['./order-inspection.component.css']
})
export class OrderInspectionComponent implements OnInit {

  images: Array<any>;
  order: any;
  orderList: any;
  isLoader: boolean;

  constructor(private dataService: DataService, public router: Router) {
  }

  ngOnInit() {
      this.isLoader = true;
      var ReportNo = window.location.href;
      var data = ReportNo.split('/');
      ReportNo = atob(data[data.length - 1]);
      this.dataService.getOrder('Order/GetInspectionOrder?Sample=' + ReportNo).subscribe(
          response => {
              this.isLoader = false;
              if (response.IsSuccess) {
                  this.orderList = response.Dt;
                  this.order = this.orderList[0];
              }
              else {
                  swal('', response.Message)
              }
          }
      );
  }

}
