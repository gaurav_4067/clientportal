import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderInspectionRoutingModule } from './order-inspection-routing.module';
import { OrderInspectionComponent } from './order-inspection.component';

@NgModule({
  imports: [
    CommonModule,
    OrderInspectionRoutingModule
  ],
  declarations: [OrderInspectionComponent]
})
export class OrderInspectionModule { }
