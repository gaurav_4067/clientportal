import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path:'',
  children:[
    {
      path: 'order/:reportNo',
      loadChildren: () => new Promise(resolve => {
       (require as any).ensure([], require => {
         resolve(require('./order/order.module').OrderModule);
       })
     })
   },
   {
    path: 'order-inspection/:reportNo',
    loadChildren: () => new Promise(resolve => {
     (require as any).ensure([], require => {
       resolve(require('./order-inspection/order-inspection.module').OrderInspectionModule);
     })
   })
 }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebLinkRoutingModule { }
