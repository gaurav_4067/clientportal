import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebLinkRoutingModule } from './web-link-routing.module';

@NgModule({
  imports: [
    CommonModule,
    WebLinkRoutingModule
  ],
  declarations: []
})
export class WebLinkModule { }
