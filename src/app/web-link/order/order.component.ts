import { Component, OnInit } from '@angular/core';
import { DataService, ETRFUrl, BaseUrl } from '../../mts.service';
import { Router } from '@angular/router';
declare let swal: any;

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  images: Array<any>;
  order: any;
  orderList: any;
  isLoader: boolean;
  constructor(private dataService: DataService, public router: Router) {
    this.images = new Array<string>();
  }
  ngOnInit() {
    this.isLoader = true;
    var ReportNo = window.location.href;
    var data = ReportNo.split('/');
    ReportNo = atob(data[data.length - 1]);
    this.dataService.getOrder('Order/GetOrder?Sample=' + ReportNo).subscribe(
      response => {
        this.isLoader = false;
        if (response.IsSuccess) {
          this.orderList = response.Dt;
          //this.orderList= this.orderList.json();
          this.order = this.orderList[0];
          response.SampleImages.forEach(element => {
            this.images.push(ETRFUrl + element);
          });
          //this.images=response.SampleImages;
        }
        else {
          swal('', response.Message)
        }
      }
    );
  }

  download(fileName) {
    let downloadFileName = encodeURIComponent(fileName.trim());
    window.location.href = BaseUrl + "ManageDocuments/DownloadFile?FileName=" + downloadFileName.trim();;
  }

}
