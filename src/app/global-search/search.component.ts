/**
 * Created by azmat on 16/12/16.
 */
import { Component } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { DataService } from '../mts.service';
@Component({
    selector: 'dummy',
    templateUrl: './search.html'
})
export class SearchComponent{
    userCredential:any;
    searchList:any;
    searchParams:any;

    constructor(public router:Router,public route:ActivatedRoute, private dataService:DataService) {
    }
    ngOnInit() {
        this.userCredential=JSON.parse(localStorage.getItem('mts_user_cred'));
        this.searchParams={};
        this.searchParams.ClientId = this.userCredential.CompanyId;
        this.searchParams.UserType = this.userCredential.UserType;
        this.searchParams.UserCompanyId = this.userCredential.UserCompanyId;
        this.searchParams.SearchText = '';
        if(this.route.params){
            this.route.params.subscribe(params => {
                this.searchParams.SearchText=params['search'];
            });
        }
        this.dataService.post('Master/GoogleSearch',this.searchParams).subscribe(
                res => {
                    if(res.IsSuccess){
                        this.searchList=res.GoogleSearchResult;
                    }
            }
        );
    }
    searchPage(url,value){
        this.router.navigate(['./'+url.toLowerCase(), value]);
    }
}

