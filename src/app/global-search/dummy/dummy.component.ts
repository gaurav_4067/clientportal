/**
 * Created by azmat on 16/12/16.
 */
import { Component } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
    selector: 'dummy',
    templateUrl: './dummy.html'
})
export class DummyComponent{
    constructor(public router:Router,public route:ActivatedRoute) {
    }
    ngOnInit() {
        if(this.route.params){
            this.route.params.subscribe(params => {
                this.router.navigate(['./search', params['search']]);
            });
        }
    }
}

