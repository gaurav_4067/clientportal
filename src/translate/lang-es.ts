// lang-es.ts

export const LANG_ES_NAME = 'es';

export const LANG_ES_TRANS = {
    'hello world': 'hola mundo',
    'name':'azamata',
    'Index':'Indexa',
    'Home':'Homea',
    'Detail':'Detaila',
    'About':'Abouta',
    'Role':'Rolea',
};